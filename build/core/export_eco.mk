ifdef IMPORTS
ELASTOS_INCLUDES = $(OUT_DIR)/elastos_include

IMPORTS:= $(strip $(IMPORTS))       # remove multiple spaces
IMPORTS:= $(filter-out \,$(IMPORTS))
IMPORTHS=$(sort $(IMPORTS))

$(IMPORTHS): $(XDK_TOOLS)/libs/$@
	@echo Generating H files from $@
	@if [ ! -f "$(ELASTOS_INCLUDES)" ]; then mkdir -p $(ELASTOS_INCLUDES); fi
	@cd $(ELASTOS_INCLUDES);$(XDK_TOOLS)/lube -C $(XDK_TOOLS)/libs/$@ -f -T header2 -T headercpp

endif
