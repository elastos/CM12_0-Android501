
package com.android.internal.telephony;

import android.telephony.SubInfoRecord;
import java.util.List;

/**
 * @hide
 */
class ElSubscriptionController extends ISub.Stub
{
    private long mNativeProxy;

    private native SubInfoRecord nativeGetSubInfoForSubscriber(long nativeProxy, long subId);
    private native List<SubInfoRecord> nativeGetSubInfoUsingIccId(long nativeProxy, String iccId);
    private native List<SubInfoRecord> nativeGetSubInfoUsingSlotId(long nativeProxy, int slotId);
    private native List<SubInfoRecord> nativeGetAllSubInfoList(long nativeProxy);
    private native List<SubInfoRecord> nativeGetActiveSubInfoList(long nativeProxy);
    private native int nativeGetAllSubInfoCount(long nativeProxy);
    private native int nativeGetActiveSubInfoCount(long nativeProxy);
    private native int nativeAddSubInfoRecord(long nativeProxy, String iccId, int slotId);
    private native int nativeSetColor(long nativeProxy, int color, long subId);
    private native int nativeSetDisplayName(long nativeProxy, String displayName, long subId);
    private native int nativeSetDisplayNameUsingSrc(long nativeProxy, String displayName, long subId, long nameSource);
    private native int nativeSetDisplayNumber(long nativeProxy, String number, long subId);
    private native int nativeSetDisplayNumberFormat(long nativeProxy, int format, long subId);
    private native int nativeSetDataRoaming(long nativeProxy, int roaming, long subId);
    private native int nativeGetSlotId(long nativeProxy, long subId);
    private native long[] nativeGetSubId(long nativeProxy, int slotId);
    private native long nativeGetDefaultSubId(long nativeProxy);
    private native int nativeClearSubInfo(long nativeProxy);
    private native int nativeGetPhoneId(long nativeProxy, long subId);
    private native long nativeGetDefaultDataSubId(long nativeProxy);
    private native void nativeSetDefaultDataSubId(long nativeProxy, long subId);
    private native long nativeGetDefaultVoiceSubId(long nativeProxy);
    private native void nativeSetDefaultVoiceSubId(long nativeProxy, long subId);
    private native long nativeGetDefaultSmsSubId(long nativeProxy);
    private native void nativeSetDefaultSmsSubId(long nativeProxy, long subId);
    private native void nativeClearDefaultsForInactiveSubIds(long nativeProxy);
    private native long[] nativeGetActiveSubIdList(long nativeProxy);
    private native boolean nativeIsSMSPromptEnabled(long nativeProxy);
    private native void nativeSetSMSPromptEnabled(long nativeProxy, boolean enabled);
    private native boolean nativeIsVoicePromptEnabled(long nativeProxy);
    private native void nativeSetVoicePromptEnabled(long nativeProxy, boolean enabled);
    private native void nativeActivateSubId(long nativeProxy, long subId);
    private native void nativeDeactivateSubId(long nativeProxy, long subId);
    private native int nativeSetSubState(long nativeProxy, long subId, int subStatus);
    private native int nativeGetSubState(long nativeProxy, long subId);
    private native long nativeGetOnDemandDataSubId(long nativeProxy);

    public ElSubscriptionController(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    /**
     * Get the SubInfoRecord according to an index
     * @param subId The unique SubInfoRecord index in database
     * @return SubInfoRecord, maybe null
     */
    @Override
    public SubInfoRecord getSubInfoForSubscriber(long subId) {
        return nativeGetSubInfoForSubscriber(mNativeProxy, subId);
    }

    /**
     * Get the SubInfoRecord according to an IccId
     * @param iccId the IccId of SIM card
     * @return SubInfoRecord, maybe null
     */
    @Override
    public List<SubInfoRecord> getSubInfoUsingIccId(String iccId) {
        return nativeGetSubInfoUsingIccId(mNativeProxy, iccId);
    }

    /**
     * Get the SubInfoRecord according to slotId
     * @param slotId the slot which the SIM is inserted
     * @return SubInfoRecord, maybe null
     */
    @Override
    public List<SubInfoRecord> getSubInfoUsingSlotId(int slotId) {
        return nativeGetSubInfoUsingSlotId(mNativeProxy, slotId);
    }

    /**
     * Get all the SubInfoRecord(s) in subinfo database
     * @return Array list of all SubInfoRecords in database, include thsoe that were inserted before
     */
    @Override
    public List<SubInfoRecord> getAllSubInfoList() {
        return nativeGetAllSubInfoList(mNativeProxy);
    }

    /**
     * Get the SubInfoRecord(s) of the currently inserted SIM(s)
     * @return Array list of currently inserted SubInfoRecord(s)
     */
    @Override
    public List<SubInfoRecord> getActiveSubInfoList() {
        return nativeGetActiveSubInfoList(mNativeProxy);
    }

    /**
     * Get the SUB count of all SUB(s) in subinfo database
     * @return all SIM count in database, include what was inserted before
     */
    @Override
    public int getAllSubInfoCount() {
        return nativeGetAllSubInfoCount(mNativeProxy);
    }

    /**
     * Get the count of active SUB(s)
     * @return active SIM count
     */
    @Override
    public int getActiveSubInfoCount() {
        return nativeGetActiveSubInfoCount(mNativeProxy);
    }

    /**
     * Add a new SubInfoRecord to subinfo database if needed
     * @param iccId the IccId of the SIM card
     * @param slotId the slot which the SIM is inserted
     * @return the URL of the newly created row or the updated row
     */
    @Override
    public int addSubInfoRecord(String iccId, int slotId) {
        return nativeAddSubInfoRecord(mNativeProxy, iccId, slotId);
    }

    /**
     * Set SIM color by simInfo index
     * @param color the color of the SIM
     * @param subId the unique SubInfoRecord index in database
     * @return the number of records updated
     */
    @Override
    public int setColor(int color, long subId) {
        return nativeSetColor(mNativeProxy, color, subId);
    }

    /**
     * Set display name by simInfo index
     * @param displayName the display name of SIM card
     * @param subId the unique SubInfoRecord index in database
     * @return the number of records updated
     */
    @Override
    public int setDisplayName(String displayName, long subId) {
        return nativeSetDisplayName(mNativeProxy, displayName, subId);
    }

    /**
     * Set display name by simInfo index with name source
     * @param displayName the display name of SIM card
     * @param subId the unique SubInfoRecord index in database
     * @param nameSource, 0: DEFAULT_SOURCE, 1: SIM_SOURCE, 2: USER_INPUT
     * @return the number of records updated
     */
    @Override
    public int setDisplayNameUsingSrc(String displayName, long subId, long nameSource) {
        return nativeSetDisplayNameUsingSrc(mNativeProxy, displayName, subId, nameSource);
    }

    /**
     * Set phone number by subId
     * @param number the phone number of the SIM
     * @param subId the unique SubInfoRecord index in database
     * @return the number of records updated
     */
    @Override
    public int setDisplayNumber(String number, long subId) {
        return nativeSetDisplayNumber(mNativeProxy, number, subId);
    }

    /**
     * Set number display format. 0: none, 1: the first four digits, 2: the last four digits
     * @param format the display format of phone number
     * @param subId the unique SubInfoRecord index in database
     * @return the number of records updated
     */
    @Override
    public int setDisplayNumberFormat(int format, long subId) {
        return nativeSetDisplayNumberFormat(mNativeProxy, format, subId);
    }

    /**
     * Set data roaming by simInfo index
     * @param roaming 0:Don't allow data when roaming, 1:Allow data when roaming
     * @param subId the unique SubInfoRecord index in database
     * @return the number of records updated
     */
    @Override
    public int setDataRoaming(int roaming, long subId) {
        return nativeSetDataRoaming(mNativeProxy, roaming, subId);
    }

    @Override
    public int getSlotId(long subId) {
        return nativeGetSlotId(mNativeProxy, subId);
    }

    @Override
    public long[] getSubId(int slotId) {
        return nativeGetSubId(mNativeProxy, slotId);
    }

    @Override
    public long getDefaultSubId() {
        return nativeGetDefaultSubId(mNativeProxy);
    }

    @Override
    public int clearSubInfo() {
        return nativeClearSubInfo(mNativeProxy);
    }

    @Override
    public int getPhoneId(long subId) {
        return nativeGetPhoneId(mNativeProxy, subId);
    }

    /**
     * Get the default data subscription
     * @return Id of the data subscription
     */
    @Override
    public long getDefaultDataSubId() {
        return nativeGetDefaultDataSubId(mNativeProxy);
    }

    @Override
    public void setDefaultDataSubId(long subId) {
        nativeSetDefaultDataSubId(mNativeProxy, subId);
    }

    @Override
    public long getDefaultVoiceSubId() {
        return nativeGetDefaultVoiceSubId(mNativeProxy);
    }

    @Override
    public void setDefaultVoiceSubId(long subId) {
        nativeSetDefaultVoiceSubId(mNativeProxy, subId);
    }

    @Override
    public long getDefaultSmsSubId() {
        return nativeGetDefaultSmsSubId(mNativeProxy);
    }

    @Override
    public void setDefaultSmsSubId(long subId) {
        nativeSetDefaultSmsSubId(mNativeProxy, subId);
    }

    @Override
    public void clearDefaultsForInactiveSubIds() {
        nativeClearDefaultsForInactiveSubIds(mNativeProxy);
    }

    @Override
    public long[] getActiveSubIdList() {
        return nativeGetActiveSubIdList(mNativeProxy);
    }

    @Override
    public boolean isSMSPromptEnabled() {
        return nativeIsSMSPromptEnabled(mNativeProxy);
    }

    @Override
    public void setSMSPromptEnabled(boolean enabled) {
        nativeSetSMSPromptEnabled(mNativeProxy, enabled);
    }

    @Override
    public boolean isVoicePromptEnabled() {
        return nativeIsVoicePromptEnabled(mNativeProxy);
    }

    @Override
    public void setVoicePromptEnabled(boolean enabled) {
        nativeSetVoicePromptEnabled(mNativeProxy, enabled);
    }

    @Override
    public void activateSubId(long subId) {
        nativeActivateSubId(mNativeProxy, subId);
    }

    @Override
    public void deactivateSubId(long subId) {
        nativeDeactivateSubId(mNativeProxy, subId);
    }

    @Override
    public int setSubState(long subId, int subStatus) {
        return nativeSetSubState(mNativeProxy, subId, subStatus);
    }

    @Override
    public int getSubState(long subId) {
        return nativeGetSubState(mNativeProxy, subId);
    }

    @Override
    public long getOnDemandDataSubId() {
        return nativeGetOnDemandDataSubId(mNativeProxy);
    }
}
