
package com.android.internal.telephony;

import android.telephony.SubInfoRecord;
import android.app.PendingIntent;
import android.net.Uri;
import java.util.List;

/**
 * @hide
 */
class ElUiccSmsController extends ISms.Stub
{
    private long mNativeProxy;

    public ElUiccSmsController(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    private native void nativeSynthesizeMessages(long nativeProxy, String originatingAddress, String scAddress, List<String> messages, long timestampMillis);

    private native List<SmsRawData> nativeGetAllMessagesFromIccEf(long nativeProxy, String callingPkg);

    private native List<SmsRawData> nativeGetAllMessagesFromIccEfForSubscriber(long nativeProxy, long subId, String callingPkg);

    private native boolean nativeUpdateMessageOnIccEf(long nativeProxy, String callingPkg, int messageIndex, int newStatus,
        byte[] pdu);

    private native boolean nativeUpdateMessageOnIccEfForSubscriber(long nativeProxy, long subId, String callingPkg,
         int messageIndex, int newStatus, byte[] pdu);

    private native boolean nativeCopyMessageToIccEf(long nativeProxy, String callingPkg, int status, byte[] pdu, byte[] smsc);

    private native boolean nativeCopyMessageToIccEfForSubscriber(long nativeProxy, long subId, String callingPkg, int status,
        byte[] pdu, byte[] smsc);

    private native void nativeSendData(long nativeProxy, String callingPkg, String destAddr, String scAddr, int destPort,
        byte[] data, PendingIntent sentIntent, PendingIntent deliveryIntent);

    private native void nativeSendDataForSubscriber(long nativeProxy, long subId, String callingPkg, String destAddr,
        String scAddr, int destPort, byte[] data, PendingIntent sentIntent,
        PendingIntent deliveryIntent);

    private native void nativeSendDataWithOrigPort(long nativeProxy, String callingPkg, String destAddr, String scAddr,
        int destPort, int origPort, byte[] data, PendingIntent sentIntent,
        PendingIntent deliveryIntent);

    private native void nativeSendDataWithOrigPortUsingSubscriber(long nativeProxy, long subId, String callingPkg, String destAddr,
        String scAddr,int destPort, int origPort, byte[] data,
        PendingIntent sentIntent, PendingIntent deliveryIntent);

    private native void nativeSendText(long nativeProxy, String callingPkg, String destAddr, String scAddr, String text,
        PendingIntent sentIntent, PendingIntent deliveryIntent);

    private native void nativeSendTextForSubscriber(long nativeProxy, long subId, String callingPkg, String destAddr,
        String scAddr, String text, PendingIntent sentIntent,
        PendingIntent deliveryIntent);

    private native void nativeSendTextWithOptionsUsingSubscriber(long nativeProxy, long subId, String callingPkg, String destAddr,
        String scAddr, String text, PendingIntent sentIntent,
        PendingIntent deliveryIntent, int priority, boolean isExpectMore,
        int validityPeriod);

    private native void nativeInjectSmsPdu(long nativeProxy, byte[] pdu, String format, PendingIntent receivedIntent);

    private native void nativeInjectSmsPduForSubscriber(long nativeProxy, long subId, byte[] pdu, String format,
        PendingIntent receivedIntent);

    private native void nativeUpdateSmsSendStatus(long nativeProxy, int messageRef, boolean success);

    private native void nativeSendMultipartText(long nativeProxy, String callingPkg, String destinationAddress, String scAddress,
        List<String> parts, List<PendingIntent> sentIntents,
        List<PendingIntent> deliveryIntents);

    private native void nativeSendMultipartTextForSubscriber(long nativeProxy, long subId, String callingPkg,
        String destinationAddress, String scAddress,
        List<String> parts, List<PendingIntent> sentIntents,
        List<PendingIntent> deliveryIntents);

    private native void nativeSendMultipartTextWithOptionsUsingSubscriber(long nativeProxy, long subId, String callingPkg,
        String destinationAddress, String scAddress, List<String> parts,
        List<PendingIntent> sentIntents, List<PendingIntent> deliveryIntents,
        int priority, boolean isExpectMore, int validityPeriod);

    private native boolean nativeEnableCellBroadcast(long nativeProxy, int messageIdentifier);

    private native boolean nativeEnableCellBroadcastForSubscriber(long nativeProxy, long subId, int messageIdentifier);

    private native boolean nativeDisableCellBroadcast(long nativeProxy, int messageIdentifier);

    private native boolean nativeDisableCellBroadcastForSubscriber(long nativeProxy, long subId, int messageIdentifier);

    private native boolean nativeEnableCellBroadcastRange(long nativeProxy, int startMessageId, int endMessageId);

    private native boolean nativeEnableCellBroadcastRangeForSubscriber(long nativeProxy, long subId, int startMessageId, int endMessageId);

    private native boolean nativeDisableCellBroadcastRange(long nativeProxy, int startMessageId, int endMessageId);

    private native boolean nativeDisableCellBroadcastRangeForSubscriber(long nativeProxy, long subId, int startMessageId,
        int endMessageId);

    private native int nativeGetPremiumSmsPermission(long nativeProxy, String packageName);

    private native int nativeGetPremiumSmsPermissionForSubscriber(long nativeProxy, long subId, String packageName);

    private native void nativeSetPremiumSmsPermission(long nativeProxy, String packageName, int permission);

    private native void nativeSetPremiumSmsPermissionForSubscriber(long nativeProxy, long subId, String packageName, int permission);


    private native boolean nativeIsImsSmsSupported(long nativeProxy);

    private native boolean nativeIsImsSmsSupportedForSubscriber(long nativeProxy, long subId);

    private native String nativeGetImsSmsFormat(long nativeProxy);

    private native String nativeGetImsSmsFormatForSubscriber(long nativeProxy, long subId);

    private native void nativeSendStoredText(long nativeProxy, long subId, String callingPkg, Uri messageUri, String scAddress,
        PendingIntent sentIntent, PendingIntent deliveryIntent);

    private native void nativeSendStoredMultipartText(long nativeProxy, long subId, String callingPkg, Uri messageUri,
            String scAddress, List<PendingIntent> sentIntents,
            List<PendingIntent> deliveryIntents);

    private native int nativeGetSmsCapacityOnIccForSubscriber(long nativeProxy, long subId);

    @Override
    public void synthesizeMessages(String originatingAddress, String scAddress, List<String> messages, long timestampMillis) {
        nativeSynthesizeMessages(mNativeProxy, originatingAddress, scAddress, messages, timestampMillis);
    }

    @Override
    public List<SmsRawData> getAllMessagesFromIccEf(String callingPkg) {
        return nativeGetAllMessagesFromIccEf(mNativeProxy, callingPkg);
    }

    @Override
    public List<SmsRawData> getAllMessagesFromIccEfForSubscriber(long subId, String callingPkg) {
        return nativeGetAllMessagesFromIccEfForSubscriber(mNativeProxy, subId, callingPkg);
    }

    @Override
    public boolean updateMessageOnIccEf(String callingPkg, int messageIndex, int newStatus,
            byte[] pdu) {
        return nativeUpdateMessageOnIccEf(mNativeProxy, callingPkg, messageIndex, newStatus, pdu);
    }

    @Override
    public boolean updateMessageOnIccEfForSubscriber(long subId, String callingPkg,
             int messageIndex, int newStatus, byte[] pdu) {
        return nativeUpdateMessageOnIccEfForSubscriber(mNativeProxy, subId, callingPkg, messageIndex, newStatus, pdu);
    }

    @Override
    public boolean copyMessageToIccEf(String callingPkg, int status, byte[] pdu, byte[] smsc) {
        return nativeCopyMessageToIccEf(mNativeProxy, callingPkg, status, pdu, smsc);
    }

    @Override
    public boolean copyMessageToIccEfForSubscriber(long subId, String callingPkg, int status,
            byte[] pdu, byte[] smsc) {
        return nativeCopyMessageToIccEfForSubscriber(mNativeProxy, subId, callingPkg, status, pdu, smsc);
    }

    @Override
    public void sendData(String callingPkg, String destAddr, String scAddr, int destPort,
            byte[] data, PendingIntent sentIntent, PendingIntent deliveryIntent) {
        nativeSendData(mNativeProxy, callingPkg, destAddr, scAddr, destPort, data, sentIntent, deliveryIntent);
    }

    @Override
    public void sendDataForSubscriber(long subId, String callingPkg, String destAddr,
            String scAddr, int destPort, byte[] data, PendingIntent sentIntent,
            PendingIntent deliveryIntent) {
        nativeSendDataForSubscriber(mNativeProxy, subId, callingPkg, destAddr, scAddr, destPort, data, sentIntent, deliveryIntent);
    }

    @Override
    public void sendDataWithOrigPort(String callingPkg, String destAddr, String scAddr,
        int destPort, int origPort, byte[] data, PendingIntent sentIntent,
        PendingIntent deliveryIntent) {
        nativeSendDataWithOrigPort(mNativeProxy, callingPkg, destAddr, scAddr, destPort, origPort, data, sentIntent, deliveryIntent);
    }

    @Override
    public void sendDataWithOrigPortUsingSubscriber(long subId, String callingPkg, String destAddr,
        String scAddr,int destPort, int origPort, byte[] data,
        PendingIntent sentIntent, PendingIntent deliveryIntent) {
        nativeSendDataWithOrigPortUsingSubscriber(mNativeProxy, subId, callingPkg, destAddr, scAddr, destPort, origPort
            , data, sentIntent, deliveryIntent);
    }

    @Override
    public void sendText(String callingPkg, String destAddr, String scAddr, String text,
            PendingIntent sentIntent, PendingIntent deliveryIntent) {
        nativeSendText(mNativeProxy, callingPkg, destAddr, scAddr, text, sentIntent, deliveryIntent);
    }

    @Override
    public void sendTextForSubscriber(long subId, String callingPkg, String destAddr,
            String scAddr, String text, PendingIntent sentIntent,
            PendingIntent deliveryIntent) {
        nativeSendTextForSubscriber(mNativeProxy, subId, callingPkg, destAddr, scAddr, text, sentIntent, deliveryIntent);
    }

    @Override
    public void sendTextWithOptionsUsingSubscriber(long subId, String callingPkg, String destAddr,
            String scAddr, String text, PendingIntent sentIntent,
            PendingIntent deliveryIntent, int priority, boolean isExpectMore,
            int validityPeriod) {
        nativeSendTextWithOptionsUsingSubscriber(mNativeProxy, subId, callingPkg, destAddr, scAddr, text
            , sentIntent, deliveryIntent, priority, isExpectMore, validityPeriod);
    }

    @Override
    public void injectSmsPdu(byte[] pdu, String format, PendingIntent receivedIntent) {
        nativeInjectSmsPdu(mNativeProxy, pdu, format, receivedIntent);
    }

    @Override
    public void injectSmsPduForSubscriber(long subId, byte[] pdu, String format,
            PendingIntent receivedIntent) {
        nativeInjectSmsPduForSubscriber(mNativeProxy, subId, pdu, format, receivedIntent);
    }

    @Override
    public void updateSmsSendStatus(int messageRef, boolean success) {
        nativeUpdateSmsSendStatus(mNativeProxy, messageRef, success);
    }

    @Override
    public void sendMultipartText(String callingPkg, String destinationAddress, String scAddress,
            List<String> parts, List<PendingIntent> sentIntents,
            List<PendingIntent> deliveryIntents) {
        nativeSendMultipartText(mNativeProxy, callingPkg, destinationAddress, scAddress, parts, sentIntents, deliveryIntents);
    }

    @Override
    public void sendMultipartTextForSubscriber(long subId, String callingPkg,
            String destinationAddress, String scAddress,
            List<String> parts, List<PendingIntent> sentIntents,
            List<PendingIntent> deliveryIntents) {
        nativeSendMultipartTextForSubscriber(mNativeProxy, subId, callingPkg, destinationAddress, scAddress
            , parts, sentIntents, deliveryIntents);
    }

    @Override
    public void sendMultipartTextWithOptionsUsingSubscriber(long subId, String callingPkg,
            String destinationAddress, String scAddress, List<String> parts,
            List<PendingIntent> sentIntents, List<PendingIntent> deliveryIntents,
            int priority, boolean isExpectMore, int validityPeriod) {
        nativeSendMultipartTextWithOptionsUsingSubscriber(mNativeProxy, subId, callingPkg, destinationAddress
            , scAddress, parts, sentIntents, deliveryIntents, priority, isExpectMore, validityPeriod);
    }

    @Override
    public boolean enableCellBroadcast(int messageIdentifier) {
        return nativeEnableCellBroadcast(mNativeProxy, messageIdentifier);
    }

    @Override
    public boolean enableCellBroadcastForSubscriber(long subId, int messageIdentifier) {
        return nativeEnableCellBroadcastForSubscriber(mNativeProxy, subId, messageIdentifier);
    }

    @Override
    public boolean disableCellBroadcast(int messageIdentifier) {
        return nativeDisableCellBroadcast(mNativeProxy, messageIdentifier);
    }

    @Override
    public boolean disableCellBroadcastForSubscriber(long subId, int messageIdentifier) {
        return nativeDisableCellBroadcastForSubscriber(mNativeProxy, subId, messageIdentifier);
    }

    @Override
    public boolean enableCellBroadcastRange(int startMessageId, int endMessageId) {
        return nativeEnableCellBroadcastRange(mNativeProxy, startMessageId, endMessageId);
    }

    @Override
    public boolean enableCellBroadcastRangeForSubscriber(long subId, int startMessageId, int endMessageId) {
        return nativeEnableCellBroadcastRangeForSubscriber(mNativeProxy, subId, startMessageId, endMessageId);
    }

    @Override
    public boolean disableCellBroadcastRange(int startMessageId, int endMessageId) {
        return nativeDisableCellBroadcastRange(mNativeProxy, startMessageId, endMessageId);
    }

    @Override
    public boolean disableCellBroadcastRangeForSubscriber(long subId, int startMessageId, int endMessageId) {
        return nativeDisableCellBroadcastRangeForSubscriber(mNativeProxy, subId, startMessageId, endMessageId);
    }

    @Override
    public int getPremiumSmsPermission(String packageName) {
        return nativeGetPremiumSmsPermission(mNativeProxy, packageName);
    }

    @Override
    public int getPremiumSmsPermissionForSubscriber(long subId, String packageName) {
        return nativeGetPremiumSmsPermissionForSubscriber(mNativeProxy, subId, packageName);
    }

    @Override
    public void setPremiumSmsPermission(String packageName, int permission) {
        nativeSetPremiumSmsPermission(mNativeProxy, packageName, permission);
    }

    @Override
    public void setPremiumSmsPermissionForSubscriber(long subId, String packageName, int permission) {
        nativeSetPremiumSmsPermissionForSubscriber(mNativeProxy, subId, packageName, permission);
    }

    @Override
    public boolean isImsSmsSupported() {
        return nativeIsImsSmsSupported(mNativeProxy);
    }

    @Override
    public boolean isImsSmsSupportedForSubscriber(long subId) {
        return nativeIsImsSmsSupportedForSubscriber(mNativeProxy, subId);
    }

    @Override
    public String getImsSmsFormat() {
        return nativeGetImsSmsFormat(mNativeProxy);
    }

    @Override
    public String getImsSmsFormatForSubscriber(long subId) {
        return nativeGetImsSmsFormatForSubscriber(mNativeProxy, subId);
    }

    @Override
    public void sendStoredText(long subId, String callingPkg, Uri messageUri, String scAddress,
            PendingIntent sentIntent, PendingIntent deliveryIntent) {
        nativeSendStoredText(mNativeProxy, subId, callingPkg, messageUri, scAddress
            , sentIntent, deliveryIntent);
    }

    @Override
    public void sendStoredMultipartText(long subId, String callingPkg, Uri messageUri,
                String scAddress, List<PendingIntent> sentIntents,
                List<PendingIntent> deliveryIntents) {
        nativeSendStoredMultipartText(mNativeProxy, subId, callingPkg, messageUri
            , scAddress, sentIntents, deliveryIntents);
    }

    @Override
    public int getSmsCapacityOnIccForSubscriber(long subId) {
        return nativeGetSmsCapacityOnIccForSubscriber(mNativeProxy, subId);
    }
}
