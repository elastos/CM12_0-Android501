/* //device/java/android/android/app/IActivityPendingResult.aidl
**
** Copyright 2007, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

package android.os;

import android.os.Message;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/** @hide */
public class ElMessengerProxy extends IMessenger.Stub {
    private long mNativeProxy;

    private native void nativeSend(long proxy, Message msg);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElMessengerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void send(Message msg) throws RemoteException {
        nativeSend(mNativeProxy, msg);
    }
}
