
package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.storage.StorageVolume;
import android.util.Log;

/** {@hide} */
public class ElMountServiceProxy extends IMountService.Stub {
    private long mNativeProxy;

    private native StorageVolume[] nativeGetVolumeList(long nativeProxy);

    private native void nativeRegisterListener(long nativeProxy, IMountServiceListener listener);

    private native void nativeUnregisterListener(long nativeProxy, int listener);

    private native String nativeGetVolumeState(long nativeProxy, String mountPoint);

    private native boolean nativeIsUsbMassStorageConnected(long nativeProxy);

    private native void nativeSetUsbMassStorageEnabled(long nativeProxy, boolean enable);

    private native boolean nativeIsUsbMassStorageEnabled(long nativeProxy);

    private native int nativeMountVolume(long nativeProxy, String mountPoint);

    private native void nativeUnmountVolume(long nativeProxy, String mountPoint, boolean force, boolean removeEncryption);

    private native int nativeFormatVolume(long nativeProxy, String mountPoint);

    private native int[] nativeGetStorageUsers(long nativeProxy, String path);

    private native int nativeCreateSecureContainer(long nativeProxy, String id, int sizeMb, String fstype, String key,
            int ownerUid, boolean external);

    private native int nativeDestroySecureContainer(long nativeProxy, String id, boolean force);

    private native int nativeFinalizeSecureContainer(long nativeProxy, String id);

    private native int nativeMountSecureContainer(long nativeProxy, String id, String key, int ownerUid, boolean readOnly);

    private native int nativeUnmountSecureContainer(long nativeProxy, String id, boolean force);

    private native boolean nativeIsSecureContainerMounted(long nativeProxy, String id);

    private native int nativeRenameSecureContainer(long nativeProxy, String oldId, String newId);

    private native String nativeGetSecureContainerPath(long nativeProxy, String id);

    private native String[] nativeGetSecureContainerList(long nativeProxy);

    private native void nativeShutdown(long nativeProxy, IMountShutdownObserver observer);

    private native void nativeFinishMediaUpdate(long nativeProxy);

    private native void nativeMountObb(long nativeProxy, String rawPath, String canonicalPath, String key,
            IObbActionListener token, int nonce);

    private native void nativeUnmountObb(long nativeProxy, String rawPath, boolean force,
            IObbActionListener token, int nonce);

    private native boolean nativeIsObbMounted(long nativeProxy, String rawPath);

    private native String nativeGetMountedObbPath(long nativeProxy, String rawPath);

    private native boolean nativeIsExternalStorageEmulated(long nativeProxy);

    private native int nativeGetEncryptionState(long nativeProxy);

    private native int nativeDecryptStorage(long nativeProxy, String password);

    private native int nativeEncryptStorage(long nativeProxy, int type, String password);

    private native int nativeChangeEncryptionPassword(long nativeProxy, int type, String password);

    private native int nativeVerifyEncryptionPassword(long nativeProxy, String password);

    private native String nativeGetSecureContainerFilesystemPath(long nativeProxy, String id);

    private native int nativeFixPermissionsSecureContainer(long nativeProxy, String id, int gid, String filename);

    private native String nativeGetVolumeFSLabel(long nativeProxy, String mountPoint);

    private native String nativeGetVolumeUUID(long nativeProxy, String mountPoint);

    private native int nativeGetVolumedevnode(long nativeProxy, String mountPoint);

    private native int nativeGetVolumedevtype(long nativeProxy, String mountPoint);

    private native java.util.ArrayList<java.lang.String> nativeGetAllVolume(long nativeProxy);

    private native int nativeMkdirs(long nativeProxy, String callingPkg, String path);

    private native int nativeGetPasswordType(long nativeProxy);

    private native String nativeGetPassword(long nativeProxy);

    private native void nativeClearPassword(long nativeProxy);

    private native void nativeSetField(long nativeProxy, String field, String contents);

    private native String nativeGetField(long nativeProxy, String field);

    private native int nativeResizeSecureContainer(long nativeProxy, String id, int sizeMb, String key);

    private native long nativeLastMaintenance(long nativeProxy);

    private native void nativeRunMaintenance(long nativeProxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElMountServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    public StorageVolume[] getVolumeList() throws RemoteException {
        return nativeGetVolumeList(mNativeProxy);
    }

    public void registerListener(IMountServiceListener listener) throws RemoteException {
        nativeRegisterListener(mNativeProxy, listener);
    }

    public void unregisterListener(IMountServiceListener listener) throws RemoteException {
        nativeUnregisterListener(mNativeProxy, listener.hashCode());
    }

    public String getVolumeState(String mountPoint) throws RemoteException {
        return nativeGetVolumeState(mNativeProxy, mountPoint);
    }

    public boolean isUsbMassStorageConnected() throws RemoteException {
        return nativeIsUsbMassStorageConnected(mNativeProxy);
    }

    public void setUsbMassStorageEnabled(boolean enable) throws RemoteException {
        nativeSetUsbMassStorageEnabled(mNativeProxy, enable);
    }

    public boolean isUsbMassStorageEnabled() throws RemoteException {
        return nativeIsUsbMassStorageEnabled(mNativeProxy);
    }

    public int mountVolume(String mountPoint) throws RemoteException {
        return nativeMountVolume(mNativeProxy, mountPoint);
    }

    public void unmountVolume(String mountPoint, boolean force, boolean removeEncryption) throws RemoteException {
        nativeUnmountVolume(mNativeProxy, mountPoint, force, removeEncryption);
    }

    public int formatVolume(String mountPoint) throws RemoteException {
        return nativeFormatVolume(mNativeProxy, mountPoint);
    }

    public int[] getStorageUsers(String path) throws RemoteException {
        return nativeGetStorageUsers(mNativeProxy, path);
    }

    public int createSecureContainer(String id, int sizeMb, String fstype, String key,
            int ownerUid, boolean external) throws RemoteException {
        return nativeCreateSecureContainer(mNativeProxy, id, sizeMb, fstype, key, ownerUid, external);
    }

    public int destroySecureContainer(String id, boolean force) throws RemoteException {
        return nativeDestroySecureContainer(mNativeProxy, id, force);
    }

    public int finalizeSecureContainer(String id) throws RemoteException {
        return nativeFinalizeSecureContainer(mNativeProxy, id);
    }

    public int mountSecureContainer(String id, String key, int ownerUid, boolean readOnly) throws RemoteException {
        return nativeMountSecureContainer(mNativeProxy, id, key, ownerUid, readOnly);
    }

    public int unmountSecureContainer(String id, boolean force) throws RemoteException {
        return nativeUnmountSecureContainer(mNativeProxy, id, force);
    }

    public boolean isSecureContainerMounted(String id) throws RemoteException {
        return nativeIsSecureContainerMounted(mNativeProxy, id);
    }

    public int renameSecureContainer(String oldId, String newId) throws RemoteException {
        return nativeRenameSecureContainer(mNativeProxy, oldId, newId);
    }

    public String getSecureContainerPath(String id) throws RemoteException {
        return nativeGetSecureContainerPath(mNativeProxy, id);
    }

    public String[] getSecureContainerList() throws RemoteException {
        return nativeGetSecureContainerList(mNativeProxy);
    }

    public void shutdown(IMountShutdownObserver observer) throws RemoteException {
        nativeShutdown(mNativeProxy, observer);
    }

    public void finishMediaUpdate() throws RemoteException {
        nativeFinishMediaUpdate(mNativeProxy);
    }

    public void mountObb(String rawPath, String canonicalPath, String key,
            IObbActionListener token, int nonce) throws RemoteException {
        nativeMountObb(mNativeProxy, rawPath, canonicalPath, key, token, nonce);
    }

    public void unmountObb(String rawPath, boolean force, IObbActionListener token, int nonce) throws RemoteException {
        nativeUnmountObb(mNativeProxy, rawPath, force, token, nonce);
    }

    public boolean isObbMounted(String rawPath) throws RemoteException {
        return nativeIsObbMounted(mNativeProxy, rawPath);
    }

    public String getMountedObbPath(String rawPath) throws RemoteException {
        return nativeGetMountedObbPath(mNativeProxy, rawPath);
    }

    public boolean isExternalStorageEmulated() throws RemoteException {
        return nativeIsExternalStorageEmulated(mNativeProxy);
    }

    public int getEncryptionState() throws RemoteException {
        return nativeGetEncryptionState(mNativeProxy);
    }

    public int decryptStorage(String password) throws RemoteException {
        return nativeDecryptStorage(mNativeProxy, password);
    }

    public int encryptStorage(int type, String password) throws RemoteException {
        return nativeEncryptStorage(mNativeProxy, type, password);
    }

    public int changeEncryptionPassword(int type, String password) throws RemoteException {
        return nativeChangeEncryptionPassword(mNativeProxy, type, password);
    }

    @Override
    public int verifyEncryptionPassword(String password) throws RemoteException {
        return nativeVerifyEncryptionPassword(mNativeProxy, password);
    }

    public String getSecureContainerFilesystemPath(String id) throws RemoteException {
        return nativeGetSecureContainerFilesystemPath(mNativeProxy, id);
    }

    public int fixPermissionsSecureContainer(String id, int gid, String filename) throws RemoteException {
        return nativeFixPermissionsSecureContainer(mNativeProxy, id, gid, filename);
    }

    public int mkdirs(String callingPkg, String path) throws RemoteException {
        return nativeMkdirs(mNativeProxy, callingPkg, path);
    }

    public int getPasswordType() throws RemoteException {
        return nativeGetPasswordType(mNativeProxy);
    }

    public String getPassword() throws RemoteException {
        return nativeGetPassword(mNativeProxy);
    }

    public void clearPassword() throws RemoteException {
        nativeClearPassword(mNativeProxy);
    }

    public void setField(String field, String contents) throws RemoteException {
        nativeSetField(mNativeProxy, field, contents);
    }

    public String getField(String field) throws RemoteException {
        return nativeGetField(mNativeProxy, field);
    }

    public int resizeSecureContainer(String id, int sizeMb, String key) throws RemoteException {
        return nativeResizeSecureContainer(mNativeProxy, id, sizeMb, key);
    }

    public long lastMaintenance() throws RemoteException {
        return nativeLastMaintenance(mNativeProxy);
    }

    public void runMaintenance() throws RemoteException {
        nativeRunMaintenance(mNativeProxy);
    }
}
