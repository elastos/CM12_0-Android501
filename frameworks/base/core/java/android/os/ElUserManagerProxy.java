
package android.os;

import android.os.Bundle;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.content.pm.UserInfo;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.List;

/**
 *  {@hide}
 */
 public class ElUserManagerProxy extends IUserManager.Stub {
    private long mNativeProxy;

    private native int nativeGetUserSerialNumber(long nativeProxy, int userHandle);

    private native UserInfo nativeCreateUser(long nativeProxy, String name, int flags);

    private native UserInfo nativeCreateProfileForUser(long nativeProxy, String name, int flags, int userHandle);

    private native void nativeSetUserEnabled(long nativeProxy, int userHandle);

    private native boolean nativeRemoveUser(long nativeProxy, int userHandle);

    private native void nativeSetUserName(long nativeProxy, int userHandle, String name);

    private native void nativeSetUserIcon(long nativeProxy, int userHandle, Bitmap icon);

    private native Bitmap nativeGetUserIcon(long nativeProxy, int userHandle);

    private native List<UserInfo> nativeGetUsers(long nativeProxy, boolean excludeDying);

    private native List<UserInfo> nativeGetProfiles(long nativeProxy, int userHandle, boolean enabledOnly);

    private native UserInfo nativeGetProfileParent(long nativeProxy, int userHandle);

    private native UserInfo nativeGetUserInfo(long nativeProxy, int userHandle);

    private native boolean nativeIsRestricted(long nativeProxy);

    private native int nativeGetUserHandle(long nativeProxy, int userSerialNumber);

    private native Bundle nativeGetUserRestrictions(long nativeProxy, int userHandle);

    private native boolean nativeHasUserRestriction(long nativeProxy, String restrictionKey, int userHandle);

    private native void nativeSetUserRestrictions(long nativeProxy, Bundle restrictions, int userHandle);

    private native void nativeSetApplicationRestrictions(long nativeProxy, String packageName, Bundle restrictions, int userHandle);

    private native Bundle nativeGetApplicationRestrictions(long nativeProxy, String packageName);

    private native Bundle nativeGetApplicationRestrictionsForUser(long nativeProxy, String packageName, int userHandle);

    private native boolean nativeSetRestrictionsChallenge(long nativeProxy, String newPin);

    private native int nativeCheckRestrictionsChallenge(long nativeProxy, String pin);

    private native boolean nativeHasRestrictionsChallenge(long nativeProxy);

    private native void nativeRemoveRestrictions(long nativeProxy);

    private native void nativeSetDefaultGuestRestrictions(long nativeProxy, Bundle restrictions);

    private native Bundle nativeGetDefaultGuestRestrictions(long nativeProxy);

    private native boolean nativeMarkGuestForDeletion(long nativeProxy, int userHandle);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElUserManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public int getUserSerialNumber(int userHandle) throws RemoteException {
        return nativeGetUserSerialNumber(mNativeProxy, userHandle);
    }

    @Override
    public UserInfo createUser(String name, int flags) throws RemoteException {
        return nativeCreateUser(mNativeProxy, name, flags);
    }

    @Override
    public UserInfo createProfileForUser(String name, int flags, int userHandle) throws RemoteException {
        return nativeCreateProfileForUser(mNativeProxy, name, flags, userHandle);
    }

    @Override
    public void setUserEnabled(int userHandle) throws RemoteException {
        nativeSetUserEnabled(mNativeProxy, userHandle);
    }

    @Override
    public boolean removeUser(int userHandle) throws RemoteException {
        return nativeRemoveUser(mNativeProxy, userHandle);
    }

    @Override
    public void setUserName(int userHandle, String name) throws RemoteException {
        nativeSetUserName(mNativeProxy, userHandle, name);
    }

    @Override
    public void setUserIcon(int userHandle, Bitmap icon) throws RemoteException {
        nativeSetUserIcon(mNativeProxy, userHandle, icon);
    }

    @Override
    public Bitmap getUserIcon(int userHandle) throws RemoteException {
        return nativeGetUserIcon(mNativeProxy, userHandle);
    }

    @Override
    public List<UserInfo> getUsers(boolean excludeDying) throws RemoteException {
        return nativeGetUsers(mNativeProxy, excludeDying);
    }

    @Override
    public List<UserInfo> getProfiles(int userHandle, boolean enabledOnly) throws RemoteException {
        return nativeGetProfiles(mNativeProxy, userHandle, enabledOnly);
    }

    @Override
    public UserInfo getProfileParent(int userHandle) throws RemoteException {
        return nativeGetProfileParent(mNativeProxy, userHandle);
    }

    @Override
    public UserInfo getUserInfo(int userHandle) throws RemoteException {
        return nativeGetUserInfo(mNativeProxy, userHandle);
    }

    @Override
    public boolean isRestricted() throws RemoteException {
        return nativeIsRestricted(mNativeProxy);
    }

    @Override
    public int getUserHandle(int userSerialNumber) throws RemoteException {
        return nativeGetUserHandle(mNativeProxy, userSerialNumber);
    }

    @Override
    public Bundle getUserRestrictions(int userHandle) throws RemoteException {
        return nativeGetUserRestrictions(mNativeProxy, userHandle);
    }

    @Override
    public boolean hasUserRestriction(String restrictionKey, int userHandle) throws RemoteException {
        return nativeHasUserRestriction(mNativeProxy, restrictionKey, userHandle);
    }

    @Override
    public void setUserRestrictions(Bundle restrictions, int userHandle) throws RemoteException {
        nativeSetUserRestrictions(mNativeProxy, restrictions, userHandle);
    }

    @Override
    public void setApplicationRestrictions(String packageName, Bundle restrictions, int userHandle) throws RemoteException {
        nativeSetApplicationRestrictions(mNativeProxy, packageName, restrictions, userHandle);
    }

    @Override
    public Bundle getApplicationRestrictions(String packageName) throws RemoteException {
        return nativeGetApplicationRestrictions(mNativeProxy, packageName);
    }

    @Override
    public Bundle getApplicationRestrictionsForUser(String packageName, int userHandle) throws RemoteException {
        return nativeGetApplicationRestrictionsForUser(mNativeProxy, packageName, userHandle);
    }

    @Override
    public boolean setRestrictionsChallenge(String newPin) throws RemoteException {
        return nativeSetRestrictionsChallenge(mNativeProxy, newPin);
    }

    @Override
    public int checkRestrictionsChallenge(String pin) throws RemoteException {
        return nativeCheckRestrictionsChallenge(mNativeProxy, pin);
    }

    @Override
    public boolean hasRestrictionsChallenge() throws RemoteException {
        return nativeHasRestrictionsChallenge(mNativeProxy);
    }

    @Override
    public void removeRestrictions() throws RemoteException {
        nativeRemoveRestrictions(mNativeProxy);
    }

    @Override
    public void setDefaultGuestRestrictions(Bundle restrictions) throws RemoteException {
        nativeSetDefaultGuestRestrictions(mNativeProxy, restrictions);
    }

    @Override
    public Bundle getDefaultGuestRestrictions() throws RemoteException {
        return nativeGetDefaultGuestRestrictions(mNativeProxy);
    }

    @Override
    public boolean markGuestForDeletion(int userHandle) throws RemoteException {
        return nativeMarkGuestForDeletion(mNativeProxy, userHandle);
    }
}
