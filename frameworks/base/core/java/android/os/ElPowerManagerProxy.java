
package android.os;

import android.util.Log;
import android.os.WorkSource;
import android.os.RemoteException;
import android.os.IBinder;

/** @hide */
public class ElPowerManagerProxy extends IPowerManager.Stub {
    private long mNativeProxy;

    private native void nativeAcquireWakeLock(long nativeProxy, IBinder lock, int flags, String tag, String packageName, WorkSource ws, String historyTag);

    private native void nativeAcquireWakeLockWithUid(long nativeProxy, IBinder lock, int flags, String tag, String packageName, int uidtoblame);

    private native void nativeUpdateWakeLockWorkSource(long nativeProxy, int lock, WorkSource ws, String historyTag);

    private native void nativeReleaseWakeLock(long nativeProxy, int lock, int flags);

    private native void nativeUpdateWakeLockUids(long nativeProxy, int lock, int[] uids);

    private native void nativePowerHint(long nativeProxy, int hintId, int data);

    private native boolean nativeIsWakeLockLevelSupported(long nativeProxy, int level);

    private native void nativeUserActivity(long nativeProxy, long time, int event, int flags);

    private native void nativeWakeUp(long nativeProxy, long time);

    private native void nativeGoToSleep(long nativeProxy, long time, int reason, int flags);

    private native void nativeNap(long nativeProxy, long time);

    private native boolean nativeIsInteractive(long nativeProxy);

    private native boolean nativeIsPowerSaveMode(long nativeProxy);

    private native boolean nativeSetPowerSaveMode(long nativeProxy, boolean mode);

    private native void nativeReboot(long nativeProxy, boolean confirm, String reason, boolean wait);

    private native void nativeShutdown(long nativeProxy, boolean confirm, boolean wait);

    private native void nativeCrash(long nativeProxy, String message);

    private native void nativeSetStayOnSetting(long nativeProxy, int val);

    private native void nativeSetMaximumScreenOffTimeoutFromDeviceAdmin(long nativeProxy, int timeMs);

    private native void nativeSetTemporaryScreenBrightnessSettingOverride(long nativeProxy, int brightness);

    private native void nativeSetTemporaryScreenAutoBrightnessAdjustmentSettingOverride(long nativeProxy, float adj);

    private native void nativeSetAttentionLight(long nativeProxy, boolean on, int color);

    private native void nativeUpdateBlockedUids(long nativeProxy, int uid, boolean isBlocked);

    private native void nativeCpuBoost(long nativeProxy, int duration);

    private native void nativeSetKeyboardVisibility(long nativeProxy, boolean visible);

    private native void nativeSetKeyboardLight(long nativeProxy, boolean on, int key);

    private native void nativeWakeUpWithProximityCheck(long nativeProxy, long time);

    private native boolean nativeSetPowerProfile(long nativeProxy, String profile);

    private native String nativeGetPowerProfile(long nativeProxy);

    private native void nativeActivityResumed(long nativeProxy, String componentName);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElPowerManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void acquireWakeLock(IBinder lock, int flags, String tag, String packageName, WorkSource ws, String historyTag) throws RemoteException {
        nativeAcquireWakeLock(mNativeProxy, lock, flags, tag, packageName, ws, historyTag);
    }

    @Override
    public void acquireWakeLockWithUid(IBinder lock, int flags, String tag, String packageName, int uidtoblame) throws RemoteException {
        nativeAcquireWakeLockWithUid(mNativeProxy, lock, flags, tag, packageName, uidtoblame);
    }

    @Override
    public void updateWakeLockWorkSource(IBinder lock, WorkSource ws, String historyTag) throws RemoteException {
        nativeUpdateWakeLockWorkSource(mNativeProxy, lock.hashCode(), ws, historyTag);
    }

    @Override
    public void releaseWakeLock(IBinder lock, int flags) throws RemoteException {
        nativeReleaseWakeLock(mNativeProxy, lock.hashCode(), flags);
    }

    @Override
    public void updateWakeLockUids(IBinder lock, int[] uids) throws RemoteException {
        nativeUpdateWakeLockUids(mNativeProxy, lock.hashCode(), uids);
    }

    @Override
    public void powerHint(int hintId, int data) throws RemoteException {
        nativePowerHint(mNativeProxy, hintId, data);
    }

    @Override
    public boolean isWakeLockLevelSupported(int level) throws RemoteException {
        return nativeIsWakeLockLevelSupported(mNativeProxy, level);
    }

    @Override
    public void userActivity(long time, int event, int flags) throws RemoteException {
        nativeUserActivity(mNativeProxy, time, event, flags);
    }

    @Override
    public void wakeUp(long time) throws RemoteException {
        nativeWakeUp(mNativeProxy, time);
    }

    @Override
    public void goToSleep(long time, int reason, int flags) throws RemoteException {
        nativeGoToSleep(mNativeProxy, time, reason, flags);
    }

    @Override
    public void nap(long time) throws RemoteException {
        nativeNap(mNativeProxy, time);
    }

    @Override
    public boolean isInteractive() throws RemoteException {
        return nativeIsInteractive(mNativeProxy);
    }

    @Override
    public boolean isPowerSaveMode() throws RemoteException {
        return nativeIsPowerSaveMode(mNativeProxy);
    }

    @Override
    public boolean setPowerSaveMode(boolean mode) throws RemoteException {
        return nativeSetPowerSaveMode(mNativeProxy, mode);
    }

    @Override
    public void reboot(boolean confirm, String reason, boolean wait) throws RemoteException {
        nativeReboot(mNativeProxy, confirm, reason, wait);
    }

    @Override
    public void shutdown(boolean confirm, boolean wait) throws RemoteException {
        nativeShutdown(mNativeProxy, confirm, wait);
    }

    @Override
    public void crash(String message) throws RemoteException {
        nativeCrash(mNativeProxy, message);
    }

    @Override
    public void setStayOnSetting(int val) throws RemoteException {
        nativeSetStayOnSetting(mNativeProxy, val);
    }

    @Override
    public void setMaximumScreenOffTimeoutFromDeviceAdmin(int timeMs) throws RemoteException {
        nativeSetMaximumScreenOffTimeoutFromDeviceAdmin(mNativeProxy, timeMs);
    }

    @Override
    public void setTemporaryScreenBrightnessSettingOverride(int brightness) throws RemoteException {
        nativeSetTemporaryScreenBrightnessSettingOverride(mNativeProxy, brightness);
    }

    @Override
    public void setTemporaryScreenAutoBrightnessAdjustmentSettingOverride(float adj) throws RemoteException {
        nativeSetTemporaryScreenAutoBrightnessAdjustmentSettingOverride(mNativeProxy, adj);
    }

    @Override
    public void setAttentionLight(boolean on, int color) throws RemoteException {
        nativeSetAttentionLight(mNativeProxy, on, color);
    }

    @Override
    public void updateBlockedUids(int uid, boolean isBlocked) throws RemoteException {
        nativeUpdateBlockedUids(mNativeProxy, uid, isBlocked);
    }

    @Override
    public void cpuBoost(int duration) throws RemoteException {
        nativeCpuBoost(mNativeProxy, duration);
    }

    @Override
    public void setKeyboardVisibility(boolean visible) throws RemoteException {
        nativeSetKeyboardVisibility(mNativeProxy, visible);
    }

    @Override
    public void setKeyboardLight(boolean on, int key) throws RemoteException {
        nativeSetKeyboardLight(mNativeProxy, on, key);
    }

    @Override
    public void wakeUpWithProximityCheck(long time) throws RemoteException {
        nativeWakeUpWithProximityCheck(mNativeProxy, time);
    }

    @Override
    public boolean setPowerProfile(String profile) throws RemoteException {
        return nativeSetPowerProfile(mNativeProxy, profile);
    }

    @Override
    public String getPowerProfile() throws RemoteException {
        return nativeGetPowerProfile(mNativeProxy);
    }

    @Override
    public void activityResumed(String componentName) throws RemoteException {
        nativeActivityResumed(mNativeProxy, componentName);
    }
}
