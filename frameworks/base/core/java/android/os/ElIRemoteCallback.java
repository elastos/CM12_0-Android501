
package android.os;

import android.os.Message;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/** @hide */
public class ElIRemoteCallback extends IRemoteCallback.Stub {
    private long mNativeProxy;

    private native void nativesendResult(long nativeProxy, Bundle data);

    public ElIRemoteCallback(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void sendResult(Bundle data) {
        nativesendResult(mNativeProxy, data);
    }
}
