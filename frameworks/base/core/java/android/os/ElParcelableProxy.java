
package android.os;

import android.app.ActivityManager;
import android.app.IActivityManager;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ParceledListSlice;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

/**
 * {@hide}
 */
public class ElParcelableProxy {
    private static final String TAG = "ElParcelableProxy";

    private static List<RunningAppInfo> mRunningApps;

    public static String getCurrentApkPath() {
        return getApkPath(Process.myPid());
    }

    public static String getApkPath(int pid) {
        if (mRunningApps == null) {
            mRunningApps = queryAllRunningAppInfo();
        }

        for (RunningAppInfo app : mRunningApps) {
            if (pid == app.pid) {
                Log.d("getApkPath", "Found apk: " + app.sourceDir + " for pid:" + pid + "; package = " + app.packageName);
                return app.sourceDir;
            }
        }

        Log.e(TAG, "No apk found for pid:" + pid);
        return null;
    }

    private static List<RunningAppInfo> queryAllRunningAppInfo() {
        Log.d(TAG, "queryAllRunningAppInfo()");
        List<RunningAppInfo> runningAppInfos = new ArrayList<RunningAppInfo>();
        HashMap<String, String> appSourceDirs = new HashMap<String, String>();
        List<ApplicationInfo> listAppcations = getInstalledApps();

        for (ApplicationInfo app : listAppcations) {
            appSourceDirs.put(app.packageName, app.sourceDir);
        }

        IActivityManager mActivityManager = (IActivityManager) ServiceManager.getService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcessList = null;

        try {
            appProcessList = mActivityManager.getRunningAppProcesses();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        if (appProcessList != null) {
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcessList) {
                String[] pkgNameList = appProcess.pkgList;

                for (int i = 0; i < pkgNameList.length; i++) {
                    String pkgName = pkgNameList[i];
                    RunningAppInfo appInfo = new RunningAppInfo();
                    appInfo.packageName = pkgName;
                    appInfo.sourceDir = appSourceDirs.get(pkgName);
                    appInfo.pid = appProcess.pid;
                    appInfo.processName = appProcess.processName;
                    runningAppInfos.add(appInfo);
                }
            }
        }

        return runningAppInfos;
    }

    @SuppressWarnings("unchecked")
    private static List<ApplicationInfo> getInstalledApps() {
        List<ApplicationInfo> listApps = new ArrayList<ApplicationInfo>();

        try {
            ApplicationInfo lastItem = null;
            ParceledListSlice<ApplicationInfo> slice;
            int userId = UserHandle.getUserId(Process.myUid());
            IPackageManager pm = (IPackageManager) ServiceManager.getService("package");

            slice = pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES, userId);
            if (slice != null) {
                listApps = slice.getList();
            }
            else {
                Log.e(TAG, "slice is null");
            }

        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }

        return listApps;
    }

    static class RunningAppInfo {
        private String processName;
        private int pid;
        private String packageName;
        private String sourceDir;
    }
}
