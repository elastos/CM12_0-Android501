
package android.accounts;

import android.accounts.IAccountManager;
import android.accounts.IAccountManagerResponse;
import android.accounts.Account;
import android.accounts.AuthenticatorDescription;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * @hide
 */
public class ElAccountManagerProxy extends IAccountManager.Stub {
    private long mNativeProxy;

    private native Account[] nativeGetAccountsAsUser(long nativeProxy, String accountType, int userId);

    private native boolean nativeAddAccountExplicitly(long nativeProxy, Account account, String password, Bundle extras);

    private native void nativeAddAccount(long nativeProxy, IAccountManagerResponse response, String accountType,
        String authTokenType, String[] requiredFeatures, boolean expectActivityLaunch, Bundle options);

    private native void nativeAddAccountAsUser(long nativeProxy, IAccountManagerResponse response, String accountType,
        String authTokenType, String[] requiredFeatures, boolean expectActivityLaunch, Bundle options, int userId);

    private native void nativeSetUserData(long nativeProxy, Account account, String key, String value);

    private native String nativeGetPassword(long nativeProxy, Account account);

    private native String nativeGetUserData(long nativeProxy, Account account, String key);

    private native void nativeSetPassword(long nativeProxy, Account account, String password);

    private native void nativeClearPassword(long nativeProxy, Account account);

    private native AuthenticatorDescription[] nativeGetAuthenticatorTypes(long nativeProxy, int userId);

    private native Account[] nativeGetAccounts(long nativeProxy, String accountType);

    private native Account[] nativeGetAccountsForPackage(long nativeProxy, String packageName, int uid);

    private native Account[] nativeGetAccountsByTypeForPackage(long nativeProxy, String type, String packageName);

    private native void nativeHasFeatures(long nativeProxy, IAccountManagerResponse response, Account account, String[] features);

    private native void nativeGetAccountsByFeatures(long nativeProxy, IAccountManagerResponse response, String accountType, String[] features);

    private native void nativeRemoveAccount(long nativeProxy, IAccountManagerResponse response, Account account);

    private native void nativeRemoveAccountAsUser(long nativeProxy, IAccountManagerResponse response, Account account, int userId);

    private native void nativeInvalidateAuthToken(long nativeProxy, String accountType, String authToken);

    private native String nativePeekAuthToken(long nativeProxy, Account account, String authTokenType);

    private native void nativeSetAuthToken(long nativeProxy, Account account, String authTokenType, String authToken);

    private native void nativeUpdateAppPermission(long nativeProxy, Account account, String authTokenType, int uid, boolean value);

    private native void nativeGetAuthToken(long nativeProxy, IAccountManagerResponse response, Account account,
        String authTokenType, boolean notifyOnAuthFailure, boolean expectActivityLaunch, Bundle options);

    private native void nativeUpdateCredentials(long nativeProxy, IAccountManagerResponse response, Account account,
        String authTokenType, boolean expectActivityLaunch, Bundle options);

    private native void nativeEditProperties(long nativeProxy, IAccountManagerResponse response, String accountType,
        boolean expectActivityLaunch);

    private native void nativeConfirmCredentialsAsUser(long nativeProxy, IAccountManagerResponse response, Account account,
        Bundle options, boolean expectActivityLaunch, int userId);

    private native void nativeGetAuthTokenLabel(long nativeProxy, IAccountManagerResponse response, String accountType,
        String authTokenType);

    public ElAccountManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public Account[] getAccountsAsUser(String accountType, int userId) throws RemoteException {
        return nativeGetAccountsAsUser(mNativeProxy, accountType, userId);
    }

    @Override
    public boolean addAccountExplicitly(Account account, String password, Bundle extras) throws RemoteException {
        return nativeAddAccountExplicitly(mNativeProxy, account, password, extras);
    }

    @Override
    public void addAccount(IAccountManagerResponse response, String accountType, String authTokenType,
            String[] requiredFeatures, boolean expectActivityLaunch, Bundle options) throws RemoteException {
        nativeAddAccount(mNativeProxy, response, accountType, authTokenType, requiredFeatures, expectActivityLaunch, options);
    }

    @Override
    public void addAccountAsUser(IAccountManagerResponse response, String accountType, String authTokenType,
            String[] requiredFeatures, boolean expectActivityLaunch, Bundle options, int userId) throws RemoteException {
        nativeAddAccountAsUser(mNativeProxy, response, accountType, authTokenType, requiredFeatures, expectActivityLaunch, options, userId);
    }

    @Override
    public void setUserData(Account account, String key, String value) throws RemoteException {
        nativeSetUserData(mNativeProxy, account, key, value);
    }

    @Override
    public String getPassword(Account account) throws RemoteException {
        return nativeGetPassword(mNativeProxy, account);
    }

    @Override
    public String getUserData(Account account, String key) throws RemoteException {
        return nativeGetUserData(mNativeProxy, account, key);
    }

    @Override
    public void setPassword(Account account, String password) throws RemoteException {
        nativeSetPassword(mNativeProxy, account, password);
    }

    @Override
    public void clearPassword(Account account) throws RemoteException {
        nativeClearPassword(mNativeProxy, account);
    }

    @Override
    public AuthenticatorDescription[] getAuthenticatorTypes(int userId) throws RemoteException {
        return nativeGetAuthenticatorTypes(mNativeProxy, userId);
    }

    @Override
    public Account[] getAccounts(String accountType) throws RemoteException {
        return nativeGetAccounts(mNativeProxy, accountType);
    }

    @Override
    public Account[] getAccountsForPackage(String packageName, int uid) throws RemoteException {
        return nativeGetAccountsForPackage(mNativeProxy, packageName, uid);
    }

    @Override
    public Account[] getAccountsByTypeForPackage(String type, String packageName) throws RemoteException {
        return nativeGetAccountsByTypeForPackage(mNativeProxy, type, packageName);
    }

    @Override
    public void hasFeatures(IAccountManagerResponse response, Account account, String[] features) throws RemoteException {
        nativeHasFeatures(mNativeProxy, response, account, features);
    }

    @Override
    public void getAccountsByFeatures(IAccountManagerResponse response, String accountType, String[] features) throws RemoteException {
        nativeGetAccountsByFeatures(mNativeProxy, response, accountType, features);
    }

    @Override
    public void removeAccount(IAccountManagerResponse response, Account account) throws RemoteException {
        nativeRemoveAccount(mNativeProxy, response, account);
    }

    @Override
    public void removeAccountAsUser(IAccountManagerResponse response, Account account, int userId) throws RemoteException {
        nativeRemoveAccountAsUser(mNativeProxy, response, account, userId);
    }

    @Override
    public void invalidateAuthToken(String accountType, String authToken) throws RemoteException {
        nativeInvalidateAuthToken(mNativeProxy, accountType, authToken);
    }

    @Override
    public String peekAuthToken(Account account, String authTokenType) throws RemoteException {
        return nativePeekAuthToken(mNativeProxy, account, authTokenType);
    }

    @Override
    public void setAuthToken(Account account, String authTokenType, String authToken) throws RemoteException {
        nativeSetAuthToken(mNativeProxy, account, authTokenType, authToken);
    }

    @Override
    public void updateAppPermission(Account account, String authTokenType, int uid, boolean value) throws RemoteException {
        nativeUpdateAppPermission(mNativeProxy, account, authTokenType, uid, value);
    }

    @Override
    public void getAuthToken(IAccountManagerResponse response, Account account,
        String authTokenType, boolean notifyOnAuthFailure, boolean expectActivityLaunch,
        Bundle options) throws RemoteException {
        nativeGetAuthToken(mNativeProxy, response, account, authTokenType, notifyOnAuthFailure, expectActivityLaunch, options);
    }

    @Override
    public void updateCredentials(IAccountManagerResponse response, Account account,
        String authTokenType, boolean expectActivityLaunch, Bundle options) throws RemoteException {
        nativeUpdateCredentials(mNativeProxy, response, account, authTokenType, expectActivityLaunch, options);
    }

    @Override
    public void editProperties(IAccountManagerResponse response, String accountType,
        boolean expectActivityLaunch) throws RemoteException {
        nativeEditProperties(mNativeProxy, response, accountType, expectActivityLaunch);
    }

    @Override
    public void confirmCredentialsAsUser(IAccountManagerResponse response, Account account,
        Bundle options, boolean expectActivityLaunch, int userId) throws RemoteException {
        nativeConfirmCredentialsAsUser(mNativeProxy, response, account, options, expectActivityLaunch, userId);
    }

    @Override
    public void getAuthTokenLabel(IAccountManagerResponse response, String accountType,
        String authTokenType) throws RemoteException {
        nativeGetAuthTokenLabel(mNativeProxy, response, accountType, authTokenType);
    }

    @Override
    public boolean addSharedAccountAsUser(Account account, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public Account[] getSharedAccountsAsUser(int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public boolean removeSharedAccountAsUser(Account account, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void renameAccount(IAccountManagerResponse response, Account accountToRename, String newName) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public String getPreviousName(Account account) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public boolean renameSharedAccountAsUser(Account accountToRename, String newName, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

}
