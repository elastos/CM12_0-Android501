
package android.database;

import android.net.Uri;
import android.os.Handler;
import android.util.Log;

/**
 * {@hide}
 */
public class ElContentObserverProxy extends ContentObserver {
    public static final String TAG = "ElContentObserverProxy";

    private long mNativeProxy;

    private native void naitveOnChange(long proxy, boolean selfChange);

    private native void nativeOnChangeEx(long proxy, boolean selfChange, Uri uri);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElContentObserverProxy(long nativeProxy) {
        super(null);
        mNativeProxy = nativeProxy;
    }

    @Override
    public IContentObserver getContentObserver() {
        Log.e(TAG, "Call getContentObserver()");
        return null;
    }

    @Override
    public IContentObserver releaseContentObserver() {
        Log.e(TAG, "Call releaseContentObserver()");
        return null;
    }

    @Override
    public boolean deliverSelfNotifications() {
        Log.e(TAG, "Call deliverSelfNotifications()");
        return false;
    }

    @Override
    public void onChange(boolean selfChange) {
        naitveOnChange(mNativeProxy, selfChange);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        nativeOnChangeEx(mNativeProxy, selfChange, uri);
    }
}
