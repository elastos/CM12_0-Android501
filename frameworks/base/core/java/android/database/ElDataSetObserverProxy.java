
package android.database;

/**
 * {@hide}
 */
public class ElDataSetObserverProxy extends DataSetObserver {
    public static final String TAG = "ElDataSetObserverProxy";

    private long mNativeProxy;

    private native void naitveOnChanged(long proxy);

    private native void nativeOnInvalidated(long proxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElDataSetObserverProxy(long nativeProxy) {
        super();
        mNativeProxy = nativeProxy;
    }

    public void onChanged() {
        naitveOnChanged(mNativeProxy);
    }

    public void onInvalidated() {
        nativeOnInvalidated(mNativeProxy);
    }
}
