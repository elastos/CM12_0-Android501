
package android.database;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;

import android.util.Log;

import java.io.Closeable;

/** @hide */
public class ElCursorProxy implements Cursor {

    public static final String TAG = "ElCursorProxy";

    private long mNativeProxy;

    private native int nativeGetCount(long proxy);

    private native int nativeGetColumnIndexOrThrow(long proxy, String columnName);

    private native int nativeGetColumnIndex(long proxy, String columnName);

    private native boolean nativeMoveToPosition(long proxy, int position);

    private native long nativeGetLong(long proxy, int columnIndex);

    private native boolean nativeIsClosed(long proxy);

    private native void nativeClose(long proxy);

    private native boolean nativeMove(long proxy, int offset);

    private native int nativeGetPosition(long proxy);

    private native boolean nativeMoveToFirst(long proxy);

    private native boolean nativeMoveToLast(long proxy);

    private native boolean nativeMoveToNext(long proxy);

    private native boolean nativeMoveToPrevious(long proxy);

    private native boolean nativeIsFirst(long proxy);

    private native boolean nativeIsLast(long proxy);

    private native boolean nativeIsBeforeFirst(long proxy);

    private native boolean nativeIsAfterLast(long proxy);

    private native String nativeGetColumnName(long proxy, int columnIndex);

    private native String[] nativeGetColumnNames(long proxy);

    private native int nativeGetColumnCount(long proxy);

    private native byte[] nativeGetBlob(long proxy, int columnIndex);

    private native String nativeGetString(long proxy, int columnIndex);

    private native int nativeGetInt(long proxy, int columnIndex);

    private native int nativeGetType(long proxy, int columnIndex);

    private native void nativeRegisterContentObserver(long proxy, ContentObserver observer);

    private native void nativeUnregisterContentObserver(long proxy, ContentObserver observer);

    private native boolean nativeRequery(long proxy);

    private native boolean nativeIsNull(long proxy, int columnIndex);

    private native void nativeCopyStringToBuffer(long proxy, int columnIndex, CharArrayBuffer buffer);

    private native short nativeGetShort(long proxy, int columnIndex);

    private native float nativeGetFloat(long proxy, int columnIndex);

    private native double nativeGetDouble(long proxy, int columnIndex);

    private native void nativeDeactivate(long proxy);

    private native void nativeRegisterDataSetObserver(long proxy, DataSetObserver observer);

    private native void nativeUnregisterDataSetObserver(long proxy, DataSetObserver observer);

    private native void nativeSetNotificationUri(long proxy, ContentResolver cr, Uri uri);

    private native Uri nativeGetNotificationUri(long proxy);

    private native boolean nativeGetWantsAllOnMoveCalls(long proxy);

    private native Bundle nativeGetExtras(long proxy);

    private native Bundle nativeRespond(long proxy, Bundle extras);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElCursorProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public int getCount() {
        return nativeGetCount(mNativeProxy);
    }

    @Override
    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
        return nativeGetColumnIndexOrThrow(mNativeProxy, columnName);
    }

    @Override
    public int getColumnIndex(String columnName) {
        return nativeGetColumnIndex(mNativeProxy, columnName);
    }

    @Override
    public boolean moveToPosition(int position) {
        return nativeMoveToPosition(mNativeProxy, position);
    }

    @Override
    public long getLong(int columnIndex) {
        return nativeGetLong(mNativeProxy, columnIndex);
    }

    @Override
    public boolean isClosed() {
        return nativeIsClosed(mNativeProxy);
    }

    @Override
    public void close() {
        nativeClose(mNativeProxy);
    }

    @Override
    public boolean move(int offset) {
        return nativeMove(mNativeProxy, offset);
    }

    @Override
    public int getPosition() {
        return nativeGetPosition(mNativeProxy);
    }

    @Override
    public boolean moveToFirst() {
        return nativeMoveToFirst(mNativeProxy);
    }

    @Override
    public boolean moveToLast() {
        return nativeMoveToLast(mNativeProxy);
    }

    @Override
    public boolean moveToNext() {
        return nativeMoveToNext(mNativeProxy);
    }

    @Override
    public boolean moveToPrevious() {
        return nativeMoveToPrevious(mNativeProxy);
    }

    @Override
    public boolean isFirst() {
        return nativeIsFirst(mNativeProxy);
    }

    @Override
    public boolean isLast() {
        return nativeIsLast(mNativeProxy);
    }

    @Override
    public boolean isBeforeFirst() {
        return nativeIsBeforeFirst(mNativeProxy);
    }

    @Override
    public boolean isAfterLast() {
        return nativeIsAfterLast(mNativeProxy);
    }

    @Override
    public String getColumnName(int columnIndex) {
        return nativeGetColumnName(mNativeProxy, columnIndex);
    }

    @Override
    public String[] getColumnNames() {
        return nativeGetColumnNames(mNativeProxy);
    }

    @Override
    public int getColumnCount() {
        return nativeGetColumnCount(mNativeProxy);
    }

    @Override
    public byte[] getBlob(int columnIndex) {
        return nativeGetBlob(mNativeProxy, columnIndex);
    }

    @Override
    public String getString(int columnIndex) {
        return nativeGetString(mNativeProxy, columnIndex);
    }

    @Override
    public int getInt(int columnIndex) {
        return nativeGetInt(mNativeProxy, columnIndex);
    }

    @Override
    public int getType(int columnIndex) {
        return nativeGetType(mNativeProxy, columnIndex);
    }

    @Override
    public void registerContentObserver(ContentObserver observer) {
        nativeRegisterContentObserver(mNativeProxy, observer);
    }

    @Override
    public void unregisterContentObserver(ContentObserver observer) {
        nativeUnregisterContentObserver(mNativeProxy, observer);
    }

    @Deprecated @Override
    public boolean requery() {
        return nativeRequery(mNativeProxy);
    }

    @Override
    public boolean isNull(int columnIndex) {
        return nativeIsNull(mNativeProxy, columnIndex);
    }

    @Override
    public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
        nativeCopyStringToBuffer(mNativeProxy, columnIndex, buffer);
    }

    @Override
    public short getShort(int columnIndex) {
        return nativeGetShort(mNativeProxy, columnIndex);
    }

    @Override
    public float getFloat(int columnIndex) {
        return nativeGetFloat(mNativeProxy, columnIndex);
    }

    @Override
    public double getDouble(int columnIndex) {
        return nativeGetDouble(mNativeProxy, columnIndex);
    }

    @Override
    public void deactivate() {
        nativeDeactivate(mNativeProxy);
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        nativeRegisterDataSetObserver(mNativeProxy, observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        nativeUnregisterDataSetObserver(mNativeProxy, observer);
    }

    @Override
    public void setNotificationUri(ContentResolver cr, Uri uri) {
        nativeSetNotificationUri(mNativeProxy, cr, uri);
    }

    @Override
    public Uri getNotificationUri() {
        return nativeGetNotificationUri(mNativeProxy);
    }

    @Override
    public boolean getWantsAllOnMoveCalls() {
        return nativeGetWantsAllOnMoveCalls(mNativeProxy);
    }

    @Override
    public Bundle getExtras() {
        return nativeGetExtras(mNativeProxy);
    }

    @Override
    public Bundle respond(Bundle extras) {
        return nativeRespond(mNativeProxy, extras);
    }
}
