
package android.webkit;

import android.os.RemoteException;

/** @hide */
public class ElWebViewUpdateServiceProxy extends IWebViewUpdateService.Stub {
    private long mNativeProxy;

    private native void nativeNotifyRelroCreationCompleted(long nativeProxy, boolean is64Bit, boolean success);

    private native void nativeWaitForRelroCreationCompleted(long nativeProxy, boolean is64Bit);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElWebViewUpdateServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void notifyRelroCreationCompleted(boolean is64Bit, boolean success) throws RemoteException {
        nativeNotifyRelroCreationCompleted(mNativeProxy, is64Bit, success);
    }

    @Override
    public void waitForRelroCreationCompleted(boolean is64Bit) throws RemoteException {
        nativeWaitForRelroCreationCompleted(mNativeProxy, is64Bit);
    }

}
