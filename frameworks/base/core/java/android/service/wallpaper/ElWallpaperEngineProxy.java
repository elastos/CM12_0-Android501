
package android.service.wallpaper;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.MotionEvent;

/**
 * @hide
 */
public class ElWallpaperEngineProxy extends IWallpaperEngine.Stub {
    private long mNativeProxy;

    private native void nativeSetDesiredSize(long proxy, int width, int height);

    private native void nativeSetDisplayPadding(long proxy, Rect padding);

    private native void nativeSetVisibility(long proxy, boolean visible);

    private native void nativeDispatchPointer(long proxy, MotionEvent event);

    private native void nativeDispatchWallpaperCommand(long proxy, String action, int x, int y,
            int z, Bundle extras);

    private native void nativeDestroy(long proxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElWallpaperEngineProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void setDesiredSize(int width, int height) throws RemoteException {
        nativeSetDesiredSize(mNativeProxy, width, height);
    }

    @Override
    public void setDisplayPadding(Rect padding) throws RemoteException {
        nativeSetDisplayPadding(mNativeProxy, padding);
    }

    @Override
    public void setVisibility(boolean visible) throws RemoteException {
        nativeSetVisibility(mNativeProxy, visible);
    }

    @Override
    public void dispatchPointer(MotionEvent event) throws RemoteException {
        nativeDispatchPointer(mNativeProxy, event);
    }

    @Override
    public void dispatchWallpaperCommand(String action, int x, int y,
            int z, Bundle extras) throws RemoteException {
        nativeDispatchWallpaperCommand(mNativeProxy, action, x, y, z, extras);
    }

    @Override
    public void destroy() throws RemoteException {
        nativeDestroy(mNativeProxy);
    }
}
