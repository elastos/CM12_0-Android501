
package android.service.wallpaper;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.service.wallpaper.IWallpaperEngine;
import android.util.Log;

/** @hide */
public class ElWallpaperConnectionProxy extends IWallpaperConnection.Stub
            implements ServiceConnection {

    private long mNativeProxy;

    private native void nativeAttachEngine(long proxy, IWallpaperEngine engine);

    private native void nativeEngineShown(long proxy, IWallpaperEngine engine);

    private native ParcelFileDescriptor nativeSetWallpaper(long proxy, String name);

    private native void nativeOnServiceConnected(long proxy, ComponentName name, IBinder service);

    private native void nativeOnServiceDisconnected(long proxy, ComponentName name);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElWallpaperConnectionProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void attachEngine(IWallpaperEngine engine) throws RemoteException {
        nativeAttachEngine(mNativeProxy, engine);
    }

    @Override
    public void engineShown(IWallpaperEngine engine) throws RemoteException {
        nativeEngineShown(mNativeProxy, engine);
    }

    @Override
    public ParcelFileDescriptor setWallpaper(String name) throws RemoteException {
        return nativeSetWallpaper(mNativeProxy, name);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        nativeOnServiceConnected(mNativeProxy, name, service);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        nativeOnServiceDisconnected(mNativeProxy, name);
    }
}
