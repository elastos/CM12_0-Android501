
package android.service.wallpaper;

import android.graphics.Rect;
import android.os.IBinder;
import android.os.RemoteException;
import android.service.wallpaper.IWallpaperConnection;

/**
 * @hide
 */
public class ElWallpaperServiceProxy extends IWallpaperService.Stub {
    private long mNativeProxy;

    private native void nativeAttach(long proxy, IWallpaperConnection connection,
        IBinder windowToken, int windowType, boolean isPreview,
        int reqWidth, int reqHeight, Rect padding);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElWallpaperServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void attach(IWallpaperConnection connection,
        IBinder windowToken, int windowType, boolean isPreview,
        int reqWidth, int reqHeight, Rect padding) throws RemoteException {
        nativeAttach(mNativeProxy, connection, windowToken, windowType, isPreview, reqWidth, reqHeight, padding);
    }
}
