
package android.content;

import android.content.Intent;
import android.content.IIntentReceiver;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * {@hide}
 */
public class ElIIntentReceiverProxy extends IIntentReceiver.Stub {
    private long mNativeProxy;

    private static final native void nativeFinalize(long obj);

    private native void nativePerformReceive(long proxy, Intent intent, int resultCode, String data,
            Bundle extras, boolean ordered, boolean sticky, int sendingUser);

    public ElIIntentReceiverProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void performReceive(Intent intent, int resultCode, String data,
            Bundle extras, boolean ordered, boolean sticky, int sendingUser) throws RemoteException {
        nativePerformReceive(mNativeProxy, intent, resultCode, data, extras, ordered, sticky, sendingUser);
    }

    public IBinder asBinder() {
        Log.e("ElIIntentReceiverProxy", "asBinder()");
        return null;
    }

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
        }
    }
}

