
package android.content;

import android.content.IIntentReceiver;
import android.content.Intent;

/** @hide */
public class ElIIntentSenderProxy extends IIntentSender.Stub {
    private long mNativeProxy;

    private static final native void nativeFinalize(long nativeProxy);

    private native int nativeSend(long proxy, int code, Intent intent, String resolvedType,
            IIntentReceiver finishedReceiver, String requiredPermission);

    public ElIIntentSenderProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public long getIIntentSender() {
        return mNativeProxy;
    }

    public int send(int code, Intent intent, String resolvedType,
            IIntentReceiver finishedReceiver, String requiredPermission) {
        return nativeSend(mNativeProxy, code, intent, resolvedType, finishedReceiver, requiredPermission);
    }
}
