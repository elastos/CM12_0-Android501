
package android.content;

import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Binder;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.IInterface;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import android.os.Parcel;
import android.database.sqlite.SQLiteException;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * The ipc interface to talk to a content provider.
 * @hide
 */
public class ElContentProviderProxy extends Binder implements IContentProvider{
    private long mNativeProxy;

    private native Bundle nativeCall(long nativeProxy, String callingPkg, String method, String arg, Bundle extras);

    private native Cursor nativeQuery(long nativeProxy, String callingPkg, Uri url, String[] projection, String selection,
            String[] selectionArgs, String sortOrder, ICancellationSignal cancellationSignal);

    private native AssetFileDescriptor nativeOpenTypedAssetFile(long nativeProxy, String callingPkg, Uri url,
            String mimeType, Bundle opts, ICancellationSignal signal);

    private native Uri nativeInsert(long nativeProxy, String callingPkg, Uri url, ContentValues initialValues);

    private native int nativeDelete(long nativeProxy, String callingPkg, Uri url, String selection, String[] selectionArgs);

    private native int nativeUpdate(long nativeProxy, String callingPkg, Uri url, ContentValues value, String selection, String[] selectionArgs);

    private native String nativeGetType(long nativeProxy, Uri uri);

    private native int nativeBulkInsert(long nativeProxy, String callingPkg, Uri uri, ContentValues[] initialValues);

    private native ParcelFileDescriptor nativeOpenFile(long nativeProxy, String callingPkg, Uri uri, String mode, ICancellationSignal signal);

    private native AssetFileDescriptor nativeOpenAssetFile(long nativeProxy, String callingPkg, Uri url, String mode, ICancellationSignal signal);

    private native ContentProviderResult[] nativeApplyBatch(long nativeProxy, String callingPkg, ArrayList<ContentProviderOperation> operations);

    private native ICancellationSignal nativeCreateCancellationSignal(long nativeProxy);

    private native Uri nativeCanonicalize(long nativeProxy, String callingPkg, Uri uri);

    private native Uri nativeUncanonicalize(long nativeProxy, String callingPkg, Uri uri);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElContentProviderProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public Bundle call(String callingPkg, String method, String arg, Bundle extras) throws RemoteException {
        return nativeCall(mNativeProxy, callingPkg, method, arg, extras);
    }

    @Override
    public Cursor query(String callingPkg, Uri url, String[] projection, String selection,
            String[] selectionArgs, String sortOrder, ICancellationSignal cancellationSignal)
                    throws RemoteException, SQLiteException {
        return nativeQuery(mNativeProxy, callingPkg, url, projection, selection, selectionArgs, sortOrder, cancellationSignal);
    }

    @Override
    public AssetFileDescriptor openTypedAssetFile(String callingPkg, Uri url, String mimeType, Bundle opts, ICancellationSignal signal)
            throws RemoteException, FileNotFoundException {
        return nativeOpenTypedAssetFile(mNativeProxy, callingPkg, url, mimeType, opts, signal);
    }

    @Override
    public String getType(Uri url) throws RemoteException {
        return nativeGetType(mNativeProxy, url);
    }

    @Override
    public Uri insert(String callingPkg, Uri url, ContentValues initialValues) throws RemoteException {
        return nativeInsert(mNativeProxy, callingPkg, url, initialValues);
    }

    @Override
    public int bulkInsert(String callingPkg, Uri url, ContentValues[] initialValues) throws RemoteException {
        return nativeBulkInsert(mNativeProxy, callingPkg, url, initialValues);
    }

    @Override
    public int delete(String callingPkg, Uri url, String selection, String[] selectionArgs)
            throws RemoteException {
        return nativeDelete(mNativeProxy, callingPkg, url, selection, selectionArgs);
    }

    @Override
    public int update(String callingPkg, Uri url, ContentValues values, String selection,
            String[] selectionArgs) throws RemoteException {
        return nativeUpdate(mNativeProxy, callingPkg, url, values, selection, selectionArgs);
    }

    @Override
    public ParcelFileDescriptor openFile(String callingPkg, Uri url, String mode, ICancellationSignal signal)
            throws RemoteException, FileNotFoundException {
        return nativeOpenFile(mNativeProxy, callingPkg, url, mode, signal);
    }

    @Override
    public AssetFileDescriptor openAssetFile(String callingPkg, Uri url, String mode, ICancellationSignal signal)
            throws RemoteException, FileNotFoundException {
        return nativeOpenAssetFile(mNativeProxy, callingPkg, url, mode, signal);
    }

    @Override
    public ContentProviderResult[] applyBatch(String callingPkg, ArrayList<ContentProviderOperation> operations)
            throws RemoteException, OperationApplicationException {
        return nativeApplyBatch(mNativeProxy, callingPkg, operations);
    }

    @Override
    public ICancellationSignal createCancellationSignal() throws RemoteException {
        return nativeCreateCancellationSignal(mNativeProxy);
    }

    @Override
    public Uri canonicalize(String callingPkg, Uri uri) throws RemoteException {
        return nativeCanonicalize(mNativeProxy, callingPkg, uri);
    }

    @Override
    public Uri uncanonicalize(String callingPkg, Uri uri) throws RemoteException {
        return nativeUncanonicalize(mNativeProxy, callingPkg, uri);
    }

    @Override
    public String[] getStreamTypes(Uri url, String mimeTypeFilter) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    public IBinder asBinder() {
        return this;
    }
}
