
package android.content;

import android.content.res.AssetFileDescriptor;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/** @hide */
public class ElContentResolverProxy extends ContentResolver{
    public static final String TAG = "ElDataSetObserverProxy";

    private long mNativeProxy;

    // private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            // nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElContentResolverProxy(long nativeProxy) {
        super(null);
        mNativeProxy = nativeProxy;
    }

    protected IContentProvider acquireProvider(Context c, String name) {
        Log.e(TAG, "acquireProvider() E_NOT_IMPLEMENTED");
        return null;
    }

    public boolean releaseProvider(IContentProvider icp) {
        Log.e(TAG, "releaseProvider() E_NOT_IMPLEMENTED");
        return false;
    }

    protected IContentProvider acquireUnstableProvider(Context c, String name) {
        Log.e(TAG, "acquireUnstableProvider() E_NOT_IMPLEMENTED");
        return null;
    }

    public boolean releaseUnstableProvider(IContentProvider icp) {
        Log.e(TAG, "releaseUnstableProvider() E_NOT_IMPLEMENTED");
        return false;
    }

    public void unstableProviderDied(IContentProvider icp) {
        Log.e(TAG, "unstableProviderDied() E_NOT_IMPLEMENTED");
    }

    public String[] getStreamTypes(Uri url, String mimeTypeFilter) {
        Log.e(TAG, "getStreamTypes() E_NOT_IMPLEMENTED");
        return null;
    }

    public OpenResourceIdResult getResourceId(Uri uri) throws FileNotFoundException {
        Log.e(TAG, "getResourceId() E_NOT_IMPLEMENTED");
        return null;
    }

    public ContentProviderResult[] applyBatch(String authority,
            ArrayList<ContentProviderOperation> operations)
            throws RemoteException, OperationApplicationException {
        Log.e(TAG, "applyBatch() E_NOT_IMPLEMENTED");
        return null;
    }

    public void notifyChange(Uri uri, ContentObserver observer) {
        Log.e(TAG, "notifyChange() E_NOT_IMPLEMENTED");
    }

    public void notifyChange(Uri uri, ContentObserver observer, boolean syncToNetwork) {
        Log.e(TAG, "notifyChange() E_NOT_IMPLEMENTED");
    }

    public void notifyChange(Uri uri, ContentObserver observer, boolean syncToNetwork,
            int userHandle) {
        Log.e(TAG, "notifyChange() E_NOT_IMPLEMENTED");
    }

    @Deprecated
    public void startSync(Uri uri, Bundle extras) {
        Log.e(TAG, "notifyChange() E_NOT_IMPLEMENTED");
    }

    @Deprecated
    public void cancelSync(Uri uri) {
        Log.e(TAG, "notifyChange() E_NOT_IMPLEMENTED");
    }
}
