
package android.content;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.IClipboard;
import android.content.IOnPrimaryClipChangedListener;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * {@hide}
 */
public class ElClipboardProxy extends IClipboard.Stub {
    private long mNativeProxy;

    private native boolean nativeHasPrimaryClip(long proxy, String callingPackage);

    private native boolean nativeHasClipboardText(long proxy, String callingPackage);

    private native void nativeSetPrimaryClip(long proxy, ClipData clip, String callingPackage);

    private native ClipData nativeGetPrimaryClip(long proxy, String pkg);

    private native ClipDescription nativeGetPrimaryClipDescription(long proxy, String callingPackage);

    private native void nativeAddPrimaryClipChangedListener(long proxy, IOnPrimaryClipChangedListener listener, String callingPackage);

    private native void nativeRemovePrimaryClipChangedListener(long proxy, IOnPrimaryClipChangedListener listener);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElClipboardProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public boolean hasPrimaryClip(String callingPackage) throws RemoteException {
        return nativeHasPrimaryClip(mNativeProxy, callingPackage);
    }

    @Override
    public boolean hasClipboardText(String callingPackage) throws RemoteException {
        return nativeHasClipboardText(mNativeProxy, callingPackage);
    }

    @Override
    public void setPrimaryClip(ClipData clip, String callingPackage) throws RemoteException {
        nativeSetPrimaryClip(mNativeProxy, clip, callingPackage);
    }

    @Override
    public ClipData getPrimaryClip(String pkg) throws RemoteException {
        return nativeGetPrimaryClip(mNativeProxy, pkg);
    }

    @Override
    public ClipDescription getPrimaryClipDescription(String callingPackage) throws RemoteException {
        return nativeGetPrimaryClipDescription(mNativeProxy, callingPackage);
    }

    @Override
    public void addPrimaryClipChangedListener(IOnPrimaryClipChangedListener listener, String callingPackage) throws RemoteException {
        nativeAddPrimaryClipChangedListener(mNativeProxy, listener, callingPackage);
    }

    @Override
    public void removePrimaryClipChangedListener(IOnPrimaryClipChangedListener listener) throws RemoteException {
        nativeRemovePrimaryClipChangedListener(mNativeProxy, listener);
    }
}
