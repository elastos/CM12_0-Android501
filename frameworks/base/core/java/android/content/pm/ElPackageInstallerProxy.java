/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.content.pm;

import android.content.pm.IPackageDeleteObserver2;
import android.content.pm.IPackageInstallerCallback;
import android.content.pm.IPackageInstallerSession;
import android.content.pm.PackageInstaller;
import android.content.pm.ParceledListSlice;
import android.content.IntentSender;

import android.graphics.Bitmap;
import android.os.RemoteException;

class ElPackageInstallerProxy implements IPackageInstaller
{
    private long mNativeProxy;

    private native void nativeUninstall(long nativeProxy, String packageName, int flags, IntentSender statusReceiver, int userId);

    private native void nativeFinalize(long nativeProxy);

    public ElPackageInstallerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    public void finalize() {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    @Override
    public int createSession(PackageInstaller.SessionParams params, String installerPackageName, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void updateSessionAppIcon(int sessionId, Bitmap appIcon) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void updateSessionAppLabel(int sessionId, String appLabel) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void abandonSession(int sessionId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public IPackageInstallerSession openSession(int sessionId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public PackageInstaller.SessionInfo getSessionInfo(int sessionId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public ParceledListSlice getAllSessions(int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public ParceledListSlice getMySessions(String installerPackageName, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void registerCallback(IPackageInstallerCallback callback, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void unregisterCallback(IPackageInstallerCallback callback) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void uninstall(String packageName, int flags, IntentSender statusReceiver, int userId) throws RemoteException {
        nativeUninstall(mNativeProxy, packageName, flags, statusReceiver, userId);
    }

    @Override
    public void setPermissionsResult(int sessionId, boolean accepted) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public android.os.IBinder asBinder() {
        return null;
    }
}
