
package android.content.pm;

import android.app.ComposedIconInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ContainerEncryptionParams;
import android.content.pm.FeatureInfo;
import android.content.pm.IPackageInstallObserver2;
import android.content.pm.IPackageInstaller;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.IPackageDeleteObserver2;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageMoveObserver;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.InstrumentationInfo;
import android.content.pm.KeySet;
import android.content.pm.PackageInfo;
import android.content.pm.ManifestDigest;
import android.content.pm.PackageCleanItem;
import android.content.pm.ParceledListSlice;
import android.content.pm.ProviderInfo;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.UserInfo;
import android.content.pm.VerificationParams;
import android.content.pm.VerifierDeviceIdentity;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.content.IntentSender;
import android.os.RemoteException;

import java.util.List;


class ElPackageManagerProxy extends IPackageManager.Stub {

    private long mNativeProxy;

    private native boolean nativeIsPackageAvailable(long nativeProxy, String packageName, int userId);

    private native String[] nativeGetPackagesForUid(long nativeProxy, int uid);

    private native PackageInfo nativeGetPackageInfo(long nativeProxy, String packageName, int flags, int userId);

    private native ActivityInfo nativeGetActivityInfo(long nativeProxy, ComponentName componentName, int flags, int userId);

    private native boolean nativeActivitySupportsIntent(long nativeProxy,ComponentName className, Intent intent, String resolvedType);

    private native List<ResolveInfo> nativeQueryIntentActivities(long nativeProxy, Intent intent, String resolvedType, int flags, int userId);

    private native boolean nativeIsSafeMode(long nativeProxy);

    private native int nativeGetFlagsForUid(long nativeProxy, int uid);

    private native boolean nativeIsUidPrivileged(long nativeProxy, int uid);

    private native String[] nativeGetAppOpPermissionPackages(long nativeProxy, String permissionName);

    private native ResolveInfo nativeResolveIntent(long nativeProxy, Intent intent, String resolvedType, int flags, int userId);

    private native boolean nativeCanForwardTo(long nativeProxy, Intent intent, String resolvedType, int sourceUserId, int targetUserId);

    private native ApplicationInfo nativeGetApplicationInfo(long nativeProxy, String packageName, int flags ,int userId);

    private native ParceledListSlice nativeGetInstalledApplications(long nativeProxy, int flags, int userId);

    private native void nativeGetPackageSizeInfo(long nativeProxy, String packageName, int userHandle, IPackageStatsObserver observer);

    private native String[] nativeCurrentToCanonicalPackageNames(long nativeProxy, String[] names);

    private native int nativeCheckPermission(long nativeProxy, String permName, String pkgName);

    private native boolean nativeHasSystemFeature(long nativeProxy, String name);

    private native ProviderInfo nativeResolveContentProvider(long nativeProxy, String name, int flags, int userId);

    private native List<ResolveInfo> nativeQueryIntentServices(long nativeProxy, Intent intent, String resolvedType, int flags, int userId);

    private native List<ResolveInfo> nativeQueryIntentContentProviders(long nativeProxy, Intent intent, String resolvedType, int flags, int userId);

    private native int nativeGetComponentEnabledSetting(long nativeProxy, ComponentName componentName, int userId);

    private native ServiceInfo nativeGetServiceInfo(long nativeProxy, ComponentName className, int flags, int userId);

    private native ActivityInfo nativeGetReceiverInfo(long nativeProxy, ComponentName className, int flags, int userId);

    private native List<ResolveInfo> nativeQueryIntentReceivers(long nativeProxy, Intent intent, String resolvedType, int flags, int userId);

    private native PermissionInfo nativeGetPermissionInfo(long nativeProxy, String name, int flags);

    private native boolean nativeIsPermissionEnforced(long nativeProxy, String permission);

    private native PermissionGroupInfo nativeGetPermissionGroupInfo(long nativeProxy, String name, int flags);

    private native int nativeGetPreferredActivities(long nativeProxy, List<IntentFilter> outFilters, List<ComponentName> outActivities, String packageName);

    private native void nativeAddPersistentPreferredActivity(long nativeProxy, IntentFilter filter, ComponentName activity, int userId);

    private native void nativeClearPackagePersistentPreferredActivities(long nativeProxy, String packageName, int userId);

    private native void nativeAddCrossProfileIntentFilter(long nativeProxy, IntentFilter intentFilter, String ownerPackage,
            int ownerUserId, int sourceUserId, int targetUserId, int flags);

    private native void nativeClearCrossProfileIntentFilters(long nativeProxy, int sourceUserId, String ownerPackage, int ownerUserId);

    private native ComponentName nativeGetHomeActivities(long nativeProxy, List<ResolveInfo> outHomeCandidates);

    private native void nativeSetComponentEnabledSetting(long nativeProxy, ComponentName componentName, int newState, int flags, int userId);

    private native int nativeGetApplicationEnabledSetting(long nativeProxy, String packageName, int userId);

    private native String nativeGetInstallerPackageName(long nativeProxy, String packageName);

    private native ResolveInfo nativeResolveService(long nativeProxy, Intent intent, String resolvedType, int flags, int userId);

    private native boolean nativeIsOnlyCoreApps(long nativeProxy);

    private native void nativeDeletePackageAsUser(long nativeProxy, String packageName, IPackageDeleteObserver observer, int userId, int flags);

    private native void nativeDeletePackage(long nativeProxy, String packageName, IPackageDeleteObserver2 observer, int userId, int flags);

    private native String[] nativeCanonicalToCurrentPackageNames(long nativeProxy, String[] names);

    private native int nativeCheckSignatures(long nativeProxy, String pkg1, String pkg2);

    private native int nativeCheckUidSignatures(long nativeProxy, int uid1, int uid2);

    private native void nativeDeleteApplicationCacheFiles(long nativeProxy, String packageName, IPackageDataObserver observer);

    private native String[] nativeGetSystemSharedLibraryNames(long nativeProxy);

    private native FeatureInfo[] nativeGetSystemAvailableFeatures(long nativeProxy);

    private native int nativeGetPackageUid(long nativeProxy, String packageName, int userId);

    private native int[] nativeGetPackageGids(long nativeProxy, String packageName);

    private native PackageCleanItem nativeNextPackageToClean(long nativeProxy, PackageCleanItem lastPackage);

    private native List<PermissionInfo> nativeQueryPermissionsByGroup(long nativeProxy, String group, int flags);

    private native List<PermissionGroupInfo> nativeGetAllPermissionGroups(long nativeProxy, int flags);

    private native ProviderInfo nativeGetProviderInfo(long nativeProxy, ComponentName className, int flags, int userId);

    private native int nativeCheckUidPermission(long nativeProxy, String permName, int uid);

    private native boolean nativeAddPermission(long nativeProxy, PermissionInfo info);

    private native void nativeRemovePermission(long nativeProxy, String name);

    private native void nativeGrantPermission(long nativeProxy, String packageName, String permissionName);

    private native void nativeRevokePermission(long nativeProxy, String packageName, String permissionName);

    private native boolean nativeIsProtectedBroadcast(long nativeProxy, String actionName);

    private native String nativeGetNameForUid(long nativeProxy, int uid);

    private native int nativeGetUidForSharedUser(long nativeProxy, String sharedUserName);

    private native List<ResolveInfo> nativeQueryIntentActivityOptions(long nativeProxy, ComponentName caller,
            Intent[] specifics, String[] specificTypes, Intent intent, String resolvedType, int flags, int userId);

    private native ParceledListSlice nativeGetInstalledPackages(long nativeProxy, int flags, int userId);

    private native ParceledListSlice nativeGetPackagesHoldingPermissions(long nativeProxy, String[] permissions, int flags, int userId);

    private native List<ApplicationInfo> nativeGetPersistentApplications(long nativeProxy, int flags);

    private native void nativeQuerySyncProviders(long nativeProxy, List<String> outNames, List<ProviderInfo> outInfo);

    private native List<ProviderInfo> nativeQueryContentProviders(long nativeProxy, String processName, int uid, int flags);

    private native InstrumentationInfo nativeGetInstrumentationInfo(long nativeProxy, ComponentName className, int flags);

    private native List<InstrumentationInfo> nativeQueryInstrumentation(long nativeProxy, String targetPackage, int flags);

    private native void nativeInstallPackage(long nativeProxy, String originPath, IPackageInstallObserver2 observer, int flags,
            String installerPackageName, VerificationParams verificationParams, String packageAbiOverride);

    private native void nativeInstallPackageAsUser(long nativeProxy, String originPath, IPackageInstallObserver2 observer, int flags,
            String installerPackageName, VerificationParams verificationParams, String packageAbiOverride, int userId);

    private native void nativeFinishPackageInstall(long nativeProxy, int token);

    private native void nativeSetInstallerPackageName(long nativeProxy, String targetPackage, String installerPackageName);

    private native void nativeAddPackageToPreferred(long nativeProxy, String packageName);

    private native void nativeRemovePackageFromPreferred(long nativeProxy, String packageName);

    private native List<PackageInfo> nativeGetPreferredPackages(long nativeProxy, int flags);

    private native void nativeResetPreferredActivities(long nativeProxy, int userId);

    private native ResolveInfo nativeGetLastChosenActivity(long nativeProxy, Intent intent,
            String resolvedType, int flags);

    private native void nativeSetLastChosenActivity(long nativeProxy, Intent intent, String resolvedType, int flags,
            IntentFilter filter, int match, ComponentName activity);

    private native void nativeAddPreferredActivity(long nativeProxy, IntentFilter filter, int match, ComponentName[] set, ComponentName activity, int userId);

    private native void naitveReplacePreferredActivity(long nativeProxy, IntentFilter filter, int match,
            ComponentName[] set, ComponentName activity, int userId);

    private native void nativeClearPackagePreferredActivities(long nativeProxy, String packageName);

    private native void nativeSetApplicationEnabledSetting(long nativeProxy, String packageName, int newState, int flags, int userId, String callingPackage);

    private native void nativeSetPackageStoppedState(long nativeProxy, String packageName, boolean stopped, int userId);

    private native void nativeFreeStorageAndNotify(long nativeProxy, long freeStorageSize, IPackageDataObserver observer);

    private native void nativeFreeStorage(long nativeProxy, long freeStorageSize, IntentSender pi);

    private native void nativeClearApplicationUserData(long nativeProxy, String packageName, IPackageDataObserver observer, int userId);

    private native void nativeEnterSafeMode(long nativeProxy);

    private native void nativeSystemReady(long nativeProxy);

    private native boolean nativeHasSystemUidErrors(long nativeProxy);

    private native void nativePerformBootDexOpt(long nativeProxy);

    private native boolean nativePerformDexOptIfNeeded(long nativeProxy, String packageName, String instructionSet);

    private native void nativeForceDexOpt(long nativeProxy, String packageName);

    private native void nativeUpdateExternalMediaStatus(long nativeProxy, boolean mounted, boolean reportStatus);

    private native void nativeMovePackage(long nativeProxy, String packageName, IPackageMoveObserver observer, int flags);

    private native boolean nativeAddPermissionAsync(long nativeProxy, PermissionInfo info);

    private native boolean nativeSetInstallLocation(long nativeProxy, int loc);

    private native int nativeGetInstallLocation(long nativeProxy);

    private native int nativeInstallExistingPackageAsUser(long nativeProxy, String packageName, int userId);

    private native void nativeVerifyPendingInstall(long nativeProxy, int id, int verificationCode);

    private native void nativeExtendVerificationTimeout(long nativeProxy, int id, int verificationCodeAtTimeout, long millisecondsToDelay);

    private native VerifierDeviceIdentity nativeGetVerifierDeviceIdentity(long nativeProxy);

    private native boolean nativeIsFirstBoot(long nativeProxy);

    private native void nativeSetPermissionEnforced(long nativeProxy, String permission, boolean enforced);

    private native boolean nativeIsStorageLow(long nativeProxy);

    private native boolean nativeSetApplicationHiddenSettingAsUser(long nativeProxy, String packageName, boolean hidden, int userId);

    private native boolean nativeGetApplicationHiddenSettingAsUser(long nativeProxy, String packageName, int userId);

    private native IPackageInstaller nativeGetPackageInstaller(long nativeProxy);

    private native boolean nativeSetBlockUninstallForUser(long nativeProxy, String packageName, boolean blockUninstall, int userId);

    private native boolean nativeGetBlockUninstallForUser(long nativeProxy, String packageName, int userId);

    private native KeySet nativeGetKeySetByAlias(long nativeProxy, String packageName, String alias);

    private native KeySet nativeGetSigningKeySet(long nativeProxy, String packageName);

    private native boolean nativeIsPackageSignedByKeySet(long nativeProxy, String packageName, KeySet ks);

    private native boolean nativeIsPackageSignedByKeySetExactly(long nativeProxy, String packageName, KeySet ks);

    private native void nativeSetComponentProtectedSetting(long nativeProxy, ComponentName componentName, boolean newState, int userId);

    private native void nativeUpdateIconMapping(long nativeProxy, String pkgName);

    private native ComposedIconInfo nativeGetComposedIconInfo(long nativeProxy);

    private native int nativeProcessThemeResources(long nativeProxy, String themePkgName);

    private native void nativeSetPreLaunchCheckActivity(long nativeProxy, ComponentName componentName);

    private native void nativeAddPreLaunchCheckPackage(long nativeProxy, String packageName);

    private native void nativeRemovePreLaunchCheckPackage(long nativeProxy, String packageName);

    private native void nativeClearPreLaunchCheckPackages(long nativeProxy);

    private native void nativeParseSignatureByJava(long nativeProxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElPackageManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    public boolean isPackageAvailable(String packageName, int userId) throws RemoteException {
        return nativeIsPackageAvailable(mNativeProxy, packageName, userId);
    }

    public String[] getPackagesForUid(int uid) throws RemoteException {
        return nativeGetPackagesForUid(mNativeProxy, uid);
    }

    public PackageInfo getPackageInfo(String packageName, int flags, int userId) throws RemoteException {
        return nativeGetPackageInfo(mNativeProxy,packageName, flags, userId);
    }

    public ActivityInfo getActivityInfo(ComponentName componentName, int flags, int userId) throws RemoteException {
        return nativeGetActivityInfo(mNativeProxy, componentName, flags, userId);
    }

    public boolean activitySupportsIntent(ComponentName className, Intent intent, String resolvedType) throws RemoteException {
        return nativeActivitySupportsIntent(mNativeProxy, className, intent, resolvedType);
    }

    public List<ResolveInfo> queryIntentActivities(Intent intent,
            String resolvedType, int flags, int userId) throws RemoteException {
        return nativeQueryIntentActivities(mNativeProxy, intent, resolvedType, flags, userId);
    }

    public boolean isSafeMode() throws RemoteException {
        return nativeIsSafeMode(mNativeProxy);
    }

    public int getFlagsForUid(int uid) throws RemoteException {
        return nativeGetFlagsForUid(mNativeProxy, uid);
    }

    public boolean isUidPrivileged(int uid) throws RemoteException {
        return nativeIsUidPrivileged(mNativeProxy, uid);
    }

    public String[] getAppOpPermissionPackages(String permissionName) throws RemoteException {
        return nativeGetAppOpPermissionPackages(mNativeProxy, permissionName);
    }

    public ResolveInfo resolveIntent(Intent intent, String resolvedType, int flags, int userId) throws RemoteException {
        return nativeResolveIntent(mNativeProxy, intent, resolvedType, flags, userId);
    }

    public boolean canForwardTo(Intent intent, String resolvedType, int sourceUserId, int targetUserId) throws RemoteException {
        return nativeCanForwardTo(mNativeProxy, intent, resolvedType, sourceUserId, targetUserId);
    }

    public ApplicationInfo getApplicationInfo(String packageName, int flags ,int userId) throws RemoteException {
        return nativeGetApplicationInfo(mNativeProxy, packageName, flags, userId);
    }

    public ParceledListSlice getInstalledApplications(int flags, int userId) throws RemoteException {
        return nativeGetInstalledApplications(mNativeProxy, flags, userId);
    }

    public void getPackageSizeInfo(String packageName, int userHandle, IPackageStatsObserver observer) throws RemoteException {
        nativeGetPackageSizeInfo(mNativeProxy, packageName, userHandle, observer);
    }

    public int checkPermission(String permName, String pkgName) throws RemoteException {
        return nativeCheckPermission(mNativeProxy, permName, pkgName);
    }

    public boolean hasSystemFeature(String name) throws RemoteException {
        return nativeHasSystemFeature(mNativeProxy, name);
    }

    public ProviderInfo resolveContentProvider(String name, int flags, int userId) throws RemoteException {
        return nativeResolveContentProvider(mNativeProxy, name, flags, userId);
    }

    public List<ResolveInfo> queryIntentServices(Intent intent,
            String resolvedType, int flags, int userId) throws RemoteException {
        return nativeQueryIntentServices(mNativeProxy, intent, resolvedType, flags, userId);
    }

    public List<ResolveInfo> queryIntentContentProviders(Intent intent,
            String resolvedType, int flags, int userId) throws RemoteException {
        return nativeQueryIntentContentProviders(mNativeProxy, intent, resolvedType, flags, userId);
    }

    public int getComponentEnabledSetting(ComponentName componentName, int userId) throws RemoteException {
        return nativeGetComponentEnabledSetting(mNativeProxy, componentName, userId);
    }

    public ServiceInfo getServiceInfo(ComponentName className, int flags, int userId) throws RemoteException {
        return nativeGetServiceInfo(mNativeProxy, className, flags, userId);
    }

    public ActivityInfo getReceiverInfo(ComponentName className, int flags, int userId) throws RemoteException {
        return nativeGetReceiverInfo(mNativeProxy, className, flags, userId);
    }

    public List<ResolveInfo> queryIntentReceivers(Intent intent,
            String resolvedType, int flags, int userId) throws RemoteException {
        return nativeQueryIntentReceivers(mNativeProxy, intent, resolvedType, flags, userId);
    }

    public PermissionInfo getPermissionInfo(String name, int flags) throws RemoteException {
        return nativeGetPermissionInfo(mNativeProxy, name, flags);
    }

    public boolean isPermissionEnforced(String permission) throws RemoteException {
        return nativeIsPermissionEnforced(mNativeProxy, permission);
    }

    public PermissionGroupInfo getPermissionGroupInfo(String name, int flags) throws RemoteException {
        return nativeGetPermissionGroupInfo(mNativeProxy, name, flags);
    }

    public int getPreferredActivities(List<IntentFilter> outFilters,
            List<ComponentName> outActivities, String packageName) throws RemoteException {
        return nativeGetPreferredActivities(mNativeProxy, outFilters, outActivities, packageName);
    }

    public void addPersistentPreferredActivity(IntentFilter filter, ComponentName activity, int userId) throws RemoteException {
        nativeAddPersistentPreferredActivity(mNativeProxy, filter, activity, userId);
    }

    public void clearPackagePersistentPreferredActivities(String packageName, int userId) throws RemoteException {
        nativeClearPackagePersistentPreferredActivities(mNativeProxy, packageName, userId);
    }

    public void addCrossProfileIntentFilter(IntentFilter intentFilter, String ownerPackage,
            int ownerUserId, int sourceUserId, int targetUserId, int flags) throws RemoteException {
        nativeAddCrossProfileIntentFilter(mNativeProxy, intentFilter, ownerPackage, ownerUserId, sourceUserId, targetUserId, flags);
    }

    public void clearCrossProfileIntentFilters(int sourceUserId, String ownerPackage, int ownerUserId) throws RemoteException {
        nativeClearCrossProfileIntentFilters(mNativeProxy, sourceUserId, ownerPackage, ownerUserId);
    }

    public ComponentName getHomeActivities(List<ResolveInfo> outHomeCandidates) throws RemoteException {
        return nativeGetHomeActivities(mNativeProxy, outHomeCandidates);
    }

    public String[] currentToCanonicalPackageNames(String[] names) throws RemoteException {
        return nativeCurrentToCanonicalPackageNames(mNativeProxy, names);
    }

    public void setComponentEnabledSetting(ComponentName componentName,
            int newState, int flags, int userId) throws RemoteException {
        nativeSetComponentEnabledSetting(mNativeProxy, componentName, newState, flags, userId);
    }

    public int getApplicationEnabledSetting(String packageName, int userId) throws RemoteException {
        return nativeGetApplicationEnabledSetting(mNativeProxy, packageName, userId);
    }

    public String getInstallerPackageName(String packageName) throws RemoteException {
        return nativeGetInstallerPackageName(mNativeProxy, packageName);
    }

    public ResolveInfo resolveService(Intent intent,
            String resolvedType, int flags, int userId) throws RemoteException {
        return nativeResolveService(mNativeProxy, intent, resolvedType, flags, userId);
    }

    public boolean isOnlyCoreApps() throws RemoteException {
        return nativeIsOnlyCoreApps(mNativeProxy);
    }

    public void deletePackageAsUser(String packageName, IPackageDeleteObserver observer, int userId, int flags) throws RemoteException {
        nativeDeletePackageAsUser(mNativeProxy, packageName, observer, userId, flags);
    }

    public void deletePackage(String packageName, IPackageDeleteObserver2 observer, int userId, int flags) throws RemoteException {
        nativeDeletePackage(mNativeProxy, packageName, observer, userId, flags);
    }

    public int checkSignatures(String pkg1, String pkg2) throws RemoteException {
        return nativeCheckSignatures(mNativeProxy, pkg1, pkg2);
    }

    public int checkUidSignatures(int uid1, int uid2) throws RemoteException {
        return nativeCheckUidSignatures(mNativeProxy, uid1, uid2);
    }

    public void deleteApplicationCacheFiles(String packageName, IPackageDataObserver observer) throws RemoteException {
        nativeDeleteApplicationCacheFiles(mNativeProxy, packageName, observer);
    }

    public String[] getSystemSharedLibraryNames() throws RemoteException {
        return nativeGetSystemSharedLibraryNames(mNativeProxy);
    }

    public FeatureInfo[] getSystemAvailableFeatures() throws RemoteException {
        return nativeGetSystemAvailableFeatures(mNativeProxy);
    }

    public int getPackageUid(String packageName, int userId) throws RemoteException {
        return nativeGetPackageUid(mNativeProxy, packageName, userId);
    }

    public int[] getPackageGids(String packageName) throws RemoteException {
        return nativeGetPackageGids(mNativeProxy, packageName);
    }

    public String[] canonicalToCurrentPackageNames(String[] names) throws RemoteException {
        return nativeCanonicalToCurrentPackageNames(mNativeProxy, names);
    }

    public PackageCleanItem nextPackageToClean(PackageCleanItem lastPackage) throws RemoteException {
        return nativeNextPackageToClean(mNativeProxy, lastPackage);
    }

    public List<PermissionInfo> queryPermissionsByGroup(String group, int flags) throws RemoteException {
        return nativeQueryPermissionsByGroup(mNativeProxy, group, flags);
    }

    public List<PermissionGroupInfo> getAllPermissionGroups(int flags) throws RemoteException {
        return nativeGetAllPermissionGroups(mNativeProxy, flags);
    }

    public ProviderInfo getProviderInfo(ComponentName className, int flags, int userId) throws RemoteException {
        return nativeGetProviderInfo(mNativeProxy, className, flags, userId);
    }

    public int checkUidPermission(String permName, int uid) throws RemoteException {
        return nativeCheckUidPermission(mNativeProxy, permName, uid);
    }

    public boolean addPermission(PermissionInfo info) throws RemoteException {
        return nativeAddPermission(mNativeProxy, info);
    }

    public void removePermission(String name) throws RemoteException {
        nativeRemovePermission(mNativeProxy, name);
    }

    public void grantPermission(String packageName, String permissionName) throws RemoteException {
        nativeGrantPermission(mNativeProxy, packageName, permissionName);
    }

    public void revokePermission(String packageName, String permissionName) throws RemoteException {
        nativeRevokePermission(mNativeProxy, packageName, permissionName);
    }

    public boolean isProtectedBroadcast(String actionName) throws RemoteException {
        return nativeIsProtectedBroadcast(mNativeProxy, actionName);
    }

    public String getNameForUid(int uid) throws RemoteException {
        return nativeGetNameForUid(mNativeProxy, uid);
    }

    public int getUidForSharedUser(String sharedUserName) throws RemoteException {
        return nativeGetUidForSharedUser(mNativeProxy, sharedUserName);
    }

    public List<ResolveInfo> queryIntentActivityOptions(ComponentName caller, Intent[] specifics,
            String[] specificTypes, Intent intent, String resolvedType, int flags, int userId) throws RemoteException {
        return nativeQueryIntentActivityOptions(mNativeProxy, caller, specifics, specificTypes, intent,
            resolvedType, flags, userId);
    }

    public ParceledListSlice getInstalledPackages(int flags, int userId) throws RemoteException {
        return nativeGetInstalledPackages(mNativeProxy, flags, userId);
    }

    public ParceledListSlice getPackagesHoldingPermissions(String[] permissions, int flags, int userId) throws RemoteException {
        return nativeGetPackagesHoldingPermissions(mNativeProxy, permissions, flags, userId);
    }

    public List<ApplicationInfo> getPersistentApplications(int flags) throws RemoteException {
        return nativeGetPersistentApplications(mNativeProxy, flags);
    }

    public void querySyncProviders(List<String> outNames, List<ProviderInfo> outInfo) throws RemoteException {
        nativeQuerySyncProviders(mNativeProxy, outNames, outInfo);
    }

    public List<ProviderInfo> queryContentProviders(String processName, int uid, int flags) throws RemoteException {
        return nativeQueryContentProviders(mNativeProxy, processName, uid, flags);
    }

    public InstrumentationInfo getInstrumentationInfo(ComponentName className, int flags) throws RemoteException {
        return nativeGetInstrumentationInfo(mNativeProxy, className, flags);
    }

    public List<InstrumentationInfo> queryInstrumentation(String targetPackage, int flags) throws RemoteException {
        return nativeQueryInstrumentation(mNativeProxy, targetPackage, flags);
    }

    public void installPackage(String originPath, IPackageInstallObserver2 observer, int flags,
            String installerPackageName, VerificationParams verificationParams, String packageAbiOverride) throws RemoteException {
        nativeInstallPackage(mNativeProxy, originPath, observer, flags, installerPackageName, verificationParams, packageAbiOverride);
    }

    public void installPackageAsUser(String originPath, IPackageInstallObserver2 observer, int flags,
            String installerPackageName, VerificationParams verificationParams, String packageAbiOverride, int userId) throws RemoteException {
        nativeInstallPackageAsUser(mNativeProxy, originPath, observer, flags, installerPackageName, verificationParams, packageAbiOverride, userId);
    }

    public void finishPackageInstall(int token) throws RemoteException {
        nativeFinishPackageInstall(mNativeProxy, token);
    }

    public void setInstallerPackageName(String targetPackage, String installerPackageName) throws RemoteException {
        nativeSetInstallerPackageName(mNativeProxy, targetPackage, installerPackageName);
    }

    public void addPackageToPreferred(String packageName) throws RemoteException {
        nativeAddPackageToPreferred(mNativeProxy, packageName);
    }

    public void removePackageFromPreferred(String packageName) throws RemoteException {
        nativeRemovePackageFromPreferred(mNativeProxy, packageName);
    }

    public List<PackageInfo> getPreferredPackages(int flags) throws RemoteException {
        return nativeGetPreferredPackages(mNativeProxy, flags);
    }

    public void resetPreferredActivities(int userId) throws RemoteException {
        nativeResetPreferredActivities(mNativeProxy, userId);
    }

    public ResolveInfo getLastChosenActivity(Intent intent,
            String resolvedType, int flags) throws RemoteException {
        return nativeGetLastChosenActivity(mNativeProxy, intent, resolvedType, flags);
    }

    public void setLastChosenActivity(Intent intent, String resolvedType, int flags,
            IntentFilter filter, int match, ComponentName activity) throws RemoteException {
        nativeSetLastChosenActivity(mNativeProxy, intent, resolvedType, flags, filter, match, activity);
    }

    public void addPreferredActivity(IntentFilter filter, int match,
            ComponentName[] set, ComponentName activity, int userId) throws RemoteException {
        nativeAddPreferredActivity(mNativeProxy, filter, match, set, activity, userId);
    }

    public void replacePreferredActivity(IntentFilter filter, int match,
            ComponentName[] set, ComponentName activity, int userId) throws RemoteException {
        naitveReplacePreferredActivity(mNativeProxy, filter, match, set, activity, userId);
    }

    public void clearPackagePreferredActivities(String packageName) throws RemoteException {
        nativeClearPackagePreferredActivities(mNativeProxy, packageName);
    }

    public void setApplicationEnabledSetting(String packageName, int newState, int flags, int userId, String callingPackage) throws RemoteException {
        nativeSetApplicationEnabledSetting(mNativeProxy, packageName, newState, flags, userId, callingPackage);
    }

    public void setPackageStoppedState(String packageName, boolean stopped, int userId) throws RemoteException {
        nativeSetPackageStoppedState(mNativeProxy, packageName, stopped, userId);
    }

    public void freeStorageAndNotify(long freeStorageSize, IPackageDataObserver observer) throws RemoteException {
        nativeFreeStorageAndNotify(mNativeProxy, freeStorageSize, observer);
    }

    public void freeStorage(long freeStorageSize, IntentSender pi) throws RemoteException {
        nativeFreeStorage(mNativeProxy, freeStorageSize, pi);
    }

    public void clearApplicationUserData(String packageName, IPackageDataObserver observer, int userId) throws RemoteException {
        nativeClearApplicationUserData(mNativeProxy, packageName, observer, userId);
    }

    public void enterSafeMode() throws RemoteException {
        nativeEnterSafeMode(mNativeProxy);
    }

    public void systemReady() throws RemoteException {
        nativeSystemReady(mNativeProxy);
    }

    public boolean hasSystemUidErrors() throws RemoteException {
        return nativeHasSystemUidErrors(mNativeProxy);
    }

    public void performBootDexOpt() throws RemoteException {
        nativePerformBootDexOpt(mNativeProxy);
    }

    public boolean performDexOptIfNeeded(String packageName, String instructionSet) throws RemoteException {
        return nativePerformDexOptIfNeeded(mNativeProxy, packageName, instructionSet);
    }

    public void forceDexOpt(String packageName) throws RemoteException {
        nativeForceDexOpt(mNativeProxy, packageName);
    }

    public void updateExternalMediaStatus(boolean mounted, boolean reportStatus) throws RemoteException {
        nativeUpdateExternalMediaStatus(mNativeProxy, mounted, reportStatus);
    }

    public void movePackage(String packageName, IPackageMoveObserver observer, int flags) throws RemoteException {
        nativeMovePackage(mNativeProxy, packageName, observer, flags);
    }

    public boolean addPermissionAsync(PermissionInfo info) throws RemoteException {
        return nativeAddPermissionAsync(mNativeProxy, info);
    }

    public boolean setInstallLocation(int loc) throws RemoteException {
        return nativeSetInstallLocation(mNativeProxy, loc);
    }

    public int getInstallLocation() throws RemoteException {
        return nativeGetInstallLocation(mNativeProxy);
    }

    public int installExistingPackageAsUser(String packageName, int userId) throws RemoteException {
        return nativeInstallExistingPackageAsUser(mNativeProxy, packageName, userId);
    }

    public void verifyPendingInstall(int id, int verificationCode) throws RemoteException {
        nativeVerifyPendingInstall(mNativeProxy, id, verificationCode);
    }

    public void extendVerificationTimeout(int id, int verificationCodeAtTimeout, long millisecondsToDelay) throws RemoteException {
        nativeExtendVerificationTimeout(mNativeProxy, id, verificationCodeAtTimeout, millisecondsToDelay);
    }

    public VerifierDeviceIdentity getVerifierDeviceIdentity() throws RemoteException {
        return nativeGetVerifierDeviceIdentity(mNativeProxy);
    }

    public boolean isFirstBoot() throws RemoteException {
        return nativeIsFirstBoot(mNativeProxy);
    }

    public void setPermissionEnforced(String permission, boolean enforced) throws RemoteException {
        nativeSetPermissionEnforced(mNativeProxy, permission, enforced);
    }

    public boolean isStorageLow() throws RemoteException {
        return nativeIsStorageLow(mNativeProxy);
    }

    public boolean setApplicationHiddenSettingAsUser(String packageName, boolean hidden, int userId) throws RemoteException {
        return nativeSetApplicationHiddenSettingAsUser(mNativeProxy, packageName, hidden, userId);
    }

    public boolean getApplicationHiddenSettingAsUser(String packageName, int userId) throws RemoteException {
        return nativeGetApplicationHiddenSettingAsUser(mNativeProxy, packageName, userId);
    }

    public IPackageInstaller getPackageInstaller() throws RemoteException {
        return nativeGetPackageInstaller(mNativeProxy);
    }

    public boolean setBlockUninstallForUser(String packageName, boolean blockUninstall, int userId) throws RemoteException {
        return nativeSetBlockUninstallForUser(mNativeProxy, packageName, blockUninstall, userId);
    }

    public boolean getBlockUninstallForUser(String packageName, int userId) throws RemoteException {
        return nativeGetBlockUninstallForUser(mNativeProxy, packageName, userId);
    }

    public KeySet getKeySetByAlias(String packageName, String alias) throws RemoteException {
        return nativeGetKeySetByAlias(mNativeProxy, packageName, alias);
    }

    public KeySet getSigningKeySet(String packageName) throws RemoteException {
        return nativeGetSigningKeySet(mNativeProxy, packageName);
    }

    public boolean isPackageSignedByKeySet(String packageName, KeySet ks) throws RemoteException {
        return nativeIsPackageSignedByKeySet(mNativeProxy, packageName, ks);
    }

    public boolean isPackageSignedByKeySetExactly(String packageName, KeySet ks) throws RemoteException {
        return nativeIsPackageSignedByKeySetExactly(mNativeProxy, packageName, ks);
    }

    public void setComponentProtectedSetting(ComponentName componentName, boolean newState, int userId) throws RemoteException {
        nativeSetComponentProtectedSetting(mNativeProxy, componentName, newState, userId);
    }

    public void updateIconMapping(String pkgName) throws RemoteException {
        nativeUpdateIconMapping(mNativeProxy, pkgName);
    }

    public ComposedIconInfo getComposedIconInfo() throws RemoteException {
        return nativeGetComposedIconInfo(mNativeProxy);
    }

    public int processThemeResources(String themePkgName) throws RemoteException {
        return nativeProcessThemeResources(mNativeProxy, themePkgName);
    }

    public void setPreLaunchCheckActivity(ComponentName componentName) throws RemoteException {
        nativeSetPreLaunchCheckActivity(mNativeProxy, componentName);
    }

    public void addPreLaunchCheckPackage(String packageName) throws RemoteException {
        nativeAddPreLaunchCheckPackage(mNativeProxy, packageName);
    }

    public void removePreLaunchCheckPackage(String packageName) throws RemoteException {
        nativeRemovePreLaunchCheckPackage(mNativeProxy, packageName);
    }

    public void clearPreLaunchCheckPackages() throws RemoteException {
        nativeClearPreLaunchCheckPackages(mNativeProxy);
    }

    // TODO: delete this after elastos's signature can be parsed
    public void parseSignatureByJava() throws RemoteException {
        nativeParseSignatureByJava(mNativeProxy);
    }
}

