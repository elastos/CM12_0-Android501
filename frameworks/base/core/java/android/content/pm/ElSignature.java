
package android.content.pm;

import android.util.Log;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.File;

/**
 * {@hide}
 */
public class ElSignature {

    private static HashMap<String, Signature> gSignatures = new HashMap<String, Signature>();
    private static HashMap<String, String> gPkgPathMap = new HashMap<String, String>();

    public static Signature getSignature(String pkg) {
        if (pkg == null) {
            Log.e("ElSignature", "Invalid argumenet!");
            return null;
        }

        // Signature sign = gSignatures.get(pkg);
        // if (sign != null)  return sign;

        String apkPath = gPkgPathMap.get(pkg);
        if (apkPath == null) {
            // Refresh
            loadInstalledAppsMap();

            apkPath = gPkgPathMap.get(pkg);
            if (apkPath == null) {
                Log.e("ElSignature", "Invalid packageName: " + pkg);
                return null;
            }
        }

        if (apkPath.endsWith(".epk")) return null;

        try {
            PackageParser pkgParser = new PackageParser();
            PackageParser.Package p = pkgParser.parsePackage(new File(apkPath), PackageManager.GET_SIGNATURES);
            pkgParser.collectCertificates(p, PackageManager.GET_SIGNATURES);
            Signature[] info = p.mSignatures;

            if (info != null) {
                // gSignatures.put(pkg, info[0]);
                return info[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private static void loadInstalledAppsMap() {

        gPkgPathMap.clear();

        try {
            ApplicationInfo lastItem = null;
            ParceledListSlice<ApplicationInfo> slice;
            List<ApplicationInfo> listApps = new ArrayList<ApplicationInfo>();
            int userId = UserHandle.getUserId(Process.myUid());
            IPackageManager pm = (IPackageManager) ServiceManager.getService("package");

            slice = pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES, userId);
            if (slice != null) {
                listApps = slice.getList();
            }
            else {
                Log.e("ElSignature", "slice is null");
            }

            for (ApplicationInfo app : listApps) {
                gPkgPathMap.put(app.packageName, app.sourceDir);
            }

        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }
}