
package android.content;

import android.accounts.Account;
import android.content.SyncInfo;
import android.content.ISyncStatusObserver;
import android.content.SyncAdapterType;
import android.content.SyncStatusInfo;
import android.content.PeriodicSync;
import android.database.IContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.List;

/**
 * @hide
 */
public class ElContentServiceProxy extends IContentService.Stub {
    private long mNativeProxy;

    private native void nativeRegisterContentObserver(long proxy, Uri uri, boolean notifyForDescendants,
            IContentObserver observer, int observerHashCode, int userHandle);

    private native void nativeUnregisterContentObserver(long proxy, int observer);

    private native void nativeNotifyChange(long proxy, Uri uri, int observer,
            boolean observerWantsSelfNotifications, boolean syncToNetwork,
            int userHandle);

    private native void nativeRequestSync(long proxy, Account account, String authority, Bundle extras);

    private native void nativeSync(long proxy, SyncRequest request);

    private native void nativeSyncAsUser(long proxy, SyncRequest request, int userId);

    private native void nativeCancelSync(long proxy, Account account, String authority, ComponentName cname);

    private native void nativeCancelSyncAsUser(long proxy, Account account, String authority, ComponentName cname, int userId);

    private native void nativeCancelRequest(long proxy, SyncRequest request);

    private native boolean nativeGetSyncAutomatically(long proxy, Account account, String providerName);

    private native boolean nativeGetSyncAutomaticallyAsUser(long proxy, Account account, String providerName, int userId);

    private native void nativeSetSyncAutomatically(long proxy, Account account, String providerName, boolean sync);

    private native void nativeSetSyncAutomaticallyAsUser(long proxy, Account account, String providerName, boolean sync, int userId);

    private native List<PeriodicSync> nativeGetPeriodicSyncs(long proxy, Account account, String providerName, ComponentName cname);

    private native void nativeAddPeriodicSync(long proxy, Account account, String providerName, Bundle extras, long pollFrequency);

    private native void nativeRemovePeriodicSync(long proxy, Account account, String providerName, Bundle extras);

    private native int nativeGetIsSyncable(long proxy, Account account, String providerName);

    private native int nativeGetIsSyncableAsUser(long proxy, Account account, String providerName, int userId);

    private native void nativeSetIsSyncable(long proxy, Account account, String providerName, int syncable);

    private native void nativeSetMasterSyncAutomatically(long proxy, boolean flag);

    private native void nativeSetMasterSyncAutomaticallyAsUser(long proxy, boolean flag, int userId);

    private native boolean nativeGetMasterSyncAutomatically(long proxy);

    private native boolean nativeGetMasterSyncAutomaticallyAsUser(long proxy, int userId);

    private native boolean nativeIsSyncActive(long proxy, Account account, String authority, ComponentName cname);

    private native List<SyncInfo> nativeGetCurrentSyncs(long proxy);

    private native List<SyncInfo> nativeGetCurrentSyncsAsUser(long proxy, int userId);

    private native SyncAdapterType[] nativeGetSyncAdapterTypes(long proxy);

    private native SyncAdapterType[] nativeGetSyncAdapterTypesAsUser(long proxy, int userId);

    private native SyncStatusInfo nativeGetSyncStatus(long proxy, Account account, String authority, ComponentName cname);

    private native SyncStatusInfo nativeGetSyncStatusAsUser(long proxy, Account account, String authority, ComponentName cname, int userId);

    private native boolean nativeIsSyncPending(long proxy, Account account, String authority, ComponentName cname);

    private native boolean nativeIsSyncPendingAsUser(long proxy, Account account, String authority, ComponentName cname, int userId);

    private native void nativeAddStatusChangeListener(long proxy, int mask, ISyncStatusObserver callback);

    private native void nativeRemoveStatusChangeListener(long proxy, ISyncStatusObserver callback);

    private static final native void nativeFinalize(long nativeProxy);
    // ----- TODO:Update jni


    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElContentServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void registerContentObserver(Uri uri, boolean notifyForDescendants,
            IContentObserver observer, int userHandle) throws RemoteException {
        nativeRegisterContentObserver(mNativeProxy, uri, notifyForDescendants, observer, observer.hashCode(), userHandle);
    }

    @Override
    public void unregisterContentObserver(IContentObserver observer) throws RemoteException {
        nativeUnregisterContentObserver(mNativeProxy, observer.hashCode());
    }

    @Override
    public void notifyChange(Uri uri, IContentObserver observer,
            boolean observerWantsSelfNotifications, boolean syncToNetwork,
            int userHandle) throws RemoteException {
        nativeNotifyChange(mNativeProxy, uri, observer == null ? 0 : observer.hashCode(), observerWantsSelfNotifications,
                syncToNetwork, userHandle);
    }

    @Override
    public void requestSync(Account account, String authority, Bundle extras) throws RemoteException {
        nativeRequestSync(mNativeProxy, account, authority, extras);
    }

    @Override
    public void sync(SyncRequest request) throws RemoteException {
        nativeSync(mNativeProxy, request);
    }

    @Override
    public void syncAsUser(SyncRequest request, int userId) throws RemoteException {
        nativeSyncAsUser(mNativeProxy, request, userId);
    }

    @Override
    public void cancelSync(Account account, String authority, ComponentName cname) throws RemoteException {
        nativeCancelSync(mNativeProxy, account, authority, cname);
    }

    @Override
    public void cancelSyncAsUser(Account account, String authority, ComponentName cname, int userId) throws RemoteException {
        nativeCancelSyncAsUser(mNativeProxy, account, authority, cname, userId);
    }

    @Override
    public void cancelRequest(SyncRequest request) throws RemoteException {
        nativeCancelRequest(mNativeProxy, request);
    }

    @Override
    public boolean getSyncAutomatically(Account account, String providerName) throws RemoteException {
        return nativeGetSyncAutomatically(mNativeProxy, account, providerName);
    }

    @Override
    public boolean getSyncAutomaticallyAsUser(Account account, String providerName, int userId) throws RemoteException {
        return nativeGetSyncAutomaticallyAsUser(mNativeProxy, account, providerName, userId);
    }

    @Override
    public void setSyncAutomatically(Account account, String providerName, boolean sync) throws RemoteException {
        nativeSetSyncAutomatically(mNativeProxy, account, providerName, sync);
    }

    @Override
    public void setSyncAutomaticallyAsUser(Account account, String providerName, boolean sync, int userId) throws RemoteException {
        nativeSetSyncAutomaticallyAsUser(mNativeProxy, account, providerName, sync, userId);
    }

    @Override
    public List<PeriodicSync> getPeriodicSyncs(Account account, String providerName, ComponentName cname) throws RemoteException {
        return nativeGetPeriodicSyncs(mNativeProxy, account, providerName, cname);
    }

    @Override
    public void addPeriodicSync(Account account, String providerName, Bundle extras,
      long pollFrequency) throws RemoteException {
        nativeAddPeriodicSync(mNativeProxy, account, providerName, extras, pollFrequency);
    }

    @Override
    public void removePeriodicSync(Account account, String providerName, Bundle extras) throws RemoteException {
        nativeRemovePeriodicSync(mNativeProxy, account, providerName, extras);
    }

    @Override
    public int getIsSyncable(Account account, String providerName) throws RemoteException {
        return nativeGetIsSyncable(mNativeProxy, account, providerName);
    }

    @Override
    public int getIsSyncableAsUser(Account account, String providerName, int userId) throws RemoteException {
        return nativeGetIsSyncableAsUser(mNativeProxy, account, providerName, userId);
    }

    @Override
    public void setIsSyncable(Account account, String providerName, int syncable) throws RemoteException {
        nativeSetIsSyncable(mNativeProxy, account, providerName, syncable);
    }

    @Override
    public void setMasterSyncAutomatically(boolean flag) throws RemoteException {
        nativeSetMasterSyncAutomatically(mNativeProxy, flag);
    }

    @Override
    public void setMasterSyncAutomaticallyAsUser(boolean flag, int userId) throws RemoteException {
        nativeSetMasterSyncAutomaticallyAsUser(mNativeProxy, flag, userId);
    }

    @Override
    public boolean getMasterSyncAutomatically() throws RemoteException {
        return nativeGetMasterSyncAutomatically(mNativeProxy);
    }

    @Override
    public boolean getMasterSyncAutomaticallyAsUser(int userId) throws RemoteException {
        return nativeGetMasterSyncAutomaticallyAsUser(mNativeProxy, userId);
    }

    @Override
    public boolean isSyncActive(Account account, String authority, ComponentName cname) throws RemoteException {
        return nativeIsSyncActive(mNativeProxy, account, authority, cname);
    }

    @Override
    public List<SyncInfo> getCurrentSyncs() throws RemoteException {
        return nativeGetCurrentSyncs(mNativeProxy);
    }

    @Override
    public List<SyncInfo> getCurrentSyncsAsUser(int userId) throws RemoteException {
        return nativeGetCurrentSyncsAsUser(mNativeProxy, userId);
    }

    @Override
    public SyncAdapterType[] getSyncAdapterTypes() throws RemoteException {
        return nativeGetSyncAdapterTypes(mNativeProxy);
    }

    @Override
    public SyncAdapterType[] getSyncAdapterTypesAsUser(int userId) throws RemoteException {
        return nativeGetSyncAdapterTypesAsUser(mNativeProxy, userId);
    }

    @Override
    public SyncStatusInfo getSyncStatus(Account account, String authority, ComponentName cname) throws RemoteException {
        return nativeGetSyncStatus(mNativeProxy, account, authority, cname);
    }

    @Override
    public SyncStatusInfo getSyncStatusAsUser(Account account, String authority, ComponentName cname, int userId) throws RemoteException {
        return nativeGetSyncStatusAsUser(mNativeProxy, account, authority, cname, userId);
    }

    @Override
    public boolean isSyncPending(Account account, String authority, ComponentName cname) throws RemoteException {
        return nativeIsSyncPending(mNativeProxy, account, authority, cname);
    }

    @Override
    public boolean isSyncPendingAsUser(Account account, String authority, ComponentName cname, int userId) throws RemoteException {
        return nativeIsSyncPendingAsUser(mNativeProxy, account, authority, cname, userId);
    }

    @Override
    public void addStatusChangeListener(int mask, ISyncStatusObserver callback) throws RemoteException {
        nativeAddStatusChangeListener(mNativeProxy, mask, callback);
    }

    @Override
    public void removeStatusChangeListener(ISyncStatusObserver callback) throws RemoteException {
        nativeRemoveStatusChangeListener(mNativeProxy, callback);
    }
}
