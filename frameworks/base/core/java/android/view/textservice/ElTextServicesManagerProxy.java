
package android.view.textservice;

import com.android.internal.textservice.ISpellCheckerSessionListener;
import com.android.internal.textservice.ITextServicesManager;
import com.android.internal.textservice.ITextServicesSessionListener;

import android.content.ComponentName;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.textservice.SpellCheckerInfo;
import android.view.textservice.SpellCheckerSubtype;

/**
 * @hide
 */
public class ElTextServicesManagerProxy extends ITextServicesManager.Stub {
    private long mNativeProxy;

    private native boolean nativeIsSpellCheckerEnabled(long nativeProxy);

    private native SpellCheckerSubtype nativeGetCurrentSpellCheckerSubtype(long nativeProxy,
            String locale, boolean allowImplicitlySelectedSubtype);

    private native SpellCheckerInfo nativeGetCurrentSpellChecker(long nativeProxy, String locale);

    private native void nativeSetCurrentSpellChecker(long nativeProxy, String locale, String sciId);

    private native void nativeSetCurrentSpellCheckerSubtype(long nativeProxy, String locale, int hashCode);

    private native void nativeSetSpellCheckerEnabled(long nativeProxy, boolean enabled);

    private native SpellCheckerInfo[] nativeGetEnabledSpellCheckers(long nativeProxy);

    private native void nativeGetSpellCheckerService(long nativeProxy, String sciId, String locale,
            ITextServicesSessionListener tsListener, ISpellCheckerSessionListener scListener, Bundle bundle);

    private native void nativeFinishSpellCheckerService(long nativeProxy, ISpellCheckerSessionListener listener);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElTextServicesManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public boolean isSpellCheckerEnabled() throws RemoteException {
        return nativeIsSpellCheckerEnabled(mNativeProxy);
    }

    @Override
    public SpellCheckerSubtype getCurrentSpellCheckerSubtype(
            String locale, boolean allowImplicitlySelectedSubtype) throws RemoteException {
        return nativeGetCurrentSpellCheckerSubtype(mNativeProxy, locale, allowImplicitlySelectedSubtype);
    }

    @Override
    public SpellCheckerInfo getCurrentSpellChecker(String locale) throws RemoteException {
        return nativeGetCurrentSpellChecker(mNativeProxy, locale);
    }

    @Override
    public void setCurrentSpellChecker(String locale, String sciId) throws RemoteException {
        nativeSetCurrentSpellChecker(mNativeProxy, locale, sciId);
    }

    @Override
    public void setCurrentSpellCheckerSubtype(String locale, int hashCode) throws RemoteException {
        nativeSetCurrentSpellCheckerSubtype(mNativeProxy, locale, hashCode);
    }

    @Override
    public void setSpellCheckerEnabled(boolean enabled) throws RemoteException {
        nativeSetSpellCheckerEnabled(mNativeProxy, enabled);
    }

    @Override
    public SpellCheckerInfo[] getEnabledSpellCheckers() throws RemoteException {
        return nativeGetEnabledSpellCheckers(mNativeProxy);
    }

    @Override
    public void getSpellCheckerService(String sciId, String locale,
            ITextServicesSessionListener tsListener,
            ISpellCheckerSessionListener scListener, Bundle bundle) throws RemoteException {
        nativeGetSpellCheckerService(mNativeProxy, sciId, locale, tsListener, scListener, bundle);
    }

    @Override
    public void finishSpellCheckerService(ISpellCheckerSessionListener listener) throws RemoteException {
        nativeFinishSpellCheckerService(mNativeProxy, listener);
    }
}
