
package android.view;

import android.content.ClipData;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.IWindow;
import android.view.IWindowId;
import android.view.IWindowSession;
import android.view.InputChannel;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.WindowManager.LayoutParams;

/** {@hide} */
public final class ElWindowSessionProxy extends IWindowSession.Stub {

    private long mNativeProxy;

    private native int nativeAddToDisplay(long proxy, IWindow window, int windowHashCode, int seq, WindowManager.LayoutParams attrs,
            int viewVisibility, int displayId, Rect outContentInsets, InputChannel outInputChannel);

    private native int nativeRelayout(long proxy, int window, int seq, WindowManager.LayoutParams attrs, int requestedWidth, int requestedHeight,
            int viewFlags, int flags, Rect outFrame, Rect outOverscanInsets, Rect outContentInsets,Rect outVisibleInsets, Rect outStableInsets, Configuration outConfig, Surface outSurface);

    private native void nativeFinishDrawing(long proxy, int window);

    private native void nativeOnRectangleOnScreenRequested(long proxy, int window, Rect rectangle);

    private native void nativeRemove(long proxy, int window);

    private native boolean nativePerformHapticFeedback(long proxy, int window, int effectId, boolean always);

    private native void nativeSetWallpaperPosition(long proxy, int window, float x, float y, float xStep, float yStep);

    private native boolean nativeGetInTouchMode(long proxy);

    private native void nativeSetInTouchMode(long proxy, boolean showFocus);

    private native int nativeAddToDisplayWithoutInputChannel(long proxy, IWindow window, int windowHashCode, int seq, LayoutParams attrs,
                int viewVisibility, int displayId, Rect inContentInsets);

    private native void nativeSetTransparentRegion(long proxy, int window, Region region);

    private native void nativeGetDisplayFrame(long proxy, int window, Rect outDisplayFrame);

    private native void nativePerformDeferredDestroy(long proxy, int window);

    private native void nativeSetInsets(long proxy, int window, int touchable, Rect contentInsets,
            Rect visibleInsets, Region touchableRegion);

    private native Bundle nativeSendWallpaperCommand(long proxy, int window, String action, int x, int y,
            int z, Bundle extras, boolean sync);

    private native void nativeWallpaperOffsetsComplete(long proxy, int window);

    private native void nativeSetWallpaperDisplayOffset(long proxy, int windowToken, int x, int y);

    private native int nativeGetLastWallpaperX(long proxy);

    private native int nativeGetLastWallpaperY(long proxy);

    private native void nativeWallpaperCommandComplete(long proxy, int window, Bundle result);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElWindowSessionProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public int addToDisplay(IWindow window, int seq, WindowManager.LayoutParams attrs,
            int viewVisibility, int displayId, Rect outContentInsets,
            InputChannel outInputChannel) throws RemoteException {
        return nativeAddToDisplay(mNativeProxy, window, window.hashCode(), seq, attrs, viewVisibility, displayId, outContentInsets, outInputChannel);
    }

    @Override
    public int relayout(IWindow window, int seq, WindowManager.LayoutParams attrs, int requestedWidth, int requestedHeight,
            int viewFlags, int flags, Rect outFrame, Rect outOverscanInsets, Rect outContentInsets,Rect outVisibleInsets, Rect outStableInsets, Configuration outConfig, Surface outSurface)
        throws RemoteException {
        return nativeRelayout(mNativeProxy, window.hashCode(), seq, attrs, requestedWidth, requestedHeight, viewFlags, flags,
                outFrame, outOverscanInsets, outContentInsets, outVisibleInsets, outStableInsets, outConfig, outSurface);
    }

    @Override
    public void finishDrawing(IWindow window) throws RemoteException {
        nativeFinishDrawing(mNativeProxy, window.hashCode());
    }

    @Override
    public void onRectangleOnScreenRequested(IBinder window, Rect rectangle) {
        nativeOnRectangleOnScreenRequested(mNativeProxy, window.hashCode(), rectangle);
    }

    @Override
    public boolean performHapticFeedback(IWindow window, int effectId, boolean always) {
        return nativePerformHapticFeedback(mNativeProxy, window.hashCode(), effectId, always);
    }

    @Override
    public void remove(IWindow window) throws RemoteException {
        nativeRemove(mNativeProxy, window.hashCode());
    }

    @Override
    public void setWallpaperPosition(IBinder window, float x, float y, float xStep, float yStep) {
        nativeSetWallpaperPosition(mNativeProxy, window.hashCode(), x, y, xStep, yStep);
    }

    @Override
    public boolean getInTouchMode() throws RemoteException {
        return nativeGetInTouchMode(mNativeProxy);
    }

    @Override
    public void setInTouchMode(boolean showFocus) throws RemoteException {
        nativeSetInTouchMode(mNativeProxy, showFocus);
    }

    @Override
    public int addToDisplayWithoutInputChannel(IWindow window, int seq, LayoutParams attrs, int viewVisibility,
               int displayId, Rect inContentInsets) throws RemoteException {
        return nativeAddToDisplayWithoutInputChannel(mNativeProxy, window, window.hashCode(), seq, attrs, viewVisibility, displayId, inContentInsets);
    }

    @Override
    public void setTransparentRegion(IWindow window, Region region) throws RemoteException {
        nativeSetTransparentRegion(mNativeProxy, window.hashCode(), region);
    }

    @Override
    public void getDisplayFrame(IWindow window, Rect outDisplayFrame) {
        nativeGetDisplayFrame(mNativeProxy, window.hashCode(), outDisplayFrame);
    }

    @Override
    public void performDeferredDestroy(IWindow window) {
        nativePerformDeferredDestroy(mNativeProxy, window.hashCode());
    }

    @Override
    public void setInsets(IWindow window, int touchable, Rect contentInsets,
            Rect visibleInsets, Region touchableRegion) {
        nativeSetInsets(mNativeProxy, window.hashCode(), touchable, contentInsets, visibleInsets, touchableRegion);
    }

    @Override
    public Bundle sendWallpaperCommand(IBinder window, String action, int x, int y,
            int z, Bundle extras, boolean sync) {
        return nativeSendWallpaperCommand(mNativeProxy, window.hashCode(), action, x, y, z, extras, sync);
    }

    @Override
    public void wallpaperOffsetsComplete(IBinder window) {
        nativeWallpaperOffsetsComplete(mNativeProxy, window.hashCode());
    }

    @Override
    public void setWallpaperDisplayOffset(IBinder windowToken, int x, int y) {
        nativeSetWallpaperDisplayOffset(mNativeProxy, windowToken.hashCode(), x, y);
    }

    @Override
    public int getLastWallpaperX() {
        return nativeGetLastWallpaperX(mNativeProxy);
    }

    @Override
    public int getLastWallpaperY() {
        return nativeGetLastWallpaperY(mNativeProxy);
    }

    @Override
    public void wallpaperCommandComplete(IBinder window, Bundle result) {
        nativeWallpaperCommandComplete(mNativeProxy, window.hashCode(), result);
    }

    @Override
    public int add(IWindow arg0, int seq, LayoutParams arg1, int arg2, Rect arg3,
            InputChannel outInputchannel) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public int addWithoutInputChannel(IWindow arg0, int seq, LayoutParams arg1, int arg2,
                                      Rect arg3) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public boolean outOfMemory(IWindow window) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public IBinder prepareDrag(IWindow window, int flags,
            int thumbnailWidth, int thumbnailHeight, Surface outSurface)
            throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public boolean performDrag(IWindow window, IBinder dragToken,
            float touchX, float touchY, float thumbCenterX, float thumbCenterY,
            ClipData data)
            throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void reportDropResult(IWindow window, boolean consumed) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void dragRecipientEntered(IWindow window) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void dragRecipientExited(IWindow window) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void setUniverseTransform(IBinder window, float alpha, float offx, float offy,
            float dsdx, float dtdx, float dsdy, float dtdy) {
        Log.e("ElWindowSessionProxy", "setUniverseTransform()");
    }

    @Override
    public IWindowId getWindowId(IBinder window) throws RemoteException {
        RemoteException e = new RemoteException();
        // e.printStackTrace();
        throw e;
    }

    @Override
    public IBinder asBinder() {
        Log.e("ElWindowSessionProxy", "asBinder()");
        return null;
    }
}
