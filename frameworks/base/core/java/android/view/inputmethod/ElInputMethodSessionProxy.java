
package android.view.inputmethod;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodSession;

import android.util.Log;

public class ElInputMethodSessionProxy implements InputMethodSession {
    private long mNativeProxy;

    private native void nativeSetEnabled(long proxy, boolean enabled);

    private static final native void nativeFinalize(long nativeProxy);

    private static final native void nativeFinishInput(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElInputMethodSessionProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    public void setEnabled(boolean enabled) {
        nativeSetEnabled(mNativeProxy, enabled);
    }

    @Override
    public void finishInput() {
        nativeFinishInput(mNativeProxy);
    }

    @Override
    public void updateSelection(int oldSelStart, int oldSelEnd,
            int newSelStart, int newSelEnd,
            int candidatesStart, int candidatesEnd) {
        Log.e("ElInputMethodSessionProxy", "updateSelection() E_NOT_IMPLEMENTED");
    }

    @Override
    public void viewClicked(boolean focusChanged) {
        Log.e("ElInputMethodSessionProxy", "viewClicked() E_NOT_IMPLEMENTED");
    }

    @Override
    public void updateCursor(Rect newCursor) {
        Log.e("ElInputMethodSessionProxy", "updateCursor() E_NOT_IMPLEMENTED");
    }

    @Override
    public void displayCompletions(CompletionInfo[] completions) {
        Log.e("ElInputMethodSessionProxy", "displayCompletions() E_NOT_IMPLEMENTED");
    }

    @Override
    public void updateExtractedText(int token, ExtractedText text) {
        Log.e("ElInputMethodSessionProxy", "updateExtractedText() E_NOT_IMPLEMENTED");
    }

    @Override
    public void dispatchKeyEvent(int seq, KeyEvent event, EventCallback callback) {
        Log.e("ElInputMethodSessionProxy", "dispatchKeyEvent() E_NOT_IMPLEMENTED");
    }

    @Override
    public void dispatchTrackballEvent(int seq, MotionEvent event, EventCallback callback) {
        Log.e("ElInputMethodSessionProxy", "dispatchTrackballEvent() E_NOT_IMPLEMENTED");
    }

    @Override
    public void dispatchGenericMotionEvent(int seq, MotionEvent event, EventCallback callback) {
        Log.e("ElInputMethodSessionProxy", "dispatchGenericMotionEvent() E_NOT_IMPLEMENTED");
    }

    @Override
    public void appPrivateCommand(String action, Bundle data) {
        Log.e("ElInputMethodSessionProxy", "appPrivateCommand() E_NOT_IMPLEMENTED");
    }

    @Override
    public void toggleSoftInput(int showFlags, int hideFlags) {
        Log.e("ElInputMethodSessionProxy", "toggleSoftInput() E_NOT_IMPLEMENTED");
    }

    @Override
    public void updateCursorAnchorInfo(CursorAnchorInfo cursorAnchorInfo) {
        Log.e("ElInputMethodSessionProxy", "updateCursorAnchorInfo() E_NOT_IMPLEMENTED");
    }
}
