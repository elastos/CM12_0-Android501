
package android.view.inputmethod;


import android.os.IBinder;
import android.os.ResultReceiver;
import android.os.RemoteException;
import android.util.Log;
import android.text.style.SuggestionSpan;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodSubtype;
import android.view.inputmethod.EditorInfo;
import com.android.internal.view.InputBindResult;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethodClient;
import com.android.internal.view.IInputMethodManager;

import java.util.List;

/** {@hide} */
public class ElInputMethodManagerProxy extends IInputMethodManager.Stub {
    private long mNativeProxy;

    private native InputBindResult nativeWindowGainedFocus(long proxy, int client, IBinder windowToken,
            int controlFlags, int softInputMode, int windowFlags, EditorInfo attribute, IInputContext inputContext);

    private native InputBindResult nativeStartInput(long proxy, int client, IInputContext inputContext, EditorInfo attribute, int controlFlags);

    private native void nativeFinishInput(long proxy, int client);

    private native void nativeHideMySoftInput(long proxy, IBinder token, int flags);

    private native List<InputMethodInfo> nativeGetInputMethodList(long proxy);

    private native boolean nativeShowSoftInput(long proxy, int client, int flags, ResultReceiver resultReceiver);

    private native boolean nativeHideSoftInput(long proxy, int client, int flags, ResultReceiver resultReceiver);

    private native List<InputMethodInfo> nativeGetEnabledInputMethodList(long proxy);

    private native void nativeShowInputMethodPickerFromClient(long proxy, int client);

    private native void nativeSetImeWindowStatus(long proxy, IBinder token, int vis, int backDisposition);

    private native void nativeUpdateStatusIcon(long proxy, IBinder token, String packageName, int iconId);

    private native List<InputMethodSubtype> nativeGetEnabledInputMethodSubtypeList(long proxy, String imiId,
            boolean allowsImplicitlySelectedSubtypes);

    private native InputMethodSubtype nativeGetLastInputMethodSubtype(long proxy);

    private native List nativeGetShortcutInputMethodsAndSubtypes(long proxy);

    private native void nativeAddClient(long proxy, IInputMethodClient client, IInputContext inputContext, int uid, int pid);

    private native void nativeRemoveClient(long proxy, IInputMethodClient client);

    private native void nativeShowInputMethodAndSubtypeEnablerFromClient(long proxy, IInputMethodClient client, String topId);

    private native void nativeSetInputMethod(long proxy, IBinder token, String id);

    private native void nativeSetInputMethodAndSubtype(long proxy, IBinder token, String id, InputMethodSubtype subtype);

    private native void nativeShowMySoftInput(long proxy, IBinder token, int flags);

    private native void nativeRegisterSuggestionSpansForNotification(long proxy, SuggestionSpan[] spans);

    private native boolean nativeNotifySuggestionPicked(long proxy, SuggestionSpan span, String originalString, int index);

    private native InputMethodSubtype nativeGetCurrentInputMethodSubtype(long proxy);

    private native boolean nativeSetCurrentInputMethodSubtype(long proxy, InputMethodSubtype subtype);

    private native boolean nativeSwitchToLastInputMethod(long proxy, IBinder token);

    private native boolean nativeSwitchToNextInputMethod(long proxy, IBinder token, boolean onlyCurrentIme);

    private native boolean nativeShouldOfferSwitchingToNextInputMethod(long proxy, IBinder token);

    private native boolean nativeSetInputMethodEnabled(long proxy, String id, boolean enabled);

    private native void nativeSetAdditionalInputMethodSubtypes(long proxy, String id, InputMethodSubtype[] subtypes);

    private native int nativeGetInputMethodWindowVisibleHeight(long proxy);

    private native void nativeNotifyUserAction(long proxy, int sequenceNumber);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElInputMethodManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public InputBindResult windowGainedFocus(IInputMethodClient client, IBinder windowToken,
            int controlFlags, int softInputMode, int windowFlags,
            EditorInfo attribute, IInputContext inputContext) throws RemoteException {
        return nativeWindowGainedFocus(mNativeProxy, client == null ? 0 : client.hashCode(),
            windowToken, controlFlags, softInputMode, windowFlags, attribute, inputContext);
    }

    @Override
    public InputBindResult startInput(IInputMethodClient client,
            IInputContext inputContext, EditorInfo attribute, int controlFlags) throws RemoteException {
        return nativeStartInput(mNativeProxy, client.hashCode(), inputContext, attribute, controlFlags);
    }

    @Override
    public void finishInput(IInputMethodClient client) throws RemoteException {
        nativeFinishInput(mNativeProxy, client.hashCode());
    }

    @Override
    public void hideMySoftInput(IBinder token, int flags) throws RemoteException {
        nativeHideMySoftInput(mNativeProxy, token, flags);
    }

    @Override
    public List<InputMethodInfo> getInputMethodList() throws RemoteException {
        return nativeGetInputMethodList(mNativeProxy);
    }

    @Override
    public boolean showSoftInput(IInputMethodClient client, int flags,
            ResultReceiver resultReceiver) throws RemoteException {
        return nativeShowSoftInput(mNativeProxy, client.hashCode(), flags, resultReceiver);
    }

    @Override
    public boolean hideSoftInput(IInputMethodClient client, int flags,
            ResultReceiver resultReceiver) throws RemoteException {
        return nativeHideSoftInput(mNativeProxy, client.hashCode(), flags, resultReceiver);
    }

    @Override
    public List<InputMethodInfo> getEnabledInputMethodList() throws RemoteException {
        return nativeGetEnabledInputMethodList(mNativeProxy);
    }

    @Override
    public void showInputMethodPickerFromClient(IInputMethodClient client) throws RemoteException {
        nativeShowInputMethodPickerFromClient(mNativeProxy, client.hashCode());
    }

    @Override
    public void setImeWindowStatus(IBinder token, int vis, int backDisposition) throws RemoteException {
        nativeSetImeWindowStatus(mNativeProxy, token, vis, backDisposition);
    }

    @Override
    public void updateStatusIcon(IBinder token, String packageName, int iconId) throws RemoteException {
        nativeUpdateStatusIcon(mNativeProxy, token, packageName, iconId);
    }

    @Override
    public List<InputMethodSubtype> getEnabledInputMethodSubtypeList(String imiId,
            boolean allowsImplicitlySelectedSubtypes) throws RemoteException {
        return nativeGetEnabledInputMethodSubtypeList(mNativeProxy, imiId, allowsImplicitlySelectedSubtypes);
    }

    @Override
    public InputMethodSubtype getLastInputMethodSubtype() throws RemoteException {
        return nativeGetLastInputMethodSubtype(mNativeProxy);
    }

    @Override
    public List getShortcutInputMethodsAndSubtypes() throws RemoteException {
        return nativeGetShortcutInputMethodsAndSubtypes(mNativeProxy);
    }

    @Override
    public void addClient(IInputMethodClient client, IInputContext inputContext, int uid, int pid) throws RemoteException {
        nativeAddClient(mNativeProxy, client, inputContext, uid, pid);
    }

    @Override
    public void removeClient(IInputMethodClient client) throws RemoteException {
        nativeRemoveClient(mNativeProxy, client);
    }

    @Override
    public void showInputMethodAndSubtypeEnablerFromClient(IInputMethodClient client, String topId) throws RemoteException {
        nativeShowInputMethodAndSubtypeEnablerFromClient(mNativeProxy, client, topId);
    }

    @Override
    public void setInputMethod(IBinder token, String id) throws RemoteException {
        nativeSetInputMethod(mNativeProxy, token, id);
    }

    @Override
    public void setInputMethodAndSubtype(IBinder token, String id, InputMethodSubtype subtype) throws RemoteException {
        nativeSetInputMethodAndSubtype(mNativeProxy, token, id, subtype);
    }

    @Override
    public void showMySoftInput(IBinder token, int flags) throws RemoteException {
        nativeShowMySoftInput(mNativeProxy, token, flags);
    }

    @Override
    public void registerSuggestionSpansForNotification(SuggestionSpan[] spans) throws RemoteException {
        nativeRegisterSuggestionSpansForNotification(mNativeProxy, spans);
    }

    @Override
    public boolean notifySuggestionPicked(SuggestionSpan span, String originalString, int index) throws RemoteException {
        return nativeNotifySuggestionPicked(mNativeProxy, span, originalString, index);
    }

    @Override
    public InputMethodSubtype getCurrentInputMethodSubtype() throws RemoteException {
        return nativeGetCurrentInputMethodSubtype(mNativeProxy);
    }

    @Override
    public boolean setCurrentInputMethodSubtype(InputMethodSubtype subtype) throws RemoteException {
        return nativeSetCurrentInputMethodSubtype(mNativeProxy, subtype);
    }

    @Override
    public boolean switchToLastInputMethod(IBinder token) throws RemoteException {
        return nativeSwitchToLastInputMethod(mNativeProxy, token);
    }

    @Override
    public boolean switchToNextInputMethod(IBinder token, boolean onlyCurrentIme) throws RemoteException {
        return nativeSwitchToNextInputMethod(mNativeProxy, token, onlyCurrentIme);
    }

    @Override
    public boolean shouldOfferSwitchingToNextInputMethod(IBinder token) throws RemoteException {
        return nativeShouldOfferSwitchingToNextInputMethod(mNativeProxy, token);
    }

    @Override
    public boolean setInputMethodEnabled(String id, boolean enabled) throws RemoteException {
        return nativeSetInputMethodEnabled(mNativeProxy, id, enabled);
    }

    @Override
    public void setAdditionalInputMethodSubtypes(String id, InputMethodSubtype[] subtypes) throws RemoteException {
        nativeSetAdditionalInputMethodSubtypes(mNativeProxy, id, subtypes);
    }

    @Override
    public int getInputMethodWindowVisibleHeight() throws RemoteException {
        return nativeGetInputMethodWindowVisibleHeight(mNativeProxy);
    }

    @Override
    public void notifyUserAction(int sequenceNumber) throws RemoteException {
        nativeNotifyUserAction(mNativeProxy, sequenceNumber);
    }
}
