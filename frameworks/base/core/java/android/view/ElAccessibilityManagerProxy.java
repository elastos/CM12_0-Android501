
package android.view;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.IAccessibilityServiceConnection;
import android.accessibilityservice.IAccessibilityServiceClient;
import android.content.ComponentName;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.IAccessibilityInteractionConnection;
import android.view.accessibility.IAccessibilityManager;
import android.view.accessibility.IAccessibilityManagerClient;
import android.view.IWindow;
import java.util.List;


/** {@hide} */
public class ElAccessibilityManagerProxy extends IAccessibilityManager.Stub {
    private long mNativeProxy;

    // private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            // nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElAccessibilityManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public int addClient(IAccessibilityManagerClient client, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public boolean sendAccessibilityEvent(AccessibilityEvent uiEvent, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList(int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int feedbackType, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void interrupt(int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public int addAccessibilityInteractionConnection(IWindow windowToken,
        IAccessibilityInteractionConnection connection, int userId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void removeAccessibilityInteractionConnection(IWindow windowToken) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void registerUiTestAutomationService(IBinder owner, IAccessibilityServiceClient client,
        AccessibilityServiceInfo info) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void unregisterUiTestAutomationService(IAccessibilityServiceClient client) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void temporaryEnableAccessibilityStateUntilKeyguardRemoved(ComponentName service,
            boolean touchExplorationEnabled) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public IBinder getWindowToken(int windowId) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }
}
