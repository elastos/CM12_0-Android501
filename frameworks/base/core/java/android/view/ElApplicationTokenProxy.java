
package android.view;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

class ElApplicationTokenProxy extends IApplicationToken.Stub {
    private long mNativeProxy;

    public ElApplicationTokenProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    private native void nativeFinalize(long nativeProxy);

    @Override public void windowsDrawn() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override public void windowsVisible() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override public void windowsGone() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override public boolean keyDispatchingTimedOut(String reason) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override public long getKeyDispatchingTimeout() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public IBinder asBinder() {
        Log.e("ElApplicationTokenProxy", "asBinder()");
        return null;
    }
}
