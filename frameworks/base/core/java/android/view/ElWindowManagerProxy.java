
package android.view;

import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethodClient;

import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IRemoteCallback;
import android.os.RemoteException;
import android.view.IApplicationToken;
import android.view.IOnKeyguardExitResult;
import android.view.IRotationWatcher;
import android.view.IWindowSession;
import android.view.IWindowSessionCallback;
import android.view.KeyEvent;
import android.view.InputEvent;
import android.view.MagnificationSpec;
import android.view.MotionEvent;
import android.view.InputChannel;
import android.view.InputDevice;
import android.view.IInputFilter;
import android.view.WindowContentFrameStats;

/** {@hide} */
public class ElWindowManagerProxy extends IWindowManager.Stub {

    private long mNativeProxy;

    private native IWindowSession nativeOpenSession(long nativeProxy, IWindowSessionCallback callback, IInputMethodClient client, int clientHashCode, IInputContext context, int contextHashCode);

    private native boolean nativeHasNavigationBar(long nativeProxy);

    private native boolean nativeHasPermanentMenuKey(long nativeProxy);

    private native boolean nativeNeedsNavigationBar(long nativeProxy);

    private native float nativeGetAnimationScale(long nativeProxy, int which);

    private native boolean nativeInKeyguardRestrictedInputMode(long nativeProxy);

    private native int nativeGetRotation(long nativeProxy);

    private native void nativeAddAppToken(long nativeProxy, int addPos, IApplicationToken token, int groupId, int stackId,
            int requestedOrientation, boolean fullscreen, boolean showWhenLocked, int userId,
            int configChanges, boolean voiceInteraction, boolean launchTaskBehind);

    private native void nativeAddWindowToken(long nativeProxy, IBinder token, int type);

    private native void nativeClearForcedDisplaySize(long nativeProxy, int displayId);

    private native void nativeClearForcedDisplayDensity(long nativeProxy, int displayId);

    private native void nativeSetOverscan(long nativeProxy, int displayId, int left, int top, int right, int bottom);

    private native void nativeCloseSystemDialogs(long nativeProxy, String reason);

    private native void nativeStartFreezingScreen(long nativeProxy, int exitAnim, int enterAnim);

    private native void nativeStopFreezingScreen(long nativeProxy);

    private native void nativeDisableKeyguard(long nativeProxy, IBinder token, String tag);

    private native void nativeExecuteAppTransition(long nativeProxy);

    private native void nativeExitKeyguardSecurely(long nativeProxy, IOnKeyguardExitResult callback);

    private native void nativeFreezeRotation(long nativeProxy, int rotation);

    private native float[] nativeGetAnimationScales(long nativeProxy);

    private native int nativeGetAppOrientation(long nativeProxy, IApplicationToken token);

    private native int nativeGetPendingAppTransition(long nativeProxy);

    private native boolean nativeInputMethodClientHasFocus(long nativeProxy, IInputMethodClient client);

    private native boolean nativeIsKeyguardLocked(long nativeProxy);

    private native boolean nativeIsKeyguardSecure(long nativeProxy);

    private native boolean nativeIsViewServerRunning(long nativeProxy);

    private native void nativeOverridePendingAppTransition(long nativeProxy, String packageName,
            int enterAnim, int exitAnim, IRemoteCallback startedCallback);

    private native void nativeOverridePendingAppTransitionScaleUp(long nativeProxy, int startX, int startY,
            int startWidth, int startHeight);

    private native void nativeOverridePendingAppTransitionThumb(long nativeProxy, Bitmap srcThumb, int startX, int startY,
            IRemoteCallback startedCallback, boolean scaleUp);

    private native void nativeOverridePendingAppTransitionAspectScaledThumb(long nativeProxy, Bitmap srcThumb, int startX,
            int startY, int targetWidth, int targetHeight, IRemoteCallback startedCallback,
            boolean scaleUp);

    private native void nativePauseKeyDispatching(long nativeProxy, IBinder _token);

    private native void nativePrepareAppTransition(long nativeProxy, int transit, boolean alwaysKeepCurrent);

    private native void nativeReenableKeyguard(long nativeProxy, IBinder token);

    private native void nativeRemoveAppToken(long nativeProxy, IBinder token);

    private native void nativeRemoveWindowToken(long nativeProxy, IBinder token);

    private native void nativeResumeKeyDispatching(long nativeProxy, IBinder _token);

    private native boolean nativeIsRotationFrozen(long nativeProxy);

    private native Bitmap nativeScreenshotApplications(long nativeProxy, IBinder appToken, int displayId,
            int width, int height, boolean force565);

    private native void nativeSetAnimationScale(long nativeProxy, int which, float scale);

    private native void nativeSetAnimationScales(long nativeProxy, float[] scales);

    private native float nativeGetCurrentAnimatorScale(long nativeProxy);

    private native void nativeSetAppGroupId(long nativeProxy, IBinder token, int groupId);

    private native void nativeSetAppOrientation(long nativeProxy, IApplicationToken token, int requestedOrientation);

    private native void nativeSetAppStartingWindow(long nativeProxy, IBinder token, String pkg, int theme,
            CompatibilityInfo compatInfo, CharSequence nonLocalizedLabel, int labelRes, int icon, int logo,
            int windowFlags, IBinder transferFrom, boolean createIfNeeded);

    private native void nativeSetAppVisibility(long nativeProxy, IBinder token, boolean visible);

    private native void nativeSetAppWillBeHidden(long nativeProxy, IBinder token);

    private native void nativeSetEventDispatching(long nativeProxy, boolean enabled);

    private native void nativeSetFocusedApp(long nativeProxy, IBinder token, boolean moveFocusNow);

    private native void nativeGetInitialDisplaySize(long nativeProxy, int displayId, Point size);

    private native void nativeGetBaseDisplaySize(long nativeProxy, int displayId, Point size);

    private native int nativeGetInitialDisplayDensity(long nativeProxy, int displayId);

    private native int nativeGetBaseDisplayDensity(long nativeProxy, int displayId);

    private native void nativeSetForcedDisplaySize(long nativeProxy, int displayId, int width, int height) ;

    private native void nativeSetForcedDisplayDensity(long nativeProxy, int displayId, int density);

    private native void nativeSetInTouchMode(long nativeProxy, boolean mode);

    private native void nativeSetNewConfiguration(long nativeProxy, Configuration config);

    private native void nativeUpdateRotation(long nativeProxy, boolean alwaysSendConfiguration, boolean forceRelayout);

    private native void nativeSetStrictModeVisualIndicatorPreference(long nativeProxy, String value);

    private native void nativeSetScreenCaptureDisabled(long nativeProxy, int userId, boolean disabled);

    private native void nativeShowStrictModeViolation(long nativeProxy, boolean on);

    private native void nativeStartAppFreezingScreen(long nativeProxy, IBinder token, int configChanges);

    private native boolean nativeStartViewServer(long nativeProxy, int port);

    private native int nativeGetLastWallpaperX(long nativeProxy);

    private native int nativeGetLastWallpaperY(long nativeProxy);

    private native void nativeStatusBarVisibilityChanged(long nativeProxy, int visibility);

    private native void nativeStopAppFreezingScreen(long nativeProxy, IBinder token, boolean force);

    private native boolean nativeStopViewServer(long nativeProxy);

    private native void nativeThawRotation(long nativeProxy);

    private native Configuration nativeUpdateOrientationFromAppTokens(long nativeProxy, Configuration currentConfig,
            IBinder freezeThisOneIfNeeded);

    private native int nativeWatchRotation(long nativeProxy, IRotationWatcher watcher);

    private native void nativeRemoveRotationWatcher(long nativeProxy, IRotationWatcher watcher);

    private native int nativeGetPreferredOptionsPanelGravity(long nativeProxy);

    private native void nativeDismissKeyguard(long nativeProxy);

    private native void nativeKeyguardGoingAway(long nativeProxy, boolean disableWindowAnimations,
            boolean keyguardGoingToNotificationShade, boolean keyguardShowingMedia);

    private native void nativeLockNow(long nativeProxy, Bundle options);

    private native boolean nativeIsSafeModeEnabled(long nativeProxy);

    private native void nativeEnableScreenIfNeeded(long nativeProxy);

    private native boolean nativeClearWindowContentFrameStats(long nativeProxy, IBinder token);

    private native WindowContentFrameStats nativeGetWindowContentFrameStats(long nativeProxy, IBinder token);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElWindowManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public IWindowSession openSession(IWindowSessionCallback callback, IInputMethodClient client, IInputContext context)
            throws RemoteException {
        return nativeOpenSession(mNativeProxy, callback, client, client.hashCode(), context, context.hashCode());
    }

    @Override
    public float getAnimationScale(int which) throws RemoteException {
        return nativeGetAnimationScale(mNativeProxy, which);
    }

    @Override
    public boolean hasNavigationBar() throws RemoteException {
        return nativeHasNavigationBar(mNativeProxy);
    }

    @Override
    public boolean hasPermanentMenuKey() throws RemoteException {
        return nativeHasPermanentMenuKey(mNativeProxy);
    }

    @Override
    public boolean needsNavigationBar() throws RemoteException {
        return nativeNeedsNavigationBar(mNativeProxy);
    }

    @Override
    public boolean inKeyguardRestrictedInputMode() throws RemoteException {
        return nativeInKeyguardRestrictedInputMode(mNativeProxy);
    }

    @Override
    public int getRotation() throws RemoteException {
        return nativeGetRotation(mNativeProxy);
    }

    @Override
    public void addAppToken(int addPos, IApplicationToken token, int groupId, int stackId,
            int requestedOrientation, boolean fullscreen, boolean showWhenLocked, int userId,
            int configChanges, boolean voiceInteraction, boolean launchTaskBehind) throws RemoteException {
        nativeAddAppToken(mNativeProxy, addPos, token, groupId, stackId, requestedOrientation, fullscreen, showWhenLocked,
            userId, configChanges, voiceInteraction, launchTaskBehind);
    }

    @Override
    public void addWindowToken(IBinder token, int type) throws RemoteException {
        nativeAddWindowToken(mNativeProxy, token, type);
    }

    @Override
    public void clearForcedDisplaySize(int displayId) throws RemoteException {
        nativeClearForcedDisplaySize(mNativeProxy, displayId);
    }

    @Override
    public void clearForcedDisplayDensity(int displayId) throws RemoteException {
        nativeClearForcedDisplayDensity(mNativeProxy, displayId);
    }

    @Override
    public void setOverscan(int displayId, int left, int top, int right, int bottom) throws RemoteException {
        nativeSetOverscan(mNativeProxy, displayId, left, top, right, bottom);
    }

    @Override
    public void closeSystemDialogs(String reason) throws RemoteException {
        nativeCloseSystemDialogs(mNativeProxy, reason);
    }

    @Override
    public void startFreezingScreen(int exitAnim, int enterAnim) {
        nativeStartFreezingScreen(mNativeProxy, exitAnim, enterAnim);
    }

    @Override
    public void stopFreezingScreen() {
        nativeStopFreezingScreen(mNativeProxy);
    }

    @Override
    public void disableKeyguard(IBinder token, String tag) throws RemoteException {
        nativeDisableKeyguard(mNativeProxy, token, tag);

    }

    @Override
    public void executeAppTransition() throws RemoteException {
        nativeExecuteAppTransition(mNativeProxy);
    }

    @Override
    public void exitKeyguardSecurely(IOnKeyguardExitResult callback) throws RemoteException {
        nativeExitKeyguardSecurely(mNativeProxy, callback);
    }

    @Override
    public void freezeRotation(int rotation) throws RemoteException {
        nativeFreezeRotation(mNativeProxy, rotation);
    }

    @Override
    public float[] getAnimationScales() throws RemoteException {
        return nativeGetAnimationScales(mNativeProxy);
    }

    @Override
    public int getAppOrientation(IApplicationToken token) throws RemoteException {
        return nativeGetAppOrientation(mNativeProxy, token);
    }

    @Override
    public int getPendingAppTransition() throws RemoteException {
        return nativeGetPendingAppTransition(mNativeProxy);
    }

    @Override
    public boolean inputMethodClientHasFocus(IInputMethodClient client) throws RemoteException {
        return nativeInputMethodClientHasFocus(mNativeProxy, client);
    }

    @Override
    public boolean isKeyguardLocked() throws RemoteException {
        return nativeIsKeyguardLocked(mNativeProxy);
    }

    @Override
    public boolean isKeyguardSecure() throws RemoteException {
        return nativeIsKeyguardSecure(mNativeProxy);
    }

    @Override
    public boolean isViewServerRunning() throws RemoteException {
        return nativeIsViewServerRunning(mNativeProxy);
    }

    @Override
    public void overridePendingAppTransition(String packageName, int enterAnim, int exitAnim,
            IRemoteCallback startedCallback) throws RemoteException {
        nativeOverridePendingAppTransition(mNativeProxy, packageName, enterAnim, exitAnim, startedCallback);
    }

    @Override
    public void overridePendingAppTransitionScaleUp(int startX, int startY, int startWidth,
            int startHeight) throws RemoteException {
        nativeOverridePendingAppTransitionScaleUp(mNativeProxy, startX, startY, startWidth, startHeight);
    }

    @Override
    public void overridePendingAppTransitionThumb(Bitmap srcThumb, int startX, int startY,
            IRemoteCallback startedCallback, boolean scaleUp) throws RemoteException {
        nativeOverridePendingAppTransitionThumb(mNativeProxy, srcThumb, startY, startY, startedCallback, scaleUp);
    }

    @Override
    public void overridePendingAppTransitionAspectScaledThumb(Bitmap srcThumb, int startX,
            int startY, int targetWidth, int targetHeight, IRemoteCallback startedCallback,
            boolean scaleUp) throws RemoteException {
        nativeOverridePendingAppTransitionAspectScaledThumb(mNativeProxy, srcThumb, startX, startY, targetWidth, targetHeight,
            startedCallback, scaleUp);
    }

    @Override
    public void pauseKeyDispatching(IBinder _token) throws RemoteException {
        nativePauseKeyDispatching(mNativeProxy, _token);
    }

    @Override
    public void prepareAppTransition(int transit, boolean alwaysKeepCurrent) throws RemoteException {
        nativePrepareAppTransition(mNativeProxy, transit, alwaysKeepCurrent);
    }

    @Override
    public void reenableKeyguard(IBinder token) throws RemoteException {
        nativeReenableKeyguard(mNativeProxy, token);
    }

    @Override
    public void removeAppToken(IBinder token) throws RemoteException {
        nativeRemoveAppToken(mNativeProxy, token);
    }

    @Override
    public void removeWindowToken(IBinder token) throws RemoteException {
        nativeRemoveWindowToken(mNativeProxy, token);
    }

    @Override
    public void resumeKeyDispatching(IBinder _token) throws RemoteException {
        nativeResumeKeyDispatching(mNativeProxy, _token);
    }

    @Override
    public boolean isRotationFrozen() throws RemoteException {
        return nativeIsRotationFrozen(mNativeProxy);
    }

    @Override
    public Bitmap screenshotApplications(IBinder appToken, int displayId, int width, int height, boolean force565) throws RemoteException {
        return nativeScreenshotApplications(mNativeProxy, appToken, displayId, width, height, force565);
    }

    @Override
    public void setAnimationScale(int which, float scale) throws RemoteException {
        nativeSetAnimationScale(mNativeProxy, which, scale);
    }

    @Override
    public void setAnimationScales(float[] scales) throws RemoteException {
        nativeSetAnimationScales(mNativeProxy, scales);
    }

    @Override
    public float getCurrentAnimatorScale() throws RemoteException {
        return nativeGetCurrentAnimatorScale(mNativeProxy);
    }

    @Override
    public void setAppGroupId(IBinder token, int groupId) throws RemoteException {
        nativeSetAppGroupId(mNativeProxy, token, groupId);
    }

    @Override
    public void setAppOrientation(IApplicationToken token, int requestedOrientation) throws RemoteException {
        nativeSetAppOrientation(mNativeProxy, token, requestedOrientation);
    }

    @Override
    public void setAppStartingWindow(IBinder token, String pkg, int theme,
            CompatibilityInfo compatInfo, CharSequence nonLocalizedLabel, int labelRes, int icon,
            int logo, int windowFlags, IBinder transferFrom, boolean createIfNeeded) throws RemoteException {
        nativeSetAppStartingWindow(mNativeProxy, token, pkg, theme, compatInfo, nonLocalizedLabel,
            labelRes, icon, logo, windowFlags, transferFrom, createIfNeeded);
    }

    @Override
    public void setAppVisibility(IBinder token, boolean visible) throws RemoteException {
        nativeSetAppVisibility(mNativeProxy, token, visible);
    }

    @Override
    public void setAppWillBeHidden(IBinder token) throws RemoteException {
        nativeSetAppWillBeHidden(mNativeProxy, token);
    }

    @Override
    public void setEventDispatching(boolean enabled) throws RemoteException {
        nativeSetEventDispatching(mNativeProxy, enabled);
    }

    @Override
    public void setFocusedApp(IBinder token, boolean moveFocusNow) throws RemoteException {
        nativeSetFocusedApp(mNativeProxy, token, moveFocusNow);
    }

    @Override
    public void getInitialDisplaySize(int displayId, Point size) throws RemoteException {
        nativeGetInitialDisplaySize(mNativeProxy, displayId, size);
    }

    @Override
    public void getBaseDisplaySize(int displayId, Point size) throws RemoteException {
        nativeGetBaseDisplaySize(mNativeProxy, displayId, size);
    }

    @Override
    public int getInitialDisplayDensity(int displayId) throws RemoteException {
        return nativeGetInitialDisplayDensity(mNativeProxy, displayId);
    }

    @Override
    public int getBaseDisplayDensity(int displayId) throws RemoteException {
        return nativeGetBaseDisplayDensity(mNativeProxy, displayId);
    }

    @Override
    public void setForcedDisplaySize(int displayId, int width, int height) throws RemoteException {
        nativeSetForcedDisplaySize(mNativeProxy, displayId, width, height);
    }

    @Override
    public void setForcedDisplayDensity(int displayId, int density) throws RemoteException {
        nativeSetForcedDisplayDensity(mNativeProxy, displayId, density);
    }

    @Override
    public void setInTouchMode(boolean mode) throws RemoteException {
        nativeSetInTouchMode(mNativeProxy, mode);
    }

    @Override
    public void setNewConfiguration(Configuration config) throws RemoteException {
        nativeSetNewConfiguration(mNativeProxy, config);
    }

    @Override
    public void updateRotation(boolean alwaysSendConfiguration, boolean forceRelayout) throws RemoteException {
        nativeUpdateRotation(mNativeProxy, alwaysSendConfiguration, forceRelayout);
    }

    @Override
    public void setStrictModeVisualIndicatorPreference(String value) throws RemoteException {
        nativeSetStrictModeVisualIndicatorPreference(mNativeProxy, value);
    }

    @Override
    public void setScreenCaptureDisabled(int userId, boolean disabled) throws RemoteException {
        nativeSetScreenCaptureDisabled(mNativeProxy, userId, disabled);
    }

    @Override
    public void showStrictModeViolation(boolean on) throws RemoteException {
        nativeShowStrictModeViolation(mNativeProxy, on);
    }

    @Override
    public void startAppFreezingScreen(IBinder token, int configChanges) throws RemoteException {
        nativeStartAppFreezingScreen(mNativeProxy, token, configChanges);
    }

    @Override
    public boolean startViewServer(int port) throws RemoteException {
        return nativeStartViewServer(mNativeProxy, port);
    }

    @Override
    public int getLastWallpaperX() throws RemoteException {
        return nativeGetLastWallpaperX(mNativeProxy);
    }

    @Override
    public int getLastWallpaperY() throws RemoteException {
        return nativeGetLastWallpaperY(mNativeProxy);
    }

    @Override
    public void statusBarVisibilityChanged(int visibility) throws RemoteException {
        nativeStatusBarVisibilityChanged(mNativeProxy, visibility);
    }

    @Override
    public void stopAppFreezingScreen(IBinder token, boolean force) throws RemoteException {
        nativeStopAppFreezingScreen(mNativeProxy, token, force);
    }

    @Override
    public boolean stopViewServer() throws RemoteException {
        return nativeStopViewServer(mNativeProxy);
    }

    @Override
    public void thawRotation() throws RemoteException {
        nativeThawRotation(mNativeProxy);
    }

    @Override
    public Configuration updateOrientationFromAppTokens(Configuration currentConfig,
            IBinder freezeThisOneIfNeeded) throws RemoteException {
        return nativeUpdateOrientationFromAppTokens(mNativeProxy, currentConfig, freezeThisOneIfNeeded);
    }

    @Override
    public int watchRotation(IRotationWatcher watcher) throws RemoteException {
        return nativeWatchRotation(mNativeProxy, watcher);
    }

    @Override
    public void removeRotationWatcher(IRotationWatcher watcher) throws RemoteException {
        nativeRemoveRotationWatcher(mNativeProxy, watcher);
    }

    @Override
    public int getPreferredOptionsPanelGravity() throws RemoteException {
        return nativeGetPreferredOptionsPanelGravity(mNativeProxy);
    }

    @Override
    public void dismissKeyguard() {
        nativeDismissKeyguard(mNativeProxy);
    }

    @Override
    public void keyguardGoingAway(boolean disableWindowAnimations,
            boolean keyguardGoingToNotificationShade, boolean keyguardShowingMedia) {
        nativeKeyguardGoingAway(mNativeProxy, disableWindowAnimations, keyguardGoingToNotificationShade, keyguardShowingMedia);
    }

    @Override
    public void lockNow(Bundle options) {
        nativeLockNow(mNativeProxy, options);
    }

    @Override
    public boolean isSafeModeEnabled() {
        return nativeIsSafeModeEnabled(mNativeProxy);
    }

    @Override
    public void enableScreenIfNeeded() {
        nativeEnableScreenIfNeeded(mNativeProxy);
    }

    @Override
    public boolean clearWindowContentFrameStats(IBinder token) {
        return nativeClearWindowContentFrameStats(mNativeProxy, token);
    }

    @Override
    public WindowContentFrameStats getWindowContentFrameStats(IBinder token) {
        return nativeGetWindowContentFrameStats(mNativeProxy, token);
    }
}

