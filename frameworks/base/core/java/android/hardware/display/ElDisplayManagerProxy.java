
package android.hardware.display;

import android.hardware.display.IDisplayManagerCallback;
import android.hardware.display.IVirtualDisplayCallback;
import android.hardware.display.WifiDisplay;
import android.hardware.display.WifiDisplayStatus;
import android.media.projection.IMediaProjection;
import android.os.RemoteException;
import android.view.DisplayInfo;
import android.view.Surface;

/** @hide */
public class ElDisplayManagerProxy extends IDisplayManager.Stub {
    private long mNativeProxy;

    private native DisplayInfo nativeGetDisplayInfo(long nativeProxy, int displayId);

    private native void nativeRegisterCallback(long nativeProxy, IDisplayManagerCallback callback);

    private native int[] nativeGetDisplayIds(long nativeProxy);

    private native void nativeStartWifiDisplayScan(long nativeProxy);

    private native void nativeStopWifiDisplayScan(long nativeProxy);

    private native void nativeConnectWifiDisplay(long nativeProxy, String address);

    private native void nativeDisconnectWifiDisplay(long nativeProxy);

    private native void nativeRenameWifiDisplay(long nativeProxy, String address, String alias);

    private native void nativeForgetWifiDisplay(long nativeProxy, String address);

    private native void nativePauseWifiDisplay(long nativeProxy);

    private native void nativeResumeWifiDisplay(long nativeProxy);

    private native WifiDisplayStatus nativeGetWifiDisplayStatus(long nativeProxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElDisplayManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public DisplayInfo getDisplayInfo(int displayId) throws RemoteException {
        return nativeGetDisplayInfo(mNativeProxy, displayId);
    }

    @Override
    public void registerCallback(IDisplayManagerCallback callback) throws RemoteException {
        nativeRegisterCallback(mNativeProxy, callback);
    }

    @Override
    public int[] getDisplayIds() throws RemoteException {
        return nativeGetDisplayIds(mNativeProxy);
    }

    @Override
    public void startWifiDisplayScan() throws RemoteException {
        nativeStartWifiDisplayScan(mNativeProxy);
    }

    @Override
    public void stopWifiDisplayScan() throws RemoteException {
        nativeStopWifiDisplayScan(mNativeProxy);
    }

    @Override
    public void connectWifiDisplay(String address) throws RemoteException {
        nativeConnectWifiDisplay(mNativeProxy, address);
    }

    @Override
    public void disconnectWifiDisplay() throws RemoteException {
        nativeDisconnectWifiDisplay(mNativeProxy);
    }

    @Override
    public void renameWifiDisplay(String address, String alias) throws RemoteException {
        nativeRenameWifiDisplay(mNativeProxy, address, alias);
    }

    @Override
    public void forgetWifiDisplay(String address) throws RemoteException {
        nativeForgetWifiDisplay(mNativeProxy, address);
    }

    @Override
    public void pauseWifiDisplay() throws RemoteException {
        nativePauseWifiDisplay(mNativeProxy);
    }

    @Override
    public void resumeWifiDisplay() throws RemoteException {
        nativeResumeWifiDisplay(mNativeProxy);
    }

    @Override
    public WifiDisplayStatus getWifiDisplayStatus() throws RemoteException {
        return nativeGetWifiDisplayStatus(mNativeProxy);
    }

    @Override
    public int createVirtualDisplay(IVirtualDisplayCallback callback,
            IMediaProjection projectionToken, String packageName, String name,
            int width, int height, int densityDpi, Surface surface, int flags) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void resizeVirtualDisplay(IVirtualDisplayCallback token,
            int width, int height, int densityDpi) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void setVirtualDisplaySurface(IVirtualDisplayCallback token, Surface surface) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void releaseVirtualDisplay(IVirtualDisplayCallback token) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }
}
