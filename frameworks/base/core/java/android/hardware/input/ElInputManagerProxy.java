
package android.hardware.input;

import android.hardware.input.InputDeviceIdentifier;
import android.hardware.input.KeyboardLayout;
import android.hardware.input.IInputManager;
import android.hardware.input.IInputDevicesChangedListener;
import android.hardware.input.TouchCalibration;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.InputDevice;
import android.view.InputEvent;

/** @hide */
public class ElInputManagerProxy extends IInputManager.Stub {
    private long mNativeProxy;

    private native void nativeRegisterInputDevicesChangedListener(long nativeProxy, IInputDevicesChangedListener listener);

    private native int[] nativeGetInputDeviceIds(long nativeProxy);

    private native InputDevice nativeGetInputDevice(long nativeProxy, int deviceId);

    private native boolean nativeHasKeys(long nativeProxy, int deviceId, int sourceMask, int[] keyCodes, boolean[] keyExists);

    private native boolean nativeInjectInputEvent(long nativeProxy, InputEvent ev, int mode);

    private native TouchCalibration nativeGetTouchCalibrationForInputDevice(long nativeProxy, String inputDeviceDescriptor, int rotation);

    private native void nativeSetTouchCalibrationForInputDevice(long nativeProxy, String inputDeviceDescriptor, int rotation, TouchCalibration calibration);

    private native KeyboardLayout[] nativeGetKeyboardLayouts(long nativeProxy);

    private native KeyboardLayout nativeGetKeyboardLayout(long nativeProxy, String keyboardLayoutDescriptor);

    private native String nativeGetCurrentKeyboardLayoutForInputDevice(long nativeProxy, InputDeviceIdentifier identifier);

    private native void nativeSetCurrentKeyboardLayoutForInputDevice(long nativeProxy, InputDeviceIdentifier identifier, String keyboardLayoutDescriptor);

    private native String[] nativeGetKeyboardLayoutsForInputDevice(long nativeProxy, InputDeviceIdentifier identifier);

    private native void nativeAddKeyboardLayoutForInputDevice(long nativeProxy, InputDeviceIdentifier identifier, String keyboardLayoutDescriptor);

    private native void nativeRemoveKeyboardLayoutForInputDevice(long nativeProxy, InputDeviceIdentifier identifier, String keyboardLayoutDescriptor);

    private native void nativeVibrate(long nativeProxy, int deviceId, long[] pattern, int repeat, IBinder token);

    private native void nativeCancelVibrate(long nativeProxy, int deviceId, IBinder token);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElInputManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void registerInputDevicesChangedListener(IInputDevicesChangedListener listener) throws RemoteException {
        nativeRegisterInputDevicesChangedListener(mNativeProxy, listener);
    }

    @Override
    public int[] getInputDeviceIds() throws RemoteException {
        return nativeGetInputDeviceIds(mNativeProxy);
    }

    @Override
    public InputDevice getInputDevice(int deviceId) throws RemoteException {
        return nativeGetInputDevice(mNativeProxy, deviceId);
    }

    @Override
    public boolean hasKeys(int deviceId, int sourceMask, int[] keyCodes, boolean[] keyExists) throws RemoteException {
        return nativeHasKeys(mNativeProxy, deviceId, sourceMask, keyCodes, keyExists);
    }

    @Override
    public void tryPointerSpeed(int speed) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public boolean injectInputEvent(InputEvent ev, int mode) throws RemoteException {
        return nativeInjectInputEvent(mNativeProxy, ev, mode);
    }

    @Override
    public TouchCalibration getTouchCalibrationForInputDevice(String inputDeviceDescriptor, int rotation) throws RemoteException {
        return nativeGetTouchCalibrationForInputDevice(mNativeProxy, inputDeviceDescriptor, rotation);
    }

    @Override
    public void setTouchCalibrationForInputDevice(String inputDeviceDescriptor, int rotation, TouchCalibration calibration) throws RemoteException {
        nativeSetTouchCalibrationForInputDevice(mNativeProxy, inputDeviceDescriptor, rotation, calibration);
    }

    @Override
    public KeyboardLayout[] getKeyboardLayouts() throws RemoteException {
        return nativeGetKeyboardLayouts(mNativeProxy);
    }

    @Override
    public KeyboardLayout getKeyboardLayout(String keyboardLayoutDescriptor) throws RemoteException {
        return nativeGetKeyboardLayout(mNativeProxy, keyboardLayoutDescriptor);
    }

    @Override
    public String getCurrentKeyboardLayoutForInputDevice(InputDeviceIdentifier identifier) throws RemoteException {
        return nativeGetCurrentKeyboardLayoutForInputDevice(mNativeProxy, identifier);
    }

    @Override
    public void setCurrentKeyboardLayoutForInputDevice(InputDeviceIdentifier identifier,
            String keyboardLayoutDescriptor) throws RemoteException {
        nativeSetCurrentKeyboardLayoutForInputDevice(mNativeProxy, identifier, keyboardLayoutDescriptor);
    }

    @Override
    public String[] getKeyboardLayoutsForInputDevice(InputDeviceIdentifier identifier) throws RemoteException {
        return nativeGetKeyboardLayoutsForInputDevice(mNativeProxy, identifier);
    }

    @Override
    public void addKeyboardLayoutForInputDevice(InputDeviceIdentifier identifier,
            String keyboardLayoutDescriptor) throws RemoteException {
        nativeAddKeyboardLayoutForInputDevice(mNativeProxy, identifier, keyboardLayoutDescriptor);
    }

    @Override
    public void removeKeyboardLayoutForInputDevice(InputDeviceIdentifier identifier,
            String keyboardLayoutDescriptor) throws RemoteException {
        nativeRemoveKeyboardLayoutForInputDevice(mNativeProxy, identifier, keyboardLayoutDescriptor);
    }

    @Override
    public void vibrate(int deviceId, long[] pattern, int repeat, IBinder token) throws RemoteException {
        nativeVibrate(mNativeProxy, deviceId, pattern, repeat, token);
    }

    @Override
    public void cancelVibrate(int deviceId, IBinder token) throws RemoteException {
        nativeCancelVibrate(mNativeProxy, deviceId, token);
    }
}
