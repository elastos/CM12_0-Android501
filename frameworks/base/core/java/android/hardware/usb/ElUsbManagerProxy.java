
package android.hardware.usb;

import android.app.PendingIntent;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbDevice;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;

/** @hide */
public class ElUsbManagerProxy extends IUsbManager.Stub
{
    private long mNativeProxy;

    private native void nativeGetDeviceList(long proxy, Bundle devices);

    private native boolean nativeHasDefaults(long proxy, String packageName, int userId);

    private native ParcelFileDescriptor nativeOpenDevice(long proxy, String deviceName);

    private native UsbAccessory nativeGetCurrentAccessory(long proxy);

    private native ParcelFileDescriptor nativeOpenAccessory(long proxy, UsbAccessory accessory);

    private native void naitveSetDevicePackage(long proxy, UsbDevice device, String packageName, int userId);

    private native void nativeSetAccessoryPackage(long proxy, UsbAccessory accessory, String packageName, int userId);

    private native boolean nativeHasDevicePermission(long proxy, UsbDevice device);

    private native boolean nativeHasAccessoryPermission(long proxy, UsbAccessory accessory);

    private native void nativeRequestDevicePermission(long proxy, UsbDevice device, String packageName, PendingIntent pi);

    private native void nativeRequestAccessoryPermission(long proxy, UsbAccessory accessory, String packageName,
            PendingIntent pi);

    private native void nativeGrantDevicePermission(long proxy, UsbDevice device, int uid);

    private native void nativeGrantAccessoryPermission(long proxy, UsbAccessory accessory, int uid);

    private native void nativeClearDefaults(long proxy, String packageName, int userId);

    private native void nativeSetCurrentFunction(long proxy, String function, boolean makeDefault);

    private native void nativeSetMassStorageBackingFile(long proxy, String path);

    private native void nativeAllowUsbDebugging(long proxy, boolean alwaysAllow, String publicKey);

    private native void nativeDenyUsbDebugging(long proxy);

    private native void nativeClearUsbDebuggingKeys(long proxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElUsbManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void getDeviceList(Bundle devices) throws RemoteException {
        nativeGetDeviceList(mNativeProxy, devices);
    }

    @Override
    public boolean hasDefaults(String packageName, int userId) throws RemoteException {
        return nativeHasDefaults(mNativeProxy, packageName, userId);
    }

    @Override
    public ParcelFileDescriptor openDevice(String deviceName) throws RemoteException {
        return nativeOpenDevice(mNativeProxy, deviceName);
    }

    @Override
    public UsbAccessory getCurrentAccessory() throws RemoteException {
        return nativeGetCurrentAccessory(mNativeProxy);
    }

    @Override
    public ParcelFileDescriptor openAccessory(UsbAccessory accessory) throws RemoteException {
        return nativeOpenAccessory(mNativeProxy, accessory);
    }

    @Override
    public void setDevicePackage(UsbDevice device, String packageName, int userId) throws RemoteException {
        naitveSetDevicePackage(mNativeProxy, device, packageName, userId);
    }

    @Override
    public void setAccessoryPackage(UsbAccessory accessory, String packageName, int userId) throws RemoteException {
        nativeSetAccessoryPackage(mNativeProxy, accessory, packageName, userId);
    }

    @Override
    public boolean hasDevicePermission(UsbDevice device) throws RemoteException {
        return nativeHasDevicePermission(mNativeProxy, device);
    }

    @Override
    public boolean hasAccessoryPermission(UsbAccessory accessory) throws RemoteException {
        return nativeHasAccessoryPermission(mNativeProxy, accessory);
    }

    @Override
    public void requestDevicePermission(UsbDevice device, String packageName, PendingIntent pi) throws RemoteException {
        nativeRequestDevicePermission(mNativeProxy, device, packageName, pi);
    }

    @Override
    public void requestAccessoryPermission(UsbAccessory accessory, String packageName,
            PendingIntent pi) throws RemoteException {
        nativeRequestAccessoryPermission(mNativeProxy, accessory, packageName, pi);
    }

    @Override
    public void grantDevicePermission(UsbDevice device, int uid) throws RemoteException {
        nativeGrantDevicePermission(mNativeProxy, device, uid);
    }

    @Override
    public void grantAccessoryPermission(UsbAccessory accessory, int uid) throws RemoteException {
        nativeGrantAccessoryPermission(mNativeProxy, accessory, uid);
    }

    @Override
    public void clearDefaults(String packageName, int userId) throws RemoteException {
        nativeClearDefaults(mNativeProxy, packageName, userId);
    }

    @Override
    public void setCurrentFunction(String function, boolean makeDefault) throws RemoteException {
        nativeSetCurrentFunction(mNativeProxy, function, makeDefault);
    }

    @Override
    public void setMassStorageBackingFile(String path) throws RemoteException {
        nativeSetMassStorageBackingFile(mNativeProxy, path);
    }

    @Override
    public void allowUsbDebugging(boolean alwaysAllow, String publicKey) throws RemoteException {
        nativeAllowUsbDebugging(mNativeProxy, alwaysAllow, publicKey);
    }

    @Override
    public void denyUsbDebugging() throws RemoteException {
        nativeDenyUsbDebugging(mNativeProxy);
    }

    @Override
    public void clearUsbDebuggingKeys() throws RemoteException {
        nativeClearUsbDebuggingKeys(mNativeProxy);
    }
}
