
package android.inputmethodservice;

import com.android.internal.view.IInputMethodSession;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.InputMethodSession;
import android.util.Log;

class ElIInputMethodSessionProxy extends IInputMethodSession.Stub {
    private long mNativeProxy;

    InputMethodSession mInputMethodSession;

    private static final native void nativeFinalize(long obj);

    private native void nativeViewClicked(long nativeProxy, boolean focusChanged);

    private native void nativeUpdateSelection(long nativeProxy, int oldSelStart, int oldSelEnd,
            int newSelStart, int newSelEnd, int candidatesStart, int candidatesEnd);

    private native void nativeDisplayCompletions(long nativeProxy, CompletionInfo[] completions);

    private static final native void nativeFinishInput(long nativeProxy);

    private static final native void nativeFinishSession(long nativeProxy);

    public ElIInputMethodSessionProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }

        mInputMethodSession = null;
    }

    @Override
    public void viewClicked(boolean focusChanged) throws RemoteException {
        nativeViewClicked(mNativeProxy, focusChanged);
    }

    @Override
    public void updateSelection(int oldSelStart, int oldSelEnd,
            int newSelStart, int newSelEnd, int candidatesStart, int candidatesEnd) throws RemoteException {
        nativeUpdateSelection(mNativeProxy, oldSelStart, oldSelEnd, newSelStart, newSelEnd, candidatesStart, candidatesEnd);
    }

    @Override
    public void displayCompletions(CompletionInfo[] completions) throws RemoteException {
        nativeDisplayCompletions(mNativeProxy, completions);
    }

    @Override
    public void finishInput() throws RemoteException {
        nativeFinishInput(mNativeProxy);
    }

    @Override
    public void updateExtractedText(int token, ExtractedText text) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void updateCursor(Rect newCursor) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void appPrivateCommand(String action, Bundle data) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void toggleSoftInput(int showFlags, int hideFlags) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void finishSession() throws RemoteException {
        nativeFinishSession(mNativeProxy);
    }

    @Override
    public void updateCursorAnchorInfo(CursorAnchorInfo cursorAnchorInfo) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public IBinder asBinder() {
        Log.e("ElIInputMethodSessionProxy", "asBinder()");
        return null;
    }
}
