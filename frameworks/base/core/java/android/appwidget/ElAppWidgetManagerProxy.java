
package android.appwidget;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.appwidget.AppWidgetProviderInfo;
import com.android.internal.appwidget.IAppWidgetService;
import com.android.internal.appwidget.IAppWidgetHost;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.RemoteViews;

import java.util.List;

/** {@hide} */
public class ElAppWidgetManagerProxy extends IAppWidgetService.Stub  {
    private long mNativeProxy;

    private native int[] nativeStartListening(long proxy, IAppWidgetHost host, String callingPackage, int hostId,
            List<RemoteViews> updatedViews);

    private native void nativeStopListening(long proxy, String callingPackage, int hostId);

    private native List<AppWidgetProviderInfo> nativeGetInstalledProvidersForProfile(long proxy, int categoryFilter, int profileId);

    private native void nativeDeleteHost(long proxy, String packageName, int hostId);

    private native boolean nativeHasBindAppWidgetPermission(long proxy, String packageName, int userId);

    private native int nativeAllocateAppWidgetId(long proxy, String callingPackage, int hostId);

    private native void nativeDeleteAppWidgetId(long proxy, String callingPackage, int appWidgetId);

    private native void nativeDeleteAllHosts(long proxy);

    private native RemoteViews nativeGetAppWidgetViews(long proxy, String callingPackage, int appWidgetId);

    private native int[] nativeGetAppWidgetIdsForHost(long proxy, String callingPackage, int hostId);

    private native IntentSender nativeCreateAppWidgetConfigIntentSender(long proxy, String callingPackage, int appWidgetId);

    private native void nativeUpdateAppWidgetIds(long proxy, String callingPackage, int[] appWidgetIds,  RemoteViews views);

    private native void nativeUpdateAppWidgetOptions(long proxy, String callingPackage, int appWidgetId, Bundle extras);

    private native Bundle nativeGetAppWidgetOptions(long proxy, String callingPackage, int appWidgetId);

    private native void nativePartiallyUpdateAppWidgetIds(long proxy, String callingPackage, int[] appWidgetIds, RemoteViews views);

    private native void nativeUpdateAppWidgetProvider(long proxy, ComponentName provider, RemoteViews views);

    private native void nativeNotifyAppWidgetViewDataChanged(long proxy, String packageName, int[] appWidgetIds, int viewId);

    private native AppWidgetProviderInfo nativeGetAppWidgetInfo(long proxy, String callingPackage, int appWidgetId);

    private native void nativeSetBindAppWidgetPermission(long proxy, String packageName, int userId, boolean permission);

    private native boolean nativeBindAppWidgetId(long proxy, String callingPackage, int appWidgetId, int providerProfileId, ComponentName provider, Bundle options);

    private native void nativeBindRemoteViewsService(long proxy, String callingPackage, int appWidgetId, Intent intent, IBinder connection);

    private native void nativeUnbindRemoteViewsService(long proxy, String callingPackage, int appWidgetId, Intent intent);

    private native int[] nativeGetAppWidgetIds(long proxy, ComponentName provider);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElAppWidgetManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public int[] startListening(IAppWidgetHost host, String callingPackage, int hostId,
            List<RemoteViews> updatedViews) throws RemoteException {
        return nativeStartListening(mNativeProxy, host, callingPackage, hostId, updatedViews);
    }

    @Override
    public void stopListening(String callingPackage, int hostId) throws RemoteException {
        nativeStopListening(mNativeProxy, callingPackage, hostId);
    }

    @Override
    public List<AppWidgetProviderInfo> getInstalledProvidersForProfile(int categoryFilter, int profileId) throws RemoteException {
        return nativeGetInstalledProvidersForProfile(mNativeProxy, categoryFilter, profileId);
    }

    @Override
    public void deleteHost(String packageName, int hostId) throws RemoteException {
        nativeDeleteHost(mNativeProxy, packageName, hostId);
    }

    @Override
    public boolean hasBindAppWidgetPermission(String packageName, int userId) throws RemoteException {
        return nativeHasBindAppWidgetPermission(mNativeProxy, packageName, userId);
    }

    @Override
    public int allocateAppWidgetId(String callingPackage, int hostId) throws RemoteException {
        return nativeAllocateAppWidgetId(mNativeProxy, callingPackage, hostId);
    }

    @Override
    public void deleteAppWidgetId(String callingPackage, int appWidgetId) throws RemoteException {
        nativeDeleteAppWidgetId(mNativeProxy, callingPackage, appWidgetId);
    }

    @Override
    public void deleteAllHosts() throws RemoteException {
        nativeDeleteAllHosts(mNativeProxy);
    }

    @Override
    public RemoteViews getAppWidgetViews(String callingPackage, int appWidgetId) throws RemoteException {
        return nativeGetAppWidgetViews(mNativeProxy, callingPackage, appWidgetId);
    }

    @Override
    public int[] getAppWidgetIdsForHost(String callingPackage, int hostId) throws RemoteException {
        return nativeGetAppWidgetIdsForHost(mNativeProxy, callingPackage, hostId);
    }

    @Override
    public IntentSender createAppWidgetConfigIntentSender(String callingPackage, int appWidgetId) throws RemoteException {
        return nativeCreateAppWidgetConfigIntentSender(mNativeProxy, callingPackage, appWidgetId);
    }

    @Override
    public void updateAppWidgetIds(String callingPackage, int[] appWidgetIds,  RemoteViews views) throws RemoteException {
        nativeUpdateAppWidgetIds(mNativeProxy, callingPackage, appWidgetIds, views);
    }

    @Override
    public void updateAppWidgetOptions(String callingPackage, int appWidgetId, Bundle extras) throws RemoteException {
        nativeUpdateAppWidgetOptions(mNativeProxy, callingPackage, appWidgetId, extras);
    }

    @Override
    public Bundle getAppWidgetOptions(String callingPackage, int appWidgetId) throws RemoteException {
        return nativeGetAppWidgetOptions(mNativeProxy, callingPackage, appWidgetId);
    }

    @Override
    public void partiallyUpdateAppWidgetIds(String callingPackage, int[] appWidgetIds, RemoteViews views) throws RemoteException {
        nativePartiallyUpdateAppWidgetIds(mNativeProxy, callingPackage, appWidgetIds, views);
    }

    @Override
    public void updateAppWidgetProvider(ComponentName provider, RemoteViews views) throws RemoteException {
        nativeUpdateAppWidgetProvider(mNativeProxy, provider, views);
    }

    @Override
    public void notifyAppWidgetViewDataChanged(String packageName, int[] appWidgetIds, int viewId) throws RemoteException {
        nativeNotifyAppWidgetViewDataChanged(mNativeProxy, packageName, appWidgetIds, viewId);
    }

    @Override
    public AppWidgetProviderInfo getAppWidgetInfo(String callingPackage, int appWidgetId) throws RemoteException {
        return nativeGetAppWidgetInfo(mNativeProxy, callingPackage, appWidgetId);
    }

    @Override
    public void setBindAppWidgetPermission(String packageName, int userId, boolean permission) throws RemoteException {
         nativeSetBindAppWidgetPermission(mNativeProxy, packageName, userId, permission);
    }

    @Override
    public boolean bindAppWidgetId(String callingPackage, int appWidgetId, int providerProfileId, ComponentName provider, Bundle options) throws RemoteException {
        return nativeBindAppWidgetId(mNativeProxy, callingPackage, appWidgetId, providerProfileId, provider, options);
    }

    @Override
    public void bindRemoteViewsService(String callingPackage, int appWidgetId, Intent intent, IBinder connection) throws RemoteException {
        nativeBindRemoteViewsService(mNativeProxy, callingPackage, appWidgetId, intent, connection);
    }

    @Override
    public void unbindRemoteViewsService(String callingPackage, int appWidgetId, Intent intent) throws RemoteException {
        nativeUnbindRemoteViewsService(mNativeProxy, callingPackage, appWidgetId, intent);
    }

    @Override
    public int[] getAppWidgetIds(ComponentName provider) throws RemoteException {
        return nativeGetAppWidgetIds(mNativeProxy, provider);
    }
}

