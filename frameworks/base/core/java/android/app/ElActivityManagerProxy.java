
package android.app;

import android.app.ActivityManager.RunningTaskInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ActivityManager.StackInfo;
import android.content.ComponentName;
import android.content.ContentProviderNative;
import android.content.IContentProvider;
import android.content.IIntentReceiver;
import android.content.IIntentSender;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.UriPermission;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.ParceledListSlice;
import android.content.pm.ProviderInfo;
import android.content.pm.UserInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.StrictMode;
import android.service.voice.IVoiceInteractionSession;
import com.android.internal.app.IVoiceInteractor;

import java.util.List;


/** {@hide} */
class ElActivityManagerProxy extends Binder implements IActivityManager
{
    private long mNativeProxy;

    private native int nativeStartActivity(long nativeProxy, IApplicationThread caller, String callingPackage, Intent intent,
            String resolvedType, IBinder resultTo, String resultWho, int requestCode, int flags,
            ProfilerInfo profilerInfo, Bundle options);

    private native int nativeStartActivityIntentSender(long nativeProxy, IApplicationThread caller,
            IntentSender intent, Intent fillInIntent, String resolvedType,
            IBinder resultTo, String resultWho, int requestCode,
            int flagsMask, int flagsValues, Bundle options);

    private native int nativeStartVoiceActivity(long nativeProxy, String callingPackage, int callingPid, int callingUid,
            Intent intent, String resolvedType, IVoiceInteractionSession session,
            IVoiceInteractor interactor, int flags, ProfilerInfo profilerInfo, Bundle options,
            int userId);

    private native void nativeAttachApplication(long nativeProxy, IApplicationThread app, int jappHashCode);

    private native void nativeActivityPaused(long nativeProxy, IBinder token);

    private native int nativeStartActivityFromRecents(long nativeProxy, int taskId, Bundle options);

    private native boolean nativeFinishActivity(long nativeProxy, IBinder token, int resultCode, Intent resultData, boolean finishTask);

    private native void nativeActivityDestroyed(long nativeProxy, IBinder token);

    private native boolean nativeWillActivityBeVisible(long nativeProxy, IBinder token);

    private native void nativeActivityResumed(long nativeProxy, IBinder token);

    private native void nativeActivityIdle(long nativeProxy, IBinder token, Configuration config, boolean stopProfiling);

    private native void nativePublishContentProviders(long nativeProxy, IApplicationThread caller, List<ContentProviderHolder> providers);

    private native Intent nativeRegisterReceiver(long nativeProxy, IApplicationThread caller, String packageName, IIntentReceiver receiver,
            IntentFilter filter, String perm, int userId);

    private native void nativeUnregisterReceiver(long nativeProxy, int receiver);

    private native ContentProviderHolder  nativeGetContentProvider(long nativeProxy, IApplicationThread caller,
            String name, int userId, boolean stable);

    private native void nativeSetRequestedOrientation(long nativeProxy, IBinder token, int requestedOrientation);

    private native int nativeGetRequestedOrientation(long nativeProxy, IBinder token);

    private native void nativeActivityStopped(long nativeProxy, IBinder token, Bundle state,
            PersistableBundle persistentState, CharSequence description);

    private native void nativeGetMemoryInfo(long nativeProxy, ActivityManager.MemoryInfo outInfo);

    private native List<ActivityManager.RunningAppProcessInfo> nativeGetRunningAppProcesses(long nativeProxy);

    private native List<RunningServiceInfo> nativeGetServices(long nativeProxy, int maxNum, int flags);

    private native int nativeCheckPermission(long nativeProxy, String permission, int pid, int uid);

    private native UserInfo nativeGetCurrentUser(long nativeProxy);

    private native boolean nativeRefContentProvider(long nativeProxy, IBinder connection, int stable, int unstable);

    private native int nativeBroadcastIntent(long nativeProxy, IApplicationThread caller,
            Intent intent, String resolvedType,  int resultTo,
            int resultCode, String resultData, Bundle map,
            String requiredPermission, int appOp, boolean serialized,
            boolean sticky, int userId);

    private native void nativeHandleApplicationCrash(long nativeProxy, int app, ApplicationErrorReport.CrashInfo crashInfo);

    private native ComponentName nativeStartService(long nativeProxy, IApplicationThread caller, Intent service, String resolvedType, int userId);

    private native int nativeStopService(long nativeProxy, IApplicationThread caller, Intent service, String resolvedType, int userId);

    private native void nativeServiceDoneExecuting(long nativeProxy, IBinder token, int type, int startId, int res);

    private native IIntentSender nativeGetIntentSender(long nativeProxy, int type, String packageName, IBinder token, String resultWho,
            int requestCode, Intent[] intents, String[] resolvedTypes, int flags, Bundle options, int userId);

    private native boolean nativeMoveActivityTaskToBack(long nativeProxy, IBinder token, boolean nonRoot);

    private native Configuration nativeGetConfiguration(long nativeProxy);

    private native void nativeSetServiceForeground(long nativeProxy, ComponentName className, IBinder token,
            int id, Notification notification, boolean removeNotification);

    private native void nativeRemoveContentProvider(long nativeProxy, IBinder connection, boolean stable);

    private native void nativeUpdateConfiguration(long nativeProxy, Configuration values);

    private native int nativeBindService(long nativeProxy, IApplicationThread caller, IBinder token, Intent service, String resolvedType,
            IServiceConnection connection, int flags, int userId);

    private native boolean nativeUnbindService(long nativeProxy, IServiceConnection connection);

    private native ConfigurationInfo nativeGetDeviceConfigurationInfo(long nativeProxy);

    private native boolean nativeStopServiceToken(long nativeProxy, ComponentName className, IBinder token, int startId);

    private native void nativePublishService(long nativeProxy, IBinder token, Intent intent, IBinder service);

    private native List<IAppTask> nativeGetAppTasks(long nativeProxy, String callingPackage);

    private native int nativeAddAppTask(long nativeProxy, IBinder activityToken, Intent intent,
            ActivityManager.TaskDescription description, Bitmap thumbnail);

    private native Point nativeGetAppTaskThumbnailSize(long nativeProxy);

    private native List<RunningTaskInfo> nativeGetTasks(long nativeProxy, int maxNum, int flags);

    private native String nativeGetCallingPackage(long nativeProxy, IBinder token);

    private native String nativeGetCallingPackageForBroadcast(long nativeProxy, boolean foreground);

    private native void nativeFinishReceiver(long nativeProxy, IBinder who, int resultCode, String resultData,
            Bundle map, boolean abortBroadcast, int flags);

    private native void nativeOverridePendingTransition(long nativeProxy, IBinder token, String packageName,
            int enterAnim, int exitAnim);

    private native Debug.MemoryInfo[] nativeGetProcessMemoryInfo(long nativeProxy, int[] pids);

    private native boolean nativeIsUserAMonkey(long nativeProxy);

    private native void nativeForceStopPackage(long nativeProxy, String packageName, int userId);

    private native void nativeUpdatePersistentConfiguration(long nativeProxy, Configuration values);

    private native long[] nativeGetProcessPss(long nativeProxy, int[] pids);

    private native boolean nativeClearApplicationUserData(long nativeProxy, final String packageName,
            final IPackageDataObserver observer, final int userId);

    private native void nativeKillBackgroundProcesses(long nativeProxy, String packageName, int userId);

    private native int nativeStartActivityAsUser(long nativeProxy, IApplicationThread caller, String callingPackage, Intent intent,
            String resolvedType, IBinder resultTo, String resultWho, int requestCode, int startFlags,
            ProfilerInfo profilerInfo, Bundle options, int userId);

    private native int nativeStartActivityAsCaller(long nativeProxy, IApplicationThread caller, String callingPackage,
            Intent intent, String resolvedType, IBinder resultTo, String resultWho, int requestCode,
            int flags, ProfilerInfo profilerInfo, Bundle options, int userId);

    private native WaitResult nativeStartActivityAndWait(long nativeProxy, IApplicationThread caller, String callingPackage, Intent intent,
            String resolvedType, IBinder resultTo, String resultWho, int requestCode, int startFlags,
            ProfilerInfo profilerInfo, Bundle options, int userId);

    private native int nativeStartActivityWithConfig(long nativeProxy, IApplicationThread caller, String callingPackage, Intent intent,
            String resolvedType, IBinder resultTo, String resultWho, int requestCode, int startFlags,
            Configuration config, Bundle options, int userId);

    private native boolean nativeStartNextMatchingActivity(long nativeProxy, IBinder callingActivity, Intent intent, Bundle options);

    private native void nativeFinishSubActivity(long nativeProxy, IBinder token, String resultWho, int requestCode);

    private native boolean nativeFinishActivityAffinity(long nativeProxy, IBinder token);

    private native void nativeFinishVoiceTask(long nativeProxy, IVoiceInteractionSession session);

    private native boolean nativeReleaseActivityInstance(long nativeProxy, IBinder token);

    private native void nativeReleaseSomeActivities(long nativeProxy, IApplicationThread app);

    private native void nativeUnbroadcastIntent(long nativeProxy, IApplicationThread caller, Intent intent, int userId);

    private native void nativeActivitySlept(long nativeProxy, IBinder token);

    private native ComponentName nativeGetCallingActivity(long nativeProxy, IBinder token);

    private native List<ActivityManager.RecentTaskInfo> nativeGetRecentTasks(long nativeProxy, int maxNum, int flags, int userId);

    private native ActivityManager.TaskThumbnail nativeGetTaskThumbnail(long nativeProxy, int id);

    private native List<ActivityManager.ProcessErrorStateInfo> nativeGetProcessesInErrorState(long nativeProxy);

    private native List<ApplicationInfo> nativeGetRunningExternalApplications(long nativeProxy);

    private native void nativeMoveTaskToFront(long nativeProxy, int task, int flags, Bundle options);

    private native void nativeMoveTaskToBack(long nativeProxy, int task);

    private native void nativeMoveTaskBackwards(long nativeProxy, int task);

    private native void nativeMoveTaskToStack(long nativeProxy, int taskId, int stackId, boolean toTop);

    private native void nativeResizeStack(long nativeProxy, int stackId, Rect bounds);

    private native List<StackInfo> nativeGetAllStackInfos(long nativeProxy);

    private native StackInfo nativeGetStackInfo(long nativeProxy, int stackId);

    private native boolean nativeIsInHomeStack(long nativeProxy, int taskId);

    private native void nativeSetFocusedStack(long nativeProxy, int stackId);

    private native int nativeGetTaskForActivity(long nativeProxy, IBinder token, boolean onlyRoot);

    private native ContentProviderHolder nativeGetContentProviderExternal(long nativeProxy, String name, int userId, IBinder token);

    private native void nativeUnstableProviderDied(long nativeProxy, IBinder connection);

    private native void nativeAppNotRespondingViaProvider(long nativeProxy, IBinder connection);

    private native void nativeRemoveContentProviderExternal(long nativeProxy, String name, IBinder token);

    private native PendingIntent nativeGetRunningServiceControlPanel(long nativeProxy, ComponentName service);

    private native void nativeUnbindFinished(long nativeProxy, IBinder token, Intent intent, boolean doRebind);

    private native IBinder nativePeekService(long nativeProxy, Intent service, String resolvedType);

    private native boolean nativeBindBackupAgent(long nativeProxy, ApplicationInfo app, int backupRestoreMode);

    private native void nativeClearPendingBackup(long nativeProxy);

    private native void nativeBackupAgentCreated(long nativeProxy, String packageName, IBinder agent);

    private native void nativeUnbindBackupAgent(long nativeProxy, ApplicationInfo app);

    private native boolean nativeStartInstrumentation(long nativeProxy, ComponentName className, String profileFile,
            int flags, Bundle arguments, IInstrumentationWatcher watcher,
            IUiAutomationConnection connection, int userId, String abiOverride);

    private native void nativeFinishInstrumentation(long nativeProxy, IApplicationThread target, int resultCode, Bundle results);

    private native ComponentName nativeGetActivityClassForToken(long nativeProxy, IBinder token);

    private native String nativeGetPackageForToken(long nativeProxy, IBinder token);

    private native void nativeCancelIntentSender(long nativeProxy, IIntentSender sender);

    private native String nativeGetPackageForIntentSender(long nativeProxy, IIntentSender sender);

    private native int nativeGetUidForIntentSender(long nativeProxy, IIntentSender sender);

    private native int nativeHandleIncomingUser(long nativeProxy, int callingPid, int callingUid, int userId,
            boolean allowAll, boolean requireFull, String name, String callerPackage);

    private native void nativeSetProcessLimit(long nativeProxy, int max);

    private native int nativeGetProcessLimit(long nativeProxy);

    private native void nativeSetProcessForeground(long nativeProxy, IBinder token, int pid, boolean isForeground);

    private native int nativeCheckUriPermission(long nativeProxy, Uri uri, int pid, int uid, int mode, int userId);

    private native void nativeGrantUriPermission(long nativeProxy, IApplicationThread caller, String targetPkg, Uri uri, int mode, int userId);

    private native void nativeRevokeUriPermission(long nativeProxy, IApplicationThread caller, Uri uri, int mode, int userId);

    private native void nativeTakePersistableUriPermission(long nativeProxy, Uri uri, int modeFlags, int userId);

    private native void nativeReleasePersistableUriPermission(long nativeProxy, Uri uri, int modeFlags, int userId);

    private native ParceledListSlice<UriPermission> nativeGetPersistedUriPermissions(long nativeProxy,
            String packageName, boolean incoming);

    private native void nativeShowWaitingForDebugger(long nativeProxy, IApplicationThread who, boolean waiting);

    private native void nativeUnhandledBack(long nativeProxy);

    private native ParcelFileDescriptor nativeOpenContentUri(long nativeProxy, Uri uri);

    private native void nativeSetLockScreenShown(long nativeProxy, boolean shown);

    private native void nativeSetDebugApp(long nativeProxy, String packageName, boolean waitForDebugger, boolean persistent);

    private native void nativeSetAlwaysFinish(long nativeProxy, boolean enabled);

    private native void nativeSetActivityController(long nativeProxy, IActivityController watcher);

    private native void nativeEnterSafeMode(long nativeProxy);

    private native void nativeNoteWakeupAlarm(long nativeProxy, IIntentSender sender, int sourceUid, String sourcePkg);

    private native boolean nativeKillPids(long nativeProxy, int[] pids, String reason, boolean secure);

    private native boolean nativeKillProcessesBelowForeground(long nativeProxy, String reason);

    private native boolean nativeHandleApplicationWtf(long nativeProxy, IBinder app, String tag, boolean system, ApplicationErrorReport.CrashInfo crashInfo);

    private native void nativeHandleApplicationStrictModeViolation(long nativeProxy, IBinder app,
            int violationMask, StrictMode.ViolationInfo info);

    private native void nativeSignalPersistentProcesses(long nativeProxy, int sig);

    private native void nativeKillAllBackgroundProcesses(long nativeProxy);

    private native void nativeGetMyMemoryState(long nativeProxy, ActivityManager.RunningAppProcessInfo outInfo);

    private native boolean nativeProfileControl(long nativeProxy, String process, int userId, boolean start,
            ProfilerInfo profilerInfo, int profileType);

    private native boolean nativeShutdown(long nativeProxy, int timeout);

    private native void nativeStopAppSwitches(long nativeProxy);

    private native void nativeResumeAppSwitches(long nativeProxy);

    private native void nativeAddPackageDependency(long nativeProxy, String packageName);

    private native void nativeKillApplicationWithAppId(long nativeProxy, String pkg, int appid, String reason);

    private native void nativeCloseSystemDialogs(long nativeProxy, String reason);

    private native void nativeKillApplicationProcess(long nativeProxy, String processName, int uid);

    private native void nativeSetUserIsMonkey(long nativeProxy, boolean monkey);

    private native void nativeFinishHeavyWeightApp(long nativeProxy);

    private native boolean nativeConvertFromTranslucent(long nativeProxy, IBinder token);

    private native boolean nativeConvertToTranslucent(long nativeProxy, IBinder token, ActivityOptions options);

    private native void nativeNotifyActivityDrawn(long nativeProxy, IBinder token);

    private native ActivityOptions nativeGetActivityOptions(long nativeProxy, IBinder token);

    private native void nativeBootAnimationComplete(long nativeProxy);

    private native void nativeSetImmersive(long nativeProxy, IBinder token, boolean immersive);

    private native boolean nativeIsImmersive(long nativeProxy, IBinder token);

    private native boolean nativeIsTopActivityImmersive(long nativeProxy);

    private native boolean nativeIsTopOfTask(long nativeProxy, IBinder token);

    private native void nativeCrashApplication(long nativeProxy, int uid, int initialPid, String packageName, String message);

    private native String nativeGetProviderMimeType(long nativeProxy, Uri uri, int userId);

    private native IBinder nativeNewUriPermissionOwner(long nativeProxy, String name);

    private native void nativeGrantUriPermissionFromOwner(long nativeProxy, IBinder owner, int fromUid, String targetPkg,
            Uri uri, int mode, int sourceUserId, int targetUserId);

    private native void nativeRevokeUriPermissionFromOwner(long nativeProxy, IBinder owner, Uri uri, int mode, int userId);

    private native int nativeCheckGrantUriPermission(long nativeProxy, int callingUid, String targetPkg, Uri uri, int modeFlags, int userId);

    private native boolean nativeDumpHeap(long nativeProxy, String process, int userId, boolean managed,
            String path, ParcelFileDescriptor fd);

    private native int nativeStartActivities(long nativeProxy, IApplicationThread caller, String callingPackage, Intent[] intents,
            String[] resolvedTypes, IBinder resultTo, Bundle options, int userId);

    private native int nativeGetFrontActivityScreenCompatMode(long nativeProxy);

    private native void nativeSetFrontActivityScreenCompatMode(long nativeProxy, int mode);

    private native int nativeGetPackageScreenCompatMode(long nativeProxy, String packageName);

    private native void nativeSetPackageScreenCompatMode(long nativeProxy, String packageName, int mode);

    private native boolean nativeGetPackageAskScreenCompat(long nativeProxy, String packageName);

    private native void nativeSetPackageAskScreenCompat(long nativeProxy, String packageName, boolean ask);

    private native boolean nativeSwitchUser(long nativeProxy, int userId);

    private native boolean nativeStartUserInBackground(long nativeProxy, int userid);

    private native int nativeStopUser(long nativeProxy, int userId, IStopUserCallback callback);

    private native boolean nativeIsUserRunning(long nativeProxy, int userId, boolean orStopping);

    private native int[] nativeGetRunningUserIds(long nativeProxy);

    private native boolean nativeRemoveTask(long nativeProxy, int taskId, int flags);

    private native void nativeRegisterProcessObserver(long nativeProxy, IProcessObserver observer);

    private native void nativeUnregisterProcessObserver(long nativeProxy, IProcessObserver observer);

    private native boolean nativeIsIntentSenderTargetedToPackage(long nativeProxy, IIntentSender sender);

    private native boolean nativeIsIntentSenderAnActivity(long nativeProxy, IIntentSender sender);

    private native Intent nativeGetIntentForIntentSender(long nativeProxy, IIntentSender sender);

    private native String nativeGetTagForIntentSender(long nativeProxy, IIntentSender sender, String prefix);

    private native void nativeShowBootMessage(long nativeProxy, CharSequence msg, boolean always);

    private native void nativeKeyguardWaitingForActivityDrawn(long nativeProxy);

    private native boolean nativeShouldUpRecreateTask(long nativeProxy, IBinder token, String destAffinity);

    private native boolean nativeNavigateUpTo(long nativeProxy, IBinder token, Intent target, int resultCode, Intent resultData);

    private native int nativeGetLaunchedFromUid(long nativeProxy, IBinder activityToken);

    private native String nativeGetLaunchedFromPackage(long nativeProxy, IBinder activityToken);

    private native void nativeRegisterUserSwitchObserver(long nativeProxy, IUserSwitchObserver observer);

    private native void nativeUnregisterUserSwitchObserver(long nativeProxy, IUserSwitchObserver observer);

    private native void nativeRequestBugReport(long nativeProxy);

    private native long nativeInputDispatchingTimedOut(long nativeProxy, int pid, boolean aboveSystem, String reason);

    private native Bundle nativeGetAssistContextExtras(long nativeProxy, int requestType);

    private native void nativeReportAssistContextExtras(long nativeProxy, IBinder token, Bundle extras);

    private native boolean nativeLaunchAssistIntent(long nativeProxy, Intent intent, int requestType, String hint, int userHandle);

    private native void nativeKillUid(long nativeProxy, int uid, String reason);

    private native void nativeHang(long nativeProxy, IBinder who, boolean allowRestart);

    private native void nativeReportActivityFullyDrawn(long nativeProxy, IBinder token);

    private native void nativeRestart(long nativeProxy);

    private native void nativePerformIdleMaintenance(long nativeProxy);

    private native IActivityContainer nativeCreateActivityContainer(long nativeProxy, IBinder parentActivityToken, IActivityContainerCallback callback);

    private native void nativeDeleteActivityContainer(long nativeProxy, IActivityContainer container);

    private native int nativeGetActivityDisplayId(long nativeProxy, IBinder activityToken);

    private native IBinder nativeGetHomeActivityToken(long nativeProxy);

    private native void nativeStartLockTaskModeOnCurrent(long nativeProxy);

    private native void nativeStartLockTaskMode(long nativeProxy, int taskId);

    private native void nativeStartLockTaskMode(long nativeProxy, IBinder token);

    private native void nativeStopLockTaskMode(long nativeProxy);

    private native void nativeStopLockTaskModeOnCurrent(long nativeProxy);

    private native boolean nativeIsInLockTaskMode(long nativeProxy);

    private native void nativeSetTaskDescription(long nativeProxy, IBinder token, ActivityManager.TaskDescription values);

    private native Bitmap nativeGetTaskDescriptionIcon(long nativeProxy, String filename);

    private native boolean nativeRequestVisibleBehind(long nativeProxy, IBinder token, boolean visible);

    private native boolean nativeIsBackgroundVisibleBehind(long nativeProxy, IBinder token);

    private native void nativeBackgroundResourcesReleased(long nativeProxy, IBinder token);

    private native void nativeNotifyLaunchTaskBehindComplete(long nativeProxy, IBinder token);

    private native void nativeNotifyEnterAnimationComplete(long nativeProxy, IBinder token);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElActivityManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    public int startActivity(IApplicationThread caller, String callingPackage, Intent intent,
            String resolvedType, IBinder resultTo, String resultWho, int requestCode, int flags,
            ProfilerInfo profilerInfo, Bundle options) throws RemoteException {
        return nativeStartActivity(mNativeProxy, caller, callingPackage, intent,
            resolvedType, resultTo, resultWho, requestCode, flags, profilerInfo, options);
    }

    public int startActivityFromRecents(int taskId, Bundle options) throws RemoteException {
        return nativeStartActivityFromRecents(mNativeProxy, taskId, options);
    }

    public boolean finishActivity(IBinder token, int resultCode, Intent resultData, boolean finishTask) throws RemoteException {
        return nativeFinishActivity(mNativeProxy, token, resultCode, resultData, finishTask);
    }

    public boolean willActivityBeVisible(IBinder token) throws RemoteException {
        return nativeWillActivityBeVisible(mNativeProxy, token);
    }

    public void attachApplication(IApplicationThread app) throws RemoteException
    {
        nativeAttachApplication(mNativeProxy, app, app.hashCode());
    }

    public void activityResumed(IBinder token) throws RemoteException
    {
        nativeActivityResumed(mNativeProxy, token);
    }
    public void activityPaused(IBinder token) throws RemoteException
    {
        nativeActivityPaused(mNativeProxy, token);
    }

    public void activityDestroyed(IBinder token) throws RemoteException
    {
        nativeActivityDestroyed(mNativeProxy, token);
    }

    public void activityIdle(IBinder token, Configuration config, boolean stopProfiling)
            throws RemoteException
    {
        nativeActivityIdle(mNativeProxy, token, config, stopProfiling);
    }

    public void publishContentProviders(IApplicationThread caller,
            List<ContentProviderHolder> providers) throws RemoteException
    {
        nativePublishContentProviders(mNativeProxy, caller, providers);
    }

    public Intent registerReceiver(IApplicationThread caller, String packageName,
            IIntentReceiver receiver,
            IntentFilter filter, String perm, int userId) throws RemoteException
    {
        return nativeRegisterReceiver(mNativeProxy, caller, packageName, receiver, filter, perm, userId);
    }

    public void unregisterReceiver(IIntentReceiver receiver) throws RemoteException
    {
        nativeUnregisterReceiver(mNativeProxy, receiver.hashCode());
    }

    public ContentProviderHolder getContentProvider(IApplicationThread caller,
            String name, int userId, boolean stable) throws RemoteException {
        return nativeGetContentProvider(mNativeProxy, caller, name, userId, stable);
    }

    public void setRequestedOrientation(IBinder token, int requestedOrientation)
            throws RemoteException {
        nativeSetRequestedOrientation(mNativeProxy, token, requestedOrientation);
    }

    public int getRequestedOrientation(IBinder token) throws RemoteException {
        return nativeGetRequestedOrientation(mNativeProxy, token);
    }

    public void activityStopped(IBinder token, Bundle state,
            PersistableBundle persistentState, CharSequence description) throws RemoteException
    {
        nativeActivityStopped(mNativeProxy, token, state, persistentState, description);
    }

    public void getMemoryInfo(ActivityManager.MemoryInfo outInfo) throws RemoteException {
        nativeGetMemoryInfo(mNativeProxy, outInfo);
    }

    public List<ActivityManager.RunningAppProcessInfo> getRunningAppProcesses()
            throws RemoteException {
        return nativeGetRunningAppProcesses(mNativeProxy);
    }

    public List<RunningServiceInfo> getServices(int maxNum, int flags) throws RemoteException {
        return nativeGetServices(mNativeProxy, maxNum, flags);
    }

    public int checkPermission(String permission, int pid, int uid)
            throws RemoteException {
        return nativeCheckPermission(mNativeProxy, permission, pid, uid);
    }

    public UserInfo getCurrentUser() throws RemoteException {
        return nativeGetCurrentUser(mNativeProxy);
    }

    public boolean refContentProvider(IBinder connection, int stable, int unstable)
            throws RemoteException {
        return nativeRefContentProvider(mNativeProxy, connection, stable, unstable);
    }

    public int broadcastIntent(IApplicationThread caller,
            Intent intent, String resolvedType,  IIntentReceiver resultTo,
            int resultCode, String resultData, Bundle map,
            String requiredPermission, int appOp,  boolean serialized,
            boolean sticky, int userId) throws RemoteException
    {
        return nativeBroadcastIntent(mNativeProxy, caller, intent, resolvedType, resultTo == null ? 0 : resultTo.hashCode(),
            resultCode, resultData, map, requiredPermission, appOp, serialized, sticky, userId);
    }

    public void handleApplicationCrash(IBinder app,
            ApplicationErrorReport.CrashInfo crashInfo) throws RemoteException
    {
        nativeHandleApplicationCrash(mNativeProxy, app.hashCode(), crashInfo);
    }

    public ComponentName startService(IApplicationThread caller, Intent service,
            String resolvedType, int userId) throws RemoteException
    {
        return nativeStartService(mNativeProxy, caller, service, resolvedType, userId);
    }
    public int stopService(IApplicationThread caller, Intent service,
            String resolvedType, int userId) throws RemoteException
    {
        return nativeStopService(mNativeProxy, caller, service, resolvedType, userId);
    }

    public void serviceDoneExecuting(IBinder token, int type, int startId,
            int res) throws RemoteException {
        nativeServiceDoneExecuting(mNativeProxy, token, type, startId, res);
    }

    public IIntentSender getIntentSender(int type,
            String packageName, IBinder token, String resultWho,
            int requestCode, Intent[] intents, String[] resolvedTypes, int flags,
            Bundle options, int userId) throws RemoteException {
        return nativeGetIntentSender(mNativeProxy, type, packageName, token, resultWho, requestCode, intents,
            resolvedTypes, flags, options, userId);
    }

    public boolean moveActivityTaskToBack(IBinder token, boolean nonRoot)
            throws RemoteException {
        return nativeMoveActivityTaskToBack(mNativeProxy, token, nonRoot);
    }

    public Configuration getConfiguration() throws RemoteException
    {
        return nativeGetConfiguration(mNativeProxy);
    }

    public void setServiceForeground(ComponentName className, IBinder token,
            int id, Notification notification, boolean removeNotification) throws RemoteException {
        nativeSetServiceForeground(mNativeProxy, className, token, id, notification, removeNotification);
    }

    public void removeContentProvider(IBinder connection, boolean stable) throws RemoteException {
        nativeRemoveContentProvider(mNativeProxy, connection, stable);
    }

    public void updateConfiguration(Configuration values) throws RemoteException
    {
        nativeUpdateConfiguration(mNativeProxy, values);
    }

    public int bindService(IApplicationThread caller, IBinder token,
            Intent service, String resolvedType, IServiceConnection connection,
            int flags, int userId) throws RemoteException {
        return nativeBindService(mNativeProxy, caller, token, service, resolvedType, connection, flags, userId);
    }

    public boolean unbindService(IServiceConnection connection) throws RemoteException
    {
        return nativeUnbindService(mNativeProxy, connection);
    }

    public ConfigurationInfo getDeviceConfigurationInfo() throws RemoteException
    {
        return nativeGetDeviceConfigurationInfo(mNativeProxy);
    }

    public boolean stopServiceToken(ComponentName className, IBinder token,
            int startId) throws RemoteException {
        return nativeStopServiceToken(mNativeProxy, className, token, startId);
    }

    public void publishService(IBinder token,
            Intent intent, IBinder service) throws RemoteException {
        nativePublishService(mNativeProxy, token, intent, service);
    }

    public List<IAppTask> getAppTasks(String callingPackage) throws RemoteException {
        return nativeGetAppTasks(mNativeProxy, callingPackage);
    }

    public int addAppTask(IBinder activityToken, Intent intent,
            ActivityManager.TaskDescription description, Bitmap thumbnail) throws RemoteException {
        return nativeAddAppTask(mNativeProxy, activityToken, intent, description, thumbnail);
    }

    public Point getAppTaskThumbnailSize() throws RemoteException {
        return nativeGetAppTaskThumbnailSize(mNativeProxy);
    }

    public List<RunningTaskInfo> getTasks(int maxNum, int flags) throws RemoteException {
        return nativeGetTasks(mNativeProxy, maxNum, flags);
    }

    public void finishReceiver(IBinder who, int resultCode, String resultData, Bundle map, boolean abortBroadcast, int flags) throws RemoteException
    {
        nativeFinishReceiver(mNativeProxy, who, resultCode, resultData, map, abortBroadcast, flags);
    }

    public void overridePendingTransition(IBinder token, String packageName,
            int enterAnim, int exitAnim) throws RemoteException {
        nativeOverridePendingTransition(mNativeProxy, token, packageName, enterAnim, exitAnim);
    }

    public Debug.MemoryInfo[] getProcessMemoryInfo(int[] pids)
            throws RemoteException {
        return nativeGetProcessMemoryInfo(mNativeProxy, pids);
    }

    public boolean isUserAMonkey() throws RemoteException {
        return nativeIsUserAMonkey(mNativeProxy);
    }

    public void forceStopPackage(String packageName, int userId) throws RemoteException {
        nativeForceStopPackage(mNativeProxy, packageName, userId);
    }

    public void updatePersistentConfiguration(Configuration values) throws RemoteException
    {
        nativeUpdatePersistentConfiguration(mNativeProxy, values);
    }

    public long[] getProcessPss(int[] pids) throws RemoteException {
        return nativeGetProcessPss(mNativeProxy, pids);
    }

    public boolean clearApplicationUserData(final String packageName,
            final IPackageDataObserver observer, final int userId) throws RemoteException {
        return nativeClearApplicationUserData(mNativeProxy, packageName, observer, userId);
    }

    public void killBackgroundProcesses(String packageName, int userId) throws RemoteException {
        nativeKillBackgroundProcesses(mNativeProxy, packageName, userId);
    }

    public int startActivityAsUser(IApplicationThread caller, String callingPackage, Intent intent,
            String resolvedType, IBinder resultTo, String resultWho, int requestCode,
            int startFlags, ProfilerInfo profilerInfo, Bundle options, int userId) throws RemoteException {
        return nativeStartActivityAsUser(mNativeProxy, caller, callingPackage, intent, resolvedType, resultTo,
            resultWho, requestCode, startFlags, profilerInfo, options, userId);
    }

    public int startActivityAsCaller(IApplicationThread caller, String callingPackage, Intent intent,
            String resolvedType, IBinder resultTo, String resultWho, int requestCode,
            int flags, ProfilerInfo profilerInfo, Bundle options, int userId) throws RemoteException {
        return nativeStartActivityAsCaller(mNativeProxy, caller, callingPackage, intent,
            resolvedType, resultTo, resultWho, requestCode, flags, profilerInfo, options, userId);
    }

    public WaitResult startActivityAndWait(IApplicationThread caller, String callingPackage, Intent intent,
            String resolvedType, IBinder resultTo, String resultWho, int requestCode,
            int startFlags, ProfilerInfo profilerInfo, Bundle options, int userId) throws RemoteException {
        return nativeStartActivityAndWait(mNativeProxy, caller, callingPackage, intent, resolvedType, resultTo,
            resultWho, requestCode, startFlags, profilerInfo, options, userId);
    }

    public int startActivityWithConfig(IApplicationThread caller, String callingPackage, Intent intent,
            String resolvedType, IBinder resultTo, String resultWho, int requestCode,
            int startFlags, Configuration config, Bundle options, int userId) throws RemoteException {
        return nativeStartActivityWithConfig(mNativeProxy, caller, callingPackage, intent, resolvedType, resultTo,
            resultWho, requestCode, startFlags, config, options, userId);
    }

    public int startActivityIntentSender(IApplicationThread caller,
            IntentSender intent, Intent fillInIntent, String resolvedType,
            IBinder resultTo, String resultWho, int requestCode,
            int flagsMask, int flagsValues, Bundle options) throws RemoteException {
        return nativeStartActivityIntentSender(mNativeProxy, caller, intent, fillInIntent, resolvedType, resultTo, resultWho, requestCode, flagsMask, flagsValues, options);
    }

    public int startVoiceActivity(String callingPackage, int callingPid, int callingUid,
            Intent intent, String resolvedType, IVoiceInteractionSession session,
            IVoiceInteractor interactor, int flags, ProfilerInfo profilerInfo, Bundle options,
            int userId) throws RemoteException {
        return nativeStartVoiceActivity(mNativeProxy, callingPackage, callingPid, callingUid, intent, resolvedType, session,
            interactor, flags, profilerInfo, options, userId);
    }

    public boolean startNextMatchingActivity(IBinder callingActivity,
            Intent intent, Bundle options) throws RemoteException {
        return nativeStartNextMatchingActivity(mNativeProxy, callingActivity, intent, options);
    }

    public void finishSubActivity(IBinder token, String resultWho, int requestCode) throws RemoteException {
        nativeFinishSubActivity(mNativeProxy, token, resultWho, requestCode);
    }

    public boolean finishActivityAffinity(IBinder token) throws RemoteException {
        return nativeFinishActivityAffinity(mNativeProxy, token);
    }

    public void finishVoiceTask(IVoiceInteractionSession session) throws RemoteException
    {
        nativeFinishVoiceTask(mNativeProxy, session);
    }

    public boolean releaseActivityInstance(IBinder token) throws RemoteException
    {
        return nativeReleaseActivityInstance(mNativeProxy, token);
    }

    public void releaseSomeActivities(IApplicationThread app) throws RemoteException
    {
        nativeReleaseSomeActivities(mNativeProxy, app);
    }

    public void unbroadcastIntent(IApplicationThread caller, Intent intent, int userId)
            throws RemoteException {
        nativeUnbroadcastIntent(mNativeProxy, caller, intent, userId);
    }

    public void activitySlept(IBinder token) throws RemoteException {
        nativeActivitySlept(mNativeProxy, token);
    }

    public String getCallingPackage(IBinder token) throws RemoteException {
        return nativeGetCallingPackage(mNativeProxy, token);
    }

    public String getCallingPackageForBroadcast(boolean foreground) throws RemoteException {
        return nativeGetCallingPackageForBroadcast(mNativeProxy, foreground);
    }

    public ComponentName getCallingActivity(IBinder token)
            throws RemoteException {
        return nativeGetCallingActivity(mNativeProxy, token);
    }

    public List<ActivityManager.RecentTaskInfo> getRecentTasks(int maxNum,
            int flags, int userId) throws RemoteException {
        return nativeGetRecentTasks(mNativeProxy, maxNum, flags, userId);
    }

    public ActivityManager.TaskThumbnail getTaskThumbnail(int id) throws RemoteException {
        return nativeGetTaskThumbnail(mNativeProxy, id);
    }

    public List<ActivityManager.ProcessErrorStateInfo> getProcessesInErrorState()
            throws RemoteException {
        return nativeGetProcessesInErrorState(mNativeProxy);
    }

    public List<ApplicationInfo> getRunningExternalApplications()
            throws RemoteException {
        return nativeGetRunningExternalApplications(mNativeProxy);
    }

    public void moveTaskToFront(int task, int flags, Bundle options) throws RemoteException {
        nativeMoveTaskToFront(mNativeProxy, task, flags, options);
    }

    public void moveTaskToBack(int task) throws RemoteException {
        nativeMoveTaskToBack(mNativeProxy, task);
    }

    public void moveTaskBackwards(int task) throws RemoteException {
        nativeMoveTaskBackwards(mNativeProxy, task);
    }

    public void moveTaskToStack(int taskId, int stackId, boolean toTop) throws RemoteException {
        nativeMoveTaskToStack(mNativeProxy, taskId, stackId, toTop);
    }

    public void resizeStack(int stackId, Rect bounds) throws RemoteException {
        nativeResizeStack(mNativeProxy, stackId, bounds);
    }

    public List<StackInfo> getAllStackInfos() throws RemoteException {
        return nativeGetAllStackInfos(mNativeProxy);
    }

    public StackInfo getStackInfo(int stackId) throws RemoteException {
        return nativeGetStackInfo(mNativeProxy, stackId);
    }

    public boolean isInHomeStack(int taskId) throws RemoteException {
        return nativeIsInHomeStack(mNativeProxy, taskId);
    }

    public void setFocusedStack(int stackId) throws RemoteException {
        nativeSetFocusedStack(mNativeProxy, stackId);
    }

    public int getTaskForActivity(IBinder token, boolean onlyRoot) throws RemoteException {
        return nativeGetTaskForActivity(mNativeProxy, token, onlyRoot);
    }

    public ContentProviderHolder getContentProviderExternal(String name, int userId, IBinder token)
            throws RemoteException {
        return nativeGetContentProviderExternal(mNativeProxy, name, userId, token);
    }

    public void unstableProviderDied(IBinder connection) throws RemoteException {
        nativeUnstableProviderDied(mNativeProxy, connection);
    }

    public void appNotRespondingViaProvider(IBinder connection) throws RemoteException {
        nativeAppNotRespondingViaProvider(mNativeProxy, connection);
    }

    public void removeContentProviderExternal(String name, IBinder token) throws RemoteException {
        nativeRemoveContentProviderExternal(mNativeProxy, name, token);
    }

    public PendingIntent getRunningServiceControlPanel(ComponentName service)
            throws RemoteException
    {
        return nativeGetRunningServiceControlPanel(mNativeProxy, service);
    }

    public void unbindFinished(IBinder token, Intent intent, boolean doRebind)
            throws RemoteException {
        nativeUnbindFinished(mNativeProxy, token, intent, doRebind);
    }

    public IBinder peekService(Intent service, String resolvedType) throws RemoteException {
        return nativePeekService(mNativeProxy, service, resolvedType);
    }

    public boolean bindBackupAgent(ApplicationInfo app, int backupRestoreMode)
            throws RemoteException {
        return nativeBindBackupAgent(mNativeProxy, app, backupRestoreMode);
    }

    public void clearPendingBackup() throws RemoteException {
        nativeClearPendingBackup(mNativeProxy);
    }

    public void backupAgentCreated(String packageName, IBinder agent) throws RemoteException {
        nativeBackupAgentCreated(mNativeProxy, packageName, agent);
    }

    public void unbindBackupAgent(ApplicationInfo app) throws RemoteException {
        nativeUnbindBackupAgent(mNativeProxy, app);
    }

    public boolean startInstrumentation(ComponentName className, String profileFile,
            int flags, Bundle arguments, IInstrumentationWatcher watcher,
            IUiAutomationConnection connection, int userId, String abiOverride)
            throws RemoteException {
        return nativeStartInstrumentation(mNativeProxy, className, profileFile, flags,
            arguments, watcher, connection, userId, abiOverride);
    }

    public void finishInstrumentation(IApplicationThread target,
            int resultCode, Bundle results) throws RemoteException {
        nativeFinishInstrumentation(mNativeProxy, target, resultCode, results);
    }

    public ComponentName getActivityClassForToken(IBinder token)
            throws RemoteException {
        return nativeGetActivityClassForToken(mNativeProxy, token);
    }

    public String getPackageForToken(IBinder token) throws RemoteException {
        return nativeGetPackageForToken(mNativeProxy, token);
    }

    public void cancelIntentSender(IIntentSender sender) throws RemoteException {
        nativeCancelIntentSender(mNativeProxy, sender);
    }

    public String getPackageForIntentSender(IIntentSender sender) throws RemoteException {
        return nativeGetPackageForIntentSender(mNativeProxy, sender);
    }

    public int getUidForIntentSender(IIntentSender sender) throws RemoteException {
        return nativeGetUidForIntentSender(mNativeProxy, sender);
    }

    public int handleIncomingUser(int callingPid, int callingUid, int userId, boolean allowAll,
            boolean requireFull, String name, String callerPackage) throws RemoteException {
        return nativeHandleIncomingUser(mNativeProxy, callingPid, callingUid, userId, allowAll,
            requireFull, name, callerPackage);
    }

    public void setProcessLimit(int max) throws RemoteException {
        nativeSetProcessLimit(mNativeProxy, max);
    }

    public int getProcessLimit() throws RemoteException {
        return nativeGetProcessLimit(mNativeProxy);
    }

    public void setProcessForeground(IBinder token, int pid, boolean isForeground) throws RemoteException {
        nativeSetProcessForeground(mNativeProxy, token, pid, isForeground);
    }

    public int checkUriPermission(Uri uri, int pid, int uid, int mode, int userId) throws RemoteException {
        return nativeCheckUriPermission(mNativeProxy, uri, pid, uid, mode, userId);
    }

    public void grantUriPermission(IApplicationThread caller, String targetPkg,
            Uri uri, int mode, int userId) throws RemoteException {
        nativeGrantUriPermission(mNativeProxy, caller, targetPkg, uri, mode, userId);
    }

    public void revokeUriPermission(IApplicationThread caller, Uri uri,
            int mode, int userId) throws RemoteException {
        nativeRevokeUriPermission(mNativeProxy, caller, uri, mode, userId);
    }

    public void takePersistableUriPermission(Uri uri, int modeFlags, int userId) throws RemoteException {
        nativeTakePersistableUriPermission(mNativeProxy, uri, modeFlags, userId);
    }

    public void releasePersistableUriPermission(Uri uri, int modeFlags, int userId) throws RemoteException {
        nativeReleasePersistableUriPermission(mNativeProxy, uri, modeFlags, userId);
    }

    public ParceledListSlice<UriPermission> getPersistedUriPermissions(
            String packageName, boolean incoming) throws RemoteException {
        return nativeGetPersistedUriPermissions(mNativeProxy, packageName, incoming);
    }

    public void showWaitingForDebugger(IApplicationThread who, boolean waiting) throws RemoteException {
        nativeShowWaitingForDebugger(mNativeProxy, who, waiting);
    }

    public void unhandledBack() throws RemoteException {
        nativeUnhandledBack(mNativeProxy);
    }

    public ParcelFileDescriptor openContentUri(Uri uri) throws RemoteException {
        return nativeOpenContentUri(mNativeProxy, uri);
    }

    public void setLockScreenShown(boolean shown) throws RemoteException {
        nativeSetLockScreenShown(mNativeProxy, shown);
    }

    public void setDebugApp(String packageName, boolean waitForDebugger, boolean persistent) throws RemoteException {
        nativeSetDebugApp(mNativeProxy, packageName, waitForDebugger, persistent);
    }

    public void setAlwaysFinish(boolean enabled) throws RemoteException {
        nativeSetAlwaysFinish(mNativeProxy, enabled);
    }

    public void setActivityController(IActivityController watcher) throws RemoteException {
        nativeSetActivityController(mNativeProxy, watcher);
    }

    public void enterSafeMode() throws RemoteException {
        nativeEnterSafeMode(mNativeProxy);
    }

    public void noteWakeupAlarm(IIntentSender sender, int sourceUid, String sourcePkg) throws RemoteException {
        nativeNoteWakeupAlarm(mNativeProxy, sender, sourceUid, sourcePkg);
    }

    public boolean killPids(int[] pids, String reason, boolean secure) throws RemoteException {
        return nativeKillPids(mNativeProxy, pids, reason, secure);
    }

    @Override
    public boolean killProcessesBelowForeground(String reason) throws RemoteException {
        return nativeKillProcessesBelowForeground(mNativeProxy, reason);
    }

    public boolean testIsSystemReady()
    {
        /* this base class version is never called */
        return true;
    }

    public boolean handleApplicationWtf(IBinder app, String tag, boolean system, ApplicationErrorReport.CrashInfo crashInfo) throws RemoteException {
        return nativeHandleApplicationWtf(mNativeProxy, app, tag, system, crashInfo);
    }

    public void handleApplicationStrictModeViolation(IBinder app,
            int violationMask, StrictMode.ViolationInfo info) throws RemoteException {
        nativeHandleApplicationStrictModeViolation(mNativeProxy, app, violationMask, info);
    }

    public void signalPersistentProcesses(int sig) throws RemoteException {
        nativeSignalPersistentProcesses(mNativeProxy, sig);
    }

    public void killAllBackgroundProcesses() throws RemoteException {
        nativeKillAllBackgroundProcesses(mNativeProxy);
    }

    public void getMyMemoryState(ActivityManager.RunningAppProcessInfo outInfo) throws RemoteException {
        nativeGetMyMemoryState(mNativeProxy, outInfo);
    }

    public boolean profileControl(String process, int userId, boolean start,
            ProfilerInfo profilerInfo, int profileType) throws RemoteException {
        return nativeProfileControl(mNativeProxy, process, userId, start, profilerInfo, profileType);
    }

    public boolean shutdown(int timeout) throws RemoteException {
        return nativeShutdown(mNativeProxy, timeout);
    }

    public void stopAppSwitches() throws RemoteException {
        nativeStopAppSwitches(mNativeProxy);
    }

    public void resumeAppSwitches() throws RemoteException {
        nativeResumeAppSwitches(mNativeProxy);
    }

    public void addPackageDependency(String packageName) throws RemoteException {
        nativeAddPackageDependency(mNativeProxy, packageName);
    }

    public void killApplicationWithAppId(String pkg, int appid, String reason) throws RemoteException {
        nativeKillApplicationWithAppId(mNativeProxy, pkg, appid, reason);
    }

    public void closeSystemDialogs(String reason) throws RemoteException {
        nativeCloseSystemDialogs(mNativeProxy, reason);
    }

    public void killApplicationProcess(String processName, int uid) throws RemoteException {
        nativeKillApplicationProcess(mNativeProxy, processName, uid);
    }

    public void setUserIsMonkey(boolean monkey) throws RemoteException {
        nativeSetUserIsMonkey(mNativeProxy, monkey);
    }

    public void finishHeavyWeightApp() throws RemoteException {
        nativeFinishHeavyWeightApp(mNativeProxy);
    }

    public boolean convertFromTranslucent(IBinder token) throws RemoteException {
        return nativeConvertFromTranslucent(mNativeProxy, token);
    }

    public boolean convertToTranslucent(IBinder token, ActivityOptions options) throws RemoteException {
        return nativeConvertToTranslucent(mNativeProxy, token, options);
    }

    public void notifyActivityDrawn(IBinder token) throws RemoteException {
        nativeNotifyActivityDrawn(mNativeProxy, token);
    }

    public ActivityOptions getActivityOptions(IBinder token) throws RemoteException {
        return nativeGetActivityOptions(mNativeProxy, token);
    }

    public void bootAnimationComplete() throws RemoteException {
        nativeBootAnimationComplete(mNativeProxy);
    }


    public void setImmersive(IBinder token, boolean immersive) throws RemoteException {
        nativeSetImmersive(mNativeProxy, token, immersive);
    }

    public boolean isImmersive(IBinder token) throws RemoteException {
        return nativeIsImmersive(mNativeProxy, token);
    }

    public boolean isTopActivityImmersive() throws RemoteException {
        return nativeIsTopActivityImmersive(mNativeProxy);
    }

    public boolean isTopOfTask(IBinder token) throws RemoteException {
        return nativeIsTopOfTask(mNativeProxy, token);
    }

    public void crashApplication(int uid, int initialPid, String packageName,
            String message) throws RemoteException {
        nativeCrashApplication(mNativeProxy, uid, initialPid, packageName, message);
    }

    public String getProviderMimeType(Uri uri, int userId) throws RemoteException {
        return nativeGetProviderMimeType(mNativeProxy, uri, userId);
    }

    public IBinder newUriPermissionOwner(String name) throws RemoteException {
        return nativeNewUriPermissionOwner(mNativeProxy, name);
    }

    public void grantUriPermissionFromOwner(IBinder owner, int fromUid, String targetPkg,
            Uri uri, int mode, int sourceUserId, int targetUserId) throws RemoteException {
        nativeGrantUriPermissionFromOwner(mNativeProxy, owner, fromUid, targetPkg, uri, mode, sourceUserId, targetUserId);
    }

    public void revokeUriPermissionFromOwner(IBinder owner, Uri uri, int mode, int userId) throws RemoteException {
        nativeRevokeUriPermissionFromOwner(mNativeProxy, owner, uri, mode, userId);
    }

    public int checkGrantUriPermission(int callingUid, String targetPkg,
            Uri uri, int modeFlags, int userId) throws RemoteException {
        return nativeCheckGrantUriPermission(mNativeProxy, callingUid, targetPkg, uri, modeFlags, userId);
    }

    public boolean dumpHeap(String process, int userId, boolean managed,
            String path, ParcelFileDescriptor fd) throws RemoteException {
        return nativeDumpHeap(mNativeProxy, process, userId, managed, path, fd);
    }

    public int startActivities(IApplicationThread caller, String callingPackage, Intent[] intents, String[] resolvedTypes,
            IBinder resultTo, Bundle options, int userId) throws RemoteException {
        return nativeStartActivities(mNativeProxy, caller, callingPackage, intents, resolvedTypes, resultTo, options, userId);
    }

    public int getFrontActivityScreenCompatMode() throws RemoteException {
        return nativeGetFrontActivityScreenCompatMode(mNativeProxy);
    }

    public void setFrontActivityScreenCompatMode(int mode) throws RemoteException {
        nativeSetFrontActivityScreenCompatMode(mNativeProxy, mode);
    }

    public int getPackageScreenCompatMode(String packageName) throws RemoteException {
        return nativeGetPackageScreenCompatMode(mNativeProxy, packageName);
    }

    public void setPackageScreenCompatMode(String packageName, int mode) throws RemoteException {
        nativeSetPackageScreenCompatMode(mNativeProxy, packageName, mode);
    }

    public boolean getPackageAskScreenCompat(String packageName) throws RemoteException {
        return nativeGetPackageAskScreenCompat(mNativeProxy, packageName);
    }

    public void setPackageAskScreenCompat(String packageName, boolean ask) throws RemoteException {
        nativeSetPackageAskScreenCompat(mNativeProxy, packageName, ask);
    }

    public boolean switchUser(int userId) throws RemoteException {
        return nativeSwitchUser(mNativeProxy, userId);
    }

    public boolean startUserInBackground(int userid) throws RemoteException {
        return nativeStartUserInBackground(mNativeProxy, userid);
    }

    public int stopUser(int userId, IStopUserCallback callback) throws RemoteException {
        return nativeStopUser(mNativeProxy, userId, callback);
    }

    public boolean isUserRunning(int userId, boolean orStopping) throws RemoteException {
        return nativeIsUserRunning(mNativeProxy, userId, orStopping);
    }

    public int[] getRunningUserIds() throws RemoteException {
        return nativeGetRunningUserIds(mNativeProxy);
    }

    public boolean removeTask(int taskId, int flags) throws RemoteException {
        return nativeRemoveTask(mNativeProxy, taskId, flags);
    }

    public void registerProcessObserver(IProcessObserver observer) throws RemoteException {
        nativeRegisterProcessObserver(mNativeProxy, observer);
    }

    public void unregisterProcessObserver(IProcessObserver observer) throws RemoteException {
        nativeUnregisterProcessObserver(mNativeProxy, observer);
    }

    public boolean isIntentSenderTargetedToPackage(IIntentSender sender) throws RemoteException {
        return nativeIsIntentSenderTargetedToPackage(mNativeProxy, sender);
    }

    public boolean isIntentSenderAnActivity(IIntentSender sender) throws RemoteException {
        return nativeIsIntentSenderAnActivity(mNativeProxy, sender);
    }

    public Intent getIntentForIntentSender(IIntentSender sender) throws RemoteException {
        return nativeGetIntentForIntentSender(mNativeProxy, sender);
    }

    public String getTagForIntentSender(IIntentSender sender, String prefix) throws RemoteException {
        return nativeGetTagForIntentSender(mNativeProxy, sender, prefix);
    }

    public void showBootMessage(CharSequence msg, boolean always) throws RemoteException {
        nativeShowBootMessage(mNativeProxy, msg, always);
    }

    public void keyguardWaitingForActivityDrawn() throws RemoteException {
        nativeKeyguardWaitingForActivityDrawn(mNativeProxy);
    }

    public boolean shouldUpRecreateTask(IBinder token, String destAffinity) throws RemoteException {
        return nativeShouldUpRecreateTask(mNativeProxy, token, destAffinity);
    }

    public boolean navigateUpTo(IBinder token, Intent target, int resultCode, Intent resultData) throws RemoteException {
        return nativeNavigateUpTo(mNativeProxy, token, target, resultCode, resultData);
    }

    public int getLaunchedFromUid(IBinder activityToken) throws RemoteException {
        return nativeGetLaunchedFromUid(mNativeProxy, activityToken);
    }

    public String getLaunchedFromPackage(IBinder activityToken) throws RemoteException {
        return nativeGetLaunchedFromPackage(mNativeProxy, activityToken);
    }

    public void registerUserSwitchObserver(IUserSwitchObserver observer) throws RemoteException {
        nativeRegisterUserSwitchObserver(mNativeProxy, observer);
    }

    public void unregisterUserSwitchObserver(IUserSwitchObserver observer) throws RemoteException {
        nativeUnregisterUserSwitchObserver(mNativeProxy, observer);
    }

    public void requestBugReport() throws RemoteException {
        nativeRequestBugReport(mNativeProxy);
    }

    public long inputDispatchingTimedOut(int pid, boolean aboveSystem, String reason) throws RemoteException {
        return nativeInputDispatchingTimedOut(mNativeProxy, pid, aboveSystem, reason);
    }

    public Bundle getAssistContextExtras(int requestType) throws RemoteException {
        return nativeGetAssistContextExtras(mNativeProxy, requestType);
    }

    public void reportAssistContextExtras(IBinder token, Bundle extras) throws RemoteException {
        nativeReportAssistContextExtras(mNativeProxy, token, extras);
    }

    public boolean launchAssistIntent(Intent intent, int requestType, String hint, int userHandle) throws RemoteException {
        return nativeLaunchAssistIntent(mNativeProxy, intent, requestType, hint, userHandle);
    }

    public void killUid(int uid, String reason) throws RemoteException {
        nativeKillUid(mNativeProxy, uid, reason);
    }

    public void hang(IBinder who, boolean allowRestart) throws RemoteException {
        nativeHang(mNativeProxy, who, allowRestart);
    }

    public void reportActivityFullyDrawn(IBinder token) throws RemoteException {
        nativeReportActivityFullyDrawn(mNativeProxy, token);
    }

    public void restart() throws RemoteException {
        nativeRestart(mNativeProxy);
    }

    public void performIdleMaintenance() throws RemoteException {
        nativePerformIdleMaintenance(mNativeProxy);
    }

    public IActivityContainer createActivityContainer(IBinder parentActivityToken, IActivityContainerCallback callback) throws RemoteException {
        return nativeCreateActivityContainer(mNativeProxy, parentActivityToken, callback);
    }

    public void deleteActivityContainer(IActivityContainer container) throws RemoteException {
        nativeDeleteActivityContainer(mNativeProxy, container);
    }

    public int getActivityDisplayId(IBinder activityToken) throws RemoteException {
        return nativeGetActivityDisplayId(mNativeProxy, activityToken);
    }

    public IBinder getHomeActivityToken() throws RemoteException {
        return nativeGetHomeActivityToken(mNativeProxy);
    }

    public void startLockTaskModeOnCurrent() throws RemoteException {
        nativeStartLockTaskModeOnCurrent(mNativeProxy);
    }

    public void startLockTaskMode(int taskId) throws RemoteException {
        nativeStartLockTaskMode(mNativeProxy, taskId);
    }

    public void startLockTaskMode(IBinder token) throws RemoteException {
        nativeStartLockTaskMode(mNativeProxy, token);
    }

    public void stopLockTaskMode() throws RemoteException {
        nativeStopLockTaskMode(mNativeProxy);
    }

    public void stopLockTaskModeOnCurrent() throws RemoteException {
        nativeStopLockTaskModeOnCurrent(mNativeProxy);
    }

    public boolean isInLockTaskMode() throws RemoteException {
        return nativeIsInLockTaskMode(mNativeProxy);
    }

    public void setTaskDescription(IBinder token, ActivityManager.TaskDescription values) throws RemoteException {
        nativeSetTaskDescription(mNativeProxy, token, values);
    }

    public Bitmap getTaskDescriptionIcon(String filename) throws RemoteException {
        return nativeGetTaskDescriptionIcon(mNativeProxy, filename);
    }

    public boolean requestVisibleBehind(IBinder token, boolean visible) throws RemoteException {
        return nativeRequestVisibleBehind(mNativeProxy, token, visible);
    }

    public boolean isBackgroundVisibleBehind(IBinder token) throws RemoteException {
        return nativeIsBackgroundVisibleBehind(mNativeProxy, token);
    }

    public void backgroundResourcesReleased(IBinder token) throws RemoteException {
        nativeBackgroundResourcesReleased(mNativeProxy, token);
    }

    public void notifyLaunchTaskBehindComplete(IBinder token) throws RemoteException {
        nativeNotifyLaunchTaskBehindComplete(mNativeProxy, token);
    }

    public void notifyEnterAnimationComplete(IBinder token) throws RemoteException {
        nativeNotifyEnterAnimationComplete(mNativeProxy, token);
    }

    public IBinder asBinder() {
        return this;
    }
}
