
package android.app;

import android.app.IServiceConnection;
import android.content.ComponentName;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.HashMap;

/** @hide */
public class ElIServiceConnectionProxy {

    private IServiceConnection mSerConnection;

    private static HashMap<Integer, IBinder> mServicesMap = new HashMap<Integer, IBinder>();

    public ElIServiceConnectionProxy(IServiceConnection serConnection) {
        mSerConnection = serConnection;
    }

    public static void addService(int key, IBinder service) {
        mServicesMap.put(key, service);
    }

    public static IBinder getService(int key) {
        return mServicesMap.get(key);
    }

    public static void removeService(int key) {
        mServicesMap.remove(key);
    }

    public void connected(ComponentName name, int key) {
        IBinder service = ElIServiceConnectionProxy.getService(key);

        try {
            mSerConnection.connected(name, service);
        } catch (RemoteException e) {
            Log.e("ElIServiceConnectionProxy", "name: " + name);
            Log.e("ElIServiceConnectionProxy", "Error call connected: " + e);
        }

        // mServicesMap.remove(key);
    }

    public void connected(ComponentName name, IBinder service) {
        try {
            mSerConnection.connected(name, service);
        } catch (RemoteException e) {
            Log.e("ElIServiceConnectionProxy", "name: " + name);
            Log.e("ElIServiceConnectionProxy", "Error call connected: " + e);
        }

        // mServicesMap.remove(key);
    }
}

