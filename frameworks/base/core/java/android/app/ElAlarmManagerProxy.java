
package android.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.WorkSource;
import android.util.Log;

/**
 * {@hide}
 */
public class ElAlarmManagerProxy extends IAlarmManager.Stub {
    private long mNativeProxy;

    private static final native void nativeFinalize(long nativeProxy);

    private native void nativeSetTimeZone(long nativeProxy, String zone);

    private native void nativeRemove(long nativeProxy, PendingIntent operation);

    private native void nativeSet(long nativeProxy, int type, long triggerAtTime, long windowLength,
            long interval, PendingIntent operation, WorkSource workSource, AlarmManager.AlarmClockInfo alarmClock);

    private native boolean nativeSetTime(long nativeProxy, long millis);

    private native AlarmManager.AlarmClockInfo nativeGetNextAlarmClock(long nativeProxy, int userId);

    private native void nativeUpdateBlockedUids(long nativeProxy, int uid, boolean isBlocked);

    public ElAlarmManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    @Override
    public void setTimeZone(String zone) throws RemoteException {
        nativeSetTimeZone(mNativeProxy, zone);
    }

    @Override
    public void remove(PendingIntent operation) throws RemoteException {
        nativeRemove(mNativeProxy, operation);
    }

    @Override
    public void set(int type, long triggerAtTime, long windowLength,
            long interval, PendingIntent operation, WorkSource workSource, AlarmManager.AlarmClockInfo alarmClock) throws RemoteException {
        nativeSet(mNativeProxy, type, triggerAtTime, windowLength, interval, operation, workSource, alarmClock);
    }

    @Override
    public boolean setTime(long millis) throws RemoteException {
        return nativeSetTime(mNativeProxy, millis);
    }

    @Override
    public AlarmManager.AlarmClockInfo getNextAlarmClock(int userId) throws RemoteException {
        return nativeGetNextAlarmClock(mNativeProxy, userId);
    }

    @Override
    public void updateBlockedUids(int uid, boolean isBlocked) throws RemoteException {
        nativeUpdateBlockedUids(mNativeProxy, uid, isBlocked);
    }
}


