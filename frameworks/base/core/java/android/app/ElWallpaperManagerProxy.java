/**
 * Copyright (c) 2008, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.app.IWallpaperManagerCallback;
import android.app.WallpaperInfo;
import android.content.ComponentName;
import android.os.RemoteException;
import android.util.Log;

/** @hide */
public class ElWallpaperManagerProxy extends IWallpaperManager.Stub {

    private long mNativeProxy;

    private native ParcelFileDescriptor nativeSetWallpaper(long proxy, String name);

    private native ParcelFileDescriptor nativeSetKeyguardWallpaper(long proxy, String name);

    private native void nativeSetWallpaperComponent(long proxy, ComponentName name);

    private native ParcelFileDescriptor nativeGetWallpaper(long proxy, IWallpaperManagerCallback cb,
            Bundle outParams);

    private native ParcelFileDescriptor nativeGetKeyguardWallpaper(long proxy, IWallpaperManagerCallback cb,
            Bundle outParams);

    private native WallpaperInfo nativeGetWallpaperInfo(long proxy);

    private native void nativeClearWallpaper(long proxy);

    private native void nativeClearKeyguardWallpaper(long proxy);

    private native boolean nativeHasNamedWallpaper(long proxy, String name);

    private native void nativeSetDimensionHints(long proxy, int width, int height);

    private native int nativeGetWidthHint(long proxy);

    private native int nativeGetHeightHint(long proxy);

    private native void nativeSetDisplayPadding(long proxy, Rect padding);

    private native String nativeGetName(long proxy);

    private native void nativeSettingsRestored(long proxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElWallpaperManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public ParcelFileDescriptor setWallpaper(String name) throws RemoteException {
        return nativeSetWallpaper(mNativeProxy, name);
    }

    @Override
    public ParcelFileDescriptor setKeyguardWallpaper(String name) throws RemoteException {
        return nativeSetKeyguardWallpaper(mNativeProxy, name);
    }

    @Override
    public void setWallpaperComponent(ComponentName name) throws RemoteException {
        nativeSetWallpaperComponent(mNativeProxy, name);
    }

    @Override
    public ParcelFileDescriptor getWallpaper(IWallpaperManagerCallback cb,
            Bundle outParams) throws RemoteException {
        return nativeGetWallpaper(mNativeProxy, cb, outParams);
    }

    @Override
    public ParcelFileDescriptor getKeyguardWallpaper(IWallpaperManagerCallback cb,
            Bundle outParams) throws RemoteException {
        return nativeGetKeyguardWallpaper(mNativeProxy, cb, outParams);
    }

    @Override
    public WallpaperInfo getWallpaperInfo() throws RemoteException {
        return nativeGetWallpaperInfo(mNativeProxy);
    }

    @Override
    public void clearWallpaper() throws RemoteException {
        nativeClearWallpaper(mNativeProxy);
    }

    @Override
    public void clearKeyguardWallpaper() throws RemoteException {
        nativeClearKeyguardWallpaper(mNativeProxy);
    }

    @Override
    public boolean hasNamedWallpaper(String name) throws RemoteException {
        return nativeHasNamedWallpaper(mNativeProxy, name);
    }

    @Override
    public void setDimensionHints(int width, int height) throws RemoteException {
        nativeSetDimensionHints(mNativeProxy, width, height);
    }

    @Override
    public int getWidthHint() throws RemoteException {
        return nativeGetWidthHint(mNativeProxy);
    }

    @Override
    public int getHeightHint() throws RemoteException {
        return nativeGetHeightHint(mNativeProxy);
    }

    @Override
    public void setDisplayPadding(Rect padding) throws RemoteException {
        nativeSetDisplayPadding(mNativeProxy, padding);
    }

    @Override
    public String getName() throws RemoteException {
        return nativeGetName(mNativeProxy);
    }

    @Override
    public void settingsRestored() throws RemoteException {
        nativeSettingsRestored(mNativeProxy);
    }
}
