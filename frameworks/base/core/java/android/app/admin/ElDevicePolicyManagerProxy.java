/*
**
** Copyright 2010, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

package android.app.admin;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ProxyInfo;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.RemoteCallback;
import android.os.UserHandle;
import java.util.List;

/**
 * {@hide}
 */
public class ElDevicePolicyManagerProxy extends IDevicePolicyManager.Stub {
    private long mNativeProxy;

    private native boolean nativePackageHasActiveAdmins(long proxy, String packageName, int userHandle);
    private native void nativeSetPasswordQuality(long proxy, ComponentName who, int quality, int userHandle);
    private native int nativeGetPasswordQuality(long proxy, ComponentName who, int userHandle);
    private native void nativeSetPasswordMinimumLength(long proxy, ComponentName who, int length, int userHandle);
    private native int nativeGetPasswordMinimumLength(long proxy, ComponentName who, int userHandle);
    private native void nativeSetPasswordMinimumUpperCase(long proxy, ComponentName who, int length, int userHandle);
    private native int nativeGetPasswordMinimumUpperCase(long proxy, ComponentName who, int userHandle);
    private native void nativeSetPasswordMinimumLowerCase(long proxy, ComponentName who, int length, int userHandle);
    private native int nativeGetPasswordMinimumLowerCase(long proxy, ComponentName who, int userHandle);
    private native void nativeSetPasswordMinimumLetters(long proxy, ComponentName who, int length, int userHandle);
    private native int nativeGetPasswordMinimumLetters(long proxy, ComponentName who, int userHandle) ;
    private native void nativeSetPasswordMinimumNumeric(long proxy, ComponentName who, int length, int userHandle);
    private native int nativeGetPasswordMinimumNumeric(long proxy, ComponentName who, int userHandle);
    private native void nativeSetPasswordMinimumSymbols(long proxy, ComponentName who, int length, int userHandle);
    private native int nativeGetPasswordMinimumSymbols(long proxy, ComponentName who, int userHandle);
    private native void nativeSetPasswordMinimumNonLetter(long proxy, ComponentName who, int length, int userHandle);
    private native int nativeGetPasswordMinimumNonLetter(long proxy, ComponentName who, int userHandle);
    private native void nativeSetPasswordHistoryLength(long proxy, ComponentName who, int length, int userHandle);
    private native int nativeGetPasswordHistoryLength(long proxy, ComponentName who, int userHandle);
    private native void nativeSetPasswordExpirationTimeout(long proxy, ComponentName who, long expiration, int userHandle);
    private native long nativeGetPasswordExpirationTimeout(long proxy, ComponentName who, int userHandle);
    private native long nativeGetPasswordExpiration(long proxy, ComponentName who, int userHandle);
    private native boolean nativeIsActivePasswordSufficient(long proxy, int userHandle) ;
    private native int nativeGetCurrentFailedPasswordAttempts(long proxy, int userHandle);
    private native void nativeSetMaximumFailedPasswordsForWipe(long proxy, ComponentName admin, int num, int userHandle);
    private native int nativeGetMaximumFailedPasswordsForWipe(long proxy, ComponentName admin, int userHandle);
    private native boolean nativeResetPassword(long proxy, String password, int flags, int userHandle);
    private native void nativeSetMaximumTimeToLock(long proxy, ComponentName who, long timeMs, int userHandle);
    private native long nativeGetMaximumTimeToLock(long proxy, ComponentName who, int userHandle);
    private native void nativeLockNow(long proxy);
    private native void nativeWipeData(long proxy, int flags, int userHandle);
    private native ComponentName nativeSetGlobalProxy(long proxy, ComponentName admin, String proxySpec, String exclusionList, int userHandle);
    private native ComponentName nativeGetGlobalProxyAdmin(long proxy, int userHandle);
    private native void nativeSetRecommendedGlobalProxy(long proxy, ComponentName admin, ProxyInfo proxyInfo);
    private native int nativeSetStorageEncryption(long proxy, ComponentName who, boolean encrypt, int userHandle);
    private native boolean nativeGetStorageEncryption(long proxy, ComponentName who, int userHandle);
    private native int nativeGetStorageEncryptionStatus(long proxy, int userHandle);
    private native void nativeSetCameraDisabled(long proxy, ComponentName who, boolean disabled, int userHandle);
    private native boolean nativeGetCameraDisabled(long proxy, ComponentName who, int userHandle);
    private native void nativeSetScreenCaptureDisabled(long proxy, ComponentName who, int userHandle, boolean disabled);
    private native boolean nativeGetScreenCaptureDisabled(long proxy, ComponentName who, int userHandle);
    private native void nativeSetKeyguardDisabledFeatures(long proxy, ComponentName who, int which, int userHandle);
    private native int nativeGetKeyguardDisabledFeatures(long proxy, ComponentName who, int userHandle);
    private native void nativeSetActiveAdmin(long proxy, ComponentName policyReceiver, boolean refreshing, int userHandle);
    private native boolean nativeIsAdminActive(long proxy, ComponentName policyReceiver, int userHandle);
    private native List<ComponentName> nativeGetActiveAdmins(long proxy, int userHandle);
    private native void nativeGetRemoveWarning(long proxy, ComponentName policyReceiver, RemoteCallback result, int userHandle);
    private native void nativeRemoveActiveAdmin(long proxy, ComponentName policyReceiver, int userHandle);
    private native boolean nativeHasGrantedPolicy(long proxy, ComponentName policyReceiver, int usesPolicy, int userHandle);
    private native void nativeSetActivePasswordState(long proxy, int quality, int length, int letters, int uppercase, int lowercase,
        int numbers, int symbols, int nonletter, int userHandle);
    private native void nativeReportFailedPasswordAttempt(long proxy, int userHandle);
    private native void nativeReportSuccessfulPasswordAttempt(long proxy, int userHandle);

    private native boolean nativeSetDeviceOwner(long proxy, String packageName, String ownerName);
    private native boolean nativeIsDeviceOwner(long proxy, String packageName);
    private native String nativeGetDeviceOwner(long proxy);
    private native String nativeGetDeviceOwnerName(long proxy);
    private native void nativeClearDeviceOwner(long proxy, String packageName);
    private native boolean nativeSetProfileOwner(long proxy, ComponentName who, String ownerName, int userHandle);
    private native ComponentName nativeGetProfileOwner(long proxy, int userHandle);
    private native String nativeGetProfileOwnerName(long proxy, int userHandle);
    private native void nativeSetProfileEnabled(long proxy, ComponentName who);
    private native void nativeSetProfileName(long proxy, ComponentName who, String profileName);
    private native void nativeClearProfileOwner(long proxy, ComponentName who);
    private native boolean nativeHasUserSetupCompleted(long proxy);
    private native boolean nativeInstallCaCert(long proxy, ComponentName admin, byte[] certBuffer);
    private native void nativeUninstallCaCert(long proxy, ComponentName admin, String alias);
    private native void nativeEnforceCanManageCaCerts(long proxy, ComponentName admin);
    private native boolean nativeInstallKeyPair(long proxy, ComponentName who, byte[] privKeyBuffer, byte[] certBuffer, String alias);
    private native void nativeAddPersistentPreferredActivity(long proxy, ComponentName admin, IntentFilter filter, ComponentName activity);
    private native void nativeClearPackagePersistentPreferredActivities(long proxy, ComponentName admin, String packageName);
    private native void nativeSetApplicationRestrictions(long proxy, ComponentName who, String packageName, Bundle settings);
    private native Bundle nativeGetApplicationRestrictions(long proxy, ComponentName who, String packageName);
    private native void nativeSetRestrictionsProvider(long proxy, ComponentName who, ComponentName provider);
    private native ComponentName nativeGetRestrictionsProvider(long proxy, int userHandle);
    private native void nativeSetUserRestriction(long proxy, ComponentName who, String key, boolean enable);
    private native void nativeAddCrossProfileIntentFilter(long proxy, ComponentName admin, IntentFilter filter, int flags);
    private native void nativeClearCrossProfileIntentFilters(long proxy, ComponentName admin);
    private native boolean nativeSetPermittedAccessibilityServices(long proxy, ComponentName admin,List packageList);
    private native List nativeGetPermittedAccessibilityServices(long proxy, ComponentName admin);
    private native List nativeGetPermittedAccessibilityServicesForUser(long proxy, int userId);
    private native boolean nativeSetPermittedInputMethods(long proxy, ComponentName admin,List packageList);
    private native List nativeGetPermittedInputMethods(long proxy, ComponentName admin);
    private native List nativeGetPermittedInputMethodsForCurrentUser(long proxy);
    private native boolean nativeSetApplicationHidden(long proxy, ComponentName admin, String packageName, boolean hidden);
    private native boolean nativeIsApplicationHidden(long proxy, ComponentName admin, String packageName);
    private native UserHandle nativeCreateUser(long proxy, ComponentName who, String name);
    private native UserHandle nativeCreateAndInitializeUser(long proxy, ComponentName who, String name, String profileOwnerName, ComponentName profileOwnerComponent, Bundle adminExtras);
    private native boolean nativeRemoveUser(long proxy, ComponentName who, UserHandle userHandle);
    private native boolean nativeSwitchUser(long proxy, ComponentName who, UserHandle userHandle);
    private native void nativeEnableSystemApp(long proxy, ComponentName admin, String packageName);
    private native int nativeEnableSystemAppWithIntent(long proxy, ComponentName admin, Intent intent);
    private native void nativeSetAccountManagementDisabled(long proxy, ComponentName who, String accountType, boolean disabled);
    private native String[] nativeGetAccountTypesWithManagementDisabled(long proxy);
    private native String[] nativeGetAccountTypesWithManagementDisabledAsUser(long proxy, int userId);
    private native void nativeSetLockTaskPackages(long proxy, ComponentName who, String[] packages);
    private native String[] nativeGetLockTaskPackages(long proxy, ComponentName who);
    private native boolean nativeIsLockTaskPermitted(long proxy, String pkg);
    private native void nativeSetGlobalSetting(long proxy, ComponentName who, String setting, String value);
    private native void nativeSetSecureSetting(long proxy, ComponentName who, String setting, String value);
    private native void nativeSetMasterVolumeMuted(long proxy, ComponentName admin, boolean on);
    private native boolean nativeIsMasterVolumeMuted(long proxy, ComponentName admin);
    private native void nativeNotifyLockTaskModeChanged(long proxy, boolean isEnabled, String pkg, int userId);
    private native void nativeSetUninstallBlocked(long proxy, ComponentName admin, String packageName, boolean uninstallBlocked);
    private native boolean nativeIsUninstallBlocked(long proxy, ComponentName admin, String packageName);
    private native void nativeSetCrossProfileCallerIdDisabled(long proxy, ComponentName who, boolean disabled);
    private native boolean nativeGetCrossProfileCallerIdDisabled(long proxy, ComponentName who);
    private native boolean nativeGetCrossProfileCallerIdDisabledForUser(long proxy, int userId);
    private native void nativeSetTrustAgentFeaturesEnabled(long proxy, ComponentName admin, ComponentName agent, List<String> features, int userId);
    private native List<String> nativeGetTrustAgentFeaturesEnabled(long proxy, ComponentName admin, ComponentName agent, int userId);
    private native boolean nativeAddCrossProfileWidgetProvider(long proxy, ComponentName admin, String packageName);
    private native boolean nativeRemoveCrossProfileWidgetProvider(long proxy, ComponentName admin, String packageName);
    private native List<String> nativeGetCrossProfileWidgetProviders(long proxy, ComponentName admin);
    private native void nativeSetAutoTimeRequired(long proxy, ComponentName who, int userHandle, boolean required);
    private native boolean nativeGetAutoTimeRequired(long proxy);
    private native boolean nativeRequireSecureKeyguard(long proxy, int userHandle);
    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElDevicePolicyManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public boolean packageHasActiveAdmins(String packageName, int userHandle) throws RemoteException {
        return nativePackageHasActiveAdmins(mNativeProxy, packageName, userHandle);
    }

    @Override
    public void setPasswordQuality(ComponentName who, int quality, int userHandle) throws RemoteException {
        nativeSetPasswordQuality(mNativeProxy, who, quality, userHandle);
    }

    @Override
    public int getPasswordQuality(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordQuality(mNativeProxy, who, userHandle);
    }

    @Override
    public void setPasswordMinimumLength(ComponentName who, int length, int userHandle) throws RemoteException {
        nativeSetPasswordMinimumLength(mNativeProxy, who, length, userHandle);
    }

    @Override
    public int getPasswordMinimumLength(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordMinimumLength(mNativeProxy, who, userHandle);
    }

    @Override
    public void setPasswordMinimumUpperCase(ComponentName who, int length, int userHandle) throws RemoteException {
        nativeSetPasswordMinimumUpperCase(mNativeProxy, who, length, userHandle);
    }

    @Override
    public int getPasswordMinimumUpperCase(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordMinimumUpperCase(mNativeProxy, who, userHandle);
    }

    @Override
    public void setPasswordMinimumLowerCase(ComponentName who, int length, int userHandle) throws RemoteException {
        nativeSetPasswordMinimumLowerCase(mNativeProxy, who, length, userHandle);
    }

    @Override
    public int getPasswordMinimumLowerCase(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordMinimumLowerCase(mNativeProxy, who, userHandle);
    }

    @Override
    public void setPasswordMinimumLetters(ComponentName who, int length, int userHandle) throws RemoteException {
        nativeSetPasswordMinimumLetters(mNativeProxy, who, length, userHandle);
    }

    @Override
    public int getPasswordMinimumLetters(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordMinimumLetters(mNativeProxy, who, userHandle);
    }

    @Override
    public void setPasswordMinimumNumeric(ComponentName who, int length, int userHandle) throws RemoteException {
        nativeSetPasswordMinimumNumeric(mNativeProxy, who, length, userHandle);
    }

    @Override
    public int getPasswordMinimumNumeric(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordMinimumNumeric(mNativeProxy, who, userHandle);
    }

    @Override
    public void setPasswordMinimumSymbols(ComponentName who, int length, int userHandle) throws RemoteException {
        nativeSetPasswordMinimumSymbols(mNativeProxy, who, length, userHandle);
    }

    @Override
    public int getPasswordMinimumSymbols(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordMinimumSymbols(mNativeProxy, who, userHandle);
    }

    @Override
    public void setPasswordMinimumNonLetter(ComponentName who, int length, int userHandle) throws RemoteException {
        nativeSetPasswordMinimumNonLetter(mNativeProxy, who, length, userHandle);
    }

    @Override
    public int getPasswordMinimumNonLetter(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordMinimumNonLetter(mNativeProxy, who, userHandle);
    }

    @Override
    public void setPasswordHistoryLength(ComponentName who, int length, int userHandle) throws RemoteException {
        nativeSetPasswordHistoryLength(mNativeProxy, who, length, userHandle);
    }

    @Override
    public int getPasswordHistoryLength(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordHistoryLength(mNativeProxy, who, userHandle);
    }

    @Override
    public void setPasswordExpirationTimeout(ComponentName who, long expiration, int userHandle) throws RemoteException {
        nativeSetPasswordExpirationTimeout(mNativeProxy, who, expiration, userHandle);
    }

    @Override
    public long getPasswordExpirationTimeout(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordExpirationTimeout(mNativeProxy, who, userHandle);
    }

    @Override
    public long getPasswordExpiration(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetPasswordExpiration(mNativeProxy, who, userHandle);
    }

    @Override
    public boolean isActivePasswordSufficient(int userHandle) throws RemoteException {
        return nativeIsActivePasswordSufficient(mNativeProxy, userHandle);
    }

    @Override
    public int getCurrentFailedPasswordAttempts(int userHandle) throws RemoteException {
        return nativeGetCurrentFailedPasswordAttempts(mNativeProxy, userHandle);
    }

    @Override
    public void setMaximumFailedPasswordsForWipe(ComponentName admin, int num, int userHandle) throws RemoteException {
        nativeSetMaximumFailedPasswordsForWipe(mNativeProxy, admin, num, userHandle);
    }

    @Override
    public int getMaximumFailedPasswordsForWipe(ComponentName admin, int userHandle) throws RemoteException {
        return nativeGetMaximumFailedPasswordsForWipe(mNativeProxy, admin, userHandle);
    }

    @Override
    public boolean resetPassword(String password, int flags, int userHandle) throws RemoteException {
        return nativeResetPassword(mNativeProxy, password, flags, userHandle);
    }

    @Override
    public void setMaximumTimeToLock(ComponentName who, long timeMs, int userHandle) throws RemoteException {
        nativeSetMaximumTimeToLock(mNativeProxy, who, timeMs, userHandle);
    }

    @Override
    public long getMaximumTimeToLock(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetMaximumTimeToLock(mNativeProxy, who, userHandle);
    }

    @Override
    public void lockNow() throws RemoteException {
        nativeLockNow(mNativeProxy);
    }

    @Override
    public void wipeData(int flags, int userHandle) throws RemoteException {
        nativeWipeData(mNativeProxy, flags, userHandle);
    }

    @Override
    public ComponentName setGlobalProxy(ComponentName admin, String proxySpec, String exclusionList, int userHandle) throws RemoteException {
        return nativeSetGlobalProxy(mNativeProxy, admin, proxySpec, exclusionList, userHandle);
    }

    @Override
    public ComponentName getGlobalProxyAdmin(int userHandle) throws RemoteException {
        return nativeGetGlobalProxyAdmin(mNativeProxy, userHandle);
    }

    @Override
    public void setRecommendedGlobalProxy(ComponentName admin, ProxyInfo proxyInfo) throws RemoteException {
        nativeSetRecommendedGlobalProxy(mNativeProxy, admin, proxyInfo);
    }

    @Override
    public int setStorageEncryption(ComponentName who, boolean encrypt, int userHandle) throws RemoteException {
        return nativeSetStorageEncryption(mNativeProxy, who, encrypt, userHandle);
    }

    @Override
    public boolean getStorageEncryption(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetStorageEncryption(mNativeProxy, who, userHandle);
    }

    @Override
    public int getStorageEncryptionStatus(int userHandle) throws RemoteException {
        return nativeGetStorageEncryptionStatus(mNativeProxy, userHandle);
    }

    @Override
    public void setCameraDisabled(ComponentName who, boolean disabled, int userHandle) throws RemoteException {
        nativeSetCameraDisabled(mNativeProxy, who, disabled, userHandle);
    }

    @Override
    public boolean getCameraDisabled(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetCameraDisabled(mNativeProxy, who, userHandle);
    }

    @Override
    public void setScreenCaptureDisabled(ComponentName who, int userHandle, boolean disabled) throws RemoteException {
        nativeSetScreenCaptureDisabled(mNativeProxy, who, userHandle, disabled);
    }

    @Override
    public boolean getScreenCaptureDisabled(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetScreenCaptureDisabled(mNativeProxy, who, userHandle);
    }

    @Override
    public void setKeyguardDisabledFeatures(ComponentName who, int which, int userHandle) throws RemoteException {
        nativeSetKeyguardDisabledFeatures(mNativeProxy, who, which, userHandle);
    }

    @Override
    public int getKeyguardDisabledFeatures(ComponentName who, int userHandle) throws RemoteException {
        return nativeGetKeyguardDisabledFeatures(mNativeProxy, who, userHandle);
    }

    @Override
    public void setActiveAdmin(ComponentName policyReceiver, boolean refreshing, int userHandle) throws RemoteException {
        nativeSetActiveAdmin(mNativeProxy, policyReceiver, refreshing, userHandle);
    }

    @Override
    public boolean isAdminActive(ComponentName policyReceiver, int userHandle) throws RemoteException {
        return nativeIsAdminActive(mNativeProxy, policyReceiver, userHandle);
    }

    @Override
    public List<ComponentName> getActiveAdmins(int userHandle) throws RemoteException {
        return nativeGetActiveAdmins(mNativeProxy,userHandle);
    }

    @Override
    public void getRemoveWarning(ComponentName policyReceiver, RemoteCallback result, int userHandle) throws RemoteException {
        nativeGetRemoveWarning(mNativeProxy, policyReceiver, result, userHandle);
    }

    @Override
    public void removeActiveAdmin(ComponentName policyReceiver, int userHandle) throws RemoteException {
        nativeRemoveActiveAdmin(mNativeProxy, policyReceiver, userHandle);
    }

    @Override
    public boolean hasGrantedPolicy(ComponentName policyReceiver, int usesPolicy, int userHandle) throws RemoteException {
        return nativeHasGrantedPolicy(mNativeProxy, policyReceiver, usesPolicy, userHandle);
    }

    @Override
    public void setActivePasswordState(int quality, int length, int letters, int uppercase, int lowercase,
        int numbers, int symbols, int nonletter, int userHandle) throws RemoteException {
        nativeSetActivePasswordState(mNativeProxy, quality, length,letters, uppercase, lowercase, numbers, symbols,
            nonletter, userHandle);
    }

    @Override
    public void reportFailedPasswordAttempt(int userHandle) throws RemoteException {
        nativeReportFailedPasswordAttempt(mNativeProxy, userHandle);
    }

    @Override
    public void reportSuccessfulPasswordAttempt(int userHandle) throws RemoteException {
        nativeReportSuccessfulPasswordAttempt(mNativeProxy, userHandle);
    }

    @Override
    public boolean setDeviceOwner(String packageName, String ownerName) throws RemoteException {
        return nativeSetDeviceOwner(mNativeProxy, packageName, ownerName);
    }

    @Override
    public boolean isDeviceOwner(String packageName) throws RemoteException {
        return nativeIsDeviceOwner(mNativeProxy, packageName);
    }

    @Override
    public String getDeviceOwner() throws RemoteException {
        return nativeGetDeviceOwner(mNativeProxy);
    }

    @Override
    public String getDeviceOwnerName() throws RemoteException {
        return nativeGetDeviceOwnerName(mNativeProxy);
    }

    @Override
    public void clearDeviceOwner(String packageName) throws RemoteException {
        nativeClearDeviceOwner(mNativeProxy, packageName);
    }

    @Override
    public boolean setProfileOwner(ComponentName who, String ownerName, int userHandle) throws RemoteException {
        return nativeSetProfileOwner(mNativeProxy, who, ownerName, userHandle);
    }

    @Override
    public ComponentName getProfileOwner(int userHandle) throws RemoteException {
        return nativeGetProfileOwner(mNativeProxy, userHandle);
    }

    @Override
    public String getProfileOwnerName(int userHandle) throws RemoteException {
        return nativeGetProfileOwnerName(mNativeProxy, userHandle);
    }

    @Override
    public void setProfileEnabled(ComponentName who) throws RemoteException {
        nativeSetProfileEnabled(mNativeProxy, who);
    }

    @Override
    public void setProfileName(ComponentName who, String profileName) throws RemoteException {
        nativeSetProfileName(mNativeProxy, who, profileName);
    }

    @Override
    public void clearProfileOwner(ComponentName who) throws RemoteException {
        nativeClearProfileOwner(mNativeProxy, who);
    }

    @Override
    public boolean hasUserSetupCompleted() throws RemoteException {
        return nativeHasUserSetupCompleted(mNativeProxy);
    }

    @Override
    public boolean installCaCert(ComponentName admin, byte[] certBuffer) throws RemoteException {
        return nativeInstallCaCert(mNativeProxy, admin, certBuffer);
    }

    @Override
    public void uninstallCaCert(ComponentName admin, String alias) throws RemoteException {
        nativeUninstallCaCert(mNativeProxy, admin, alias);
    }

    @Override
    public void enforceCanManageCaCerts(ComponentName admin) throws RemoteException {
        nativeEnforceCanManageCaCerts(mNativeProxy, admin);
    }

    @Override
    public boolean installKeyPair(ComponentName who, byte[] privKeyBuffer, byte[] certBuffer, String alias) throws RemoteException {
        return nativeInstallKeyPair(mNativeProxy, who, privKeyBuffer, certBuffer, alias);
    }

    @Override
    public void addPersistentPreferredActivity(ComponentName admin, IntentFilter filter, ComponentName activity) throws RemoteException {
        nativeAddPersistentPreferredActivity(mNativeProxy, admin, filter, activity);
    }

    @Override
    public void clearPackagePersistentPreferredActivities(ComponentName admin, String packageName) throws RemoteException {
        nativeClearPackagePersistentPreferredActivities(mNativeProxy, admin, packageName);
    }

    @Override
    public void setApplicationRestrictions(ComponentName who, String packageName, Bundle settings) throws RemoteException {
        nativeSetApplicationRestrictions(mNativeProxy, who, packageName, settings);
    }

    @Override
    public Bundle getApplicationRestrictions(ComponentName who, String packageName) throws RemoteException {
        return nativeGetApplicationRestrictions(mNativeProxy, who, packageName);
    }

    @Override
    public void setRestrictionsProvider(ComponentName who, ComponentName provider) throws RemoteException {
        nativeSetRestrictionsProvider(mNativeProxy, who, provider);
    }

    @Override
    public ComponentName getRestrictionsProvider(int userHandle) throws RemoteException {
        return nativeGetRestrictionsProvider(mNativeProxy, userHandle);
    }

    @Override
    public void setUserRestriction(ComponentName who, String key, boolean enable) throws RemoteException {
        nativeSetUserRestriction(mNativeProxy, who, key, enable);
    }

    @Override
    public void addCrossProfileIntentFilter(ComponentName admin, IntentFilter filter, int flags) throws RemoteException {
        nativeAddCrossProfileIntentFilter(mNativeProxy, admin, filter, flags);
    }

    @Override
    public void clearCrossProfileIntentFilters(ComponentName admin) throws RemoteException {
        nativeClearCrossProfileIntentFilters(mNativeProxy, admin);
    }

    @Override
    public boolean setPermittedAccessibilityServices(ComponentName admin,List packageList) throws RemoteException {
        return nativeSetPermittedAccessibilityServices(mNativeProxy, admin, packageList);
    }

    @Override
    public List getPermittedAccessibilityServices(ComponentName admin) throws RemoteException {
        return nativeGetPermittedAccessibilityServices(mNativeProxy, admin);
    }

    @Override
    public List getPermittedAccessibilityServicesForUser(int userId) throws RemoteException {
        return nativeGetPermittedAccessibilityServicesForUser(mNativeProxy, userId);
    }

    @Override
    public boolean setPermittedInputMethods(ComponentName admin,List packageList) throws RemoteException {
        return nativeSetPermittedInputMethods(mNativeProxy, admin, packageList);
    }

    @Override
    public List getPermittedInputMethods(ComponentName admin) throws RemoteException {
        return nativeGetPermittedInputMethods(mNativeProxy, admin);
    }

    @Override
    public List getPermittedInputMethodsForCurrentUser() throws RemoteException {
        return nativeGetPermittedInputMethodsForCurrentUser(mNativeProxy);
    }

    @Override
    public boolean setApplicationHidden(ComponentName admin, String packageName, boolean hidden) throws RemoteException {
        return nativeSetApplicationHidden(mNativeProxy, admin, packageName, hidden);
    }

    @Override
    public boolean isApplicationHidden(ComponentName admin, String packageName) throws RemoteException {
        return nativeIsApplicationHidden(mNativeProxy, admin, packageName);
    }

    @Override
    public UserHandle createUser(ComponentName who, String name) throws RemoteException {
        return nativeCreateUser(mNativeProxy, who, name);
    }

    @Override
    public UserHandle createAndInitializeUser(ComponentName who, String name, String profileOwnerName, ComponentName profileOwnerComponent, Bundle adminExtras) throws RemoteException {
        return nativeCreateAndInitializeUser(mNativeProxy, who, name, profileOwnerName, profileOwnerComponent, adminExtras);
    }

    @Override
    public boolean removeUser(ComponentName who, UserHandle userHandle) throws RemoteException {
        return nativeRemoveUser(mNativeProxy, who, userHandle);
    }

    @Override
    public boolean switchUser(ComponentName who, UserHandle userHandle) throws RemoteException {
        return nativeSwitchUser(mNativeProxy, who, userHandle);
    }

    @Override
    public void enableSystemApp(ComponentName admin, String packageName) throws RemoteException {
        nativeEnableSystemApp(mNativeProxy, admin, packageName);
    }

    @Override
    public int enableSystemAppWithIntent(ComponentName admin, Intent intent) throws RemoteException {
        return nativeEnableSystemAppWithIntent(mNativeProxy, admin, intent);
    }

    @Override
    public void setAccountManagementDisabled(ComponentName who, String accountType, boolean disabled) throws RemoteException {
        nativeSetAccountManagementDisabled(mNativeProxy, who, accountType, disabled);
    }

    @Override
    public String[] getAccountTypesWithManagementDisabled() throws RemoteException {
        return nativeGetAccountTypesWithManagementDisabled(mNativeProxy);
    }

    @Override
    public String[] getAccountTypesWithManagementDisabledAsUser(int userId) throws RemoteException {
        return nativeGetAccountTypesWithManagementDisabledAsUser(mNativeProxy, userId);
    }

    @Override
    public void setLockTaskPackages(ComponentName who, String[] packages) throws RemoteException {
        nativeSetLockTaskPackages(mNativeProxy, who, packages);
    }

    @Override
    public String[] getLockTaskPackages(ComponentName who) throws RemoteException {
        return nativeGetLockTaskPackages(mNativeProxy, who);
    }

    @Override
    public boolean isLockTaskPermitted(String pkg) throws RemoteException {
        return nativeIsLockTaskPermitted(mNativeProxy, pkg);
    }

    @Override
    public void setGlobalSetting(ComponentName who, String setting, String value) throws RemoteException {
        nativeSetGlobalSetting(mNativeProxy, who, setting, value);
    }

    @Override
    public void setSecureSetting(ComponentName who, String setting, String value) throws RemoteException {
        nativeSetSecureSetting(mNativeProxy, who, setting, value);
    }

    @Override
    public void setMasterVolumeMuted(ComponentName admin, boolean on) throws RemoteException {
        nativeSetMasterVolumeMuted(mNativeProxy, admin, on);
    }

    @Override
    public boolean isMasterVolumeMuted(ComponentName admin) throws RemoteException {
        return nativeIsMasterVolumeMuted(mNativeProxy, admin);
    }

    @Override
    public void notifyLockTaskModeChanged(boolean isEnabled, String pkg, int userId) throws RemoteException {
        nativeNotifyLockTaskModeChanged(mNativeProxy, isEnabled, pkg, userId);
    }

    @Override
    public void setUninstallBlocked(ComponentName admin, String packageName, boolean uninstallBlocked) throws RemoteException {
        nativeSetUninstallBlocked(mNativeProxy, admin, packageName, uninstallBlocked);
    }

    @Override
    public boolean isUninstallBlocked(ComponentName admin, String packageName) throws RemoteException {
        return nativeIsUninstallBlocked(mNativeProxy, admin, packageName);
    }

    @Override
    public void setCrossProfileCallerIdDisabled(ComponentName who, boolean disabled) throws RemoteException {
        nativeSetCrossProfileCallerIdDisabled(mNativeProxy, who, disabled);
    }

    @Override
    public boolean getCrossProfileCallerIdDisabled(ComponentName who) throws RemoteException {
        return nativeGetCrossProfileCallerIdDisabled(mNativeProxy, who);
    }

    @Override
    public boolean getCrossProfileCallerIdDisabledForUser(int userId) throws RemoteException {
        return nativeGetCrossProfileCallerIdDisabledForUser(mNativeProxy, userId);
    }

    @Override
    public void setTrustAgentFeaturesEnabled(ComponentName admin, ComponentName agent, List<String> features, int userId) throws RemoteException {
        nativeSetTrustAgentFeaturesEnabled(mNativeProxy, admin, agent, features, userId);
    }

    @Override
    public List<String> getTrustAgentFeaturesEnabled(ComponentName admin, ComponentName agent, int userId) throws RemoteException {
        return nativeGetTrustAgentFeaturesEnabled(mNativeProxy, admin, agent, userId);
    }

    @Override
    public boolean addCrossProfileWidgetProvider(ComponentName admin, String packageName) throws RemoteException {
        return nativeAddCrossProfileWidgetProvider(mNativeProxy, admin, packageName);
    }

    @Override
    public boolean removeCrossProfileWidgetProvider(ComponentName admin, String packageName) throws RemoteException {
        return nativeRemoveCrossProfileWidgetProvider(mNativeProxy, admin, packageName);
    }

    @Override
    public List<String> getCrossProfileWidgetProviders(ComponentName admin) throws RemoteException {
        return nativeGetCrossProfileWidgetProviders(mNativeProxy, admin);
    }

    @Override
    public void setAutoTimeRequired(ComponentName who, int userHandle, boolean required) throws RemoteException {
        nativeSetAutoTimeRequired(mNativeProxy, who, userHandle, required);
    }

    @Override
    public boolean getAutoTimeRequired() throws RemoteException {
        return nativeGetAutoTimeRequired(mNativeProxy);
    }

    @Override
    public boolean requireSecureKeyguard(int userHandle) throws RemoteException {
        return nativeRequireSecureKeyguard(mNativeProxy, userHandle);
    }
}
