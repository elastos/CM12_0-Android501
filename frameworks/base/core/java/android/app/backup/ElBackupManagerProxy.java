
package android.app.backup;

import android.app.backup.IFullBackupRestoreObserver;
import android.app.backup.IRestoreSession;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.content.Intent;

/**
 * {@hide}
 */
public class ElBackupManagerProxy extends IBackupManager.Stub {
    private long mNativeProxy;

    private native void nativeDataChanged(long proxy, String packageName) ;

    private native void nativeClearBackupData(long proxy, String transportName, String packageName);

    private native void nativeAgentConnected(long proxy, String packageName, IBinder agent);

    private native void nativeAgentDisconnected(long proxy, String packageName);

    private native void nativeRestoreAtInstall(long proxy, String packageName, int token);

    private native void nativeSetBackupEnabled(long proxy, boolean isEnabled);

    private native void nativeSetAutoRestore(long proxy, boolean doAutoRestore);

    private native void nativeSetBackupProvisioned(long proxy, boolean isProvisioned);

    private native boolean nativeIsBackupEnabled(long proxy);

    private native boolean nativeSetBackupPassword(long proxy, String currentPw, String newPw);

    private native boolean nativeHasBackupPassword(long proxy);

    private native void nativeBackupNow(long proxy) ;

    private native void nativeFullBackup(long proxy, ParcelFileDescriptor fd, boolean includeApks, boolean includeObbs,
        boolean includeShared, boolean doWidgets, boolean allApps, boolean allIncludesSystem,
        boolean doCompress, String[] packageNames);

    private native void nativeFullTransportBackup(long proxy, String[] packageNames);

    private native void nativeFullBackupNoninteractive(long proxy, ParcelFileDescriptor fd, String[] domainTokens,
            String excludeFilesRegex, String packageName, boolean shouldKillAfterBackup,
            boolean ignoreEncryptionPasswordCheck, IFullBackupRestoreObserver observer);

    private native void nativeFullRestore(long proxy, ParcelFileDescriptor fd);

    private native void nativeFullRestoreNoninteractive(long proxy, ParcelFileDescriptor fd,
            boolean ignoreEncryptionPasswordCheck, IFullBackupRestoreObserver observer);

    private native void nativeAcknowledgeFullBackupOrRestore(long proxy, int token, boolean allow, String curPassword,
        String encryptionPassword, IFullBackupRestoreObserver observer);

    private native String nativeGetCurrentTransport(long proxy);

    private native String[] nativeListAllTransports(long proxy);

    private native String nativeSelectBackupTransport(long proxy, String transport);

    private native Intent nativeGetConfigurationIntent(long proxy, String transport);

    private native String nativeGetDestinationString(long proxy, String transport);

    private native Intent nativeGetDataManagementIntent(long proxy, String transport);

    private native String nativeGetDataManagementLabel(long proxy, String transport);

    private native IRestoreSession nativeBeginRestoreSession(long proxy, String packageName, String transportID);

    private native void nativeOpComplete(long proxy, int token);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElBackupManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void dataChanged(String packageName) throws RemoteException {
        nativeDataChanged(mNativeProxy, packageName);
    }

    @Override
    public void clearBackupData(String transportName, String packageName) throws RemoteException {
        nativeClearBackupData(mNativeProxy, transportName, packageName);
    }

    @Override
    public void agentConnected(String packageName, IBinder agent) throws RemoteException {
        nativeAgentConnected(mNativeProxy, packageName, agent);
    }

    @Override
    public void agentDisconnected(String packageName) throws RemoteException {
        nativeAgentDisconnected(mNativeProxy, packageName);
    }

    @Override
    public void restoreAtInstall(String packageName, int token) throws RemoteException {
        nativeRestoreAtInstall(mNativeProxy, packageName, token);
    }

    @Override
    public void setBackupEnabled(boolean isEnabled) throws RemoteException {
        nativeSetBackupEnabled(mNativeProxy, isEnabled);
    }

    @Override
    public void setAutoRestore(boolean doAutoRestore) throws RemoteException {
        nativeSetAutoRestore(mNativeProxy, doAutoRestore);
    }

    @Override
    public void setBackupProvisioned(boolean isProvisioned) throws RemoteException {
        nativeSetBackupProvisioned(mNativeProxy, isProvisioned);
    }

    @Override
    public boolean isBackupEnabled() throws RemoteException {
        return nativeIsBackupEnabled(mNativeProxy);
    }

    @Override
    public boolean setBackupPassword(String currentPw, String newPw) throws RemoteException {
        return nativeSetBackupPassword(mNativeProxy, currentPw, newPw);
    }

    @Override
    public boolean hasBackupPassword() throws RemoteException {
        return nativeHasBackupPassword(mNativeProxy);
    }

    @Override
    public void backupNow() throws RemoteException {
        nativeBackupNow(mNativeProxy);
    }

    @Override
    public void fullBackup(ParcelFileDescriptor fd, boolean includeApks, boolean includeObbs,
        boolean includeShared, boolean doWidgets, boolean allApps, boolean allIncludesSystem,
        boolean doCompress, String[] packageNames) throws RemoteException {
        nativeFullBackup(mNativeProxy, fd, includeApks, includeObbs, includeShared, doWidgets,
            allApps, allIncludesSystem, doCompress, packageNames);
    }

    @Override
    public void fullTransportBackup(String[] packageNames) throws RemoteException {
        nativeFullTransportBackup(mNativeProxy, packageNames);
    }

    @Override
    public void fullBackupNoninteractive(ParcelFileDescriptor fd, String[] domainTokens,
            String excludeFilesRegex, String packageName, boolean shouldKillAfterBackup,
            boolean ignoreEncryptionPasswordCheck, IFullBackupRestoreObserver observer) throws RemoteException {
        nativeFullBackupNoninteractive(mNativeProxy, fd, domainTokens, excludeFilesRegex, packageName,
            shouldKillAfterBackup, ignoreEncryptionPasswordCheck, observer);
    }

    @Override
    public void fullRestore(ParcelFileDescriptor fd) throws RemoteException {
        nativeFullRestore(mNativeProxy, fd);
    }

    @Override
    public void fullRestoreNoninteractive(ParcelFileDescriptor fd,
            boolean ignoreEncryptionPasswordCheck, IFullBackupRestoreObserver observer) throws RemoteException {
        nativeFullRestoreNoninteractive(mNativeProxy, fd, ignoreEncryptionPasswordCheck, observer);
    }

    @Override
    public void acknowledgeFullBackupOrRestore(int token, boolean allow, String curPassword, String encryptionPassword,
            IFullBackupRestoreObserver observer) throws RemoteException {
        nativeAcknowledgeFullBackupOrRestore(mNativeProxy, token, allow, curPassword, encryptionPassword, observer);
    }

    @Override
    public String getCurrentTransport() throws RemoteException {
        return nativeGetCurrentTransport(mNativeProxy);
    }

    @Override
    public String[] listAllTransports() throws RemoteException {
        return nativeListAllTransports(mNativeProxy);
    }

    @Override
    public String selectBackupTransport(String transport) throws RemoteException {
        return nativeSelectBackupTransport(mNativeProxy, transport);
    }

    @Override
    public Intent getConfigurationIntent(String transport) throws RemoteException {
        return nativeGetConfigurationIntent(mNativeProxy, transport);
    }

    @Override
    public String getDestinationString(String transport) throws RemoteException {
        return nativeGetDestinationString(mNativeProxy, transport);
    }

    @Override
    public Intent getDataManagementIntent(String transport) throws RemoteException {
        return nativeGetDataManagementIntent(mNativeProxy, transport);
    }

    @Override
    public String getDataManagementLabel(String transport) throws RemoteException {
        return nativeGetDataManagementLabel(mNativeProxy, transport);
    }

    @Override
    public IRestoreSession beginRestoreSession(String packageName, String transportID) throws RemoteException {
        return nativeBeginRestoreSession(mNativeProxy, packageName, transportID);
    }

    @Override
    public void opComplete(int token) throws RemoteException {
        nativeOpComplete(mNativeProxy, token);
    }
}
