
package android.app.backup;

import android.app.backup.RestoreSet;
import android.app.backup.IRestoreObserver;
import android.os.RemoteException;

/** {@hide} */
public class ElIRestoreSessionProxy extends IRestoreSession.Stub {
    private long mNativeProxy;

    private native int nativeGetAvailableRestoreSets(long proxy, IRestoreObserver observer);

    private native int nativeRestoreAll(long proxy, long token, IRestoreObserver observer);

    private native int nativeRestoreSome(long proxy, long token, IRestoreObserver observer, String[] packages);

    private native int nativeRestorePackage(long proxy, String packageName, IRestoreObserver observer);

    private native void nativeEndRestoreSession(long proxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElIRestoreSessionProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public int getAvailableRestoreSets(IRestoreObserver observer) throws RemoteException {
        return nativeGetAvailableRestoreSets(mNativeProxy, observer);
    }

    @Override
    public int restoreAll(long token, IRestoreObserver observer) throws RemoteException {
        return nativeRestoreAll(mNativeProxy, token, observer);
    }

    @Override
    public int restoreSome(long token, IRestoreObserver observer, String[] packages) throws RemoteException {
        return nativeRestoreSome(mNativeProxy, token, observer, packages);
    }

    @Override
    public int restorePackage(String packageName, IRestoreObserver observer) throws RemoteException {
        return nativeRestorePackage(mNativeProxy, packageName, observer);
    }

    @Override
    public void endRestoreSession() throws RemoteException {
        nativeEndRestoreSession(mNativeProxy);
    }
}
