
package android.app;

import android.app.SearchableInfo;
import android.app.ISearchManagerCallback;
import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.List;

/** @hide */
public class ElSearchManagerProxy extends ISearchManager.Stub {
	private long mNativeProxy;

	private native ComponentName nativeGetGlobalSearchActivity(long proxy);

    private native ComponentName nativeGetWebSearchActivity(long proxy);

    private native ComponentName nativeGetAssistIntent(long proxy, int userHandle);

    private native SearchableInfo nativeGetSearchableInfo(long proxy, ComponentName launchActivity);

    private native List<SearchableInfo> nativeGetSearchablesInGlobalSearch(long proxy);

    private native List<ResolveInfo> nativeGetGlobalSearchActivities(long proxy);

    private native boolean nativeLaunchAssistAction(long proxy, int requestType, String hint, int userHandle);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElSearchManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public ComponentName getGlobalSearchActivity() throws RemoteException {
        return nativeGetGlobalSearchActivity(mNativeProxy);
    }

    @Override
    public ComponentName getWebSearchActivity() throws RemoteException {
        return nativeGetWebSearchActivity(mNativeProxy);
    }

    @Override
    public ComponentName getAssistIntent(int userHandle) throws RemoteException {
        return nativeGetAssistIntent(mNativeProxy, userHandle);
    }

    @Override
    public SearchableInfo getSearchableInfo(ComponentName launchActivity) throws RemoteException {
        return nativeGetSearchableInfo(mNativeProxy, launchActivity);
    }

    @Override
    public List<SearchableInfo> getSearchablesInGlobalSearch() throws RemoteException {
        return nativeGetSearchablesInGlobalSearch(mNativeProxy);
    }

    @Override
    public List<ResolveInfo> getGlobalSearchActivities() throws RemoteException {
        return nativeGetGlobalSearchActivities(mNativeProxy);
    }

    @Override
    public boolean launchAssistAction(int requestType, String hint, int userHandle) throws RemoteException {
        return nativeLaunchAssistAction(mNativeProxy, requestType, hint, userHandle);
    }
}
