
package android.app;

import android.app.ITransientNotification;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ParceledListSlice;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.service.notification.Condition;
import android.service.notification.IConditionListener;
import android.service.notification.IConditionProvider;
import android.service.notification.INotificationListener;
import android.service.notification.StatusBarNotification;
import android.service.notification.ZenModeConfig;

/** {@hide} */
public class ElNotificationManagerProxy extends INotificationManager.Stub {
    private long mNativeProxy;

    private native void nativeEnqueueToast(long nativeProxy, String pkg, ITransientNotification callback, int duration);

    private native void nativeCancelAllNotifications(long nativeProxy, String pkg, int userId);

    private native void nativeCancelToast(long nativeProxy, String pkg, ITransientNotification callback);

    private native void nativeCancelNotificationWithTag(long nativeProxy, String pkg, String tag, int id, int userId);

    private native void nativeEnqueueNotificationWithTag(long nativeProxy, String pkg, String opPkg, String tag, int id,
            Notification notification, int[] idReceived, int userId);

    private native void nativeSetNotificationsEnabledForPackage(long nativeProxy, String pkg, int uid, boolean enabled);

    private native boolean nativeAreNotificationsEnabledForPackage(long nativeProxy, String pkg, int uid);

    private native void nativeSetPackagePriority(long nativeProxy, String pkg, int uid, int priority);

    private native int nativeGetPackagePriority(long nativeProxy, String pkg, int uid);

    private native void nativeSetPackageVisibilityOverride(long nativeProxy, String pkg, int uid, int visibility);

    private native int nativeGetPackageVisibilityOverride(long nativeProxy, String pkg, int uid);

    private native void nativeSetShowNotificationForPackageOnKeyguard(long nativeProxy, String pkg, int uid, int status);

    private native int nativeGetShowNotificationForPackageOnKeyguard(long nativeProxy, String pkg, int uid);

    private native StatusBarNotification[] nativeGetActiveNotifications(long nativeProxy, String callingPkg);

    private native StatusBarNotification[] nativeGetHistoricalNotifications(long nativeProxy, String callingPkg, int count);

    private native void nativeRegisterListener(long nativeProxy, INotificationListener listener, ComponentName component, int userid);

    private native void nativeUnregisterListener(long nativeProxy, INotificationListener listener, int userid);

    private native void nativeCancelNotificationFromListener(long nativeProxy, INotificationListener token, String pkg, String tag, int id);

    private native void nativeCancelNotificationsFromListener(long nativeProxy, INotificationListener token, String[] keys);

    private native ParceledListSlice nativeGetActiveNotificationsFromListener(long nativeProxy, INotificationListener token, String[] keys, int trim);

    private native void nativeRequestHintsFromListener(long nativeProxy, INotificationListener token, int hints);

    private native int nativeGetHintsFromListener(long nativeProxy, INotificationListener token);

    private native void nativeRequestInterruptionFilterFromListener(long nativeProxy, INotificationListener token, int interruptionFilter);

    private native int nativeGetInterruptionFilterFromListener(long nativeProxy, INotificationListener token);

    private native void nativeSetOnNotificationPostedTrimFromListener(long nativeProxy, INotificationListener token, int trim);

    private native ComponentName nativeGetEffectsSuppressor(long nativeProxy);

    private native boolean nativeMatchesCallFilter(long nativeProxy, Bundle extras);

    private native ZenModeConfig nativeGetZenModeConfig(long nativeProxy);

    private native boolean nativeSetZenModeConfig(long nativeProxy, ZenModeConfig config);

    private native void nativeNotifyConditions(long nativeProxy, String pkg, IConditionProvider provider, Condition[] conditions);

    private native void nativeRequestZenModeConditions(long nativeProxy, IConditionListener callback, int relevance);

    private native void nativeSetZenModeCondition(long nativeProxy, Condition condition);

    private native void nativeSetAutomaticZenModeConditions(long nativeProxy, Uri[] conditionIds);

    private native Condition[] nativeGetAutomaticZenModeConditions(long nativeProxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElNotificationManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void enqueueToast(String pkg, ITransientNotification callback, int duration) throws RemoteException {
        nativeEnqueueToast(mNativeProxy, pkg, callback, duration);
    }

    @Override
    public void cancelAllNotifications(String pkg, int userId) throws RemoteException {
        nativeCancelAllNotifications(mNativeProxy, pkg, userId);
    }

    @Override
    public void cancelToast(String pkg, ITransientNotification callback) throws RemoteException {
        nativeCancelToast(mNativeProxy, pkg, callback);
    }

    @Override
    public void enqueueNotificationWithTag(String pkg, String opPkg, String tag, int id,
            Notification notification, int[] idReceived, int userId) throws RemoteException {
        nativeEnqueueNotificationWithTag(mNativeProxy, pkg, opPkg, tag, id, notification, idReceived, userId);
    }

    @Override
    public void cancelNotificationWithTag(String pkg, String tag, int id, int userId) throws RemoteException {
        nativeCancelNotificationWithTag(mNativeProxy, pkg, tag, id, userId);
    }

    @Override
    public void setNotificationsEnabledForPackage(String pkg, int uid, boolean enabled) throws RemoteException {
        nativeSetNotificationsEnabledForPackage(mNativeProxy, pkg, uid, enabled);
    }

    @Override
    public boolean areNotificationsEnabledForPackage(String pkg, int uid) throws RemoteException {
        return nativeAreNotificationsEnabledForPackage(mNativeProxy, pkg, uid);
    }

    @Override
    public void setPackagePriority(String pkg, int uid, int priority) throws RemoteException {
        nativeSetPackagePriority(mNativeProxy, pkg, uid, priority);
    }

    @Override
    public int getPackagePriority(String pkg, int uid) throws RemoteException {
        return nativeGetPackagePriority(mNativeProxy, pkg, uid);
    }

    @Override
    public void setPackageVisibilityOverride(String pkg, int uid, int visibility) throws RemoteException {
        nativeSetPackageVisibilityOverride(mNativeProxy, pkg, uid, visibility );
    }

    @Override
    public int getPackageVisibilityOverride(String pkg, int uid) throws RemoteException {
        return nativeGetPackageVisibilityOverride(mNativeProxy, pkg, uid);
    }

    @Override
    public void setShowNotificationForPackageOnKeyguard(String pkg, int uid, int status) throws RemoteException {
        nativeSetShowNotificationForPackageOnKeyguard(mNativeProxy, pkg, uid, status);
    }

    @Override
    public int getShowNotificationForPackageOnKeyguard(String pkg, int uid) throws RemoteException {
        return nativeGetShowNotificationForPackageOnKeyguard(mNativeProxy, pkg, uid);
    }

    @Override
    public StatusBarNotification[] getActiveNotifications(String callingPkg) throws RemoteException {
        return nativeGetActiveNotifications(mNativeProxy, callingPkg);
    }

    @Override
    public StatusBarNotification[] getHistoricalNotifications(String callingPkg, int count) throws RemoteException {
        return nativeGetHistoricalNotifications(mNativeProxy, callingPkg, count);
    }

    @Override
    public void registerListener(INotificationListener listener, ComponentName component, int userid) throws RemoteException {
        nativeRegisterListener(mNativeProxy, listener, component, userid);
    }

    @Override
    public void unregisterListener(INotificationListener listener, int userid) throws RemoteException {
        nativeUnregisterListener(mNativeProxy, listener, userid);
    }

    @Override
    public void cancelNotificationFromListener(INotificationListener token, String pkg, String tag, int id) throws RemoteException {
        nativeCancelNotificationFromListener(mNativeProxy, token, pkg, tag, id);
    }

    @Override
    public void cancelNotificationsFromListener(INotificationListener token, String[] keys) throws RemoteException {
        nativeCancelNotificationsFromListener(mNativeProxy, token, keys);
    }

    @Override
    public ParceledListSlice getActiveNotificationsFromListener(INotificationListener token, String[] keys, int trim) throws RemoteException {
        return nativeGetActiveNotificationsFromListener(mNativeProxy, token, keys, trim);
    }

    @Override
    public void requestHintsFromListener(INotificationListener token, int hints) throws RemoteException {
        nativeRequestHintsFromListener(mNativeProxy, token, hints);
    }

    @Override
    public int getHintsFromListener(INotificationListener token) throws RemoteException {
        return nativeGetHintsFromListener(mNativeProxy, token);
    }

    @Override
    public void requestInterruptionFilterFromListener(INotificationListener token, int interruptionFilter) throws RemoteException {
        nativeRequestInterruptionFilterFromListener(mNativeProxy, token, interruptionFilter);
    }

    @Override
    public int getInterruptionFilterFromListener(INotificationListener token) throws RemoteException {
        return nativeGetInterruptionFilterFromListener(mNativeProxy, token);
    }

    @Override
    public void setOnNotificationPostedTrimFromListener(INotificationListener token, int trim) throws RemoteException {
        nativeSetOnNotificationPostedTrimFromListener(mNativeProxy, token, trim);
    }

    @Override
    public ComponentName getEffectsSuppressor() throws RemoteException {
        return nativeGetEffectsSuppressor(mNativeProxy);
    }

    @Override
    public boolean matchesCallFilter(Bundle extras) throws RemoteException {
        return nativeMatchesCallFilter(mNativeProxy, extras);
    }

    @Override
    public ZenModeConfig getZenModeConfig() throws RemoteException {
        return nativeGetZenModeConfig(mNativeProxy);
    }

    @Override
    public boolean setZenModeConfig(ZenModeConfig config) throws RemoteException {
        return nativeSetZenModeConfig(mNativeProxy, config);
    }

    @Override
    public void notifyConditions(String pkg, IConditionProvider provider, Condition[] conditions) throws RemoteException {
        nativeNotifyConditions(mNativeProxy, pkg, provider, conditions);
    }

    @Override
    public void requestZenModeConditions(IConditionListener callback, int relevance) throws RemoteException {
        nativeRequestZenModeConditions(mNativeProxy, callback, relevance);
    }

    @Override
    public void setZenModeCondition(Condition condition) throws RemoteException {
        nativeSetZenModeCondition(mNativeProxy, condition);
    }

    @Override
    public void setAutomaticZenModeConditions(Uri[] conditionIds) throws RemoteException {
        nativeSetAutomaticZenModeConditions(mNativeProxy, conditionIds);
    }

    @Override
    public Condition[] getAutomaticZenModeConditions() throws RemoteException {
        return nativeGetAutomaticZenModeConditions(mNativeProxy);
    }
}

