
package android.app;

import android.content.ComponentName;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

/** {@hide} */
class ElInstrumentationWatcherProxy extends IInstrumentationWatcher.Stub
{
    private long mNativeProxy;

    private native void nativeInstrumentationStatus(long nativeProxy, ComponentName name, int resultCode,
            Bundle results);

    private native void nativeInstrumentationFinished(long nativeProxy, ComponentName name, int resultCode,
            Bundle results);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElInstrumentationWatcherProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    public void instrumentationStatus(ComponentName name, int resultCode,
            Bundle results) {
        nativeInstrumentationStatus(mNativeProxy, name, resultCode, results);
    }

    public void instrumentationFinished(ComponentName name, int resultCode,
            Bundle results) {
        nativeInstrumentationFinished(mNativeProxy, name, resultCode, results);
    }

    public IBinder asBinder() {
        return null;
    }
}