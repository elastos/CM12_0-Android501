
package android.net;

import android.net.INetworkPolicyListener;
import android.net.NetworkPolicy;
import android.net.NetworkQuotaInfo;
import android.net.NetworkState;
import android.net.NetworkTemplate;
import android.os.RemoteException;

/** @hide */
public class ElNetworkPolicyManagerProxy extends INetworkPolicyManager.Stub
{
    private long mNativeProxy;

    private native void nativeSetUidPolicy(long nativeProxy, int uid, int policy);

    private native void nativeAddUidPolicy(long nativeProxy, int uid, int policy);

    private native void nativeRemoveUidPolicy(long nativeProxy, int uid, int policy);

    private native int nativeGetUidPolicy(long nativeProxy, int uid);

    private native int[] nativeGetUidsWithPolicy(long nativeProxy, int policy);

    private native boolean nativeIsUidForeground(long nativeProxy, int uid);

    private native int[] nativeGetPowerSaveAppIdWhitelist(long nativeProxy);

    private native void nativeRegisterListener(long nativeProxy, INetworkPolicyListener listener);

    private native void nativeUnregisterListener(long nativeProxy, INetworkPolicyListener listener);

    private native void nativeSetNetworkPolicies(long nativeProxy, NetworkPolicy[] policies);

    private native NetworkPolicy[] nativeGetNetworkPolicies(long nativeProxy);

    private native void nativeSnoozeLimit(long nativeProxy, NetworkTemplate template);

    private native void nativeSetRestrictBackground(long nativeProxy, boolean restrictBackground);

    private native boolean nativeGetRestrictBackground(long nativeProxy);

    private native NetworkQuotaInfo nativeGetNetworkQuotaInfo(long nativeProxy, NetworkState state);

    private native boolean nativeIsNetworkMetered(long nativeProxy, NetworkState state);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElNetworkPolicyManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void setUidPolicy(int uid, int policy) throws RemoteException {
        nativeSetUidPolicy(mNativeProxy, uid, policy);
    }

    @Override
    public void addUidPolicy(int uid, int policy) throws RemoteException {
        nativeAddUidPolicy(mNativeProxy, uid, policy);
    }

    @Override
    public void removeUidPolicy(int uid, int policy) throws RemoteException {
        nativeRemoveUidPolicy(mNativeProxy, uid, policy);
    }

    @Override
    public int getUidPolicy(int uid) throws RemoteException {
        return nativeGetUidPolicy(mNativeProxy, uid);
    }

    @Override
    public int[] getUidsWithPolicy(int policy) throws RemoteException {
        return nativeGetUidsWithPolicy(mNativeProxy, policy);
    }

    @Override
    public boolean isUidForeground(int uid) throws RemoteException {
        return nativeIsUidForeground(mNativeProxy, uid);
    }

    @Override
    public int[] getPowerSaveAppIdWhitelist() throws RemoteException {
        return nativeGetPowerSaveAppIdWhitelist(mNativeProxy);
    }

    @Override
    public void registerListener(INetworkPolicyListener listener) throws RemoteException {
        nativeRegisterListener(mNativeProxy, listener);
    }

    @Override
    public void unregisterListener(INetworkPolicyListener listener) throws RemoteException {
        nativeUnregisterListener(mNativeProxy, listener);
    }

    @Override
    public void setNetworkPolicies(NetworkPolicy[] policies) throws RemoteException {
        nativeSetNetworkPolicies(mNativeProxy, policies);
    }

    @Override
    public NetworkPolicy[] getNetworkPolicies() throws RemoteException {
        return nativeGetNetworkPolicies(mNativeProxy);
    }

    @Override
    public void snoozeLimit(NetworkTemplate template) throws RemoteException {
        nativeSnoozeLimit(mNativeProxy, template);
    }

    @Override
    public void setRestrictBackground(boolean restrictBackground) throws RemoteException {
        nativeSetRestrictBackground(mNativeProxy, restrictBackground);
    }

    @Override
    public boolean getRestrictBackground() throws RemoteException {
        return nativeGetRestrictBackground(mNativeProxy);
    }

    @Override
    public NetworkQuotaInfo getNetworkQuotaInfo(NetworkState state) throws RemoteException {
        return nativeGetNetworkQuotaInfo(mNativeProxy, state);
    }

    @Override
    public boolean isNetworkMetered(NetworkState state) throws RemoteException {
        return nativeIsNetworkMetered(mNativeProxy, state);
    }
}
