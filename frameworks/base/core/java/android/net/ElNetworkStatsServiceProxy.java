
package android.net;

import android.net.INetworkStatsSession;
import android.net.NetworkStats;
import android.net.NetworkStatsHistory;
import android.net.NetworkTemplate;
import android.os.RemoteException;

/** @hide */
public class ElNetworkStatsServiceProxy extends INetworkStatsService.Stub
{
    private long mNativeProxy;

    private native INetworkStatsSession nativeOpenSession(long nativeProxy);

    private native long nativeGetNetworkTotalBytes(long nativeProxy, NetworkTemplate template, long start, long end);

    private native NetworkStats nativeGetDataLayerSnapshotForUid(long nativeProxy, int uid);

    private native String[] nativeGetMobileIfaces(long nativeProxy);

    private native void nativeIncrementOperationCount(long nativeProxy, int uid, int tag, int operationCount);

    private native void nativeSetUidForeground(long nativeProxy, int uid, boolean uidForeground);

    private native void nativeForceUpdate(long nativeProxy);

    private native void nativeAdvisePersistThreshold(long nativeProxy, long thresholdBytes);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElNetworkStatsServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public INetworkStatsSession openSession() throws RemoteException {
        return nativeOpenSession(mNativeProxy);
    }

    @Override
    public long getNetworkTotalBytes(NetworkTemplate template, long start, long end) throws RemoteException {
        return nativeGetNetworkTotalBytes(mNativeProxy, template, start, end);
    }

    @Override
    public NetworkStats getDataLayerSnapshotForUid(int uid) throws RemoteException {
        return nativeGetDataLayerSnapshotForUid(mNativeProxy, uid);
    }

    @Override
    public String[] getMobileIfaces() throws RemoteException {
        return nativeGetMobileIfaces(mNativeProxy);
    }

    @Override
    public void incrementOperationCount(int uid, int tag, int operationCount) throws RemoteException {
        nativeIncrementOperationCount(mNativeProxy, uid, tag, operationCount);
    }

    @Override
    public void setUidForeground(int uid, boolean uidForeground) throws RemoteException {
        nativeSetUidForeground(mNativeProxy, uid, uidForeground);
    }

    @Override
    public void forceUpdate() throws RemoteException {
        nativeForceUpdate(mNativeProxy);
    }

    @Override
    public void advisePersistThreshold(long thresholdBytes) throws RemoteException {
        nativeAdvisePersistThreshold(mNativeProxy, thresholdBytes);
    }
}
