/**
 * Copyright (c) 2008, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net;

import android.app.PendingIntent;
import android.net.LinkQualityInfo;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkMisc;
import android.net.NetworkQuotaInfo;
import android.net.NetworkRequest;
import android.net.NetworkState;
import android.net.ProxyInfo;
import android.net.wifi.WifiDevice;
import android.os.IBinder;
import android.os.Messenger;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.os.RemoteException;

import com.android.internal.net.LegacyVpnInfo;
import com.android.internal.net.VpnConfig;
import com.android.internal.net.VpnProfile;

import java.util.List;

/**
 * Interface that answers queries about, and allows changing, the
 * state of network connectivity.
 */
/** {@hide} */
public class ElConnectivityManagerProxy extends IConnectivityManager.Stub
{
    private long mNativeProxy;

    private native NetworkInfo nativeGetNetworkInfo(long nativeProxy, int networkType);

    private native NetworkInfo nativeGetActiveNetworkInfo(long nativeProxy);

    private native LinkProperties nativeGetLinkPropertiesForType(long nativeProxy, int networkType);

    private native LinkProperties nativeGetActiveLinkProperties(long nativeProxy);

    private native boolean nativeIsNetworkSupported(long nativeProxy, int networkType);

    private native NetworkInfo[] nativeGetAllNetworkInfo(long nativeProxy);

    private native ProxyInfo nativeGetProxy(long nativeProxy);

    private native NetworkInfo nativeGetActiveNetworkInfoForUid(long nativeProxy, int uid);

    private native void nativeStartLegacyVpn(long nativeProxy, VpnProfile profile);

    private native void nativeReportInetCondition(long nativeProxy, int networkType, int percentage);

    private native NetworkState[] nativeGetAllNetworkState(long nativeProxy) ;

    private native NetworkQuotaInfo nativeGetActiveNetworkQuotaInfo(long nativeProxy);

    private native boolean nativeIsActiveNetworkMetered(long nativeProxy);

    private native boolean nativeRequestRouteToHostAddress(long nativeProxy, int networkType, byte[] hostAddress);

    private native void nativeSetPolicyDataEnable(long nativeProxy, int networkType, boolean enabled);

    private native int nativeTether(long nativeProxy, String iface);

    private native int nativeUntether(long nativeProxy, String iface);

    private native int nativeGetLastTetherError(long nativeProxy, String iface);

    private native boolean nativeIsTetheringSupported(long nativeProxy) ;

    private native String[] nativeGetTetherableIfaces(long nativeProxy) ;

    private native String[] nativeGetTetheredIfaces(long nativeProxy) ;

    private native String[] nativeGetTetheringErroredIfaces(long nativeProxy) ;

    private native String[] nativeGetTetheredDhcpRanges(long nativeProxy) ;

    private native String[] nativeGetTetherableUsbRegexs(long nativeProxy) ;

    private native String[] nativeGetTetherableWifiRegexs(long nativeProxy) ;

    private native String[] nativeGetTetherableBluetoothRegexs(long nativeProxy) ;

    private native int nativeSetUsbTethering(long nativeProxy, boolean enable);

    private native ProxyInfo nativeGetGlobalProxy(long nativeProxy);

    private native void nativeSetGlobalProxy(long nativeProxy, ProxyInfo p);

    private native void nativeSetDataDependency(long nativeProxy, int networkType, boolean met);

    private native boolean nativePrepareVpn(long nativeProxy, String oldPackage, String newPackage);

    private native void nativeSetVpnPackageAuthorization(long nativeProxy, boolean authorized);

    private native ParcelFileDescriptor nativeEstablishVpn(long nativeProxy, VpnConfig config);

    private native LegacyVpnInfo nativeGetLegacyVpnInfo(long nativeProxy) ;

    private native boolean nativeUpdateLockdownVpn(long nativeProxy);

    private native void nativeCaptivePortalCheckCompleted(long nativeProxy, NetworkInfo info, boolean isCaptivePortal);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElConnectivityManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public NetworkInfo getNetworkInfo(int networkType) throws RemoteException {
        return nativeGetNetworkInfo(mNativeProxy, networkType);
    }

    @Override
    public NetworkInfo getNetworkInfoForNetwork(Network network) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public NetworkInfo getActiveNetworkInfo() throws RemoteException {
        return nativeGetActiveNetworkInfo(mNativeProxy);
    }

    @Override
    public LinkProperties getLinkPropertiesForType(int networkType) throws RemoteException {
        return nativeGetLinkPropertiesForType(mNativeProxy, networkType);
    }

    @Override
    public LinkProperties getLinkProperties(Network network) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public NetworkCapabilities getNetworkCapabilities(Network network) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public LinkProperties getActiveLinkProperties() throws RemoteException {
        return nativeGetActiveLinkProperties(mNativeProxy);
    }

    @Override
    public boolean isNetworkSupported(int networkType) throws RemoteException {
        return nativeIsNetworkSupported(mNativeProxy, networkType);
    }

    @Override
    public NetworkInfo[] getAllNetworkInfo() throws RemoteException {
        return nativeGetAllNetworkInfo(mNativeProxy);
    }

    @Override
    public Network getNetworkForType(int networkType) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public Network[] getAllNetworks() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public NetworkInfo getProvisioningOrActiveNetworkInfo() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public ProxyInfo getProxy() throws RemoteException {
        return nativeGetProxy(mNativeProxy);
    }

    @Override
    public void startLegacyVpn(VpnProfile profile) throws RemoteException {
        nativeStartLegacyVpn(mNativeProxy, profile);
    }

    @Override
    public void reportInetCondition(int networkType, int percentage) throws RemoteException {
        nativeReportInetCondition(mNativeProxy, networkType, percentage);
    }

    @Override
    public void reportBadNetwork(Network network) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public NetworkInfo getActiveNetworkInfoForUid(int uid) throws RemoteException {
        return nativeGetActiveNetworkInfoForUid(mNativeProxy, uid);
    }

    @Override
    public NetworkState[] getAllNetworkState() throws RemoteException {
        return nativeGetAllNetworkState(mNativeProxy);
    }

    @Override
    public NetworkQuotaInfo getActiveNetworkQuotaInfo() throws RemoteException {
        return nativeGetActiveNetworkQuotaInfo(mNativeProxy);
    }

    @Override
    public boolean isActiveNetworkMetered() throws RemoteException {
        return nativeIsActiveNetworkMetered(mNativeProxy);
    }

    @Override
    public boolean requestRouteToHostAddress(int networkType, byte[] hostAddress) throws RemoteException {
        return nativeRequestRouteToHostAddress(mNativeProxy, networkType, hostAddress);
    }

    @Override
    public void setPolicyDataEnable(int networkType, boolean enabled) throws RemoteException {
        nativeSetPolicyDataEnable(mNativeProxy, networkType, enabled);
    }

    @Override
    public int tether(String iface) throws RemoteException {
        return nativeTether(mNativeProxy, iface);
    }

    @Override
    public int untether(String iface) throws RemoteException {
        return nativeUntether(mNativeProxy, iface);
    }

    @Override
    public int getLastTetherError(String iface) throws RemoteException {
        return nativeGetLastTetherError(mNativeProxy, iface);
    }

    @Override
    public boolean isTetheringSupported() throws RemoteException {
        return nativeIsTetheringSupported(mNativeProxy);
    }

    @Override
    public String[] getTetherableIfaces() throws RemoteException {
        return nativeGetTetherableIfaces(mNativeProxy);
    }

    @Override
    public String[] getTetheredIfaces() throws RemoteException {
        return nativeGetTetheredIfaces(mNativeProxy);
    }

    @Override
    public String[] getTetheringErroredIfaces() throws RemoteException {
        return nativeGetTetheringErroredIfaces(mNativeProxy);
    }

    @Override
    public String[] getTetheredDhcpRanges() throws RemoteException {
        return nativeGetTetheredDhcpRanges(mNativeProxy);
    }

    @Override
    public String[] getTetherableUsbRegexs() throws RemoteException {
        return nativeGetTetherableUsbRegexs(mNativeProxy);
    }

    @Override
    public String[] getTetherableWifiRegexs() throws RemoteException {
        return nativeGetTetherableWifiRegexs(mNativeProxy);
    }

    @Override
    public String[] getTetherableBluetoothRegexs() throws RemoteException {
        return nativeGetTetherableBluetoothRegexs(mNativeProxy);
    }

    @Override
    public int setUsbTethering(boolean enable) throws RemoteException {
        return nativeSetUsbTethering(mNativeProxy, enable);
    }

    @Override
    public List<WifiDevice> getTetherConnectedSta() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public ProxyInfo getGlobalProxy() throws RemoteException {
        return nativeGetGlobalProxy(mNativeProxy);
    }

    @Override
    public void setGlobalProxy(ProxyInfo p) throws RemoteException {
        nativeSetGlobalProxy(mNativeProxy, p);
    }

    @Override
    public void setDataDependency(int networkType, boolean met) throws RemoteException {
        nativeSetDataDependency(mNativeProxy, networkType, met);
    }

    @Override
    public boolean prepareVpn(String oldPackage, String newPackage) throws RemoteException {
        return nativePrepareVpn(mNativeProxy, oldPackage, newPackage);
    }

    @Override
    public void setVpnPackageAuthorization(boolean authorized) throws RemoteException {
        nativeSetVpnPackageAuthorization(mNativeProxy, authorized);
    }

    @Override
    public ParcelFileDescriptor establishVpn(VpnConfig config) throws RemoteException {
        return nativeEstablishVpn(mNativeProxy, config);
    }

    @Override
    public VpnConfig getVpnConfig() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public LegacyVpnInfo getLegacyVpnInfo() throws RemoteException {
        return nativeGetLegacyVpnInfo(mNativeProxy);
    }

    @Override
    public boolean updateLockdownVpn() throws RemoteException {
        return nativeUpdateLockdownVpn(mNativeProxy);
    }

    @Override
    public void captivePortalCheckCompleted(NetworkInfo info, boolean isCaptivePortal) throws RemoteException {
        nativeCaptivePortalCheckCompleted(mNativeProxy, info, isCaptivePortal);
    }

    @Override
    public void supplyMessenger(int networkType, Messenger messenger) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public int findConnectionTypeForIface(String iface) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public int checkMobileProvisioning(int suggestedTimeOutMs) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public String getMobileProvisioningUrl() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public String getMobileRedirectedProvisioningUrl() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public LinkQualityInfo getLinkQualityInfo(int networkType) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public LinkQualityInfo getActiveLinkQualityInfo() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public LinkQualityInfo[] getAllLinkQualityInfo() throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void setProvisioningNotificationVisible(boolean visible, int networkType, String action) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void setAirplaneMode(boolean enable) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void registerNetworkFactory(Messenger messenger, String name) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void unregisterNetworkFactory(Messenger messenger) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void registerNetworkAgent(Messenger messenger, NetworkInfo ni, LinkProperties lp,
            NetworkCapabilities nc, int score, NetworkMisc misc) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public NetworkRequest requestNetwork(NetworkCapabilities networkCapabilities,
            Messenger messenger, int timeoutSec, IBinder binder, int legacy) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public NetworkRequest pendingRequestForNetwork(NetworkCapabilities networkCapabilities,
            PendingIntent operation) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public NetworkRequest listenForNetwork(NetworkCapabilities networkCapabilities,
            Messenger messenger, IBinder binder) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void pendingListenForNetwork(NetworkCapabilities networkCapabilities,
            PendingIntent operation) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public void releaseNetworkRequest(NetworkRequest networkRequest) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public int getRestoreDefaultNetworkDelay(int networkType) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public boolean addVpnAddress(String address, int prefixLength) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }

    @Override
    public boolean removeVpnAddress(String address, int prefixLength) throws RemoteException {
        RemoteException e = new RemoteException();
        e.printStackTrace();
        throw e;
    }
}
