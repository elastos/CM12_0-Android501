
package android.net;

import android.net.NetworkStats;
import android.net.NetworkStatsHistory;
import android.net.NetworkTemplate;
import android.os.RemoteException;

/** @hide */
public class ElNetworkStatsSessionProxy extends INetworkStatsSession.Stub
{
    private long mNativeProxy;

    private native NetworkStats nativeGetSummaryForNetwork(long nativeProxy, NetworkTemplate template, long start, long end);

    private native NetworkStatsHistory nativeGetHistoryForNetwork(long nativeProxy, NetworkTemplate template, int fields);

    private native NetworkStats nativeGetSummaryForAllUid(long nativeProxy, NetworkTemplate template, long start, long end, boolean includeTags);

    private native NetworkStatsHistory nativeGetHistoryForUid(long nativeProxy, NetworkTemplate template, int uid, int set, int tag, int fields);

    private native void nativeClose(long nativeProxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElNetworkStatsSessionProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public NetworkStats getSummaryForNetwork(NetworkTemplate template, long start, long end) throws RemoteException {
        return nativeGetSummaryForNetwork(mNativeProxy, template, start, end);
    }

    @Override
    public NetworkStatsHistory getHistoryForNetwork(NetworkTemplate template, int fields) throws RemoteException {
        return nativeGetHistoryForNetwork(mNativeProxy, template, fields);
    }

    @Override
    public NetworkStats getSummaryForAllUid(NetworkTemplate template, long start, long end, boolean includeTags) throws RemoteException {
        return nativeGetSummaryForAllUid(mNativeProxy, template, start, end, includeTags);
    }

    @Override
    public NetworkStatsHistory getHistoryForUid(NetworkTemplate template, int uid, int set, int tag, int fields) throws RemoteException {
        return nativeGetHistoryForUid(mNativeProxy, template, uid, set, tag, fields);
    }

    @Override
    public void close() throws RemoteException {
        nativeClose(mNativeProxy);
    }
}
