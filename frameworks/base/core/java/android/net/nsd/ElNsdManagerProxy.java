
package android.net.nsd;

import android.os.Messenger;
import android.os.RemoteException;

/**
 * {@hide}
 */
public class ElNsdManagerProxy extends INsdManager.Stub {
    private long mNativeProxy;

    private native Messenger nativeGetMessenger(long proxy);

    private native void nativeSetEnabled(long proxy, boolean enable);

    private static final native void nativeFinalize(long nativeProxy);

    public ElNsdManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    @Override
    public Messenger getMessenger() throws RemoteException {
        return nativeGetMessenger(mNativeProxy);
    }

    @Override
    public void setEnabled(boolean enable) throws RemoteException {
        nativeSetEnabled(mNativeProxy, enable);
    }
}
