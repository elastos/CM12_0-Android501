
package android.net;

import android.net.IpConfiguration;
import android.os.RemoteException;

/** @hide */
public class ElEthernetManagerProxy extends IEthernetManager.Stub
{
    private long mNativeProxy;

    private native IpConfiguration nativeGetConfiguration(long nativeProxy);

    private native void nativeSetConfiguration(long nativeProxy, IpConfiguration config);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElEthernetManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public IpConfiguration getConfiguration() throws RemoteException {
        return nativeGetConfiguration(mNativeProxy);
    }

    @Override
    public void setConfiguration(IpConfiguration config) throws RemoteException {
        nativeSetConfiguration(mNativeProxy, config);
    }
}
