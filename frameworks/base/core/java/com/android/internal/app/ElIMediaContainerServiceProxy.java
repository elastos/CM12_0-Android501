
package com.android.internal.app;

import com.android.internal.os.IParcelFileDescriptorFactory;
import android.os.IBinder;
import android.os.RemoteException;
import android.content.pm.PackageInfoLite;
import android.content.res.ObbInfo;
import android.util.Log;

public class ElIMediaContainerServiceProxy extends IMediaContainerService.Stub {
    private long mNativeProxy;

    private native String nativeCopyPackageToContainer(long proxy, String packagePath, String containerId, String key,
            boolean isExternal, boolean isForwardLocked, String abiOverride);

    private native int nativeCopyPackage(long proxy, String packagePath, IParcelFileDescriptorFactory target);

    private native PackageInfoLite nativeGetMinimalPackageInfo(long proxy, String packagePath, int flags, String abiOverride);

    private native ObbInfo nativeGetObbInfo(long proxy, String filename);

    private native long nativeCalculateDirectorySize(long proxy, String directory) ;

    private native long[] nativeGetFileSystemStats(long proxy, String path);

    private native void nativeClearDirectory(long proxy, String directory);

    private native long nativeCalculateInstalledSize(long proxy, String packagePath, boolean isForwardLocked, String abiOverride);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElIMediaContainerServiceProxy(long proxy) {
        mNativeProxy = proxy;
    }

    @Override
    public String copyPackageToContainer(String packagePath, String containerId, String key,
            boolean isExternal, boolean isForwardLocked, String abiOverride) throws RemoteException {
        return nativeCopyPackageToContainer(mNativeProxy, packagePath, containerId, key, isExternal, isForwardLocked, abiOverride);
    }

    @Override
    public int copyPackage(String packagePath, IParcelFileDescriptorFactory target) throws RemoteException {
        return nativeCopyPackage(mNativeProxy, packagePath, target);
    }

    @Override
    public PackageInfoLite getMinimalPackageInfo(String packagePath, int flags, String abiOverride) throws RemoteException {
        return nativeGetMinimalPackageInfo(mNativeProxy, packagePath, flags, abiOverride);
    }

    @Override
    public ObbInfo getObbInfo(String filename) throws RemoteException {
        return nativeGetObbInfo(mNativeProxy, filename);
    }

    @Override
    public long calculateDirectorySize(String directory) throws RemoteException {
        return nativeCalculateDirectorySize(mNativeProxy, directory);
    }

    @Override
    public long[] getFileSystemStats(String path) throws RemoteException {
        return nativeGetFileSystemStats(mNativeProxy, path);
    }

    @Override
    public void clearDirectory(String directory) throws RemoteException {
        nativeClearDirectory(mNativeProxy, directory);
    }

    @Override
    public long calculateInstalledSize(String packagePath, boolean isForwardLocked, String abiOverride) throws RemoteException {
        return nativeCalculateInstalledSize(mNativeProxy, packagePath, isForwardLocked, abiOverride);
    }
}
