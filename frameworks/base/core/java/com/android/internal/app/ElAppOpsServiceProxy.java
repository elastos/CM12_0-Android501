
package com.android.internal.app;

import android.app.AppOpsManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import java.util.List;

/**
 * {@hide}
 */
public class ElAppOpsServiceProxy extends IAppOpsService.Stub {
    private long mNativeProxy;

    private static final native void nativeFinalize(long nativeProxy);

    private native int nativeCheckOperation(long nativeProxy, int code, int uid, String packageName);

    private native int nativeNoteOperation(long nativeProxy, int code, int uid, String packageName);

    private native int nativeStartOperation(long nativeProxy, IBinder token, int code, int uid, String packageName);

    private native void nativeFinishOperation(long nativeProxy, IBinder token, int code, int uid, String packageName);

    private native void nativeStartWatchingMode(long nativeProxy, int op, String packageName, IAppOpsCallback callback);

    private native void nativeStopWatchingMode(long nativeProxy, IAppOpsCallback callback);

    private native IBinder nativeGetToken(long nativeProxy, IBinder clientToken);

    private native int nativeCheckPackage(long nativeProxy, int uid, String packageName);

    private native List<AppOpsManager.PackageOps> nativeGetPackagesForOps(long nativeProxy, int[] ops);

    private native List<AppOpsManager.PackageOps> nativeGetOpsForPackage(long nativeProxy, int uid, String packageName, int[] ops);

    private native void nativeSetMode(long nativeProxy, int code, int uid, String packageName, int mode);

    private native void nativeResetAllModes(long nativeProxy);

    private native int nativeCheckAudioOperation(long nativeProxy, int code, int usage, int uid, String packageName);

    private native void nativeSetAudioRestriction(long nativeProxy, int code, int usage, int uid, int mode, String[] exceptionPackages);

    private native void nativeSetUserRestrictions(long nativeProxy, Bundle restrictions, int userHandle);

    private native void nativeRemoveUser(long nativeProxy, int userHandle);

    private native boolean nativeIsControlAllowed(long nativeProxy, int code, String packageName);

    private native boolean nativeGetPrivacyGuardSettingForPackage(long nativeProxy, int uid, String packageName);

    private native void nativeSetPrivacyGuardSettingForPackage(long nativeProxy, int uid, String packageName, boolean state);

    private native void nativeResetCounters(long nativeProxy);


    public ElAppOpsServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    @Override
    public int checkOperation(int code, int uid, String packageName) throws RemoteException {
        return nativeCheckOperation(mNativeProxy, code, uid, packageName);
    }

    @Override
    public int noteOperation(int code, int uid, String packageName) throws RemoteException {
        return nativeNoteOperation(mNativeProxy, code, uid, packageName);
    }

    @Override
    public int startOperation(IBinder token, int code, int uid, String packageName) throws RemoteException {
        return nativeStartOperation(mNativeProxy, token, code, uid, packageName);
    }

    @Override
    public void finishOperation(IBinder token, int code, int uid, String packageName) throws RemoteException {
        nativeFinishOperation(mNativeProxy, token, code, uid, packageName);
    }

    @Override
    public void startWatchingMode(int op, String packageName, IAppOpsCallback callback) throws RemoteException {
        nativeStartWatchingMode(mNativeProxy, op, packageName, callback);
    }

    @Override
    public void stopWatchingMode(IAppOpsCallback callback) throws RemoteException {
        nativeStopWatchingMode(mNativeProxy, callback);
    }

    @Override
    public IBinder getToken(IBinder clientToken) throws RemoteException {
        return nativeGetToken(mNativeProxy, clientToken);
    }

    @Override
    public int checkPackage(int uid, String packageName) throws RemoteException {
        return nativeCheckPackage(mNativeProxy, uid, packageName);
    }

    @Override
    public List<AppOpsManager.PackageOps> getPackagesForOps(int[] ops) throws RemoteException {
        return nativeGetPackagesForOps(mNativeProxy, ops);
    }

    @Override
    public List<AppOpsManager.PackageOps> getOpsForPackage(int uid, String packageName, int[] ops) throws RemoteException {
        return nativeGetOpsForPackage(mNativeProxy, uid, packageName, ops);
    }

    @Override
    public void setMode(int code, int uid, String packageName, int mode) throws RemoteException {
        nativeSetMode(mNativeProxy, code, uid, packageName, mode);
    }

    @Override
    public void resetAllModes() throws RemoteException {
        nativeResetAllModes(mNativeProxy);
    }

    @Override
    public int checkAudioOperation(int code, int usage, int uid, String packageName) throws RemoteException {
        return nativeCheckAudioOperation(mNativeProxy, code, usage, uid, packageName);
    }

    @Override
    public void setAudioRestriction(int code, int usage, int uid, int mode, String[] exceptionPackages) throws RemoteException {
        nativeSetAudioRestriction(mNativeProxy, code, usage, uid, mode, exceptionPackages);
    }

    @Override
    public void setUserRestrictions(Bundle restrictions, int userHandle) throws RemoteException {
        nativeSetUserRestrictions(mNativeProxy, restrictions, userHandle);
    }

    @Override
    public void removeUser(int userHandle) throws RemoteException {
        nativeRemoveUser(mNativeProxy, userHandle);
    }

    @Override
    public boolean isControlAllowed(int code, String packageName) throws RemoteException {
        return nativeIsControlAllowed(mNativeProxy, code, packageName);
    }

    @Override
    public boolean getPrivacyGuardSettingForPackage(int uid, String packageName) throws RemoteException {
        return nativeGetPrivacyGuardSettingForPackage(mNativeProxy, uid, packageName);
    }

    @Override
    public void setPrivacyGuardSettingForPackage(int uid, String packageName, boolean state) throws RemoteException {
        nativeSetPrivacyGuardSettingForPackage(mNativeProxy, uid, packageName, state);
    }

    @Override
    public void resetCounters() throws RemoteException {
        nativeResetCounters(mNativeProxy);
    }
}
