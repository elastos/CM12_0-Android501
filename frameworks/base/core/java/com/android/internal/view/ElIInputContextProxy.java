
package com.android.internal.view;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedTextRequest;

import com.android.internal.view.IInputContextCallback;

/**
 * {@hide}
 */
public class ElIInputContextProxy extends IInputContext.Stub {
    private long mNativeProxy;

    private static final native void nativeFinalize(long obj);

    private native void nativeReportFullscreenMode(long proxy, boolean enabled);

    private native void nativeGetTextBeforeCursor(long proxy, int length, int flags, int seq, IInputContextCallback callback);

    private native void nativeFinishComposingText(long proxy);

    private native void nativeCommitText(long proxy, CharSequence text, int newCursorPosition);

    private native void nativeSendKeyEvent(long proxy, KeyEvent event);

    private native void nativeClearMetaKeyStates(long proxy, int states);

    private native void nativeBeginBatchEdit(long proxy);

    private native void nativeEndBatchEdit(long proxy);

    private native void nativePerformEditorAction(long proxy, int actionCode);

    private native void nativeSetComposingText(long proxy, CharSequence text, int newCursorPosition);

    private native void nativeGetExtractedText(long proxy, ExtractedTextRequest request, int flags, int seq, IInputContextCallback callback);

    private native void nativePerformContextMenuAction(long proxy, int id);

    private native void nativeGetTextAfterCursor(long proxy, int length, int flags, int seq, IInputContextCallback callback);

    private native void nativeGetCursorCapsMode(long proxy, int reqModes, int seq, IInputContextCallback callback);

    private native void nativeDeleteSurroundingText(long proxy, int leftLength, int rightLength);

    private native void nativeCommitCompletion(long proxy, CompletionInfo completion);

    private native void nativeCommitCorrection(long proxy, CorrectionInfo correction);

    private native void nativeSetSelection(long proxy, int start, int end);

    private native void nativePerformPrivateCommand(long proxy, String action, Bundle data);

    private native void nativeSetComposingRegion(long proxy, int start, int end);

    private native void nativeGetSelectedText(long proxy, int flags, int seq, IInputContextCallback callback);

    private native void nativeRequestUpdateCursorAnchorInfo(long proxy, int cursorUpdateMode, int seq,
            IInputContextCallback callback);

    public ElIInputContextProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
        }
    }

    @Override
    public void reportFullscreenMode(boolean enabled) throws RemoteException {
        nativeReportFullscreenMode(mNativeProxy, enabled);
    }

    @Override
    public void getTextBeforeCursor(int length, int flags, int seq, IInputContextCallback callback) throws RemoteException {
        nativeGetTextBeforeCursor(mNativeProxy, length, flags, seq, callback);
    }

    @Override
    public void finishComposingText() throws RemoteException {
        nativeFinishComposingText(mNativeProxy);
    }

    @Override
    public void commitText(CharSequence text, int newCursorPosition) throws RemoteException {
        nativeCommitText(mNativeProxy, text, newCursorPosition);
    }

    @Override
    public void sendKeyEvent(KeyEvent event) throws RemoteException {
        nativeSendKeyEvent(mNativeProxy, event);
    }

    @Override
    public void clearMetaKeyStates(int states) throws RemoteException {
        nativeClearMetaKeyStates(mNativeProxy, states);
    }

    @Override
    public void beginBatchEdit() throws RemoteException {
        nativeBeginBatchEdit(mNativeProxy);
    }

    @Override
    public void endBatchEdit() throws RemoteException {
        nativeEndBatchEdit(mNativeProxy);
    }

    @Override
    public void performEditorAction(int actionCode) throws RemoteException {
        nativePerformEditorAction(mNativeProxy, actionCode);
    }

    @Override
    public void setComposingText(CharSequence text, int newCursorPosition) throws RemoteException {
        nativeSetComposingText(mNativeProxy, text, newCursorPosition);
    }

    @Override
    public void getExtractedText(ExtractedTextRequest request, int flags, int seq,
            IInputContextCallback callback) throws RemoteException {
        nativeGetExtractedText(mNativeProxy, request, flags, seq, callback);
    }

    @Override
    public void performContextMenuAction(int id) throws RemoteException {
        nativePerformContextMenuAction(mNativeProxy, id);
    }

    @Override
    public void getTextAfterCursor(int length, int flags, int seq, IInputContextCallback callback) throws RemoteException {
        nativeGetTextAfterCursor(mNativeProxy, length, flags, seq, callback);
    }

    @Override
    public void getCursorCapsMode(int reqModes, int seq, IInputContextCallback callback) throws RemoteException {
        nativeGetCursorCapsMode(mNativeProxy, reqModes, seq, callback);
    }

    @Override
    public void deleteSurroundingText(int leftLength, int rightLength) throws RemoteException {
        nativeDeleteSurroundingText(mNativeProxy, leftLength, rightLength);
    }

    @Override
    public void commitCompletion(CompletionInfo completion) throws RemoteException {
        nativeCommitCompletion(mNativeProxy, completion);
    }

    @Override
    public void commitCorrection(CorrectionInfo correction) throws RemoteException {
        nativeCommitCorrection(mNativeProxy, correction);
    }

    @Override
    public void setSelection(int start, int end) throws RemoteException {
        nativeSetSelection(mNativeProxy, start, end);
    }

    @Override
    public void performPrivateCommand(String action, Bundle data) throws RemoteException {
        nativePerformPrivateCommand(mNativeProxy, action, data);
    }

    @Override
    public void setComposingRegion(int start, int end) throws RemoteException {
        nativeSetComposingRegion(mNativeProxy, start, end);
    }

    @Override
    public void getSelectedText(int flags, int seq, IInputContextCallback callback) throws RemoteException {
        nativeGetSelectedText(mNativeProxy, flags, seq, callback);
    }

    @Override
    public void requestUpdateCursorAnchorInfo(int cursorUpdateMode, int seq,
            IInputContextCallback callback) throws RemoteException {
        nativeRequestUpdateCursorAnchorInfo(mNativeProxy, cursorUpdateMode, seq, callback);
    }

}
