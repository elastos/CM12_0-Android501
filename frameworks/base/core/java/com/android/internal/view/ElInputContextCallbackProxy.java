
package com.android.internal.view;

import com.android.internal.view.IInputContextCallback;

import android.os.IBinder;
import android.os.RemoteException;
import android.view.inputmethod.ExtractedText;
import android.util.Log;

/**
 * {@hide}
 */
public class ElInputContextCallbackProxy implements IInputContextCallback {
    private long mNativeProxy;

    private native void nativeSetCursorCapsMode(long nativeProxy, int capsMode, int seq);

    private native void nativeSetTextBeforeCursor(long nativeProxy, CharSequence textBeforeCursor, int seq);

    private native void nativeSetTextAfterCursor(long nativeProxy, CharSequence textAfterCursor, int seq);

    private native void nativeSetExtractedText(long nativeProxy, ExtractedText extractedText, int seq);

    private native void nativeSetSelectedText(long nativeProxy, CharSequence selectedText, int seq);

    private native void nativeSetRequestUpdateCursorAnchorInfoResult(long nativeProxy, boolean result, int seq);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElInputContextCallbackProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void setCursorCapsMode(int capsMode, int seq) throws RemoteException {
        nativeSetCursorCapsMode(mNativeProxy, capsMode, seq);
    }

    @Override
    public void setTextBeforeCursor(CharSequence textBeforeCursor, int seq) throws RemoteException {
        nativeSetTextBeforeCursor(mNativeProxy, textBeforeCursor, seq);
    }

    @Override
    public void setTextAfterCursor(CharSequence textAfterCursor, int seq) throws RemoteException {
        nativeSetTextAfterCursor(mNativeProxy, textAfterCursor, seq);
    }

    @Override
    public void setExtractedText(ExtractedText extractedText, int seq) throws RemoteException {
        nativeSetExtractedText(mNativeProxy, extractedText, seq);
    }

    @Override
    public void setSelectedText(CharSequence selectedText, int seq) throws RemoteException {
        nativeSetSelectedText(mNativeProxy, selectedText, seq);
    }

    @Override
    public void setRequestUpdateCursorAnchorInfoResult(boolean result, int seq) throws RemoteException {
        nativeSetRequestUpdateCursorAnchorInfoResult(mNativeProxy, result, seq);
    }

    public IBinder asBinder() {
        Log.e("ElInputContextCallbackProxy", "asBinder()");
        return null;
    }
}
