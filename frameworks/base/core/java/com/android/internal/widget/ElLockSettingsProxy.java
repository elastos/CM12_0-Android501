package com.android.internal.widget;

import android.os.RemoteException;
import android.util.Log;

public class ElLockSettingsProxy extends ILockSettings.Stub {
	private long mNativeProxy;

	public ElLockSettingsProxy(long proxy){
		mNativeProxy = proxy;
	}

	public void setBoolean(java.lang.String key, boolean value, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public void setLong(java.lang.String key, long value, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public void setString(java.lang.String key, java.lang.String value, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public boolean getBoolean(java.lang.String key, boolean defaultValue, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public long getLong(java.lang.String key, long defaultValue, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public java.lang.String getString(java.lang.String key, java.lang.String defaultValue, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public byte getLockPatternSize(int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public void setLockPattern(String pattern, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public boolean checkPattern(String pattern, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public void setLockPassword(String password, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public boolean checkPassword(String password, int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public boolean checkVoldPassword(int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public boolean havePattern(int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public boolean havePassword(int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public void removeUser(int userId) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

	public void registerObserver(ILockSettingsObserver observer) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}

    public void unregisterObserver(ILockSettingsObserver observer) throws android.os.RemoteException {
		throw new RemoteException("Not implemented!");
	}
}
