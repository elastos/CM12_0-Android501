package com.android.internal.widget;

import android.content.Intent;
import android.os.RemoteException;
import android.widget.RemoteViews;
import android.util.Log;

public class ElIRemoteViewsFactoryProxy extends IRemoteViewsFactory.Stub {
	private long mNativeProxy;

	private native void nativeOnDataSetChanged(long proxy);

	private native void nativeOnDataSetChangedAsync(long proxy);

	private native void nativeOnDestroy(long proxy, Intent intent);

	private native int nativeGetCount(long proxy);

	private native RemoteViews nativeGetViewAt(long proxy, int position);

	private native RemoteViews nativeGetLoadingView(long proxy);

	private native int nativeGetViewTypeCount(long proxy);

	private native long nativeGetItemId(long proxy, int position);

	private native boolean nativeHasStableIds(long proxy);

	private native boolean nativeIsCreated(long proxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

	public ElIRemoteViewsFactoryProxy(long proxy) {
        mNativeProxy = proxy;
    }

	public void onDataSetChanged() throws RemoteException {
		nativeOnDataSetChanged(mNativeProxy);
	}

    public void onDataSetChangedAsync() throws RemoteException {
		nativeOnDataSetChangedAsync(mNativeProxy);
    }

    public void onDestroy(Intent intent) throws RemoteException {
		nativeOnDestroy(mNativeProxy, intent);
	}

    public int getCount() throws RemoteException {
		return nativeGetCount(mNativeProxy);
	}

    public RemoteViews getViewAt(int position) throws RemoteException {
		return nativeGetViewAt(mNativeProxy, position);
	}

    public RemoteViews getLoadingView() throws RemoteException {
		return nativeGetLoadingView(mNativeProxy);
	}

    public int getViewTypeCount() throws RemoteException {
		return nativeGetViewTypeCount(mNativeProxy);
	}

    public long getItemId(int position) throws RemoteException {
    	return nativeGetItemId(mNativeProxy, position);
	}

    public boolean hasStableIds() throws RemoteException {
    	return nativeHasStableIds(mNativeProxy);
	}

    public boolean isCreated() throws RemoteException {
    	return nativeIsCreated(mNativeProxy);
	}

}
