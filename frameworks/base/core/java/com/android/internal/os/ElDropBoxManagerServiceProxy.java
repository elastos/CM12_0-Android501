
package com.android.internal.os;

import android.os.DropBoxManager;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;

/**
 * @hide
 */
public class ElDropBoxManagerServiceProxy extends IDropBoxManagerService.Stub {

    private long mNativeProxy;

    private native void nativeAdd(long proxy, DropBoxManager.Entry entry);

    private native boolean nativeIsTagEnabled(long proxy, String tag);

    private native DropBoxManager.Entry nativeGetNextEntry(long proxy, String tag, long millis);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElDropBoxManagerServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void add(DropBoxManager.Entry entry) throws RemoteException {
        nativeAdd(mNativeProxy, entry);
    }

    @Override
    public boolean isTagEnabled(String tag) throws RemoteException {
        return nativeIsTagEnabled(mNativeProxy, tag);
    }

    @Override
    public DropBoxManager.Entry getNextEntry(String tag, long millis) throws RemoteException {
        return nativeGetNextEntry(mNativeProxy, tag, millis);
    }
}
