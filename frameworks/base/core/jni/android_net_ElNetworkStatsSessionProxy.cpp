
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Net.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Net::IINetworkStatsSession;

static jobject android_net_ElNetworkStatsSessionProxy_nativeGetSummaryForNetwork(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtemplate, jlong start, jlong end)
{
    // ALOGD("+ android_net_ElNetworkStatsSessionProxy_nativeGetSummaryForNetwork()");

    AutoPtr<INetworkTemplate> templ;
    if (jtemplate != NULL) {
        if (!ElUtil::ToElNetworkTemplate(env, jtemplate, (INetworkTemplate**)&templ))
            ALOGE("nativeGetSummaryForNetwork: ToElNetworkTemplate fail");
    }

    IINetworkStatsSession* nss = (IINetworkStatsSession*)jproxy;
    AutoPtr<INetworkStats> stats;
    nss->GetSummaryForNetwork(templ, start, end, (INetworkStats**)&stats);

    jobject jstats = NULL;
    if (stats != NULL) {
        jstats = ElUtil::GetJavaNetworkStats(env, stats);
    }
    // ALOGD("- android_net_ElNetworkStatsSessionProxy_nativeGetSummaryForNetwork()");
    return jstats;
}

static jobject android_net_ElNetworkStatsSessionProxy_nativeGetHistoryForNetwork(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtemplate, jint fields)
{
    // ALOGD("+ android_net_ElNetworkStatsSessionProxy_nativeGetHistoryForNetwork()");

    AutoPtr<INetworkTemplate> templ;
    if (jtemplate != NULL) {
        if (!ElUtil::ToElNetworkTemplate(env, jtemplate, (INetworkTemplate**)&templ))
            ALOGE("nativeGetHistoryForNetwork: ToElNetworkTemplate fail");
    }

    IINetworkStatsSession* nss = (IINetworkStatsSession*)jproxy;
    AutoPtr<INetworkStatsHistory> stats;
    nss->GetHistoryForNetwork(templ, fields, (INetworkStatsHistory**)&stats);

    jobject jstats = NULL;
    if (stats != NULL) {
        jstats = ElUtil::GetJavaNetworkStatsHistory(env, stats);
    }
    // ALOGD("- android_net_ElNetworkStatsSessionProxy_nativeGetHistoryForNetwork()");
    return jstats;
}

static jobject android_net_ElNetworkStatsSessionProxy_nativeGetSummaryForAllUid(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtemplate, jlong start, jlong end, jboolean includeTags)
{
    // ALOGD("+ android_net_ElNetworkStatsSessionProxy_nativeGetSummaryForAllUid()");

    AutoPtr<INetworkTemplate> templ;
    if (jtemplate != NULL) {
        if (!ElUtil::ToElNetworkTemplate(env, jtemplate, (INetworkTemplate**)&templ))
            ALOGE("nativeGetSummaryForAllUid: ToElNetworkTemplate fail");
    }

    IINetworkStatsSession* nss = (IINetworkStatsSession*)jproxy;
    AutoPtr<INetworkStats> stats;
    nss->GetSummaryForAllUid(templ, start, end, includeTags, (INetworkStats**)&stats);

    jobject jstats = NULL;
    if (stats != NULL) {
        jstats = ElUtil::GetJavaNetworkStats(env, stats);
    }
    // ALOGD("- android_net_ElNetworkStatsSessionProxy_nativeGetSummaryForAllUid()");
    return jstats;
}

static jobject android_net_ElNetworkStatsSessionProxy_nativeGetHistoryForUid(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtemplate, jint uid, jint set, jint tag, jint fields)
{
    // ALOGD("+ android_net_ElNetworkStatsSessionProxy_nativeGetHistoryForUid()");

    AutoPtr<INetworkTemplate> templ;
    if (jtemplate != NULL) {
        if (!ElUtil::ToElNetworkTemplate(env, jtemplate, (INetworkTemplate**)&templ))
            ALOGE("nativeGetHistoryForUid: ToElNetworkTemplate fail");
    }

    IINetworkStatsSession* nss = (IINetworkStatsSession*)jproxy;
    AutoPtr<INetworkStatsHistory> stats;
    nss->GetHistoryForUid(templ, uid, set, tag, fields, (INetworkStatsHistory**)&stats);

    jobject jstats = NULL;
    if (stats != NULL) {
        jstats = ElUtil::GetJavaNetworkStatsHistory(env, stats);
    }
    // ALOGD("- android_net_ElNetworkStatsSessionProxy_nativeGetHistoryForUid()");
    return jstats;
}

static void android_net_ElNetworkStatsSessionProxy_nativeClose(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkStatsSessionProxy_nativeClose()");

    IINetworkStatsSession* nss = (IINetworkStatsSession*)jproxy;
    nss->Close();

    // ALOGD("- android_net_ElNetworkStatsSessionProxy_nativeClose()");
}

static void android_net_ElNetworkStatsSessionProxy_nativeFinalize(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkStatsSessionProxy_nativeDestroy()");

    IINetworkStatsSession* obj = (IINetworkStatsSession*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_net_ElNetworkStatsSessionProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeGetSummaryForNetwork",      "(JLandroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;",
            (void*) android_net_ElNetworkStatsSessionProxy_nativeGetSummaryForNetwork },
    { "nativeGetHistoryForNetwork",      "(JLandroid/net/NetworkTemplate;I)Landroid/net/NetworkStatsHistory;",
            (void*) android_net_ElNetworkStatsSessionProxy_nativeGetHistoryForNetwork },
    { "nativeGetSummaryForAllUid",      "(JLandroid/net/NetworkTemplate;JJZ)Landroid/net/NetworkStats;",
            (void*) android_net_ElNetworkStatsSessionProxy_nativeGetSummaryForAllUid },
    { "nativeGetHistoryForUid",      "(JLandroid/net/NetworkTemplate;IIII)Landroid/net/NetworkStatsHistory;",
            (void*) android_net_ElNetworkStatsSessionProxy_nativeGetHistoryForUid },
    { "nativeClose",      "(J)V",
            (void*) android_net_ElNetworkStatsSessionProxy_nativeClose },
    { "nativeFinalize",      "(J)V",
            (void*) android_net_ElNetworkStatsSessionProxy_nativeFinalize },
};

int register_android_net_ElNetworkStatsSessionProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/net/ElNetworkStatsSessionProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

