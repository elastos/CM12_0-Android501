
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Os::IBundle;
using Elastos::Droid::Os::IIRemoteCallback;

static void android_os_ElIRemoteCallback_nativesendResult(JNIEnv* env, jobject clazz, jlong jproxy, jobject jdata)
{
    // ALOGD("+ android_os_ElIRemoteCallback_nativesendResult()");

    IIRemoteCallback* obj = (IIRemoteCallback*)jproxy;
    if (obj != NULL) {
        AutoPtr<IBundle> data;
        ElUtil::ToElBundle(env, jdata, (IBundle**)&data);
        obj->SendResult(data);
    }

    // ALOGD("- android_os_ElIRemoteCallback_nativesendResult()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativesendResult",    "(JLandroid/os/Bundle;)V",
            (void*) android_os_ElIRemoteCallback_nativesendResult },
};

int register_android_os_ElIRemoteCallback(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/os/ElIRemoteCallback",
        gMethods, NELEM(gMethods));
}

}; // namespace android
