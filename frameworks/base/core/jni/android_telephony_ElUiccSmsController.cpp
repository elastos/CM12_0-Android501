
#define LOG_TAG "ElUiccSmsControllerJNI"
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Telephony.h>
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Utility.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Utility.h>
#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::Internal::Telephony::IISms;
using Elastos::Droid::Internal::Telephony::ISmsRawData;
using Elastos::Droid::Utility::CParcelableList;

static jobject GetSmsRawDataJavaList(JNIEnv* env, IList* list)
{
    if (list == NULL) {
        return NULL;
    }

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "GetSmsRawDataJavaList FindClass: ArrayList : %d!", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "GetSmsRawDataJavaList Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jlist = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "GetSmsRawDataJavaList Fail NewObject: ArrayList : %d!\n", __LINE__);

    jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
    ElUtil::CheckErrorAndLog(env, "GetSmsRawDataJavaList Fail GetMethodID: add : %d!\n", __LINE__);
    env->DeleteLocalRef(listKlass);

    jclass srdClass = env->FindClass("com/android/internal/telephony/SmsRawData");
    ElUtil::CheckErrorAndLog(env, "GetSmsRawDataJavaList Error FindClass: SmsRawData : %d!\n", __LINE__);

    jmethodID init = env->GetMethodID(srdClass, "<init>", "([B)V");
    ElUtil::CheckErrorAndLog(env, "GetSmsRawDataJavaList GetMethodID: SmsRawData : %d!\n", __LINE__);

    Int32 size;
    list->GetSize(&size);
    for (Int32 i = 0; i < size; i++) {
        AutoPtr<IInterface> obj;
        list->Get(i, (IInterface**)&obj);
        AutoPtr<ArrayOf<Byte> > bytes;
        ISmsRawData::Probe(obj)->GetBytes((ArrayOf<Byte>**)&bytes);
        jbyteArray jbytearray = ElUtil::GetJavaByteArray(env, bytes);

        jobject data = env->NewObject(srdClass, init, jbytearray);
        ElUtil::CheckErrorAndLog(env, "GetSmsRawDataJavaList NewObject: SmsRawData : %d!\n", __LINE__);

        env->CallBooleanMethod(jlist, mAdd, data);
        ElUtil::CheckErrorAndLog(env, "GetSmsRawDataJavaList Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
        env->DeleteLocalRef(data);
    }

    env->DeleteLocalRef(srdClass);
    ALOGD("- GetSmsRawDataJavaList()");
    return jlist;
}

bool ToElPendingIntentList(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jlist,
    /* [out] */ IList** list)
{
    if (env == NULL || jlist == NULL || list == NULL) {
        ALOGE("ToElPendingIntentList() Invalid arguments!");
        return FALSE;
    }

    if (NOERROR != CParcelableList::New(list)) {
        return FALSE;
    }

    jclass listKlass = env->FindClass("java/util/List");
    ElUtil::CheckErrorAndLog(env, "FindClass: List : %d!\n", __LINE__);

    jmethodID mSize = env->GetMethodID(listKlass, "size", "()I");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: size : %d!\n", __LINE__);

    jint size = env->CallIntMethod(jlist, mSize);
    ElUtil::CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

    jmethodID mGet = env->GetMethodID(listKlass, "get", "(I)Ljava/lang/Object;");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

    for (jint i = 0; i < size; i++) {
        jobject pi = (jobject)env->CallObjectMethod(jlist, mGet, i);
        ElUtil::CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

        AutoPtr<IPendingIntent> obj;
        if (!ElUtil::ToElPendingIntent(env, pi, (IPendingIntent**)&obj)) {
            ALOGE("ToElPendingIntentList ToElPendingIntent [receivedIntent] fail!");
        }
        (*list)->Add(obj);

        env->DeleteLocalRef(pi);
    }
    env->DeleteLocalRef(listKlass);

    return true;
}

static void android_telephony_ElUiccSmsController_nativeSynthesizeMessages(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring originatingAddress, jstring scAddress, jobject messages, jlong timestampMillis)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSynthesizeMessages()");
    IISms* isms = (IISms*)jproxy;
    String oa = ElUtil::ToElString(env, originatingAddress);
    String sa = ElUtil::ToElString(env, scAddress);
    AutoPtr<IList> ms;
    if (messages != NULL) {
        if (!ElUtil::ToElStringList(env, messages, (IList**)&ms)) {
            ALOGE("nativeSynthesizeMessages: ToElStringList fail");
        }
    }
    isms->SynthesizeMessages(oa, sa, ms, (Int64)timestampMillis);
    ALOGD("- android_telephony_ElUiccSmsController_nativeSynthesizeMessages()");
}

static jobject android_telephony_ElUiccSmsController_nativeGetAllMessagesFromIccEf(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring callingPkg)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeGetAllMessagesFromIccEf()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    AutoPtr<IList> list;
    isms->GetAllMessagesFromIccEf(cp, (IList**)&list);
    if (list == NULL) {
        return NULL;
    }

    jobject result = GetSmsRawDataJavaList(env, list);

    ALOGD("- android_telephony_ElUiccSmsController_nativeGetAllMessagesFromIccEf()");
    return result;
}

static jobject android_telephony_ElUiccSmsController_nativeGetAllMessagesFromIccEfForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeGetAllMessagesFromIccEfForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    AutoPtr<IList> list;
    isms->GetAllMessagesFromIccEfForSubscriber((Int64)subId, cp, (IList**)&list);
    if (list == NULL) {
        return NULL;
    }

    jobject result = GetSmsRawDataJavaList(env, list);

    ALOGD("- android_telephony_ElUiccSmsController_nativeGetAllMessagesFromIccEfForSubscriber()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeUpdateMessageOnIccEf(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring callingPkg, jint messageIndex, jint newStatus,
    jbyteArray pdu)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeUpdateMessageOnIccEf()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    AutoPtr<ArrayOf<Byte> > byteArrayValue;
    if (!ElUtil::ToElByteArray(env, pdu, (ArrayOf<Byte>**)&byteArrayValue)) {
        ALOGE("nativeUpdateMessageOnIccEf ToElByteArray fail! Line: %d", __LINE__);
    }
    Boolean result = FALSE;
    isms->UpdateMessageOnIccEf(cp, (Int32)messageIndex, (Int32)newStatus, byteArrayValue, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeUpdateMessageOnIccEf()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeUpdateMessageOnIccEfForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg,
     jint messageIndex, jint newStatus, jbyteArray pdu)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeUpdateMessageOnIccEfForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    AutoPtr<ArrayOf<Byte> > byteArrayValue;
    if (!ElUtil::ToElByteArray(env, pdu, (ArrayOf<Byte>**)&byteArrayValue)) {
        ALOGE("nativeUpdateMessageOnIccEfForSubscriber ToElByteArray fail! Line: %d", __LINE__);
    }
    Boolean result = FALSE;
    isms->UpdateMessageOnIccEfForSubscriber((Int64)subId, cp, (Int32)messageIndex, (Int32)newStatus, byteArrayValue, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeUpdateMessageOnIccEfForSubscriber()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeCopyMessageToIccEf(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring callingPkg
    , jint status, jbyteArray pdu, jbyteArray smsc)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeCopyMessageToIccEf()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    AutoPtr<ArrayOf<Byte> > pdus;
    if (!ElUtil::ToElByteArray(env, pdu, (ArrayOf<Byte>**)&pdus)) {
        ALOGE("nativeCopyMessageToIccEf ToElByteArray fail! Line: %d", __LINE__);
    }

    AutoPtr<ArrayOf<Byte> > smscs;
    if (!ElUtil::ToElByteArray(env, smsc, (ArrayOf<Byte>**)&smscs)) {
        ALOGE("nativeCopyMessageToIccEf ToElByteArray fail! Line: %d", __LINE__);
    }

    Boolean result = FALSE;
    isms->CopyMessageToIccEf(cp, (Int32)status, pdus, smscs, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeCopyMessageToIccEf()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeCopyMessageToIccEfForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg, jint status,
    jbyteArray pdu, jbyteArray smsc)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeCopyMessageToIccEfForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    AutoPtr<ArrayOf<Byte> > pdus;
    if (!ElUtil::ToElByteArray(env, pdu, (ArrayOf<Byte>**)&pdus)) {
        ALOGE("nativeCopyMessageToIccEfForSubscriber ToElByteArray fail! Line: %d", __LINE__);
    }

    AutoPtr<ArrayOf<Byte> > smscs;
    if (!ElUtil::ToElByteArray(env, smsc, (ArrayOf<Byte>**)&smscs)) {
        ALOGE("nativeCopyMessageToIccEfForSubscriber ToElByteArray fail! Line: %d", __LINE__);
    }

    Boolean result = FALSE;
    isms->CopyMessageToIccEfForSubscriber((Int64)subId, cp, (Int32)status, pdus, smscs, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeCopyMessageToIccEfForSubscriber()");
    return result;
}

static void android_telephony_ElUiccSmsController_nativeSendData(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring callingPkg, jstring destAddr, jstring scAddr
    , jint destPort, jbyteArray data, jobject sentIntent, jobject deliveryIntent)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendData()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destAddr);
    String sa = ElUtil::ToElString(env, scAddr);
    AutoPtr<ArrayOf<Byte> > datas;
    if (!ElUtil::ToElByteArray(env, data, (ArrayOf<Byte>**)&datas)) {
        ALOGE("nativeSendData ToElByteArray fail! Line: %d", __LINE__);
    }

    AutoPtr<IPendingIntent> si;
    if (!ElUtil::ToElPendingIntent(env, sentIntent, (IPendingIntent**)&si)) {
        ALOGE("nativeSendData ToElPendingIntent [sentIntent] fail!");
    }

    AutoPtr<IPendingIntent> di;
    if (!ElUtil::ToElPendingIntent(env, deliveryIntent, (IPendingIntent**)&di)) {
        ALOGE("nativeSendData ToElPendingIntent [deliveryIntent] fail!");
    }

    isms->SendData(cp, da, sa, (Int32)destPort, datas, si, di);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendData()");
}

static void android_telephony_ElUiccSmsController_nativeSendDataForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg, jstring destAddr,
    jstring scAddr, jint destPort, jbyteArray data, jobject sentIntent,
    jobject deliveryIntent)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendDataForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destAddr);
    String sa = ElUtil::ToElString(env, scAddr);
    AutoPtr<ArrayOf<Byte> > datas;
    if (!ElUtil::ToElByteArray(env, data, (ArrayOf<Byte>**)&datas)) {
        ALOGE("nativeSendDataForSubscriber ToElByteArray fail! Line: %d", __LINE__);
    }

    AutoPtr<IPendingIntent> si;
    if (!ElUtil::ToElPendingIntent(env, sentIntent, (IPendingIntent**)&si)) {
        ALOGE("nativeSendDataForSubscriber ToElPendingIntent [sentIntent] fail!");
    }

    AutoPtr<IPendingIntent> di;
    if (!ElUtil::ToElPendingIntent(env, deliveryIntent, (IPendingIntent**)&di)) {
        ALOGE("nativeSendDataForSubscriber ToElPendingIntent [deliveryIntent] fail!");
    }

    isms->SendDataForSubscriber((Int64)subId, cp, da, sa, (Int32)destPort, datas, si, di);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendDataForSubscriber()");
}

static void android_telephony_ElUiccSmsController_nativeSendDataWithOrigPort(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring callingPkg, jstring destAddr, jstring scAddr,
    jint destPort, jint origPort, jbyteArray data, jobject sentIntent,
    jobject deliveryIntent)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendDataWithOrigPort()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destAddr);
    String sa = ElUtil::ToElString(env, scAddr);
    AutoPtr<ArrayOf<Byte> > datas;
    if (!ElUtil::ToElByteArray(env, data, (ArrayOf<Byte>**)&datas)) {
        ALOGE("nativeSendDataWithOrigPort ToElByteArray fail! Line: %d", __LINE__);
    }

    AutoPtr<IPendingIntent> si;
    if (!ElUtil::ToElPendingIntent(env, sentIntent, (IPendingIntent**)&si)) {
        ALOGE("nativeSendDataWithOrigPort ToElPendingIntent [sentIntent] fail!");
    }

    AutoPtr<IPendingIntent> di;
    if (!ElUtil::ToElPendingIntent(env, deliveryIntent, (IPendingIntent**)&di)) {
        ALOGE("nativeSendDataWithOrigPort ToElPendingIntent [deliveryIntent] fail!");
    }

    isms->SendDataWithOrigPort(cp, da, sa, (Int32)destPort, (Int32)origPort, datas, si, di);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendDataWithOrigPort()");
}

static void android_telephony_ElUiccSmsController_nativeSendDataWithOrigPortUsingSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg, jstring destAddr,
    jstring scAddr,jint destPort, jint origPort, jbyteArray data,
    jobject sentIntent, jobject deliveryIntent)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendDataWithOrigPortUsingSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destAddr);
    String sa = ElUtil::ToElString(env, scAddr);
    AutoPtr<ArrayOf<Byte> > datas;
    if (!ElUtil::ToElByteArray(env, data, (ArrayOf<Byte>**)&datas)) {
        ALOGE("nativeSendDataWithOrigPortUsingSubscriber ToElByteArray fail! Line: %d", __LINE__);
    }

    AutoPtr<IPendingIntent> si;
    if (!ElUtil::ToElPendingIntent(env, sentIntent, (IPendingIntent**)&si)) {
        ALOGE("nativeSendDataWithOrigPortUsingSubscriber ToElPendingIntent [sentIntent] fail!");
    }

    AutoPtr<IPendingIntent> di;
    if (!ElUtil::ToElPendingIntent(env, deliveryIntent, (IPendingIntent**)&di)) {
        ALOGE("nativeSendDataWithOrigPortUsingSubscriber ToElPendingIntent [deliveryIntent] fail!");
    }

    isms->SendDataWithOrigPortUsingSubscriber((Int64)subId, cp, da, sa, (Int32)destPort, (Int32)origPort
            , datas, si, di);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendDataWithOrigPortUsingSubscriber()");
}

static void android_telephony_ElUiccSmsController_nativeSendText(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring callingPkg, jstring destAddr, jstring scAddr
    , jstring text, jobject sentIntent, jobject deliveryIntent)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendText()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destAddr);
    String sa = ElUtil::ToElString(env, scAddr);
    String tx = ElUtil::ToElString(env, text);

    AutoPtr<IPendingIntent> si;
    if (!ElUtil::ToElPendingIntent(env, sentIntent, (IPendingIntent**)&si)) {
        ALOGE("nativeSendText ToElPendingIntent [sentIntent] fail!");
    }

    AutoPtr<IPendingIntent> di;
    if (!ElUtil::ToElPendingIntent(env, deliveryIntent, (IPendingIntent**)&di)) {
        ALOGE("nativeSendText ToElPendingIntent [deliveryIntent] fail!");
    }

    isms->SendText(cp, da, sa, tx, si, di);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendText()");
}

static void android_telephony_ElUiccSmsController_nativeSendTextForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg, jstring destAddr,
    jstring scAddr, jstring text, jobject sentIntent, jobject deliveryIntent)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendTextForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destAddr);
    String sa = ElUtil::ToElString(env, scAddr);
    String tx = ElUtil::ToElString(env, text);

    AutoPtr<IPendingIntent> si;
    if (!ElUtil::ToElPendingIntent(env, sentIntent, (IPendingIntent**)&si)) {
        ALOGE("nativeSendTextForSubscriber ToElPendingIntent [sentIntent] fail!");
    }

    AutoPtr<IPendingIntent> di;
    if (!ElUtil::ToElPendingIntent(env, deliveryIntent, (IPendingIntent**)&di)) {
        ALOGE("nativeSendTextForSubscriber ToElPendingIntent [deliveryIntent] fail!");
    }

    isms->SendTextForSubscriber((Int64)subId, cp, da, sa, tx, si, di);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendTextForSubscriber()");
}

static void android_telephony_ElUiccSmsController_nativeSendTextWithOptionsUsingSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg, jstring destAddr,
    jstring scAddr, jstring text, jobject sentIntent,
    jobject deliveryIntent, jint priority, jboolean isExpectMore, jint validityPeriod)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendTextWithOptionsUsingSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destAddr);
    String sa = ElUtil::ToElString(env, scAddr);
    String tx = ElUtil::ToElString(env, text);
    AutoPtr<IPendingIntent> si;
    if (!ElUtil::ToElPendingIntent(env, sentIntent, (IPendingIntent**)&si)) {
        ALOGE("nativeSendTextWithOptionsUsingSubscriber ToElPendingIntent [sentIntent] fail!");
    }

    AutoPtr<IPendingIntent> di;
    if (!ElUtil::ToElPendingIntent(env, deliveryIntent, (IPendingIntent**)&di)) {
        ALOGE("nativeSendTextWithOptionsUsingSubscriber ToElPendingIntent [deliveryIntent] fail!");
    }

    isms->SendTextWithOptionsUsingSubscriber((Int64)subId, cp, da, sa, tx, si, di
            , (Int32)priority, isExpectMore, (Int32)validityPeriod);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendTextWithOptionsUsingSubscriber()");
}

static void android_telephony_ElUiccSmsController_nativeInjectSmsPdu(
    JNIEnv* env, jobject clazz, jlong jproxy, jbyteArray pdu, jstring format, jobject receivedIntent)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeInjectSmsPdu()");
    IISms* isms = (IISms*)jproxy;
    String value = ElUtil::ToElString(env, format);
    AutoPtr<ArrayOf<Byte> > pdus;
    if (!ElUtil::ToElByteArray(env, pdu, (ArrayOf<Byte>**)&pdus)) {
        ALOGE("nativeInjectSmsPdu ToElByteArray fail! Line: %d", __LINE__);
    }

    AutoPtr<IPendingIntent> ri;
    if (!ElUtil::ToElPendingIntent(env, receivedIntent, (IPendingIntent**)&ri)) {
        ALOGE("nativeInjectSmsPdu ToElPendingIntent [receivedIntent] fail!");
    }

    isms->InjectSmsPdu(pdus, value, ri);

    ALOGD("- android_telephony_ElUiccSmsController_nativeInjectSmsPdu()");
}

static void android_telephony_ElUiccSmsController_nativeInjectSmsPduForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jbyteArray pdu, jstring format,
    jobject receivedIntent)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeInjectSmsPduForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String value = ElUtil::ToElString(env, format);
    AutoPtr<ArrayOf<Byte> > pdus;
    if (!ElUtil::ToElByteArray(env, pdu, (ArrayOf<Byte>**)&pdus)) {
        ALOGE("nativeInjectSmsPduForSubscriber ToElByteArray fail! Line: %d", __LINE__);
    }

    AutoPtr<IPendingIntent> ri;
    if (!ElUtil::ToElPendingIntent(env, receivedIntent, (IPendingIntent**)&ri)) {
        ALOGE("nativeInjectSmsPduForSubscriber ToElPendingIntent [receivedIntent] fail!");
    }

    isms->InjectSmsPduForSubscriber((Int64)subId, pdus, value, ri);

    ALOGD("- android_telephony_ElUiccSmsController_nativeInjectSmsPduForSubscriber()");
}

static void android_telephony_ElUiccSmsController_nativeUpdateSmsSendStatus(
    JNIEnv* env, jobject clazz, jlong jproxy, jint messageRef, jboolean success)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeUpdateSmsSendStatus()");
    IISms* isms = (IISms*)jproxy;
    isms->UpdateSmsSendStatus((Int32)messageRef, success);

    ALOGD("- android_telephony_ElUiccSmsController_nativeUpdateSmsSendStatus()");
}

static void android_telephony_ElUiccSmsController_nativeSendMultipartText(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring callingPkg, jstring destinationAddress, jstring scAddress,
    jobject parts, jobject sentIntents, jobject deliveryIntents)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendMultipartText()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destinationAddress);
    String sa = ElUtil::ToElString(env, scAddress);
    AutoPtr<IList> partList;
    if (parts != NULL) {
        if (!ElUtil::ToElStringList(env, parts, (IList**)&partList)) {
            ALOGE("nativeSendMultipartText: ToElStringList fail");
        }
    }

    AutoPtr<IList> sentIntentList;
    if (sentIntents != NULL) {
        if (!ToElPendingIntentList(env, sentIntents, (IList**)&sentIntentList)) {
            ALOGE("nativeSendMultipartText: ToElPendingIntentList fail");
        }
    }

    AutoPtr<IList> deliveryIntentList;
    if (deliveryIntents != NULL) {
        if (!ToElPendingIntentList(env, deliveryIntents, (IList**)&deliveryIntentList)) {
            ALOGE("nativeSendMultipartText: ToElPendingIntentList fail");
        }
    }
    isms->SendMultipartText(cp, da, sa, partList, sentIntentList, deliveryIntentList);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendMultipartText()");
}

static void android_telephony_ElUiccSmsController_nativeSendMultipartTextForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg,
    jstring destinationAddress, jstring scAddress, jobject parts, jobject sentIntents, jobject deliveryIntents)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendMultipartTextForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destinationAddress);
    String sa = ElUtil::ToElString(env, scAddress);
    AutoPtr<IList> partList;
    if (parts != NULL) {
        if (!ElUtil::ToElStringList(env, parts, (IList**)&partList)) {
            ALOGE("nativeSendMultipartTextForSubscriber: ToElStringList fail");
        }
    }

    AutoPtr<IList> sentIntentList;
    if (sentIntents != NULL) {
        if (!ToElPendingIntentList(env, sentIntents, (IList**)&sentIntentList)) {
            ALOGE("nativeSendMultipartTextForSubscriber: ToElPendingIntentList fail");
        }
    }

    AutoPtr<IList> deliveryIntentList;
    if (deliveryIntents != NULL) {
        if (!ToElPendingIntentList(env, deliveryIntents, (IList**)&deliveryIntentList)) {
            ALOGE("nativeSendMultipartTextForSubscriber: ToElPendingIntentList fail");
        }
    }
    isms->SendMultipartTextForSubscriber((Int64)subId, cp, da, sa, partList, sentIntentList, deliveryIntentList);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendMultipartTextForSubscriber()");
}

static void android_telephony_ElUiccSmsController_nativeSendMultipartTextWithOptionsUsingSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg,
    jstring destinationAddress, jstring scAddress, jobject parts,
    jobject sentIntents, jobject deliveryIntents,
    jint priority, jboolean isExpectMore, jint validityPeriod)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendMultipartTextWithOptionsUsingSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String da = ElUtil::ToElString(env, destinationAddress);
    String sa = ElUtil::ToElString(env, scAddress);
    AutoPtr<IList> partList;
    if (parts != NULL) {
        if (!ElUtil::ToElStringList(env, parts, (IList**)&partList)) {
            ALOGE("nativeSendMultipartTextWithOptionsUsingSubscriber: ToElStringList fail");
        }
    }

    AutoPtr<IList> sentIntentList;
    if (sentIntents != NULL) {
        if (!ToElPendingIntentList(env, sentIntents, (IList**)&sentIntentList)) {
            ALOGE("nativeSendMultipartTextWithOptionsUsingSubscriber: ToElPendingIntentList fail");
        }
    }

    AutoPtr<IList> deliveryIntentList;
    if (deliveryIntents != NULL) {
        if (!ToElPendingIntentList(env, deliveryIntents, (IList**)&deliveryIntentList)) {
            ALOGE("nativeSendMultipartTextWithOptionsUsingSubscriber: ToElPendingIntentList fail");
        }
    }
    isms->SendMultipartTextWithOptionsUsingSubscriber((Int64)subId, cp, da, sa, partList, sentIntentList
        , deliveryIntentList, (Int32)priority, isExpectMore, (Int32)validityPeriod);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendMultipartTextWithOptionsUsingSubscriber()");
}

static jboolean android_telephony_ElUiccSmsController_nativeEnableCellBroadcast(
    JNIEnv* env, jobject clazz, jlong jproxy, jint messageIdentifier)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeEnableCellBroadcast()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->EnableCellBroadcast((Int32)messageIdentifier, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeEnableCellBroadcast()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeEnableCellBroadcastForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jint messageIdentifier)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeEnableCellBroadcastForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->EnableCellBroadcastForSubscriber((Int64)subId, (Int32)messageIdentifier, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeEnableCellBroadcastForSubscriber()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeDisableCellBroadcast(
    JNIEnv* env, jobject clazz, jlong jproxy, jint messageIdentifier)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeDisableCellBroadcast()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->DisableCellBroadcast((Int32)messageIdentifier, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeDisableCellBroadcast()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeDisableCellBroadcastForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jint messageIdentifier)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeDisableCellBroadcastForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->DisableCellBroadcastForSubscriber((Int64)subId, (Int32)messageIdentifier, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeDisableCellBroadcastForSubscriber()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeEnableCellBroadcastRange(
    JNIEnv* env, jobject clazz, jlong jproxy, jint startMessageId, jint endMessageId)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeEnableCellBroadcastRange()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->EnableCellBroadcastRange((Int32)startMessageId, (Int32)endMessageId, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeEnableCellBroadcastRange()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeEnableCellBroadcastRangeForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jint startMessageId, jint endMessageId)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeEnableCellBroadcastRangeForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->EnableCellBroadcastRangeForSubscriber((Int64)subId, (Int32)startMessageId, (Int32)endMessageId, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeEnableCellBroadcastRangeForSubscriber()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeDisableCellBroadcastRange(
    JNIEnv* env, jobject clazz, jlong jproxy, jint startMessageId, jint endMessageId)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeDisableCellBroadcastRange()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->DisableCellBroadcastRange((Int32)startMessageId, (Int32)endMessageId, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeDisableCellBroadcastRange()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeDisableCellBroadcastRangeForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jint startMessageId,
    jint endMessageId)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeDisableCellBroadcastRangeForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->DisableCellBroadcastRangeForSubscriber((Int64)subId, (Int32)startMessageId, (Int32)endMessageId, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeDisableCellBroadcastRangeForSubscriber()");
    return result;
}

static jint android_telephony_ElUiccSmsController_nativeGetPremiumSmsPermission(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring packageName)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeGetPremiumSmsPermission()");
    IISms* isms = (IISms*)jproxy;
    String pn = ElUtil::ToElString(env, packageName);
    Int32 per;
    isms->GetPremiumSmsPermission(pn, &per);

    ALOGD("- android_telephony_ElUiccSmsController_nativeGetPremiumSmsPermission()");
    return (jint)per;
}

static jint android_telephony_ElUiccSmsController_nativeGetPremiumSmsPermissionForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring packageName)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeGetPremiumSmsPermissionForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String pn = ElUtil::ToElString(env, packageName);
    Int32 per;
    isms->GetPremiumSmsPermissionForSubscriber((Int64)subId, pn, &per);

    ALOGD("- android_telephony_ElUiccSmsController_nativeGetPremiumSmsPermissionForSubscriber()");
    return (jint)per;
}

static void android_telephony_ElUiccSmsController_nativeSetPremiumSmsPermission(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring packageName, jint permission)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSetPremiumSmsPermission()");
    IISms* isms = (IISms*)jproxy;
    String pn = ElUtil::ToElString(env, packageName);
    Int32 per;
    isms->SetPremiumSmsPermission(pn, (Int32)permission);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSetPremiumSmsPermission()");
}

static void android_telephony_ElUiccSmsController_nativeSetPremiumSmsPermissionForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring packageName, jint permission)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSetPremiumSmsPermissionForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String pn = ElUtil::ToElString(env, packageName);
    Int32 per;
    isms->SetPremiumSmsPermissionForSubscriber((Int64)subId, pn, (Int32)permission);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSetPremiumSmsPermissionForSubscriber()");
}

static jboolean android_telephony_ElUiccSmsController_nativeIsImsSmsSupported(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeIsImsSmsSupported()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->IsImsSmsSupported(&result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeIsImsSmsSupported()");
    return result;
}

static jboolean android_telephony_ElUiccSmsController_nativeIsImsSmsSupportedForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeIsImsSmsSupportedForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    Boolean result = FALSE;
    isms->IsImsSmsSupportedForSubscriber((Int64)subId, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeIsImsSmsSupportedForSubscriber()");
    return result;
}

static jstring android_telephony_ElUiccSmsController_nativeGetImsSmsFormat(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeGetImsSmsFormat()");
    IISms* isms = (IISms*)jproxy;
    String value;
    isms->GetImsSmsFormat(&value);
    jstring result = ElUtil::GetJavaString(env, value);

    ALOGD("- android_telephony_ElUiccSmsController_nativeGetImsSmsFormat()");
    return result;
}

static jstring android_telephony_ElUiccSmsController_nativeGetImsSmsFormatForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeGetImsSmsFormatForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    String value;
    isms->GetImsSmsFormatForSubscriber((Int64)subId, &value);
    jstring result = ElUtil::GetJavaString(env, value);

    ALOGD("- android_telephony_ElUiccSmsController_nativeGetImsSmsFormatForSubscriber()");
    return result;
}

static void android_telephony_ElUiccSmsController_nativeSendStoredText(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg, jobject messageUri,
    jstring scAddress, jobject sentIntent, jobject deliveryIntent)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendStoredText()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String sa = ElUtil::ToElString(env, scAddress);

    AutoPtr<IUri> uri;
    if (messageUri != NULL)  {
        if (!ElUtil::ToElUri(env, messageUri, (IUri**)&uri)) {
            ALOGE("nativeSendStoredText: ToElUri fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IPendingIntent> si;
    if (!ElUtil::ToElPendingIntent(env, sentIntent, (IPendingIntent**)&si)) {
        ALOGE("nativeSendStoredText ToElPendingIntent [sentIntent] fail!");
    }

    AutoPtr<IPendingIntent> di;
    if (!ElUtil::ToElPendingIntent(env, deliveryIntent, (IPendingIntent**)&di)) {
        ALOGE("nativeSendStoredText ToElPendingIntent [sentIntent] fail!");
    }
    isms->SendStoredText((Int64)subId, cp, uri, sa, si, di);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendStoredText()");
}

static void android_telephony_ElUiccSmsController_nativeSendStoredMultipartText(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jstring callingPkg, jobject messageUri,
        jstring scAddress, jobject sentIntents, jobject deliveryIntents)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeSendStoredMultipartText()");
    IISms* isms = (IISms*)jproxy;
    String cp = ElUtil::ToElString(env, callingPkg);
    String sa = ElUtil::ToElString(env, scAddress);

    AutoPtr<IUri> uri;
    if (messageUri != NULL)  {
        if (!ElUtil::ToElUri(env, messageUri, (IUri**)&uri)) {
            ALOGE("nativeSendStoredMultipartText: ToElUri fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IList> sentIntentList;
    if (sentIntents != NULL) {
        if (!ToElPendingIntentList(env, sentIntents, (IList**)&sentIntentList)) {
            ALOGE("nativeSendStoredMultipartText: ToElPendingIntentList fail");
        }
    }

    AutoPtr<IList> deliveryIntentList;
    if (deliveryIntents != NULL) {
        if (!ToElPendingIntentList(env, deliveryIntents, (IList**)&deliveryIntentList)) {
            ALOGE("nativeSendStoredMultipartText: ToElPendingIntentList fail");
        }
    }
    isms->SendStoredMultipartText((Int64)subId, cp, uri, sa, sentIntentList, deliveryIntentList);

    ALOGD("- android_telephony_ElUiccSmsController_nativeSendStoredMultipartText()");
}

static jint android_telephony_ElUiccSmsController_nativeGetSmsCapacityOnIccForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElUiccSmsController_nativeGetSmsCapacityOnIccForSubscriber()");
    IISms* isms = (IISms*)jproxy;
    Int32 result = 0;
    isms->GetSmsCapacityOnIccForSubscriber((Int64)subId, &result);

    ALOGD("- android_telephony_ElUiccSmsController_nativeGetSmsCapacityOnIccForSubscriber()");
    return result;
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeSynthesizeMessages",      "(JLjava/lang/String;Ljava/lang/String;Ljava/util/List;J)V",
            (void*) android_telephony_ElUiccSmsController_nativeSynthesizeMessages },
    { "nativeGetAllMessagesFromIccEf",      "(JLjava/lang/String;)Ljava/util/List;",
            (void*) android_telephony_ElUiccSmsController_nativeGetAllMessagesFromIccEf },
    { "nativeGetAllMessagesFromIccEfForSubscriber",      "(JJLjava/lang/String;)Ljava/util/List;",
            (void*) android_telephony_ElUiccSmsController_nativeGetAllMessagesFromIccEfForSubscriber },
    { "nativeUpdateMessageOnIccEf",      "(JLjava/lang/String;II[B)Z",
            (void*) android_telephony_ElUiccSmsController_nativeUpdateMessageOnIccEf },
    { "nativeUpdateMessageOnIccEfForSubscriber",      "(JJLjava/lang/String;II[B)Z",
            (void*) android_telephony_ElUiccSmsController_nativeUpdateMessageOnIccEfForSubscriber },
    { "nativeCopyMessageToIccEf",      "(JLjava/lang/String;I[B[B)Z",
            (void*) android_telephony_ElUiccSmsController_nativeCopyMessageToIccEf },
    { "nativeCopyMessageToIccEfForSubscriber",      "(JJLjava/lang/String;I[B[B)Z",
            (void*) android_telephony_ElUiccSmsController_nativeCopyMessageToIccEfForSubscriber },
    { "nativeSendData",
        "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendData },
    { "nativeSendDataForSubscriber",
        "(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendDataForSubscriber },
    { "nativeSendDataWithOrigPort",
        "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendDataWithOrigPort },
    { "nativeSendDataWithOrigPortUsingSubscriber",
        "(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendDataWithOrigPortUsingSubscriber },
    { "nativeSendText",      "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendText },
    { "nativeSendTextForSubscriber",
        "(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendTextForSubscriber },
    { "nativeSendTextWithOptionsUsingSubscriber",
        "(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;IZI)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendTextWithOptionsUsingSubscriber },
    { "nativeInjectSmsPdu",      "(J[BLjava/lang/String;Landroid/app/PendingIntent;)V",
            (void*) android_telephony_ElUiccSmsController_nativeInjectSmsPdu },
    { "nativeInjectSmsPduForSubscriber",      "(JJ[BLjava/lang/String;Landroid/app/PendingIntent;)V",
            (void*) android_telephony_ElUiccSmsController_nativeInjectSmsPduForSubscriber },
    { "nativeUpdateSmsSendStatus",      "(JIZ)V",
            (void*) android_telephony_ElUiccSmsController_nativeUpdateSmsSendStatus },
    { "nativeSendMultipartText",
        "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendMultipartText },
    { "nativeSendMultipartTextForSubscriber",
        "(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendMultipartTextForSubscriber },
    { "nativeSendMultipartTextWithOptionsUsingSubscriber",
        "(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;IZI)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendMultipartTextWithOptionsUsingSubscriber },
    { "nativeEnableCellBroadcast",      "(JI)Z",
            (void*) android_telephony_ElUiccSmsController_nativeEnableCellBroadcast },
    { "nativeEnableCellBroadcastForSubscriber",      "(JJI)Z",
            (void*) android_telephony_ElUiccSmsController_nativeEnableCellBroadcastForSubscriber },
    { "nativeDisableCellBroadcast",      "(JI)Z",
            (void*) android_telephony_ElUiccSmsController_nativeDisableCellBroadcast },
    { "nativeDisableCellBroadcastForSubscriber",      "(JJI)Z",
            (void*) android_telephony_ElUiccSmsController_nativeDisableCellBroadcastForSubscriber },
    { "nativeEnableCellBroadcastRange",      "(JII)Z",
            (void*) android_telephony_ElUiccSmsController_nativeEnableCellBroadcastRange },
    { "nativeEnableCellBroadcastRangeForSubscriber",      "(JJII)Z",
            (void*) android_telephony_ElUiccSmsController_nativeEnableCellBroadcastRangeForSubscriber },
    { "nativeDisableCellBroadcastRange",      "(JII)Z",
            (void*) android_telephony_ElUiccSmsController_nativeDisableCellBroadcastRange },
    { "nativeDisableCellBroadcastRangeForSubscriber",      "(JJII)Z",
            (void*) android_telephony_ElUiccSmsController_nativeDisableCellBroadcastRangeForSubscriber },
    { "nativeGetPremiumSmsPermission",      "(JLjava/lang/String;)I",
            (void*) android_telephony_ElUiccSmsController_nativeGetPremiumSmsPermission },
    { "nativeGetPremiumSmsPermissionForSubscriber",      "(JJLjava/lang/String;)I",
            (void*) android_telephony_ElUiccSmsController_nativeGetPremiumSmsPermissionForSubscriber },
    { "nativeSetPremiumSmsPermission",      "(JLjava/lang/String;I)V",
            (void*) android_telephony_ElUiccSmsController_nativeSetPremiumSmsPermission },
    { "nativeSetPremiumSmsPermissionForSubscriber",      "(JJLjava/lang/String;I)V",
            (void*) android_telephony_ElUiccSmsController_nativeSetPremiumSmsPermissionForSubscriber },
    { "nativeIsImsSmsSupported",      "(J)Z",
            (void*) android_telephony_ElUiccSmsController_nativeIsImsSmsSupported },
    { "nativeIsImsSmsSupportedForSubscriber",      "(JJ)Z",
            (void*) android_telephony_ElUiccSmsController_nativeIsImsSmsSupportedForSubscriber },
    { "nativeGetImsSmsFormat",      "(J)Ljava/lang/String;",
            (void*) android_telephony_ElUiccSmsController_nativeGetImsSmsFormat },
    { "nativeGetImsSmsFormatForSubscriber",      "(JJ)Ljava/lang/String;",
            (void*) android_telephony_ElUiccSmsController_nativeGetImsSmsFormatForSubscriber },
    { "nativeSendStoredText",
        "(JJLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendStoredText },
    { "nativeSendStoredMultipartText",
        "(JJLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V",
            (void*) android_telephony_ElUiccSmsController_nativeSendStoredMultipartText },
    { "nativeGetSmsCapacityOnIccForSubscriber",      "(JJ)I",
            (void*) android_telephony_ElUiccSmsController_nativeGetSmsCapacityOnIccForSubscriber },
};

int register_android_telephony_ElUiccSmsController(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/internal/telephony/ElUiccSmsController",
        gMethods, NELEM(gMethods));
}

}; // end namespace android
