
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.Droid.Internal.h>
#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Internal::View::IIInputMethodSession;
using Elastos::Droid::JavaProxy::IInputMethodSessionNative;

static void android_view_inputmethod_ElInputMethodSessionProxy_nativeSetEnabled(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jenabled)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodSessionProxy_nativeSetEnabled()");

    IInputMethodSessionNative* session = (IInputMethodSessionNative*)jproxy;

    session->SetEnabled((Boolean)jenabled);

    // ALOGD("- android_view_inputmethod_ElInputMethodSessionProxy_nativeSetEnabled()");
}

static void android_view_inputmethod_ElInputMethodSessionProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodSessionProxy_nativeFinalize()");

    IInputMethodSessionNative* obj = (IInputMethodSessionNative*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodSessionProxy_nativeFinalize()");
}


static void android_view_inputmethod_ElInputMethodSessionProxy_nativeFinishInput(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodSessionProxy_nativeFinishInput()");

    IInputMethodSessionNative* obj = (IInputMethodSessionNative*)jproxy;
    if (obj != NULL) {
        IIInputMethodSession::Probe(obj)->FinishInput();
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodSessionProxy_nativeFinishInput()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_view_inputmethod_ElInputMethodSessionProxy_nativeFinalize },
    { "nativeSetEnabled",    "(JZ)V",
            (void*) android_view_inputmethod_ElInputMethodSessionProxy_nativeSetEnabled },
    { "nativeFinishInput",      "(J)V",
            (void*) android_view_inputmethod_ElInputMethodSessionProxy_nativeFinishInput },
};

int register_android_view_inputmethod_ElInputMethodSessionProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/view/inputmethod/ElInputMethodSessionProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

