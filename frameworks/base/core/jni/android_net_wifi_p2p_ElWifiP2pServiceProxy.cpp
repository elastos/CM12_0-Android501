
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Wifi.h>

#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::Wifi::P2p::IIWifiP2pManager;
using Elastos::Droid::Os::IMessenger;

static jobject android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeGetMessenger(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeGetMessenger()");
    IIWifiP2pManager* wp = (IIWifiP2pManager*)jproxy;

    AutoPtr<IMessenger> msg;
    wp->GetMessenger((IMessenger**)&msg);

    jobject jmsg = NULL;
    if (msg != NULL) {
        jmsg = ElUtil::GetJavaMessenger(env, msg);
    }

    // ALOGD("- android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeGetMessenger()");
    return jmsg;
}

static jobject android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeGetP2pStateMachineMessenger(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeGetP2pStateMachineMessenger()");
    IIWifiP2pManager* wp = (IIWifiP2pManager*)jproxy;

    AutoPtr<IMessenger> msg;
    wp->GetP2pStateMachineMessenger((IMessenger**)&msg);

    jobject jmsg = NULL;
    if (msg != NULL) {
        jmsg = ElUtil::GetJavaMessenger(env, msg);
    }

    // ALOGD("- android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeGetP2pStateMachineMessenger()");
    return jmsg;
}

static void android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeSetMiracastMode(JNIEnv* env, jobject clazz, jlong jproxy, jint mode)
{
    // ALOGD("+ android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeSetMiracastMode()");

    IIWifiP2pManager* wp = (IIWifiP2pManager*)jproxy;
    wp->SetMiracastMode(mode);

    // ALOGD("- android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeSetMiracastMode()");
}


static void android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeDestroy()");

    IIWifiP2pManager* obj = (IIWifiP2pManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeFinalize },
    { "nativeGetMessenger",    "(J)Landroid/os/Messenger;",
            (void*) android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeGetMessenger },
    { "nativeGetP2pStateMachineMessenger",    "(J)Landroid/os/Messenger;",
            (void*) android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeGetP2pStateMachineMessenger },
    { "nativeSetMiracastMode",    "(JI)V",
            (void*) android_net_wifi_p2p_ElWifiP2pServiceProxy_nativeSetMiracastMode },
};

int register_android_net_wifi_p2p_ElWifiP2pServiceProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/net/wifi/p2p/ElWifiP2pServiceProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

