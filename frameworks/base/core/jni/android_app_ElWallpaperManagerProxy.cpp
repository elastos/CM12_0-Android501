
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Graphics.h>
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::App::IIWallpaperManager;
using Elastos::Droid::App::IWallpaperInfo;
using Elastos::Droid::App::IIWallpaperManagerCallback;
using Elastos::Droid::Content::IComponentName;
using Elastos::Droid::Os::IParcelFileDescriptor;
using Elastos::Droid::JavaProxy::CIWallpaperManagerCallbackNative;

static jobject android_app_ElWallpaperManagerProxy_nativeSetWallpaper(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeSetWallpaper()");

    String name = ElUtil::ToElString(env, jname);

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;

    AutoPtr<IParcelFileDescriptor> pfDescptor;
    ECode ec = wam->SetWallpaper(name, (IParcelFileDescriptor**)&pfDescptor);
    jobject jpfDescptor = NULL;
    if (pfDescptor != NULL) {
        jpfDescptor = ElUtil::GetJavaParcelFileDescriptor(env, pfDescptor);
    }

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeSetWallpaper()");
    return jpfDescptor;
}

static jobject android_app_ElWallpaperManagerProxy_nativeSetKeyguardWallpaper(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeSetKeyguardWallpaper()");

    String name = ElUtil::ToElString(env, jname);

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;

    AutoPtr<IParcelFileDescriptor> pfDescptor;
    ECode ec = wam->SetKeyguardWallpaper(name, (IParcelFileDescriptor**)&pfDescptor);
    jobject jpfDescptor = NULL;
    if (pfDescptor != NULL) {
        jpfDescptor = ElUtil::GetJavaParcelFileDescriptor(env, pfDescptor);
    }

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeSetKeyguardWallpaper()");
    return jpfDescptor;
}

static void android_app_ElWallpaperManagerProxy_nativeSetWallpaperComponent(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jname)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeSetWallpaperComponent()");

    AutoPtr<IComponentName> name;
    if (jname != NULL) {
        if (!ElUtil::ToElComponentName(env, jname, (IComponentName**)&name)) {
            ALOGE("nativeSetWallpaperComponent ToElComponentName fail!");
        }
    }

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;

    ECode ec = wam->SetWallpaperComponent(name);

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeSetWallpaperComponent()");
}

static jobject android_app_ElWallpaperManagerProxy_nativeGetWallpaper(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcb, jobject joutParams)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeGetWallpaper()");

    AutoPtr<IIWallpaperManagerCallback> cb;
    if (jcb != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcb);
        CIWallpaperManagerCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIWallpaperManagerCallback**)&cb);
    }

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;

    AutoPtr<IBundle> outParams;
    AutoPtr<IParcelFileDescriptor> pfDescptor;
    ECode ec = wam->GetWallpaper(cb, (IBundle**)&outParams, (IParcelFileDescriptor**)&pfDescptor);

    if (outParams != NULL) {
        if(!ElUtil::SetJavaBaseBundle(env, IBaseBundle::Probe(outParams), joutParams)) {
            ALOGE("nativeGetWallpaper() SetJavaBaseBundle fail!");
        }
    }

    jobject jpfDescptor = NULL;
    if (pfDescptor != NULL) {
        jpfDescptor = ElUtil::GetJavaParcelFileDescriptor(env, pfDescptor);
    }

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeGetWallpaper()");
    return jpfDescptor;
}

static jobject android_app_ElWallpaperManagerProxy_nativeGetKeyguardWallpaper(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcb, jobject joutParams)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeGetKeyguardWallpaper()");

    AutoPtr<IIWallpaperManagerCallback> cb;
    if (jcb != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcb);
        CIWallpaperManagerCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIWallpaperManagerCallback**)&cb);
    }

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;

    AutoPtr<IBundle> outParams;
    AutoPtr<IParcelFileDescriptor> pfDescptor;
    ECode ec = wam->GetKeyguardWallpaper(cb, (IBundle**)&outParams, (IParcelFileDescriptor**)&pfDescptor);

    if (outParams != NULL) {
        if(!ElUtil::SetJavaBaseBundle(env, IBaseBundle::Probe(outParams), joutParams)) {
            ALOGE("nativeGetKeyguardWallpaper() SetJavaBaseBundle fail!");
        }
    }

    jobject jpfDescptor = NULL;
    if (pfDescptor != NULL) {
        jpfDescptor = ElUtil::GetJavaParcelFileDescriptor(env, pfDescptor);
    }

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeGetKeyguardWallpaper()");
    return jpfDescptor;
}

static jobject android_app_ElWallpaperManagerProxy_nativeGetWallpaperInfo(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeGetWallpaperInfo()");

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;

    AutoPtr<IWallpaperInfo> info;
    ECode ec = wam->GetWallpaperInfo((IWallpaperInfo**)&info);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaWallpaperInfo(env, info);
    }

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeGetWallpaperInfo()");
    return jinfo;
}

static void android_app_ElWallpaperManagerProxy_nativeClearWallpaper(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeClearWallpaper()");

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;
    wam->ClearWallpaper();

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeClearWallpaper()");
}

static void android_app_ElWallpaperManagerProxy_nativeClearKeyguardWallpaper(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeClearKeyguardWallpaper()");

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;
    wam->ClearKeyguardWallpaper();

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeClearKeyguardWallpaper()");
}

static jboolean android_app_ElWallpaperManagerProxy_nativeHasNamedWallpaper(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeHasNamedWallpaper()");

    String name = ElUtil::ToElString(env, jname);

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;
    Boolean result = FALSE;
    wam->HasNamedWallpaper(name, &result);

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeHasNamedWallpaper()");
    return (jboolean)result;
}

static void android_app_ElWallpaperManagerProxy_nativeSetDimensionHints(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jwidth, jint jheight)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeSetDimensionHints()");

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;
    wam->SetDimensionHints((Int32)jwidth, (Int32)jheight);

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeSetDimensionHints()");
}

static jint android_app_ElWallpaperManagerProxy_nativeGetWidthHint(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeGetWidthHint()");

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;
    Int32 width = 0;
    wam->GetWidthHint(&width);

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeGetWidthHint()");
    return (jint)width;
}

static jint android_app_ElWallpaperManagerProxy_nativeGetHeightHint(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeGetHeightHint()");

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;
    Int32 height = 0;
    wam->GetHeightHint(&height);

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeGetHeightHint()");
    return (jint)height;
}

static void android_app_ElWallpaperManagerProxy_nativeSetDisplayPadding(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jpadding)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeSetDisplayPadding()");

    AutoPtr<IRect> padding;
    if (jpadding != NULL) {
        ElUtil::ToElRect(env, jpadding, (IRect**)&padding);
    }
    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;
    wam->SetDisplayPadding(padding);

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeSetDisplayPadding()");
}

static jstring android_app_ElWallpaperManagerProxy_nativeGetName(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeGetName()");

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;
    String name;
    wam->GetName(&name);

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeGetName()");
    return ElUtil::GetJavaString(env, name);
}

static void android_app_ElWallpaperManagerProxy_nativeSettingsRestored(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeSettingsRestored()");

    IIWallpaperManager* wam = (IIWallpaperManager*)jproxy;
    wam->SettingsRestored();

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeSettingsRestored()");
}

static void android_app_ElWallpaperManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_app_ElWallpaperManagerProxy_nativeDestroy()");

    IIWallpaperManager* obj = (IIWallpaperManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_app_ElWallpaperManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_app_ElWallpaperManagerProxy_nativeFinalize },
    { "nativeSetWallpaper",    "(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_app_ElWallpaperManagerProxy_nativeSetWallpaper },
    { "nativeSetKeyguardWallpaper",    "(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_app_ElWallpaperManagerProxy_nativeSetKeyguardWallpaper },
    { "nativeSetWallpaperComponent",    "(JLandroid/content/ComponentName;)V",
            (void*) android_app_ElWallpaperManagerProxy_nativeSetWallpaperComponent },
    { "nativeGetWallpaper",    "(JLandroid/app/IWallpaperManagerCallback;Landroid/os/Bundle;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_app_ElWallpaperManagerProxy_nativeGetWallpaper },
    { "nativeGetKeyguardWallpaper",    "(JLandroid/app/IWallpaperManagerCallback;Landroid/os/Bundle;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_app_ElWallpaperManagerProxy_nativeGetKeyguardWallpaper },
    { "nativeGetWallpaperInfo",    "(J)Landroid/app/WallpaperInfo;",
            (void*) android_app_ElWallpaperManagerProxy_nativeGetWallpaperInfo },
    { "nativeClearWallpaper",    "(J)V",
            (void*) android_app_ElWallpaperManagerProxy_nativeClearWallpaper },
    { "nativeClearKeyguardWallpaper",    "(J)V",
            (void*) android_app_ElWallpaperManagerProxy_nativeClearKeyguardWallpaper },
    { "nativeHasNamedWallpaper",    "(JLjava/lang/String;)Z",
            (void*) android_app_ElWallpaperManagerProxy_nativeHasNamedWallpaper },
    { "nativeSetDimensionHints",    "(JII)V",
            (void*) android_app_ElWallpaperManagerProxy_nativeSetDimensionHints },
    { "nativeGetWidthHint",    "(J)I",
            (void*) android_app_ElWallpaperManagerProxy_nativeGetWidthHint },
    { "nativeGetHeightHint",    "(J)I",
            (void*) android_app_ElWallpaperManagerProxy_nativeGetHeightHint },
    { "nativeSetDisplayPadding",    "(JLandroid/graphics/Rect;)V",
            (void*) android_app_ElWallpaperManagerProxy_nativeSetDisplayPadding },
    { "nativeGetName",    "(J)Ljava/lang/String;",
            (void*) android_app_ElWallpaperManagerProxy_nativeGetName },
    { "nativeSettingsRestored",    "(J)V",
            (void*) android_app_ElWallpaperManagerProxy_nativeSettingsRestored },
};

int register_android_app_ElWallpaperManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/app/ElWallpaperManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

