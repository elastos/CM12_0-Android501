
#include "JNIHelp.h"
#include "ElUtil.h"
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.CoreLibrary.Utility.h>

#include <utils/Log.h>
#include <utils/Mutex.h>
#include <elastos/utility/etl/HashMap.h>

using android::ElUtil;
using Elastos::Droid::Internal::App::IIAppOpsService;
using Elastos::Droid::Internal::App::IIAppOpsCallback;
using Elastos::Droid::Os::IBinder;
using Elastos::Utility::IList;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, AutoPtr<IIAppOpsCallback> > sCallbackMap;
static android::Mutex sCallbackMapLock;

static jint com_android_internal_app_ElAppOpsServiceProxy_nativeCheckOperation(
    JNIEnv* env, jobject clazz, jlong jproxy, jint code, jint uid, jstring jpackageName)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeCheckOperation()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    Int32 res;
    aos->CheckOperation(code, uid, packageName, &res);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeCheckOperation()");
    return res;
}

static jint com_android_internal_app_ElAppOpsServiceProxy_nativeNoteOperation(
    JNIEnv* env, jobject clazz, jlong jproxy, jint code, jint uid, jstring jpackageName)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeNoteOperation()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    Int32 res;
    aos->NoteOperation(code, uid, packageName, &res);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeNoteOperation()");
    return res;
}

static jint com_android_internal_app_ElAppOpsServiceProxy_nativeStartOperation(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jint code, jint uid, jstring jpackageName)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeStartOperation()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeStartOperation() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }
    else {
        ALOGE("nativeStartOperation() jtoken == NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    Int32 res;
    aos->StartOperation(token, code, uid, packageName, &res);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeStartOperation()");
    return res;
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeFinishOperation(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jint code, jint uid, jstring jpackageName)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeFinishOperation()");
    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeFinishOperation() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }
    else {
        ALOGE("nativeFinishOperation() jtoken == NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->FinishOperation(token, code, uid, packageName);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeFinishOperation()");
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeStartWatchingMode(
    JNIEnv* env, jobject clazz, jlong jproxy, jint op, jstring jpackageName, jobject jcallback)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeStartWatchingMode()");

    AutoPtr<IIAppOpsCallback> callback;
    if (jcallback != NULL) {
        ALOGE("nativeStartWatchingMode jcallback != NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->StartWatchingMode(op, packageName, callback);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeStartWatchingMode()");
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeStopWatchingMode(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcallback)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeStopWatchingMode()");

    AutoPtr<IIAppOpsCallback> callback;
    if (jcallback != NULL) {
        ALOGE("nativeStartWatchingMode jcallback != NULL!");
    }

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->StopWatchingMode(callback);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeStopWatchingMode()");
}

static jobject com_android_internal_app_ElAppOpsServiceProxy_nativeGetToken(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclientToken)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeGetToken()");

    AutoPtr<IBinder> clientToken;
    if (jclientToken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jclientToken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            clientToken = (IBinder*)(Int64)env->GetLongField(jclientToken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeGetToken() what is this clientToken ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    AutoPtr<IBinder> token;
    aos->GetToken(clientToken, (IBinder**)&token);

    jobject jtoken = NULL;
    if (token != NULL) {
        jclass bClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(bClass, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "nativeGetToken GetMethodID: ElBinderProxy : %d!\n", __LINE__);

        jtoken = env->NewObject(bClass, m, (jlong)token.Get());
        ElUtil::CheckErrorAndLog(env, "nativeGetToken NewObject: ElBinderProxy : %d!\n", __LINE__);
        token->AddRef();

        env->DeleteLocalRef(bClass);
    }

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeGetToken()");
    return jtoken;
}

static jint com_android_internal_app_ElAppOpsServiceProxy_nativeCheckPackage(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jstring jpackageName)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeCheckPackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    Int32 res;
    aos->CheckPackage(uid, packageName, &res);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeCheckPackage()");
    return res;
}

static jobject com_android_internal_app_ElAppOpsServiceProxy_nativeGetPackagesForOps(
    JNIEnv* env, jobject clazz, jlong jproxy, jintArray jops)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeGetPackagesForOps()");

    AutoPtr<ArrayOf<Int32> > ops;
    if (jops != NULL) {
        if (!ElUtil::ToElIntArray(env, jops, (ArrayOf<Int32>**)&ops)) {
            ALOGE("nativeGetPackagesForOps ToElIntArray fail!");
        }
    }
    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    AutoPtr<IList> list;
    aos->GetPackagesForOps(ops, (IList**)&list);
    jobject jlist = NULL;
    if (list != NULL) {
        ALOGE("nativeGetPackagesForOps list != NULL!");
    }

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeGetPackagesForOps()");
    return jlist;
}

static jobject com_android_internal_app_ElAppOpsServiceProxy_nativeGetOpsForPackage(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jstring jpackageName, jintArray jops)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeGetOpsForPackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<ArrayOf<Int32> > ops;
    if (jops != NULL) {
        if (!ElUtil::ToElIntArray(env, jops, (ArrayOf<Int32>**)&ops)) {
            ALOGE("nativeGetOpsForPackage ToElIntArray fail!");
        }
    }
    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    AutoPtr<IList> list;
    aos->GetOpsForPackage(uid, packageName, ops, (IList**)&list);
    jobject jlist = NULL;
    if (list != NULL) {
        ALOGE("nativeGetOpsForPackage list != NULL!");
    }

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeGetOpsForPackage()");
    return jlist;
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeSetMode(
    JNIEnv* env, jobject clazz, jlong jproxy, jint code, jint uid, jstring jpackageName, jint mode)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeSetMode()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->SetMode(code, uid, packageName, mode);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeSetMode()");
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeResetAllModes(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeResetAllModes()");

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->ResetAllModes();

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeResetAllModes()");
}

static jint com_android_internal_app_ElAppOpsServiceProxy_nativeCheckAudioOperation(
    JNIEnv* env, jobject clazz, jlong jproxy, jint code, jint usage, jint uid, jstring jpackageName)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeCheckAudioOperation()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    Int32 res;
    aos->CheckAudioOperation(code, usage, uid, packageName, &res);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeCheckAudioOperation()");
    return res;
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeSetAudioRestriction(
    JNIEnv* env, jobject clazz, jlong jproxy, jint code, jint usage, jint uid, jint mode, jobjectArray jexceptionPackages)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeSetAudioRestriction()");

    AutoPtr<ArrayOf<String> > exceptionPackages;
    if (jexceptionPackages != NULL) {
        if (!ElUtil::ToElStringArray(env, jexceptionPackages, (ArrayOf<String>**)&exceptionPackages)) {
            ALOGE("nativeSetAudioRestriction ToElStringArray fail!");
        }
    }
    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->SetAudioRestriction(code, usage, uid, mode, exceptionPackages);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeSetAudioRestriction()");
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeSetUserRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrestrictions, jint userHandle)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeSetUserRestrictions()");

    AutoPtr<IBundle> restrictions;
    if (jrestrictions != NULL) {
        if (!ElUtil::ToElBundle(env, jrestrictions, (IBundle**)&restrictions)) {
            ALOGE("nativeSetUserRestrictions ToElBundle fail!");
        }
    }

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->SetUserRestrictions(restrictions, userHandle);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeSetUserRestrictions()");
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeRemoveUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jint userHandle)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeRemoveUser()");

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->RemoveUser(userHandle);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeRemoveUser()");
}

static jboolean com_android_internal_app_ElAppOpsServiceProxy_nativeIsControlAllowed(
    JNIEnv* env, jobject clazz, jlong jproxy, jint code, jstring jpackageName)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeIsControlAllowed()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    Boolean res;
    aos->IsControlAllowed(code, packageName, &res);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeIsControlAllowed()");
    return res;
}

static jboolean com_android_internal_app_ElAppOpsServiceProxy_nativeGetPrivacyGuardSettingForPackage(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jstring jpackageName)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeGetPrivacyGuardSettingForPackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    Boolean res;
    aos->GetPrivacyGuardSettingForPackage(uid, packageName, &res);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeGetPrivacyGuardSettingForPackage()");
    return res;
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeSetPrivacyGuardSettingForPackage(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jstring jpackageName, jboolean state)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeSetPrivacyGuardSettingForPackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->SetPrivacyGuardSettingForPackage(uid, packageName, state);

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeSetPrivacyGuardSettingForPackage()");
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeResetCounters(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeResetCounters()");

    IIAppOpsService* aos = (IIAppOpsService*)jproxy;
    aos->ResetCounters();

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeResetCounters()");
}

static void com_android_internal_app_ElAppOpsServiceProxy_nativeFinalize(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ com_android_internal_app_ElAppOpsServiceProxy_nativeDestroy()");

    IIAppOpsService* obj = (IIAppOpsService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- com_android_internal_app_ElAppOpsServiceProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeFinalize },
    { "nativeCheckOperation",      "(JIILjava/lang/String;)I",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeCheckOperation },
    { "nativeNoteOperation",      "(JIILjava/lang/String;)I",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeNoteOperation },
    { "nativeStartOperation",      "(JLandroid/os/IBinder;IILjava/lang/String;)I",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeStartOperation },
    { "nativeFinishOperation",      "(JLandroid/os/IBinder;IILjava/lang/String;)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeFinishOperation },
    { "nativeStartWatchingMode",      "(JILjava/lang/String;Lcom/android/internal/app/IAppOpsCallback;)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeStartWatchingMode },
    { "nativeStopWatchingMode",      "(JLcom/android/internal/app/IAppOpsCallback;)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeStopWatchingMode },
    { "nativeGetToken",      "(JLandroid/os/IBinder;)Landroid/os/IBinder;",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeGetToken },
    { "nativeCheckPackage",      "(JILjava/lang/String;)I",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeCheckPackage },
    { "nativeGetPackagesForOps",      "(J[I)Ljava/util/List;",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeGetPackagesForOps },
    { "nativeGetOpsForPackage",      "(JILjava/lang/String;[I)Ljava/util/List;",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeGetOpsForPackage },
    { "nativeSetMode",      "(JIILjava/lang/String;I)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeSetMode },
    { "nativeResetAllModes",      "(J)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeResetAllModes },
    { "nativeCheckAudioOperation",      "(JIIILjava/lang/String;)I",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeCheckAudioOperation },
    { "nativeSetAudioRestriction",      "(JIIII[Ljava/lang/String;)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeSetAudioRestriction },
    { "nativeSetUserRestrictions",      "(JLandroid/os/Bundle;I)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeSetUserRestrictions },
    { "nativeRemoveUser",      "(JI)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeRemoveUser },
    { "nativeIsControlAllowed",      "(JILjava/lang/String;)Z",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeIsControlAllowed },
    { "nativeGetPrivacyGuardSettingForPackage",      "(JILjava/lang/String;)Z",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeGetPrivacyGuardSettingForPackage },
    { "nativeSetPrivacyGuardSettingForPackage",      "(JILjava/lang/String;Z)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeSetPrivacyGuardSettingForPackage },
    { "nativeResetCounters",      "(J)V",
            (void*) com_android_internal_app_ElAppOpsServiceProxy_nativeResetCounters },
};


int register_com_android_internal_app_ElAppOpsServiceProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/internal/app/ElAppOpsServiceProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

