
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Internal::Net::IVpnProfile;
using Elastos::Droid::Net::CNetworkInfo;
using Elastos::Droid::Net::INetworkInfo;
using Elastos::Droid::Net::INetworkQuotaInfo;
using Elastos::Droid::Net::INetworkState;
using Elastos::Droid::Net::ILinkProperties;
using Elastos::Droid::Net::IProxyInfo;
using Elastos::Droid::Net::IIConnectivityManager;
using Elastos::Droid::Net::CConnectivityManagerHelper;
using Elastos::Droid::Net::IConnectivityManagerHelper;
using Elastos::Droid::Os::IBinder;

static jobject android_net_ElConnectivityManagerProxy_nativeGetNetworkInfo(JNIEnv* env, jobject clazz, jlong jproxy, jint jnetworkType)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetNetworkInfo()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<INetworkInfo> info;
    connMagr->GetNetworkInfo((Int32)jnetworkType, (INetworkInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaNetworkInfo(env, info);
    }else{
        ALOGE("android_net_ElConnectivityManagerProxy_nativeGetNetworkInfo(), return a fake state with unknown state for net type:%d!", jnetworkType);
        AutoPtr<IConnectivityManagerHelper> helper;
        CConnectivityManagerHelper::AcquireSingleton((IConnectivityManagerHelper**)&helper);
        String networkTypeName;
        helper->GetNetworkTypeName((Int32)jnetworkType, &networkTypeName);

        CNetworkInfo::New((Int32)jnetworkType, (Int32)jnetworkType, networkTypeName, String(""), (INetworkInfo**)&info);
        info->SetIsAvailable(FALSE);
        info->SetRoaming(FALSE);
        info->SetFailover(TRUE);
        info->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_IDLE, String(NULL), String(NULL));

        jinfo = ElUtil::GetJavaNetworkInfo(env, info);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetNetworkInfo()");
    return jinfo;
}

static jobject android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkInfo(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkInfo()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<INetworkInfo> info;
    connMagr->GetActiveNetworkInfo((INetworkInfo**)&info);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaNetworkInfo(env, info);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkInfo()");
    return jinfo;
}

static jobject android_net_ElConnectivityManagerProxy_nativeGetLinkPropertiesForType(JNIEnv* env, jobject clazz, jlong jproxy, jint jnetworkType)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetLinkPropertiesForType()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;

    AutoPtr<ILinkProperties> properties;
    ECode ec = connMagr->GetLinkPropertiesForType((Int32)jnetworkType, (ILinkProperties**)&properties);
    jobject jproperties = NULL;
    if (properties != NULL) {
        jproperties = ElUtil::GetJavaLinkProperties(env, properties);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetLinkPropertiesForType()");
    return jproperties;
}

static jobject android_net_ElConnectivityManagerProxy_nativeGetActiveLinkProperties(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetActiveLinkProperties()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;

    AutoPtr<ILinkProperties> properties;
    ECode ec = connMagr->GetActiveLinkProperties((ILinkProperties**)&properties);
    jobject jproperties = NULL;
    if (properties != NULL) {
        jproperties = ElUtil::GetJavaLinkProperties(env, properties);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetActiveLinkProperties()");
    return jproperties;
}

static jboolean android_net_ElConnectivityManagerProxy_nativeIsNetworkSupported(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jnetworkType)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeIsNetworkSupported()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;

    Boolean result;
    connMagr->IsNetworkSupported((Int32)jnetworkType, &result);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeIsNetworkSupported()");
    return (jboolean)result;
}

static jobjectArray android_net_ElConnectivityManagerProxy_nativeGetAllNetworkInfo(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetAllNetworkInfo()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;

    AutoPtr<ArrayOf<INetworkInfo*> > allInfo;
    connMagr->GetAllNetworkInfo((ArrayOf<INetworkInfo*>**)&allInfo);
    jobjectArray jallInfo = NULL;
    if (allInfo != NULL) {
        jclass netInfoKlass = env->FindClass("android/net/NetworkInfo");
        ElUtil::CheckErrorAndLog(env, "nativeGetAllNetworkInfo FindClass: NetworkInfo : %d!\n", __LINE__);

        Int32 count = allInfo->GetLength();
        jallInfo = env->NewObjectArray((jsize)count, netInfoKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "nativeGetAllNetworkInfo NewObjectArray: NetworkInfo : %d!\n", __LINE__);

        for (Int32 i = 0; i < count; i++) {
            AutoPtr<INetworkInfo> info = (*allInfo)[i];

            jobject jinfo = ElUtil::GetJavaNetworkInfo(env, info);

            env->SetObjectArrayElement(jallInfo, i, jinfo);
            ElUtil::CheckErrorAndLog(env, "nativeGetAllNetworkInfo SetObjectArrayElement: NetworkInfo : %d!\n", __LINE__);

            env->DeleteLocalRef(jinfo);
        }

        env->DeleteLocalRef(netInfoKlass);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetAllNetworkInfo()");
    return jallInfo;
}

static jobject android_net_ElConnectivityManagerProxy_nativeGetProxy(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetProxy()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;

    AutoPtr<IProxyInfo> proxy;
    connMagr->GetProxy((IProxyInfo**)&proxy);
    jobject jproxyPro = NULL;
    if (proxy != NULL) {
        jproxyPro = ElUtil::GetJavaProxyInfo(env, proxy);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetProxy()");
    return jproxyPro;
}

static jobject android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkInfoForUid(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juid)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkInfoForUid()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<INetworkInfo> info;
    connMagr->GetActiveNetworkInfoForUid(juid, (INetworkInfo**)&info);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaNetworkInfo(env, info);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkInfoForUid()");
    return jinfo;
}

static void android_net_ElConnectivityManagerProxy_nativeStartLegacyVpn(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jprofile)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeStartLegacyVpn()");

    AutoPtr<IVpnProfile> profile;
    if (jprofile != NULL) {
        if (!ElUtil::ToElVpnProfile(env, jprofile, (IVpnProfile**)&profile)) {
            ALOGE("nativeStartLegacyVpn() ToElVpnProfile fail!");
        }
    }

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    // connMagr->StartLegacyVpn(profile);
    ALOGE("nativeStartLegacyVpn() E_NOT_IMPLEMENTED");

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeStartLegacyVpn()");
}

static void android_net_ElConnectivityManagerProxy_nativeReportInetCondition(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jnetworkType, jint jpercentage)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeReportInetCondition()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    connMagr->ReportInetCondition((Int32)jnetworkType, (Int32)jpercentage);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeReportInetCondition()");
}

static jobjectArray android_net_ElConnectivityManagerProxy_nativeGetAllNetworkState(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetAllNetworkState()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<ArrayOf<INetworkState*> > states;
    ECode ec = connMagr->GetAllNetworkState((ArrayOf<INetworkState*>**)&states);
    if (FAILED(ec))
        ALOGE("nativeGetAllNetworkState() ec: 0x%08x", ec);

    jobjectArray jstates = NULL;
    if (states != NULL) {
        jclass nsKlass = env->FindClass("android/net/NetworkState");
        ElUtil::CheckErrorAndLog(env, "FindClass: NetworkState : %d!\n", __LINE__);

        Int32 count = states->GetLength();
        jstates = env->NewObjectArray((jsize)count, nsKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: NetworkState : %d!\n", __LINE__);

        for (Int32 i = 0; i < count; i++) {
            AutoPtr<INetworkState> state = (*states)[i];

            jobject jstate = ElUtil::GetJavaNetworkState(env, state);

            env->SetObjectArrayElement(jstates, i, jstate);
            ElUtil::CheckErrorAndLog(env, "SetObjectArrayElement: NetworkState : %d!\n", __LINE__);

            env->DeleteLocalRef(jstate);
        }

        env->DeleteLocalRef(nsKlass);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetAllNetworkState()");
    return jstates;
}

static jobject android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkQuotaInfo(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkQuotaInfo()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<INetworkQuotaInfo> info;
    ECode ec = connMagr->GetActiveNetworkQuotaInfo((INetworkQuotaInfo**)&info);
    if (FAILED(ec))
        ALOGE("nativeGetActiveNetworkQuotaInfo() ec: 0x%08x", ec);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaNetworkQuotaInfo(env, info);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkQuotaInfo()");
    return jinfo;
}

static jboolean android_net_ElConnectivityManagerProxy_nativeIsActiveNetworkMetered(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeIsActiveNetworkMetered()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = connMagr->IsActiveNetworkMetered(&result);
    if (FAILED(ec))
        ALOGE("nativeIsActiveNetworkMetered() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeIsActiveNetworkMetered()");
    return (jboolean)result;
}

static jboolean android_net_ElConnectivityManagerProxy_nativeRequestRouteToHostAddress(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jnetworkType, jbyteArray jhostAddress)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeRequestRouteToHostAddress()");

    AutoPtr<ArrayOf<Byte> >hostAddress;
    if (jhostAddress != NULL) {
        if (!ElUtil::ToElByteArray(env, jhostAddress, (ArrayOf<Byte>**)&hostAddress)) {
            ALOGE("nativeRequestRouteToHostAddress() ToElByteArray fail!");
        }
    }

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = connMagr->RequestRouteToHostAddress((Int32)jnetworkType, hostAddress, &result);
    if (FAILED(ec))
        ALOGE("nativeRequestRouteToHostAddress() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeRequestRouteToHostAddress()");
    return (jboolean)result;
}

static void android_net_ElConnectivityManagerProxy_nativeSetPolicyDataEnable(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jnetworkType, jboolean jenabled)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeSetPolicyDataEnable()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    ECode ec = connMagr->SetPolicyDataEnable((Int32)jnetworkType, (Boolean)jenabled);
    if (FAILED(ec))
        ALOGE("nativeSetPolicyDataEnable() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeSetPolicyDataEnable()");
}

static jint android_net_ElConnectivityManagerProxy_nativeTether(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jiface)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeTether()");

    String iface = ElUtil::ToElString(env, jiface);

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    Int32 result = 0;
    ECode ec = connMagr->Tether(iface, &result);
    if (FAILED(ec))
        ALOGE("nativeTether() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeTether()");
    return (jint)result;
}

static jint android_net_ElConnectivityManagerProxy_nativeUntether(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jiface)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeUntether()");

    String iface = ElUtil::ToElString(env, jiface);

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    Int32 result = 0;
    ECode ec = connMagr->Untether(iface, &result);
    if (FAILED(ec))
        ALOGE("nativeUntether() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeUntether()");
    return (jint)result;
}

static jint android_net_ElConnectivityManagerProxy_nativeGetLastTetherError(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jiface)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetLastTetherError()");

    String iface = ElUtil::ToElString(env, jiface);

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    Int32 error = 0;
    ECode ec = connMagr->GetLastTetherError(iface, &error);
    if (FAILED(ec))
        ALOGE("nativeGetLastTetherError() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetLastTetherError()");
    return (jint)error;
}

static jboolean android_net_ElConnectivityManagerProxy_nativeIsTetheringSupported(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeIsTetheringSupported()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = connMagr->IsTetheringSupported(&result);
    if (FAILED(ec))
        ALOGE("nativeIsTetheringSupported() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeIsTetheringSupported()");
    return (jboolean)result;
}

static jobjectArray android_net_ElConnectivityManagerProxy_nativeGetTetherableIfaces(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetTetherableIfaces()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<ArrayOf<String> > ifaces;
    ECode ec = connMagr->GetTetherableIfaces((ArrayOf<String>**)&ifaces);
    if (FAILED(ec))
        ALOGE("nativeGetTetherableIfaces() ec: 0x%08x", ec);

    jobjectArray jifaces = NULL;
    if (ifaces != NULL) {
        jifaces = ElUtil::GetJavaStringArray(env, ifaces);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetTetherableIfaces()");
    return jifaces;
}

static jobjectArray android_net_ElConnectivityManagerProxy_nativeGetTetheredIfaces(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetTetheredIfaces()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<ArrayOf<String> > ifaces;
    ECode ec = connMagr->GetTetheredIfaces((ArrayOf<String>**)&ifaces);
    if (FAILED(ec))
        ALOGE("nativeGetTetheredIfaces() ec: 0x%08x", ec);

    jobjectArray jifaces = NULL;
    if (ifaces != NULL) {
        jifaces = ElUtil::GetJavaStringArray(env, ifaces);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetTetheredIfaces()");
    return jifaces;
}

static jobjectArray android_net_ElConnectivityManagerProxy_nativeGetTetheringErroredIfaces(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetTetheringErroredIfaces()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<ArrayOf<String> > ifaces;
    ECode ec = connMagr->GetTetheringErroredIfaces((ArrayOf<String>**)&ifaces);
    if (FAILED(ec))
        ALOGE("nativeGetTetheringErroredIfaces() ec: 0x%08x", ec);

    jobjectArray jifaces = NULL;
    if (ifaces != NULL) {
        jifaces = ElUtil::GetJavaStringArray(env, ifaces);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetTetheringErroredIfaces()");
    return jifaces;
}

static jobjectArray android_net_ElConnectivityManagerProxy_nativeGetTetheredDhcpRanges(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetTetheredDhcpRanges()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<ArrayOf<String> > ifaces;
    ECode ec = connMagr->GetTetheredDhcpRanges((ArrayOf<String>**)&ifaces);
    if (FAILED(ec))
        ALOGE("nativeGetTetheredDhcpRanges() ec: 0x%08x", ec);

    jobjectArray jifaces = NULL;
    if (ifaces != NULL) {
        jifaces = ElUtil::GetJavaStringArray(env, ifaces);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetTetheredDhcpRanges()");
    return jifaces;
}

static jobjectArray android_net_ElConnectivityManagerProxy_nativeGetTetherableUsbRegexs(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetTetherableUsbRegexs()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<ArrayOf<String> > regexs;
    ECode ec = connMagr->GetTetherableUsbRegexs((ArrayOf<String>**)&regexs);
    if (FAILED(ec))
        ALOGE("nativeGetTetherableUsbRegexs() ec: 0x%08x", ec);

    jobjectArray jregexs = NULL;
    if (regexs != NULL) {
        jregexs = ElUtil::GetJavaStringArray(env, regexs);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetTetherableUsbRegexs()");
    return jregexs;
}

static jobjectArray android_net_ElConnectivityManagerProxy_nativeGetTetherableWifiRegexs(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetTetherableWifiRegexs()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<ArrayOf<String> > regexs;
    ECode ec = connMagr->GetTetherableWifiRegexs((ArrayOf<String>**)&regexs);
    if (FAILED(ec))
        ALOGE("nativeGetTetherableWifiRegexs() ec: 0x%08x", ec);

    jobjectArray jregexs = NULL;
    if (regexs != NULL) {
        jregexs = ElUtil::GetJavaStringArray(env, regexs);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetTetherableWifiRegexs()");
    return jregexs;
}

static jobjectArray android_net_ElConnectivityManagerProxy_nativeGetTetherableBluetoothRegexs(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetTetherableBluetoothRegexs()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<ArrayOf<String> > regexs;
    ECode ec = connMagr->GetTetherableBluetoothRegexs((ArrayOf<String>**)&regexs);
    if (FAILED(ec))
        ALOGE("nativeGetTetherableBluetoothRegexs() ec: 0x%08x", ec);

    jobjectArray jregexs = NULL;
    if (regexs != NULL) {
        jregexs = ElUtil::GetJavaStringArray(env, regexs);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetTetherableBluetoothRegexs()");
    return jregexs;
}

static jint android_net_ElConnectivityManagerProxy_nativeSetUsbTethering(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jenable)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeSetUsbTethering()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    Int32 result = 0;
    ECode ec = connMagr->SetUsbTethering((Boolean)jenable, &result);
    if (FAILED(ec))
        ALOGE("nativeSetUsbTethering() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeSetUsbTethering()");
    return (jint)result;
}

static jobject android_net_ElConnectivityManagerProxy_nativeGetGlobalProxy(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetGlobalProxy()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<IProxyInfo> pproperties;
    ECode ec = connMagr->GetGlobalProxy((IProxyInfo**)&pproperties);
    if (FAILED(ec))
        ALOGE("nativeGetGlobalProxy() ec: 0x%08x", ec);

    jobject jpproperties = NULL;
    if (pproperties != NULL) {
        jpproperties = ElUtil::GetJavaProxyInfo(env, pproperties);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetGlobalProxy()");
    return jpproperties;
}

static void android_net_ElConnectivityManagerProxy_nativeSetGlobalProxy(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jp)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeSetGlobalProxy()");

    AutoPtr<IProxyInfo> p;
    if (jp != NULL) {
        if (!ElUtil::ToElProxyInfo(env, jp, (IProxyInfo**)&p)) {
            ALOGE("nativeSetGlobalProxy() ToElIProxyInfo fail!");
        }
    }

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    ECode ec = connMagr->SetGlobalProxy(p);
    if (FAILED(ec))
        ALOGE("nativeSetGlobalProxy() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeSetGlobalProxy()");
}

static void android_net_ElConnectivityManagerProxy_nativeSetDataDependency(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jnetworkType, jboolean jmet)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeSetDataDependency()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    ECode ec = connMagr->SetDataDependency((Int32)jnetworkType, (Boolean)jmet);
    if (FAILED(ec))
        ALOGE("nativeSetDataDependency() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeSetDataDependency()");
}

static jboolean android_net_ElConnectivityManagerProxy_nativePrepareVpn(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring joldPackage, jstring jnewPackage)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativePrepareVpn()");

    String oldPackage = ElUtil::ToElString(env, joldPackage);
    String newPackage = ElUtil::ToElString(env, jnewPackage);

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = connMagr->PrepareVpn(oldPackage, newPackage, &result);
    if (FAILED(ec))
        ALOGE("nativePrepareVpn() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativePrepareVpn()");
    return (jboolean)result;
}

static void android_net_ElConnectivityManagerProxy_nativeSetVpnPackageAuthorization(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean authorized)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeSetVpnPackageAuthorization()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    ECode ec = connMagr->SetVpnPackageAuthorization(authorized);
    if (FAILED(ec))
        ALOGE("nativeSetVpnPackageAuthorization() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeSetVpnPackageAuthorization()");
}

static jobject android_net_ElConnectivityManagerProxy_nativeEstablishVpn(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jconfig)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeEstablishVpn()");

    AutoPtr<IVpnConfig> config;
    if (jconfig != NULL) {
        if (!ElUtil::ToElVpnConfig(env, jconfig, (IVpnConfig**)&config)) {
            ALOGE("nativeEstablishVpn() ToElVpnConfig fail!");
        }
    }

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<IParcelFileDescriptor> fd;
    ECode ec = connMagr->EstablishVpn(config, (IParcelFileDescriptor**)&fd);
    if (FAILED(ec))
        ALOGE("nativeEstablishVpn() ec: 0x%08x", ec);

    jobject jfd = NULL;
    if (fd != NULL) {
        jfd = ElUtil::GetJavaParcelFileDescriptor(env, fd);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeEstablishVpn()");
    return jfd;
}

static jobject android_net_ElConnectivityManagerProxy_nativeGetLegacyVpnInfo(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeGetLegacyVpnInfo()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    AutoPtr<ILegacyVpnInfo> info;
    ECode ec = connMagr->GetLegacyVpnInfo((ILegacyVpnInfo**)&info);
    if (FAILED(ec))
        ALOGE("nativeGetLegacyVpnInfo() ec: 0x%08x", ec);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaLegacyVpnInfo(env, info);
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeGetLegacyVpnInfo()");
    return jinfo;
}

static jboolean android_net_ElConnectivityManagerProxy_nativeUpdateLockdownVpn(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeUpdateLockdownVpn()");

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = connMagr->UpdateLockdownVpn(&result);
    if (FAILED(ec))
        ALOGE("nativeUpdateLockdownVpn() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeUpdateLockdownVpn()");
    return (jboolean)result;
}

static void android_net_ElConnectivityManagerProxy_nativeCaptivePortalCheckCompleted(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jinfo, jboolean isCaptivePortal)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeCaptivePortalCheckCompleted()");

    AutoPtr<INetworkInfo> info;
    if (jinfo != NULL) {
        if (!ElUtil::ToElNetworkInfo(env, jinfo, (INetworkInfo**)&info)) {
            ALOGE("nativeCaptivePortalCheckCompleted() ToElNetworkInfo fail!");
        }
    }

    IIConnectivityManager* connMagr = (IIConnectivityManager*)jproxy;
    ECode ec = connMagr->CaptivePortalCheckCompleted(info, isCaptivePortal);
    if (FAILED(ec))
        ALOGE("nativeCaptivePortalCheckCompleted() ec: 0x%08x", ec);

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeCaptivePortalCheckCompleted()");
}

static void android_net_ElConnectivityManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_net_ElConnectivityManagerProxy_nativeDestroy()");

    IIConnectivityManager* obj = (IIConnectivityManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_net_ElConnectivityManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_net_ElConnectivityManagerProxy_nativeFinalize },
    { "nativeGetNetworkInfo",    "(JI)Landroid/net/NetworkInfo;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetNetworkInfo },
    { "nativeGetActiveNetworkInfo",    "(J)Landroid/net/NetworkInfo;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkInfo },
    { "nativeGetLinkPropertiesForType",    "(JI)Landroid/net/LinkProperties;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetLinkPropertiesForType },
    { "nativeGetActiveLinkProperties",    "(J)Landroid/net/LinkProperties;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetActiveLinkProperties },
    { "nativeIsNetworkSupported",    "(JI)Z",
            (void*) android_net_ElConnectivityManagerProxy_nativeIsNetworkSupported },
    { "nativeGetAllNetworkInfo",    "(J)[Landroid/net/NetworkInfo;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetAllNetworkInfo },
    { "nativeGetProxy",    "(J)Landroid/net/ProxyInfo;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetProxy },
    { "nativeGetActiveNetworkInfoForUid",    "(JI)Landroid/net/NetworkInfo;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkInfoForUid },
    { "nativeStartLegacyVpn",    "(JLcom/android/internal/net/VpnProfile;)V",
            (void*) android_net_ElConnectivityManagerProxy_nativeStartLegacyVpn },
    { "nativeReportInetCondition",    "(JII)V",
            (void*) android_net_ElConnectivityManagerProxy_nativeReportInetCondition },
    { "nativeGetAllNetworkState",    "(J)[Landroid/net/NetworkState;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetAllNetworkState },
    { "nativeGetActiveNetworkQuotaInfo",    "(J)Landroid/net/NetworkQuotaInfo;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetActiveNetworkQuotaInfo },
    { "nativeIsActiveNetworkMetered",    "(J)Z",
            (void*) android_net_ElConnectivityManagerProxy_nativeIsActiveNetworkMetered },
    { "nativeRequestRouteToHostAddress",    "(JI[B)Z",
            (void*) android_net_ElConnectivityManagerProxy_nativeRequestRouteToHostAddress },
    { "nativeSetPolicyDataEnable",    "(JIZ)V",
            (void*) android_net_ElConnectivityManagerProxy_nativeSetPolicyDataEnable },
    { "nativeTether",    "(JLjava/lang/String;)I",
            (void*) android_net_ElConnectivityManagerProxy_nativeTether },
    { "nativeUntether",    "(JLjava/lang/String;)I",
            (void*) android_net_ElConnectivityManagerProxy_nativeUntether },
    { "nativeGetLastTetherError",    "(JLjava/lang/String;)I",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetLastTetherError },
    { "nativeIsTetheringSupported",    "(J)Z",
            (void*) android_net_ElConnectivityManagerProxy_nativeIsTetheringSupported },
    { "nativeGetTetherableIfaces",    "(J)[Ljava/lang/String;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetTetherableIfaces },
    { "nativeGetTetheredIfaces",    "(J)[Ljava/lang/String;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetTetheredIfaces },
    { "nativeGetTetheringErroredIfaces",    "(J)[Ljava/lang/String;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetTetheringErroredIfaces },
    { "nativeGetTetheredDhcpRanges",    "(J)[Ljava/lang/String;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetTetheredDhcpRanges },
    { "nativeGetTetherableUsbRegexs",    "(J)[Ljava/lang/String;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetTetherableUsbRegexs },
    { "nativeGetTetherableWifiRegexs",    "(J)[Ljava/lang/String;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetTetherableWifiRegexs },
    { "nativeGetTetherableBluetoothRegexs",    "(J)[Ljava/lang/String;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetTetherableBluetoothRegexs },
    { "nativeSetUsbTethering",    "(JZ)I",
            (void*) android_net_ElConnectivityManagerProxy_nativeSetUsbTethering },
    { "nativeGetGlobalProxy",    "(J)Landroid/net/ProxyInfo;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetGlobalProxy },
    { "nativeSetGlobalProxy",    "(JLandroid/net/ProxyInfo;)V",
            (void*) android_net_ElConnectivityManagerProxy_nativeSetGlobalProxy },
    { "nativeSetDataDependency",    "(JIZ)V",
            (void*) android_net_ElConnectivityManagerProxy_nativeSetDataDependency },
    { "nativePrepareVpn",    "(JLjava/lang/String;Ljava/lang/String;)Z",
            (void*) android_net_ElConnectivityManagerProxy_nativePrepareVpn },
    { "nativeSetVpnPackageAuthorization",    "(JZ)V",
            (void*) android_net_ElConnectivityManagerProxy_nativeSetVpnPackageAuthorization },
    { "nativeEstablishVpn",    "(JLcom/android/internal/net/VpnConfig;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_net_ElConnectivityManagerProxy_nativeEstablishVpn },
    { "nativeGetLegacyVpnInfo",    "(J)Lcom/android/internal/net/LegacyVpnInfo;",
            (void*) android_net_ElConnectivityManagerProxy_nativeGetLegacyVpnInfo },
    { "nativeUpdateLockdownVpn",    "(J)Z",
            (void*) android_net_ElConnectivityManagerProxy_nativeUpdateLockdownVpn },
    { "nativeCaptivePortalCheckCompleted",    "(JLandroid/net/NetworkInfo;Z)V",
            (void*) android_net_ElConnectivityManagerProxy_nativeCaptivePortalCheckCompleted },
};

int register_android_net_ElConnectivityManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/net/ElConnectivityManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

