
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Os::IBinder;

static void android_view_ElApplicationTokenProxy_nativeFinalize(JNIEnv* env, jobject clazz, jint jtoken)
{
    // ALOGD("+ android_view_ElApplicationTokenProxy_nativeFinalize()");

    IBinder* token = (IBinder*)jtoken;
    token->Release();

    // ALOGD("- android_view_ElApplicationTokenProxy_nativeFinalize()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",    "(J)V",
            (void*) android_view_ElApplicationTokenProxy_nativeFinalize },
};

int register_android_view_ElApplicationTokenProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/view/ElApplicationTokenProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android
