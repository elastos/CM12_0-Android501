
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <_Elastos.Droid.JavaProxy.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::App::Backup::IIRestoreObserver;
using Elastos::Droid::App::Backup::IIRestoreSession;
using Elastos::Droid::JavaProxy::CIRestoreObserverNative;

static jint android_app_backup_ElIRestoreSessionProxy_nativeGetAvailableRestoreSets(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jobserver)
{
    ALOGD("+ android_app_backup_ElIRestoreSessionProxy_nativeGetAvailableRestoreSets()");

    AutoPtr<IIRestoreObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        if(NOERROR != CIRestoreObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIRestoreObserver**)&observer)) {
            ALOGE("nativeGetAvailableRestoreSets new CIRestoreObserverNative fail!\n");
        }
    }

    IIRestoreSession* irs = (IIRestoreSession*)jproxy;

    Int32 result = -1;
    ECode ec = irs->GetAvailableRestoreSets(observer, &result);
    ALOGE("nativeGetAvailableRestoreSets() ec = 0x%08x result: %d", ec, result);

    ALOGD("- android_app_backup_ElIRestoreSessionProxy_nativeGetAvailableRestoreSets()");
    return (jint)result;
}

static jint android_app_backup_ElIRestoreSessionProxy_nativeRestoreAll(JNIEnv* env, jobject clazz, jlong jproxy,
        jlong jtoken, jobject jobserver)
{
    ALOGD("+ android_app_backup_ElIRestoreSessionProxy_nativeRestoreAll()");

    AutoPtr<IIRestoreObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        if(NOERROR != CIRestoreObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIRestoreObserver**)&observer)) {
            ALOGE("nativeRestoreAll new CIRestoreObserverNative fail!\n");
        }
    }

    IIRestoreSession* irs = (IIRestoreSession*)jproxy;

    Int32 result = -1;
    ECode ec = irs->RestoreAll((Int64)jtoken, observer, &result);
    ALOGE("nativeRestoreAll() ec = 0x%08x result = %d", ec, result);

    ALOGD("- android_app_backup_ElIRestoreSessionProxy_nativeRestoreAll()");
    return (jint)result;
}

static jint android_app_backup_ElIRestoreSessionProxy_nativeRestoreSome(JNIEnv* env, jobject clazz, jlong jproxy,
        jlong jtoken, jobject jobserver, jobjectArray jpackages)
{
    ALOGD("+ android_app_backup_ElIRestoreSessionProxy_nativeRestoreSome()");

    AutoPtr<IIRestoreObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        if(NOERROR != CIRestoreObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIRestoreObserver**)&observer)) {
            ALOGE("nativeRestoreSome new CIRestoreObserverNative fail!\n");
        }
    }

    AutoPtr<ArrayOf<String> > packages;
    if (jpackages != NULL) {
        if (!ElUtil::ToElStringArray(env, jpackages, (ArrayOf<String>**)&packages)) {
            ALOGE("nativeRestoreSome() ToElStringArray fail!!");
        }
    }

    IIRestoreSession* irs = (IIRestoreSession*)jproxy;

    Int32 result = -1;
    ECode ec = irs->RestoreSome((Int64)jtoken, observer, *packages, &result);
    ALOGE("nativeRestoreSome() ec = 0x%08x result = %d", ec, result);

    ALOGD("- android_app_backup_ElIRestoreSessionProxy_nativeRestoreSome()");
    return (jint)result;
}

static jint android_app_backup_ElIRestoreSessionProxy_nativeRestorePackage(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpackageName, jobject jobserver)
{
    // ALOGD("+ android_app_backup_ElIRestoreSessionProxy_nativeRestorePackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIRestoreObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        if(NOERROR != CIRestoreObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIRestoreObserver**)&observer)) {
            ALOGE("nativeRestorePackage new CIRestoreObserverNative fail!\n");
        }
    }

    IIRestoreSession* irs = (IIRestoreSession*)jproxy;

    Int32 result = -1;
    irs->RestorePackage(packageName, observer, &result);

    // ALOGD("- android_app_backup_ElIRestoreSessionProxy_nativeRestorePackage()");
    return (jint)result;
}

static void android_app_backup_ElIRestoreSessionProxy_nativeEndRestoreSession(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_backup_ElIRestoreSessionProxy_nativeEndRestoreSession()");

    IIRestoreSession* irs = (IIRestoreSession*)jproxy;
    irs->EndRestoreSession();

    // ALOGD("- android_app_backup_ElIRestoreSessionProxy_nativeEndRestoreSession()");
}

static void android_app_backup_ElIRestoreSessionProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_app_backup_ElIRestoreSessionProxy_nativeDestroy()");

    IIRestoreSession* obj = (IIRestoreSession*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_app_backup_ElIRestoreSessionProxy_nativeDestroy()");
}

namespace android
{
static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_app_backup_ElIRestoreSessionProxy_nativeFinalize },
    { "nativeGetAvailableRestoreSets",    "(JLandroid/app/backup/IRestoreObserver;)I",
            (void*) android_app_backup_ElIRestoreSessionProxy_nativeGetAvailableRestoreSets },
    { "nativeRestoreAll",    "(JJLandroid/app/backup/IRestoreObserver;)I",
            (void*) android_app_backup_ElIRestoreSessionProxy_nativeRestoreAll },
    { "nativeRestoreSome",    "(JJLandroid/app/backup/IRestoreObserver;[Ljava/lang/String;)I",
            (void*) android_app_backup_ElIRestoreSessionProxy_nativeRestoreSome },
    { "nativeRestorePackage",    "(JLjava/lang/String;Landroid/app/backup/IRestoreObserver;)I",
            (void*) android_app_backup_ElIRestoreSessionProxy_nativeRestorePackage },
    { "nativeEndRestoreSession",    "(J)V",
            (void*) android_app_backup_ElIRestoreSessionProxy_nativeEndRestoreSession },
};

int register_android_app_backup_ElIRestoreSessionProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/app/backup/ElIRestoreSessionProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

