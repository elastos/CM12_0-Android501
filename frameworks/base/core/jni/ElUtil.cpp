#define LOG_TAG "ElUtil"
#include "ElUtil.h"
#include <JNIHelp.h>
#include <utils/Log.h>
#include <elastos/core/StringUtils.h>
#include <elastos/utility/etl/HashMap.h>
#include <elastos/droid/text/TextUtils.h>
#include <SkRegion.h>
#include <input/Input.h>
#include <binder/Parcel.h>

#include <Elastos.Droid.Accounts.h>
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.AppWidget.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Database.h>
#include <Elastos.Droid.Graphics.h>
#include <Elastos.Droid.Hardware.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.Droid.Location.h>
#include <Elastos.Droid.Media.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.View.h>
#include <Elastos.Droid.Service.h>
#include <Elastos.Droid.Text.h>
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Widget.h>
#include <Elastos.Droid.Wifi.h>
#include <Elastos.Droid.Utility.h>
#include <Elastos.CoreLibrary.Core.h>
#include <Elastos.CoreLibrary.Extensions.h>
#include <Elastos.CoreLibrary.IO.h>
#include <Elastos.CoreLibrary.Net.h>
#include <Elastos.CoreLibrary.Security.h>
#include <Elastos.CoreLibrary.Utility.h>
#include <Elastos.CoreLibrary.Utility.Zip.h>

#define BUFFER_SIZE 1024

using Elastos::Core::IByte;
using Elastos::Core::IInteger16;
using Elastos::Core::IInteger32;
using Elastos::Core::IInteger64;
using Elastos::Core::IFloat;
using Elastos::Core::IDouble;
using Elastos::Core::IBoolean;
using Elastos::Core::IArrayOf;
using Elastos::Core::ICharSequence;
using Elastos::Core::CByte;
using Elastos::Core::CInteger16;
using Elastos::Core::CInteger32;
using Elastos::Core::CInteger64;
using Elastos::Core::CFloat;
using Elastos::Core::CDouble;
using Elastos::Core::CBoolean;
using Elastos::Core::CArrayOf;
using Elastos::Core::CString;
using Elastos::Core::EIID_IByte;
using Elastos::Core::StringUtils;

using Elastos::Droid::Accounts::CAccount;
using Elastos::Droid::App::IRemoteInput;
using Elastos::Droid::App::CAlarmClockInfo;
using Elastos::Droid::App::CActivityManagerTaskDescription;
using Elastos::Droid::App::CApplicationErrorReportCrashInfo;
using Elastos::Droid::App::CContentProviderHolder;
using Elastos::Droid::App::INotificationAction;
using Elastos::Droid::App::INotificationActionBuilder;
using Elastos::Droid::App::CNotificationAction;
using Elastos::Droid::App::CNotificationActionBuilder;
using Elastos::Droid::App::CNotification;
using Elastos::Droid::App::CPendingIntent;
using Elastos::Droid::App::CProfilerInfo;
using Elastos::Droid::AppWidget::CAppWidgetProviderInfo;
using Elastos::Droid::Content::CClipData;
using Elastos::Droid::Content::CClipDataItem;
using Elastos::Droid::Content::CClipDescription;
using Elastos::Droid::Content::CComponentName;
using Elastos::Droid::Content::CContentProviderOperation;
using Elastos::Droid::Content::CContentProviderOperationBuilder;
using Elastos::Droid::Content::CContentValues;
using Elastos::Droid::Content::CIntent;
using Elastos::Droid::Content::CIntentFilter;
using Elastos::Droid::Content::CIntentSender;
using Elastos::Droid::Content::CIntentShortcutIconResource;
using Elastos::Droid::Content::CIntentFilterAuthorityEntry;
using Elastos::Droid::Content::ECLSID_CUriPermission;
using Elastos::Droid::Content::IClipData;
using Elastos::Droid::Content::IContentProviderOperationBuilder;
using Elastos::Droid::Content::IIContentProvider;
using Elastos::Droid::Content::IIIntentSender;
using Elastos::Droid::Content::IIntentFilter;
using Elastos::Droid::Content::Pm::EIID_IApplicationInfo;
using Elastos::Droid::Content::Pm::EIID_IPackageInfo;
using Elastos::Droid::Content::Pm::ECLSID_CApplicationInfo;
using Elastos::Droid::Content::Pm::ECLSID_CPackageInfo;
using Elastos::Droid::Content::Pm::IBaseThemeInfo;
using Elastos::Droid::Content::Pm::IConfigurationInfo;
using Elastos::Droid::Content::Pm::IInstrumentationInfo;
using Elastos::Droid::Content::Pm::IProviderInfo;
using Elastos::Droid::Content::Pm::IPermissionInfo;
using Elastos::Droid::Content::Pm::ISignature;
using Elastos::Droid::Content::Pm::CApplicationInfo;
using Elastos::Droid::Content::Pm::CActivityInfo;
using Elastos::Droid::Content::Pm::CPackageCleanItem;
using Elastos::Droid::Content::Pm::CPathPermission;
using Elastos::Droid::Content::Pm::CPermissionInfo;
using Elastos::Droid::Content::Pm::CProviderInfo;
using Elastos::Droid::Content::Pm::CResolveInfo;
using Elastos::Droid::Content::Pm::CServiceInfo;
using Elastos::Droid::Content::Pm::CVerificationParams;
using Elastos::Droid::Content::Pm::CManifestDigest;
using Elastos::Droid::Content::Pm::CContainerEncryptionParams;
using Elastos::Droid::Content::Res::CCompatibilityInfo;
using Elastos::Droid::Content::Res::CConfiguration;
using Elastos::Droid::Content::Res::IThemeConfig;
using Elastos::Droid::Database::CCharArrayBuffer;
using Elastos::Droid::Graphics::CBitmap;
using Elastos::Droid::Graphics::CBitmapFactory;
using Elastos::Droid::Graphics::CPoint;
using Elastos::Droid::Graphics::CRect;
using Elastos::Droid::Graphics::CRegion;
using Elastos::Droid::Graphics::IBitmapFactory;
using Elastos::Droid::Hardware::Usb::CUsbAccessory;
using Elastos::Droid::Hardware::Usb::CUsbDevice;
using Elastos::Droid::Hardware::Usb::CUsbConfiguration;
using Elastos::Droid::Hardware::Usb::CUsbInterface;
using Elastos::Droid::Hardware::Usb::CUsbEndpoint;
using Elastos::Droid::JavaProxy::CContentProviderNative;
using Elastos::Droid::JavaProxy::CBinderNative;
using Elastos::Droid::JavaProxy::CIIntentSenderNative;
using Elastos::Droid::JavaProxy::CIParcelableNative;
using Elastos::Droid::JavaProxy::CISerializableNative;
using Elastos::Droid::JavaProxy::CIServiceConnectionNative;
using Elastos::Droid::JavaProxy::CMessengerNative;
using Elastos::Droid::Location::CAddress;
using Elastos::Droid::Location::CCriteria;
using Elastos::Droid::Location::CGeofenceHelper;
using Elastos::Droid::Location::IGeofenceHelper;
using Elastos::Droid::Location::CGeocoderParams;
using Elastos::Droid::Location::CLocation;
using Elastos::Droid::Location::CLocationRequest;
using Elastos::Droid::Media::IAudioAttributes;
using Elastos::Droid::Media::IAudioAttributesBuilder;
using Elastos::Droid::Media::CAudioAttributesBuilder;
using Elastos::Droid::Media::IMediaRecorderAudioSource;
using Elastos::Droid::Media::CMediaMetadata;
using Elastos::Droid::Media::Session::CPlaybackState;
using Elastos::Droid::Media::Session::CPlaybackStateCustomAction;
// using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Os::IParcelFileDescriptorAutoCloseInputStream;
using Elastos::Droid::Os::IMessageHelper;
using Elastos::Droid::Os::IIMessenger;
using Elastos::Droid::Os::CBundle;
// using Elastos::Droid::Os::CDropBoxManagerEntry;
using Elastos::Droid::Os::CMessageHelper;
using Elastos::Droid::Os::CMessenger;
using Elastos::Droid::Os::CPatternMatcher;
using Elastos::Droid::Os::CParcelFileDescriptor;
using Elastos::Droid::Os::CUserHandle;
using Elastos::Droid::Os::CPersistableBundle;
using Elastos::Droid::Os::CStrictModeViolationInfo;
using Elastos::Droid::Os::CWorkSource;
using Elastos::Droid::Os::Storage::CStorageVolume;
using Elastos::Droid::Os::CServiceManager;
using Elastos::Droid::Os::IIRemoteCallback;
using Elastos::Droid::Os::IServiceManager;
// using Elastos::Droid::Privacy::CPrivacySettings;
using Elastos::Droid::Net::CHierarchicalUri;
using Elastos::Droid::Net::CLinkAddress;
using Elastos::Droid::Net::CLinkProperties;
using Elastos::Droid::Net::CIpConfiguration;
using Elastos::Droid::Net::CStaticIpConfiguration;
using Elastos::Droid::Net::CNetworkInfo;
using Elastos::Droid::Net::CNetworkTemplate;
using Elastos::Droid::Net::COpaqueUri;
using Elastos::Droid::Net::CProxyInfo;
using Elastos::Droid::Net::CRouteInfo;
using Elastos::Droid::Net::CStringUri;
using Elastos::Droid::Net::CIpPrefix;
using Elastos::Droid::Net::ECLSID_CStringUri;
using Elastos::Droid::Net::ECLSID_CHierarchicalUri;
using Elastos::Droid::Net::ECLSID_COpaqueUri;
using Elastos::Droid::Net::NetworkInfoState;
using Elastos::Droid::Net::NetworkInfoDetailedState;
using Elastos::Droid::Net::INetworkStatsEntry;
using Elastos::Droid::Net::INetworkStatsHistoryEntry;
using Elastos::Droid::Wifi::SupplicantState;
using Elastos::Droid::Wifi::CWifiConfiguration;
using Elastos::Droid::Wifi::CWifiEnterpriseConfig;
using Elastos::Droid::Wifi::CWifiInfo;
using Elastos::Droid::Wifi::CWifiSsidHelper;
using Elastos::Droid::Wifi::CWpsInfo;
using Elastos::Droid::Wifi::CSupplicantState;
using Elastos::Droid::Wifi::IScanResultInformationElement;
using Elastos::Droid::Wifi::IWifiSsidHelper;
using Elastos::Droid::Wifi::P2p::CWifiP2pConfig;
using Elastos::Droid::Wifi::P2p::CWifiP2pDevice;
using Elastos::Droid::Wifi::P2p::CWifiP2pInfo;
using Elastos::Droid::Wifi::P2p::CWifiP2pWfdInfo;
using Elastos::Droid::Service::Notification::CCondition;
using Elastos::Droid::Text::Style::CSuggestionSpan;
using Elastos::Droid::Text::TextUtils;
using Elastos::Droid::Text::ISpanned;
using Elastos::Droid::Text::IParcelableSpan;
using Elastos::Droid::Text::Style::ICharacterStyle;
using Elastos::Droid::Utility::CSparseArray;
using Elastos::Droid::View::CKeyEvent;
using Elastos::Droid::View::CMotionEvent;
using Elastos::Droid::View::CWindowManagerLayoutParams;
using Elastos::Droid::View::CMotionEventHelper;
using Elastos::Droid::View::IMotionEventHelper;
using Elastos::Droid::View::IMotionRange;
using Elastos::Droid::View::ISurface;
using Elastos::Droid::View::InputMethod::CCompletionInfo;
using Elastos::Droid::View::InputMethod::CCorrectionInfo;
using Elastos::Droid::View::InputMethod::CEditorInfo;
using Elastos::Droid::View::InputMethod::CExtractedText;
using Elastos::Droid::View::InputMethod::CExtractedTextRequest;
using Elastos::Droid::View::InputMethod::CInputMethodInfo;
using Elastos::Droid::View::InputMethod::CInputMethodSubtype;
using Elastos::Droid::View::InputMethod::CInputMethodSubtypeArray;
using Elastos::Droid::View::InputMethod::IInputMethodSubtypeArray;
using Elastos::Droid::View::IIWindow;
using Elastos::Droid::Widget::CRemoteViews;
using Elastos::Droid::Widget::CRemoteViewsBitmapCache;
using Elastos::Droid::Widget::IBitmapCache;
using Elastos::Droid::Internal::Net::CVpnConfig;
using Elastos::Droid::Internal::Net::CVpnProfile;
using Elastos::Droid::Internal::Location::CProviderProperties;
using Elastos::Droid::Internal::View::IIInputMethodSession;
using Elastos::Droid::Utility::CParcelableList;

using Elastos::IO::CFile;
using Elastos::IO::CFileDescriptor;
using Elastos::IO::IByteArrayInputStream;
using Elastos::IO::IByteArrayOutputStream;
using Elastos::IO::CByteArrayOutputStream;
using Elastos::IO::IFileInputStream;
using Elastos::IO::IInputStream;
using Elastos::IO::IOutputStream;
using Elastos::Net::IInetAddressHelper;
using Elastos::Net::CInetAddressHelper;
using Elastos::Security::Spec::IAlgorithmParameterSpec;
using Elastos::Utility::IArrayList;
using Elastos::Utility::CArrayList;
using Elastos::Utility::CBitSetHelper;
using Elastos::Utility::IBitSetHelper;
using Elastos::Utility::ISet;
using Elastos::Utility::CLocale;
using Elastos::Utility::ILocaleHelper;
using Elastos::Utility::CLocaleHelper;
using Elastos::Utility::CHashMap;
using Elastos::Utility::Etl::HashMap;
using Elastos::Utility::Zip::IGZIPInputStream;

using Elastosx::Crypto::Spec::CIvParameterSpec;

extern HashMap<Int32, AutoPtr<IIWindow> > sWindowMap;

#ifndef FAIL_RETURN
#define FAIL_RETURN(expr) \
    do { \
        ECode fail_ec_ = expr; \
        if (FAILED(fail_ec_)) return fail_ec_; \
    } while(0);
#endif

namespace android {

jint ElUtil::GetJavaIntegerField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, jint defaultInt, const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavaIntegerField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return defaultInt;
    }
    jfieldID integerField = env->GetFieldID(klass, fieldName, "Ljava/lang/Integer;");
    CheckErrorAndLog(env, "%s: Fail get integer fieldid: %s : %d!\n", tag, fieldName, __LINE__);
    jobject jIntegerValue = env->GetObjectField(jobj ,integerField);

    if (jIntegerValue == NULL) {
        ALOGW("ElUtil::GetJavaIntegerField integer field is null, name:%s", fieldName);
        return defaultInt;
    }

    CheckErrorAndLog(env, "%s Fail get integer field : %s : %d!\n", tag, fieldName, __LINE__);

    jclass jintegerCls = env->FindClass("java/lang/Integer");
    CheckErrorAndLog(env, "GetJavaIntegerField(): FindClass: Integer : %d!\n", __LINE__);
    jmethodID toIntMid = env->GetMethodID(jintegerCls, "intValue", "()I");
    CheckErrorAndLog(env, "GetJavaIntegerField(): FindMethod: toValue: %d!\n", __LINE__);
    jint value = env->CallIntMethod(jIntegerValue, toIntMid);
    CheckErrorAndLog(env, "GetJavaIntegerField: call method toValue: %d!\n", __LINE__);

    env->DeleteLocalRef(jIntegerValue);
    env->DeleteLocalRef(jintegerCls);
    return value;
}

jint ElUtil::GetJavaIntField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavaIntField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return 0;
    }

    jfieldID intField = env->GetFieldID(klass, fieldName, "I");
    CheckErrorAndLog(env, "%s: Fail get int field id:%s  : %d!\n", tag, fieldName, __LINE__);
    jint intValue = env->GetIntField(jobj, intField);
    CheckErrorAndLog(env, "%s: Fail get int field: %s : %d!\n", tag, fieldName, __LINE__);
    return intValue;
}

jlong ElUtil::GetJavalongField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, const char* tag)
{
    if (env == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavalongField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return 0;
    }

    jfieldID field = env->GetFieldID(klass, fieldName, "J");
    CheckErrorAndLog(env, "%s: Fail get long field id:%s  : %d!\n", tag, fieldName, __LINE__);
    jlong value = env->GetLongField(jobj, field);
    CheckErrorAndLog(env, "%s: Fail get int field: %s : %d!\n", tag, fieldName, __LINE__);
    return value;
}

/**get byte field of java object*/
jbyte ElUtil::GetJavabyteField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavabyteField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return 0;
    }

    jfieldID byteField = env->GetFieldID(klass, fieldName, "B");
    CheckErrorAndLog(env, "%s: Fail get byte field id:%s  : %d!\n", tag, fieldName, __LINE__);
    jbyte byteValue = env->GetByteField(jobj, byteField);
    CheckErrorAndLog(env, "%s: Fail get byte field: %s : %d!\n", tag, fieldName, __LINE__);
    return byteValue;
}

jshort ElUtil::GetJavaShortField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavabyteField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return 0;
    }

    jfieldID fieldID = env->GetFieldID(klass, fieldName, "S");
    CheckErrorAndLog(env, "%s: Fail get byte field id:%s  : %d!\n", tag, fieldName, __LINE__);
    jshort jvalue = env->GetShortField(jobj, fieldID);
    CheckErrorAndLog(env, "%s: Fail get byte field: %s : %d!\n", tag, fieldName, __LINE__);
    return jvalue;
}

jchar ElUtil::GetJavaCharField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavabyteField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return 0;
    }

    jfieldID fieldID = env->GetFieldID(klass, fieldName, "C");
    CheckErrorAndLog(env, "%s: Fail get byte field id:%s  : %d!\n", tag, fieldName, __LINE__);
    jchar jvalue = env->GetCharField(jobj, fieldID);
    CheckErrorAndLog(env, "%s: Fail get byte field: %s : %d!\n", tag, fieldName, __LINE__);
    return jvalue;
}

jboolean ElUtil::GetJavaBoolField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavaBoolField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return JNI_FALSE;
    }

    jfieldID field = env->GetFieldID(klass, fieldName, "Z");
    CheckErrorAndLog(env, "%s: Fail get boolean field id:%s  : %d!\n", tag, fieldName, __LINE__);
    jboolean value = env->GetBooleanField(jobj, field);
    CheckErrorAndLog(env, "%s: Fail get boolean field: %s : %d!\n", tag, fieldName, __LINE__);
    return value;
}

jfloat ElUtil::GetJavafloatField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavafloatField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return 0.0f;
    }

    jfieldID field = env->GetFieldID(klass, fieldName, "F");
    CheckErrorAndLog(env, "%s: Fail get float field id:%s  : %d!\n", tag, fieldName, __LINE__);
    jfloat value = env->GetFloatField(jobj, field);
    CheckErrorAndLog(env, "%s: Fail get float field: %s : %d!\n", tag, fieldName, __LINE__);
    return value;
}

jdouble ElUtil::GetJavadoubleField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavadoubleField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return 0.0;
    }

    jfieldID field = env->GetFieldID(klass, fieldName, "D");
    CheckErrorAndLog(env, "%s: Fail get double field id:%s  : %d!\n", tag, fieldName, __LINE__);
    jdouble value = env->GetDoubleField(jobj, field);
    CheckErrorAndLog(env, "%s: Fail get double field: %s : %d!\n", tag, fieldName, __LINE__);
    return value;
}

String ElUtil::GetJavaStringField(JNIEnv* env, jclass klass, jobject jobj, const char* fieldName, const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: GetJavaStringField() invalid param while get field:%s  : %d!\n", tag, fieldName, __LINE__);
        return String(NULL);
    }

    jfieldID stringField = env->GetFieldID(klass, fieldName, "Ljava/lang/String;");
    CheckErrorAndLog(env, "%s: Fail get string field id: %s : %d!\n", tag, fieldName, __LINE__);
    jstring stringValue = (jstring)env->GetObjectField(jobj, stringField);
    CheckErrorAndLog(env, "%s: Fail get string field: %s : %d!\n", tag, fieldName, __LINE__);
    String s = ToElString(env, stringValue);
    env->DeleteLocalRef(stringValue);
    return s;
}

bool ElUtil::SetJavaIntegerField(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass klass,
    /* [in] */ jobject jobj,
    /* [in] */ Int32 intvalue,
    /* [in] */ const char* fieldName,
    /* [in] */ const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: SetJavaIntegerField() invalid param while set field:%s  : %d!\n", tag, fieldName, __LINE__);
        return false;
    }

    jclass jintegerCls = env->FindClass("java/lang/Integer");
    CheckErrorAndLog(env, "SetJavaIntegerField(): failed FindClass: Integer : %d!\n", __LINE__);
    jmethodID mid = env->GetStaticMethodID(jintegerCls, "valueOf", "(I)Ljava/lang/Integer;");
    CheckErrorAndLog(env, "SetJavaIntegerField(): failed get statis methond valueof(): %d!\n", __LINE__);
    jobject jinteger = env->CallStaticObjectMethod(jintegerCls, mid, intvalue);
    CheckErrorAndLog(env, "SetJavaIntegerField(): failed call static method valueOf(): %d!\n", __LINE__);

    jfieldID integerField = env->GetFieldID(klass, fieldName, "Ljava/lang/Integer;");
    CheckErrorAndLog(env, "%s: Fail get integer fieldid: %s : %d!\n", tag, fieldName, __LINE__);
    env->SetObjectField(jobj, integerField, jinteger);
    CheckErrorAndLog(env, "%s Fail get integer field : %s : %d!\n", tag, fieldName, __LINE__);
    env->DeleteLocalRef(jintegerCls);
    env->DeleteLocalRef(jinteger);
    return true;
}

bool ElUtil::SetJavalongField(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass klass,
    /* [in] */ jobject jobj,
    /* [in] */ Int64 longvalue,
    /* [in] */ const char* fieldName,
    /* [in] */ const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: SetJavalongField() invalid param while set field:%s  : %d!\n", tag, fieldName, __LINE__);
        return false;
    }

    jfieldID intField = env->GetFieldID(klass, fieldName, "J");
    CheckErrorAndLog(env, "%s: Fail get long field id:%s  : %d!\n", tag, fieldName, __LINE__);
    env->SetLongField(jobj, intField, longvalue);
    CheckErrorAndLog(env, "%s: Fail Set long field: %s : %d!\n", tag, fieldName, __LINE__);
    return true;
}

bool ElUtil::SetJavaIntField(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass klass,
    /* [in] */ jobject jobj,
    /* [in] */ Int32 intvalue,
    /* [in] */ const char* fieldName,
    /* [in] */ const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: SetJavaIntField() invalid param while set field:%s  : %d!\n", tag, fieldName, __LINE__);
        return false;
    }
    jfieldID intField = env->GetFieldID(klass, fieldName, "I");
    CheckErrorAndLog(env, "%s: Fail get int field id:%s  : %d!\n", tag, fieldName, __LINE__);
    env->SetIntField(jobj, intField, intvalue);
    CheckErrorAndLog(env, "%s: Fail Set int field: %s : %d!\n", tag, fieldName, __LINE__);
    return true;
}

bool ElUtil::SetJavabyteField(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass klass,
    /* [in] */ jobject jobj,
    /* [in] */ Byte bytevalue,
    /* [in] */ const char* fieldName,
    /* [in] */ const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: SetJavabyteField() invalid param while set field:%s  : %d!\n", tag, fieldName, __LINE__);
        return false;
    }
    jfieldID byteField = env->GetFieldID(klass, fieldName, "B");
    CheckErrorAndLog(env, "%s: Fail get byte field id:%s  : %d!\n", tag, fieldName, __LINE__);
    env->SetByteField(jobj, byteField, bytevalue);
    CheckErrorAndLog(env, "%s: Fail set byte field: %s : %d!\n", tag, fieldName, __LINE__);
    return true;
}

bool ElUtil::SetJavaBoolField(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass klass,
    /* [in] */ jobject jobj,
    /* [in] */ Boolean boolValue,
    /* [in] */ const char* fieldName,
    /* [in] */ const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: SetJavaBoolField() invalid param while set field:%s  : %d!\n", tag, fieldName, __LINE__);
        return false;
    }
    jfieldID field = env->GetFieldID(klass, fieldName, "Z");
    CheckErrorAndLog(env, "%s: Fail get boolean field id:%s  : %d!\n", tag, fieldName, __LINE__);
    env->SetBooleanField(jobj, field, boolValue);
    CheckErrorAndLog(env, "%s: Fail set boolean field: %s : %d!\n", tag, fieldName, __LINE__);
    return true;
}

bool ElUtil::SetJavafloatField(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass klass,
    /* [in] */ jobject jobj,
    /* [in] */ Float floatValue,
    /* [in] */ const char* fieldName,
    /* [in] */ const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: SetJavafloatField() invalid param while set field:%s  : %d!\n", tag, fieldName, __LINE__);
        return false;
    }

    jfieldID field = env->GetFieldID(klass, fieldName, "F");
    CheckErrorAndLog(env, "%s: Fail get float field id:%s  : %d!\n", tag, fieldName, __LINE__);
    env->SetFloatField(jobj, field, floatValue);
    CheckErrorAndLog(env, "%s: Fail set float field: %s : %d!\n", tag, fieldName, __LINE__);
    return true;
}

bool ElUtil::SetJavadoubleField(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass klass,
    /* [in] */ jobject jobj,
    /* [in] */ Double doubleValue,
    /* [in] */ const char* fieldName,
    /* [in] */ const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: SetJavadoubleField() invalid param while set field:%s  : %d!\n", tag, fieldName, __LINE__);
        return false;
    }

    jfieldID field = env->GetFieldID(klass, fieldName, "D");
    CheckErrorAndLog(env, "%s: Fail get double field id:%s  : %d!\n", tag, fieldName, __LINE__);
    env->SetDoubleField(jobj, field, doubleValue);
    CheckErrorAndLog(env, "%s: Fail set double field: %s : %d!\n", tag, fieldName, __LINE__);
    return true;
}

bool ElUtil::SetJavaStringField(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass klass,
    /* [in] */ jobject jobj,
    /* [in] */ String strValue,
    /* [in] */ const char* fieldName,
    /* [in] */ const char* tag)
{
    if (env == NULL || klass == NULL || jobj == NULL || fieldName == NULL) {
        ALOGE("%s: SetJavaStringField() invalid param while set field:%s  : %d!\n", tag, fieldName, __LINE__);
        return false;
    }
    jstring jvalue = GetJavaString(env, strValue);
    jfieldID stringField = env->GetFieldID(klass, fieldName, "Ljava/lang/String;");
    CheckErrorAndLog(env, "%s: Fail get string field id: %s : %d!\n", tag, fieldName, __LINE__);
    env->SetObjectField(jobj, stringField, jvalue);
    CheckErrorAndLog(env, "%s: Fail Set string field: %s : %d!\n", tag, fieldName, __LINE__);
    env->DeleteLocalRef(jvalue);
    return true;
}

void ElUtil::ThrowExceptionByName(
    /* [in] */ JNIEnv* env,
    /* [in] */ const char* className,
    /* [in] */ const char* tag)
{
    env->ExceptionDescribe();
    env->ExceptionClear();

    jclass c = env->FindClass(className);
     if (c != NULL) {
        env->ThrowNew(c, tag);
        env->DeleteLocalRef(c);
     }
}

String ElUtil::ToElString(
    /* [in] */ JNIEnv* env,
    /* [in] */ jstring jstr)
{
    if (env == NULL || jstr == NULL) {
        return String(NULL);
    }

    const char* str = NULL;
    str = env->GetStringUTFChars(jstr, NULL);
    String elStr = String(str);
    env->ReleaseStringUTFChars(jstr, str);

    return elStr;
}

jstring ElUtil::GetJavaString(
    /* [in] */ JNIEnv* env,
    /* [in] */ const String& str)
{
    if (env == NULL) {
        ALOGD("GetJavaString() Invalid arguments!");
        return NULL;
    }

    jstring jstr = NULL;

    if (!str.IsNull()) {
        jstr = env->NewStringUTF(str.string());
    }

    return jstr;
}


void ElUtil::CheckErrorAndLog(
    /* [in] */ JNIEnv* env,
    /* [in] */ const char* errlog,
    /* [in] */ int line)
{
    if (env->ExceptionCheck() != 0) {
        ALOGE(errlog, line);
        env->ExceptionDescribe();
        env->ExceptionClear();
    }
}

void ElUtil::CheckErrorAndLog(
    /* [in] */ JNIEnv* env,
    /* [in] */ const char* errlog,
    /* [in] */ const char* paramname,
    /* [in] */ const char* tag,
    /* [in] */ Int32 line)
{
    if (env->ExceptionCheck() != 0) {
        ALOGE(errlog, paramname, tag, line);
        env->ExceptionDescribe();
        env->ExceptionClear();
    }
}

String ElUtil::GetClassName(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject obj)
{
    jclass cls, clsobj;
    jmethodID getName;
    jstring jstr;
    String className;
    char *temp;
    jboolean isCopy;

    /* Get the class of the object */
    cls = env->GetObjectClass(obj);

    /* take that Class object and get it's Class object */
    clsobj = env->GetObjectClass(cls);

    /* get the Class.getName() methodid */
    getName = env->GetMethodID(clsobj, "getName", "()Ljava/lang/String;");

    /* Get the jstring name of the object, from the Class class. */
    jstr = (jstring)env->CallObjectMethod(cls, getName);

    /* Convert the name into a char* */
    temp = (char *)env->GetStringUTFChars(jstr, &isCopy);
    className = String(temp);
    env->ReleaseStringUTFChars(jstr, temp);

    env->DeleteLocalRef(cls);
    env->DeleteLocalRef(clsobj);
    env->DeleteLocalRef(jstr);

    return className;
}

String ElUtil::GetJavaToString(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jobj)
{
    jclass c = env->FindClass("java/lang/Object");
    CheckErrorAndLog(env, "FindClass: Object : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(c, "toString", "()Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: toString : %d!\n", __LINE__);

    jstring jstr = (jstring)env->CallObjectMethod(jobj, m);
    CheckErrorAndLog(env, "CallObjectMethod: toString : %d!\n", __LINE__);

    String str = ElUtil::ToElString(env, jstr);

    env->DeleteLocalRef(c);
    env->DeleteLocalRef(jstr);
    return str;
}

jobjectArray ElUtil::GetJavaStringArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ ArrayOf<String>* strArray)
{
    if (env == NULL || strArray == NULL) {
        ALOGD("GetJavaStringArray() Invalid arguments!");
        return NULL;
    }

    Int32 count = strArray->GetLength();

    jclass stringKlass = env->FindClass("java/lang/String");
    CheckErrorAndLog(env, "FindClass: String : %d!\n", __LINE__);

    jobjectArray jstrArray = env->NewObjectArray((jsize)count, stringKlass, 0);
    CheckErrorAndLog(env, "NewObjectArray: String : %d!\n", __LINE__);
    env->DeleteLocalRef(stringKlass);

    for(Int32 i = 0; i < count; i++ ) {
        jstring str = GetJavaString(env, (*strArray)[i]);
        if (str != NULL) {
            env->SetObjectArrayElement(jstrArray, i, str);
            env->DeleteLocalRef(str);
        }
    }

    return jstrArray;
}

bool ElUtil::ToElStringArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobjectArray jarr,
    /* [out] */ ArrayOf<String>** strArray)
{
    if (env == NULL || jarr == NULL || strArray == NULL) {
        ALOGE("ToElStringArray() Invalid arguments!");
        return false;
    }

    int size = env->GetArrayLength(jarr);
    // ALOGD("ToElStringArray(), input array size:%d", size);
    CheckErrorAndLog(env, "ToElStringArray(); GetArrayLength failed: %d!\n", __LINE__);

    AutoPtr<ArrayOf<String> > elStrArr = ArrayOf<String>::Alloc(size);

    for(int i = 0; i < size; i++) {
        jstring strTemp = (jstring)env->GetObjectArrayElement(jarr, i);
        CheckErrorAndLog(env, "ToElStringArray(); GetObjectArrayelement failed : %d!\n", __LINE__);
        elStrArr->Set(i, ElUtil::ToElString(env, strTemp));
        env->DeleteLocalRef(strTemp);
    }

    *strArray = elStrArr.Get();
    (*strArray)->AddRef();
    return true;
}

jintArray ElUtil::GetJavaIntArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ ArrayOf<Int32>* intArray)
{
    if (env == NULL || intArray == NULL) {
        ALOGD("GetJavaIntArray() Invalid arguments!");
        return NULL;
    }

    Int32 count = intArray->GetLength();

    jintArray jintArray = env->NewIntArray((jsize)count);
    CheckErrorAndLog(env, "NewIntArray: %d!\n", __LINE__);

    jint* intArrayInd = (jint*)intArray->GetPayload();

    env->SetIntArrayRegion(jintArray,0, count, intArrayInd);
    CheckErrorAndLog(env, "SetIntArrayRegion: %d!\n", __LINE__);

    return jintArray;
}

jlongArray ElUtil::GetJavaLongArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ ArrayOf<Int64>* lArray)
{
    if (env == NULL || lArray == NULL) {
        ALOGD("GetJavaLongArray() Invalid arguments!");
        return NULL;
    }

    Int32 count = lArray->GetLength();

    jlongArray jlArray = env->NewLongArray((jsize)count);
    CheckErrorAndLog(env, "NewLongArray: %d!", __LINE__);

    jlong* lArrayInd = (jlong*)lArray->GetPayload();

    env->SetLongArrayRegion(jlArray, 0, count, lArrayInd);
    CheckErrorAndLog(env, "SetLongArrayRegion: %d!", __LINE__);

    return jlArray;
}

jfloatArray ElUtil::GetJavaFloatArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ ArrayOf<Float>* fArray)
{
    if (env == NULL || fArray == NULL) {
        ALOGD("GetJavaFloatArray() Invalid arguments!");
        return NULL;
    }

    Int32 count = fArray->GetLength();

    jfloatArray jfArray = env->NewFloatArray((jsize)count);
    CheckErrorAndLog(env, "NewLongArray: %d!", __LINE__);

    jfloat* fArrayInd = (jfloat*)fArray->GetPayload();

    env->SetFloatArrayRegion(jfArray, 0, count, fArrayInd);
    CheckErrorAndLog(env, "SetFloatArrayRegion: %d!", __LINE__);

    return jfArray;
}

bool ElUtil::ToElIntArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ jintArray jarr,
    /* [out] */ ArrayOf<Int32>** intArray)
{
    if (intArray == NULL) {
        return FALSE;
    }

    *intArray = NULL;

    if (env == NULL || jarr == NULL) {
        ALOGD("ToElIntArray() Invalid arguments!");
        return FALSE;
    }

    jint size = env->GetArrayLength(jarr);
    CheckErrorAndLog(env, "GetArrayLength:(): %d!\n", __LINE__);

    jint* jpayload = (jint*)malloc(size * sizeof(Int32));

    env->GetIntArrayRegion(jarr, 0, size, jpayload);
    CheckErrorAndLog(env, "GetIntArrayRegion:(): %d!\n", __LINE__);

    (*intArray) = ArrayOf<Int32>::Alloc(size);
    (*intArray)->Copy((Int32*)jpayload, size);
    (*intArray)->AddRef();

    free(jpayload);
    return TRUE;
}

bool ElUtil::ToElLongArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ jlongArray jarr,
    /* [out] */ ArrayOf<Int64>** longArray)
{
    if (longArray == NULL) {
        return FALSE;
    }

    *longArray = NULL;

    if (env == NULL || jarr == NULL) {
        ALOGD("ToElIntArray() Invalid arguments!");
        return FALSE;
    }

    jint size = env->GetArrayLength(jarr);
    CheckErrorAndLog(env, "GetArrayLength:(): %d!\n", __LINE__);

    jlong* jpayload = (jlong*)malloc(size * sizeof(Int64));

    env->GetLongArrayRegion(jarr, 0, size, jpayload);
    CheckErrorAndLog(env, "GetLongArrayRegion:(): %d!\n", __LINE__);

    (*longArray) = ArrayOf<Int64>::Alloc(size);
    (*longArray)->Copy((Int64*)jpayload, size);
    (*longArray)->AddRef();

    free(jpayload);
    return TRUE;
}

bool ElUtil::ToElFloatArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ jfloatArray jarr,
    /* [out] */ ArrayOf<Float>** fArray)
{
    if (fArray == NULL) {
        return FALSE;
    }

    *fArray = NULL;

    if (env == NULL || jarr == NULL) {
        ALOGD("ToElFloatArray() Invalid arguments!");
        return FALSE;
    }

    jint size = env->GetArrayLength(jarr);
    CheckErrorAndLog(env, "GetArrayLength:(): %d!\n", __LINE__);

    jfloat* jpayload = (jfloat*)malloc(size * sizeof(Float));

    env->GetFloatArrayRegion(jarr, 0, size, jpayload);
    CheckErrorAndLog(env, "GetFloatArrayRegion:(): %d!\n", __LINE__);

    (*fArray) = ArrayOf<Float>::Alloc(size);
    (*fArray)->Copy((Float*)jpayload, size);
    (*fArray)->AddRef();

    free(jpayload);
    return TRUE;
}

jbyteArray ElUtil::GetJavaByteArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ ArrayOf<Byte>* byteArray)
{

    if (env == NULL || byteArray == NULL) {
        ALOGD("GetJavaByteArray() Invalid arguments!");
        return NULL;
    }

    Int32 count = byteArray->GetLength();

    jbyteArray jbytearray = env->NewByteArray((jsize)count);
    CheckErrorAndLog(env, "NewByteArray: %d!\n", __LINE__);

    jbyte* byteArrayInd = (jbyte*)byteArray->GetPayload();

    env->SetByteArrayRegion(jbytearray, 0, count, byteArrayInd);
    CheckErrorAndLog(env, "SetByteArrayRegion: %d!\n", __LINE__);

    return jbytearray;
}

bool ElUtil::ToElByteArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ jbyteArray jarray,
    /* [out] */ ArrayOf<Byte>** byteArray)
{
    if (env == NULL || jarray == NULL || byteArray == NULL) {
        ALOGE("ToElByteArray() Invalid arguments!");
        return false;
    }

    jint size = env->GetArrayLength(jarray);
    CheckErrorAndLog(env, "GetArrayLength:(): %d!\n", __LINE__);

    jbyte* jpayload = (jbyte*)malloc(size);
    env->GetByteArrayRegion(jarray, 0, size, jpayload);
    CheckErrorAndLog(env, "GetByteArrayRegion:(): %d!\n", __LINE__);

    (*byteArray) = ArrayOf<Byte>::Alloc(size);
    (*byteArray)->Copy((Byte*)jpayload, size);
    (*byteArray)->AddRef();

    free(jpayload);
    return true;
}

bool ElUtil::ToElCharArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ jcharArray jarray,
    /* [out] */ ArrayOf<Char32>** array)
{
    if (env == NULL || jarray == NULL || array == NULL) {
        ALOGE("ToElByteArray() Invalid arguments!");
        return false;
    }

    jint size = env->GetArrayLength(jarray);
    CheckErrorAndLog(env, "GetArrayLength:(): %d!\n", __LINE__);

    jchar* jpayload = (jchar*)malloc(size);
    env->GetCharArrayRegion(jarray, 0, size, jpayload);
    CheckErrorAndLog(env, "GetCharArrayRegion:(): %d!\n", __LINE__);

    (*array) = ArrayOf<Char32>::Alloc(size);
    (*array)->Copy((Char32*)jpayload, size);
    (*array)->AddRef();

    free(jpayload);
    return true;
}

bool ElUtil::ToElIntent(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jintent,
    /* [out] */ IIntent** elIntent)
{
    if (elIntent == NULL) {
        ALOGD("ToElIntent: Invalid argumenet!");
        return false;
    }

    jclass intentKlass = env->FindClass("android/content/Intent");
    CheckErrorAndLog(env, "FindClass: android/content/Intent : %d!\n", __LINE__);

    if (NOERROR != CIntent::New(elIntent)) {
        ALOGD("ToElIntent: create CIntent fail!");
        return false;
    }

    jfieldID f = env->GetFieldID(intentKlass, "mAction", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: mAction : %d!\n", __LINE__);

    jstring jaction = (jstring)env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField: jaction : %d!\n", __LINE__);

    if (jaction != NULL) {
        String actionArray = ToElString(env, jaction);
        (*elIntent)->SetAction(actionArray);
        env->DeleteLocalRef(jaction);
    }

    f = env->GetFieldID(intentKlass, "mData", "Landroid/net/Uri;");
    CheckErrorAndLog(env, "GetFieldID: mData : %d!\n", __LINE__);

    jobject jdata = env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField: jdata : %d!\n", __LINE__);

    AutoPtr<IUri> data;
    if (jdata != NULL)  {
        if (!ToElUri(env, jdata, (IUri**)&data)) {
            ALOGE("ToElIntent: ToElUri fail : %d!\n", __LINE__);
        }

        env->DeleteLocalRef(jdata);
    }

    f = env->GetFieldID(intentKlass, "mType", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: mType : %d!\n", __LINE__);

    jstring jtype = (jstring)env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField: jtype : %d!\n", __LINE__);

    if (jtype != NULL) {
        String type = ToElString(env, jtype);

        if (data != NULL) {
            (*elIntent)->SetDataAndType(data, String(type));
        }
        else {
            (*elIntent)->SetType(String(type));
        }

        env->DeleteLocalRef(jtype);
    }
    else if (data != NULL) {
        (*elIntent)->SetData(data);
    }

    f = env->GetFieldID(intentKlass, "mPackage", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: mPackage : %d!\n", __LINE__);

    jstring jpackage = (jstring)env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField: jpackage : %d!\n", __LINE__);

    if (jpackage != NULL)  {
        String packageArray = ToElString(env, jpackage);
        (*elIntent)->SetPackage(packageArray);
        env->DeleteLocalRef(jpackage);
    }

    f = env->GetFieldID(intentKlass, "mComponent", "Landroid/content/ComponentName;");
    CheckErrorAndLog(env, "GetFieldID: mComponent : %d!\n", __LINE__);

    jobject jComponent = env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField jintent : %d!\n", __LINE__);

    if (jComponent != NULL) {
        AutoPtr<IComponentName> elcomponentName;
        if (ToElComponentName(env, jComponent, (IComponentName**)&elcomponentName)) {
            (*elIntent)->SetComponent(elcomponentName);
        }
        else {
            ALOGE("ToElIntent: ToElComponentName fail : %d!\n", __LINE__);
        }
        env->DeleteLocalRef(jComponent);
    }

    f = env->GetFieldID(intentKlass, "mFlags", "I");
    CheckErrorAndLog(env, "GetFieldID: mFlags : %d!\n", __LINE__);

    jint jflags = env->GetIntField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField jflags : %d!\n", __LINE__);
    (*elIntent)->SetFlags((Int32)jflags);

    f = env->GetFieldID(intentKlass, "mCategories", "Landroid/util/ArraySet;");
    CheckErrorAndLog(env, "GetFieldID: mCategories : %d!\n", __LINE__);

    jobject jcategories = env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField jcategories : %d!\n", __LINE__);

    if (jcategories != NULL) {
        jclass setKlass = env->FindClass("android/util/ArraySet");
        CheckErrorAndLog(env, "ToElIntent FindClass: ArraySet fail : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(setKlass, "size", "()I");
        CheckErrorAndLog(env, "ToElIntent Fail GetMethodID: size : %d!\n", __LINE__);

        jint size = env->CallIntMethod(jcategories, m);
        CheckErrorAndLog(env, "ToElIntent Fail CallObjectMethod: size : %d!\n", __LINE__);

        if (size > 0) {
            m = env->GetMethodID(setKlass, "iterator", "()Ljava/util/Iterator;");
            CheckErrorAndLog(env, "ToElIntent Fail GetMethodID: iterator : %d!\n", __LINE__);

            jobject jiterator = env->CallObjectMethod(jcategories, m);
            CheckErrorAndLog(env, "ToElIntent Fail CallObjectMethod: size : %d!\n", __LINE__);

            jclass iterKlass = env->FindClass("java/util/Iterator");
            CheckErrorAndLog(env, "ToElIntent FindClass: Iterator fail : %d!\n", __LINE__);

            m = env->GetMethodID(iterKlass, "hasNext", "()Z");
            CheckErrorAndLog(env, "ToElIntent Fail GetMethodID: iterator : %d!\n", __LINE__);

            jboolean jhasNext = env->CallBooleanMethod(jiterator, m);
            CheckErrorAndLog(env, "ToElIntent Fail CallBooleanMethod: jhasNext : %d!\n", __LINE__);

            jmethodID mnext = env->GetMethodID(iterKlass, "next", "()Ljava/lang/Object;");
            CheckErrorAndLog(env, "ToElIntent Fail GetMethodID: iterator : %d!\n", __LINE__);

            while (jhasNext) {
                jstring jCategorie = (jstring)env->CallObjectMethod(jiterator, mnext);
                CheckErrorAndLog(env, "ToElIntent Fail CallObjectMethod: size : %d!\n", __LINE__);

                String categorie = ToElString(env, jCategorie);
                env->DeleteLocalRef(jCategorie);

                (*elIntent)->AddCategory(categorie);

                jhasNext = env->CallBooleanMethod(jiterator, m);
                CheckErrorAndLog(env, "ToElIntent Fail CallBooleanMethod: jhasNext : %d!\n", __LINE__);
            }

            env->DeleteLocalRef(iterKlass);
            env->DeleteLocalRef(jiterator);
        }

        env->DeleteLocalRef(setKlass);
        env->DeleteLocalRef(jcategories);
    }

    f = env->GetFieldID(intentKlass, "mExtras", "Landroid/os/Bundle;");
    CheckErrorAndLog(env, "GetFieldID: mExtras Landroid/os/Bundle; : %d!\n", __LINE__);

    jobject jBundle = env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField: jintent : %d!\n", __LINE__);

    if (jBundle != NULL) {
        AutoPtr<IBundle> elBundle;
        if (ToElBundle(env, jBundle, (IBundle**)&elBundle)) {
            (*elIntent)->ReplaceExtras(elBundle);

            jclass bundleKlass = env->FindClass("android/os/Bundle");
            CheckErrorAndLog(env, "FindClass: android/os/Bundle : %d!\n", __LINE__);
            jboolean jallowFds = GetJavaBoolField(env, bundleKlass, jBundle, "mAllowFds", "ToElIntent");
            (*elIntent)->SetAllowFds(jallowFds);
        }
        else {
            ALOGE("ToElIntent: ToElBundle(jBundle) fail : %d!\n", __LINE__);
        }
        env->DeleteLocalRef(jBundle);
    }

    f = env->GetFieldID(intentKlass, "mSourceBounds", "Landroid/graphics/Rect;");
    CheckErrorAndLog(env, "GetFieldID: mSourceBounds : %d!\n", __LINE__);

    jobject jsourceBounds = env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField jsourceBounds : %d!\n", __LINE__);

    if (jsourceBounds != NULL) {
        AutoPtr<IRect> sourceBounds;
        if (ToElRect(env, jsourceBounds, (IRect**)&sourceBounds)) {
            (*elIntent)->SetSourceBounds(sourceBounds);
        }
        else {
            ALOGE("ToElIntent: ToElRect() fail : %d!\n", __LINE__);
        }

        env->DeleteLocalRef(jsourceBounds);
    }

    f = env->GetFieldID(intentKlass, "mSelector", "Landroid/content/Intent;");
    CheckErrorAndLog(env, "GetFieldID: mSelector : %d!\n", __LINE__);

    jobject jselector = env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField jselector : %d!\n", __LINE__);

    if (jselector != NULL) {
        AutoPtr<IIntent> intent;
        if (ToElIntent(env, jselector, (IIntent**)&intent)) {
            (*elIntent)->SetSelector(intent);
        }
        else {
            ALOGE("ToElIntent: ToElIntent(jselector) fail : %d!\n", __LINE__);
        }
        env->DeleteLocalRef(jselector);
    }

    f = env->GetFieldID(intentKlass, "mClipData", "Landroid/content/ClipData;");
    CheckErrorAndLog(env, "GetFieldID: mClipData : %d!\n", __LINE__);

    jobject jclipData = env->GetObjectField(jintent, f);
    CheckErrorAndLog(env, "GetObjectField jclipData : %d!\n", __LINE__);

    if (jclipData != NULL) {
        AutoPtr<IClipData> clipData;
        if (ElUtil::ToElClipData(env, jclipData, (IClipData**)&clipData)) {
            (*elIntent)->SetClipData(clipData);
        }
        else {
            ALOGE("ToElIntent: ToElClipData() fail : %d!\n", __LINE__);
        }
        env->DeleteLocalRef(jclipData);
    }

    jint jcontentUserHint = GetJavaIntField(env, intentKlass, jintent, "mContentUserHint", "ToElIntent");
    (*elIntent)->SetContentUserHint(jcontentUserHint);

    env->DeleteLocalRef(intentKlass);

    return true;
}

bool ElUtil::ToElComponentName(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jcomponentName,
    /* [out] */ IComponentName** elcomponentName)
{
    if (elcomponentName == NULL) {
        ALOGE("ToElComponentName: Invalid argumenet!");
        return false;
    }

    if (jcomponentName == NULL) {
        ALOGE("ToElComponentName(), jComponent is NULL");
        return false;
    }

    //create fake java ComponentName
    jclass componentNameKlass = env->FindClass("android/content/ComponentName");
    CheckErrorAndLog(env, "FindClass: android/content/ComponentName : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(componentNameKlass, "mPackage", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: mPackage : %d!\n", __LINE__);

    jstring jpackage = (jstring)env->GetObjectField(jcomponentName, f);
    CheckErrorAndLog(env, "GetObjectField: jpackage : %d!\n", __LINE__);

    f = env->GetFieldID(componentNameKlass, "mClass", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: componentNameKlass : %d!\n", __LINE__);
    env->DeleteLocalRef(componentNameKlass);

    jstring jclassname = (jstring)env->GetObjectField(jcomponentName, f);
    CheckErrorAndLog(env, "GetObjectField: jclassname : %d!\n", __LINE__);

    String pakcage;
    if (jpackage != NULL) {
        pakcage = ToElString(env, jpackage);
        env->DeleteLocalRef(jpackage);
    }

    String classname;
    if (jclassname != NULL) {
        classname = ToElString(env, jclassname);
        env->DeleteLocalRef(jclassname);
    }

    if (NOERROR != CComponentName::New(pakcage, classname, elcomponentName)) {
        ALOGD("ToElComponentName: create CComponentName fail!");
        return false;
    }

    return true;
}

bool ElUtil::ToElBundle(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jBundle,
    /* [out] */ IBundle** elBundle)
{
    if (env == NULL || elBundle == NULL) {
        ALOGD("ToElBundle: Invalid argumenet!");
        return false;
    }

    if (NOERROR != CBundle::New(elBundle)) {
        ALOGD("ToElBundle: create CBundle fail!");
        return false;
    }

    jclass bundleKlass = env->FindClass("android/os/Bundle");
    CheckErrorAndLog(env, "FindClass: android/os/Bundle : %d!\n", __LINE__);

    jboolean jallowFds = GetJavaBoolField(env, bundleKlass, jBundle, "mAllowFds", "ToElBundle");
    Boolean old;
    (*elBundle)->SetAllowFds(jallowFds, &old);

    return SetElBaseBundle(env, jBundle, IBaseBundle::Probe(*elBundle));
}

bool ElUtil::SetElBaseBundle(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jBundle,
    /* [in] */ IBaseBundle* elBundle)
{
    jclass bundleKlass = env->GetObjectClass(jBundle);
    CheckErrorAndLog(env, "GetObjectClass: jBundle : %d!\n", __LINE__);

    jclass parcelClass = env->FindClass("android/os/Parcel");
    ElUtil::CheckErrorAndLog(env, "FindClass: Parcel : %d!\n", __LINE__);

    jmethodID m = env->GetStaticMethodID(parcelClass, "obtain", "()Landroid/os/Parcel;");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: obtain : %d!\n", __LINE__);

    jobject jparcel = env->CallStaticObjectMethod(parcelClass, m);
    ElUtil::CheckErrorAndLog(env, "CallStaticObjectMethod: obtain : %d!\n", __LINE__);

    m = env->GetMethodID(bundleKlass, "writeToParcel", "(Landroid/os/Parcel;I)V");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: writeToParcel : %d!\n", __LINE__);

    env->CallVoidMethod(jBundle, m, jparcel, 0);
    ElUtil::CheckErrorAndLog(env, "CallVoidMethod: writeToParcel : %d!\n", __LINE__);

    Int64 nativePtr = ElUtil::GetJavalongField(env, parcelClass, jparcel, "mNativePtr", "SetElBaseBundle");
    android::Parcel* parcel = reinterpret_cast< android::Parcel*>(nativePtr);
    parcel->setDataPosition(0);
    if (parcel->dataSize() > 0) {
        AutoPtr<ArrayOf<Byte> > data = ArrayOf<Byte>::Alloc(parcel->dataSize());
        memcpy(data->GetPayload(), parcel->data(), parcel->dataSize());
        elBundle->SetJavaData(data);
    }
    env->DeleteLocalRef(parcelClass);
    env->DeleteLocalRef(jparcel);

    m = env->GetMethodID(bundleKlass, "keySet", "()Ljava/util/Set;");
    CheckErrorAndLog(env, "GetMethodID: keySet : %d!\n", __LINE__);

    jobject jKeySet = env->CallObjectMethod(jBundle, m);
    // CheckErrorAndLog(env, "CallObjectMethod: jKeySet : %d!\n", __LINE__);
    if (env->ExceptionCheck() != 0) {
        jKeySet = NULL;
        env->ExceptionClear();
    }

    if (jKeySet != NULL) {
        jclass setKlass = env->FindClass("java/util/Set");
        CheckErrorAndLog(env, "FindClass: java/util/Set : %d!\n", __LINE__);

        m = env->GetMethodID(setKlass, "iterator", "()Ljava/util/Iterator;");
        CheckErrorAndLog(env, "GetMethodID: iterator ()Ljava/util/Iterator; : %d!\n", __LINE__);

        env->DeleteLocalRef(setKlass);

        jobject jIterator = env->CallObjectMethod(jKeySet, m);
        CheckErrorAndLog(env, "CallObjectMethod: jIterator : %d!\n", __LINE__);

        if (jIterator != NULL) {
            jclass iteratorKlass = env->FindClass("java/util/Iterator");
            CheckErrorAndLog(env, "FindClass: java/util/Iterator : %d!\n", __LINE__);

            jmethodID jHasNext = env->GetMethodID(iteratorKlass, "hasNext", "()Z");
            CheckErrorAndLog(env, "GetMethodID: hasNext : %d!\n", __LINE__);

            jmethodID jNext = env->GetMethodID(iteratorKlass, "next", "()Ljava/lang/Object;");
            CheckErrorAndLog(env, "GetMethodID: next : %d!\n", __LINE__);

            env->DeleteLocalRef(iteratorKlass);

            jboolean hasNext = env->CallBooleanMethod(jIterator, jHasNext);
            while (hasNext) {
                jstring jKey = (jstring)env->CallObjectMethod(jIterator, jNext);
                String ckey = ToElString(env, jKey);

                m = env->GetMethodID(bundleKlass, "get", "(Ljava/lang/String;)Ljava/lang/Object;");
                CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

                jobject jValue = env->CallObjectMethod(jBundle, m, jKey);
                CheckErrorAndLog(env, "CallObjectMethod: jValue : %d!\n", __LINE__);

                env->DeleteLocalRef(jKey);

                if (jValue != NULL) {
                    String objClassName = GetClassName(env, jValue);

        //ALOGE("SetElBaseBundle key: %s, valueType:%s ! line : %d", ckey.string(), objClassName.string(), __LINE__);
                    if (objClassName.Equals("java.lang.String")) {
                        String value = ToElString(env, (jstring)jValue);
                        elBundle->PutString(ckey, value);
                    }
                    else if (objClassName.Equals("java.lang.Boolean")) {
                        jclass c = env->GetObjectClass(jValue);
                        jboolean value = GetJavaBoolField(env, c, jValue, "value", "SetElBaseBundle()");
                        env->DeleteLocalRef(c);
                        elBundle->PutBoolean(ckey, value);
                    }
                    else if (objClassName.Equals("java.lang.Integer")) {
                        jclass c = env->GetObjectClass(jValue);
                        jint value = GetJavaIntField(env, c, jValue, "value", "SetElBaseBundle");
                        env->DeleteLocalRef(c);
                        elBundle->PutInt32(ckey, value);
                    }
                    else if (objClassName.Equals("java.lang.Short")) {
                        jclass c = env->GetObjectClass(jValue);
                        jshort value = GetJavaShortField(env, c, jValue, "value", "SetElBaseBundle");
                        env->DeleteLocalRef(c);
                        elBundle->PutInt16(ckey, value);
                    }
                    else if (objClassName.Equals("android.content.Intent")) {
                        AutoPtr<IIntent> intent;
                        ToElIntent(env, jValue, (IIntent**)&intent);
                        AutoPtr<IParcelable> pracelable = IParcelable::Probe(intent);
                        if (IBundle::Probe(elBundle))
                            IBundle::Probe(elBundle)->PutParcelable(ckey, pracelable);
                        else {
                            ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                        }
                    }
                    else if (objClassName.Equals("java.lang.Long")) {
                        jclass c = env->GetObjectClass(jValue);
                        jlong jlongValue = GetJavalongField(env, c, jValue, "value", "SetElBaseBundle()");
                        elBundle->PutInt64(ckey, (Int64)jlongValue);
                    }
                    else if (objClassName.Equals("android.os.Bundle")) {
                        AutoPtr<IBundle> bundle;
                        ElUtil::ToElBundle(env, jValue, (IBundle**)&bundle);
                        if (IBundle::Probe(elBundle))
                            IBundle::Probe(elBundle)->PutBundle(ckey, bundle);
                        else {
                            ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                        }
                    }
                    else if (objClassName.Equals("android.graphics.Bitmap")) {
                        AutoPtr<IBitmap> bitmap;
                        ElUtil::ToElBitmap(env, jValue, (IBitmap**)&bitmap);
                        if (bitmap != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(bitmap));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::ToElBitmap(), create elastos bitmap failed!");
                        }
                    }
                    else if (objClassName.Equals("android.content.ComponentName")) {
                        AutoPtr<IComponentName> componentName;
                        if (ToElComponentName(env, jValue, (IComponentName**)&componentName))
                        if (componentName != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(componentName));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                    }
                    else if (objClassName.Equals("android.appwidget.AppWidgetProviderInfo")) {
                        AutoPtr<IAppWidgetProviderInfo> providerInfo;
                        ToElAppWidgetProviderInfo(env, jValue, (IAppWidgetProviderInfo**)&providerInfo);
                        if (providerInfo != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(providerInfo));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                    }
                    else if (objClassName.Equals("android.content.Intent$ShortcutIconResource")) {
                        AutoPtr<IIntentShortcutIconResource> iconRes;
                        ToElIntentShortcutIconResource(env, jValue, (IIntentShortcutIconResource**)&iconRes);
                        if (iconRes != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(iconRes));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                    }
                    else if (objClassName.Equals("[Landroid.os.Parcelable;")) {
                        ALOGE("SetElBaseBundle, Found parcelable array for key:%s", ckey.string());
                    }
                    else if (objClassName.Equals("android.app.PendingIntent")) {
                        AutoPtr<IPendingIntent> intent;
                        ElUtil::ToElPendingIntent(env, jValue, (IPendingIntent**)&intent);
                        if (intent != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(intent));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::ToElPendingIntent(), create elastos intent failed!");
                        }
                    }
                    else if (objClassName.Equals("android.net.NetworkInfo")) {
                        AutoPtr<INetworkInfo> info;
                        ElUtil::ToElNetworkInfo(env, jValue, (INetworkInfo**)&info);
                        if (info != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(info));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::ToElNetworkInfo() failed!");
                        }
                    }
                    else if (objClassName.Equals("android.net.wifi.WifiInfo")) {
                        AutoPtr<IWifiInfo> info;
                        ElUtil::ToElWifiInfo(env, jValue, (IWifiInfo**)&info);
                        if (info != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(info));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::ToElWifiInfo() failed!");
                        }
                    }
                    else if (objClassName.Equals("android.net.LinkProperties")) {
                        AutoPtr<ILinkProperties> properties;
                        ElUtil::ToElLinkProperties(env, jValue, (ILinkProperties**)&properties);
                        if (properties != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(properties));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::ToElLinkProperties() failed!");
                        }
                    }
                    else if (objClassName.Equals("android.os.storage.StorageVolume")) {
                        AutoPtr<IStorageVolume> volume;
                        ElUtil::ToElStorageVolume(env, jValue, (IStorageVolume**)&volume);
                        if (volume != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(volume));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::ToElStorageVolume() failed!");
                        }
                    }
                    else if (objClassName.Equals("android.content.pm.ApplicationInfo")) {
                        AutoPtr<IApplicationInfo> info;
                        ElUtil::ToElApplicationInfo(env, jValue, (IApplicationInfo**)&info);
                        if (info != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(info));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElApplicationInfo failed!");
                        }
                    }
                    else if (objClassName.Equals("android.net.Uri$StringUri") ||
                            objClassName.Equals("android.net.Uri$OpaqueUri")||
                            objClassName.Equals("android.net.Uri$HierarchicalUri")) {
                        AutoPtr<IUri> uri;
                        ElUtil::ToElUri(env, jValue, (IUri**)&uri);
                        if (uri != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(uri));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElUri failed!");
                        }
                    }
                    else if (objClassName.Equals("android.app.FragmentManagerState")) {
                        AutoPtr<IFragmentManagerState> state;
                        ElUtil::ToElFragmentManagerState(env, jValue, (IFragmentManagerState**)&state);
                        if (state != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(state));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElFragmentManagerState failed!");
                        }
                    }
                    else if (objClassName.Equals("[B")) {
                        jbyteArray jbyteArrayValue = (jbyteArray)jValue;

                        AutoPtr<ArrayOf<Byte> > byteArrayValue;
                        if (ElUtil::ToElByteArray(env, jbyteArrayValue, (ArrayOf<Byte>**)&byteArrayValue)) {
                            elBundle->PutByteArray(ckey, byteArrayValue);
                        }
                        else {
                            ALOGE("SetElBaseBundle() ToElByteArray fail! Line: %d", __LINE__);
                        }
                    }
                    else if (objClassName.Equals("[I")) {
                        jintArray jintArrayValue = (jintArray)jValue;

                        AutoPtr<ArrayOf<Int32> > intArrayValue;
                        if (ElUtil::ToElIntArray(env, jintArrayValue, (ArrayOf<Int32>**)&intArrayValue)) {
                            elBundle->PutInt32Array(ckey, intArrayValue);
                        }
                        else {
                            ALOGE("SetElBaseBundle() ToElIntArray fail! Line: %d", __LINE__);
                        }
                    }
                    else if (objClassName.Equals("[J")) {
                        jlongArray jlongArrayValue = (jlongArray)jValue;

                        jint size = env->GetArrayLength(jlongArrayValue);
                        CheckErrorAndLog(env, "GetArrayLength:(): %d!\n", __LINE__);

                        jlong* jpayload = env->GetLongArrayElements(jlongArrayValue, 0);
                        CheckErrorAndLog(env, "GetLongArrayElements:(): %d!\n", __LINE__);
                        AutoPtr<ArrayOf<Int64> > longArrayValue = ArrayOf<Int64>::Alloc((Int64*)jpayload, size);
                        elBundle->PutInt64Array(ckey, longArrayValue);
                        env->ReleaseLongArrayElements(jlongArrayValue, jpayload, 0);
                    }
                    else if (objClassName.Equals("[Ljava.lang.String;")) {
                        jobjectArray jstringArrayValue = (jobjectArray)jValue;

                        AutoPtr<ArrayOf<String> > stringArrayValue;
                        if (ElUtil::ToElStringArray(env, jstringArrayValue, (ArrayOf<String>**)&stringArrayValue)) {
                            elBundle->PutStringArray(ckey, stringArrayValue);
                        }
                        else {
                            ALOGE("SetElBaseBundle() ToElStringArray fail! key:%s, objClassName:%s, Line: %d",
                                                                ckey.string(), objClassName.string(), __LINE__);
                        }
                    }
                    else if (objClassName.Equals("java.util.HashMap")) {
                        AutoPtr<IHashMap> map;
                        if (ElUtil::ToElHashMap(env, jValue, (IHashMap**)&map)) {
                            elBundle->PutSerializable(ckey, ISerializable::Probe(map));
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElHashMap failed!");
                        }
                    }
                    else if (objClassName.Equals("com.android.internal.net.VpnProfile")) {
                        AutoPtr<IVpnProfile> profile;
                        ElUtil::ToElVpnProfile(env, jValue, (IVpnProfile**)&profile);
                        if (profile != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(profile));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElVpnProfile failed!");
                        }
                    }
                    else if (objClassName.Equals("android.net.wifi.SupplicantState")) {
                        AutoPtr<ISupplicantState> state;
                        ElUtil::ToElSupplicantState(env, jValue, (ISupplicantState**)&state);
                        if (state != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(state));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElSupplicantState failed!");
                        }
                    }
                    else if (objClassName.Equals("android.net.wifi.p2p.WifiP2pDevice")) {
                        AutoPtr<IWifiP2pDevice> dev;
                        ElUtil::ToElWifiP2pDevice(env, jValue, (IWifiP2pDevice**)&dev);
                        if (dev != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(dev));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElSupplicantState failed!");
                        }
                    }
                    else if (objClassName.Equals("android.net.wifi.p2p.WifiP2pInfo")) {
                        AutoPtr<IWifiP2pInfo> info;
                        ElUtil::ToElWifiP2pInfo(env, jValue, (IWifiP2pInfo**)&info);
                        if (info != NULL) {
                            if (IBundle::Probe(elBundle))
                                IBundle::Probe(elBundle)->PutParcelable(ckey, IParcelable::Probe(info));
                            else {
                                ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                            }
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElSupplicantState failed!");
                        }
                    }
                    else if (objClassName.Equals("java.util.ArrayList")) {
                        jclass alistKlass = env->FindClass("java/util/ArrayList");
                        CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

                        jmethodID m = env->GetMethodID(alistKlass, "size", "()I");
                        CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

                        jint jsize = env->CallIntMethod(jValue, m);
                        CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

                        jmethodID mGet = env->GetMethodID(alistKlass, "get", "(I)Ljava/lang/Object;");
                        CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

                        if (jsize > 0) {
                            jobject jitem0 = env->CallObjectMethod(jValue, mGet, 0);
                            CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                            jclass serClass = env->FindClass("java/io/Serializable");
                            ElUtil::CheckErrorAndLog(env, "FindClass Serializable fail : %d!\n", __LINE__);
                            jclass parClass = env->FindClass("android/os/Parcelable");
                            ElUtil::CheckErrorAndLog(env, "FindClass Parcelable fail : %d!\n", __LINE__);

                            AutoPtr<IList> value;
                            CParcelableList::New((IList**)&value);

                            String itemCName = GetClassName(env, jitem0);
                            if (itemCName.Equals("java.lang.String")) {
                                String item0 = ElUtil::ToElString(env, (jstring)jitem0);
                                AutoPtr<ICharSequence> csitem0;
                                CString::New(item0, (ICharSequence**)&csitem0);
                                Boolean result = FALSE;
                                value->Add((IInterface*)csitem0);

                                for (Int32 i = 1; i < jsize; i++) {
                                    jstring jitem = (jstring)env->CallObjectMethod(jValue, mGet, i);
                                    CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                                    String item = ElUtil::ToElString(env, jitem);
                                    AutoPtr<ICharSequence> csitem;
                                    CString::New(item, (ICharSequence**)&csitem);
                                    value->Add((IInterface*)csitem);
                                    env->DeleteLocalRef(jitem);
                                }
                            }
                            else if (env->IsInstanceOf(jitem0, parClass)) {
                                AutoPtr<IParcelable> parcelable0;
                                if (ElUtil::ToElParcelable(env, jitem0, (IParcelable**)&parcelable0)) {
                                    value->Add((IInterface*)parcelable0.Get());
                                }
                                else {
                                    ALOGE("SetElBaseBundle() ArrayList ToElParcelable fail!");
                                }

                                for (Int32 i = 1; i < jsize; i++) {
                                    jobject jitem = env->CallObjectMethod(jValue, mGet, i);
                                    CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                                    AutoPtr<IParcelable> parcelable;
                                    if (ElUtil::ToElParcelable(env, jitem, (IParcelable**)&parcelable)) {
                                        value->Add((IInterface*)parcelable.Get());
                                    }
                                    else {
                                        ALOGE("SetElBaseBundle() ArrayList ToElParcelable fail!");
                                    }
                                    env->DeleteLocalRef(jitem);
                                }
                            }
                            else if (env->IsInstanceOf(jitem0, serClass)) {
                                ALOGD("SetElBaseBundle() Item Serializable in ArrayList, className is:%s", itemCName.string());
                                AutoPtr<ISerializable> serializable0;
                                if (ElUtil::ToElSerializable(env, jitem0, (ISerializable**)&serializable0)) {
                                    value->Add((IInterface*)serializable0.Get());
                                }
                                else {
                                    ALOGE("SetElBaseBundle() ArrayList ToElSerializable fail!");
                                }

                                for (Int32 i = 1; i < jsize; i++) {
                                    jobject jitem = env->CallObjectMethod(jValue, mGet, i);
                                    CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                                    AutoPtr<ISerializable> serializable;
                                    if (ElUtil::ToElSerializable(env, jitem, (ISerializable**)&serializable)) {
                                        value->Add((IInterface*)serializable.Get());
                                    }
                                    else {
                                        ALOGE("SetElBaseBundle() ArrayList ToElSerializable fail!");
                                    }
                                    env->DeleteLocalRef(jitem);
                                }
                            }
                            else {
                                ALOGE("SetElBaseBundle() Item Unknown in ArrayList, className is:%s", itemCName.string());
                            }

                            elBundle->PutSerializable(ckey, ISerializable::Probe(value));
                            env->DeleteLocalRef(jitem0);
                            env->DeleteLocalRef(parClass);
                        }
                        env->DeleteLocalRef(alistKlass);
                    }
                    else if (objClassName.Equals("android.text.SpannableStringBuilder")) {
                        AutoPtr<ICharSequence> value;
                        ToElCharSequence(env, jValue, (ICharSequence**)&value);

                        if (value != NULL) {
                            elBundle->PutCharSequence(ckey, value);
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElCharSequence failed!, line:%d", __LINE__);
                        }
                    }
                    else if (objClassName.Equals("android.util.SparseArray")) {
                        AutoPtr<ISparseArray> value;
                        ToElSparseArray(env, jValue, (ISparseArray**)&value);

                        if (value != NULL) {
                            IBundle::Probe(elBundle)->PutSparseParcelableArray(ckey, value);
                        }
                        else{
                            ALOGE("ElUtil::SetElBaseBundle() ToElSparseArray failed!, line:%d", __LINE__);
                        }
                    }
                    else{
                        jclass serClass = env->FindClass("java/io/Serializable");
                        jclass parClass = env->FindClass("android/os/Parcelable");
                        ElUtil::CheckErrorAndLog(env, "FindClass Serializable fail : %d!\n", __LINE__);

                        if (env->IsInstanceOf(jValue, serClass)) {
                            AutoPtr<ISerializable> serializable;
                            if (ElUtil::ToElSerializable(env, jValue, (ISerializable**)&serializable)) {
                                elBundle->PutSerializable(ckey, serializable);
                            }
                            else {
                                ALOGE("SetElBaseBundle ToElSerializable fail");
                            }
                        }
                        else if (env->IsInstanceOf(jValue, parClass)) {
                            AutoPtr<IParcelable> parcelable;
                            if (ElUtil::ToElParcelable(env, jValue, (IParcelable**)&parcelable)) {
                                if (IBundle::Probe(elBundle))
                                    IBundle::Probe(elBundle)->PutParcelable(ckey, parcelable);
                                else {
                                    ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                                }
                            }
                            else {
                                ALOGE("SetElBaseBundle ToElParcelable fail");
                            }
                        }
                        else {
                            ALOGE("SetElBaseBundle() Unsupported type:%s; Key:%s", objClassName.string(), ckey.string());
                        }

                        // if (objClassName.Equals("android.util.SparseArray")) {
                        //     jclass sparseArrayKlass = env->FindClass("android/util/SparseArray");
                        //     CheckErrorAndLog(env, "FindClass: android/util/SparseArray : %d!\n", __LINE__);
                        //     jmethodID mSize = env->GetMethodID(sparseArrayKlass, "size", "()I");
                        //     CheckErrorAndLog(env, "GetMethodID: size() : %d!\n", __LINE__);
                        //     jint value = env->CallIntMethod(jValue, mSize);
                        //     CheckErrorAndLog(env, "CallIntMethod: size() : %d!\n", __LINE__);

                        //     for(Int32 i = 0; i < value; i++) {
                        //         jmethodID mValueAt = env->GetMethodID(sparseArrayKlass, "valueAt", "(I)Ljava/lang/Object;");
                        //         CheckErrorAndLog(env, "GetMethodID: mValueAt() : %d!\n", __LINE__);
                        //         jobject jitem = env->CallObjectMethod(jValue, mValueAt, i);
                        //         CheckErrorAndLog(env, "CallIntMethod: valueAt() : %d!\n", __LINE__);

                        //         String itemClassName = GetClassName(env, jitem);
                        //         ALOGE("SetElBaseBundle", "Item %d in SparseArray, className is:%s", i, itemClassName.string());

                        //         env->DeleteLocalRef(jitem);
                        //     }

                        //     env->DeleteLocalRef(sparseArrayKlass);
                        // }
                        env->DeleteLocalRef(serClass);
                        env->DeleteLocalRef(parClass);
                    }
                    env->DeleteLocalRef(jValue);
                }
                else {
                    if (IBundle::Probe(elBundle))
                        IBundle::Probe(elBundle)->PutParcelable(ckey, NULL);
                    else {
                        ALOGE("SetElBaseBundle elBundle is not Bundle! line : %d", __LINE__);
                    }
                }

                hasNext = env->CallBooleanMethod(jIterator, jHasNext);
            }
            env->DeleteLocalRef(jIterator);
        }
        env->DeleteLocalRef(jKeySet);
    }
    env->DeleteLocalRef(bundleKlass);
    return true;
}

jobject ElUtil::GetJavaBundle(
    /* [in] */ JNIEnv* env,
    /* [in] */ IBundle* bundle)
{
    if (env == NULL || bundle == NULL) {
        ALOGD("GetJavaBundle Invalid arguments!");
        return NULL;
    }

    jobject jBundle = NULL;

    jclass bundleKlass = env->FindClass("android/os/Bundle");
    CheckErrorAndLog(env, "GetJavaBundle Fail FindClass: Bundle : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(bundleKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaBundle Fail GetMethodID: bundleKlass : %d!\n", __LINE__);

    jBundle = env->NewObject(bundleKlass, m);
    CheckErrorAndLog(env, "GetJavaBundle Fail NewObject: bundleKlass : %d!\n", __LINE__);

    if (!ElUtil::SetJavaBaseBundle(env, IBaseBundle::Probe(bundle), jBundle)) {
        ALOGE("GetJavaBundle() GetJavaBundle fail! %d", __LINE__);
        return NULL;
    }

    env->DeleteLocalRef(bundleKlass);

    return jBundle;
}

bool ElUtil::SetJavaBaseBundle(
    /* [in] */ JNIEnv* env,
    /* [in] */ IBaseBundle* bundle,
    /* [in,out] */ jobject jbundle)
{
    if (env == NULL || bundle == NULL || jbundle == NULL) {
        ALOGD("SetJavaBaseBundle Invalid arguments!");
        return false;
    }

    jclass bundleKlass = env->GetObjectClass(jbundle);
    CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetObjectClass : %d!\n", __LINE__);

    AutoPtr<ISet> keySet;
    bundle->GetKeySet((ISet**)&keySet);
    AutoPtr<ArrayOf<IInterface*> > keys;
    keySet->ToArray((ArrayOf<IInterface*>**)&keys);
    for (Int32 i = 0; i < keys->GetLength(); i++) {
        AutoPtr<IInterface> key = (*keys)[i];
        assert(key != NULL && ICharSequence::Probe(key) != NULL);
        String keyStr;
        ICharSequence::Probe(key)->ToString(&keyStr);
        jstring jKey = GetJavaString(env, keyStr);
        AutoPtr<IInterface> value;
        bundle->Get(keyStr, (IInterface**)&value);
        if (value != NULL) {
            if (IInteger32::Probe(value) != NULL) {
                Int32 iv;
                IInteger32::Probe(value)->GetValue(&iv);

                jmethodID m = env->GetMethodID(bundleKlass, "putInt", "(Ljava/lang/String;I)V");
                CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetMethodID: putInt : %d!\n", __LINE__);

                env->CallVoidMethod(jbundle, m, jKey, iv);
            }
            else if (IFloat::Probe(value) != NULL) {
                Float fv;
                IFloat::Probe(value)->GetValue(&fv);

                jmethodID m = env->GetMethodID(bundleKlass, "putFloat", "(Ljava/lang/String;F)V");
                CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetMethodID: put %d", __LINE__);

                env->CallVoidMethod(jbundle, m, jKey, fv);
            }
            else if (IBoolean::Probe(value) != NULL) {
                Boolean bv;
                IBoolean::Probe(value)->GetValue(&bv);
                jmethodID m = env->GetMethodID(bundleKlass, "putBoolean", "(Ljava/lang/String;Z)V");
                CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetMethodID: putBoolean : %d!\n", __LINE__);

                env->CallVoidMethod(jbundle, m, jKey, bv);
            }
            else if (ICharSequence::Probe(value) != NULL) {
                String sv;
                ICharSequence::Probe(value)->ToString(&sv);
                jstring jValue = GetJavaString(env, sv);

                jmethodID m = env->GetMethodID(bundleKlass, "putString", "(Ljava/lang/String;Ljava/lang/String;)V");
                CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetMethodID: putString : %d!\n", __LINE__);

                env->CallVoidMethod(jbundle, m, jKey, jValue);
                env->DeleteLocalRef(jValue);
            }
            else if (IBitmap::Probe(value) != NULL) {
                AutoPtr<IBitmap> bitmap = IBitmap::Probe(value);
                jobject jbitmap = GetJavaBitmap(env, bitmap);

                jmethodID m = env->GetMethodID(bundleKlass, "putParcelable", "(Ljava/lang/String;Landroid/os/Parcelable;)V");
                CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetMethodID: putParcelable : %d!\n", __LINE__);

                env->CallVoidMethod(jbundle, m, jKey, jbitmap);
                env->DeleteLocalRef(jbitmap);
            }
            else if (INetworkInfo::Probe(value) != NULL) {
                AutoPtr<INetworkInfo> info = INetworkInfo::Probe(value);
                jobject jinfo = GetJavaNetworkInfo(env, info);

                jmethodID m = env->GetMethodID(bundleKlass, "putParcelable", "(Ljava/lang/String;Landroid/os/Parcelable;)V");
                CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetMethodID: putParcelable : %d!\n", __LINE__);

                env->CallVoidMethod(jbundle, m, jKey, jinfo);
                env->DeleteLocalRef(jinfo);
            }
            else if (IIRemoteCallback::Probe(value) != NULL) {
                AutoPtr<IIRemoteCallback> info = IIRemoteCallback::Probe(value);
                if (info != NULL) {
                    jclass elrcKlass = env->FindClass("android/os/ElIRemoteCallback");
                    CheckErrorAndLog(env, "FindClass: ElIRemoteCallback : %d!\n", __LINE__);

                    jmethodID m = env->GetMethodID(elrcKlass, "<init>", "(J)V");
                    CheckErrorAndLog(env, "GetMethodID: ElIRemoteCallback : %d!\n", __LINE__);

                    jobject jinfo = env->NewObject(elrcKlass, m, (jlong)info.Get());
                    CheckErrorAndLog(env, "NewObject: ElIRemoteCallback : %d!\n", __LINE__);

                    jmethodID mPutBinder = env->GetMethodID(bundleKlass, "putBinder", "(Ljava/lang/String;Landroid/os/IRemoteCallback;)V");
                    CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetMethodID: putBinder : %d!\n", __LINE__);

                    env->CallVoidMethod(jbundle, mPutBinder, jKey, jinfo);
                    env->DeleteLocalRef(jinfo);
                }
            }
            else if (IArrayList::Probe(value) != NULL) {
                AutoPtr<IArrayList> info = IArrayList::Probe(value);
                if (info != NULL) {
                    AutoPtr<IInterface> type;
                    info->Get(0, (IInterface**)&type);
                    if (ICharSequence::Probe(type) != NULL) {
                        jobject jinfo = GetJavaStringList(env, IList::Probe(info));

                        jmethodID m = env->GetMethodID(bundleKlass, "putStringArrayList", "(Ljava/lang/String;Ljava/util/ArrayList;)V");
                        CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetMethodID: putParcelable : %d!\n", __LINE__);

                        env->CallVoidMethod(jbundle, m, jKey, jinfo);
                        env->DeleteLocalRef(jinfo);
                    }
                }
            }
            else {
                ALOGE("SetJavaBaseBundle() Unsupported type!, key = %s\n", keyStr.string());
                AutoPtr<IObject> object = IObject::Probe(value);
                ClassID clsid;
                object->GetClassID(&clsid);
                DUMP_CLSID(clsid, "SetJavaBaseBundle");
            }
        }
        else {
            jmethodID m = env->GetMethodID(bundleKlass, "putParcelable", "(Ljava/lang/String;Landroid/os/Parcelable;)V");
            CheckErrorAndLog(env, "SetJavaBaseBundle Fail GetMethodID: putParcelable : %d!\n", __LINE__);

            env->CallVoidMethod(jbundle, m, jKey, NULL);
            CheckErrorAndLog(env, "SetJavaBaseBundle Fail CallVoidMethod: putParcelable : %d!\n", __LINE__);
        }

        env->DeleteLocalRef(jKey);
    }

    env->DeleteLocalRef(bundleKlass);
    return true;
}

bool ElUtil::ToElBitmap(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jbitmap,
    /* [out] */ IBitmap** bitmap)
{
    if (bitmap == NULL){
        ALOGE("ToElBitmap() INVALID params");
        return FALSE;
    }

    *bitmap = NULL;

    if (jbitmap == NULL){
        ALOGW("ToElBitmap() input jbitmap null");
        return TRUE;
    }

    jclass parcelClass = env->FindClass("android/os/Parcel");
    CheckErrorAndLog(env, "FindClass: Parcel : %d!\n", __LINE__);

    jmethodID m = env->GetStaticMethodID(parcelClass, "obtain", "()Landroid/os/Parcel;");
    CheckErrorAndLog(env, "GetMethodID: obtain : %d!\n", __LINE__);

    jobject jparcel = env->CallStaticObjectMethod(parcelClass, m);
    CheckErrorAndLog(env, "CallStaticObjectMethod: obtain : %d!\n", __LINE__);

    jclass bitmapClass = env->FindClass("android/graphics/Bitmap");
    CheckErrorAndLog(env, "ToElBitmap FindClass: Bitmap : %d!\n", __LINE__);

    m = env->GetMethodID(bitmapClass, "writeToParcel", "(Landroid/os/Parcel;I)V");
    CheckErrorAndLog(env, "GetMethodID: writeToParcel : %d!\n", __LINE__);

    env->CallVoidMethod(jbitmap, m, jparcel, 0);
    CheckErrorAndLog(env, "CallVoidMethod: writeToParcel : %d!\n", __LINE__);

    Int64 nativePtr = GetJavalongField(env, parcelClass, jparcel, "mNativePtr", "ToElBitmap");
    android::Parcel* source = reinterpret_cast< android::Parcel*>(nativePtr);

    AutoPtr<IParcel> parcel;
    CParcel::New((IParcel**)&parcel);

    android::Parcel* dest;
    parcel->GetElementPayload((Handle32*)&dest);

    dest->appendFrom(source, 0, source->dataSize());
    dest->setDataPosition(0);

    CBitmap::New(bitmap);
    IParcelable::Probe(*bitmap)->ReadFromParcel(parcel);

    env->DeleteLocalRef(parcelClass);
    env->DeleteLocalRef(jparcel);
    env->DeleteLocalRef(bitmapClass);

    return TRUE;
}

jobject ElUtil::GetJavaBitmap(
    /* [in] */ JNIEnv* env,
    /* [in] */ IBitmap* bitmap)
{
    if (env == NULL || bitmap == NULL){
        ALOGE("GetJavaBitmap() INVALID params");
        return NULL;
    }

    AutoPtr<IParcel> parcel;
    CParcel::New((IParcel**)&parcel);

    IParcelable::Probe(bitmap)->ReadFromParcel(parcel);
    parcel->SetDataPosition(0);
    Handle32 source;
    parcel->GetElementPayload(&source);

    jclass parcelClass = env->FindClass("android/os/Parcel");
    CheckErrorAndLog(env, "FindClass: Parcel : %d!\n", __LINE__);

    jmethodID m = env->GetStaticMethodID(parcelClass, "obtain", "(J)Landroid/os/Parcel;");
    CheckErrorAndLog(env, "GetMethodID: obtain : %d!\n", __LINE__);

    jobject jparcel = env->CallStaticObjectMethod(parcelClass, m, (Int64)source);
    CheckErrorAndLog(env, "CallStaticObjectMethod: obtain : %d!\n", __LINE__);

    jclass bitmapClass = env->FindClass("android/graphics/Bitmap");
    CheckErrorAndLog(env, "GetJavaBitmap FindClass: Bitmap : %d!\n", __LINE__);

    m = env->GetStaticMethodID(bitmapClass, "nativeCreateFromParcel", "(Landroid/os/Parcel;)Landroid/graphics/Bitmap;");
    CheckErrorAndLog(env, "GetMethodID: nativeCreateFromParcel : %d!\n", __LINE__);

    jobject jbitmap = env->CallStaticObjectMethod(bitmapClass, m, jparcel);
    CheckErrorAndLog(env, "CallStaticObjectMethod: nativeCreateFromParcel : %d!\n", __LINE__);

    env->DeleteLocalRef(parcelClass);
    env->DeleteLocalRef(jparcel);
    env->DeleteLocalRef(bitmapClass);

    return jbitmap;
}

jobject ElUtil::GetJavaApplicationInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IApplicationInfo* appInfo)
{
    if (env == NULL || appInfo == NULL) {
        ALOGD("GetJavaApplicationInfo Invalid arguments!");
        return NULL;
    }

    jobject jAppInfo = NULL;

    jclass appInfoKlass = env->FindClass("android/content/pm/ApplicationInfo");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail FindClass: ApplicationInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(appInfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetMethodID: appInfoKlass : %d!\n", __LINE__);

    jAppInfo = env->NewObject(appInfoKlass, m);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail NewObject: appInfoKlass : %d!\n", __LINE__);

    String taskAffinity;
    appInfo->GetTaskAffinity(&taskAffinity);
    jstring jtaskAffinity = GetJavaString(env, taskAffinity);
    if (jtaskAffinity != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "taskAffinity", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: taskAffinity : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jtaskAffinity);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jtaskAffinity : %d!\n", __LINE__);
        env->DeleteLocalRef(jtaskAffinity);
    }

    String permission;
    appInfo->GetPermission(&permission);
    jstring jpermission = GetJavaString(env, permission);
    if (jpermission != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "permission", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: permission : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jpermission);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jpermission : %d!\n", __LINE__);
        env->DeleteLocalRef(jpermission);
    }

    String processName;
    appInfo->GetProcessName(&processName);
    jstring jprocessName = GetJavaString(env, processName);
    if (jprocessName != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "processName", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: processName : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jprocessName);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jprocessName : %d!\n", __LINE__);
        env->DeleteLocalRef(jprocessName);
    }

    String className;
    appInfo->GetClassName(&className);
    jstring jclassName = GetJavaString(env, className);
    if (jclassName != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "className", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: className : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jclassName);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jclassName : %d!\n", __LINE__);
        env->DeleteLocalRef(jclassName);
    }

    Int32 tempInt;
    appInfo->GetTheme(&tempInt);
    jfieldID f = env->GetFieldID(appInfoKlass, "theme", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: theme : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: theme : %d!\n", __LINE__);

    appInfo->GetFlags(&tempInt);
    f = env->GetFieldID(appInfoKlass, "flags", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: flags : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: flags : %d!\n", __LINE__);

    appInfo->GetRequiresSmallestWidthDp(&tempInt);
    f = env->GetFieldID(appInfoKlass, "requiresSmallestWidthDp", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: requiresSmallestWidthDp : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: requiresSmallestWidthDp : %d!\n", __LINE__);

    appInfo->GetCompatibleWidthLimitDp(&tempInt);
    f = env->GetFieldID(appInfoKlass, "compatibleWidthLimitDp", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: compatibleWidthLimitDp : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: compatibleWidthLimitDp : %d!\n", __LINE__);

    appInfo->GetCompatibleWidthLimitDp(&tempInt);
    f = env->GetFieldID(appInfoKlass, "compatibleWidthLimitDp", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: compatibleWidthLimitDp : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: compatibleWidthLimitDp : %d!\n", __LINE__);

    appInfo->GetLargestWidthLimitDp(&tempInt);
    f = env->GetFieldID(appInfoKlass, "largestWidthLimitDp", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: largestWidthLimitDp : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: largestWidthLimitDp : %d!\n", __LINE__);

    String scanSourceDir;
    appInfo->GetScanSourceDir(&scanSourceDir);
    jstring jscanSourceDir = GetJavaString(env, scanSourceDir);
    if (jscanSourceDir != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "scanSourceDir", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: scanSourceDir %d", __LINE__);

        env->SetObjectField(jAppInfo, f, jscanSourceDir);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jscanSourceDir %d", __LINE__);
        env->DeleteLocalRef(jscanSourceDir);
    }

    String scanPublicSourceDir;
    appInfo->GetScanPublicSourceDir(&scanPublicSourceDir);
    jstring jscanPublicSourceDir = GetJavaString(env, scanPublicSourceDir);
    if (jscanPublicSourceDir != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "scanPublicSourceDir", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: scanPublicSourceDir %d", __LINE__);

        env->SetObjectField(jAppInfo, f, jscanPublicSourceDir);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jscanPublicSourceDir %d", __LINE__);
        env->DeleteLocalRef(jscanPublicSourceDir);
    }

    String sourceDir;
    appInfo->GetSourceDir(&sourceDir);
    jstring jsourceDir = GetJavaString(env, sourceDir);
    if (jsourceDir != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "sourceDir", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: sourceDir : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jsourceDir);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jsourceDir : %d!\n", __LINE__);
        env->DeleteLocalRef(jsourceDir);
    }

    String publicSourceDir;
    appInfo->GetPublicSourceDir(&publicSourceDir);
    jstring jpublicSourceDir = GetJavaString(env, publicSourceDir);
    if (jpublicSourceDir != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "publicSourceDir", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: publicSourceDir : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jpublicSourceDir);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jpublicSourceDir : %d!\n", __LINE__);
        env->DeleteLocalRef(jpublicSourceDir);
    }

    AutoPtr<ArrayOf<String> > splitSourceDirs;
    appInfo->GetSplitSourceDirs((ArrayOf<String>**)&splitSourceDirs);
    if (splitSourceDirs != NULL) {
        jobjectArray jsplitSourceDirs = GetJavaStringArray(env, splitSourceDirs);
        if (jsplitSourceDirs != NULL) {
            jfieldID f = env->GetFieldID(appInfoKlass, "splitSourceDirs", "[Ljava/lang/String;");
            CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: splitSourceDirs %d", __LINE__);

            env->SetObjectField(jAppInfo, f, jsplitSourceDirs);
            CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jsplitSourceDirs %d", __LINE__);
            env->DeleteLocalRef(jsplitSourceDirs);
        }
    }

    AutoPtr<ArrayOf<String> > splitPublicSourceDirs;
    appInfo->GetSplitPublicSourceDirs((ArrayOf<String>**)&splitPublicSourceDirs);
    if (splitPublicSourceDirs != NULL) {
        jobjectArray jsplitPublicSourceDirs = GetJavaStringArray(env, splitPublicSourceDirs);
        if (jsplitPublicSourceDirs != NULL) {
            jfieldID f = env->GetFieldID(appInfoKlass, "splitPublicSourceDirs", "[Ljava/lang/String;");
            CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: splitPublicSourceDirs %d", __LINE__);

            env->SetObjectField(jAppInfo, f, jsplitPublicSourceDirs);
            CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jsplitPublicSourceDirs %d", __LINE__);
            env->DeleteLocalRef(jsplitPublicSourceDirs);
        }
    }

    String nativeLibraryDir;
    appInfo->GetNativeLibraryDir(&nativeLibraryDir);
    jstring jnativeLibraryDir = GetJavaString(env, nativeLibraryDir);
    if (jnativeLibraryDir != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "nativeLibraryDir", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: nativeLibraryDir : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jnativeLibraryDir);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetObjectField: jnativeLibraryDir : %d!\n", __LINE__);
        env->DeleteLocalRef(jnativeLibraryDir);
    }

    String secondaryNativeLibraryDir;
    appInfo->GetSecondaryNativeLibraryDir(&secondaryNativeLibraryDir);
    jstring jsecondaryNativeLibraryDir = GetJavaString(env, secondaryNativeLibraryDir);
    if (jsecondaryNativeLibraryDir != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "secondaryNativeLibraryDir", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: secondaryNativeLibraryDir %d", __LINE__);

        env->SetObjectField(jAppInfo, f, jsecondaryNativeLibraryDir);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jsecondaryNativeLibraryDir %d", __LINE__);
        env->DeleteLocalRef(jsecondaryNativeLibraryDir);
    }

    String nativeLibraryRootDir;
    appInfo->GetNativeLibraryRootDir(&nativeLibraryRootDir);
    jstring jnativeLibraryRootDir = GetJavaString(env, nativeLibraryRootDir);
    if (jnativeLibraryRootDir != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "nativeLibraryRootDir", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: nativeLibraryRootDir %d", __LINE__);

        env->SetObjectField(jAppInfo, f, jnativeLibraryRootDir);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jnativeLibraryRootDir %d", __LINE__);
        env->DeleteLocalRef(jnativeLibraryRootDir);
    }

    Boolean nativeLibraryRootRequiresIsa;
    appInfo->GetNativeLibraryRootRequiresIsa(&nativeLibraryRootRequiresIsa);
    f = env->GetFieldID(appInfoKlass, "nativeLibraryRootRequiresIsa", "Z");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: nativeLibraryRootRequiresIsa %d", __LINE__);

    env->SetBooleanField(jAppInfo, f, nativeLibraryRootRequiresIsa);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: nativeLibraryRootRequiresIsa %d", __LINE__);

    String primaryCpuAbi;
    appInfo->GetPrimaryCpuAbi(&primaryCpuAbi);
    jstring jprimaryCpuAbi = GetJavaString(env, primaryCpuAbi);
    if (jprimaryCpuAbi != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "primaryCpuAbi", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: primaryCpuAbi %d", __LINE__);

        env->SetObjectField(jAppInfo, f, jprimaryCpuAbi);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jprimaryCpuAbi %d", __LINE__);
        env->DeleteLocalRef(jprimaryCpuAbi);
    }

    String secondaryCpuAbi;
    appInfo->GetSecondaryCpuAbi(&secondaryCpuAbi);
    jstring jsecondaryCpuAbi = GetJavaString(env, secondaryCpuAbi);
    if (jsecondaryCpuAbi != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "secondaryCpuAbi", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: secondaryCpuAbi %d", __LINE__);

        env->SetObjectField(jAppInfo, f, jsecondaryCpuAbi);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jsecondaryCpuAbi %d", __LINE__);
        env->DeleteLocalRef(jsecondaryCpuAbi);
    }

    AutoPtr<ArrayOf<String> > resourceDirs;
    appInfo->GetResourceDirs((ArrayOf<String>**)&resourceDirs);
    if (resourceDirs != NULL) {
        jobjectArray jresourceDirs = GetJavaStringArray(env, resourceDirs);
        if (jresourceDirs != NULL) {
            jfieldID f = env->GetFieldID(appInfoKlass, "resourceDirs", "[Ljava/lang/String;");
            CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: resourceDirs : %d!\n", __LINE__);

            env->SetObjectField(jAppInfo, f, jresourceDirs);
            CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetObjectField: jresourceDirs : %d!\n", __LINE__);
            env->DeleteLocalRef(jresourceDirs);
        }
    }

    String seinfo;
    appInfo->GetSeinfo(&seinfo);
    jstring jseinfo = GetJavaString(env, seinfo);
    if (jseinfo != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "seinfo", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: seinfo %d", __LINE__);

        env->SetObjectField(jAppInfo, f, jseinfo);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jseinfo %d", __LINE__);
        env->DeleteLocalRef(jseinfo);
    }

    AutoPtr<ArrayOf<String> > sharedLibraryFiles;
    appInfo->GetSharedLibraryFiles((ArrayOf<String>**)&sharedLibraryFiles);
    if (sharedLibraryFiles != NULL) {
        jobjectArray jsharedLibraryFiles = GetJavaStringArray(env, sharedLibraryFiles);
        if (jsharedLibraryFiles != NULL) {
            jfieldID f = env->GetFieldID(appInfoKlass, "sharedLibraryFiles", "[Ljava/lang/String;");
            CheckErrorAndLog(env, "GetJavaApplicationInfo GetFieldID: sharedLibraryFiles : %d!\n", __LINE__);

            env->SetObjectField(jAppInfo, f, jsharedLibraryFiles);
            CheckErrorAndLog(env, "GetJavaApplicationInfo SetObjectField: jsharedLibraryFiles : %d!\n", __LINE__);
            env->DeleteLocalRef(jsharedLibraryFiles);
        }
    }

    String dataDir;
    appInfo->GetDataDir(&dataDir);
    jstring jdataDir = GetJavaString(env, dataDir);
    if (jdataDir != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "dataDir", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: dataDir : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jdataDir);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetObjectField: jdataDir : %d!\n", __LINE__);
        env->DeleteLocalRef(jdataDir);
    }

    appInfo->GetUid(&tempInt);
    f = env->GetFieldID(appInfoKlass, "uid", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: uid : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: uid : %d!\n", __LINE__);

    appInfo->GetTargetSdkVersion(&tempInt);
    f = env->GetFieldID(appInfoKlass, "targetSdkVersion", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: targetSdkVersion : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: targetSdkVersion : %d!\n", __LINE__);

    appInfo->GetVersionCode(&tempInt);
    f = env->GetFieldID(appInfoKlass, "versionCode", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: versionCode %d", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: versionCode %d", __LINE__);

    Boolean enabled;
    appInfo->GetEnabled(&enabled);
    f = env->GetFieldID(appInfoKlass, "enabled", "Z");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: enabled : %d!\n", __LINE__);

    env->SetBooleanField(jAppInfo, f, enabled);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: enabled : %d!\n", __LINE__);

    appInfo->GetEnabledSetting(&tempInt);
    f = env->GetFieldID(appInfoKlass, "enabledSetting", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: enabledSetting : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: enabledSetting : %d!\n", __LINE__);

    appInfo->GetInstallLocation(&tempInt);
    f = env->GetFieldID(appInfoKlass, "installLocation", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: installLocation : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: installLocation : %d!\n", __LINE__);

    String manageSpaceActivityName;
    appInfo->GetManageSpaceActivityName(&manageSpaceActivityName);
    jstring jmanageSpaceActivityName = GetJavaString(env, manageSpaceActivityName);
    if (jmanageSpaceActivityName != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "manageSpaceActivityName", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: manageSpaceActivityName : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jmanageSpaceActivityName);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jmanageSpaceActivityName : %d!\n", __LINE__);
        env->DeleteLocalRef(jmanageSpaceActivityName);
    }

    appInfo->GetDescriptionRes(&tempInt);
    f = env->GetFieldID(appInfoKlass, "descriptionRes", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: descriptionRes : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: descriptionRes : %d!\n", __LINE__);

    appInfo->GetUiOptions(&tempInt);
    f = env->GetFieldID(appInfoKlass, "uiOptions", "I");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: uiOptions : %d!\n", __LINE__);

    env->SetIntField(jAppInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: uiOptions : %d!\n", __LINE__);

    String backupAgentName;
    appInfo->GetBackupAgentName(&backupAgentName);
    jstring jbackupAgentName = GetJavaString(env, backupAgentName);
    if (jbackupAgentName != NULL) {
        jfieldID f = env->GetFieldID(appInfoKlass, "backupAgentName", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: backupAgentName : %d!\n", __LINE__);

        env->SetObjectField(jAppInfo, f, jbackupAgentName);
        CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: jbackupAgentName : %d!\n", __LINE__);
        env->DeleteLocalRef(jbackupAgentName);
    }

    Boolean protect;
    appInfo->GetProtect(&protect);
    f = env->GetFieldID(appInfoKlass, "protect", "Z");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: protect %d", __LINE__);

    env->SetBooleanField(jAppInfo, f, protect);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: protect %d", __LINE__);

    Boolean isThemeable;
    appInfo->GetIsThemeable(&isThemeable);
    f = env->GetFieldID(appInfoKlass, "isThemeable", "Z");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetFieldID: isThemeable %d", __LINE__);

    env->SetBooleanField(jAppInfo, f, isThemeable);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail SetIntField: isThemeable %d", __LINE__);

    AutoPtr<IPackageItemInfo> pkgInfo = IPackageItemInfo::Probe(appInfo);
    SetJavaPackageItemInfo(env, appInfoKlass, jAppInfo, pkgInfo);

    env->DeleteLocalRef(appInfoKlass);

    return jAppInfo;
}

jobject ElUtil::GetJavaPackageInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IPackageInfo* pkgInfo)
{
    if (env == NULL || pkgInfo == NULL) {
        ALOGD("GetJavaPackageInfo: Invalid argumenet!");
        return NULL;
    }

    jobject jpkgInfo = NULL;

    jclass pkgInfoKlass = env->FindClass("android/content/pm/PackageInfo");
    CheckErrorAndLog(env, "FindClass: PackageInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(pkgInfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetFieldID: PackageInfo : %d!\n", __LINE__);

    jpkgInfo = env->NewObject(pkgInfoKlass, m);
    CheckErrorAndLog(env, "NewObject: PackageInfo : %d!\n", __LINE__);

    String packageName;
    pkgInfo->GetPackageName(&packageName);
    if (!packageName.IsNull()) {
        jstring jpackageName = GetJavaString(env, packageName);

        jfieldID f = env->GetFieldID(pkgInfoKlass, "packageName", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetFieldID: packageName : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jpackageName);
        CheckErrorAndLog(env, "SetObjectField: jpackageName : %d!\n", __LINE__);
        env->DeleteLocalRef(jpackageName);
    }

    AutoPtr<ArrayOf<String> > splitNames;
    pkgInfo->GetSplitNames((ArrayOf<String>**)&splitNames);
    if (splitNames != NULL) {
        jobjectArray jsplitNames = GetJavaStringArray(env, splitNames);
        if (jsplitNames != NULL) {
            jfieldID f = env->GetFieldID(pkgInfoKlass, "splitNames", "[Ljava/lang/String;");
            CheckErrorAndLog(env, "GetJavaPackageInfo GetFieldID: splitNames : %d!\n", __LINE__);

            env->SetObjectField(jpkgInfo, f, jsplitNames);
            CheckErrorAndLog(env, "GetJavaPackageInfo SetObjectField: jsplitNames : %d!\n", __LINE__);
            env->DeleteLocalRef(jsplitNames);
        }
    }

    Int32 tmpInt;
    pkgInfo->GetVersionCode(&tmpInt);
    jfieldID f = env->GetFieldID(pkgInfoKlass, "versionCode", "I");
    CheckErrorAndLog(env, "GetFieldID: versionCode : %d!\n", __LINE__);

    env->SetIntField(jpkgInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: versionCode : %d!\n", __LINE__);

    String tmpString;
    pkgInfo->GetVersionName(&tmpString);
    if (!tmpString.IsNull()) {
        jstring jversionName = GetJavaString(env, tmpString);

        jfieldID f = env->GetFieldID(pkgInfoKlass, "versionName", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetFieldID: versionName : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jversionName);
        CheckErrorAndLog(env, "SetObjectField: jversionName : %d!\n", __LINE__);
        env->DeleteLocalRef(jversionName);
    }

    pkgInfo->GetSharedUserId(&tmpString);
    if (!tmpString.IsNull()) {
        jstring jsharedUserId = GetJavaString(env, tmpString);

        jfieldID f = env->GetFieldID(pkgInfoKlass, "sharedUserId", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetFieldID: sharedUserId : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jsharedUserId);
        CheckErrorAndLog(env, "SetObjectField: jsharedUserId : %d!\n", __LINE__);
        env->DeleteLocalRef(jsharedUserId);
    }

    pkgInfo->GetSharedUserLabel(&tmpInt);
    f = env->GetFieldID(pkgInfoKlass, "sharedUserLabel", "I");
    CheckErrorAndLog(env, "GetFieldID: sharedUserLabel : %d!\n", __LINE__);

    env->SetIntField(jpkgInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: sharedUserLabel : %d!\n", __LINE__);

    AutoPtr<IApplicationInfo> appInfo;
    pkgInfo->GetApplicationInfo((IApplicationInfo**)&appInfo);
    if (appInfo != NULL) {
        jobject jAppInfo = GetJavaApplicationInfo(env, appInfo);

        jfieldID f = env->GetFieldID(pkgInfoKlass, "applicationInfo", "Landroid/content/pm/ApplicationInfo;");
        CheckErrorAndLog(env, "GetFieldID: ApplicationInfo : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jAppInfo);
        CheckErrorAndLog(env, "SetObjectField: jAppInfo : %d!\n", __LINE__);

        env->DeleteLocalRef(jAppInfo);
    }

    Int64 tmpLong;
    pkgInfo->GetFirstInstallTime(&tmpLong);
    f = env->GetFieldID(pkgInfoKlass, "firstInstallTime", "J");
    CheckErrorAndLog(env, "GetFieldID: firstInstallTime : %d!\n", __LINE__);

    env->SetLongField(jpkgInfo, f, (jlong)tmpLong);
    CheckErrorAndLog(env, "SetIntField: firstInstallTime : %d!\n", __LINE__);

    pkgInfo->GetLastUpdateTime(&tmpLong);
    f = env->GetFieldID(pkgInfoKlass, "lastUpdateTime", "J");
    CheckErrorAndLog(env, "GetFieldID: lastUpdateTime : %d!\n", __LINE__);

    env->SetLongField(jpkgInfo, f, (jlong)tmpLong);
    CheckErrorAndLog(env, "SetIntField: lastUpdateTime : %d!\n", __LINE__);

    AutoPtr<ArrayOf<Int32> > gids;
    pkgInfo->GetGids((ArrayOf<Int32>**)&gids);
    if (gids != NULL) {
        jintArray jgids = GetJavaIntArray(env, gids);
        f = env->GetFieldID(pkgInfoKlass, "gids", "[I");
        CheckErrorAndLog(env, "GetFieldID: gids : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jgids);
        CheckErrorAndLog(env, "SetIntField: jgids : %d!\n", __LINE__);

        env->DeleteLocalRef(jgids);
    }

    AutoPtr<ArrayOf<IActivityInfo*> > activities;
    pkgInfo->GetActivities((ArrayOf<IActivityInfo*>**)&activities);
    if (activities != NULL) {
        jclass ainfoKlass = env->FindClass("android/content/pm/ActivityInfo");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail FindClass: ActivityInfo : %d!\n", __LINE__);

        Int32 count = activities->GetLength();
        jobjectArray jactivities = env->NewObjectArray((jsize)count, ainfoKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail NewObjectArray: ActivityInfo : %d!\n", __LINE__);
        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IActivityInfo> info = (*activities)[i];

            jobject jinfo = GetJavaActivityInfo(env, info);

            env->SetObjectArrayElement(jactivities, i, jinfo);
            CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectArrayElement: ActivityInfo : %d!\n", __LINE__);

            env->DeleteLocalRef(jinfo);
        }

        f = env->GetFieldID(pkgInfoKlass, "activities", "[Landroid/content/pm/ActivityInfo;");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetFieldID: activities[] : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jactivities);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectField: jpmerArray : %d!\n", __LINE__);

        env->DeleteLocalRef(jactivities);
        env->DeleteLocalRef(ainfoKlass);
    }

    AutoPtr<ArrayOf<IActivityInfo*> > receivers;
    pkgInfo->GetReceivers((ArrayOf<IActivityInfo*>**)&receivers);
    if (receivers != NULL) {
        jclass recvKlass = env->FindClass("android/content/pm/ActivityInfo");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo FindClass: ActivityInfo : %d!\n", __LINE__);

        Int32 count = receivers->GetLength();
        jobjectArray jreceivers = env->NewObjectArray((jsize)count, recvKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail NewObjectArray: ActivityInfo : %d!\n", __LINE__);
        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IActivityInfo> receiver = (*receivers)[i];

            jobject jreceiver = GetJavaActivityInfo(env, receiver);

            env->SetObjectArrayElement(jreceivers, i, jreceiver);
            CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectArrayElement: ActivityInfo : %d!\n", __LINE__);

            env->DeleteLocalRef(jreceiver);
        }

        f = env->GetFieldID(pkgInfoKlass, "receivers", "[Landroid/content/pm/ActivityInfo;");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetFieldID: receivers[] : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jreceivers);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectField: jreceivers : %d!\n", __LINE__);

        env->DeleteLocalRef(jreceivers);
        env->DeleteLocalRef(recvKlass);
    }

    AutoPtr<ArrayOf<IServiceInfo*> > services;
    pkgInfo->GetServices((ArrayOf<IServiceInfo*>**)&services);
    if (services != NULL) {
        jclass serKlass = env->FindClass("android/content/pm/ServiceInfo");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo FindClass: ServiceInfo : %d!\n", __LINE__);

        Int32 count = services->GetLength();
        jobjectArray jservices = env->NewObjectArray((jsize)count, serKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo NewObjectArray: ServiceInfo : %d!\n", __LINE__);
        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IServiceInfo> service = (*services)[i];

            jobject jservice = ElUtil::GetJavaServiceInfo(env, service);

            env->SetObjectArrayElement(jservices, i, jservice);
            CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectArrayElement: ServiceInfo : %d!\n", __LINE__);

            env->DeleteLocalRef(jservice);
        }

        f = env->GetFieldID(pkgInfoKlass, "services", "[Landroid/content/pm/ServiceInfo;");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetFieldID: services[] : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jservices);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectField: jservices : %d!\n", __LINE__);

        env->DeleteLocalRef(jservices);
        env->DeleteLocalRef(serKlass);
    }

    AutoPtr<ArrayOf<IProviderInfo*> > providers;
    pkgInfo->GetProviders((ArrayOf<IProviderInfo*>**)&providers);
    if (providers != NULL) {
        jclass provKlass = env->FindClass("android/content/pm/ProviderInfo");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo FindClass: ProviderInfo : %d!\n", __LINE__);

        Int32 count = providers->GetLength();
        jobjectArray jproviders = env->NewObjectArray((jsize)count, provKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo NewObjectArray: ProviderInfo : %d!\n", __LINE__);
        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IProviderInfo> provider = (*providers)[i];

            jobject jprovider = ElUtil::GetJavaProviderInfo(env, provider);

            env->SetObjectArrayElement(jproviders, i, jprovider);
            CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectArrayElement: ProviderInfo : %d!\n", __LINE__);

            env->DeleteLocalRef(jprovider);
        }

        f = env->GetFieldID(pkgInfoKlass, "providers", "[Landroid/content/pm/ProviderInfo;");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetFieldID: providers[] : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jproviders);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectField: jproviders : %d!\n", __LINE__);

        env->DeleteLocalRef(jproviders);
        env->DeleteLocalRef(provKlass);
    }

    AutoPtr<ArrayOf<IInstrumentationInfo*> > instrumentations;
    pkgInfo->GetInstrumentation((ArrayOf<IInstrumentationInfo*>**)&instrumentations);
    if (instrumentations != NULL) {
        jclass iiKlass = env->FindClass("android/content/pm/InstrumentationInfo");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo FindClass: InstrumentationInfo : %d!\n", __LINE__);

        Int32 count = instrumentations->GetLength();
        jobjectArray jinstrumentations = env->NewObjectArray((jsize)count, iiKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo NewObjectArray: InstrumentationInfo : %d!\n", __LINE__);
        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IInstrumentationInfo> instrumentation = (*instrumentations)[i];

            jobject jinstrumentation = ElUtil::GetJavaInstrumentationInfo(env, instrumentation);

            env->SetObjectArrayElement(jinstrumentations, i, jinstrumentation);
            CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectArrayElement: InstrumentationInfo : %d!\n", __LINE__);

            env->DeleteLocalRef(jinstrumentation);
        }

        f = env->GetFieldID(pkgInfoKlass, "instrumentation", "[Landroid/content/pm/InstrumentationInfo;");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetFieldID: instrumentation[] : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jinstrumentations);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectField: jinstrumentations : %d!\n", __LINE__);

        env->DeleteLocalRef(jinstrumentations);
        env->DeleteLocalRef(iiKlass);
    }

    AutoPtr<ArrayOf<IPermissionInfo*> > permissions;
    pkgInfo->GetPermissions((ArrayOf<IPermissionInfo*>**)&permissions);
    if (permissions != NULL) {
        jclass perinfoKlass = env->FindClass("android/content/pm/PermissionInfo");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo FindClass: PermissionInfo : %d!\n", __LINE__);

        Int32 count = permissions->GetLength();
        jobjectArray jpermissions = env->NewObjectArray((jsize)count, perinfoKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail NewObjectArray: PermissionInfo : %d!\n", __LINE__);
        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IPermissionInfo> permission = (*permissions)[i];

            jobject jpermission = ElUtil::GetJavaPermissionInfo(env, permission);

            env->SetObjectArrayElement(jpermissions, i, jpermission);
            CheckErrorAndLog(env, "GetJavaPackageInfo SetObjectArrayElement: PermissionInfo : %d!\n", __LINE__);

            env->DeleteLocalRef(jpermission);
        }

        f = env->GetFieldID(pkgInfoKlass, "permissions", "[Landroid/content/pm/PermissionInfo;");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetFieldID: permissions[] : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jpermissions);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectField: jpermissions : %d!\n", __LINE__);

        env->DeleteLocalRef(jpermissions);
        env->DeleteLocalRef(perinfoKlass);
    }

    AutoPtr<ArrayOf<String> > requestedPermissions;
    pkgInfo->GetRequestedPermissions((ArrayOf<String>**)&requestedPermissions);
    if (requestedPermissions != NULL) {
        jobjectArray jrequestedPermissions = ElUtil::GetJavaStringArray(env, requestedPermissions);
        if (jrequestedPermissions != NULL) {
            f = env->GetFieldID(pkgInfoKlass, "requestedPermissions", "[Ljava/lang/String;");
            CheckErrorAndLog(env, "GetFieldID: requestedPermissions : %d!\n", __LINE__);

            env->SetObjectField(jpkgInfo, f, jrequestedPermissions);
            CheckErrorAndLog(env, "SetObjectField: requestedPermissions : %d!\n", __LINE__);

            env->DeleteLocalRef(jrequestedPermissions);
        }
        else {
            ALOGE("GetJavaPackageInfo: jrequestedPermissions is NULL!");
        }

    }

    AutoPtr<ArrayOf<Int32> > requestedPermissionsFlags;
    pkgInfo->GetRequestedPermissionsFlags((ArrayOf<Int32>**)&requestedPermissionsFlags);
    if (requestedPermissionsFlags != NULL) {
        jintArray jrequestedPermissionsFlags = ElUtil::GetJavaIntArray(env, requestedPermissionsFlags);
        if (jrequestedPermissionsFlags != NULL) {
            f = env->GetFieldID(pkgInfoKlass, "requestedPermissionsFlags", "[I");
            CheckErrorAndLog(env, "GetFieldID: requestedPermissionsFlags : %d!\n", __LINE__);

            env->SetObjectField(jpkgInfo, f, jrequestedPermissionsFlags);
            CheckErrorAndLog(env, "SetObjectField: requestedPermissionsFlags : %d!\n", __LINE__);

            env->DeleteLocalRef(jrequestedPermissionsFlags);
        }
        else {
            ALOGE("GetJavaPackageInfo: jrequestedPermissionsFlags is NULL!");
        }
    }

    AutoPtr<ArrayOf<ISignature*> > signatures;
    pkgInfo->GetSignatures((ArrayOf<ISignature*>**)&signatures);
    if (signatures != NULL) {
        ALOGD("GetJavaPackageInfo: signatures not NULL!");
        jclass sKlass = env->FindClass("android/content/pm/Signature");
        CheckErrorAndLog(env, "FindClass: Signature : %d!\n", __LINE__);

        m = env->GetMethodID(sKlass, "<init>", "([B)V");
        CheckErrorAndLog(env, "GetFieldID: Signature : %d!\n", __LINE__);

        jobjectArray jsignatures = env->NewObjectArray(signatures->GetLength(), sKlass, 0);
        CheckErrorAndLog(env, "NewObjectArray: Signature : %d!\n", __LINE__);

        for (Int32 i = 0; i < signatures->GetLength(); i++) {
            AutoPtr<ArrayOf<Byte> > bArray;
            (*signatures)[i]->ToByteArray((ArrayOf<Byte>**)&bArray);
            jbyteArray jbytearray = GetJavaByteArray(env, bArray);

            jobject jsignature = env->NewObject(sKlass, m, jbytearray);
            CheckErrorAndLog(env, "NewObject: Signature : %d!\n", __LINE__);

            env->SetObjectArrayElement(jsignatures, i, jsignature);
            CheckErrorAndLog(env, "SetObjectArrayElement: Signature : %d!\n", __LINE__);

            env->DeleteLocalRef(jbytearray);
            env->DeleteLocalRef(jsignature);
        }

        f = env->GetFieldID(pkgInfoKlass, "signatures", "[Landroid/content/pm/Signature;");
        CheckErrorAndLog(env, "GetFieldID: signatures : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jsignatures);
        CheckErrorAndLog(env, "SetObjectField: signatures : %d!\n", __LINE__);

        env->DeleteLocalRef(sKlass);
        env->DeleteLocalRef(jsignatures);
    }
    else {
        // TODO: need delete
        // jclass sKlass = env->FindClass("android/content/pm/Signature");
        // CheckErrorAndLog(env, "FindClass: Signature : %d!\n", __LINE__);

        // m = env->GetMethodID(sKlass, "<init>", "([B)V");
        // CheckErrorAndLog(env, "GetFieldID: Signature : %d!\n", __LINE__);

        // jbyteArray jbytearray = env->NewByteArray(0);
        // CheckErrorAndLog(env, "NewByteArray: %d!\n", __LINE__);

        // jobjectArray jsignatures = env->NewObjectArray(1, sKlass, 0);
        // CheckErrorAndLog(env, "NewObjectArray: Signature : %d!\n", __LINE__);

        // // jobject jsignature = env->NewObject(sKlass, m, jbytearray);
        // // CheckErrorAndLog(env, "NewObject: Signature : %d!\n", __LINE__);

        // // env->SetObjectArrayElement(jsignatures, 0, jsignature);
        // // CheckErrorAndLog(env, "SetObjectArrayElement: Signature : %d!\n", __LINE__);

        // // f = env->GetFieldID(pkgInfoKlass, "signatures", "[Landroid/content/pm/Signature;");
        // // CheckErrorAndLog(env, "GetFieldID: signatures : %d!\n", __LINE__);

        // // env->SetObjectField(jpkgInfo, f, jsignatures);
        // // CheckErrorAndLog(env, "SetObjectField: signatures : %d!\n", __LINE__);

        // jclass eppKlass = env->FindClass("android/content/pm/ElSignature");
        // CheckErrorAndLog(env, "FindClass: ElSignature : %d!\n", __LINE__);

        // jmethodID m = env->GetStaticMethodID(eppKlass, "getSignature", "(Ljava/lang/String;)Landroid/content/pm/Signature;");
        // CheckErrorAndLog(env, "GetMethodID: getSignature %d", __LINE__);

        // jstring jpkgPath = GetJavaString(env, packageName);

        // jobject jsignature = env->CallStaticObjectMethod(eppKlass, m, jpkgPath);
        // CheckErrorAndLog(env, "CallStaticObjectMethod() getSignature %d", __LINE__);

        // env->SetObjectArrayElement(jsignatures, 0, jsignature);
        // CheckErrorAndLog(env, "SetObjectArrayElement: Signature : %d!\n", __LINE__);

        // f = env->GetFieldID(pkgInfoKlass, "signatures", "[Landroid/content/pm/Signature;");
        // CheckErrorAndLog(env, "GetFieldID: signatures : %d!\n", __LINE__);

        // env->SetObjectField(jpkgInfo, f, jsignatures);
        // CheckErrorAndLog(env, "SetObjectField: signatures : %d!\n", __LINE__);

        // env->DeleteLocalRef(sKlass);
        // env->DeleteLocalRef(eppKlass);
        // env->DeleteLocalRef(jsignature);
        // env->DeleteLocalRef(jsignatures);
    }

    AutoPtr<ArrayOf<IConfigurationInfo*> > configPreferences;
    pkgInfo->GetConfigPreferences((ArrayOf<IConfigurationInfo*>**)&configPreferences);
    if (configPreferences != NULL) {
        Int32 count = configPreferences->GetLength();

        jclass ciKlass = env->FindClass("android/content/pm/ConfigurationInfo");
        CheckErrorAndLog(env, "FindClass: ConfigurationInfo : %d!\n", __LINE__);

        jobjectArray jconfigPreferences = env->NewObjectArray((jsize)count, ciKlass, 0);
        CheckErrorAndLog(env, "NewObjectArray: ConfigurationInfo : %d!\n", __LINE__);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jconfigPreference = ElUtil::GetJavaConfigurationInfo(env, (*configPreferences)[i]);
            if (jconfigPreference != NULL) {
                env->SetObjectArrayElement(jconfigPreferences, i, jconfigPreference);
                env->DeleteLocalRef(jconfigPreference);
            }
            else {
                ALOGE("GetJavaPackageInfo() GetJavaConfigurationInfo fail!");
            }
        }

        f = env->GetFieldID(pkgInfoKlass, "configPreferences", "[Landroid/content/pm/ConfigurationInfo;");
        CheckErrorAndLog(env, "GetFieldID: configPreferences : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jconfigPreferences);
        CheckErrorAndLog(env, "SetObjectField: configPreferences : %d!\n", __LINE__);

        env->DeleteLocalRef(ciKlass);
        env->DeleteLocalRef(jconfigPreferences);
    }

    AutoPtr<ArrayOf<IFeatureInfo*> > reqFeatures;
    pkgInfo->GetReqFeatures((ArrayOf<IFeatureInfo*>**)&reqFeatures);
    if (reqFeatures != NULL) {
        Int32 count = reqFeatures->GetLength();

        jclass fiKlass = env->FindClass("android/content/pm/FeatureInfo");
        CheckErrorAndLog(env, "FindClass: FeatureInfo : %d!\n", __LINE__);

        jobjectArray jfeatures = env->NewObjectArray((jsize)count, fiKlass, 0);
        CheckErrorAndLog(env, "NewObjectArray: FeatureInfo : %d!\n", __LINE__);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jfeature = ElUtil::GetJavaFeatureInfo(env, (*reqFeatures)[i]);
            if (jfeature != NULL) {
                env->SetObjectArrayElement(jfeatures, i, jfeature);
                env->DeleteLocalRef(jfeature);
            }
            else {
                ALOGE("GetJavaPackageInfo() GetJavaFeatureInfo fail!");
            }
        }

        f = env->GetFieldID(pkgInfoKlass, "reqFeatures", "[Landroid/content/pm/FeatureInfo;");
        CheckErrorAndLog(env, "GetFieldID: reqFeatures : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jfeatures);
        CheckErrorAndLog(env, "SetObjectField: reqFeatures : %d!\n", __LINE__);

        env->DeleteLocalRef(fiKlass);
        env->DeleteLocalRef(jfeatures);
    }

    AutoPtr<ArrayOf<IFeatureGroupInfo*> > featureGroups;
    pkgInfo->GetFeatureGroups((ArrayOf<IFeatureGroupInfo*>**)&featureGroups);
    if (featureGroups != NULL) {
        Int32 count = featureGroups->GetLength();

        jclass fgiKlass = env->FindClass("android/content/pm/FeatureGroupInfo");
        CheckErrorAndLog(env, "FindClass: FeatureGroupInfo : %d!\n", __LINE__);

        jobjectArray jfeatureGroups = env->NewObjectArray((jsize)count, fgiKlass, 0);
        CheckErrorAndLog(env, "NewObjectArray: FeatureGroupInfo : %d!\n", __LINE__);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jfeatureGroup = ElUtil::GetJavaFeatureGroupInfo(env, (*featureGroups)[i]);
            if (jfeatureGroup != NULL) {
                env->SetObjectArrayElement(jfeatureGroups, i, jfeatureGroup);
                env->DeleteLocalRef(jfeatureGroup);
            }
            else {
                ALOGE("GetJavaPackageInfo() GetJavaFeatureGroupInfo fail!");
            }
        }

        f = env->GetFieldID(pkgInfoKlass, "featureGroups", "[Landroid/content/pm/FeatureGroupInfo;");
        CheckErrorAndLog(env, "GetFieldID: featureGroups : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, f, jfeatureGroups);
        CheckErrorAndLog(env, "SetObjectField: featureGroups : %d!\n", __LINE__);

        env->DeleteLocalRef(fgiKlass);
        env->DeleteLocalRef(jfeatureGroups);
    }

    pkgInfo->GetInstallLocation(&tmpInt);
    f = env->GetFieldID(pkgInfoKlass, "installLocation", "I");
    CheckErrorAndLog(env, "GetFieldID: installLocation : %d!\n", __LINE__);

    env->SetIntField(jpkgInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: installLocation : %d!\n", __LINE__);

    Boolean coreApp;
    pkgInfo->GetCoreApp(&coreApp);
    SetJavaBoolField(env, pkgInfoKlass, jpkgInfo, coreApp, "coreApp", "GetJavaPackageInfo");

    Boolean requiredForAllUsers;
    pkgInfo->GetRequiredForAllUsers(&requiredForAllUsers);
    SetJavaBoolField(env, pkgInfoKlass, jpkgInfo, requiredForAllUsers, "requiredForAllUsers", "GetJavaPackageInfo");

    String restrictedAccountType;
    pkgInfo->GetRestrictedAccountType(&restrictedAccountType);
    SetJavaStringField(env, pkgInfoKlass, jpkgInfo, restrictedAccountType, "restrictedAccountType", "GetJavaPackageInfo");

    String requiredAccountType;
    pkgInfo->GetRequiredAccountType(&requiredAccountType);
    SetJavaStringField(env, pkgInfoKlass, jpkgInfo, requiredAccountType, "requiredAccountType", "GetJavaPackageInfo");

    String overlayTarget;
    pkgInfo->GetOverlayTarget(&overlayTarget);
    SetJavaStringField(env, pkgInfoKlass, jpkgInfo, overlayTarget, "overlayTarget", "GetJavaPackageInfo");

    /* Theme-specific. */
    Boolean isThemeApk;
    pkgInfo->GetIsThemeApk(&isThemeApk);
    SetJavaBoolField(env, pkgInfoKlass, jpkgInfo, isThemeApk, "isThemeApk", "GetJavaPackageInfo");

    AutoPtr<IArrayList> targets;
    pkgInfo->GetOverlayTargets((IArrayList**)&targets);
    if (targets != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        CheckErrorAndLog(env, "GetJavaPackageInfo Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jobject jlist = env->NewObject(listKlass, m);
        CheckErrorAndLog(env, "GetJavaPackageInfo Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetMethodID: add : %d!\n", __LINE__);

        Int32 size;
        targets->GetSize(&size);
        for(Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> item;
            targets->Get(i, (IInterface**)&item);
            String target;
            ICharSequence::Probe(item)->ToString(&target);
            jstring jtarget = GetJavaString(env, target);
            env->CallBooleanMethod(jlist, mAdd, jtarget);
            CheckErrorAndLog(env, "GetJavaPackageInfo Fail CallObjectMethod: add() : %d!\n", __LINE__);
            env->DeleteLocalRef(jtarget);
        }

        jfieldID field = env->GetFieldID(pkgInfoKlass, "mOverlayTargets", "Ljava/util/ArrayList;");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetFieldID: mOverlayTargets : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, field, jlist);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectField: mOverlayTargets : %d!\n", __LINE__);

        env->DeleteLocalRef(listKlass);
        env->DeleteLocalRef(jlist);
    }

    AutoPtr<IThemeInfo> themeInfo;
    pkgInfo->GetThemeInfo((IThemeInfo**)&themeInfo);
    if (themeInfo != NULL) {
        jobject jthemeInfo = GetJavaThemeInfo(env, themeInfo);

        jfieldID field = env->GetFieldID(pkgInfoKlass, "themeInfo", "Landroid/content/pm/ThemeInfo;");
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail GetFieldID: themeInfo : %d!\n", __LINE__);

        env->SetObjectField(jpkgInfo, field, jthemeInfo);
        ElUtil::CheckErrorAndLog(env, "GetJavaPackageInfo Fail SetObjectField: themeInfo : %d!\n", __LINE__);

        env->DeleteLocalRef(jthemeInfo);
    }

    Boolean hasIconPack;
    pkgInfo->GetHasIconPack(&hasIconPack);
    SetJavaBoolField(env, pkgInfoKlass, jpkgInfo, hasIconPack, "hasIconPack", "GetJavaPackageInfo");

    Boolean isLegacyIconPackApk;
    pkgInfo->GetIsLegacyIconPackApk(&isLegacyIconPackApk);
    SetJavaBoolField(env, pkgInfoKlass, jpkgInfo, isLegacyIconPackApk, "isLegacyIconPackApk", "GetJavaPackageInfo");

    env->DeleteLocalRef(pkgInfoKlass);

    return jpkgInfo;
}

jobject ElUtil::GetJavaThemeInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IThemeInfo* themeInfo)
{
    if (env == NULL || themeInfo == NULL) {
        ALOGD("GetJavaThemeInfo: Invalid argumenet!");
        return NULL;
    }

    jobject jthemeInfo = NULL;

    jclass themeInfoKlass = env->FindClass("android/content/pm/ThemeInfo");
    CheckErrorAndLog(env, "FindClass: ThemeInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(themeInfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetFieldID: ThemeInfo : %d!\n", __LINE__);

    jthemeInfo = env->NewObject(themeInfoKlass, m);
    CheckErrorAndLog(env, "NewObject: ThemeInfo : %d!\n", __LINE__);

    AutoPtr<IBaseThemeInfo> baseThemeInfo = IBaseThemeInfo::Probe(themeInfo);
    String themeId;
    baseThemeInfo->GetThemeId(&themeId);
    SetJavaStringField(env, themeInfoKlass, jthemeInfo, themeId, "themeId", "GetJavaThemeInfo");

    String name;
    baseThemeInfo->GetName(&name);
    SetJavaStringField(env, themeInfoKlass, jthemeInfo, name, "name", "GetJavaThemeInfo");

    String author;
    baseThemeInfo->GetAuthor(&author);
    SetJavaStringField(env, themeInfoKlass, jthemeInfo, author, "author", "GetJavaThemeInfo");

    env->DeleteLocalRef(themeInfoKlass);

    return jthemeInfo;
}

jobject ElUtil::GetJavaDisplayInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IDisplayInfo* disInfo)
{
    if (env == NULL || disInfo == NULL) {
        ALOGD("GetJavaDisplayInfo: Invalid argumenet!");
        return NULL;
    }

    jobject jdisInfo = NULL;

    jclass disInfoKlass = env->FindClass("android/view/DisplayInfo");
    CheckErrorAndLog(env, "FindClass: DisplayInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(disInfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetFieldID: DisplayInfo : %d!\n", __LINE__);

    jdisInfo = env->NewObject(disInfoKlass, m);
    CheckErrorAndLog(env, "NewObject: DisplayInfo : %d!\n", __LINE__);

    Int32 tmpInt;
    disInfo->GetLayerStack(&tmpInt);
    jfieldID f = env->GetFieldID(disInfoKlass, "layerStack", "I");
    CheckErrorAndLog(env, "GetFieldID: layerStack : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: layerStack : %d!\n", __LINE__);

    disInfo->GetFlags(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "flags", "I");
    CheckErrorAndLog(env, "GetFieldID: flags : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: flags : %d!\n", __LINE__);

    disInfo->GetType(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "type", "I");
    CheckErrorAndLog(env, "GetFieldID: type : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: type : %d!\n", __LINE__);

    String tmpString;
    disInfo->GetAddress(&tmpString);
    if (!tmpString.IsNull()) {
        jstring jaddress = GetJavaString(env, tmpString);

        f = env->GetFieldID(disInfoKlass, "address", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetFieldID: address : %d!\n", __LINE__);

        env->SetObjectField(jdisInfo, f, jaddress);
        CheckErrorAndLog(env, "SetObjectField: jaddress : %d!\n", __LINE__);
        env->DeleteLocalRef(jaddress);
    }

    disInfo->GetName(&tmpString);
    if (!tmpString.IsNull()) {
        jstring jname = GetJavaString(env, tmpString);

        f = env->GetFieldID(disInfoKlass, "name", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetFieldID: name : %d!\n", __LINE__);

        env->SetObjectField(jdisInfo, f, jname);
        CheckErrorAndLog(env, "SetObjectField: jname : %d!\n", __LINE__);
        env->DeleteLocalRef(jname);
    }

    disInfo->GetAppWidth(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "appWidth", "I");
    CheckErrorAndLog(env, "GetFieldID: appWidth : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: appWidth : %d!\n", __LINE__);

    disInfo->GetAppHeight(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "appHeight", "I");
    CheckErrorAndLog(env, "GetFieldID: appHeight : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: appHeight : %d!\n", __LINE__);

    disInfo->GetSmallestNominalAppWidth(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "smallestNominalAppWidth", "I");
    CheckErrorAndLog(env, "GetFieldID: smallestNominalAppWidth : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: smallestNominalAppWidth : %d!\n", __LINE__);

    disInfo->GetSmallestNominalAppHeight(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "smallestNominalAppHeight", "I");
    CheckErrorAndLog(env, "GetFieldID: smallestNominalAppHeight : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: smallestNominalAppHeight : %d!\n", __LINE__);

    disInfo->GetLargestNominalAppWidth(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "largestNominalAppWidth", "I");
    CheckErrorAndLog(env, "GetFieldID: largestNominalAppWidth : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: largestNominalAppWidth : %d!\n", __LINE__);

    disInfo->GetLargestNominalAppHeight(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "largestNominalAppHeight", "I");
    CheckErrorAndLog(env, "GetFieldID: largestNominalAppHeight : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: largestNominalAppHeight : %d!\n", __LINE__);

    disInfo->GetLogicalWidth(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "logicalWidth", "I");
    CheckErrorAndLog(env, "GetFieldID: logicalWidth : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: logicalWidth : %d!\n", __LINE__);

    disInfo->GetLogicalHeight(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "logicalHeight", "I");
    CheckErrorAndLog(env, "GetFieldID: logicalHeight : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: logicalHeight : %d!\n", __LINE__);

    disInfo->GetOverscanLeft(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "overscanLeft", "I");
    CheckErrorAndLog(env, "GetFieldID: overscanLeft : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: overscanLeft : %d!\n", __LINE__);

    disInfo->GetOverscanTop(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "overscanTop", "I");
    CheckErrorAndLog(env, "GetFieldID: overscanTop : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: overscanTop : %d!\n", __LINE__);

    disInfo->GetOverscanRight(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "overscanRight", "I");
    CheckErrorAndLog(env, "GetFieldID: overscanRight : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: overscanRight : %d!\n", __LINE__);

    disInfo->GetOverscanBottom(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "overscanBottom", "I");
    CheckErrorAndLog(env, "GetFieldID: overscanBottom : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: overscanBottom : %d!\n", __LINE__);

    disInfo->GetRotation(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "rotation", "I");
    CheckErrorAndLog(env, "GetFieldID: rotation : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: rotation : %d!\n", __LINE__);

    Float tmpFloat;
    disInfo->GetRefreshRate(&tmpFloat);
    f = env->GetFieldID(disInfoKlass, "refreshRate", "F");
    CheckErrorAndLog(env, "GetFieldID: refreshRate : %d!\n", __LINE__);

    env->SetFloatField(jdisInfo, f, (jfloat)tmpFloat);
    CheckErrorAndLog(env, "SetIntField: refreshRate : %d!\n", __LINE__);

    AutoPtr<ArrayOf<Float> > supportedRefreshRates;
    disInfo->GetSupportedRefreshRates((ArrayOf<Float>**)&supportedRefreshRates);
    jfloatArray jsupportedRefreshRates = GetJavaFloatArray(env, supportedRefreshRates);
    f = env->GetFieldID(disInfoKlass, "supportedRefreshRates", "[F");
    CheckErrorAndLog(env, "GetFieldID: supportedRefreshRates : %d!\n", __LINE__);

    env->SetObjectField(jdisInfo, f, jsupportedRefreshRates);
    CheckErrorAndLog(env, "SetIntField: supportedRefreshRates : %d!\n", __LINE__);

    disInfo->GetLogicalDensityDpi(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "logicalDensityDpi", "I");
    CheckErrorAndLog(env, "GetFieldID: logicalDensityDpi : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: logicalDensityDpi : %d!\n", __LINE__);

    disInfo->GetPhysicalXDpi(&tmpFloat);
    f = env->GetFieldID(disInfoKlass, "physicalXDpi", "F");
    CheckErrorAndLog(env, "GetFieldID: physicalXDpi : %d!\n", __LINE__);

    env->SetFloatField(jdisInfo, f, (jfloat)tmpFloat);
    CheckErrorAndLog(env, "SetIntField: physicalXDpi : %d!\n", __LINE__);

    disInfo->GetPhysicalYDpi(&tmpFloat);
    f = env->GetFieldID(disInfoKlass, "physicalYDpi", "F");
    CheckErrorAndLog(env, "GetFieldID: physicalYDpi : %d!\n", __LINE__);

    env->SetFloatField(jdisInfo, f, (jfloat)tmpFloat);
    CheckErrorAndLog(env, "SetIntField: physicalYDpi : %d!\n", __LINE__);

    Int64 appVsyncOffsetNanos;
    disInfo->GetAppVsyncOffsetNanos(&appVsyncOffsetNanos);
    SetJavalongField(env, disInfoKlass, jdisInfo, appVsyncOffsetNanos, "appVsyncOffsetNanos", "GetJavaDisplayInfo");

    Int64 presentationDeadlineNanos;
    disInfo->GetPresentationDeadlineNanos(&presentationDeadlineNanos);
    SetJavalongField(env, disInfoKlass, jdisInfo, presentationDeadlineNanos, "presentationDeadlineNanos", "GetJavaDisplayInfo");

    disInfo->GetState(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "state", "I");
    CheckErrorAndLog(env, "GetFieldID: state : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: state : %d!\n", __LINE__);

    disInfo->GetOwnerUid(&tmpInt);
    f = env->GetFieldID(disInfoKlass, "ownerUid", "I");
    CheckErrorAndLog(env, "GetFieldID: ownerUid : %d!\n", __LINE__);

    env->SetIntField(jdisInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "SetIntField: ownerUid : %d!\n", __LINE__);

    String ownerPackageName;
    disInfo->GetOwnerPackageName(&ownerPackageName);
    SetJavaStringField(env, disInfoKlass, jdisInfo, ownerPackageName, "ownerPackageName", "GetJavaDisplayInfo");

    env->DeleteLocalRef(disInfoKlass);

    return jdisInfo;
}

void ElUtil::SetJavaComponentInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass parentClass,
    /* [in] */ jobject jparent,
    /* [in] */ IComponentInfo* comInfo)
{
    AutoPtr<IApplicationInfo> appInfo;
    comInfo->GetApplicationInfo((IApplicationInfo**)&appInfo);
    if (appInfo != NULL) {
        jobject jAppInfo = GetJavaApplicationInfo(env, appInfo);
        if (jAppInfo != NULL) {
            jfieldID f = env->GetFieldID(parentClass, "applicationInfo", "Landroid/content/pm/ApplicationInfo;");
            CheckErrorAndLog(env, "SetJavaComponentInfo Fail GetFieldID: ApplicationInfo : %d!\n", __LINE__);

            env->SetObjectField(jparent, f, jAppInfo);
            CheckErrorAndLog(env, "SetJavaComponentInfo Fail SetObjectField: jAppInfo : %d!\n", __LINE__);
            env->DeleteLocalRef(jAppInfo);
        }
        else {
            ALOGD("SetJavaComponentInfo Error: jExtras is NULL!");
        }
    }

    String processName;
    comInfo->GetProcessName(&processName);
    jstring jprocessName = GetJavaString(env, processName);
    if (jprocessName != NULL) {
        jfieldID f = env->GetFieldID(parentClass, "processName", "Ljava/lang/String;");
        CheckErrorAndLog(env, "SetJavaComponentInfo Fail GetFieldID: processName : %d!\n", __LINE__);

        env->SetObjectField(jparent, f, jprocessName);
        CheckErrorAndLog(env, "SetJavaComponentInfo Fail SetIntField: jprocessName : %d!\n", __LINE__);
        env->DeleteLocalRef(jprocessName);
    }

    Int32 tempInt;
    comInfo->GetDescriptionRes(&tempInt);
    jfieldID f = env->GetFieldID(parentClass, "descriptionRes", "I");
    CheckErrorAndLog(env, "SetJavaComponentInfo Fail GetFieldID: descriptionRes : %d!\n", __LINE__);

    env->SetIntField(jparent, f, tempInt);
    CheckErrorAndLog(env, "SetJavaComponentInfo Fail SetIntField: descriptionRes : %d!\n", __LINE__);

    Boolean tempBool;
    comInfo->GetEnabled(&tempBool);
    f = env->GetFieldID(parentClass, "enabled", "Z");
    CheckErrorAndLog(env, "SetJavaComponentInfo Fail GetFieldID: enabled : %d!\n", __LINE__);

    env->SetBooleanField(jparent, f, tempBool);
    CheckErrorAndLog(env, "SetJavaComponentInfo Fail SetIntField: enabled : %d!\n", __LINE__);

    comInfo->GetExported(&tempBool);
    f = env->GetFieldID(parentClass, "exported", "Z");
    CheckErrorAndLog(env, "SetJavaComponentInfo Fail GetFieldID: exported : %d!\n", __LINE__);

    env->SetBooleanField(jparent, f, tempBool);
    CheckErrorAndLog(env, "SetJavaComponentInfo Fail SetIntField: exported : %d!\n", __LINE__);

    AutoPtr<IPackageItemInfo> pkgInfo = IPackageItemInfo::Probe(comInfo);
    SetJavaPackageItemInfo(env, parentClass, jparent, pkgInfo);
}

void ElUtil::SetJavaPackageItemInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass parentClass,
    /* [in] */ jobject jparent,
    /* [in] */ IPackageItemInfo* pkgInfo)
{
    String name;
    pkgInfo->GetName(&name);
    jstring jname = GetJavaString(env, name);
    if (jname != NULL) {
        jfieldID f = env->GetFieldID(parentClass, "name", "Ljava/lang/String;");
        CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail GetFieldID: name : %d!\n", __LINE__);

        env->SetObjectField(jparent, f, jname);
        CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail SetIntField: jname : %d!\n", __LINE__);
        env->DeleteLocalRef(jname);
    }

    String packageName;
    pkgInfo->GetPackageName(&packageName);
    jstring jpackageName = GetJavaString(env, packageName);
    if (jpackageName != NULL) {
        jfieldID f = env->GetFieldID(parentClass, "packageName", "Ljava/lang/String;");
        CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail GetFieldID: packageName : %d!\n", __LINE__);

        env->SetObjectField(jparent, f, jpackageName);
        CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail SetIntField: jpackageName : %d!\n", __LINE__);
        env->DeleteLocalRef(jpackageName);
    }

    Int32 tempInt;
    pkgInfo->GetLabelRes(&tempInt);
    jfieldID f = env->GetFieldID(parentClass, "labelRes", "I");
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail GetFieldID: labelRes : %d!\n", __LINE__);

    env->SetIntField(jparent, f, tempInt);
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail SetIntField: labelRes : %d!\n", __LINE__);

    AutoPtr<ICharSequence> nonLocalizedLabel;
    pkgInfo->GetNonLocalizedLabel((ICharSequence**)&nonLocalizedLabel);
    if (nonLocalizedLabel != NULL) {
        String snonLocalizedLabel;
        nonLocalizedLabel->ToString(&snonLocalizedLabel);
        jstring jnonLocalizedLabel = GetJavaString(env, snonLocalizedLabel);

        f = env->GetFieldID(parentClass, "nonLocalizedLabel", "Ljava/lang/CharSequence;");
        CheckErrorAndLog(env, "SetJavaPackageItemInfo GetFieldID: CharSequence : %d!\n", __LINE__);

        env->SetObjectField(jparent, f, jnonLocalizedLabel);
        CheckErrorAndLog(env, "SetJavaPackageItemInfo SetObjectField: jnonLocalizedLabel : %d!\n", __LINE__);
        env->DeleteLocalRef(jnonLocalizedLabel);
    }

    pkgInfo->GetIcon(&tempInt);
    f = env->GetFieldID(parentClass, "icon", "I");
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail GetFieldID: icon : %d!\n", __LINE__);

    env->SetIntField(jparent, f, tempInt);
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail SetIntField: icon : %d!\n", __LINE__);

    pkgInfo->GetLogo(&tempInt);
    f = env->GetFieldID(parentClass, "logo", "I");
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail GetFieldID: logo : %d!\n", __LINE__);

    env->SetIntField(jparent, f, tempInt);
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail SetIntField: logo : %d!\n", __LINE__);

    AutoPtr<IBundle> metaData;
    pkgInfo->GetMetaData((IBundle**)&metaData);
    if (metaData != NULL) {
        jobject jmetaData = GetJavaBundle(env, metaData);
        if (jmetaData != NULL) {
            jfieldID f = env->GetFieldID(parentClass, "metaData", "Landroid/os/Bundle;");
            CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail GetFieldID: metaData : %d!\n", __LINE__);

            env->SetObjectField(jparent, f, jmetaData);
            CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail SetObjectField: jmetaData : %d!\n", __LINE__);
            env->DeleteLocalRef(jmetaData);
        }
        else {
            ALOGD("SetJavaPackageItemInfo Error: jmetaData is NULL!");
        }
    }

    pkgInfo->GetBanner(&tempInt);
    f = env->GetFieldID(parentClass, "banner", "I");
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail GetFieldID: banner %d", __LINE__);

    env->SetIntField(jparent, f, tempInt);
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail SetIntField: banner %d", __LINE__);

    pkgInfo->GetShowUserIcon(&tempInt);
    f = env->GetFieldID(parentClass, "showUserIcon", "I");
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail GetFieldID: showUserIcon %d", __LINE__);

    env->SetIntField(jparent, f, tempInt);
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail SetIntField: showUserIcon %d", __LINE__);

    pkgInfo->GetThemedIcon(&tempInt);
    f = env->GetFieldID(parentClass, "themedIcon", "I");
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail GetFieldID: themedIcon %d", __LINE__);

    env->SetIntField(jparent, f, tempInt);
    CheckErrorAndLog(env, "SetJavaPackageItemInfo Fail SetIntField: themedIcon %d", __LINE__);
}

jobject ElUtil::GetJavaActivityInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityInfo* actInfo)
{
    if (env == NULL || actInfo == NULL) {
        ALOGD("GetJavaActivityInfo Invalid arguments!");
        return NULL;
    }

    jobject jactInfo = NULL;

    jclass actInfoKlass = env->FindClass("android/content/pm/ActivityInfo");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail FindClass: ActivityInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(actInfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetMethodID: actInfoKlass : %d!\n", __LINE__);

    jactInfo = env->NewObject(actInfoKlass, m);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail NewObject: actInfoKlass : %d!\n", __LINE__);

    Int32 tempInt = 0;
    actInfo->GetTheme(&tempInt);
    jfieldID f = env->GetFieldID(actInfoKlass, "theme", "I");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: theme : %d!\n", __LINE__);

    env->SetIntField(jactInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: theme : %d!\n", __LINE__);

    actInfo->GetLaunchMode(&tempInt);
    f = env->GetFieldID(actInfoKlass, "launchMode", "I");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: launchMode : %d!\n", __LINE__);

    env->SetIntField(jactInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: launchMode : %d!\n", __LINE__);

    String permission;
    actInfo->GetPermission(&permission);
    jstring jpermission = GetJavaString(env, permission);
    if (jpermission != NULL) {
        jfieldID f = env->GetFieldID(actInfoKlass, "permission", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: permission : %d!\n", __LINE__);

        env->SetObjectField(jactInfo, f, jpermission);
        CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: jpermission : %d!\n", __LINE__);
        env->DeleteLocalRef(jpermission);
    }

    String taskAffinity;
    actInfo->GetTaskAffinity(&taskAffinity);
    jstring jtaskAffinity = GetJavaString(env, taskAffinity);
    if (jtaskAffinity != NULL) {
        jfieldID f = env->GetFieldID(actInfoKlass, "taskAffinity", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: taskAffinity : %d!\n", __LINE__);

        env->SetObjectField(jactInfo, f, jtaskAffinity);
        CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: jtaskAffinity : %d!\n", __LINE__);
        env->DeleteLocalRef(jtaskAffinity);
    }

    String targetActivity;
    actInfo->GetTargetActivity(&targetActivity);
    jstring jtargetActivity = GetJavaString(env, targetActivity);
    if (jtargetActivity != NULL) {
        jfieldID f = env->GetFieldID(actInfoKlass, "targetActivity", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: targetActivity : %d!\n", __LINE__);

        env->SetObjectField(jactInfo, f, jtargetActivity);
        CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: jtargetActivity : %d!\n", __LINE__);
        env->DeleteLocalRef(jtargetActivity);
    }

    actInfo->GetFlags(&tempInt);
    f = env->GetFieldID(actInfoKlass, "flags", "I");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: flags : %d!\n", __LINE__);

    env->SetIntField(jactInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: flags : %d!\n", __LINE__);

    actInfo->GetScreenOrientation(&tempInt);
    f = env->GetFieldID(actInfoKlass, "screenOrientation", "I");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: screenOrientation : %d!\n", __LINE__);

    env->SetIntField(jactInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: screenOrientation : %d!\n", __LINE__);

    actInfo->GetConfigChanges(&tempInt);
    f = env->GetFieldID(actInfoKlass, "configChanges", "I");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: configChanges : %d!\n", __LINE__);

    env->SetIntField(jactInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: configChanges : %d!\n", __LINE__);

    actInfo->GetSoftInputMode(&tempInt);
    f = env->GetFieldID(actInfoKlass, "softInputMode", "I");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: softInputMode : %d!\n", __LINE__);

    env->SetIntField(jactInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: softInputMode : %d!\n", __LINE__);

    actInfo->GetUiOptions(&tempInt);
    f = env->GetFieldID(actInfoKlass, "uiOptions", "I");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: uiOptions : %d!\n", __LINE__);

    env->SetIntField(jactInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: uiOptions : %d!\n", __LINE__);

    String parentActivityName;
    actInfo->GetParentActivityName(&parentActivityName);
    jstring jparentActivityName = GetJavaString(env, parentActivityName);
    if (jparentActivityName != NULL) {
        jfieldID f = env->GetFieldID(actInfoKlass, "parentActivityName", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: parentActivityName : %d!\n", __LINE__);

        env->SetObjectField(jactInfo, f, jparentActivityName);
        CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: jparentActivityName : %d!\n", __LINE__);
        env->DeleteLocalRef(jparentActivityName);
    }

    actInfo->GetPersistableMode(&tempInt);
    f = env->GetFieldID(actInfoKlass, "persistableMode", "I");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: persistableMode %d", __LINE__);

    env->SetIntField(jactInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: persistableMode %d", __LINE__);

    actInfo->GetMaxRecents(&tempInt);
    f = env->GetFieldID(actInfoKlass, "maxRecents", "I");
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail GetFieldID: maxRecents %d", __LINE__);

    env->SetIntField(jactInfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaActivityInfo Fail SetIntField: maxRecents %d", __LINE__);

    AutoPtr<IComponentInfo> comInfo = IComponentInfo::Probe(actInfo);
    SetJavaComponentInfo(env, actInfoKlass, jactInfo, comInfo);

    env->DeleteLocalRef(actInfoKlass);

    return jactInfo;
}

jobject ElUtil::GetJavaServiceInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IServiceInfo* serInfo)
{
    if (env == NULL || serInfo == NULL) {
        ALOGD("GetJavaServiceInfo Invalid arguments!");
        return NULL;
    }

    jobject jserInfo = NULL;

    jclass serInfoKlass = env->FindClass("android/content/pm/ServiceInfo");
    CheckErrorAndLog(env, "GetJavaServiceInfo Fail FindClass: ServiceInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(serInfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaServiceInfo Fail GetMethodID: serInfoKlass : %d!\n", __LINE__);

    jserInfo = env->NewObject(serInfoKlass, m);
    CheckErrorAndLog(env, "GetJavaServiceInfo Fail NewObject: serInfoKlass : %d!\n", __LINE__);

    String permission;
    serInfo->GetPermission(&permission);
    jstring jpermission = GetJavaString(env, permission);
    if (jpermission != NULL) {
        jfieldID f = env->GetFieldID(serInfoKlass, "permission", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaServiceInfo Fail GetFieldID: permission : %d!\n", __LINE__);

        env->SetObjectField(jserInfo, f, jpermission);
        CheckErrorAndLog(env, "GetJavaServiceInfo Fail SetIntField: jpermission : %d!\n", __LINE__);
        env->DeleteLocalRef(jpermission);
    }

    Int32 flags = 0;
    serInfo->GetFlags(&flags);
    jfieldID f = env->GetFieldID(serInfoKlass, "flags", "I");
    CheckErrorAndLog(env, "GetJavaServiceInfo Fail GetFieldID: flags : %d!\n", __LINE__);

    env->SetIntField(jserInfo, f, flags);
    CheckErrorAndLog(env, "GetJavaServiceInfo Fail SetIntField: flags : %d!\n", __LINE__);

    AutoPtr<IComponentInfo> comInfo = IComponentInfo::Probe(serInfo);
    SetJavaComponentInfo(env, serInfoKlass, jserInfo, comInfo);

    env->DeleteLocalRef(serInfoKlass);

    return jserInfo;
}

bool ElUtil::ToElWindowManagerLayoutParams(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jparams,
    /* [out] */ IWindowManagerLayoutParams** elParams)
{
    if (env == NULL || elParams == NULL) {
        ALOGD("ToElWindowManagerLayoutParams: Invalid argumenet!");
        return false;
    }

    jclass lparamsKlass = env->FindClass("android/view/WindowManager$LayoutParams");
    CheckErrorAndLog(env, "FindClass: android/os/Bundle : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(lparamsKlass, "width", "I");
    CheckErrorAndLog(env, "GetFieldID width : %d!\n", __LINE__);

    jint jwidth = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField width : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "height", "I");
    CheckErrorAndLog(env, "GetFieldID height : %d!\n", __LINE__);

    jint jheight = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetFieldID height : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "x", "I");
    CheckErrorAndLog(env, "GetFieldID x : %d!\n", __LINE__);

    jint jx = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField x : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "y", "I");
    CheckErrorAndLog(env, "GetFieldID y : %d!\n", __LINE__);

    jint jy = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField y : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "type", "I");
    CheckErrorAndLog(env, "GetFieldID type : %d!\n", __LINE__);

    jint jtype = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField type : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "flags", "I");
    CheckErrorAndLog(env, "GetFieldID flags : %d!\n", __LINE__);

    jint jflags = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField flags : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "privateFlags", "I");
    CheckErrorAndLog(env, "GetFieldID privateFlags : %d!\n", __LINE__);

    jint jprivateFlags = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField privateFlags : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "softInputMode", "I");
    CheckErrorAndLog(env, "GetFieldID softInputMode : %d!\n", __LINE__);

    jint jsoftInputMode = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField softInputMode : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "gravity", "I");
    CheckErrorAndLog(env, "GetFieldID gravity : %d!\n", __LINE__);

    jint jgravity = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField gravity : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "horizontalMargin", "F");
    CheckErrorAndLog(env, "GetFieldID horizontalMargin : %d!\n", __LINE__);

    jfloat jhorizontalMargin = env->GetFloatField(jparams, f);
    CheckErrorAndLog(env, "GetIntField horizontalMargin : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "verticalMargin", "F");
    CheckErrorAndLog(env, "GetFieldID verticalMargin : %d!\n", __LINE__);

    jfloat jverticalMargin = env->GetFloatField(jparams, f);
    CheckErrorAndLog(env, "GetIntField verticalMargin : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "format", "I");
    CheckErrorAndLog(env, "GetFieldID format : %d!\n", __LINE__);

    jint jformat = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField format : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "windowAnimations", "I");
    CheckErrorAndLog(env, "GetFieldID windowAnimations : %d!\n", __LINE__);

    jint jwindowAnimations = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField windowAnimations : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "alpha", "F");
    CheckErrorAndLog(env, "GetFieldID alpha : %d!\n", __LINE__);

    jfloat jalpha = env->GetFloatField(jparams, f);
    CheckErrorAndLog(env, "GetIntField alpha : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "dimAmount", "F");
    CheckErrorAndLog(env, "GetFieldID dimAmount : %d!\n", __LINE__);

    jfloat jdimAmount = env->GetFloatField(jparams, f);
    CheckErrorAndLog(env, "GetIntField dimAmount : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "screenBrightness", "F");
    CheckErrorAndLog(env, "GetFieldID screenBrightness : %d!\n", __LINE__);

    jfloat jscreenBrightness = env->GetFloatField(jparams, f);
    CheckErrorAndLog(env, "GetIntField screenBrightness : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "buttonBrightness", "F");
    CheckErrorAndLog(env, "GetFieldID buttonBrightness : %d!\n", __LINE__);

    jfloat jbuttonBrightness = env->GetFloatField(jparams, f);
    CheckErrorAndLog(env, "GetIntField buttonBrightness : %d!\n", __LINE__);

    jint rotationAnimation = GetJavaIntField(env, lparamsKlass, jparams,
        "rotationAnimation", "ToElWindowManagerLayoutParams");

    f = env->GetFieldID(lparamsKlass, "token", "Landroid/os/IBinder;");
    CheckErrorAndLog(env, "GetFieldID token : %d!\n", __LINE__);

    jobject jToken = env->GetObjectField(jparams, f);
    CheckErrorAndLog(env, "GetObjectField token : %d!\n", __LINE__);

    Elastos::Droid::Os::IBinder* token = NULL;
    if (jToken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        jclass viewRootWClass = env->FindClass("android/view/ViewRootImpl$W");
        ElUtil::CheckErrorAndLog(env, "FindClass ViewRootImpl$W fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jToken, tokenClass) ||
            env->IsInstanceOf(jToken, binderClass)) {

            f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (Elastos::Droid::Os::IBinder*)(Int64)env->GetLongField(jToken, f);
            CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        }
        else if (env->IsInstanceOf(jToken, viewRootWClass)) {
            jclass objectClass = env->FindClass("java/lang/Object");
            ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

            jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
            ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);

            jint jTokenHashCode = env->CallIntMethod(jToken, m);
            HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jTokenHashCode);
            if (it != sWindowMap.End()) {
                token = Elastos::Droid::Os::IBinder::Probe(it->mSecond);
            }
            else {
                ALOGE("ToElWindowManagerLayoutParams() can't find token in sWindowMap");
                assert(0);
            }

            env->DeleteLocalRef(objectClass);
         }
         else {
            String objClassName = GetClassName(env, jToken);
            ALOGE("ToElWindowManagerLayoutParams() Token[%s] not Support!", objClassName.string());
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(binderClass);
        env->DeleteLocalRef(viewRootWClass);
        env->DeleteLocalRef(jToken);
    }  else {
        // ALOGE("ToElWindowManagerLayoutParams: jToken is NULL");
    }

    f = env->GetFieldID(lparamsKlass, "packageName", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetMethodID packageName : %d!\n", __LINE__);

    jstring jpackageName = (jstring)env->GetObjectField(jparams, f);
    CheckErrorAndLog(env, "GetObjectField token : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "mTitle", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID mTitle : %d!\n", __LINE__);

    jobject jTitle = env->GetObjectField(jparams, f);
    CheckErrorAndLog(env, "GetObjectField mTitle : %d!\n", __LINE__);

    jclass csClass = env->FindClass("java/lang/CharSequence");
    CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
    CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);
    env->DeleteLocalRef(csClass);

    jstring jmTitle = (jstring)env->CallObjectMethod(jTitle, m);
    CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "screenOrientation", "I");
    CheckErrorAndLog(env, "GetFieldID screenOrientation : %d!\n", __LINE__);

    jint jscreenOrientation = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField screenOrientation : %d!\n", __LINE__);

    jfloat preferredRefreshRate = GetJavafloatField(env, lparamsKlass, jparams,
        "preferredRefreshRate", "ToElWindowManagerLayoutParams");

    f = env->GetFieldID(lparamsKlass, "systemUiVisibility", "I");
    CheckErrorAndLog(env, "GetFieldID systemUiVisibility : %d!\n", __LINE__);

    jint jsystemUiVisibility = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField systemUiVisibility : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "subtreeSystemUiVisibility", "I");
    CheckErrorAndLog(env, "GetFieldID subtreeSystemUiVisibility : %d!\n", __LINE__);

    jint jsubtreeSystemUiVisibility = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField subtreeSystemUiVisibility : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "hasSystemUiListeners", "Z");
    CheckErrorAndLog(env, "GetFieldID subtreeSystemUiVisibility : %d!\n", __LINE__);

    jboolean jhasSystemUiListeners = env->GetBooleanField(jparams, f);
    CheckErrorAndLog(env, "GetIntField hasSystemUiListeners : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "inputFeatures", "I");
    CheckErrorAndLog(env, "GetFieldID inputFeatures : %d!\n", __LINE__);

    jint jinputFeatures = env->GetIntField(jparams, f);
    CheckErrorAndLog(env, "GetIntField inputFeatures : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "userActivityTimeout", "J");
    CheckErrorAndLog(env, "GetFieldID userActivityTimeout : %d!\n", __LINE__);

    jlong juserActivityTimeout = env->GetLongField(jparams, f);
    CheckErrorAndLog(env, "GetIntField userActivityTimeout : %d!\n", __LINE__);

    f = env->GetFieldID(lparamsKlass, "surfaceInsets", "Landroid/graphics/Rect;");
    CheckErrorAndLog(env, "GetFieldID surfaceInsets : %d!\n", __LINE__);

    jobject jsurfaceInsets = env->GetObjectField(jparams, f);
    CheckErrorAndLog(env, "GetObjectField surfaceInsets : %d!\n", __LINE__);

    AutoPtr<IRect> surfaceInsets;
    ToElRect(env, jsurfaceInsets, (IRect**)&surfaceInsets);

    jfloat blurMaskAlphaThreshold = GetJavafloatField(env, lparamsKlass, jparams,
        "blurMaskAlphaThreshold", "ToElWindowManagerLayoutParams");

    if (NOERROR != CWindowManagerLayoutParams::New(jwidth, jheight, jx, jy, jtype, jflags, jformat, elParams)) {
        ALOGD("ToElWindowManagerLayoutParams: create CIntent fail!");
        return false;
    }

    (*elParams)->SetPrivateFlags((Int32)jprivateFlags);
    (*elParams)->SetSoftInputMode((Int32)jsoftInputMode);
    (*elParams)->SetGravity((Int32)jgravity);
    (*elParams)->SetHorizontalMargin((Float)jhorizontalMargin);
    (*elParams)->SetVerticalMargin((Float)jverticalMargin);
    (*elParams)->SetWindowAnimations((Int32)jwindowAnimations);
    (*elParams)->SetAlpha((Float)jalpha);
    (*elParams)->SetDimAmount((Float)jdimAmount);
    (*elParams)->SetScreenBrightness((Float)jscreenBrightness);
    (*elParams)->SetButtonBrightness((Float)jbuttonBrightness);
    (*elParams)->SetRotationAnimation(rotationAnimation);
    (*elParams)->SetToken(token);
    String packageName = ToElString(env, jpackageName);
    (*elParams)->SetPackageName(packageName);
    String mTtitle = ToElString(env, jmTitle);
    AutoPtr<ICharSequence> csTitle;
    CString::New(mTtitle, (ICharSequence**)&csTitle);
    (*elParams)->SetTitle(csTitle);
    (*elParams)->SetScreenOrientation((Int32)jscreenOrientation);
    (*elParams)->SetPreferredRefreshRate(preferredRefreshRate);
    (*elParams)->SetSystemUiVisibility((Int32)jsystemUiVisibility);
    (*elParams)->SetSubtreeSystemUiVisibility((Int32)jsubtreeSystemUiVisibility);
    (*elParams)->SetHasSystemUiListeners((Boolean)jhasSystemUiListeners);
    (*elParams)->SetInputFeatures((Int32)jinputFeatures);
    (*elParams)->SetUserActivityTimeout((Int64)juserActivityTimeout);
    (*elParams)->SetSurfaceInsets(surfaceInsets);
    (*elParams)->SetBlurMaskAlphaThreshold(blurMaskAlphaThreshold);

    env->DeleteLocalRef(lparamsKlass);
    env->DeleteLocalRef(jpackageName);
    env->DeleteLocalRef(jTitle);
    env->DeleteLocalRef(jmTitle);
    env->DeleteLocalRef(jsurfaceInsets);

    return true;
}

jobject ElUtil::GetJavaRect(
    /* [in] */ JNIEnv* env,
    /* [in] */ IRect* rect)
{
    if (env == NULL || rect == NULL) {
        ALOGD("GetJavaRect() Invalid arguments!");
        return NULL;
    }

    jobject jRect = NULL;

    jclass rectKlass = env->FindClass("android/graphics/Rect");
    CheckErrorAndLog(env, "GetJavaRect Fail FindClass: Rect : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(rectKlass, "<init>", "(IIII)V");
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail GetMethodID: appInfoKlass : %d!\n", __LINE__);

    Int32 left;
    rect->GetLeft(&left);
    Int32 top;
    rect->GetTop(&top);
    Int32 right;
    rect->GetRight(&right);
    Int32 bottom;
    rect->GetBottom(&bottom);

    jRect = env->NewObject(rectKlass, m, left, top, right, bottom);
    CheckErrorAndLog(env, "GetJavaApplicationInfo Fail NewObject: Rect : %d!\n", __LINE__);

    env->DeleteLocalRef(rectKlass);

    return jRect;
}

bool ElUtil::ToElRect(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jrect,
    /* [out] */ IRect** elRect)
{
    if (elRect == NULL) {
        ALOGD("ToElRect: Invalid argumenet!");
        return false;
    }

    jclass rectKlass = env->FindClass("android/graphics/Rect");
    CheckErrorAndLog(env, "ToElRect Fail FindClass: Rect : %d!\n", __LINE__);

    if (NOERROR != CRect::New(elRect)) {
        ALOGD("ToElIntent: create CRect fail!");
        return false;
    }

    jfieldID f = env->GetFieldID(rectKlass, "left", "I");
    CheckErrorAndLog(env, "GetFieldID: left : %d!\n", __LINE__);

    jint jtmpInt = env->GetIntField(jrect, f);
    CheckErrorAndLog(env, "GetIntField left : %d!\n", __LINE__);
    (*elRect)->SetLeft((Int32)jtmpInt);

    f = env->GetFieldID(rectKlass, "top", "I");
    CheckErrorAndLog(env, "GetFieldID: top : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jrect, f);
    CheckErrorAndLog(env, "GetIntField top : %d!\n", __LINE__);
    (*elRect)->SetTop((Int32)jtmpInt);

    f = env->GetFieldID(rectKlass, "right", "I");
    CheckErrorAndLog(env, "GetFieldID: right : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jrect, f);
    CheckErrorAndLog(env, "GetIntField right : %d!\n", __LINE__);
    (*elRect)->SetRight((Int32)jtmpInt);

    f = env->GetFieldID(rectKlass, "bottom", "I");
    CheckErrorAndLog(env, "GetFieldID: bottom : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jrect, f);
    CheckErrorAndLog(env, "GetIntField bottom : %d!\n", __LINE__);
    (*elRect)->SetBottom((Int32)jtmpInt);

    env->DeleteLocalRef(rectKlass);

    return true;
}

jobject ElUtil::GetJavaConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ IConfiguration* configuration)
{
    if (env == NULL || configuration == NULL) {
        ALOGD("GetJavaConfiguration Invalid arguments!");
        return NULL;
    }

    jobject jConfiguration = NULL;

    jclass configKlass = env->FindClass("android/content/res/Configuration");
    CheckErrorAndLog(env, "GetJavaConfiguration Fail FindClass: Configuration : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(configKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaConfiguration Fail GetMethodID: configKlass : %d!\n", __LINE__);

    jConfiguration = env->NewObject(configKlass, m);
    CheckErrorAndLog(env, "GetJavaConfiguration Fail NewObject: configKlass : %d!\n", __LINE__);

    SetJavaConfiguration(env, configuration, jConfiguration);

    env->DeleteLocalRef(configKlass);

    return jConfiguration;
}

bool ElUtil::SetJavaConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ IConfiguration* configuration,
    /* [in] */ jobject jConfiguration)
{
    if (env == NULL || configuration == NULL || jConfiguration == NULL) {
        ALOGD("SetJavaConfiguration Invalid arguments!");
        return false;
    }

    jclass configKlass = env->FindClass("android/content/res/Configuration");
    CheckErrorAndLog(env, "GetJavaConfiguration Fail FindClass: Configuration : %d!\n", __LINE__);

    Float tmpFloat;
    configuration->GetFontScale(&tmpFloat);
    jfieldID f = env->GetFieldID(configKlass, "fontScale", "F");
    CheckErrorAndLog(env, "GetJavaConfiguration Fail GetFieldID: fontScale : %d!\n", __LINE__);

    env->SetFloatField(jConfiguration, f, tmpFloat);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: fontScale : %d!\n", __LINE__);

    Int32 tempInt;
    configuration->GetMcc(&tempInt);
    f = env->GetFieldID(configKlass, "mcc", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: mcc : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: mcc : %d!\n", __LINE__);

    configuration->GetMnc(&tempInt);
    f = env->GetFieldID(configKlass, "mnc", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: mnc : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: mnc : %d!\n", __LINE__);

    AutoPtr<ILocale> locale;
    configuration->GetLocale((ILocale**)&locale);
    if (locale != NULL) {
        jobject jLocale = GetJavaLocale(env, locale);

        f = env->GetFieldID(configKlass, "locale", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: locale : %d!\n", __LINE__);

        env->SetObjectField(jConfiguration, f, jLocale);
        CheckErrorAndLog(env, "SetJavaConfiguration Fail SetObjectField: locale : %d!\n", __LINE__);
        env->DeleteLocalRef(jLocale);
    }
    else {
        // TODO: Eric locale is NULL ?
        ALOGE("SetJavaConfiguration() locale is NULL");
        jclass localKlass = env->FindClass("java/util/Locale");
        CheckErrorAndLog(env, "GetJavaLocale Fail FindClass: Locale : %d!\n", __LINE__);

        f = env->GetStaticFieldID(localKlass, "SIMPLIFIED_CHINESE", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: SIMPLIFIED_CHINESE : %d!\n", __LINE__);

        jobject jLocale = env->GetStaticObjectField(localKlass, f);
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticObjectField: : %d!\n", __LINE__);

        f = env->GetFieldID(configKlass, "locale", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: locale : %d!\n", __LINE__);

        env->SetObjectField(jConfiguration, f, jLocale);
        CheckErrorAndLog(env, "SetJavaConfiguration Fail SetObjectField: locale : %d!\n", __LINE__);

        env->DeleteLocalRef(localKlass);
        env->DeleteLocalRef(jLocale);
    }

    Boolean tempBool;
    configuration->IsUserSetLocale(&tempBool);
    f = env->GetFieldID(configKlass, "userSetLocale", "Z");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: userSetLocale : %d!\n", __LINE__);

    env->SetBooleanField(jConfiguration, f, (jboolean)tempBool);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetBooleanField: userSetLocale : %d!\n", __LINE__);

    configuration->GetTouchscreen(&tempInt);
    f = env->GetFieldID(configKlass, "touchscreen", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: touchscreen : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: touchscreen : %d!\n", __LINE__);\

    configuration->GetKeyboard(&tempInt);
    f = env->GetFieldID(configKlass, "keyboard", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: keyboard : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: keyboard : %d!\n", __LINE__);

    configuration->GetKeyboardHidden(&tempInt);
    f = env->GetFieldID(configKlass, "keyboardHidden", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: keyboardHidden : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: keyboardHidden : %d!\n", __LINE__);

    configuration->GetHardKeyboardHidden(&tempInt);
    f = env->GetFieldID(configKlass, "hardKeyboardHidden", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: hardKeyboardHidden : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: hardKeyboardHidden : %d!\n", __LINE__);

    configuration->GetNavigation(&tempInt);
    f = env->GetFieldID(configKlass, "navigation", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: navigation : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: navigation : %d!\n", __LINE__);

    configuration->GetNavigationHidden(&tempInt);
    f = env->GetFieldID(configKlass, "navigationHidden", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: navigationHidden : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: navigationHidden : %d!\n", __LINE__);

    configuration->GetOrientation(&tempInt);
    f = env->GetFieldID(configKlass, "orientation", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: orientation : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: orientation : %d!\n", __LINE__);

    configuration->GetScreenLayout(&tempInt);
    f = env->GetFieldID(configKlass, "screenLayout", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: screenLayout : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: screenLayout : %d!\n", __LINE__);

    configuration->GetUiMode(&tempInt);
    f = env->GetFieldID(configKlass, "uiMode", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: uiMode : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: uiMode : %d!\n", __LINE__);

    configuration->GetScreenWidthDp(&tempInt);
    f = env->GetFieldID(configKlass, "screenWidthDp", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: screenWidthDp : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: screenWidthDp : %d!\n", __LINE__);

    configuration->GetScreenHeightDp(&tempInt);
    f = env->GetFieldID(configKlass, "screenHeightDp", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: screenHeightDp : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: screenHeightDp : %d!\n", __LINE__);

    configuration->GetSmallestScreenWidthDp(&tempInt);
    f = env->GetFieldID(configKlass, "smallestScreenWidthDp", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: smallestScreenWidthDp : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: smallestScreenWidthDp : %d!\n", __LINE__);

    configuration->GetDensityDpi(&tempInt);
    f = env->GetFieldID(configKlass, "densityDpi", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: densityDpi : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: densityDpi : %d!\n", __LINE__);

    configuration->GetCompatScreenWidthDp(&tempInt);
    f = env->GetFieldID(configKlass, "compatScreenWidthDp", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: compatScreenWidthDp : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: compatScreenWidthDp : %d!\n", __LINE__);

    configuration->GetCompatScreenHeightDp(&tempInt);
    f = env->GetFieldID(configKlass, "compatScreenHeightDp", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: compatScreenHeightDp : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: compatScreenHeightDp : %d!\n", __LINE__);

    configuration->GetCompatSmallestScreenWidthDp(&tempInt);
    f = env->GetFieldID(configKlass, "compatSmallestScreenWidthDp", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: compatSmallestScreenWidthDp : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: compatSmallestScreenWidthDp : %d!\n", __LINE__);

    configuration->GetSeq(&tempInt);
    f = env->GetFieldID(configKlass, "seq", "I");
    CheckErrorAndLog(env, "SetJavaConfiguration Fail GetFieldID: seq : %d!\n", __LINE__);

    env->SetIntField(jConfiguration, f, tempInt);
    CheckErrorAndLog(env, "SetJavaConfiguration Fail SetIntField: seq : %d!\n", __LINE__);

    env->DeleteLocalRef(configKlass);

    AutoPtr<IThemeConfig> themeConfig;
    configuration->GetThemeConfig((IThemeConfig**)&themeConfig);
    if (themeConfig != NULL) {
        ALOGW("TODO: field \"themeConfig\" of Configuration is not translated");
    }

    return true;
}

jobject ElUtil::GetJavaLocale(
    /* [in] */ JNIEnv* env,
    /* [in] */ ILocale* locale)
{
    if (env == NULL || locale == NULL) {
        ALOGD("GetJavaLocale Invalid arguments!");
        return NULL;
    }

    jobject jLocale = NULL;

    jclass localKlass = env->FindClass("java/util/Locale");
    CheckErrorAndLog(env, "GetJavaLocale Fail FindClass: Locale : %d!\n", __LINE__);

    AutoPtr<ILocaleHelper> localeHelp;
    CLocaleHelper::AcquireSingleton((ILocaleHelper**)&localeHelp);

    jfieldID f = NULL;
    AutoPtr<ILocale> slocale;
    Boolean result = FALSE;

    if ((localeHelp->GetSIMPLIFIED_CHINESE((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "SIMPLIFIED_CHINESE", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: SIMPLIFIED_CHINESE : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetTRADITIONAL_CHINESE((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "TRADITIONAL_CHINESE", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: TRADITIONAL_CHINESE : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetUS((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "US", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: US : %d!\n", __LINE__);
    }  else if ((localeHelp->GetUK((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "UK", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: UK : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetCANADA((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "CANADA", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: CANADA : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetCANADA_FRENCH((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "CANADA_FRENCH", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: CANADA_FRENCH : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetCHINA((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "CHINA", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: CHINA : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetCHINESE((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "CHINESE", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: CHINESE : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetENGLISH((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "ENGLISH", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: ENGLISH : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetFRANCE((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "FRANCE", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: FRANCE : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetGERMAN((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "GERMAN", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: GERMAN : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetGERMANY((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "GERMANY", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: GERMANY : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetITALIAN((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "ITALIAN", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: ITALIAN : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetITALY((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "ITALY", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: ITALY : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetJAPAN((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "JAPAN", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: JAPAN : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetJAPANESE((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "JAPANESE", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: JAPANESE : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetKOREA((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "KOREA", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: KOREA : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetKOREAN((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "KOREAN", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: KOREAN : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetPRC((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "PRC", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: PRC : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetROOT((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "ROOT", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: ROOT : %d!\n", __LINE__);
    }
    else if ((localeHelp->GetTAIWAN((ILocale**)&slocale), slocale->Equals(locale, &result), result)) {
        f = env->GetStaticFieldID(localKlass, "TAIWAN", "Ljava/util/Locale;");
        CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: TAIWAN : %d!\n", __LINE__);
    }
    else {
        ALOGD("GetJavaLocale Unknown Locale!");
        assert(0);
    }

    jLocale = env->GetStaticObjectField(localKlass, f);
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticObjectField: : %d!\n", __LINE__);

    env->DeleteLocalRef(localKlass);

    return jLocale;
}

jint ElUtil::GetNativeSurface(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jsurface)
{
    if (env == NULL || jsurface == NULL) {
        ALOGD("GetNativeSurface: Invalid argumenet!");
        return false;
    }

    jclass surfaceKlass = env->FindClass("android/view/Surface");
    CheckErrorAndLog(env, "FindClass: android/os/Bundle : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(surfaceKlass, "mNativeSurface", "J");
    CheckErrorAndLog(env, "GetFieldID: mNativeSurface : %d!\n", __LINE__);

    jint jNativeSurface = env->GetIntField(jsurface, f);
    CheckErrorAndLog(env, "GetIntField: mNativeSurface : %d!\n", __LINE__);

    env->DeleteLocalRef(surfaceKlass);

    return jNativeSurface;
}

bool ElUtil::ToElConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jConfiguration,
    /* [out] */ IConfiguration** elConfiguration)
{
    if (env == NULL || elConfiguration == NULL) {
        ALOGD("ToElConfiguration: Invalid argumenet!");
        return false;
    }

    jclass configKlass = env->FindClass("android/content/res/Configuration");
    CheckErrorAndLog(env, "ToElConfiguration Fail FindClass: Configuration : %d!\n", __LINE__);

    if (NOERROR != CConfiguration::New(elConfiguration)) {
        ALOGD("ToElIntent: create CConfiguration fail!");
        return false;
    }

    jfieldID f = env->GetFieldID(configKlass, "fontScale", "F");
    CheckErrorAndLog(env, "GetFieldID: fontScale : %d!\n", __LINE__);

    jfloat jtmpFloat = env->GetFloatField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField fontScale : %d!\n", __LINE__);
    (*elConfiguration)->SetFontScale((Float)jtmpFloat);

    f = env->GetFieldID(configKlass, "mcc", "I");
    CheckErrorAndLog(env, "GetFieldID: mcc : %d!\n", __LINE__);

    jint jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField mcc : %d!\n", __LINE__);
    (*elConfiguration)->SetMcc((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "mnc", "I");
    CheckErrorAndLog(env, "GetFieldID: mnc : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField mnc : %d!\n", __LINE__);
    (*elConfiguration)->SetMnc((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "locale", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetFieldID: locale : %d!\n", __LINE__);

    jobject jlocale = env->GetObjectField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField fontScale : %d!\n", __LINE__);
    if (jlocale != NULL) {
        AutoPtr<ILocale> locale;
        if (ElUtil::ToElLocale(env, jlocale, (ILocale**)&locale)) {
            (*elConfiguration)->SetLocale(locale);
        }
        else {
            ALOGE("ToElConfiguration() ToElLocale fail!");
        }
        env->DeleteLocalRef(jlocale);
    }
    else {
        // ALOGE("ToElConfiguration() locale is NULL");
        AutoPtr<ILocaleHelper> localeHelp;
        CLocaleHelper::AcquireSingleton((ILocaleHelper**)&localeHelp);
        AutoPtr<ILocale> locale;
        localeHelp->GetSIMPLIFIED_CHINESE((ILocale**)&locale);
        (*elConfiguration)->SetLocale(locale);
    }

    f = env->GetFieldID(configKlass, "userSetLocale", "Z");
    CheckErrorAndLog(env, "GetFieldID: mnc : %d!\n", __LINE__);

    jboolean juserSetLocale = env->GetBooleanField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField mnc : %d!\n", __LINE__);
    (*elConfiguration)->SetUserSetLocale((Boolean)juserSetLocale);

    f = env->GetFieldID(configKlass, "touchscreen", "I");
    CheckErrorAndLog(env, "GetFieldID: touchscreen : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField touchscreen : %d!\n", __LINE__);
    (*elConfiguration)->SetTouchscreen((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "keyboard", "I");
    CheckErrorAndLog(env, "GetFieldID: keyboard : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField keyboard : %d!\n", __LINE__);
    (*elConfiguration)->SetKeyboard((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "keyboardHidden", "I");
    CheckErrorAndLog(env, "GetFieldID: keyboardHidden : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField keyboardHidden : %d!\n", __LINE__);
    (*elConfiguration)->SetKeyboardHidden((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "hardKeyboardHidden", "I");
    CheckErrorAndLog(env, "GetFieldID: hardKeyboardHidden : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField hardKeyboardHidden : %d!\n", __LINE__);
    (*elConfiguration)->SetHardKeyboardHidden((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "navigation", "I");
    CheckErrorAndLog(env, "GetFieldID: navigation : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField navigation : %d!\n", __LINE__);
    (*elConfiguration)->SetNavigation((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "navigationHidden", "I");
    CheckErrorAndLog(env, "GetFieldID: navigationHidden : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField navigationHidden : %d!\n", __LINE__);
    (*elConfiguration)->SetNavigationHidden((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "orientation", "I");
    CheckErrorAndLog(env, "GetFieldID: orientation : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField orientation : %d!\n", __LINE__);
    (*elConfiguration)->SetOrientation((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "screenLayout", "I");
    CheckErrorAndLog(env, "GetFieldID: screenLayout : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField screenLayout : %d!\n", __LINE__);
    (*elConfiguration)->SetScreenLayout((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "uiMode", "I");
    CheckErrorAndLog(env, "GetFieldID: uiMode : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField uiMode : %d!\n", __LINE__);
    (*elConfiguration)->SetUiMode((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "screenWidthDp", "I");
    CheckErrorAndLog(env, "GetFieldID: screenWidthDp : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField screenWidthDp : %d!\n", __LINE__);
    (*elConfiguration)->SetScreenWidthDp((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "screenHeightDp", "I");
    CheckErrorAndLog(env, "GetFieldID: screenHeightDp : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField screenHeightDp : %d!\n", __LINE__);
    (*elConfiguration)->SetScreenHeightDp((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "smallestScreenWidthDp", "I");
    CheckErrorAndLog(env, "GetFieldID: smallestScreenWidthDp : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField smallestScreenWidthDp : %d!\n", __LINE__);
    (*elConfiguration)->SetSmallestScreenWidthDp((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "densityDpi", "I");
    CheckErrorAndLog(env, "GetFieldID: densityDpi : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField densityDpi : %d!\n", __LINE__);
    (*elConfiguration)->SetDensityDpi((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "compatScreenWidthDp", "I");
    CheckErrorAndLog(env, "GetFieldID: compatScreenWidthDp : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField compatScreenWidthDp : %d!\n", __LINE__);
    (*elConfiguration)->SetCompatScreenWidthDp((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "compatScreenHeightDp", "I");
    CheckErrorAndLog(env, "GetFieldID: compatScreenHeightDp : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField compatScreenHeightDp : %d!\n", __LINE__);
    (*elConfiguration)->SetCompatScreenHeightDp((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "compatSmallestScreenWidthDp", "I");
    CheckErrorAndLog(env, "GetFieldID: compatSmallestScreenWidthDp : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField compatSmallestScreenWidthDp : %d!\n", __LINE__);
    (*elConfiguration)->SetCompatSmallestScreenWidthDp((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "seq", "I");
    CheckErrorAndLog(env, "GetFieldID: seq : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jConfiguration, f);
    CheckErrorAndLog(env, "GetIntField seq : %d!\n", __LINE__);
    (*elConfiguration)->SetSeq((Int32)jtmpInt);

    f = env->GetFieldID(configKlass, "themeConfig", "Landroid/content/res/ThemeConfig;");
    CheckErrorAndLog(env, "GetFieldID: themeConfig : %d!\n", __LINE__);

    jobject jthemeConfig = env->GetObjectField(jConfiguration, f);
    CheckErrorAndLog(env, "GetObjectField themeConfig : %d!\n", __LINE__);
    if (jthemeConfig != NULL) {
        AutoPtr<IThemeConfig> themeConfig;
        ALOGE("TODO: themeConfig of Configuration is not NULL!");
        (*elConfiguration)->SetThemeConfig(themeConfig);
        env->DeleteLocalRef(jthemeConfig);
    }

    env->DeleteLocalRef(configKlass);

    return true;
}

bool ElUtil::ToElEditorInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jEditorInfo,
    /* [out] */ IEditorInfo** elEditorInfo)
{
    if (env == NULL || elEditorInfo == NULL) {
        ALOGD("ToElEditorInfo: Invalid argumenet!");
        return false;
    }

    jclass edInfoKlass = env->FindClass("android/view/inputmethod/EditorInfo");
    CheckErrorAndLog(env, "ToElEditorInfo Fail FindClass: EditorInfo : %d!\n", __LINE__);

    if (NOERROR != CEditorInfo::New(elEditorInfo)) {
        ALOGD("ToElIntent: create CConfiguration fail!");
        return false;
    }

    jfieldID f = env->GetFieldID(edInfoKlass, "inputType", "I");
    CheckErrorAndLog(env, "GetFieldID: inputType : %d!\n", __LINE__);

    jint jtmpInt = env->GetIntField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetIntField inputType : %d!\n", __LINE__);
    (*elEditorInfo)->SetInputType((Int32)jtmpInt);

    f = env->GetFieldID(edInfoKlass, "imeOptions", "I");
    CheckErrorAndLog(env, "GetFieldID: imeOptions : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetIntField imeOptions : %d!\n", __LINE__);
    (*elEditorInfo)->SetImeOptions((Int32)jtmpInt);

    f = env->GetFieldID(edInfoKlass, "privateImeOptions", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: privateImeOptions : %d!\n", __LINE__);

    jstring jprivateImeOptions = (jstring)env->GetObjectField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetObjectField: privateImeOptions : %d!\n", __LINE__);
    if (jprivateImeOptions != NULL) {
        (*elEditorInfo)->SetPrivateImeOptions(ToElString(env, jprivateImeOptions));
        env->DeleteLocalRef(jprivateImeOptions);
    }

    f = env->GetFieldID(edInfoKlass, "actionLabel", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID actionLabel : %d!\n", __LINE__);

    jobject jactionLabel = env->GetObjectField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetObjectField actionLabel : %d!\n", __LINE__);

    if (jactionLabel != NULL) {
        AutoPtr<ICharSequence> csactionLabel;
        if (ToElCharSequence(env, jactionLabel,(ICharSequence**)&csactionLabel)) {
            (*elEditorInfo)->SetActionLabel(csactionLabel);
        }

        env->DeleteLocalRef(jactionLabel);
    }

    f = env->GetFieldID(edInfoKlass, "actionId", "I");
    CheckErrorAndLog(env, "GetFieldID: actionId : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetIntField actionId : %d!\n", __LINE__);
    (*elEditorInfo)->SetActionId((Int32)jtmpInt);

    f = env->GetFieldID(edInfoKlass, "initialSelStart", "I");
    CheckErrorAndLog(env, "GetFieldID: initialSelStart : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetIntField initialSelStart : %d!\n", __LINE__);
    (*elEditorInfo)->SetInitialSelStart((Int32)jtmpInt);

    f = env->GetFieldID(edInfoKlass, "initialSelEnd", "I");
    CheckErrorAndLog(env, "GetFieldID: initialSelEnd : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetIntField initialSelEnd : %d!\n", __LINE__);
    (*elEditorInfo)->SetInitialSelEnd((Int32)jtmpInt);

    f = env->GetFieldID(edInfoKlass, "initialCapsMode", "I");
    CheckErrorAndLog(env, "GetFieldID: initialCapsMode : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetIntField initialCapsMode : %d!\n", __LINE__);
    (*elEditorInfo)->SetInitialCapsMode((Int32)jtmpInt);

    f = env->GetFieldID(edInfoKlass, "hintText", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID hintText : %d!\n", __LINE__);

    jobject jhintText = env->GetObjectField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetObjectField hintText : %d!\n", __LINE__);

    if (jhintText != NULL) {
        AutoPtr<ICharSequence> cshintText;
        if (ToElCharSequence(env, jhintText,(ICharSequence**)&cshintText)) {
            (*elEditorInfo)->SetHintText(cshintText);
        }

        env->DeleteLocalRef(jhintText);
    }

    f = env->GetFieldID(edInfoKlass, "label", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID label : %d!\n", __LINE__);

    jobject jlabel = env->GetObjectField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetObjectField label : %d!\n", __LINE__);

    if (jlabel != NULL) {
        AutoPtr<ICharSequence> cslabel;
        if (ToElCharSequence(env, jlabel,(ICharSequence**)&cslabel)) {
            (*elEditorInfo)->SetLabel(cslabel);
        }

        env->DeleteLocalRef(jlabel);
    }

    f = env->GetFieldID(edInfoKlass, "packageName", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: packageName : %d!\n", __LINE__);

    jstring jpackageName = (jstring)env->GetObjectField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetObjectField: packageName : %d!\n", __LINE__);
    if (jpackageName != NULL) {
        (*elEditorInfo)->SetPackageName(ToElString(env, jpackageName));
        env->DeleteLocalRef(jpackageName);
    }

    f = env->GetFieldID(edInfoKlass, "fieldId", "I");
    CheckErrorAndLog(env, "GetFieldID: fieldId : %d!\n", __LINE__);

    jtmpInt = env->GetIntField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetIntField fieldId : %d!\n", __LINE__);
    (*elEditorInfo)->SetFieldId((Int32)jtmpInt);

    f = env->GetFieldID(edInfoKlass, "fieldName", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: fieldName : %d!\n", __LINE__);

    jstring jfieldName = (jstring)env->GetObjectField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetObjectField: fieldName : %d!\n", __LINE__);
    if (jfieldName != NULL) {
        (*elEditorInfo)->SetFieldName(ToElString(env, jfieldName));
        env->DeleteLocalRef(jfieldName);
    }

    f = env->GetFieldID(edInfoKlass, "extras", "Landroid/os/Bundle;");
    CheckErrorAndLog(env, "GetFieldID: extras Landroid/os/Bundle; : %d!\n", __LINE__);

    jobject jBundle = env->GetObjectField(jEditorInfo, f);
    CheckErrorAndLog(env, "GetObjectField: extras : %d!\n", __LINE__);

    if (jBundle != NULL) {
        AutoPtr<IBundle> elBundle;
        if (ToElBundle(env, jBundle, (IBundle**)&elBundle)) {
            (*elEditorInfo)->SetExtras(elBundle);
        }
        else {
            ALOGE("ToElEditorInfo: ToElBundle() fail : %d!\n", __LINE__);
        }

        env->DeleteLocalRef(jBundle);
    }

    env->DeleteLocalRef(edInfoKlass);

    return true;
}

bool ElUtil::ToElCharSequence(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jcs,
    /* [out] */ ICharSequence** elCs)
{
    if (elCs) *elCs = NULL;

    if (env == NULL || elCs == NULL) {
        ALOGD("ToElCharSequence: Invalid argumenet!");
        return false;
    }

    if (jcs == NULL) {
        return true;
    }

    jclass spannedKlass = env->FindClass("android/text/Spanned");
    CheckErrorAndLog(env, "ToElCharSequence Fail FindClass: Spanned : %d!\n", __LINE__);

    if (env->IsInstanceOf(jcs, spannedKlass)) {
        ALOGE("ToElCharSequence jcs IsInstanceOf Spanned!");
    }
    else {
        jclass spannableStringBuilderClass = env->FindClass("android/text/SpannableStringBuilder");
        CheckErrorAndLog(env, "ToElCharSequence Fail FindClass: SpannableStringBuilder: %d!\n", __LINE__);

        if (env->IsInstanceOf(jcs, spannableStringBuilderClass)) {
            ALOGE("ToElCharSequence jcs IsInstanceOf SpannableStringBuilder!");
        }
        env->DeleteLocalRef(spannableStringBuilderClass);
    }

    jclass csClass = env->FindClass("java/lang/CharSequence");
    CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
    CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

    jstring jtmpString = (jstring)env->CallObjectMethod(jcs, m);
    CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

    env->DeleteLocalRef(csClass);

    if (jtmpString != NULL) {
        ECode ec = CString::New(ToElString(env, jtmpString), elCs);
        env->DeleteLocalRef(jtmpString);

        if (NOERROR != ec) {
            ALOGE("ToElCharSequence: create CConfiguration fail!");
            return false;
        }
    }

    return true;
}

jobject ElUtil::GetJavaKeyCharacterMap(
    /* [in] */ JNIEnv* env,
    /* [in] */ IKeyCharacterMap* keyMap)
{
    if (env == NULL || keyMap == NULL) {
        ALOGD("GetJavaKeyCharacterMap() Invalid arguments!");
        return NULL;
    }

    jobject jkeyMap = NULL;

    jclass keyMapKlass = env->FindClass("android/view/KeyCharacterMap");
    CheckErrorAndLog(env, "GetJavaKeyCharacterMap Fail FindClass: KeyCharacterMap : %d!\n", __LINE__);

    Int64 map = 0;
    keyMap->GetMap(&map);

    jmethodID m = env->GetMethodID(keyMapKlass, "<init>", "(J)V");
    CheckErrorAndLog(env, "GetJavaKeyCharacterMap Fail GetMethodID: KeyCharacterMap : %d!\n", __LINE__);

    jkeyMap = env->NewObject(keyMapKlass, m, map);
    CheckErrorAndLog(env, "GetJavaKeyCharacterMap Fail NewObject: KeyCharacterMap : %d!\n", __LINE__);
    env->DeleteLocalRef(keyMapKlass);

    return jkeyMap;
}

jobject ElUtil::GetJavaInputDevice(
    /* [in] */ JNIEnv* env,
    /* [in] */ IInputDevice* inpDevice)
{
    if (env == NULL || inpDevice == NULL) {
        ALOGD("GetJavaInputDevice() Invalid arguments!");
        return NULL;
    }

    jclass inDevKlass = env->FindClass("android/view/InputDevice");
    CheckErrorAndLog(env, "GetJavaInputDevice Fail FindClass: InputDevice : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(inDevKlass, "<init>", "(IIILjava/lang/String;IILjava/lang/String;ZIILandroid/view/KeyCharacterMap;ZZ)V");
    CheckErrorAndLog(env, "GetJavaInputDevice Fail GetMethodID: appInfoKlass : %d!\n", __LINE__);

    Int32 mId;
    inpDevice->GetId(&mId);
    Int32 mGeneration;
    inpDevice->GetGeneration(&mGeneration);
    Int32 controllerNumber;
    inpDevice->GetControllerNumber(&controllerNumber);
    String mName;
    inpDevice->GetName(&mName);
    jstring jmName = GetJavaString(env, mName);
    Int32 vendorId;
    inpDevice->GetVendorId(&vendorId);
    Int32 productId;
    inpDevice->GetProductId(&productId);
    String mDescriptor;
    inpDevice->GetDescriptor(&mDescriptor);
    jstring jmDescriptor = GetJavaString(env, mDescriptor);
    Boolean mIsExternal;
    inpDevice->IsExternal(&mIsExternal);
    Int32 mSources;
    inpDevice->GetSources(&mSources);
    Int32 mKeyboardType;
    inpDevice->GetKeyboardType(&mKeyboardType);
    AutoPtr<IKeyCharacterMap> mKeyCharacterMap;
    inpDevice->GetKeyCharacterMap((IKeyCharacterMap**)&mKeyCharacterMap);
    jobject jmKeyCharacterMap = NULL;
    if (mKeyCharacterMap != NULL) {
        jmKeyCharacterMap = GetJavaKeyCharacterMap(env, mKeyCharacterMap);
    }
    Boolean hasVibrator = FALSE;
    inpDevice->GetHasVibrator(&hasVibrator);

    Boolean hasButtonUnderPad = FALSE;
    inpDevice->GetHasButtonUnderPad(&hasButtonUnderPad);

    jobject jinpDevice = env->NewObject(inDevKlass, m, mId, mGeneration, controllerNumber, jmName, vendorId, productId,
        jmDescriptor, (jboolean)mIsExternal,
        mSources, mKeyboardType, jmKeyCharacterMap, hasVibrator, hasButtonUnderPad);
    CheckErrorAndLog(env, "GetJavaInputDevice Fail NewObject: InputDevice : %d!\n", __LINE__);

    env->DeleteLocalRef(jmName);
    env->DeleteLocalRef(jmDescriptor);
    env->DeleteLocalRef(jmKeyCharacterMap);

    AutoPtr<IList> list;
    inpDevice->GetMotionRanges((IList**)&list);
    if (list != NULL) {
        Int32 count = 0;
        list->GetSize(&count);
        if (count > 0) {
            jclass listKlass = env->FindClass("java/util/ArrayList");
            CheckErrorAndLog(env, "GetJavaInputDevice Fail FindClass: ArrayList : %d!\n", __LINE__);

            jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
            CheckErrorAndLog(env, "GetJavaInputDevice Fail GetMethodID: ArrayList : %d!\n", __LINE__);

            jobject jlistMR = env->NewObject(listKlass, m);
            CheckErrorAndLog(env, "GetJavaInputDevice Fail NewObject: ArrayList : %d!\n", __LINE__);

            jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
            CheckErrorAndLog(env, "GetJavaInputDevice Fail GetMethodID: add : %d!\n", __LINE__);

            jclass inDevMRKlass = env->FindClass("android/view/InputDevice$MotionRange");
            CheckErrorAndLog(env, "GetJavaInputDevice Fail FindClass: InputDevice : %d!\n", __LINE__);

            m = env->GetMethodID(inDevMRKlass, "<init>", "(IIFFFFF)V");
            CheckErrorAndLog(env, "GetJavaInputDevice Fail GetMethodID: appInfoKlass : %d!\n", __LINE__);

            for (Int32 i = 0; i < count; i++) {
                AutoPtr<IInterface> obj;
                list->Get(i, (IInterface**)&obj);
                AutoPtr<IMotionRange> mr = IMotionRange::Probe(obj);

                Int32 axis;
                mr->GetAxis(&axis);
                Int32 source;
                mr->GetSource(&source);
                Float minimum;
                mr->GetMin(&minimum);
                Float maximum;
                mr->GetMax(&maximum);
                Float range;
                mr->GetRange(&range);
                Float flat;
                mr->GetFlat(&flat);
                Float resolution;
                mr->GetResolution(&resolution);

                jobject jmr = env->NewObject(inDevMRKlass, m, axis, source,
                    minimum, maximum, range, flat, resolution);
                CheckErrorAndLog(env, "GetJavaInputDevice Fail NewObject: jmr : %d!\n", __LINE__);

                env->CallBooleanMethod(jlistMR, mAdd, jmr);
                CheckErrorAndLog(env, "GetJavaInputDevice Fail CallObjectMethod: mAdd : %d!\n", __LINE__);

                env->DeleteLocalRef(jmr);
            }

            jfieldID f = env->GetFieldID(inDevKlass, "mMotionRanges", "Ljava/util/ArrayList;");
            CheckErrorAndLog(env, "GetJavaInputDevice Fail GetFieldID: mMotionRanges : %d!\n", __LINE__);

            env->SetObjectField(jinpDevice, f, jlistMR);
            CheckErrorAndLog(env, "GetJavaInputDevice Fail SetIntField: mMotionRanges : %d!\n", __LINE__);

            env->DeleteLocalRef(listKlass);
            env->DeleteLocalRef(inDevMRKlass);
            env->DeleteLocalRef(jlistMR);
        }
    }

    env->DeleteLocalRef(inDevKlass);

    return jinpDevice;
}

jboolean ElUtil::ToElInputEvent(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jevent,
    /* [out] */ IInputEvent** ev)
{
    if (env == NULL || jevent == NULL || ev == NULL){
        ALOGE("ToElInputEvent() Invalid arguments!");
        return JNI_FALSE;
    }

    *ev = NULL;

    String classname = GetClassName(env, jevent);

    if (classname.Equals("android.view.KeyEvent")) {
        AutoPtr<IKeyEvent> keyEvent;
        ToElKeyEvent(env, jevent, (IKeyEvent**)&keyEvent);
        *ev = IInputEvent::Probe(keyEvent);
        if (*ev != NULL) {
            (*ev)->AddRef();
        }
        else {
            ALOGE("Create Elastos KeyEvent failed!");
        }
    }
    else if (classname.Equals("android.view.MotionEvent")) {
        AutoPtr<IMotionEvent> motionEvent;
        ToElMotionEvent(env, jevent, (IMotionEvent**)&motionEvent);
        *ev = IInputEvent::Probe(motionEvent);
        if (*ev != NULL) {
            (*ev)->AddRef();
        }
        else {
            ALOGE("Obtain motion event failed!");
        }
    }
    else {
        ALOGE("ToElInputEvent(), unknown input event, class is: %s", classname.string());
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

bool ElUtil::ToElMotionEvent(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jevent,
    /* [out] */ IMotionEvent** event)
{
    if (env == NULL || jevent == NULL || event == NULL) {
        ALOGD("ToElMotionEvent: Invalid argumenet!");
        return false;
    }

    jclass eventKlass = env->FindClass("android/view/MotionEvent");
    CheckErrorAndLog(env, "ToElMotionEvent Fail FindClass: MotionEvent : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(eventKlass, "mNativePtr", "J");
    CheckErrorAndLog(env, "GetFieldID: mNativePtr : %d!\n", __LINE__);

    jlong jmNativePtr = env->GetLongField(jevent, f);
    CheckErrorAndLog(env, "GetLongField mNativePtr : %d!\n", __LINE__);

    android::MotionEvent* destEvent = new android::MotionEvent();
    android::MotionEvent* sourceEvent = (android::MotionEvent*)jmNativePtr;
    destEvent->copyFrom(sourceEvent, true);

    if (NOERROR != CMotionEvent::New(event)) {
        ALOGD("ToElMotionEvent: create CMotionEvent fail!");
        return false;
    }
    (*event)->SetNative((Handle32)destEvent);

    env->DeleteLocalRef(eventKlass);

    return true;
}

bool ElUtil::ToElKeyEvent(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jevent,
    /* [out] */ IKeyEvent** event)
{
    if (env == NULL || jevent == NULL) {
        ALOGD("ToElKeyEvent: Invalid argumenet!");
        return false;
    }

    jclass kEventKlass = env->FindClass("android/view/KeyEvent");
    CheckErrorAndLog(env, "ToElKeyEvent Fail FindClass: KeyEvent : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(kEventKlass, "mDeviceId", "I");
    CheckErrorAndLog(env, "GetFieldID: mDeviceId : %d!\n", __LINE__);

    jint jmDeviceId = env->GetIntField(jevent, f);
    CheckErrorAndLog(env, "GetIntField mDeviceId : %d!\n", __LINE__);

    f = env->GetFieldID(kEventKlass, "mSource", "I");
    CheckErrorAndLog(env, "GetFieldID: mSource : %d!\n", __LINE__);

    jint jmSource = env->GetIntField(jevent, f);
    CheckErrorAndLog(env, "GetIntField mSource : %d!\n", __LINE__);

    f = env->GetFieldID(kEventKlass, "mAction", "I");
    CheckErrorAndLog(env, "GetFieldID: mAction : %d!\n", __LINE__);

    jint jmAction = env->GetIntField(jevent, f);
    CheckErrorAndLog(env, "GetIntField mAction : %d!\n", __LINE__);

    f = env->GetFieldID(kEventKlass, "mKeyCode", "I");
    CheckErrorAndLog(env, "GetFieldID: mKeyCode : %d!\n", __LINE__);

    jint jmKeyCode = env->GetIntField(jevent, f);
    CheckErrorAndLog(env, "GetIntField mKeyCode : %d!\n", __LINE__);

    f = env->GetFieldID(kEventKlass, "mRepeatCount", "I");
    CheckErrorAndLog(env, "GetFieldID: mRepeatCount : %d!\n", __LINE__);

    jint jmRepeatCount = env->GetIntField(jevent, f);
    CheckErrorAndLog(env, "GetIntField mRepeatCount : %d!\n", __LINE__);

    f = env->GetFieldID(kEventKlass, "mMetaState", "I");
    CheckErrorAndLog(env, "GetFieldID: mMetaState : %d!\n", __LINE__);

    jint jmMetaState = env->GetIntField(jevent, f);
    CheckErrorAndLog(env, "GetIntField mMetaState : %d!\n", __LINE__);

    f = env->GetFieldID(kEventKlass, "mScanCode", "I");
    CheckErrorAndLog(env, "GetFieldID: mScanCode : %d!\n", __LINE__);

    jint jmScanCode = env->GetIntField(jevent, f);
    CheckErrorAndLog(env, "GetIntField mScanCode : %d!\n", __LINE__);

    f = env->GetFieldID(kEventKlass, "mFlags", "I");
    CheckErrorAndLog(env, "GetFieldID: mFlags : %d!\n", __LINE__);

    jint jmFlags = env->GetIntField(jevent, f);
    CheckErrorAndLog(env, "GetIntField mFlags : %d!\n", __LINE__);

    f = env->GetFieldID(kEventKlass, "mDownTime", "J");
    CheckErrorAndLog(env, "GetFieldID: mDownTime : %d!\n", __LINE__);

    jlong jmDownTime = env->GetLongField(jevent, f);
    CheckErrorAndLog(env, "GetLongField mDownTime : %d!\n", __LINE__);

    f = env->GetFieldID(kEventKlass, "mEventTime", "J");
    CheckErrorAndLog(env, "GetFieldID: mEventTime : %d!\n", __LINE__);

    jlong jmEventTime = env->GetLongField(jevent, f);
    CheckErrorAndLog(env, "GetLongField mEventTime : %d!\n", __LINE__);

    if (NOERROR != CKeyEvent::New((Int64)jmDownTime, (Int64)jmEventTime, (Int32)jmAction, (Int32)jmKeyCode,
            (Int32)jmRepeatCount, (Int32)jmMetaState, (Int32)jmDeviceId, (Int32)jmScanCode, (Int32)jmFlags, (Int32)jmSource, event)) {
        ALOGD("ToElKeyEvent: create CKeyEvent fail!");
        return false;
    }

    env->DeleteLocalRef(kEventKlass);

    return true;
}

jobject ElUtil::GetJavaIntent(
    /* [in] */ JNIEnv* env,
    /* [in] */ IIntent* intent)
{
    if (env == NULL || intent == NULL) {
        ALOGD("GetJavaIntent() Invalid arguments!");
        return NULL;
    }

    jobject jIntent = NULL;

    jclass intentKlass = env->FindClass("android/content/Intent");
    CheckErrorAndLog(env, "Fail FindClass: Intent : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(intentKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaIntent Fail GetMethodID: intentKlass : %d!\n", __LINE__);

    jIntent = env->NewObject(intentKlass, m);
    CheckErrorAndLog(env, "GetJavaIntent Fail NewObject: intentKlass : %d!\n", __LINE__);

    String mAction;
    intent->GetAction(&mAction);
    if (!mAction.IsNull()) {
        jfieldID f = env->GetFieldID(intentKlass, "mAction", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: mAction : %d!\n", __LINE__);

        jstring jmAction = ElUtil::GetJavaString(env, mAction);
        env->SetObjectField(jIntent, f, jmAction);
        ElUtil::CheckErrorAndLog(env, "GetObjectField: SetObjectField : %d!\n", __LINE__);
        env->DeleteLocalRef(jmAction);
    }


    AutoPtr<IUri> mData;
    intent->GetData((IUri**)&mData);
    if (mData != NULL)  {
        jobject jmData = ElUtil::GetJavaUri(env, mData);
        jfieldID f = env->GetFieldID(intentKlass, "mData", "Landroid/net/Uri;");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: mData : %d!\n", __LINE__);

        env->SetObjectField(jIntent, f, jmData);
        ElUtil::CheckErrorAndLog(env, "SetObjectField: jdata : %d!\n", __LINE__);
        env->DeleteLocalRef(jmData);
    }


    String mType;
    intent->GetType(&mType);
    if (!mType.IsNull()) {
        jfieldID f = env->GetFieldID(intentKlass, "mType", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: mType : %d!\n", __LINE__);

        jobject jmType = ElUtil::GetJavaString(env, mType);
        env->SetObjectField(jIntent, f, jmType);
        ElUtil::CheckErrorAndLog(env, "SetObjectField: jtype : %d!\n", __LINE__);
        env->DeleteLocalRef(jmType);
    }

    Int32 tempInt;
    intent->GetFlags(&tempInt);
    jfieldID f = env->GetFieldID(intentKlass, "mFlags", "I");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: mFlags : %d!\n", __LINE__);

    env->SetIntField(jIntent, f, (jint)tempInt);
    ElUtil::CheckErrorAndLog(env, "SetIntField: mFlags : %d!\n", __LINE__);

    String mPackage;
    intent->GetPackage(&mPackage);
    if (!mPackage.IsNull())  {
        jfieldID f = env->GetFieldID(intentKlass, "mPackage", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: mPackage : %d!\n", __LINE__);

        jobject jmPackage = ElUtil::GetJavaString(env, mPackage);
        env->SetObjectField(jIntent, f, jmPackage);
        ElUtil::CheckErrorAndLog(env, "SetObjectField: jmPackage : %d!\n", __LINE__);
        env->DeleteLocalRef(jmPackage);
    }

    AutoPtr<IComponentName> componentName;
    intent->GetComponent((IComponentName**)&componentName);
    if (componentName != NULL) {
        jobject jComponentName = GetJavaComponentName(env, componentName);
        if (jComponentName != NULL) {
            jfieldID f = env->GetFieldID(intentKlass, "mComponent", "Landroid/content/ComponentName;");
            ElUtil::CheckErrorAndLog(env, "GetJavaIntent Fail GetFieldID: mComponent : %d!\n", __LINE__);

            env->SetObjectField(jIntent, f, jComponentName);
            ElUtil::CheckErrorAndLog(env, "GetJavaIntent Fail SetObjectField: jComponent : %d!\n", __LINE__);
            env->DeleteLocalRef(jComponentName);
        }
        else {
            ALOGD("GetJavaIntent() Error: jComponentName is NULL!");
        }
    }

    AutoPtr<IRect> mSourceBounds;
    intent->GetSourceBounds((IRect**)&mSourceBounds);
    if (mSourceBounds != NULL) {
        jobject jmSourceBounds = ElUtil::GetJavaRect(env, mSourceBounds);
        if (jmSourceBounds != NULL) {
            jfieldID f = env->GetFieldID(intentKlass, "mSourceBounds", "Landroid/graphics/Rect;");
            ElUtil::CheckErrorAndLog(env, "GetFieldID: mSourceBounds : %d!\n", __LINE__);

            env->SetObjectField(jIntent, f, jmSourceBounds);
            ElUtil::CheckErrorAndLog(env, "SetObjectField jmSourceBounds : %d!\n", __LINE__);
            env->DeleteLocalRef(jmSourceBounds);
        }
        else {
            ALOGE("GetJavaIntent() Error: jmSourceBounds is NULL!");
        }
    }

    AutoPtr<ArrayOf<String> > categories;
    intent->GetCategories((ArrayOf<String>**)&categories);
    if (categories != NULL) {
        Int32 count = categories->GetLength();
        if (count > 0) {
            jmethodID m = env->GetMethodID(intentKlass, "addCategory", "(Ljava/lang/String;)Landroid/content/Intent;");
            ElUtil::CheckErrorAndLog(env, "GetJavaIntent Fail GetMethodID: size : %d!\n", __LINE__);

            for (int i = 0; i < count; i++) {
                jstring jcategorie = ElUtil::GetJavaString(env, (*categories)[i]);
                jobject jres = env->CallObjectMethod(jIntent, m, jcategorie);
                ElUtil::CheckErrorAndLog(env, "GetJavaIntent CallObjectMethod: addCategory : %d!\n", __LINE__);
                env->DeleteLocalRef(jcategorie);
                env->DeleteLocalRef(jres);
            }
        }
    }

    AutoPtr<IIntent> selector;
    intent->GetSelector((IIntent**)&selector);
    if (selector != NULL) {
        jobject jselector = ElUtil::GetJavaIntent(env, selector);
        if (jselector != NULL) {
            jfieldID f = env->GetFieldID(intentKlass, "mSelector", "Landroid/content/Intent;");
            ElUtil::CheckErrorAndLog(env, "GetJavaIntent GetFieldID: mSelector : %d!\n", __LINE__);

            env->SetObjectField(jIntent, f, jselector);
            ElUtil::CheckErrorAndLog(env, "GetJavaIntent SetObjectField jselector : %d!\n", __LINE__);
            env->DeleteLocalRef(jselector);
        }
        else {
            ALOGE("GetJavaIntent() Error: jselector is NULL!");
        }
    }

    AutoPtr<IClipData> clipData;
    intent->GetClipData((IClipData**)&clipData);
    if (clipData != NULL) {
        jobject jclipData = ElUtil::GetJavaClipData(env, clipData);
        if (jclipData != NULL) {
            jfieldID f = env->GetFieldID(intentKlass, "mClipData", "Landroid/content/ClipData;");
            ElUtil::CheckErrorAndLog(env, "GetFieldID: mClipData : %d!\n", __LINE__);

            env->SetObjectField(jIntent, f, jclipData);
            ElUtil::CheckErrorAndLog(env, "SetObjectField jclipData : %d!\n", __LINE__);
            env->DeleteLocalRef(jclipData);
        }
        else {
            ALOGE("GetJavaIntent() Error: jclipData is NULL!");
        }
    }

    Int32 contentUserHint;
    intent->GetContentUserHint(&contentUserHint);
    f = env->GetFieldID(intentKlass, "mContentUserHint", "I");
    CheckErrorAndLog(env, "GetFieldID: mContentUserHint %d", __LINE__);

    env->SetIntField(jIntent, f, (jint)contentUserHint);
    CheckErrorAndLog(env, "SetIntField: mContentUserHint %d", __LINE__);

    AutoPtr<IBundle> extras;
    intent->GetExtras((IBundle**)&extras);
    if (extras != NULL) {
        jobject jExtras = GetJavaBundle(env, extras);
        if (jExtras != NULL) {
            jfieldID f = env->GetFieldID(intentKlass, "mExtras", "Landroid/os/Bundle;");
            ElUtil::CheckErrorAndLog(env, "GetJavaIntent Fail GetFieldID: mExtras : %d!\n", __LINE__);

            env->SetObjectField(jIntent, f, jExtras);
            ElUtil::CheckErrorAndLog(env, "GetJavaIntent Fail SetObjectField: jExtras : %d!\n", __LINE__);
            env->DeleteLocalRef(jExtras);
        }
        else {
            ALOGD("GetJavaIntent() Error: jExtras is NULL!");
        }
    }

    env->DeleteLocalRef(intentKlass);
    return jIntent;
}

jobject ElUtil::GetJavaComponentName(
    /* [in] */ JNIEnv* env,
    /* [in] */ IComponentName* componentName)
{
    if (env == NULL || componentName == NULL) {
        ALOGD("GetJavaComponentName() Invalid arguments!");
        return NULL;
    }

    jobject jcomponentName = NULL;

    jclass componentNameKlass = env->FindClass("android/content/ComponentName");
    CheckErrorAndLog(env, "GetJavaComponentName Fail FindClass: ComponentName : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(componentNameKlass, "<init>", "(Ljava/lang/String;Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetJavaComponentName Fail GetMethodID: componentNameKlass : %d!\n", __LINE__);

    String packageName;
    String className;
    componentName->GetPackageName(&packageName);
    componentName->GetClassName(&className);
    jstring jPackageName = GetJavaString(env, packageName);
    jstring jClassName = GetJavaString(env, className);

    jcomponentName = env->NewObject(componentNameKlass, m, jPackageName, jClassName);
    CheckErrorAndLog(env, "GetJavaComponentName Fail NewObject: componentNameKlass : %d!\n", __LINE__);

    env->DeleteLocalRef(componentNameKlass);
    env->DeleteLocalRef(jPackageName);
    env->DeleteLocalRef(jClassName);

    return jcomponentName;
}

jobject ElUtil::GetJavaResolveInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IResolveInfo* resolveInfo)
{
    if (env == NULL || resolveInfo == NULL) {
        ALOGD("GetJavaResolveInfo() Invalid arguments!");
        return NULL;
    }

    jobject jresolveInfo = NULL;

    jclass riKlass = env->FindClass("android/content/pm/ResolveInfo");
    CheckErrorAndLog(env, "GetJavaResolveInfo Fail FindClass: ResolveInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(riKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaResolveInfo Fail GetMethodID: ResolveInfo : %d!\n", __LINE__);

    jresolveInfo = env->NewObject(riKlass, m);
    CheckErrorAndLog(env, "GetJavaResolveInfo Fail NewObject: ResolveInfo : %d!\n", __LINE__);

    AutoPtr<IActivityInfo> activityInfo;
    resolveInfo->GetActivityInfo((IActivityInfo**)&activityInfo);
    if (activityInfo != NULL) {
        jobject jactivityInfo = GetJavaActivityInfo(env, activityInfo);

        jfieldID f = env->GetFieldID(riKlass, "activityInfo", "Landroid/content/pm/ActivityInfo;");
        CheckErrorAndLog(env, "GetJavaResolveInfo Fail GetFieldID: ActivityInfo : %d!\n", __LINE__);

        env->SetObjectField(jresolveInfo, f, jactivityInfo);
        CheckErrorAndLog(env, "GetJavaResolveInfo Fail SetObjectField: jactivityInfo : %d!\n", __LINE__);
        env->DeleteLocalRef(jactivityInfo);
    }

    AutoPtr<IServiceInfo> serviceInfo;
    resolveInfo->GetServiceInfo((IServiceInfo**)&serviceInfo);
    if (serviceInfo != NULL) {
        jobject jserviceInfo = GetJavaServiceInfo(env, serviceInfo);

        jfieldID f = env->GetFieldID(riKlass, "serviceInfo", "Landroid/content/pm/ServiceInfo;");
        CheckErrorAndLog(env, "GetJavaResolveInfo Fail GetFieldID: ServiceInfo : %d!\n", __LINE__);

        env->SetObjectField(jresolveInfo, f, jserviceInfo);
        CheckErrorAndLog(env, "GetJavaResolveInfo Fail SetObjectField: jserviceInfo : %d!\n", __LINE__);
        env->DeleteLocalRef(jserviceInfo);
    }

    AutoPtr<IProviderInfo> providerInfo;
    resolveInfo->GetProviderInfo((IProviderInfo**)&providerInfo);
    if (providerInfo != NULL) {
        jobject jproviderInfo = GetJavaProviderInfo(env, providerInfo);

        jfieldID f = env->GetFieldID(riKlass, "providerInfo", "Landroid/content/pm/ProviderInfo;");
        CheckErrorAndLog(env, "GetJavaResolveInfo Fail GetFieldID: ProviderInfo : %d!\n", __LINE__);

        env->SetObjectField(jresolveInfo, f, jproviderInfo);
        CheckErrorAndLog(env, "GetJavaResolveInfo Fail SetObjectField: jproviderInfo : %d!\n", __LINE__);
        env->DeleteLocalRef(jproviderInfo);
    }

    AutoPtr<IIntentFilter> filter;
    resolveInfo->GetFilter((IIntentFilter**)&filter);
    if (filter != NULL) {
        jobject jfilter = GetJavaIntentFilter(env, filter);

        jfieldID f = env->GetFieldID(riKlass, "filter", "Landroid/content/IntentFilter;");
        CheckErrorAndLog(env, "GetJavaResolveInfo Fail GetFieldID: filter : %d!\n", __LINE__);

        env->SetObjectField(jresolveInfo, f, jfilter);
        CheckErrorAndLog(env, "GetJavaResolveInfo Fail SetObjectField: jfilter : %d!\n", __LINE__);
        env->DeleteLocalRef(jfilter);
    }

    Int32 tmpInt;
    resolveInfo->GetPriority(&tmpInt);
    jfieldID f = env->GetFieldID(riKlass, "priority", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: priority : %d!\n", __LINE__);

    env->SetIntField(jresolveInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: priority : %d!\n", __LINE__);

    resolveInfo->GetPreferredOrder(&tmpInt);
    f = env->GetFieldID(riKlass, "preferredOrder", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: preferredOrder : %d!\n", __LINE__);

    env->SetIntField(jresolveInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: preferredOrder : %d!\n", __LINE__);

    resolveInfo->GetMatch(&tmpInt);
    f = env->GetFieldID(riKlass, "match", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: match : %d!\n", __LINE__);

    env->SetIntField(jresolveInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: match : %d!\n", __LINE__);

    resolveInfo->GetSpecificIndex(&tmpInt);
    f = env->GetFieldID(riKlass, "specificIndex", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: specificIndex : %d!\n", __LINE__);

    env->SetIntField(jresolveInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: specificIndex : %d!\n", __LINE__);

    resolveInfo->GetLabelRes(&tmpInt);
    f = env->GetFieldID(riKlass, "labelRes", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: labelRes : %d!\n", __LINE__);

    env->SetIntField(jresolveInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: labelRes : %d!\n", __LINE__);

    AutoPtr<ICharSequence> nonLocalizedLabel;
    resolveInfo->GetNonLocalizedLabel((ICharSequence**)&nonLocalizedLabel);
    if (nonLocalizedLabel != NULL) {
        String snonLocalizedLabel;
        nonLocalizedLabel->ToString(&snonLocalizedLabel);
        jstring jnonLocalizedLabel = GetJavaString(env, snonLocalizedLabel);

        f = env->GetFieldID(riKlass, "nonLocalizedLabel", "Ljava/lang/CharSequence;");
        CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: CharSequence : %d!\n", __LINE__);

        env->SetObjectField(jresolveInfo, f, jnonLocalizedLabel);
        CheckErrorAndLog(env, "GetJavaResolveInfo SetObjectField: jnonLocalizedLabel : %d!\n", __LINE__);
        env->DeleteLocalRef(jnonLocalizedLabel);
    }

    resolveInfo->GetIcon(&tmpInt);
    f = env->GetFieldID(riKlass, "icon", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: icon : %d!\n", __LINE__);

    env->SetIntField(jresolveInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: icon : %d!\n", __LINE__);

    String resolvePackageName;
    resolveInfo->GetResolvePackageName(&resolvePackageName);
    if (!resolvePackageName.IsNull()) {
        jstring jresolvePackageName = GetJavaString(env, resolvePackageName);

        f = env->GetFieldID(riKlass, "resolvePackageName", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: String : %d!\n", __LINE__);

        env->SetObjectField(jresolveInfo, f, jresolvePackageName);
        CheckErrorAndLog(env, "GetJavaResolveInfo SetObjectField: jresolvePackageName : %d!\n", __LINE__);
        env->DeleteLocalRef(jresolvePackageName);
    }

    resolveInfo->GetTargetUserId(&tmpInt);
    f = env->GetFieldID(riKlass, "targetUserId", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: targetUserId : %d!\n", __LINE__);

    env->SetIntField(jresolveInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: targetUserId : %d!\n", __LINE__);

    Boolean isSystem;
    resolveInfo->GetSystem(&isSystem);
    f = env->GetFieldID(riKlass, "system", "Z");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: system : %d!\n", __LINE__);

    env->SetBooleanField(jresolveInfo, f, (jboolean)isSystem);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetBooleanField: system : %d!\n", __LINE__);

    Boolean noResourceId;
    resolveInfo->GetNoResourceId(&noResourceId);
    f = env->GetFieldID(riKlass, "noResourceId", "Z");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: noResourceId : %d!\n", __LINE__);

    env->SetBooleanField(jresolveInfo, f, (jboolean)noResourceId);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetBooleanField: noResourceId : %d!\n", __LINE__);

    env->DeleteLocalRef(riKlass);

    return jresolveInfo;
}

jobject ElUtil::GetJavaIntentFilter(
    /* [in] */ JNIEnv* env,
    /* [in] */ IIntentFilter* filter)
{
    if (env == NULL || filter == NULL) {
        ALOGD("GetJavaIntentFilter() Invalid arguments!");
        return NULL;
    }

    jclass filterKlass = env->FindClass("android/content/IntentFilter");
    CheckErrorAndLog(env, "GetJavaIntentFilter Fail FindClass: IntentFilter : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(filterKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaIntentFilter Fail GetMethodID: IntentFilter : %d!\n", __LINE__);

    jobject jfilter = env->NewObject(filterKlass, m);
    CheckErrorAndLog(env, "GetJavaIntentFilter Fail NewObject: IntentFilter : %d!\n", __LINE__);

    AutoPtr<ArrayOf<String> > actions;
    filter->GetActions((ArrayOf<String>**)&actions);
    if (actions != NULL) {
        jmethodID m = env->GetMethodID(filterKlass, "addAction", "(Ljava/lang/String;)V");
        CheckErrorAndLog(env, "GetJavaIntentFilter GetMethodID: addAction : %d!\n", __LINE__);
        for (Int32 i = 0; i < actions->GetLength(); i++) {
            jobject jaction = ElUtil::GetJavaString(env, (*actions)[i]);

            env->CallVoidMethod(jfilter, m, jaction);
            CheckErrorAndLog(env, "GetJavaIntentFilter CallObjectMethod: addAction : %d!\n", __LINE__);
            env->DeleteLocalRef(jaction);
        }
    }

    AutoPtr<ArrayOf<String> > categories;
    filter->GetCategories((ArrayOf<String>**)&categories);
    if (categories != NULL) {
        jmethodID m = env->GetMethodID(filterKlass, "addCategory", "(Ljava/lang/String;)V");
        CheckErrorAndLog(env, "GetJavaIntentFilter GetMethodID: addCategory : %d!\n", __LINE__);
        for (Int32 i = 0; i < categories->GetLength(); i++) {
            jobject jcategorie = ElUtil::GetJavaString(env, (*categories)[i]);

            env->CallVoidMethod(jfilter, m, jcategorie);
            CheckErrorAndLog(env, "GetJavaIntentFilter CallObjectMethod: addCategory : %d!\n", __LINE__);
            env->DeleteLocalRef(jcategorie);
        }
    }

    AutoPtr<ArrayOf<String> > schemes;
    filter->GetSchemes((ArrayOf<String>**)&schemes);
    if (schemes != NULL) {
        jmethodID m = env->GetMethodID(filterKlass, "addDataScheme", "(Ljava/lang/String;)V");
        CheckErrorAndLog(env, "GetJavaIntentFilter GetMethodID: addDataScheme : %d!\n", __LINE__);
        for (Int32 i = 0; i < schemes->GetLength(); i++) {
            jobject jscheme = ElUtil::GetJavaString(env, (*schemes)[i]);

            env->CallVoidMethod(jfilter, m, jscheme);
            CheckErrorAndLog(env, "GetJavaIntentFilter CallObjectMethod: addDataScheme : %d!\n", __LINE__);
            env->DeleteLocalRef(jscheme);
        }
    }

    AutoPtr<ArrayOf<String> > types;
    filter->GetTypes((ArrayOf<String>**)&types);
    if (types != NULL) {
        jmethodID m = env->GetMethodID(filterKlass, "addDataType", "(Ljava/lang/String;)V");
        CheckErrorAndLog(env, "GetJavaIntentFilter GetMethodID: addDataType : %d!\n", __LINE__);
        for (Int32 i = 0; i < types->GetLength(); i++) {
            jobject jtype = ElUtil::GetJavaString(env, (*types)[i]);

            env->CallVoidMethod(jfilter, m, jtype);
            CheckErrorAndLog(env, "GetJavaIntentFilter CallObjectMethod: addDataType : %d!\n", __LINE__);
            env->DeleteLocalRef(jtype);
        }
    }

    Int32 count;
    filter->CountDataSchemeSpecificParts(&count);
    if (count > 0) {
        jmethodID m = env->GetMethodID(filterKlass, "addDataSchemeSpecificPart", "(Ljava/lang/String;I)V");
        CheckErrorAndLog(env, "GetJavaIntentFilter GetMethodID: addDataSchemeSpecificPart : %d!\n", __LINE__);
        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IPatternMatcher> matcher;
            filter->GetDataSchemeSpecificPart(i, (IPatternMatcher**)&matcher);
            String path;
            matcher->GetPath(&path);
            jstring jpath = GetJavaString(env, path);
            Int32 type;
            matcher->GetType(&type);

            env->CallVoidMethod(jfilter, m, jpath, type);
            CheckErrorAndLog(env, "GetJavaIntentFilter CallObjectMethod: addDataSchemeSpecificPart : %d!\n", __LINE__);
            env->DeleteLocalRef(jpath);
        }
    }

    AutoPtr<ArrayOf<IIntentFilterAuthorityEntry *> > authorities;
    filter->GetAuthorities((ArrayOf<IIntentFilterAuthorityEntry *>**)&authorities);
    if (authorities != NULL) {
        jmethodID m = env->GetMethodID(filterKlass, "addDataAuthority", "(Ljava/lang/String;Ljava/lang/String;)V");
        CheckErrorAndLog(env, "GetJavaIntentFilter GetMethodID: addDataAuthority : %d!\n", __LINE__);
        for (Int32 i = 0; i < authorities->GetLength(); i++) {
            AutoPtr<IIntentFilterAuthorityEntry> authority = (*authorities)[i];

            String host;
            authority->GetHost(&host);
            jstring jhost = ElUtil::GetJavaString(env, host);
            Int32 port = -1;
            authority->GetPort(&port);
            // TODO: StringUtils link error
            jstring jport = NULL/*ElUtil::GetJavaString(env, StringUtils::Int32ToString(port))*/;
            ALOGE("GetJavaIntentFilter() StringUtils error!");

            env->CallVoidMethod(jfilter, m, jhost, jport);
            CheckErrorAndLog(env, "GetJavaIntentFilter CallObjectMethod: addDataAuthority : %d!\n", __LINE__);
            env->DeleteLocalRef(jhost);
            env->DeleteLocalRef(jport);
        }
    }

    AutoPtr<ArrayOf<IPatternMatcher *> > dataPaths;
    filter->GetPaths((ArrayOf<IPatternMatcher *>**)&dataPaths);
    if (dataPaths != NULL) {
        jmethodID m = env->GetMethodID(filterKlass, "addDataPath", "(Ljava/lang/String;I)V");
        CheckErrorAndLog(env, "GetJavaIntentFilter GetMethodID: addDataPath : %d!\n", __LINE__);
        for (Int32 i = 0; i < dataPaths->GetLength(); i++) {
            AutoPtr<IPatternMatcher> dataPath = (*dataPaths)[i];

            String path;
            dataPath->GetPath(&path);
            jstring jpath = ElUtil::GetJavaString(env, path);
            Int32 type = -1;
            dataPath->GetType(&type);

            env->CallVoidMethod(jfilter, m, jpath, (jint)type);
            CheckErrorAndLog(env, "GetJavaIntentFilter CallObjectMethod: addDataPath : %d!\n", __LINE__);
            env->DeleteLocalRef(jpath);
        }
    }

    Int32 priority = 0;
    filter->GetPriority(&priority);
    ElUtil::SetJavaIntField(env, filterKlass, jfilter, priority, "mPriority", "GetJavaIntentFilter");

    Boolean hasPartialTypes = FALSE;
    filter->HasPartialTypes(&hasPartialTypes);
    ElUtil::SetJavaBoolField(env, filterKlass, jfilter, hasPartialTypes, "mHasPartialTypes", "GetJavaIntentFilter");

    env->DeleteLocalRef(filterKlass);
    return jfilter;
}

bool ElUtil::ToElUri(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jUri,
    /* [out] */ IUri** elUri)
{
    if (env == NULL || jUri == NULL || elUri == NULL) {
        ALOGD("ToElUri() Invalid arguments!");
        return false;
    }

    *elUri = NULL;

    jclass suriKlass = env->FindClass("android/net/Uri$StringUri");
    CheckErrorAndLog(env, "ToElUri Fail FindClass: Uri : %d!\n", __LINE__);
    jclass ouriKlass = env->FindClass("android/net/Uri$OpaqueUri");
    CheckErrorAndLog(env, "ToElUri Fail FindClass: Uri : %d!\n", __LINE__);
    jclass huriKlass = env->FindClass("android/net/Uri$HierarchicalUri");
    CheckErrorAndLog(env, "ToElUri Fail FindClass: Uri : %d!\n", __LINE__);

    if (env->IsInstanceOf(jUri, suriKlass)) {
        jclass uriKlass = env->FindClass("android/net/Uri");
        CheckErrorAndLog(env, "ToElUri Fail FindClass: Uri : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(uriKlass, "toString", "()Ljava/lang/String;");

        CheckErrorAndLog(env, "ToElUri Fail GetMethodID: toString : %d!\n", __LINE__);
        env->DeleteLocalRef(uriKlass);

        jstring jStrUri = (jstring)env->CallObjectMethod(jUri, m);
        CheckErrorAndLog(env, "ToElUri Fail CallObjectMethod: toString : %d!\n", __LINE__);

        String strUri = ToElString(env, jStrUri);
        env->DeleteLocalRef(jStrUri);

        CStringUri::New(strUri, elUri);
    }
    else if (env->IsInstanceOf(jUri, ouriKlass)) {
        const char* tag = "ToElUri() OpaqueUri";
        String NOT_CACHED("NOT CACHED"); // Uri::NOT_CACHED

        AutoPtr<IParcel> parcel;
        Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);
        parcel->WriteInt32(2); //OpaqueUri::TYPE_ID

        jclass uriKlass = env->FindClass("android/net/Uri$OpaqueUri");
        CheckErrorAndLog(env, "ToElUri Fail FindClass: OpaqueUri : %d!\n", __LINE__);

        String scheme = GetJavaStringField(env, uriKlass, jUri, "scheme", tag);
        parcel->WriteString(scheme);

        jfieldID f = env->GetFieldID(uriKlass, "ssp", "Landroid/net/Uri$Part;");
        CheckErrorAndLog(env, "%s: Fail get ssp fieldid: %s : %d!\n", "ToElUri", "ssp", __LINE__);
        jobject jssp = env->GetObjectField(jUri, f);
        CheckErrorAndLog(env, "%s: Fail get path field: %s : %d!\n", "ToElUri", "ssp", __LINE__);

        jclass partKlass = env->FindClass("android/net/Uri$AbstractPart");
        String encoded = GetJavaStringField(env, partKlass, jssp, "encoded", tag);
        String decoded = GetJavaStringField(env, partKlass, jssp, "decoded", tag);
        env->DeleteLocalRef(jssp);

        Boolean hasEncoded = !encoded.Equals(NOT_CACHED);
        Boolean hasDecoded = !decoded.Equals(NOT_CACHED);
        if (hasEncoded && hasDecoded) {
            parcel->WriteInt32(0/*Representation.BOTH*/);
            parcel->WriteString(encoded);
            parcel->WriteString(decoded);
        }
        else if (hasEncoded) {
            parcel->WriteInt32(1/*Representation.ENCODED*/);
            parcel->WriteString(encoded);
        }
        else if (hasDecoded) {
            parcel->WriteInt32(2/*Representation.DECODED*/);
            parcel->WriteString(decoded);
        }
        else {
            assert(0);
        }

        f = env->GetFieldID(uriKlass, "fragment", "Landroid/net/Uri$Part;");
        CheckErrorAndLog(env, "%s: Fail get fragment fieldid: %s : %d!\n", "ToElUri", "fragment", __LINE__);
        jobject jfragment = env->GetObjectField(jUri, f);
        CheckErrorAndLog(env, "%s: Fail get path field: %s : %d!\n", "ToElUri", "fragment", __LINE__);

        encoded = GetJavaStringField(env, partKlass, jfragment, "encoded", tag);
        decoded = GetJavaStringField(env, partKlass, jfragment, "decoded", tag);
        env->DeleteLocalRef(jfragment);

        hasEncoded = !encoded.Equals(NOT_CACHED);
        hasDecoded = !decoded.Equals(NOT_CACHED);
        if (hasEncoded && hasDecoded) {
            parcel->WriteInt32(0/*Representation.BOTH*/);
            parcel->WriteString(encoded);
            parcel->WriteString(decoded);
        }
        else if (hasEncoded) {
            parcel->WriteInt32(1/*Representation.ENCODED*/);
            parcel->WriteString(encoded);
        }
        else if (hasDecoded) {
            parcel->WriteInt32(2/*Representation.DECODED*/);
            parcel->WriteString(decoded);
        }

        COpaqueUri::New(elUri);
        parcel->SetDataPosition(0);
        IParcelable::Probe(*elUri)->ReadFromParcel(parcel);

        env->DeleteLocalRef(uriKlass);
        env->DeleteLocalRef(partKlass);
    }
    else if (env->IsInstanceOf(jUri, huriKlass)) {
        const char* tag = "ToElUri() HierarchicalUri";
        String NOT_CACHED("NOT CACHED"); // Uri::NOT_CACHED

        AutoPtr<IParcel> parcel;
        Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);
        parcel->WriteInt32(3); //HierarchicalUri::TYPE_ID

        jclass uriKlass = env->FindClass("android/net/Uri$HierarchicalUri");
        CheckErrorAndLog(env, "ToElUri Fail FindClass: HierarchicalUri : %d!\n", __LINE__);

        String scheme = GetJavaStringField(env, uriKlass, jUri, "scheme", tag);
        parcel->WriteString(scheme);

        jfieldID f = env->GetFieldID(uriKlass, "authority", "Landroid/net/Uri$Part;");
        CheckErrorAndLog(env, "%s: Fail get authority fieldid: %s : %d!\n", "ToElUri", "authority", __LINE__);
        jobject jauthority = env->GetObjectField(jUri, f);
        CheckErrorAndLog(env, "%s: Fail get path field: %s : %d!\n", "ToElUri", "authority", __LINE__);

        jclass partKlass = env->FindClass("android/net/Uri$AbstractPart");
        String encoded = GetJavaStringField(env, partKlass, jauthority, "encoded", tag);
        String decoded = GetJavaStringField(env, partKlass, jauthority, "decoded", tag);
        env->DeleteLocalRef(jauthority);

        Boolean hasEncoded = !encoded.Equals(NOT_CACHED);
        Boolean hasDecoded = !decoded.Equals(NOT_CACHED);
        if (hasEncoded && hasDecoded) {
            parcel->WriteInt32(0/*Representation.BOTH*/);
            parcel->WriteString(encoded);
            parcel->WriteString(decoded);
        }
        else if (hasEncoded) {
            parcel->WriteInt32(1/*Representation.ENCODED*/);
            parcel->WriteString(encoded);
        }
        else if (hasDecoded) {
            parcel->WriteInt32(2/*Representation.DECODED*/);
            parcel->WriteString(decoded);
        }
        else {
            assert(0);
        }

        f = env->GetFieldID(uriKlass, "path", "Landroid/net/Uri$PathPart;");
        CheckErrorAndLog(env, "%s: Fail get path fieldid: %s : %d!\n", "ToElUri", "path", __LINE__);
        jobject jpath = env->GetObjectField(jUri, f);
        CheckErrorAndLog(env, "%s: Fail get path field: %s : %d!\n", "ToElUri", "path", __LINE__);
        encoded = GetJavaStringField(env, partKlass, jpath, "encoded", tag);
        decoded = GetJavaStringField(env, partKlass, jpath, "decoded", tag);
        env->DeleteLocalRef(jpath);

        hasEncoded = !encoded.Equals(NOT_CACHED);
        hasDecoded = !decoded.Equals(NOT_CACHED);
        if (hasEncoded && hasDecoded) {
            parcel->WriteInt32(0/*Representation.BOTH*/);
            parcel->WriteString(encoded);
            parcel->WriteString(decoded);
        }
        else if (hasEncoded) {
            parcel->WriteInt32(1/*Representation.ENCODED*/);
            parcel->WriteString(encoded);
        }
        else if (hasDecoded) {
            parcel->WriteInt32(2/*Representation.DECODED*/);
            parcel->WriteString(decoded);
        }
        else {
            assert(0);
        }

        f = env->GetFieldID(uriKlass, "query", "Landroid/net/Uri$Part;");
        CheckErrorAndLog(env, "%s: Fail get query fieldid: %s : %d!\n", "ToElUri", "query", __LINE__);
        jobject jquery = env->GetObjectField(jUri, f);
        CheckErrorAndLog(env, "%s: Fail get path field: %s : %d!\n", "ToElUri", "query", __LINE__);

        encoded = GetJavaStringField(env, partKlass, jquery, "encoded", tag);
        decoded = GetJavaStringField(env, partKlass, jquery, "decoded", tag);
        env->DeleteLocalRef(jquery);

        hasEncoded = !encoded.Equals(NOT_CACHED);
        hasDecoded = !decoded.Equals(NOT_CACHED);
        if (hasEncoded && hasDecoded) {
            parcel->WriteInt32(0/*Representation.BOTH*/);
            parcel->WriteString(encoded);
            parcel->WriteString(decoded);
        }
        else if (hasEncoded) {
            parcel->WriteInt32(1/*Representation.ENCODED*/);
            parcel->WriteString(encoded);
        }
        else if (hasDecoded) {
            parcel->WriteInt32(2/*Representation.DECODED*/);
            parcel->WriteString(decoded);
        }
        else {
            assert(0);
        }

        f = env->GetFieldID(uriKlass, "fragment", "Landroid/net/Uri$Part;");
        CheckErrorAndLog(env, "%s: Fail get fragment fieldid: %s : %d!\n", "ToElUri", "fragment", __LINE__);
        jobject jfragment = env->GetObjectField(jUri, f);
        CheckErrorAndLog(env, "%s: Fail get path field: %s : %d!\n", "ToElUri", "fragment", __LINE__);

        encoded = GetJavaStringField(env, partKlass, jfragment, "encoded", tag);
        decoded = GetJavaStringField(env, partKlass, jfragment, "decoded", tag);
        env->DeleteLocalRef(jfragment);

        hasEncoded = !encoded.Equals(NOT_CACHED);
        hasDecoded = !decoded.Equals(NOT_CACHED);
        if (hasEncoded && hasDecoded) {
            parcel->WriteInt32(0/*Representation.BOTH*/);
            parcel->WriteString(encoded);
            parcel->WriteString(decoded);
        }
        else if (hasEncoded) {
            parcel->WriteInt32(1/*Representation.ENCODED*/);
            parcel->WriteString(encoded);
        }
        else if (hasDecoded) {
            parcel->WriteInt32(2/*Representation.DECODED*/);
            parcel->WriteString(decoded);
        }

        CHierarchicalUri::New(elUri);
        parcel->SetDataPosition(0);
        IParcelable::Probe(*elUri)->ReadFromParcel(parcel);

        env->DeleteLocalRef(uriKlass);
        env->DeleteLocalRef(partKlass);
    }
    else {
        ALOGE("ToElUri() others type Uri!\n");
        assert(0);
    }

    env->DeleteLocalRef(suriKlass);
    env->DeleteLocalRef(ouriKlass);
    env->DeleteLocalRef(huriKlass);
    return true;
}

// bool ElUtil::ToElPrivacySettings(
//     /* [in] */ JNIEnv* env,
//     /* [in] */ jobject jsettings,
//     /* [out] */ IPrivacySettings** settings)
// {
//     if (env == NULL || jsettings == NULL || settings == NULL) {
//         ALOGE("ToElPrivacySettings() Invalid arguments!");
//         return false;
//     }

//     const char* tag = "ToElPrivacySettings()";

//     jclass psKlass = env->FindClass("android/privacy/PrivacySettings");
//     CheckErrorAndLog(env, "ToElPrivacySettings(): Fail FindClass: PrivacySettings : %d!\n", __LINE__);

//     // private Integer _id
//     jint jid = GetJavaIntegerField(env, psKlass, jsettings, "_id", -1, tag);

//     //private String packageName;
//     String pkgNameString = GetJavaStringField(env, psKlass, jsettings, "packageName", tag);

//     // private int uid;
//     jint juid = GetJavaIntField(env, psKlass, jsettings, "uid", tag);

//     // Create new Elastos PrivacySetting object
//     CPrivacySettings::New(jid, pkgNameString, juid, TRUE, settings);

//     (*settings)->SetDeviceIdSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "deviceIdSetting", tag));
//     (*settings)->SetDeviceId(GetJavaStringField(env, psKlass, jsettings, "deviceId", tag));
//     (*settings)->SetLine1NumberSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "line1NumberSetting", tag));
//     (*settings)->SetLine1Number(GetJavaStringField(env, psKlass, jsettings, "line1Number", tag));
//     (*settings)->SetLocationGpsSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "locationGpsSetting", tag));
//     (*settings)->SetLocationGpsLat(GetJavaStringField(env, psKlass, jsettings, "locationGpsLat", tag));
//     (*settings)->SetLocationGpsLon(GetJavaStringField(env, psKlass, jsettings, "locationGpsLon", tag));
//     (*settings)->SetLocationNetworkSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "locationNetworkSetting", tag));
//     (*settings)->SetLocationNetworkLat(GetJavaStringField(env, psKlass, jsettings, "locationNetworkLat", tag));
//     (*settings)->SetLocationNetworkLon(GetJavaStringField(env, psKlass, jsettings, "locationNetworkLon", tag));
//     (*settings)->SetNetworkInfoSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "networkInfoSetting", tag));
//     (*settings)->SetSimInfoSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "simInfoSetting", tag));
//     (*settings)->SetSimSerialNumberSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "simSerialNumberSetting", tag));
//     (*settings)->SetSimSerialNumber(GetJavaStringField(env, psKlass, jsettings, "simSerialNumber", tag));
//     (*settings)->SetSubscriberIdSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "subscriberIdSetting", tag));
//     (*settings)->SetSubscriberId(GetJavaStringField(env, psKlass, jsettings, "subscriberId", tag));
//     (*settings)->SetAccountsSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "accountsSetting", tag));
//     (*settings)->SetAccountsAuthTokensSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "accountsAuthTokensSetting", tag));
//     (*settings)->SetOutgoingCallsSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "outgoingCallsSetting", tag));
//     (*settings)->SetIncomingCallsSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "incomingCallsSetting", tag));
//     (*settings)->SetContactsSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "contactsSetting", tag));
//     (*settings)->SetCalendarSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "calendarSetting", tag));
//     (*settings)->SetMmsSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "mmsSetting", tag));
//     (*settings)->SetSmsSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "smsSetting", tag));
//     (*settings)->SetCallLogSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "callLogSetting", tag));
//     (*settings)->SetBookmarksSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "bookmarksSetting", tag));
//     (*settings)->SetSystemLogsSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "systemLogsSetting", tag));
//     (*settings)->SetCameraSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "cameraSetting", tag));
//     (*settings)->SetRecordAudioSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "recordAudioSetting", tag));
//     (*settings)->SetNotificationSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "notificationSetting", tag));
//     (*settings)->SetIntentBootCompletedSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "intentBootCompletedSetting", tag));
//     (*settings)->SetSmsSendSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "smsSendSetting", tag));
//     (*settings)->SetPhoneCallSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "phoneCallSetting", tag));
//     (*settings)->SetIpTableProtectSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "ipTableProtectSetting", tag));
//     (*settings)->SetIccAccessSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "iccAccessSetting", tag));
//     (*settings)->SetAddOnManagementSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "addOnManagementSetting", tag));
//     (*settings)->SetAndroidIdSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "androidIdSetting", tag));
//     (*settings)->SetAndroidID(GetJavaStringField(env, psKlass, jsettings, "androidID", tag));
//     (*settings)->SetWifiInfoSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "wifiInfoSetting", tag));
//     (*settings)->SetSwitchConnectivitySetting((Byte)GetJavabyteField(env, psKlass, jsettings, "switchConnectivitySetting", tag));
//     (*settings)->SetSendMmsSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "sendMmsSetting", tag));
//     (*settings)->SetForceOnlineState((Byte)GetJavabyteField(env, psKlass, jsettings, "forceOnlineState", tag));
//     (*settings)->SetSwitchWifiStateSetting((Byte)GetJavabyteField(env, psKlass, jsettings, "switchWifiStateSetting", tag));

//     env->DeleteLocalRef(psKlass);

//     return true;
// }

jobject ElUtil::GetJavaInputBindResult(
    /* [in] */ JNIEnv* env,
    /* [in] */ IInputBindResult* result)
{
    if (env == NULL || result == NULL) {
        ALOGE("GetJavaInputBindResult() Invalid arguments!");
        return NULL;
    }

    AutoPtr<IIInputMethodSession>  method;
    AutoPtr<IInputChannel> channel;
    String id;
    Int32 sequence, userActionNotificationSequenceNumber;
    result->GetMethod((IIInputMethodSession**)&method);
    result->GetChannel((IInputChannel**)&channel);
    result->GetId(&id);
    result->GetSequence(&sequence);
    result->GetUserActionNotificationSequenceNumber(&userActionNotificationSequenceNumber);

    jobject jIms = NULL;
    if (method != NULL) {
        jclass imsKlass = env->FindClass("android/inputmethodservice/ElIInputMethodSessionProxy");
        CheckErrorAndLog(env, "GetJavaInputBindResult FindClass: ElIInputMethodSessionProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(imsKlass, "<init>", "(J)V");
        CheckErrorAndLog(env, "GetJavaInputBindResult GetMethodID: ElIInputMethodSessionProxy : %d!\n", __LINE__);

        jIms = env->NewObject(imsKlass, m, (jlong)method.Get());
        CheckErrorAndLog(env, "GetJavaInputBindResult NewObject: ElIInputMethodSessionProxy : %d!\n", __LINE__);
        method->AddRef();

        env->DeleteLocalRef(imsKlass);
    }

    jobject jchannel = NULL;
    if (channel != NULL) {
        jchannel = GetJavaInputChannel(env, channel);
    }

    jstring jid = GetJavaString(env, id);

    jclass ibrKlass = env->FindClass("com/android/internal/view/InputBindResult");
    CheckErrorAndLog(env, "GetJavaInputBindResult FindClass: InputBindResult : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(ibrKlass, "<init>", "(Lcom/android/internal/view/IInputMethodSession;Landroid/view/InputChannel;Ljava/lang/String;II)V");
    CheckErrorAndLog(env, "GetJavaInputBindResult GetMethodID: InputBindResult : %d!\n", __LINE__);

    jobject jres = env->NewObject(ibrKlass, m, jIms, jchannel, jid, (jint)sequence, (jint)userActionNotificationSequenceNumber);
    CheckErrorAndLog(env, "GetJavaInputBindResult GetMethodID: InputBindResult : %d!\n", __LINE__);

    env->DeleteLocalRef(ibrKlass);
    env->DeleteLocalRef(jIms);
    env->DeleteLocalRef(jid);

    return jres;
}

jobject ElUtil::GetJavaInputChannel(
    /* [in] */ JNIEnv* env,
    /* [in] */ IInputChannel* channel)
{
    if (env == NULL || channel == NULL){
        ALOGE("GetJavaInputChannel() INVALID params");
        return NULL;
    }

    AutoPtr<IParcel> parcel;
    CParcel::New((IParcel**)&parcel);

    IParcelable::Probe(channel)->WriteToParcel(parcel);
    parcel->SetDataPosition(0);
    Handle32 source;
    parcel->GetElementPayload(&source);

    jclass parcelClass = env->FindClass("android/os/Parcel");
    CheckErrorAndLog(env, "FindClass: Parcel : %d!\n", __LINE__);

    jmethodID m = env->GetStaticMethodID(parcelClass, "obtain", "(J)Landroid/os/Parcel;");
    CheckErrorAndLog(env, "GetMethodID: obtain : %d!\n", __LINE__);

    jobject jparcel = env->CallStaticObjectMethod(parcelClass, m, (Int64)source);
    CheckErrorAndLog(env, "CallStaticObjectMethod: obtain : %d!\n", __LINE__);

    jclass inputChannelClass = env->FindClass("android/view/InputChannel");
    CheckErrorAndLog(env, "GetJavaInputChannel FindClass: InputChannel : %d!\n", __LINE__);

    m = env->GetMethodID(inputChannelClass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "GetJavaInputChannel Fail GetMethodID: InputChannel %d", __LINE__);

    jobject jchannel = env->NewObject(inputChannelClass, m);
    ElUtil::CheckErrorAndLog(env, "GetJavaInputChannel Fail NewObject: InputChannel %d", __LINE__);

    m = env->GetMethodID(inputChannelClass, "readFromParcel", "(Landroid/os/Parcel;)V");
    CheckErrorAndLog(env, "GetMethodID: readFromParcel : %d!\n", __LINE__);

    env->CallVoidMethod(jchannel, m, jparcel);
    CheckErrorAndLog(env, "CallVoidMethod: readFromParcel : %d!\n", __LINE__);

    env->DeleteLocalRef(parcelClass);
    env->DeleteLocalRef(jparcel);
    env->DeleteLocalRef(inputChannelClass);

    return jchannel;
}

// jobject ElUtil::GetJavaPrivacySettings(
//     /* [in] */ JNIEnv* env,
//     /* [in] */ IPrivacySettings* prSettings)
// {
//     if (env == NULL || prSettings == NULL) {
//         ALOGD("GetJavaPrivacySettings() Invalid arguments!");
//         return NULL;
//     }

//     jclass psKlass = env->FindClass("android/privacy/PrivacySettings");
//     CheckErrorAndLog(env, "GetJavaPrivacySettings Fail FindClass: PrivacySettings : %d!\n", __LINE__);

//     jmethodID m = env->GetMethodID(psKlass, "<init>", "(Ljava/lang/Integer;Ljava/lang/String;I)V");
//     CheckErrorAndLog(env, "GetJavaPrivacySettings Fail GetMethodID: PrivacySettings : %d!\n", __LINE__);

//     Int32 _id;
//     prSettings->GetId(&_id);
//     String packageName;
//     prSettings->GetPackageName(&packageName);
//     jobject jpackageName = ElUtil::GetJavaString(env, packageName);
//     Int32 uid;
//     prSettings->GetUid(&uid);

//     // Here we did not set uid
//     jobject jprSettings = env->NewObject(psKlass, m, /*(jint)_id*/NULL, jpackageName, (jint)uid);
//     CheckErrorAndLog(env, "GetJavaPrivacySettings Fail NewObject: PrivacySettings : %d!\n", __LINE__);

//     env->DeleteLocalRef(jpackageName);

//     const char* tag = "GetJavaPrivacySettings";
//     Byte tempByteSetting = 0;
//     String tempStringSetting("");

//     // Set id here
//     SetJavaIntegerField(env, psKlass, jprSettings, _id, "_id", tag);

//     // Set settings fields
//     prSettings->GetDeviceIdSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "deviceIdSetting", tag);
//     prSettings->GetDeviceId(&tempStringSetting);
//     SetJavaStringField(env, psKlass, jprSettings, tempStringSetting, "deviceId", tag);
//     prSettings->GetLine1NumberSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "line1NumberSetting", tag);
//     prSettings->GetLine1Number(&tempStringSetting);
//     SetJavaStringField(env, psKlass, jprSettings, tempStringSetting, "line1Number", tag);
//     prSettings->GetLocationGpsSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "locationGpsSetting", tag);
//     prSettings->GetLocationGpsLat(&tempStringSetting);
//     SetJavaStringField(env, psKlass, jprSettings, tempStringSetting, "locationGpsLat", tag);
//     prSettings->GetLocationGpsLon(&tempStringSetting);
//     SetJavaStringField(env, psKlass, jprSettings, tempStringSetting, "locationGpsLon", tag);
//     prSettings->GetLocationNetworkSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "locationNetworkSetting", tag);
//     prSettings->GetLocationNetworkLat(&tempStringSetting);
//     SetJavaStringField(env, psKlass, jprSettings, tempStringSetting, "locationNetworkLat", tag);
//     prSettings->GetLocationNetworkLon(&tempStringSetting);
//     SetJavaStringField(env, psKlass, jprSettings, tempStringSetting, "locationNetworkLon", tag);
//     prSettings->GetNetworkInfoSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "networkInfoSetting", tag);
//     prSettings->GetSimInfoSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "simInfoSetting", tag);
//     prSettings->GetSimSerialNumberSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "simSerialNumberSetting", tag);
//     prSettings->GetSimSerialNumber(&tempStringSetting);
//     SetJavaStringField(env, psKlass, jprSettings, tempStringSetting, "simSerialNumber", tag);
//     prSettings->GetSubscriberIdSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "subscriberIdSetting", tag);
//     prSettings->GetSubscriberId(&tempStringSetting);
//     SetJavaStringField(env, psKlass, jprSettings, tempStringSetting, "subscriberId", tag);
//     prSettings->GetAccountsSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "accountsSetting", tag);
//     prSettings->GetAccountsAuthTokensSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "accountsAuthTokensSetting", tag);
//     prSettings->GetOutgoingCallsSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "outgoingCallsSetting", tag);
//     prSettings->GetIncomingCallsSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "incomingCallsSetting", tag);
//     prSettings->GetContactsSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "contactsSetting", tag);
//     prSettings->GetCalendarSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "calendarSetting", tag);
//     prSettings->GetMmsSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "mmsSetting", tag);
//     prSettings->GetSmsSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "smsSetting", tag);
//     prSettings->GetCallLogSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "callLogSetting", tag);
//     prSettings->GetBookmarksSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "bookmarksSetting", tag);
//     prSettings->GetSystemLogsSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "systemLogsSetting", tag);
//     prSettings->GetCameraSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "cameraSetting", tag);
//     prSettings->GetRecordAudioSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "recordAudioSetting", tag);
//     prSettings->GetNotificationSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "notificationSetting", tag);
//     prSettings->GetIntentBootCompletedSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "intentBootCompletedSetting", tag);
//     prSettings->GetSmsSendSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "smsSendSetting", tag);
//     prSettings->GetPhoneCallSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "phoneCallSetting", tag);
//     prSettings->GetIpTableProtectSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "ipTableProtectSetting", tag);
//     prSettings->GetIccAccessSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "iccAccessSetting", tag);
//     prSettings->GetAddOnManagementSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "addOnManagementSetting", tag);
//     prSettings->GetAndroidIdSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "androidIdSetting", tag);
//     prSettings->GetAndroidID(&tempStringSetting);
//     SetJavaStringField(env, psKlass, jprSettings, tempStringSetting, "androidID", tag);
//     prSettings->GetWifiInfoSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "wifiInfoSetting", tag);
//     prSettings->GetSwitchConnectivitySetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "switchConnectivitySetting", tag);
//     prSettings->GetSendMmsSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "sendMmsSetting", tag);
//     prSettings->GetForceOnlineState(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "forceOnlineState", tag);
//     prSettings->GetSwitchWifiStateSetting(&tempByteSetting);
//     SetJavabyteField(env, psKlass, jprSettings, tempByteSetting, "switchWifiStateSetting", tag);

//     env->DeleteLocalRef(psKlass);

//     return jprSettings;
// }

jobject ElUtil::GetJavaParceledListSlice(
    /* [in] */ JNIEnv* env,
    /* [in] */ const ClassID& clsid,
    /* [in] */ IParceledListSlice* slice)
{
    if (env == NULL || slice == NULL) {
        ALOGD("GetJavaParceledListSlice() Invalid arguments!");
        return NULL;
    }

    AutoPtr<IList> list;
    slice->GetList((IList**)&list);
    jclass listKlass = env->FindClass("java/util/ArrayList");
    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jlist = env->NewObject(listKlass, m);
    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail NewObject: ArrayList : %d!\n", __LINE__);

    jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail GetMethodID: add : %d!\n", __LINE__);

    Int32 size;
    list->GetSize(&size);
    for (Int32 i = 0; i < size; i++) {
        AutoPtr<IInterface> item;
        list->Get(i, (IInterface**)&item);
        if (clsid == ECLSID_CApplicationInfo) {
            AutoPtr<IApplicationInfo> appInfo = IApplicationInfo::Probe(item);
            if (appInfo != NULL) {
                jobject jAppInfo = ElUtil::GetJavaApplicationInfo(env, appInfo);
                if (jAppInfo != NULL) {
                    env->CallBooleanMethod(jlist, mAdd, jAppInfo);
                    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(jAppInfo);
                }
            }
        }
        else if (clsid == ECLSID_CPackageInfo) {
            AutoPtr<IPackageInfo> packageInfo = IPackageInfo::Probe(item);
            if (packageInfo != NULL) {
                jobject jpackageInfo = ElUtil::GetJavaPackageInfo(env, packageInfo);
                if (jpackageInfo != NULL) {
                    env->CallBooleanMethod(jlist, mAdd, jpackageInfo);
                    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(jpackageInfo);
                }
            }
        }
        else if (clsid == ECLSID_CUriPermission) {
            AutoPtr<IUriPermission> uriPermission = IUriPermission::Probe(item);
            if (uriPermission != NULL) {
                jobject juriPermission = ElUtil::GetJavaUriPermission(env, uriPermission);
                if (juriPermission != NULL) {
                    env->CallBooleanMethod(jlist, mAdd, juriPermission);
                    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(juriPermission);
                }
            }
        }
        else {
            ALOGE("GetJavaParceledListSlice() others?");
            assert(0);
        }
    }

    jclass plsKlass = env->FindClass("android/content/pm/ParceledListSlice");
    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail FindClass: ParceledListSlice : %d!\n", __LINE__);

    m = env->GetMethodID(plsKlass, "<init>", "(Ljava/util/List;)V");
    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail GetMethodID: ParceledListSlice : %d!\n", __LINE__);

    jobject jslice = env->NewObject(plsKlass, m, jlist);
    CheckErrorAndLog(env, "GetJavaParceledListSlice Fail NewObject: ParceledListSlice : %d!\n", __LINE__);

    env->DeleteLocalRef(plsKlass);
    env->DeleteLocalRef(jlist);

    return jslice;
}

void ElUtil::SetMemoryInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jmemInfo,
    /* [in] */ IActivityManagerMemoryInfo* memInfo)
{
    if (env == NULL || jmemInfo == NULL || memInfo == NULL) {
        ALOGE("SetMemoryInfo() Invalid arguments!");
        return;
    }

    jclass minfoKlass = env->FindClass("android/app/ActivityManager$MemoryInfo");
    CheckErrorAndLog(env, "SetMemoryInfo Fail FindClass: MemoryInfo : %d!\n", __LINE__);

    Int64 tmpLong;
    memInfo->GetAvailMem(&tmpLong);
    jfieldID f = env->GetFieldID(minfoKlass, "availMem", "J");
    CheckErrorAndLog(env, "SetMemoryInfo GetFieldID: availMem : %d!\n", __LINE__);

    env->SetLongField(jmemInfo, f, (jlong)tmpLong);
    CheckErrorAndLog(env, "SetMemoryInfo SetLongField: availMem : %d!\n", __LINE__);

    memInfo->GetTotalMem(&tmpLong);
    f = env->GetFieldID(minfoKlass, "totalMem", "J");
    CheckErrorAndLog(env, "SetMemoryInfo GetFieldID: totalMem : %d!\n", __LINE__);

    env->SetLongField(jmemInfo, f, (jlong)tmpLong);
    CheckErrorAndLog(env, "SetMemoryInfo SetLongField: totalMem : %d!\n", __LINE__);

    memInfo->GetThreshold(&tmpLong);
    f = env->GetFieldID(minfoKlass, "threshold", "J");
    CheckErrorAndLog(env, "SetMemoryInfo GetFieldID: threshold : %d!\n", __LINE__);

    env->SetLongField(jmemInfo, f, (jlong)tmpLong);
    CheckErrorAndLog(env, "SetMemoryInfo SetLongField: threshold : %d!\n", __LINE__);

    Boolean lowMemory;
    memInfo->GetLowMemory(&lowMemory);
    f = env->GetFieldID(minfoKlass, "lowMemory", "Z");
    CheckErrorAndLog(env, "SetMemoryInfo GetFieldID: lowMemory : %d!\n", __LINE__);

    env->SetBooleanField(jmemInfo, f, (jboolean)lowMemory);
    CheckErrorAndLog(env, "SetMemoryInfo SetBooleanField: lowMemory : %d!\n", __LINE__);

    memInfo->GetHiddenAppThreshold(&tmpLong);
    f = env->GetFieldID(minfoKlass, "hiddenAppThreshold", "J");
    CheckErrorAndLog(env, "SetMemoryInfo GetFieldID: hiddenAppThreshold : %d!\n", __LINE__);

    env->SetLongField(jmemInfo, f, (jlong)tmpLong);
    CheckErrorAndLog(env, "SetMemoryInfo SetLongField: hiddenAppThreshold : %d!\n", __LINE__);

    memInfo->GetSecondaryServerThreshold(&tmpLong);
    f = env->GetFieldID(minfoKlass, "secondaryServerThreshold", "J");
    CheckErrorAndLog(env, "SetMemoryInfo GetFieldID: secondaryServerThreshold : %d!\n", __LINE__);

    env->SetLongField(jmemInfo, f, (jlong)tmpLong);
    CheckErrorAndLog(env, "SetMemoryInfo SetLongField: secondaryServerThreshold : %d!\n", __LINE__);

    memInfo->GetVisibleAppThreshold(&tmpLong);
    f = env->GetFieldID(minfoKlass, "visibleAppThreshold", "J");
    CheckErrorAndLog(env, "SetMemoryInfo GetFieldID: visibleAppThreshold : %d!\n", __LINE__);

    env->SetLongField(jmemInfo, f, (jlong)tmpLong);
    CheckErrorAndLog(env, "SetMemoryInfo SetLongField: visibleAppThreshold : %d!\n", __LINE__);

    memInfo->GetForegroundAppThreshold(&tmpLong);
    f = env->GetFieldID(minfoKlass, "foregroundAppThreshold", "J");
    CheckErrorAndLog(env, "SetMemoryInfo GetFieldID: foregroundAppThreshold : %d!\n", __LINE__);

    env->SetLongField(jmemInfo, f, (jlong)tmpLong);
    CheckErrorAndLog(env, "SetMemoryInfo SetLongField: foregroundAppThreshold : %d!\n", __LINE__);

    env->DeleteLocalRef(minfoKlass);
}

jobject ElUtil::GetJavaRunningServiceInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityManagerRunningServiceInfo* rSerInfo)
{
    if (env == NULL || rSerInfo == NULL) {
        ALOGE("GetJavaRunningServiceInfo() Invalid arguments!");
        return NULL;
    }

    jclass rSerInKlass = env->FindClass("android/app/ActivityManager$RunningServiceInfo");
    CheckErrorAndLog(env, "GetJavaRunningServiceInfo Fail FindClass: RunningServiceInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(rSerInKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaRunningServiceInfo Fail GetMethodID: RunningServiceInfo : %d!\n", __LINE__);

    jobject jrSerInfo = env->NewObject(rSerInKlass, m);
    CheckErrorAndLog(env, "GetJavaRunningServiceInfo Fail NewObject: RunningServiceInfo : %d!\n", __LINE__);

    AutoPtr<IComponentName> service;
    rSerInfo->GetService((IComponentName**)&service);
    if (service != NULL) {
        jobject jservice = ElUtil::GetJavaComponentName(env, service);
        if (jservice != NULL) {
            jfieldID f = env->GetFieldID(rSerInKlass, "service", "Landroid/content/ComponentName;");
            CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: ComponentName : %d!\n", __LINE__);

            env->SetObjectField(jrSerInfo, f, jservice);
            CheckErrorAndLog(env, "GetJavaResolveInfo SetObjectField: jrSerInfo : %d!\n", __LINE__);
            env->DeleteLocalRef(jservice);
        }
    }

    Int32 tmpInt;
    rSerInfo->GetPid(&tmpInt);
    jfieldID f = env->GetFieldID(rSerInKlass, "pid", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: pid : %d!\n", __LINE__);

    env->SetIntField(jrSerInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetLongField: pid : %d!\n", __LINE__);

    rSerInfo->GetUid(&tmpInt);
    f = env->GetFieldID(rSerInKlass, "uid", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: uid : %d!\n", __LINE__);

    env->SetIntField(jrSerInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: uid : %d!\n", __LINE__);

    String process;
    rSerInfo->GetProcess(&process);
    if (!process.IsNull()) {
        jstring jprocess = GetJavaString(env, process);

        f = env->GetFieldID(rSerInKlass, "process", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: process : %d!\n", __LINE__);

        env->SetObjectField(jrSerInfo, f, jprocess);
        CheckErrorAndLog(env, "GetJavaResolveInfo SetObjectField: process : %d!\n", __LINE__);
        env->DeleteLocalRef(jprocess);
    }

    Boolean tmpBoolean;
    rSerInfo->GetForeground(&tmpBoolean);
    f = env->GetFieldID(rSerInKlass, "foreground", "Z");
    CheckErrorAndLog(env, "GetJavaResolveInfo Fail GetFieldID: foreground : %d!\n", __LINE__);

    env->SetBooleanField(jrSerInfo, f, tmpBoolean);
    CheckErrorAndLog(env, "GetJavaResolveInfo Fail SetBooleanField: foreground : %d!\n", __LINE__);

    Int64 tmpLong;
    rSerInfo->GetActiveSince(&tmpLong);
    f = env->GetFieldID(rSerInKlass, "activeSince", "J");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: activeSince : %d!\n", __LINE__);

    env->SetLongField(jrSerInfo, f, (jint)tmpLong);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetLongField: activeSince : %d!\n", __LINE__);

    rSerInfo->IsStarted(&tmpBoolean);
    f = env->GetFieldID(rSerInKlass, "started", "Z");
    CheckErrorAndLog(env, "GetJavaResolveInfo Fail GetFieldID: started : %d!\n", __LINE__);

    env->SetBooleanField(jrSerInfo, f, tmpBoolean);
    CheckErrorAndLog(env, "GetJavaResolveInfo Fail SetBooleanField: started : %d!\n", __LINE__);

    rSerInfo->GetClientCount(&tmpInt);
    f = env->GetFieldID(rSerInKlass, "clientCount", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: clientCount : %d!\n", __LINE__);

    env->SetIntField(jrSerInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: clientCount : %d!\n", __LINE__);

    rSerInfo->GetCrashCount(&tmpInt);
    f = env->GetFieldID(rSerInKlass, "crashCount", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: crashCount : %d!\n", __LINE__);

    env->SetIntField(jrSerInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: crashCount : %d!\n", __LINE__);

    rSerInfo->GetLastActivityTime(&tmpLong);
    f = env->GetFieldID(rSerInKlass, "lastActivityTime", "J");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: lastActivityTime : %d!\n", __LINE__);

    env->SetLongField(jrSerInfo, f, (jint)tmpLong);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetLongField: lastActivityTime : %d!\n", __LINE__);

    rSerInfo->GetRestarting(&tmpLong);
    f = env->GetFieldID(rSerInKlass, "restarting", "J");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: restarting : %d!\n", __LINE__);

    env->SetLongField(jrSerInfo, f, (jint)tmpLong);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetLongField: restarting : %d!\n", __LINE__);

    String clientPackage;
    rSerInfo->GetProcess(&clientPackage);
    if (!clientPackage.IsNull()) {
        jstring jclientPackage = GetJavaString(env, clientPackage);

        f = env->GetFieldID(rSerInKlass, "clientPackage", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: clientPackage : %d!\n", __LINE__);

        env->SetObjectField(jrSerInfo, f, jclientPackage);
        CheckErrorAndLog(env, "GetJavaResolveInfo SetObjectField: clientPackage : %d!\n", __LINE__);
        env->DeleteLocalRef(jclientPackage);
    }

    rSerInfo->GetClientLabel(&tmpInt);
    f = env->GetFieldID(rSerInKlass, "clientLabel", "I");
    CheckErrorAndLog(env, "GetJavaResolveInfo GetFieldID: clientLabel : %d!\n", __LINE__);

    env->SetIntField(jrSerInfo, f, (jint)tmpInt);
    CheckErrorAndLog(env, "GetJavaResolveInfo SetIntField: clientLabel : %d!\n", __LINE__);

    env->DeleteLocalRef(rSerInKlass);

    return jrSerInfo;
}

jobject ElUtil::GetJavaProviderInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IProviderInfo* providerInfo)
{
    if (env == NULL || providerInfo == NULL) {
        ALOGE("GetJavaProviderInfo() Invalid arguments!");
        return NULL;
    }

    jobject jproviderInfo = NULL;

    jclass piKlass = env->FindClass("android/content/pm/ProviderInfo");
    CheckErrorAndLog(env, "GetJavaProviderInfo Fail FindClass: ProviderInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(piKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetMethodID: ProviderInfo : %d!\n", __LINE__);

    jproviderInfo = env->NewObject(piKlass, m);
    CheckErrorAndLog(env, "GetJavaProviderInfo Fail NewObject: ProviderInfo : %d!\n", __LINE__);

    String authority;
    providerInfo->GetAuthority(&authority);
    jstring jauthority = ElUtil::GetJavaString(env, authority);
    if (jauthority != NULL) {
        jfieldID f = env->GetFieldID(piKlass, "authority", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: authority : %d!\n", __LINE__);

        env->SetObjectField(jproviderInfo, f, jauthority);
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetIntField: jauthority : %d!\n", __LINE__);
        env->DeleteLocalRef(jauthority);
    }

    String readPermission;
    providerInfo->GetReadPermission(&readPermission);
    jstring jreadPermission = ElUtil::GetJavaString(env, readPermission);
    if (jreadPermission != NULL) {
        jfieldID f = env->GetFieldID(piKlass, "readPermission", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: readPermission : %d!\n", __LINE__);

        env->SetObjectField(jproviderInfo, f, jreadPermission);
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetIntField: jreadPermission : %d!\n", __LINE__);
        env->DeleteLocalRef(jreadPermission);
    }

    String writePermission;
    providerInfo->GetWritePermission(&writePermission);
    jstring jwritePermission = ElUtil::GetJavaString(env, writePermission);
    if (jwritePermission != NULL) {
        jfieldID f = env->GetFieldID(piKlass, "writePermission", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: writePermission : %d!\n", __LINE__);

        env->SetObjectField(jproviderInfo, f, jwritePermission);
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetIntField: jwritePermission : %d!\n", __LINE__);
        env->DeleteLocalRef(jwritePermission);
    }

    Boolean tempBool;
    providerInfo->GetGrantUriPermissions(&tempBool);
    jfieldID f = env->GetFieldID(piKlass, "grantUriPermissions", "Z");
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: grantUriPermissions : %d!\n", __LINE__);

    env->SetBooleanField(jproviderInfo, f, (jboolean)tempBool);
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetBooleanField: grantUriPermissions : %d!\n", __LINE__);

    AutoPtr<ArrayOf<IPatternMatcher*> > uriPermissionPatterns;
    providerInfo->GetUriPermissionPatterns((ArrayOf<IPatternMatcher*>**)&uriPermissionPatterns);
    if (uriPermissionPatterns != NULL) {
        jclass pmerKlass = env->FindClass("android/os/PatternMatcher");
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail FindClass: PatternMatcher : %d!\n", __LINE__);

        Int32 count = uriPermissionPatterns->GetLength();
        jobjectArray jpmerArray = env->NewObjectArray((jsize)count, pmerKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail NewObjectArray: PatternMatcher : %d!\n", __LINE__);

        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IPatternMatcher> matcher = (*uriPermissionPatterns)[i];
            String path;
            IPatternMatcher::Probe(matcher)->GetPath(&path);
            jstring jpath = ElUtil::GetJavaString(env, path);
            Int32 type;
            IPatternMatcher::Probe(matcher)->GetType(&type);

            m = env->GetMethodID(pmerKlass, "<init>", "(Ljava/lang/String;I)V");
            CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetMethodID: PatternMatcher : %d!\n", __LINE__);

            jobject jmatcher = env->NewObject(pmerKlass, m, jpath, (jint)type);
            CheckErrorAndLog(env, "GetJavaProviderInfo Fail NewObject: PatternMatcher : %d!\n", __LINE__);

            env->SetObjectArrayElement(jpmerArray, i, jmatcher);
            CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetObjectArrayElement: PatternMatcher : %d!\n", __LINE__);

            env->DeleteLocalRef(jpath);
            env->DeleteLocalRef(jmatcher);
        }

        f = env->GetFieldID(piKlass, "uriPermissionPatterns", "[Landroid/os/PatternMatcher;");
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: uriPermissionPatterns[] : %d!\n", __LINE__);

        env->SetObjectField(jproviderInfo, f, jpmerArray);
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetObjectField: jpmerArray : %d!\n", __LINE__);

        env->DeleteLocalRef(jpmerArray);
        env->DeleteLocalRef(pmerKlass);
    }

    AutoPtr<ArrayOf<IPathPermission*> > pathPermissions;
    providerInfo->GetPathPermissions((ArrayOf<IPathPermission*>**)&pathPermissions);
    if (pathPermissions != NULL) {
        jclass ppmissionsKlass = env->FindClass("android/content/pm/PathPermission");
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail FindClass: PathPermission : %d!\n", __LINE__);

        Int32 count = pathPermissions->GetLength();
        jobjectArray jppmissionsArray = env->NewObjectArray((jsize)count, ppmissionsKlass, 0);
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail NewObjectArray: PathPermission : %d!\n", __LINE__);

        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IPathPermission> permission = (*pathPermissions)[i];
            String path;
            IPatternMatcher::Probe(permission)->GetPath(&path);
            jstring jpath = ElUtil::GetJavaString(env, path);
            Int32 type;
            IPatternMatcher::Probe(permission)->GetType(&type);
            String readPermission;
            permission->GetReadPermission(&readPermission);
            jstring jreadPermission = ElUtil::GetJavaString(env, readPermission);
            String writePermission;
            permission->GetWritePermission(&writePermission);
            jstring jwritePermission = ElUtil::GetJavaString(env, writePermission);

            m = env->GetMethodID(ppmissionsKlass, "<init>", "(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V");
            CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetMethodID: PathPermission : %d!\n", __LINE__);

            jobject jppmissions = env->NewObject(ppmissionsKlass, m, jpath, (jint)type, jreadPermission, jwritePermission);
            CheckErrorAndLog(env, "GetJavaProviderInfo Fail NewObject: PathPermission : %d!\n", __LINE__);

            env->SetObjectArrayElement(jppmissionsArray, i, jppmissions);
            CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetObjectArrayElement: PathPermission : %d!\n", __LINE__);

            env->DeleteLocalRef(jpath);
            env->DeleteLocalRef(jreadPermission);
            env->DeleteLocalRef(jwritePermission);
            env->DeleteLocalRef(jppmissions);
        }

        f = env->GetFieldID(piKlass, "pathPermissions", "[Landroid/content/pm/PathPermission;");
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: pathPermissions[] : %d!\n", __LINE__);

        env->SetObjectField(jproviderInfo, f, jppmissionsArray);
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetObjectField: jppmissionsArray : %d!\n", __LINE__);

        env->DeleteLocalRef(ppmissionsKlass);
        env->DeleteLocalRef(jppmissionsArray);
    }

    providerInfo->GetMultiprocess(&tempBool);
    f = env->GetFieldID(piKlass, "multiprocess", "Z");
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: multiprocess : %d!\n", __LINE__);

    env->SetBooleanField(jproviderInfo, f, (jboolean)tempBool);
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetBooleanField: multiprocess : %d!\n", __LINE__);

    Int32 tempInt;
    providerInfo->GetInitOrder(&tempInt);
    f = env->GetFieldID(piKlass, "initOrder", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: initOrder : %d!\n", __LINE__);

    env->SetIntField(jproviderInfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetIntField: initOrder : %d!\n", __LINE__);

    providerInfo->GetFlags(&tempInt);
    f = env->GetFieldID(piKlass, "flags", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: flags : %d!\n", __LINE__);

    env->SetIntField(jproviderInfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetIntField: flags : %d!\n", __LINE__);

    providerInfo->GetIsSyncable(&tempBool);
    f = env->GetFieldID(piKlass, "isSyncable", "Z");
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: isSyncable %d", __LINE__);

    env->SetBooleanField(jproviderInfo, f, (jboolean)tempBool);
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetBooleanField: isSyncable %d", __LINE__);

    AutoPtr<IComponentInfo> comInfo = IComponentInfo::Probe(providerInfo);
    SetJavaComponentInfo(env, piKlass, jproviderInfo, comInfo);

    env->DeleteLocalRef(piKlass);

    return jproviderInfo;
}

jobject ElUtil::GetJavaContentProviderHolder(
    /* [in] */ JNIEnv* env,
    /* [in] */ IContentProviderHolder* holder)
{
    if (env == NULL || holder == NULL) {
        ALOGE("GetJavaContentProviderHolder() Invalid arguments!");
        return NULL;
    }

    jclass holderKlass = env->FindClass("android/app/IActivityManager$ContentProviderHolder");
    CheckErrorAndLog(env, "GetJavaContentProviderHolder Fail FindClass: ContentProviderHolder : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(holderKlass, "<init>", "(Landroid/content/pm/ProviderInfo;)V");
    CheckErrorAndLog(env, "GetJavaContentProviderHolder Fail GetMethodID: ContentProviderHolder : %d!\n", __LINE__);


    AutoPtr<IProviderInfo> info;
    holder->GetProviderInfo((IProviderInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaProviderInfo(env, info);
    }

    jobject jholder = env->NewObject(holderKlass, m, jinfo);
    CheckErrorAndLog(env, "GetJavaContentProviderHolder Fail NewObject: ContentProviderHolder : %d!\n", __LINE__);
    env->DeleteLocalRef(jinfo);

    Boolean noReleaseNeeded;
    holder->GetNoReleaseNeeded(&noReleaseNeeded);
    jfieldID f = env->GetFieldID(holderKlass, "noReleaseNeeded", "Z");
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: noReleaseNeeded : %d!\n", __LINE__);

    env->SetBooleanField(jholder, f, (jboolean)noReleaseNeeded);
    ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetBooleanField: noReleaseNeeded : %d!\n", __LINE__);

    AutoPtr<IIContentProvider> provider;
    holder->GetContentProvider((IIContentProvider**)&provider);
    if (provider != NULL) {
        jclass pClass = env->FindClass("android/content/ElContentProviderProxy");
        CheckErrorAndLog(env, "FindClass ElContentProviderProxy fail : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(pClass, "<init>", "(J)V");
        CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetMethodID: bundleKlass : %d!\n", __LINE__);

        jobject jprovider = env->NewObject(pClass, m, (jlong)provider.Get());
        CheckErrorAndLog(env, "GetJavaProviderInfo Fail NewObject: bundleKlass : %d!\n", __LINE__);
        provider->AddRef();

        f = env->GetFieldID(holderKlass, "provider", "Landroid/content/IContentProvider;");
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail GetFieldID: IContentProvider : %d!\n", __LINE__);

        env->SetObjectField(jholder, f, jprovider);
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo Fail SetObjectField: IContentProvider : %d!\n", __LINE__);

        env->DeleteLocalRef(pClass);
        env->DeleteLocalRef(jprovider);
    }

    AutoPtr<Elastos::Droid::Os::IBinder> connection;
    holder->GetConnection((Elastos::Droid::Os::IBinder**)&connection);
    if (connection != NULL) {
        jclass bClass = env->FindClass("android/os/ElBinderProxy");
        CheckErrorAndLog(env, "FindClass ElBinderProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(bClass, "<init>", "(J)V");
        CheckErrorAndLog(env, "GetJavaProviderInfo GetMethodID: ElBinderProxy : %d!\n", __LINE__);

        jobject jconnection = env->NewObject(bClass, m, (jlong)connection.Get());
        CheckErrorAndLog(env, "GetJavaProviderInfo NewObject: ElBinderProxy : %d!\n", __LINE__);
        connection->AddRef();

        f = env->GetFieldID(holderKlass, "connection", "Landroid/os/IBinder;");
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo GetFieldID: IContentProvider : %d!\n", __LINE__);

        env->SetObjectField(jholder, f, jconnection);
        ElUtil::CheckErrorAndLog(env, "GetJavaProviderInfo SetObjectField: IContentProvider : %d!\n", __LINE__);

        env->DeleteLocalRef(bClass);
        env->DeleteLocalRef(jconnection);
    }

    env->DeleteLocalRef(holderKlass);

    return jholder;
}

jobject ElUtil::GetJavaUserInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IUserInfo* userInfo)
{
    if (env == NULL || userInfo == NULL) {
        ALOGE("GetJavaUserInfo() Invalid arguments!");
        return NULL;
    }

    jobject juserInfo = NULL;

    jclass uiKlass = env->FindClass("android/content/pm/UserInfo");
    CheckErrorAndLog(env, "GetJavaUserInfo Fail FindClass: UserInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(uiKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaUserInfo Fail GetMethodID: UserInfo : %d!\n", __LINE__);

    juserInfo = env->NewObject(uiKlass, m);
    CheckErrorAndLog(env, "GetJavaUserInfo Fail NewObject: UserInfo : %d!\n", __LINE__);

    Int32 tempInt;
    userInfo->GetId(&tempInt);
    jfieldID f = env->GetFieldID(uiKlass, "id", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail GetFieldID: id : %d!\n", __LINE__);

    env->SetIntField(juserInfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail SetIntField: id : %d!\n", __LINE__);

    String name;
    userInfo->GetName(&name);
    jstring jname = ElUtil::GetJavaString(env, name);
    if (jname != NULL) {
        f = env->GetFieldID(uiKlass, "name", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "tempInt Fail GetFieldID: name : %d!\n", __LINE__);

        env->SetObjectField(juserInfo, f, jname);
        ElUtil::CheckErrorAndLog(env, "tempInt Fail SetObjectField: jname : %d!\n", __LINE__);
        env->DeleteLocalRef(jname);
    }

    String iconPath;
    userInfo->GetIconPath(&iconPath);
    jstring jiconPath = ElUtil::GetJavaString(env, iconPath);
    if (jiconPath != NULL) {
        f = env->GetFieldID(uiKlass, "iconPath", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "tempInt Fail GetFieldID: iconPath : %d!\n", __LINE__);

        env->SetObjectField(juserInfo, f, jiconPath);
        ElUtil::CheckErrorAndLog(env, "tempInt Fail SetObjectField: jname : %d!\n", __LINE__);
        env->DeleteLocalRef(jiconPath);
    }

    userInfo->GetFlags(&tempInt);
    f = env->GetFieldID(uiKlass, "flags", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail GetFieldID: flags : %d!\n", __LINE__);

    env->SetIntField(juserInfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail SetIntField: flags : %d!\n", __LINE__);

    userInfo->GetSerialNumber(&tempInt);
    f = env->GetFieldID(uiKlass, "serialNumber", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail GetFieldID: serialNumber : %d!\n", __LINE__);

    env->SetIntField(juserInfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail SetIntField: serialNumber : %d!\n", __LINE__);

    Int64 tempLong;
    userInfo->GetCreationTime(&tempLong);
    f = env->GetFieldID(uiKlass, "creationTime", "J");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail GetFieldID: creationTime : %d!\n", __LINE__);

    env->SetLongField(juserInfo, f, tempLong);
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail SetLongField: creationTime : %d!\n", __LINE__);

    userInfo->GetLastLoggedInTime(&tempLong);
    f = env->GetFieldID(uiKlass, "lastLoggedInTime", "J");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail GetFieldID: lastLoggedInTime : %d!\n", __LINE__);

    env->SetLongField(juserInfo, f, tempLong);
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail SetLongField: lastLoggedInTime : %d!\n", __LINE__);

    Boolean partial;
    userInfo->GetPartial(&partial);
    f = env->GetFieldID(uiKlass, "partial", "Z");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail GetFieldID: partial : %d!\n", __LINE__);

    env->SetBooleanField(juserInfo, f, partial);
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail SetBooleanField: partial : %d!\n", __LINE__);

    userInfo->GetProfileGroupId(&tempInt);
    f = env->GetFieldID(uiKlass, "profileGroupId", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail GetFieldID: profileGroupId : %d!\n", __LINE__);

    env->SetIntField(juserInfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail SetIntField: profileGroupId : %d!\n", __LINE__);

    Boolean guestToRemove;
    userInfo->GetGuestToRemove(&guestToRemove);
    f = env->GetFieldID(uiKlass, "guestToRemove", "Z");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail GetFieldID: guestToRemove : %d!\n", __LINE__);

    env->SetBooleanField(juserInfo, f, guestToRemove);
    ElUtil::CheckErrorAndLog(env, "GetJavaUserInfo Fail SetBooleanField: guestToRemove : %d!\n", __LINE__);

    env->DeleteLocalRef(uiKlass);

    return juserInfo;
}

jobject ElUtil::GetJavaInputMethodInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IInputMethodInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaInputMethodInfo() Invalid arguments!");
        return NULL;
    }

    jclass infoKlass = env->FindClass("android/view/inputmethod/InputMethodInfo");
    CheckErrorAndLog(env, "GetJavaInputMethodInfo FindClass: InputMethodInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(infoKlass, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetJavaInputMethodInfo GetMethodID: infoKlass : %d!\n", __LINE__);

    AutoPtr<IServiceInfo> serInfo;
    info->GetServiceInfo((IServiceInfo**)&serInfo);

    AutoPtr<IPackageItemInfo> piInfo = IPackageItemInfo::Probe(serInfo);
    String packageName;
    piInfo->GetPackageName(&packageName);
    jobject jpackageName = ElUtil::GetJavaString(env, packageName);
    String className;
    piInfo->GetName(&className);
    jobject jclassName = ElUtil::GetJavaString(env, className);
    AutoPtr<ICharSequence> label;
    piInfo->GetNonLocalizedLabel((ICharSequence**)&label);
    jstring jlabel = NULL;
    if (label != NULL) {
        String slabel;
        label->ToString(&slabel);
        jlabel = ElUtil::GetJavaString(env, slabel);
    }
    String settingsActivity;
    info->GetSettingsActivity(&settingsActivity);
    jobject jsettingsActivity = ElUtil::GetJavaString(env, settingsActivity);

    jobject jinputInfo = env->NewObject(infoKlass, m, jpackageName, jclassName, jlabel, jsettingsActivity);
    ElUtil::CheckErrorAndLog(env, "GetJavaInputMethodInfo NewObject: InputMethodInfo : %d!\n", __LINE__);

    env->DeleteLocalRef(jpackageName);
    env->DeleteLocalRef(jclassName);
    if (jlabel != NULL) {
        env->DeleteLocalRef(jlabel);
    }
    env->DeleteLocalRef(jsettingsActivity);

    Int32 tempInt;
    info->GetIsDefaultResourceId(&tempInt);
    jfieldID f = env->GetFieldID(infoKlass, "mIsDefaultResId", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail GetFieldID: mIsDefaultResId : %d!\n", __LINE__);

    env->SetIntField(jinputInfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail SetIntField: mIsDefaultResId : %d!\n", __LINE__);

    Boolean tempBool;
    info->IsAuxiliaryIme(&tempBool);
    f = env->GetFieldID(infoKlass, "mIsAuxIme", "Z");
    CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail GetFieldID: mIsAuxIme : %d!\n", __LINE__);

    env->SetBooleanField(jinputInfo, f, (jboolean)tempBool);
    CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail SetBooleanField: mIsAuxIme : %d!\n", __LINE__);

    info->SupportsSwitchingToNextInputMethod(&tempBool);
    f = env->GetFieldID(infoKlass, "mSupportsSwitchingToNextInputMethod", "Z");
    CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail GetFieldID: mSupportsSwitchingToNextInputMethod : %d!\n", __LINE__);

    env->SetBooleanField(jinputInfo, f, (jboolean)tempBool);
    CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail SetBooleanField: mSupportsSwitchingToNextInputMethod : %d!\n", __LINE__);

    Int32 size;
    info->GetSubtypeCount(&size);
    if (size > 0) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        CheckErrorAndLog(env, "GetJavaInputMethodInfo FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jobject jlist = env->NewObject(listKlass, m);
        CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail GetMethodID: add : %d!\n", __LINE__);

        for (Int32 i = 0; i < size; ++i) {
            AutoPtr<IInputMethodSubtype> subtype;
            info->GetSubtypeAt(i, (IInputMethodSubtype**)&subtype);
            jobject jsubtype = ElUtil::GetJavaInputMethodSubtype(env, subtype);

            env->CallBooleanMethod(jlist, mAdd, jsubtype);
            CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
            env->DeleteLocalRef(jsubtype);
        }

        jclass imsaKlass = env->FindClass("android/view/inputmethod/InputMethodSubtypeArray");
        CheckErrorAndLog(env, "GetJavaInputMethodInfo FindClass: InputMethodSubtypeArray : %d!\n", __LINE__);

        m = env->GetMethodID(imsaKlass, "<init>", "(Ljava/util/List;)V");
        CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail GetMethodID: InputMethodSubtypeArray : %d!\n", __LINE__);

        jobject jsubtypes = env->NewObject(imsaKlass, m, jlist);
        CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail NewObject: InputMethodSubtypeArray : %d!\n", __LINE__);

        jfieldID f = env->GetFieldID(infoKlass, "mSubtypes", "Landroid/view/inputmethod/InputMethodSubtypeArray;");
        CheckErrorAndLog(env, "GetFieldID: mSubtypes : %d!\n", __LINE__);

        env->SetObjectField(jinputInfo, f, jsubtypes);
        ElUtil::CheckErrorAndLog(env, "GetJavaInputMethodInfo Fail SetIntField: mSubtypes : %d!\n", __LINE__);

        env->DeleteLocalRef(listKlass);
        env->DeleteLocalRef(jlist);
        env->DeleteLocalRef(imsaKlass);
        env->DeleteLocalRef(jsubtypes);
    }

    env->DeleteLocalRef(infoKlass);

    return jinputInfo;
}

jobject ElUtil::GetJavaInputMethodSubtype(
    /* [in] */ JNIEnv* env,
    /* [in] */ IInputMethodSubtype* subType)
{
    if (env == NULL || subType == NULL) {
        ALOGE("GetJavaInputMethodSubtype() Invalid arguments!");
        return NULL;
    }

    jclass subTypeKlass = env->FindClass("android/view/inputmethod/InputMethodSubtype");
    CheckErrorAndLog(env, "GetJavaInputMethodSubtype FindClass: InputMethodSubtype : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(subTypeKlass, "<init>", "(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V");
    CheckErrorAndLog(env, "GetJavaInputMethodSubtype GetMethodID: InputMethodSubtype : %d!\n", __LINE__);

    Int32 nameId;
    subType->GetNameResId(&nameId);
    Int32 iconId;
    subType->GetIconResId(&iconId);
    String locale;
    subType->GetLocale(&locale);
    jstring jlocale = ElUtil::GetJavaString(env, locale);
    String mode;
    subType->GetMode(&mode);
    jstring jmode = ElUtil::GetJavaString(env, mode);
    String extraValue;
    subType->GetExtraValue(&extraValue);
    jstring jextraValue = ElUtil::GetJavaString(env, extraValue);
    Boolean isAuxiliary;
    subType->IsAuxiliary(&isAuxiliary);
    Boolean overridesImplicitlyEnabledSubtype;
    subType->OverridesImplicitlyEnabledSubtype(&overridesImplicitlyEnabledSubtype);

    jobject jsubType = env->NewObject(subTypeKlass, m, (jint)nameId, (jint)iconId, jlocale, jmode, jextraValue,
            (jboolean)isAuxiliary, (jboolean)overridesImplicitlyEnabledSubtype);
    CheckErrorAndLog(env, "GetJavaInputMethodSubtype NewObject: InputMethodSubtype : %d!\n", __LINE__);
    env->DeleteLocalRef(jlocale);
    env->DeleteLocalRef(jmode);
    env->DeleteLocalRef(jextraValue);

    Int32 subtypeHashCode;
    IObject::Probe(subType)->GetHashCode(&subtypeHashCode);
    SetJavaIntField(env, subTypeKlass, jsubType, subtypeHashCode, "mSubtypeHashCode", "GetJavaInputMethodSubtype");
    Boolean isAsciiCapable;
    subType->IsAsciiCapable(&isAsciiCapable);
    SetJavaBoolField(env, subTypeKlass, jsubType, isAsciiCapable, "mIsAsciiCapable", "GetJavaInputMethodSubtype");
    env->DeleteLocalRef(subTypeKlass);

    return jsubType;
}

bool ElUtil::ToElIntentFilter(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jfilter,
    /* [out] */ IIntentFilter** filter)
{
    if (env == NULL || jfilter == NULL || filter == NULL) {
        ALOGD("ToElIntentFilter: Invalid argumenet!");
        return false;
    }

    jclass filterKlass = env->FindClass("android/content/IntentFilter");
    CheckErrorAndLog(env, "FindClass: IntentFilter : %d!\n", __LINE__);

    if (NOERROR != CIntentFilter::New(filter)) {
        ALOGD("ToElIntentFilter: create CIntentFilter fail!");
        return false;
    }

    AutoPtr<IParcel> parcel;
    Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);

    jclass alistKlass = env->FindClass("java/util/ArrayList");
    CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID mSize = env->GetMethodID(alistKlass, "size", "()I");
    CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

    jmethodID mGet = env->GetMethodID(alistKlass, "get", "(I)Ljava/lang/Object;");
    CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

    env->DeleteLocalRef(alistKlass);

    jfieldID f = env->GetFieldID(filterKlass, "mActions", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: mAction : %d!\n", __LINE__);

    jobject jmActions = env->GetObjectField(jfilter, f);
    CheckErrorAndLog(env, "GetObjectField: mActions : %d!\n", __LINE__);

    if (jmActions != NULL) {
        jint jsize = env->CallIntMethod(jmActions, mSize);
        parcel->WriteInt32(jsize);
        CheckErrorAndLog(env, "CallIntMethod: mActions : %d!\n", __LINE__);
        if (jsize > 0) {
            for (jint i = 0; i < jsize; i++) {
                jstring jaction = (jstring)env->CallObjectMethod(jmActions, mGet, i);
                CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                String action = ElUtil::ToElString(env, jaction);
                parcel->WriteString(action);
                env->DeleteLocalRef(jaction);
            }
        }
        env->DeleteLocalRef(jmActions);
    }

    f = env->GetFieldID(filterKlass, "mCategories", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: mCategories : %d!\n", __LINE__);

    jobject jmCategories = env->GetObjectField(jfilter, f);
    CheckErrorAndLog(env, "GetObjectField: mCategories : %d!\n", __LINE__);

    if (jmCategories != NULL) {
        jint jsize = env->CallIntMethod(jmCategories, mSize);
        parcel->WriteInt32(jsize);
        CheckErrorAndLog(env, "CallIntMethod: mCategories : %d!\n", __LINE__);
        if (jsize > 0) {
            for (jint i = 0; i < jsize; i++) {
                jstring jcategorie = (jstring)env->CallObjectMethod(jmCategories, mGet, i);
                CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                String categorie = ElUtil::ToElString(env, jcategorie);
                parcel->WriteString(categorie);
                env->DeleteLocalRef(jcategorie);
            }
        }
        env->DeleteLocalRef(jmCategories);
    }
    else {
        parcel->WriteInt32(0);
    }

    f = env->GetFieldID(filterKlass, "mDataSchemes", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: mDataSchemes : %d!\n", __LINE__);

    jobject jmDataSchemes = env->GetObjectField(jfilter, f);
    CheckErrorAndLog(env, "GetObjectField: mDataSchemes : %d!\n", __LINE__);

    if (jmDataSchemes != NULL) {
        jint jsize = env->CallIntMethod(jmDataSchemes, mSize);
        parcel->WriteInt32(jsize);
        CheckErrorAndLog(env, "CallIntMethod: mDataSchemes : %d!\n", __LINE__);
        if (jsize > 0) {
            for (jint i = 0; i < jsize; i++) {
                jstring jdataScheme = (jstring)env->CallObjectMethod(jmDataSchemes, mGet, i);
                CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                String dataScheme = ElUtil::ToElString(env, jdataScheme);
                parcel->WriteString(dataScheme);
                env->DeleteLocalRef(jdataScheme);
            }
        }

        env->DeleteLocalRef(jmDataSchemes);
    }
    else {
        parcel->WriteInt32(0);
    }

    f = env->GetFieldID(filterKlass, "mDataTypes", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: mDataTypes : %d!\n", __LINE__);

    jobject jmDataTypes = env->GetObjectField(jfilter, f);
    CheckErrorAndLog(env, "GetObjectField: mDataTypes : %d!\n", __LINE__);

    if (jmDataTypes != NULL) {
        jint jsize = env->CallIntMethod(jmDataTypes, mSize);
        parcel->WriteInt32(jsize);
        CheckErrorAndLog(env, "CallIntMethod: jmDataTypes : %d!\n", __LINE__);
        if (jsize > 0) {
            for (jint i = 0; i < jsize; i++) {
                jstring jdataType = (jstring)env->CallObjectMethod(jmDataTypes, mGet, i);
                CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                String dataType = ElUtil::ToElString(env, jdataType);
                parcel->WriteString(dataType);
                env->DeleteLocalRef(jdataType);
            }
        }
        env->DeleteLocalRef(jmDataTypes);
    }
    else {
        parcel->WriteInt32(0);
    }

    f = env->GetFieldID(filterKlass, "mDataSchemeSpecificParts", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: mDataSchemeSpecificParts : %d!\n", __LINE__);

    jobject jmDataSchemeSpecificParts = (jstring)env->GetObjectField(jfilter, f);
    CheckErrorAndLog(env, "GetObjectField: mDataSchemeSpecificParts : %d!\n", __LINE__);

    if (jmDataSchemeSpecificParts != NULL) {
        jint jsize = env->CallIntMethod(jmDataSchemeSpecificParts, mSize);
        parcel->WriteInt32(jsize);
        CheckErrorAndLog(env, "CallIntMethod: mDataSchemeSpecificParts : %d!\n", __LINE__);
        if (jsize > 0) {
            for (jint i = 0; i < jsize; i++) {
                jobject jdssPart = env->CallObjectMethod(jmDataSchemeSpecificParts, mGet, i);
                CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                AutoPtr<IPatternMatcher> dssPart;
                if (!ToElPatternMatcher(env, jdssPart, (IPatternMatcher**)&dssPart)) {
                    ALOGE("ToElIntentFilter: ToElPatternMatcher failed !");
                }
                else {
                    parcel->WriteInterfacePtr(dssPart);
                }
                env->DeleteLocalRef(jdssPart);
            }
        }
        env->DeleteLocalRef(jmDataSchemeSpecificParts);
    }
    else {
        parcel->WriteInt32(0);
    }

    f = env->GetFieldID(filterKlass, "mDataAuthorities", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: mDataAuthorities : %d!\n", __LINE__);

    jobject jmDataAuthorities = (jstring)env->GetObjectField(jfilter, f);
    CheckErrorAndLog(env, "GetObjectField: mDataAuthorities : %d!\n", __LINE__);
    if (jmDataAuthorities != NULL) {
        jint jsize = env->CallIntMethod(jmDataAuthorities, mSize);
        parcel->WriteInt32(jsize);
        CheckErrorAndLog(env, "CallIntMethod: mDataAuthorities : %d!\n", __LINE__);
        for (jint i = 0; i < jsize; i++) {
            jobject jdataAuthority = (jobject)env->CallObjectMethod(jmDataAuthorities, mGet, i);
            CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

            AutoPtr<IIntentFilterAuthorityEntry> dataAuthority;
            if (!ToElAuthorityEntry(env, jdataAuthority, (IIntentFilterAuthorityEntry**)&dataAuthority)) {
                ALOGE("ToElIntentFilter: ToElAuthorityEntry failed !");
            }
            else {
                parcel->WriteInterfacePtr(dataAuthority);
            }
            env->DeleteLocalRef(jdataAuthority);
        }
        env->DeleteLocalRef(jmDataAuthorities);
    }
    else {
        parcel->WriteInt32(0);
    }

    f = env->GetFieldID(filterKlass, "mDataPaths", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: mDataPaths : %d!\n", __LINE__);

    jobject jmDataPaths = (jstring)env->GetObjectField(jfilter, f);
    CheckErrorAndLog(env, "GetObjectField: mDataPaths : %d!\n", __LINE__);
    if (jmDataPaths != NULL) {
        jint jsize = env->CallIntMethod(jmDataPaths, mSize);
        parcel->WriteInt32(jsize);
        CheckErrorAndLog(env, "CallIntMethod: mDataPaths : %d!\n", __LINE__);
        for (jint i = 0; i < jsize; i++) {
            jobject jdataPath = (jobject)env->CallObjectMethod(jmDataPaths, mGet, i);
            CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

            AutoPtr<IPatternMatcher> dataPath;
            if (!ToElPatternMatcher(env, jdataPath, (IPatternMatcher**)&dataPath)) {
                ALOGE("ToElIntentFilter: ToElPatternMatcher failed !");
            }
            else {
                parcel->WriteInterfacePtr(dataPath);
            }
            env->DeleteLocalRef(jdataPath);
        }
        env->DeleteLocalRef(jmDataPaths);
    }
    else {
        parcel->WriteInt32(0);
    }

    f = env->GetFieldID(filterKlass, "mPriority", "I");
    CheckErrorAndLog(env, "GetFieldID mPriority : %d!\n", __LINE__);

    jint jmPriority = env->GetIntField(jfilter, f);
    CheckErrorAndLog(env, "GetIntField mPriority : %d!\n", __LINE__);
    parcel->WriteInt32(jmPriority);

    f = env->GetFieldID(filterKlass, "mHasPartialTypes", "Z");
    CheckErrorAndLog(env, "GetFieldID mHasPartialTypes : %d!\n", __LINE__);

    jboolean jmHasPartialTypes = env->GetBooleanField(jfilter, f);
    CheckErrorAndLog(env, "GetIntField mHasPartialTypes : %d!\n", __LINE__);
    parcel->WriteBoolean(jmHasPartialTypes);
    parcel->SetDataPosition(0);

    env->DeleteLocalRef(filterKlass);

    if (IParcelable::Probe(*filter)->ReadFromParcel(parcel) != NOERROR) {
        ALOGD("ToElIntentFilter: CIntentFilter::ReadFromParcel fail!");
        return false;
    }

    return true;
}

jobject ElUtil::GetJavaUri(
    /* [in] */ JNIEnv* env,
    /* [in] */ IUri* uri)
{
    if (env == NULL || uri == NULL) {
        ALOGE("GetJavaUri: Invalid argumenet!");
        return NULL;
    }

    jobject juri = NULL;

    AutoPtr<IObject> object = IObject::Probe(uri);

    ClassID clsid;
    object->GetClassID(&clsid);
    if (ECLSID_CStringUri == clsid) {
        jclass suriKlass = env->FindClass("android/net/Uri$StringUri");
        ElUtil::CheckErrorAndLog(env, "GetJavaUri FindClass: Intent : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(suriKlass, "<init>", "(Ljava/lang/String;)V");
        ElUtil::CheckErrorAndLog(env, "GetJavaUri  GetMethodID: intentKlass : %d!\n", __LINE__);

        String suri;
        object->ToString(&suri);
        jstring jsuri = ElUtil::GetJavaString(env, suri);

        juri = env->NewObject(suriKlass, m, jsuri);
        ElUtil::CheckErrorAndLog(env, "GetJavaUri NewObject: intentKlass : %d!\n", __LINE__);
        env->DeleteLocalRef(suriKlass);
        env->DeleteLocalRef(jsuri);
    }
    else if (ECLSID_CHierarchicalUri == clsid) {
        jclass uriKlass = env->FindClass("android/net/Uri");
        ElUtil::CheckErrorAndLog(env, "GetJavaUri FindClass: Uri : %d!\n", __LINE__);

        jfieldID f = env->GetStaticFieldID(uriKlass, "NOT_CACHED", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaUri Fail GetStaticFieldID: TAIWAN : %d!\n", __LINE__);

        jstring jNOT_CACHED = (jstring)env->GetStaticObjectField(uriKlass, f);
        CheckErrorAndLog(env, "GetJavaUri Fail GetStaticObjectField: : %d!\n", __LINE__);

        AutoPtr<IParcel> parcel;
        Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);
        IParcelable::Probe(uri)->WriteToParcel(parcel);
        parcel->SetDataPosition(0);

        Int32 id;
        parcel->ReadInt32(&id);
        String scheme;
        parcel->ReadString(&scheme);

        jstring jscheme = GetJavaString(env, scheme);

        Int32 representation;
        parcel->ReadInt32(&representation);
        String encode, decode;
        switch (representation) {
            case 0/*Representation::BOTH*/:
                parcel->ReadString(&encode);
                parcel->ReadString(&decode);
                break;

            case 1/*Representation::ENCODED*/:
                parcel->ReadString(&encode);
                decode = String("NOT CACHED");
                break;

            case 2/*Representation::DECODED*/:
                encode = String("NOT CACHED");
                parcel->ReadString(&decode);
                break;
            default:
                assert(0);
        }
        jstring jencode = encode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, encode);
        jstring jdecode = decode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, decode);

        jclass partKlass = env->FindClass("android/net/Uri$Part");
        CheckErrorAndLog(env, "GetJavaUri Fail FindClass: Part %d", __LINE__);

        jmethodID mid = env->GetStaticMethodID(partKlass, "from", "(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Part;");
        CheckErrorAndLog(env, "GetJavaUri Fail GetMethodID: partKlass %d", __LINE__);

        jobject jauthority = env->CallStaticObjectMethod(partKlass, mid, jencode, jdecode);
        CheckErrorAndLog(env, "GetJavaUri Fail call static method from() %d", __LINE__);

        if (jencode != jNOT_CACHED)
            env->DeleteLocalRef(jencode);
        if (jdecode != jNOT_CACHED)
            env->DeleteLocalRef(jdecode);

        parcel->ReadInt32(&representation);
        switch (representation) {
            case 0/*Representation::BOTH*/:
                parcel->ReadString(&encode);
                parcel->ReadString(&decode);
                break;

            case 1/*Representation::ENCODED*/:
                parcel->ReadString(&encode);
                decode = String("NOT CACHED");
                break;

            case 2/*Representation::DECODED*/:
                encode = String("NOT CACHED");
                parcel->ReadString(&decode);
                break;
            default:
                assert(0);
        }
        jencode = encode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, encode);
        jdecode = decode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, decode);

        jclass pathPartKlass = env->FindClass("android/net/Uri$PathPart");
        CheckErrorAndLog(env, "GetJavaUri Fail FindClass: PathPart %d", __LINE__);

        jmethodID mid2 = env->GetStaticMethodID(pathPartKlass, "from", "(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$PathPart;");
        CheckErrorAndLog(env, "GetJavaUri Fail GetMethodID: pathPartKlass %d", __LINE__);

        jobject jpath = env->CallStaticObjectMethod(pathPartKlass, mid2, jencode, jdecode);
        CheckErrorAndLog(env, "GetJavaUri Fail call static method from() %d", __LINE__);

        if (jencode != jNOT_CACHED)
            env->DeleteLocalRef(jencode);
        if (jdecode != jNOT_CACHED)
            env->DeleteLocalRef(jdecode);

        parcel->ReadInt32(&representation);
        switch (representation) {
            case 0/*Representation::BOTH*/:
                parcel->ReadString(&encode);
                parcel->ReadString(&decode);
                break;

            case 1/*Representation::ENCODED*/:
                parcel->ReadString(&encode);
                decode = String("NOT CACHED");
                break;

            case 2/*Representation::DECODED*/:
                encode = String("NOT CACHED");
                parcel->ReadString(&decode);
                break;
            default:
                assert(0);
        }
        jencode = encode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, encode);
        jdecode = decode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, decode);

        jobject jquery = env->CallStaticObjectMethod(partKlass, mid, jencode, jdecode);
        CheckErrorAndLog(env, "GetJavaUri Fail call static method from() %d", __LINE__);

        if (jencode != jNOT_CACHED)
            env->DeleteLocalRef(jencode);
        if (jdecode != jNOT_CACHED)
            env->DeleteLocalRef(jdecode);

        parcel->ReadInt32(&representation);
        switch (representation) {
            case 0/*Representation::BOTH*/:
                parcel->ReadString(&encode);
                parcel->ReadString(&decode);
                break;

            case 1/*Representation::ENCODED*/:
                parcel->ReadString(&encode);
                decode = String("NOT CACHED");
                break;

            case 2/*Representation::DECODED*/:
                encode = String("NOT CACHED");
                parcel->ReadString(&decode);
                break;
            default:
                assert(0);
        }
        jencode = encode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, encode);
        jdecode = decode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, decode);

        jobject jfragment = env->CallStaticObjectMethod(partKlass, mid, jencode, jdecode);
        CheckErrorAndLog(env, "GetJavaUri Fail call static method from() %d", __LINE__);

        if (jencode != jNOT_CACHED)
            env->DeleteLocalRef(jencode);
        if (jdecode != jNOT_CACHED)
            env->DeleteLocalRef(jdecode);

        jclass huriKlass = env->FindClass("android/net/Uri$HierarchicalUri");
        CheckErrorAndLog(env, "GetJavaUri Fail FindClass: HierarchicalUri %d", __LINE__);

        jmethodID m = env->GetMethodID(huriKlass, "<init>", "(Ljava/lang/String;Landroid/net/Uri$Part;Landroid/net/Uri$PathPart;Landroid/net/Uri$Part;Landroid/net/Uri$Part;)V");
        CheckErrorAndLog(env, "GetJavaUri Fail GetMethodID: huriKlass %d", __LINE__);

        juri = env->NewObject(huriKlass, m, jscheme, jauthority, jpath, jquery, jfragment);
        CheckErrorAndLog(env, "GetJavaUri Fail NewObject: huriKlass %d", __LINE__);

        env->DeleteLocalRef(uriKlass);
        env->DeleteLocalRef(jNOT_CACHED);
        env->DeleteLocalRef(partKlass);
        env->DeleteLocalRef(pathPartKlass);
        env->DeleteLocalRef(huriKlass);
        env->DeleteLocalRef(jscheme);
        env->DeleteLocalRef(jpath);
        env->DeleteLocalRef(jquery);
        env->DeleteLocalRef(jfragment);
    }
    else if (ECLSID_COpaqueUri == clsid) {
        jclass uriKlass = env->FindClass("android/net/Uri");
        ElUtil::CheckErrorAndLog(env, "GetJavaUri FindClass: Uri : %d!\n", __LINE__);

        jfieldID f = env->GetStaticFieldID(uriKlass, "NOT_CACHED", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaUri Fail GetStaticFieldID: TAIWAN : %d!\n", __LINE__);

        jstring jNOT_CACHED = (jstring)env->GetStaticObjectField(uriKlass, f);
        CheckErrorAndLog(env, "GetJavaUri Fail GetStaticObjectField: : %d!\n", __LINE__);

        AutoPtr<IParcel> parcel;
        Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);
        IParcelable::Probe(uri)->WriteToParcel(parcel);
        parcel->SetDataPosition(0);

        Int32 id;
        parcel->ReadInt32(&id);
        String scheme;
        parcel->ReadString(&scheme);

        jstring jscheme = GetJavaString(env, scheme);

        Int32 representation;
        parcel->ReadInt32(&representation);
        String encode, decode;
        switch (representation) {
            case 0/*Representation::BOTH*/:
                parcel->ReadString(&encode);
                parcel->ReadString(&decode);
                break;

            case 1/*Representation::ENCODED*/:
                parcel->ReadString(&encode);
                decode = String("NOT CACHED");
                break;

            case 2/*Representation::DECODED*/:
                encode = String("NOT CACHED");
                parcel->ReadString(&decode);
                break;
            default:
                assert(0);
        }
        jstring jencode = encode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, encode);
        jstring jdecode = decode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, decode);

        jclass partKlass = env->FindClass("android/net/Uri$Part");
        CheckErrorAndLog(env, "GetJavaUri Fail FindClass: Part %d", __LINE__);

        jmethodID mid = env->GetStaticMethodID(partKlass, "from", "(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Part;");
        CheckErrorAndLog(env, "GetJavaUri Fail GetMethodID: partKlass %d", __LINE__);

        jobject jssp = env->CallStaticObjectMethod(partKlass, mid, jencode, jdecode);
        CheckErrorAndLog(env, "GetJavaUri Fail call static method from() %d", __LINE__);

        if (jencode != jNOT_CACHED)
            env->DeleteLocalRef(jencode);
        if (jdecode != jNOT_CACHED)
            env->DeleteLocalRef(jdecode);

        parcel->ReadInt32(&representation);
        switch (representation) {
            case 0/*Representation::BOTH*/:
                parcel->ReadString(&encode);
                parcel->ReadString(&decode);
                break;

            case 1/*Representation::ENCODED*/:
                parcel->ReadString(&encode);
                decode = String("NOT CACHED");
                break;

            case 2/*Representation::DECODED*/:
                encode = String("NOT CACHED");
                parcel->ReadString(&decode);
                break;
            default:
                assert(0);
        }
        jencode = encode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, encode);
        jdecode = decode.Equals("NOT CACHED") ? jNOT_CACHED : GetJavaString(env, decode);

        jobject jfragment = env->CallStaticObjectMethod(partKlass, mid, jencode, jdecode);
        CheckErrorAndLog(env, "GetJavaUri Fail call static method from() %d", __LINE__);

        if (jencode != jNOT_CACHED)
            env->DeleteLocalRef(jencode);
        if (jdecode != jNOT_CACHED)
            env->DeleteLocalRef(jdecode);

        jclass opaqueKlass = env->FindClass("android/net/Uri$OpaqueUri");
        CheckErrorAndLog(env, "GetJavaUri Fail FindClass: OpaqueUri %d", __LINE__);

        jmethodID m = env->GetMethodID(opaqueKlass, "<init>", "(Ljava/lang/String;Landroid/net/Uri$Part;Landroid/net/Uri$Part;)V");
        CheckErrorAndLog(env, "GetJavaUri Fail GetMethodID: opaqueKlass %d", __LINE__);

        juri = env->NewObject(opaqueKlass, m, jscheme, jssp, jfragment);
        CheckErrorAndLog(env, "GetJavaUri Fail NewObject: opaqueKlass %d", __LINE__);

        env->DeleteLocalRef(uriKlass);
        env->DeleteLocalRef(jNOT_CACHED);
        env->DeleteLocalRef(partKlass);
        env->DeleteLocalRef(opaqueKlass);
        env->DeleteLocalRef(jscheme);
        env->DeleteLocalRef(jssp);
        env->DeleteLocalRef(jfragment);
    }
    else {
        ALOGE("GetJavaUri() others type uri!");
    }

    return juri;
}

jobject ElUtil::GetJavaClipData(
    /* [in] */ JNIEnv* env,
    /* [in] */ IClipData* clipData)
{
    if (env == NULL || clipData == NULL) {
        ALOGE("GetJavaClipData() Invalid arguments!");
        return NULL;
    }

    jobject jclipData = NULL;

    jclass cdKlass = env->FindClass("android/content/ClipData");
    CheckErrorAndLog(env, "FindClass: ClipData : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(cdKlass, "<init>", "(Landroid/content/ClipDescription;Landroid/content/ClipData$Item;)V");
    CheckErrorAndLog(env, "GetMethodID: ClipData : %d!\n", __LINE__);

    AutoPtr<IClipDescription> clipDescription;
    clipData->GetDescription((IClipDescription**)&clipDescription);
    jobject jclipDescription = NULL;
    if (clipDescription != NULL) {
        jclipDescription = ElUtil::GetJavaClipDescription(env, clipDescription);
    }

    AutoPtr<IClipDataItem> item;
    clipData->GetItemAt(0, (IClipDataItem**)&item);
    jobject jitem = NULL;
    if (item != NULL) {
        jitem = ElUtil::GetJavaClipDataItem(env, item);
    }

    jclipData = env->NewObject(cdKlass, m, jclipDescription, jitem);
    ElUtil::CheckErrorAndLog(env, "NewObject: ClipData : %d!\n", __LINE__);

    m = env->GetMethodID(cdKlass, "addItem", "(Landroid/content/ClipData$Item;)V");
    CheckErrorAndLog(env, "GetFieldID: addItem : %d!", __LINE__);

    Int32 count = 0;
    clipData->GetItemCount(&count);
    for (Int32 i = 1; i < count; i++) {
        AutoPtr<IClipDataItem> it;
        clipData->GetItemAt(i, (IClipDataItem**)&it);

        jobject jit = ElUtil::GetJavaClipDataItem(env, item);

        env->CallVoidMethod(jclipData, m, jit);
        CheckErrorAndLog(env, "CallVoidMethod: addItem : %d!", __LINE__);
        env->DeleteLocalRef(jit);
    }

    AutoPtr<IBitmap> icon;
    clipData->GetIcon((IBitmap**)&icon);
    if (icon != NULL) {
        jobject jicon = ElUtil::GetJavaBitmap(env, icon);

        jfieldID f = env->GetFieldID(cdKlass, "mIcon", "Landroid/graphics/Bitmap;");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: mIcon : %d!", __LINE__);

        env->SetObjectField(jclipData, f, jicon);
        ElUtil::CheckErrorAndLog(env, "SetObjectField: mIcon : %d!", __LINE__);
        env->DeleteLocalRef(jicon);
    }

    env->DeleteLocalRef(cdKlass);
    env->DeleteLocalRef(jclipDescription);
    env->DeleteLocalRef(jitem);
    return jclipData;
}

jobject ElUtil::GetJavaClipDataItem(
    /* [in] */ JNIEnv* env,
    /* [in] */ IClipDataItem* item)
{
    if (env == NULL || item == NULL) {
        ALOGE("GetJavaClipData() Invalid arguments!");
        return NULL;
    }

    jobject jitem = NULL;

    jclass cdiKlass = env->FindClass("android/content/ClipData$Item");
    CheckErrorAndLog(env, "FindClass: ClipData$Item : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(cdiKlass, "<init>", "(Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/net/Uri;)V");
    CheckErrorAndLog(env, "GetMethodID: ClipData$Item : %d!\n", __LINE__);

    AutoPtr<ICharSequence> text;
    item->GetText((ICharSequence**)&text);
    jobject jtext = NULL;
    if (text != NULL) {
        String stext;
        text->ToString(&stext);
        jtext = ElUtil::GetJavaString(env, stext);
    }

    String htmlText;
    item->GetHtmlText(&htmlText);
    jstring jhtmlText = ElUtil::GetJavaString(env, htmlText);

    AutoPtr<IIntent> intent;
    item->GetIntent((IIntent**)&intent);
    jobject jintent = NULL;
    if (intent != NULL) {
        jintent = ElUtil::GetJavaIntent(env, intent);
    }

    AutoPtr<IUri> uri;
    item->GetUri((IUri**)&uri);
    jobject juri = NULL;
    if (uri != NULL) {
        juri = ElUtil::GetJavaUri(env, uri);
    }

    jitem = env->NewObject(cdiKlass, m, jtext, jhtmlText, jintent, juri);
    ElUtil::CheckErrorAndLog(env, "NewObject: ClipData : %d!\n", __LINE__);

    env->DeleteLocalRef(cdiKlass);
    return jitem;
}

jobject ElUtil::GetJavaAccount(
    /* [in] */ JNIEnv* env,
    /* [in] */ IAccount* account)
{
    if (env == NULL || account == NULL) {
        ALOGE("GetJavaAccount() Invalid arguments!");
        return NULL;
    }

    jobject jaccount = NULL;

    jclass infoKlass = env->FindClass("android/accounts/Account");
    CheckErrorAndLog(env, "GetJavaAccount FindClass: Account : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(infoKlass, "<init>", "(Ljava/lang/String;Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetJavaAccount GetMethodID: infoKlass : %d!\n", __LINE__);

    String name;
    account->GetName(&name);
    jobject jname = ElUtil::GetJavaString(env, name);
    String type;
    account->GetType(&type);
    jobject jtype = ElUtil::GetJavaString(env, type);

    jaccount = env->NewObject(infoKlass, m, jname, jtype);
    ElUtil::CheckErrorAndLog(env, "GetJavaAccount NewObject: Account : %d!\n", __LINE__);
    env->DeleteLocalRef(jname);
    env->DeleteLocalRef(jtype);

    env->DeleteLocalRef(infoKlass);

    return jaccount;
}

bool ElUtil::ToElCompletionInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jcompletion,
    /* [out] */ ICompletionInfo** completion)
{
    if (env == NULL || jcompletion == NULL || completion == NULL) {
        ALOGE("ToElCompletionInfo: Invalid argumenet!");
        return false;
    }

    jclass cinfoKlass = env->FindClass("android/view/inputmethod/CompletionInfo");
    CheckErrorAndLog(env, "FindClass: CompletionInfo : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(cinfoKlass, "mId", "J");
    CheckErrorAndLog(env, "GetFieldID: mId : %d!\n", __LINE__);

    jlong jmId = env->GetLongField(jcompletion, f);
    CheckErrorAndLog(env, "GetIntField mId : %d!\n", __LINE__);

    f = env->GetFieldID(cinfoKlass, "mPosition", "I");
    CheckErrorAndLog(env, "GetFieldID: mId : %d!\n", __LINE__);

    jint jmPosition = env->GetIntField(jcompletion, f);
    CheckErrorAndLog(env, "GetIntField mId : %d!\n", __LINE__);

    f = env->GetFieldID(cinfoKlass, "mText", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID mText : %d!\n", __LINE__);

    jobject jcmText= env->GetObjectField(jcompletion, f);
    CheckErrorAndLog(env, "GetObjectField mText : %d!\n", __LINE__);
    AutoPtr<ICharSequence> mcText;
    if (jcmText != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jmText = (jstring)env->CallObjectMethod(jcmText, m);
        CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String mText = ElUtil::ToElString(env, jmText);

        CString::New(mText, (ICharSequence**)&mcText);
        env->DeleteLocalRef(jcmText);
        env->DeleteLocalRef(csClass);
        env->DeleteLocalRef(jmText);
    }

    f = env->GetFieldID(cinfoKlass, "mLabel", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID mLabel : %d!\n", __LINE__);
    env->DeleteLocalRef(cinfoKlass);

    jobject jcmLabel= env->GetObjectField(jcompletion, f);
    CheckErrorAndLog(env, "GetObjectField mLabel : %d!\n", __LINE__);
    AutoPtr<ICharSequence> mcLabel;
    if (jcmLabel != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jmLabel = (jstring)env->CallObjectMethod(jcmLabel, m);
        CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String mLabel = ElUtil::ToElString(env, jmLabel);
        CString::New(mLabel, (ICharSequence**)&mcLabel);

        env->DeleteLocalRef(jcmLabel);
        env->DeleteLocalRef(csClass);
        env->DeleteLocalRef(jmLabel);
    }

    if (NOERROR != CCompletionInfo::New((Int64)jmId, (Int32)jmPosition, mcText, mcLabel, completion)) {
        ALOGD("ToElCompletionInfo: create CCompletionInfo fail!");
        return false;
    }

    return true;
}

bool ElUtil::ToElRegion(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jregion,
    /* [out] */ IRegion** region)
{
    if (env == NULL || jregion == NULL || region == NULL) {
        ALOGE("ToElRegion: Invalid argumenet!");
        return false;
    }

    jclass regionKlass = env->FindClass("android/graphics/Region");
    CheckErrorAndLog(env, "FindClass: Region : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(regionKlass, "mNativeRegion", "J");
    CheckErrorAndLog(env, "GetFieldID: mNativeRegion : %d!\n", __LINE__);
    env->DeleteLocalRef(regionKlass);

    jint jmNativeRegion = env->GetIntField(jregion, f);
    CheckErrorAndLog(env, "GetIntField mNativeRegion : %d!\n", __LINE__);

    if (NOERROR != CRegion::New((Int32)new SkRegion(*(SkRegion*)jmNativeRegion), region)) {
        ALOGD("ToElRegion: create CRegion fail!");
        return false;
    }

    return true;
}

jobject ElUtil::GetJavaMessenger(
    /* [in] */ JNIEnv* env,
    /* [in] */ IMessenger* messenger)
{
    if (env == NULL || messenger == NULL) {
        ALOGE("GetJavaMessenger: Invalid argumenet!");
        return NULL;
    }

    jobject jmessenger = NULL;

    jclass msgerKlass = env->FindClass("android/os/Messenger");
    CheckErrorAndLog(env, "GetJavaMessenger FindClass: Messenger : %d!\n", __LINE__);

    AutoPtr<IIMessenger> mgr;
    messenger->GetIMessenger((IIMessenger**)&mgr);

    jclass elmsgerKlass = env->FindClass("android/os/ElMessengerProxy");
    CheckErrorAndLog(env, "FindClass: ElMessengerProxy : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(elmsgerKlass, "<init>", "(J)V");
    CheckErrorAndLog(env, "GetMethodID: ElMessengerProxy : %d!\n", __LINE__);

    jobject jelMgr = env->NewObject(elmsgerKlass, m, (jlong)mgr.Get());
    CheckErrorAndLog(env, "NewObject: ElMessengerProxy : %d!\n", __LINE__);
    mgr->AddRef();

    m = env->GetMethodID(msgerKlass, "<init>", "(Landroid/os/IBinder;)V");
    CheckErrorAndLog(env, "GetJavaMessenger GetMethodID: Messenger : %d!\n", __LINE__);

    jmessenger = env->NewObject(msgerKlass, m, jelMgr);
    CheckErrorAndLog(env, "GetJavaMessenger NewObject: Messenger : %d!\n", __LINE__);

    env->DeleteLocalRef(jelMgr);
    env->DeleteLocalRef(msgerKlass);
    env->DeleteLocalRef(elmsgerKlass);

    return jmessenger;
}

jobject ElUtil::GetJavaWifiInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaWifiInfo: Invalid argumenet!");
        return NULL;
    }

    jobject jinfo = NULL;

    jclass wiinfoKlass = env->FindClass("android/net/wifi/WifiInfo");
    CheckErrorAndLog(env, "GetJavaWifiInfo FindClass: WifiInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(wiinfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaWifiInfo GetMethodID: WifiInfo : %d!\n", __LINE__);

    jinfo = env->NewObject(wiinfoKlass, m);
    CheckErrorAndLog(env, "GetJavaWifiInfo NewObject: WifiInfo : %d!\n", __LINE__);

    Int32 tempInt;
    info->GetNetworkId(&tempInt);
    jfieldID f = env->GetFieldID(wiinfoKlass, "mNetworkId", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo GetFieldID: mNetworkId : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo SetIntField: mNetworkId : %d!\n", __LINE__);

    info->GetRssi(&tempInt);
    f = env->GetFieldID(wiinfoKlass, "mRssi", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo GetFieldID: mRssi : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo SetIntField: mRssi : %d!\n", __LINE__);

    info->GetLinkSpeed(&tempInt);
    f = env->GetFieldID(wiinfoKlass, "mLinkSpeed", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo GetFieldID: mLinkSpeed : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo SetIntField: mLinkSpeed : %d!\n", __LINE__);

    Int32 iaddress = 0;
    info->GetIpAddress(&iaddress);
    if (iaddress != 0) {
        jobject jaddress = ElUtil::GetJavaInetAddress(env, 0, iaddress, String(NULL));

        f = env->GetFieldID(wiinfoKlass, "mIpAddress", "Ljava/net/InetAddress;");
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo GetFieldID: InetAddress : %d!\n", __LINE__);

        env->SetObjectField(jinfo, f, jaddress);
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo SetIntField: jaddress : %d!\n", __LINE__);
        env->DeleteLocalRef(jaddress);
    }

    AutoPtr<IWifiSsid> wiSsid;
    info->GetWifiSsid((IWifiSsid**)&wiSsid);
    if (wiSsid != NULL) {
        jobject jwiSsid = ElUtil::GetJavaWifiSsid(env, wiSsid);

        f = env->GetFieldID(wiinfoKlass, "mWifiSsid", "Landroid/net/wifi/WifiSsid;");
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo GetFieldID: WifiSsid : %d!\n", __LINE__);

        env->SetObjectField(jinfo, f, jwiSsid);
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo SetIntField: jwiSsid : %d!\n", __LINE__);
        env->DeleteLocalRef(jwiSsid);
    }

    String mBSSID;
    info->GetBSSID(&mBSSID);
    if (!mBSSID.IsNull()) {
        jfieldID f = env->GetFieldID(wiinfoKlass, "mBSSID", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: mBSSID : %d!\n", __LINE__);

        jobject jmBSSID = ElUtil::GetJavaString(env, mBSSID);
        env->SetObjectField(jinfo, f, jmBSSID);
        ElUtil::CheckErrorAndLog(env, "SetObjectField: jmBSSID : %d!\n", __LINE__);
        env->DeleteLocalRef(jmBSSID);
    }

    String mMacAddress;
    info->GetMacAddress(&mMacAddress);
    if (!mMacAddress.IsNull()) {
        jfieldID f = env->GetFieldID(wiinfoKlass, "mMacAddress", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: mMacAddress : %d!\n", __LINE__);

        jobject jmMacAddress = ElUtil::GetJavaString(env, mMacAddress);
        env->SetObjectField(jinfo, f, jmMacAddress);
        ElUtil::CheckErrorAndLog(env, "SetObjectField: jmMacAddress : %d!\n", __LINE__);
        env->DeleteLocalRef(jmMacAddress);
    }

    Boolean hint;
    info->GetMeteredHint(&hint);
    f = env->GetFieldID(wiinfoKlass, "mMeteredHint", "Z");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo GetFieldID: mMeteredHint : %d!\n", __LINE__);

    env->SetBooleanField(jinfo, f, tempInt);
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo SetBooleanField: mMeteredHint : %d!\n", __LINE__);

    info->GetScore(&tempInt);
    ElUtil::SetJavaIntField(env, wiinfoKlass, jinfo, tempInt, "score", "GetJavaWifiInfo");

    Double tempDouble;
    info->GetTxSuccessRate(&tempDouble);
    ElUtil::SetJavadoubleField(env, wiinfoKlass, jinfo, tempDouble, "txSuccessRate", "GetJavaWifiInfo");

    info->GetTxRetriesRate(&tempDouble);
    ElUtil::SetJavadoubleField(env, wiinfoKlass, jinfo, tempDouble, "txRetriesRate", "GetJavaWifiInfo");

    info->GetTxBadRate(&tempDouble);
    ElUtil::SetJavadoubleField(env, wiinfoKlass, jinfo, tempDouble, "txBadRate", "GetJavaWifiInfo");

    info->GetRxSuccessRate(&tempDouble);
    ElUtil::SetJavadoubleField(env, wiinfoKlass, jinfo, tempDouble, "rxSuccessRate", "GetJavaWifiInfo");

    info->GetBadRssiCount(&tempInt);
    ElUtil::SetJavaIntField(env, wiinfoKlass, jinfo, tempInt, "badRssiCount", "GetJavaWifiInfo");

    info->GetLowRssiCount(&tempInt);
    ElUtil::SetJavaIntField(env, wiinfoKlass, jinfo, tempInt, "lowRssiCount", "GetJavaWifiInfo");

    AutoPtr<ISupplicantState> state;
    info->GetSupplicantState((ISupplicantState**)&state);

    jobject jstate = ElUtil::GetJavaSupplicantState(env, state);
    f = env->GetFieldID(wiinfoKlass, "mSupplicantState", "Landroid/net/wifi/SupplicantState;");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo GetFieldID: mSupplicantState : %d!\n", __LINE__);

    env->SetObjectField(jinfo, f, jstate);
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiInfo SetObjectField: mSupplicantState : %d!\n", __LINE__);
    env->DeleteLocalRef(jstate);

    env->DeleteLocalRef(wiinfoKlass);
    return jinfo;
}

jobject ElUtil::GetJavaInetAddress(
    /* [in] */ JNIEnv* env,
    /* [in] */ IInetAddress* address)
{
    if (env == NULL || address == NULL) {
        ALOGE("GetJavaInetAddress: Invalid argumenet!");
        return NULL;
    }

    jobject jaddress = NULL;

    AutoPtr<ArrayOf<Byte> > baddress;
    address->GetAddress((ArrayOf<Byte>**)&baddress);
    String hostName;
    address->GetHostName(&hostName);

    jbyteArray jbArray = GetJavaByteArray(env, baddress);
    jstring jhostName = GetJavaString(env, hostName);

    jclass inaddKlass = env->FindClass("java/net/InetAddress");
    CheckErrorAndLog(env, "GetJavaInetAddress FindClass: InetAddress : %d!\n", __LINE__);

    jmethodID m = env->GetStaticMethodID(inaddKlass, "getByAddress", "(Ljava/lang/String;[B)Ljava/net/InetAddress;");
    CheckErrorAndLog(env, "GetJavaInetAddress GetStaticMethodID: getByAddress : %d!\n", __LINE__);

    jaddress = env->CallStaticObjectMethod(inaddKlass, m, jhostName, jbArray);
    CheckErrorAndLog(env, "CallStaticObjectMethod: getByAddress(): %d!\n", __LINE__);

    env->DeleteLocalRef(jbArray);
    env->DeleteLocalRef(jhostName);
    env->DeleteLocalRef(inaddKlass);

    return jaddress;
}

jobject ElUtil::GetJavaInetAddress(
    /* [in] */ JNIEnv* env,
    /* [in] */ Int32 family,
    /* [in] */ Int32 ipaddress,
    /* [in] */ String hostName)
{
    if (env == NULL || ipaddress == 0) {
        ALOGE("GetJavaInetAddress: Invalid argumenet!");
        return NULL;
    }

    jobject jaddress = NULL;

    jclass addsKlass = env->FindClass("java/net/InetAddress");
    CheckErrorAndLog(env, "GetJavaInetAddress FindClass: InetAddress : %d!\n", __LINE__);

    jclass utilsKlass = env->FindClass("android/net/NetworkUtils");
    CheckErrorAndLog(env, "GetJavaInetAddress FindClass: NetworkUtils : %d!\n", __LINE__);

    jmethodID m = env->GetStaticMethodID(utilsKlass, "intToInetAddress", "(I)Ljava/net/InetAddress;");
    CheckErrorAndLog(env, "GetJavaInetAddress GetStaticMethodID: intToInetAddress : %d!\n", __LINE__);

    jaddress = env->CallStaticObjectMethod(utilsKlass, m, ipaddress);
    CheckErrorAndLog(env, "CallStaticObjectMethod: intToInetAddress(): %d!\n", __LINE__);
    env->DeleteLocalRef(utilsKlass);

    jfieldID f = env->GetFieldID(addsKlass, "family", "I");
    ElUtil::CheckErrorAndLog(env, "GetJavaInetAddress GetFieldID: family : %d!\n", __LINE__);

    env->SetIntField(jaddress, f, family);
    ElUtil::CheckErrorAndLog(env, "GetJavaInetAddress SetIntField: family : %d!\n", __LINE__);

    if (!hostName.IsNullOrEmpty()) {
        jstring jhostName = ElUtil::GetJavaString(env, hostName);

        f = env->GetFieldID(addsKlass, "hostName", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetJavaInetAddress GetFieldID: hostName : %d!\n", __LINE__);

        env->SetObjectField(jaddress, f, jhostName);
        ElUtil::CheckErrorAndLog(env, "GetJavaInetAddress SetObjectField: hostName : %d!\n", __LINE__);
        env->DeleteLocalRef(jhostName);
    }

    env->DeleteLocalRef(addsKlass);
    return jaddress;
}

jobject ElUtil::GetJavaWifiSsid(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiSsid* wiSsid)
{
    if (env == NULL || wiSsid == NULL) {
        ALOGE("GetJavaWifiSsid: Invalid argumenet!");
        return NULL;
    }

    jobject jwiSsid = NULL;

    jclass ssidKlass = env->FindClass("android/net/wifi/WifiSsid");
    CheckErrorAndLog(env, "GetJavaWifiSsid FindClass: WifiSsid : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(ssidKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaWifiSsid GetMethodID: WifiSsid : %d!\n", __LINE__);

    jwiSsid = env->NewObject(ssidKlass, m);
    CheckErrorAndLog(env, "GetJavaWifiSsid NewObject: WifiSsid : %d!\n", __LINE__);

    AutoPtr<ArrayOf<Byte> > octets;
    wiSsid->GetOctets((ArrayOf<Byte>**)&octets);
    if (octets != NULL) {
        jfieldID f = env->GetFieldID(ssidKlass, "octets", "Ljava/io/ByteArrayOutputStream;");
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiSsid GetFieldID: ByteArrayOutputStream : %d!\n", __LINE__);

        jobject joctets = env->GetObjectField(jwiSsid, f);
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiSsid GetObjectField: octets : %d!\n", __LINE__);

        Int32 length = octets->GetLength();
        jbyteArray jbArray = env->NewByteArray((jint)length);
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiSsid NewByteArray: %d!\n", __LINE__);

        Byte* payload = octets->GetPayload();

        env->SetByteArrayRegion(jbArray, 0, length, (jbyte *)payload);
        CheckErrorAndLog(env, "GetJavaWifiSsid SetByteArrayRegion: %d!\n", __LINE__);

        jclass bostreamKlass = env->FindClass("java/io/ByteArrayOutputStream");
        CheckErrorAndLog(env, "GetJavaWifiSsid FindClass: ByteArrayOutputStream : %d!\n", __LINE__);

        m = env->GetMethodID(bostreamKlass, "write", "([BII)V");
        CheckErrorAndLog(env, "GetJavaWifiSsid GetMethodID: write : %d!\n", __LINE__);

        env->CallVoidMethod(joctets, m, jbArray, 0, length);
        CheckErrorAndLog(env, "GetJavaWifiSsid CallVoidMethod: write : %d!\n", __LINE__);

        env->SetObjectField(jwiSsid, f, joctets);
        CheckErrorAndLog(env, "GetJavaWifiSsid SetObjectField: joctets : %d!\n", __LINE__);

        env->DeleteLocalRef(joctets);
        env->DeleteLocalRef(jbArray);
        env->DeleteLocalRef(bostreamKlass);

    }

    env->DeleteLocalRef(ssidKlass);

    return jwiSsid;
}

jobject ElUtil::GetJavaSupplicantState(
    /* [in] */ JNIEnv* env,
    /* [in] */ SupplicantState state)
{
    if (env == NULL) {
        ALOGE("GetJavaSupplicantState: Invalid argumenet!");
        return NULL;
    }

    jobject jstate = NULL;

    jclass supsKlass = env->FindClass("android/net/wifi/SupplicantState");
    CheckErrorAndLog(env, "GetJavaSupplicantState FindClass: SupplicantState : %d!\n", __LINE__);

    jfieldID f = NULL;

    switch(state) {
        case Elastos::Droid::Wifi::SupplicantState_DISCONNECTED: {
            f = env->GetStaticFieldID(supsKlass, "DISCONNECTED", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_INTERFACE_DISABLED: {
            f = env->GetStaticFieldID(supsKlass, "INTERFACE_DISABLED", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_INACTIVE: {
            f = env->GetStaticFieldID(supsKlass, "INACTIVE", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_SCANNING: {
            f = env->GetStaticFieldID(supsKlass, "SCANNING", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_AUTHENTICATING: {
            f = env->GetStaticFieldID(supsKlass, "AUTHENTICATING", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_ASSOCIATING: {
            f = env->GetStaticFieldID(supsKlass, "ASSOCIATING", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_ASSOCIATED: {
            f = env->GetStaticFieldID(supsKlass, "ASSOCIATED", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_FOUR_WAY_HANDSHAKE: {
            f = env->GetStaticFieldID(supsKlass, "FOUR_WAY_HANDSHAKE", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_GROUP_HANDSHAKE: {
            f = env->GetStaticFieldID(supsKlass, "GROUP_HANDSHAKE", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_COMPLETED: {
            f = env->GetStaticFieldID(supsKlass, "COMPLETED", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_DORMANT: {
            f = env->GetStaticFieldID(supsKlass, "DORMANT", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_UNINITIALIZED: {
            f = env->GetStaticFieldID(supsKlass, "UNINITIALIZED", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        case Elastos::Droid::Wifi::SupplicantState_INVALID: {
            f = env->GetStaticFieldID(supsKlass, "INVALID", "Landroid/net/wifi/SupplicantState;");
            break;
        }
        default:
            assert(0);
            return NULL;
    }
    CheckErrorAndLog(env, "GetStaticFieldID: %d!\n", __LINE__);

    jstate = env->GetStaticObjectField(supsKlass, f);
    CheckErrorAndLog(env, "GetJavaSupplicantState GetStaticObjectField: %d!\n", __LINE__);

    env->DeleteLocalRef(supsKlass);

    return jstate;
}

jobject ElUtil::GetJavaSupplicantState(
    /* [in] */ JNIEnv* env,
    /* [in] */ ISupplicantState* state)
{
    if (env == NULL || state == NULL) {
        ALOGE("GetJavaSupplicantState: Invalid argumenet!");
        return NULL;
    }

    SupplicantState suppState;
    state->Get(&suppState);

    return GetJavaSupplicantState(env, suppState);
}

bool ElUtil::ToElAccount(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jaccount,
    /* [out] */ IAccount** account)
{
    if (env == NULL || jaccount == NULL || account == NULL) {
        ALOGE("ToElAccount: Invalid argumenet!");
        return false;
    }

    jclass accountKlass = env->FindClass("android/accounts/Account");
    CheckErrorAndLog(env, "FindClass: Account : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(accountKlass, "name", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: name : %d!\n", __LINE__);

    jstring jname = (jstring)env->GetObjectField(jaccount, f);
    CheckErrorAndLog(env, "GetObjectField name : %d!\n", __LINE__);
    String name = ElUtil::ToElString(env, jname);
    env->DeleteLocalRef(jname);

    f = env->GetFieldID(accountKlass, "type", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: type : %d!\n", __LINE__);

    jstring jtype = (jstring)env->GetObjectField(jaccount, f);
    CheckErrorAndLog(env, "GetObjectField type : %d!\n", __LINE__);
    String type = ElUtil::ToElString(env, jtype);
    env->DeleteLocalRef(jtype);

    if (NOERROR != CAccount::New(name, type, account)) {
        ALOGD("ToElAccount: create CRegion fail!");
        return false;
    }

    env->DeleteLocalRef(accountKlass);

    return true;
}

jobject ElUtil::GetJavaScanResult(
    /* [in] */ JNIEnv* env,
    /* [in] */ IScanResult* result)
{
    if (env == NULL || result == NULL) {
        ALOGE("GetJavaScanResult: Invalid argumenet!");
        return NULL;
    }

    jobject jresult = NULL;

    jclass srKlass = env->FindClass("android/net/wifi/ScanResult");
    CheckErrorAndLog(env, "GetJavaScanResult FindClass: ScanResult : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(srKlass, "<init>", "(Landroid/net/wifi/WifiSsid;Ljava/lang/String;Ljava/lang/String;IIJII)V");
    CheckErrorAndLog(env, "GetJavaScanResult GetMethodID: ScanResult : %d!\n", __LINE__);

    AutoPtr<IWifiSsid> ssid;
    result->GetWifiSsid((IWifiSsid**)&ssid);
    jobject jssid = NULL;
    if (ssid != NULL) {
        jssid = ElUtil::GetJavaWifiSsid(env, ssid);
    }

    String BSSID;
    result->GetBSSID(&BSSID);
    jobject jBSSID = NULL;
    if (!BSSID.IsNullOrEmpty()) {
        jBSSID = ElUtil::GetJavaString(env, BSSID);
    }

    String capabilities;
    result->GetCapabilities(&capabilities);
    jobject jcapabilities = NULL;
    if (!capabilities.IsNullOrEmpty()) {
        jcapabilities = ElUtil::GetJavaString(env, capabilities);
    }

    Int32 level;
    result->GetLevel(&level);

    Int32 frequency;
    result->GetFrequency(&frequency);

    Int64 timestamp;
    result->GetTimestamp(&timestamp);

    Int32 distanceCm;
    result->GetDistanceCm(&distanceCm);

    Int32 distanceSdCm;
    result->GetDistanceSdCm(&distanceSdCm);

    Int64 seen;
    result->GetSeen(&seen);

    Int32 autoJoinStatus;
    result->GetAutoJoinStatus(&autoJoinStatus);

    Boolean untrusted;
    result->GetUntrusted(&untrusted);

    Int32 numConnection;
    result->GetNumConnection(&numConnection);

    Int32 numUsage;
    result->GetNumUsage(&numUsage);

    Int32 numIpConfigFailures;
    result->GetNumIpConfigFailures(&numIpConfigFailures);

    Int32 isAutoJoinCandidate;
    result->GetIsAutoJoinCandidate(&isAutoJoinCandidate);

    AutoPtr<ArrayOf<IScanResultInformationElement*> > informationElements;
    result->GetInformationElements((ArrayOf<IScanResultInformationElement*>**)&informationElements);

    jresult = env->NewObject(srKlass, m, jssid, jBSSID, jcapabilities, (jint)level,
        (jint)frequency, (jlong)timestamp, distanceCm, distanceSdCm);
    CheckErrorAndLog(env, "GetJavaScanResult NewObject: ScanResult : %d!\n", __LINE__);

    SetJavalongField(env, srKlass, jresult, seen, "seen", "GetJavaScanResult");
    SetJavaIntField(env, srKlass, jresult, autoJoinStatus, "autoJoinStatus", "GetJavaScanResult");
    SetJavaBoolField(env, srKlass, jresult, untrusted, "untrusted", "GetJavaScanResult");
    SetJavaIntField(env, srKlass, jresult, numConnection, "numConnection", "GetJavaScanResult");
    SetJavaIntField(env, srKlass, jresult, numUsage, "numUsage", "GetJavaScanResult");
    SetJavaIntField(env, srKlass, jresult, numIpConfigFailures, "numIpConfigFailures", "GetJavaScanResult");
    SetJavaIntField(env, srKlass, jresult, isAutoJoinCandidate, "isAutoJoinCandidate", "GetJavaScanResult");

    if (informationElements != NULL && informationElements->GetLength() > 0) {
        Int32 count = informationElements->GetLength();
        jobjectArray jinformationElements = env->NewObjectArray((jsize)count, srKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: InformationElement : %d!\n", __LINE__);

        jclass srieKlass = env->FindClass("android/net/wifi/ScanResult$InformationElement");
        CheckErrorAndLog(env, "GetJavaScanResult FindClass: ScanResult$InformationElement : %d!\n", __LINE__);

        m = env->GetMethodID(srieKlass, "<init>", "()V");
        CheckErrorAndLog(env, "GetJavaScanResult GetMethodID: InformationElement : %d!\n", __LINE__);

        jfieldID fid = env->GetFieldID(srieKlass, "id", "I");
        CheckErrorAndLog(env, "GetFieldID: id : %d!\n", __LINE__);

        jfieldID fbytes = env->GetFieldID(srieKlass, "bytes", "[B");
        CheckErrorAndLog(env, "GetFieldID: bytes : %d!\n", __LINE__);

        for (Int32 i = 0; i < count; i++) {
            Int32 id;
            (*informationElements)[i]->GetId(&id);
            AutoPtr<ArrayOf<Byte> > bytes;
            (*informationElements)[i]->GetBytes((ArrayOf<Byte>**)&bytes);
            jbyteArray jbytes = GetJavaByteArray(env, bytes);

            jobject jelement = env->NewObject(srieKlass, m);
            CheckErrorAndLog(env, "GetJavaScanResult NewObject: InformationElement : %d!\n", __LINE__);

            env->SetIntField(jelement, fid, id);
            CheckErrorAndLog(env, "GetJavaScanResult: SetIntField : %d!\n", __LINE__);

            env->SetObjectField(jelement, fbytes, jbytes);
            CheckErrorAndLog(env, "GetJavaScanResult: SetIntField : %d!\n", __LINE__);

            env->SetObjectArrayElement(jinformationElements, i, jelement);
            CheckErrorAndLog(env, "SetObjectArrayElement: InformationElement : %d!\n", __LINE__);

            env->DeleteLocalRef(jelement);
            env->DeleteLocalRef(jbytes);
        }

        jfieldID f = env->GetFieldID(srKlass, "informationElements", "[Landroid/net/wifi/ScanResult$InformationElement;");
        CheckErrorAndLog(env, "GetFieldID: informationElements : %d!\n", __LINE__);

        env->SetObjectField(jresult, f, jinformationElements);

        env->DeleteLocalRef(srieKlass);
        env->DeleteLocalRef(jinformationElements);
    }
    env->DeleteLocalRef(srKlass);
    env->DeleteLocalRef(jssid);
    env->DeleteLocalRef(jBSSID);
    env->DeleteLocalRef(jcapabilities);

    return jresult;
}

bool ElUtil::ToElMessage(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jmsg,
    /* [out] */ IMessage** msg)
{
    if (env == NULL || jmsg == NULL || msg == NULL) {
        ALOGE("ToElMessage: Invalid argumenet!");
        return false;
    }

    jclass msgKlass = env->FindClass("android/os/Message");
    CheckErrorAndLog(env, "FindClass: Message : %d!\n", __LINE__);

    AutoPtr<IMessageHelper> helper;
    CMessageHelper::AcquireSingleton((IMessageHelper**)&helper);

    if (NOERROR != helper->Obtain(msg)) {
        ALOGD("ToElMessage: create CMessage fail!");
        return false;
    }

    jfieldID f = env->GetFieldID(msgKlass, "what", "I");
    CheckErrorAndLog(env, "GetFieldID: what : %d!\n", __LINE__);

    jint jwhat = env->GetIntField(jmsg, f);
    CheckErrorAndLog(env, "GetIntField what : %d!\n", __LINE__);
    (*msg)->SetWhat((Int32)jwhat);

    f = env->GetFieldID(msgKlass, "arg1", "I");
    CheckErrorAndLog(env, "GetFieldID: arg1 : %d!\n", __LINE__);

    jint jarg1 = env->GetIntField(jmsg, f);
    CheckErrorAndLog(env, "GetIntField arg1 : %d!\n", __LINE__);
    (*msg)->SetArg1((Int32)jarg1);

    f = env->GetFieldID(msgKlass, "arg2", "I");
    CheckErrorAndLog(env, "GetFieldID: arg2 : %d!\n", __LINE__);

    jint jarg2 = env->GetIntField(jmsg, f);
    CheckErrorAndLog(env, "GetIntField arg2 : %d!\n", __LINE__);
    (*msg)->SetArg2((Int32)jarg2);

    f = env->GetFieldID(msgKlass, "obj", "Ljava/lang/Object;");
    CheckErrorAndLog(env, "GetFieldID: obj : %d!\n", __LINE__);

    jobject jobj = env->GetObjectField(jmsg, f);
    CheckErrorAndLog(env, "GetIntField obj : %d!\n", __LINE__);
    if (jobj != NULL) {
        String objClassName = GetClassName(env, jobj);
        if (objClassName.Equals("android.net.wifi.WifiConfiguration")) {
            AutoPtr<IWifiConfiguration> config;
            if (ElUtil::ToElWifiConfiguration(env, jobj, (IWifiConfiguration**)&config)) {
                (*msg)->SetObj(config->Probe(EIID_IInterface));
            }
            else {
                ALOGE("ToElMessage, ToElWifiConfiguration fail!");
            }
        }
        else if (objClassName.Equals("android.net.wifi.p2p.WifiP2pWfdInfo")) {
            AutoPtr<IWifiP2pWfdInfo> info;
            if (ElUtil::ToElWifiP2pWfdInfo(env, jobj, (IWifiP2pWfdInfo**)&info)) {
                (*msg)->SetObj(info->Probe(EIID_IInterface));
            }
            else {
                ALOGE("ToElMessage, ToElWifiP2pWfdInfo fail!");
            }
        }
        else if (objClassName.Equals("android.net.wifi.p2p.WifiP2pConfig")) {
            AutoPtr<IWifiP2pConfig> config;
            if (ElUtil::ToElWifiP2pConfig(env, jobj, (IWifiP2pConfig**)&config)) {
                (*msg)->SetObj(config->Probe(EIID_IInterface));
            }
            else {
                ALOGE("ToElMessage, ToElWifiP2pWfdInfo fail!");
            }
        }
        else {
            ALOGE("ToElMessage, Unsupported object: %s line: %d", objClassName.string(), __LINE__);
        }

        env->DeleteLocalRef(jobj);
    }

    f = env->GetFieldID(msgKlass, "replyTo", "Landroid/os/Messenger;");
    CheckErrorAndLog(env, "GetFieldID: replyTo : %d!\n", __LINE__);

    jobject jreplyTo = env->GetObjectField(jmsg, f);
    CheckErrorAndLog(env, "GetIntField replyTo : %d!\n", __LINE__);
    if (jreplyTo != NULL) {
        AutoPtr<IMessenger> msgr;
        if (ElUtil::ToElMessenger(env, jreplyTo, (IMessenger**)&msgr)) {
            (*msg)->SetReplyTo(msgr);
        }
        else {
            ALOGE("ToElMessage ToElMessenger fail!");
        }

        env->DeleteLocalRef(jreplyTo);
    }


    f = env->GetFieldID(msgKlass, "when", "J");
    CheckErrorAndLog(env, "GetFieldID: when : %d!\n", __LINE__);

    jlong jwhen = env->GetLongField(jmsg, f);
    CheckErrorAndLog(env, "GetLongField when : %d!\n", __LINE__);
    (*msg)->SetWhen(jwhen);

    f = env->GetFieldID(msgKlass, "data", "Landroid/os/Bundle;");
    CheckErrorAndLog(env, "GetFieldID: data : %d!\n", __LINE__);

    jobject jdata = env->GetObjectField(jmsg, f);
    CheckErrorAndLog(env, "GetObjectField data : %d!\n", __LINE__);
    if (jdata != NULL) {
        AutoPtr<IBundle> data;
        if (ElUtil::ToElBundle(env, jdata, (IBundle**)&data)) {
            (*msg)->SetData(data);
        }
        else {
            ALOGE("ToElMessage ToElBundle fail!!");
        }

        env->DeleteLocalRef(jdata);
    }

    jint jsendingUid = GetJavaIntField(env, msgKlass, jmsg, "sendingUid", "ToElMessage");
    (*msg)->SetSendingUid(jsendingUid);

    env->DeleteLocalRef(msgKlass);

    return true;
}

bool ElUtil::ToElMessenger(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jmsgr,
    /* [out] */ IMessenger** msgr)
{
    if (env == NULL || jmsgr == NULL || msgr == NULL){
        ALOGE("ToElMessenger: Invalid argumenet!");
        return false;
    }

    jclass msgrKlass = env->FindClass("android/os/Messenger");
    CheckErrorAndLog(env, "FindClass: Messenger : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(msgrKlass, "mTarget", "Landroid/os/IMessenger;");
    CheckErrorAndLog(env, "GetFieldID: obj : %d!\n", __LINE__);
    env->DeleteLocalRef(msgrKlass);

    jobject jmTarget = env->GetObjectField(jmsgr, f);
    CheckErrorAndLog(env, "GetObjectField mTarget : %d!\n", __LINE__);
    if (jmTarget != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jmTarget);
        env->DeleteLocalRef(jmTarget);

        AutoPtr<IIMessenger> imsgr;
        if (NOERROR == CMessengerNative::New((Handle64)jvm, (Handle64)jInstance, (IIMessenger**)&imsgr)) {
            if (NOERROR == CMessenger::New(imsgr, msgr)) {
                return true;
            }
            else {
               ALOGE("ToElMessenger new CMessenger fail!\n");
            }
        }
        else {
            ALOGE("ToElMessenger new CMessengerNative fail!\n");
        }
    }
    else {
        ALOGE("ToElMessenger jmTarget is NULL!");
    }

    return false;
}

bool ElUtil::ToElWifiConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconfig,
    /* [out] */ IWifiConfiguration** config)
{
    if (env == NULL || jconfig == NULL || config == NULL) {
        ALOGE("ToElWifiConfiguration: Invalid argumenet!");
        return false;
    }

    jclass cfgKlass = env->FindClass("android/net/wifi/WifiConfiguration");
    CheckErrorAndLog(env, "FindClass: WifiConfiguration : %d!\n", __LINE__);

    if (NOERROR != CWifiConfiguration::New(config)) {
        ALOGE("ToElWifiConfiguration: new CWifiConfiguration fail!");
        return false;
    }

    jfieldID f = env->GetFieldID(cfgKlass, "networkId", "I");
    CheckErrorAndLog(env, "GetFieldID: networkId : %d!\n", __LINE__);

    jint jnetworkId = env->GetIntField(jconfig, f);
    CheckErrorAndLog(env, "GetIntField networkId : %d!\n", __LINE__);
    (*config)->SetNetworkId((Int32)jnetworkId);

    f = env->GetFieldID(cfgKlass, "status", "I");
    CheckErrorAndLog(env, "GetFieldID: status : %d!\n", __LINE__);

    jint jstatus = env->GetIntField(jconfig, f);
    CheckErrorAndLog(env, "GetIntField status : %d!\n", __LINE__);
    (*config)->SetStatus((Int32)jstatus);

    f = env->GetFieldID(cfgKlass, "disableReason", "I");
    CheckErrorAndLog(env, "GetFieldID: disableReason : %d!\n", __LINE__);

    jint jdisableReason = env->GetIntField(jconfig, f);
    CheckErrorAndLog(env, "GetIntField disableReason : %d!\n", __LINE__);
    (*config)->SetDisableReason((Int32)jdisableReason);

    f = env->GetFieldID(cfgKlass, "SSID", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: SSID : %d!\n", __LINE__);

    jstring jSSID = (jstring)env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField SSID : %d!\n", __LINE__);
    String SSID = ElUtil::ToElString(env, jSSID);
    (*config)->SetSSID(SSID);

    f = env->GetFieldID(cfgKlass, "BSSID", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: BSSID : %d!\n", __LINE__);

    jstring jBSSID = (jstring)env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField BSSID : %d!\n", __LINE__);
    String BSSID = ElUtil::ToElString(env, jBSSID);
    (*config)->SetBSSID(BSSID);
    env->DeleteLocalRef(jBSSID);

    String autoJoinBSSID = GetJavaStringField(env, cfgKlass, jconfig, "autoJoinBSSID", "ToElWifiConfiguration");
    (*config)->SetAutoJoinBSSID(autoJoinBSSID);

    String FQDN = GetJavaStringField(env, cfgKlass, jconfig, "FQDN", "ToElWifiConfiguration");
    (*config)->SetFQDN(FQDN);

    String naiRealm = GetJavaStringField(env, cfgKlass, jconfig, "naiRealm", "ToElWifiConfiguration");
    (*config)->SetNaiRealm(naiRealm);

    f = env->GetFieldID(cfgKlass, "preSharedKey", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: preSharedKey : %d!\n", __LINE__);

    jstring jpreSharedKey = (jstring)env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField preSharedKey : %d!\n", __LINE__);
    String preSharedKey = ElUtil::ToElString(env, jpreSharedKey);
    (*config)->SetPreSharedKey(preSharedKey);
    env->DeleteLocalRef(jpreSharedKey);

    f = env->GetFieldID(cfgKlass, "wepKeys", "[Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: wepKeys : %d!\n", __LINE__);

    jobjectArray jwepKeys = (jobjectArray)env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField wepKeys : %d!\n", __LINE__);
    if (jwepKeys != NULL) {
        AutoPtr<ArrayOf<String> > wepKeys;
        if (ElUtil::ToElStringArray(env, jwepKeys, (ArrayOf<String>**)&wepKeys)) {
            (*config)->SetWepKeys(wepKeys);
        }
        else {
            ALOGE("ToElWifiConfiguration() ToElStringArray fail!");
        }
        env->DeleteLocalRef(jwepKeys);
    }

    f = env->GetFieldID(cfgKlass, "wepTxKeyIndex", "I");
    CheckErrorAndLog(env, "GetFieldID: wepTxKeyIndex : %d!\n", __LINE__);

    jint jwepTxKeyIndex = env->GetIntField(jconfig, f);
    CheckErrorAndLog(env, "GetIntField wepTxKeyIndex : %d!\n", __LINE__);
    (*config)->SetWepTxKeyIndex((Int32)jwepTxKeyIndex);

    f = env->GetFieldID(cfgKlass, "priority", "I");
    CheckErrorAndLog(env, "GetFieldID: priority : %d!\n", __LINE__);

    jint jpriority = env->GetIntField(jconfig, f);
    CheckErrorAndLog(env, "GetIntField priority : %d!\n", __LINE__);
    (*config)->SetPriority((Int32)jpriority);

    f = env->GetFieldID(cfgKlass, "hiddenSSID", "Z");
    CheckErrorAndLog(env, "GetFieldID: hiddenSSID : %d!\n", __LINE__);

    jboolean jhiddenSSID = env->GetBooleanField(jconfig, f);
    CheckErrorAndLog(env, "GetBooleanField hiddenSSID : %d!\n", __LINE__);
    (*config)->SetHiddenSSID((Boolean)jhiddenSSID);

    jboolean jisIBSS = GetJavaBoolField(env, cfgKlass, jconfig, "isIBSS", "ToElWifiConfiguration");
    (*config)->SetIsIBSS(jisIBSS);

    jint jfrequency = GetJavaIntField(env, cfgKlass, jconfig, "frequency", "ToElWifiConfiguration");
    (*config)->SetFrequency(jfrequency);

    jboolean jrequirePMF = GetJavaBoolField(env, cfgKlass, jconfig, "requirePMF", "ToElWifiConfiguration");
    (*config)->SetRequirePMF(jrequirePMF);

    String updateIdentifier = GetJavaStringField(env, cfgKlass, jconfig, "updateIdentifier", "ToElWifiConfiguration");
    (*config)->SetUpdateIdentifier(updateIdentifier);

    f = env->GetFieldID(cfgKlass, "allowedKeyManagement", "Ljava/util/BitSet;");
    CheckErrorAndLog(env, "GetFieldID: allowedKeyManagement : %d!\n", __LINE__);

    jobject jallowedKeyManagement = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField allowedKeyManagement : %d!\n", __LINE__);
    if (jallowedKeyManagement != NULL) {
        AutoPtr<IBitSet> allowedKeyManagement;
        if (ElUtil::ToElBitSet(env, jallowedKeyManagement, (IBitSet**)&allowedKeyManagement)) {
            (*config)->SetAllowedKeyManagement(allowedKeyManagement);
        }
        else {
            ALOGE("ToElWifiConfiguration: ToElBitSet fail!");
        }

        env->DeleteLocalRef(jallowedKeyManagement);
    }

    f = env->GetFieldID(cfgKlass, "allowedProtocols", "Ljava/util/BitSet;");
    CheckErrorAndLog(env, "GetFieldID: allowedProtocols : %d!\n", __LINE__);

    jobject jallowedProtocols = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField allowedProtocols : %d!\n", __LINE__);
    if (jallowedProtocols != NULL) {
        AutoPtr<IBitSet> allowedProtocols;
        if (ElUtil::ToElBitSet(env, jallowedProtocols, (IBitSet**)&allowedProtocols)) {
            (*config)->SetAllowedProtocols(allowedProtocols);
        }
        else {
            ALOGE("ToElWifiConfiguration: ToElBitSet fail!");
        }
        env->DeleteLocalRef(jallowedProtocols);
    }

    f = env->GetFieldID(cfgKlass, "allowedAuthAlgorithms", "Ljava/util/BitSet;");
    CheckErrorAndLog(env, "GetFieldID: allowedAuthAlgorithms : %d!\n", __LINE__);

    jobject jallowedAuthAlgorithms = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField allowedAuthAlgorithms : %d!\n", __LINE__);
    if (jallowedAuthAlgorithms != NULL) {
        AutoPtr<IBitSet> allowedAuthAlgorithms;
        if (ElUtil::ToElBitSet(env, jallowedAuthAlgorithms, (IBitSet**)&allowedAuthAlgorithms)) {
            (*config)->SetAllowedAuthAlgorithms(allowedAuthAlgorithms);
        }
        else {
            ALOGE("ToElWifiConfiguration: ToElBitSet fail!");
        }

        env->DeleteLocalRef(jallowedAuthAlgorithms);
    }

    f = env->GetFieldID(cfgKlass, "allowedPairwiseCiphers", "Ljava/util/BitSet;");
    CheckErrorAndLog(env, "GetFieldID: allowedPairwiseCiphers : %d!\n", __LINE__);

    jobject jallowedPairwiseCiphers = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField allowedPairwiseCiphers : %d!\n", __LINE__);
    if (jallowedPairwiseCiphers != NULL) {
        AutoPtr<IBitSet> allowedPairwiseCiphers;
        if (ElUtil::ToElBitSet(env, jallowedPairwiseCiphers, (IBitSet**)&allowedPairwiseCiphers)) {
            (*config)->SetAllowedPairwiseCiphers(allowedPairwiseCiphers);
        }
        else {
            ALOGE("ToElWifiConfiguration: ToElBitSet fail!");
        }

        env->DeleteLocalRef(jallowedPairwiseCiphers);
    }

    f = env->GetFieldID(cfgKlass, "allowedGroupCiphers", "Ljava/util/BitSet;");
    CheckErrorAndLog(env, "GetFieldID: allowedGroupCiphers : %d!\n", __LINE__);

    jobject jallowedGroupCiphers = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField allowedGroupCiphers : %d!\n", __LINE__);
    if (jallowedGroupCiphers != NULL) {
        AutoPtr<IBitSet> allowedGroupCiphers;
        if (ElUtil::ToElBitSet(env, jallowedGroupCiphers, (IBitSet**)&allowedGroupCiphers)) {
            (*config)->SetAllowedPairwiseCiphers(allowedGroupCiphers);
        }
        else {
            ALOGE("ToElWifiConfiguration: ToElBitSet fail!");
        }
        env->DeleteLocalRef(jallowedGroupCiphers);
    }

    f = env->GetFieldID(cfgKlass, "enterpriseConfig", "Landroid/net/wifi/WifiEnterpriseConfig;");
    CheckErrorAndLog(env, "GetFieldID: enterpriseConfig : %d!\n", __LINE__);

    jobject jenterpriseConfig = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField enterpriseConfig : %d!\n", __LINE__);
    if (jenterpriseConfig != NULL) {
        AutoPtr<IWifiEnterpriseConfig> enterpriseConfig;
        if (ElUtil::ToElWifiEnterpriseConfig(env, jenterpriseConfig, (IWifiEnterpriseConfig**)&enterpriseConfig)) {
            (*config)->SetEnterpriseConfig(enterpriseConfig);
        }
        else {
            ALOGE("ToElWifiConfiguration: ToElWifiEnterpriseConfig fail!");
        }
        env->DeleteLocalRef(jenterpriseConfig);
    }

    f = env->GetFieldID(cfgKlass, "mIpConfiguration", "Landroid/net/IpConfiguration;");
    CheckErrorAndLog(env, "GetFieldID: mIpConfiguration : %d!\n", __LINE__);

    jobject jipConfiguration = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField mIpConfiguration : %d!\n", __LINE__);
    if (jipConfiguration != NULL) {
        AutoPtr<IIpConfiguration> ipConfiguration;
        if (ElUtil::ToElIpConfiguration(env, jipConfiguration, (IIpConfiguration**)&ipConfiguration)) {
            (*config)->SetIpConfiguration(ipConfiguration);
        }
        else {
            ALOGE("ToElWifiConfiguration: ToElIpConfiguration fail!");
        }
        env->DeleteLocalRef(jipConfiguration);
    }

    String dhcpServer = GetJavaStringField(env, cfgKlass, jconfig, "dhcpServer", "ToElWifiConfiguration");
    (*config)->SetDhcpServer(dhcpServer);

    String defaultGwMacAddress = GetJavaStringField(env, cfgKlass, jconfig, "defaultGwMacAddress", "ToElWifiConfiguration");
    (*config)->SetDefaultGwMacAddress(defaultGwMacAddress);

    jint autoJoinStatus = GetJavaIntField(env, cfgKlass, jconfig, "autoJoinStatus", "ToElWifiConfiguration");
    (*config)->SetAutoJoinStatus(autoJoinStatus);

    jboolean selfAdded = GetJavaBoolField(env, cfgKlass, jconfig, "selfAdded", "ToElWifiConfiguration");
    (*config)->SetSelfAdded(selfAdded);

    jboolean didSelfAdd = GetJavaBoolField(env, cfgKlass, jconfig, "didSelfAdd", "ToElWifiConfiguration");
    (*config)->SetDidSelfAdd(didSelfAdd);

    jboolean noInternetAccess = GetJavaBoolField(env, cfgKlass, jconfig, "noInternetAccess", "ToElWifiConfiguration");
    (*config)->SetNoInternetAccess(noInternetAccess);

    jint creatorUid = GetJavaIntField(env, cfgKlass, jconfig, "creatorUid", "ToElWifiConfiguration");
    (*config)->SetCreatorUid(creatorUid);

    jint lastConnectUid = GetJavaIntField(env, cfgKlass, jconfig, "lastConnectUid", "ToElWifiConfiguration");
    (*config)->SetLastConnectUid(lastConnectUid);

    jint lastUpdateUid = GetJavaIntField(env, cfgKlass, jconfig, "lastUpdateUid", "ToElWifiConfiguration");
    (*config)->SetLastUpdateUid(lastUpdateUid);

    jlong blackListTimestamp = GetJavalongField(env, cfgKlass, jconfig, "blackListTimestamp", "ToElWifiConfiguration");
    (*config)->SetBlackListTimestamp(blackListTimestamp);

    jlong lastConnectionFailure = GetJavalongField(env, cfgKlass, jconfig, "lastConnectionFailure", "ToElWifiConfiguration");
    (*config)->SetLastConnectionFailure(lastConnectionFailure);

    jint numConnectionFailures = GetJavaIntField(env, cfgKlass, jconfig, "numConnectionFailures", "ToElWifiConfiguration");
    (*config)->SetNumConnectionFailures(numConnectionFailures);

    jint numIpConfigFailures = GetJavaIntField(env, cfgKlass, jconfig, "numIpConfigFailures", "ToElWifiConfiguration");
    (*config)->SetNumIpConfigFailures(numIpConfigFailures);

    jint numAuthFailures = GetJavaIntField(env, cfgKlass, jconfig, "numAuthFailures", "ToElWifiConfiguration");
    (*config)->SetNumAuthFailures(numAuthFailures);

    jint numScorerOverride = GetJavaIntField(env, cfgKlass, jconfig, "numScorerOverride", "ToElWifiConfiguration");
    (*config)->SetNumScorerOverride(numScorerOverride);

    jint numScorerOverrideAndSwitchedNetwork = GetJavaIntField(env, cfgKlass, jconfig, "numScorerOverrideAndSwitchedNetwork", "ToElWifiConfiguration");
    (*config)->SetNumScorerOverrideAndSwitchedNetwork(numScorerOverrideAndSwitchedNetwork);

    jint numAssociation = GetJavaIntField(env, cfgKlass, jconfig, "numAssociation", "ToElWifiConfiguration");
    (*config)->SetNumAssociation(numAssociation);

    jint numUserTriggeredWifiDisableLowRSSI = GetJavaIntField(env, cfgKlass, jconfig, "numUserTriggeredWifiDisableLowRSSI", "ToElWifiConfiguration");
    (*config)->SetNumUserTriggeredWifiDisableLowRSSI(numUserTriggeredWifiDisableLowRSSI);

    jint numUserTriggeredWifiDisableBadRSSI = GetJavaIntField(env, cfgKlass, jconfig, "numUserTriggeredWifiDisableBadRSSI", "ToElWifiConfiguration");
    (*config)->SetNumUserTriggeredWifiDisableBadRSSI(numUserTriggeredWifiDisableBadRSSI);

    jint numUserTriggeredWifiDisableNotHighRSSI = GetJavaIntField(env, cfgKlass, jconfig, "numUserTriggeredWifiDisableNotHighRSSI", "ToElWifiConfiguration");
    (*config)->SetNumUserTriggeredWifiDisableNotHighRSSI(numUserTriggeredWifiDisableNotHighRSSI);

    jint numTicksAtLowRSSI = GetJavaIntField(env, cfgKlass, jconfig, "numTicksAtLowRSSI", "ToElWifiConfiguration");
    (*config)->SetNumTicksAtLowRSSI(numTicksAtLowRSSI);

    jint numTicksAtBadRSSI = GetJavaIntField(env, cfgKlass, jconfig, "numTicksAtBadRSSI", "ToElWifiConfiguration");
    (*config)->SetNumTicksAtBadRSSI(numTicksAtBadRSSI);

    jint numTicksAtNotHighRSSI = GetJavaIntField(env, cfgKlass, jconfig, "numTicksAtNotHighRSSI", "ToElWifiConfiguration");
    (*config)->SetNumTicksAtNotHighRSSI(numTicksAtNotHighRSSI);

    jint numUserTriggeredJoinAttempts = GetJavaIntField(env, cfgKlass, jconfig, "numUserTriggeredJoinAttempts", "ToElWifiConfiguration");
    (*config)->SetNumUserTriggeredJoinAttempts(numUserTriggeredJoinAttempts);

    jint autoJoinUseAggressiveJoinAttemptThreshold = GetJavaIntField(env, cfgKlass, jconfig, "autoJoinUseAggressiveJoinAttemptThreshold", "ToElWifiConfiguration");
    (*config)->SetAutoJoinUseAggressiveJoinAttemptThreshold(autoJoinUseAggressiveJoinAttemptThreshold);

    jboolean autoJoinBailedDueToLowRssi = GetJavaBoolField(env, cfgKlass, jconfig, "autoJoinBailedDueToLowRssi", "ToElWifiConfiguration");
    (*config)->SetAutoJoinBailedDueToLowRssi(autoJoinBailedDueToLowRssi);

    env->DeleteLocalRef(cfgKlass);

    return true;
}

bool ElUtil::ToElWifiEnterpriseConfig(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconfig,
    /* [out] */ IWifiEnterpriseConfig** config)
{
    if (env == NULL || jconfig == NULL || config == NULL) {
        ALOGE("ToElWifiEnterpriseConfig: Invalid argumenet!");
        return false;
    }

    if (NOERROR != CWifiEnterpriseConfig::New(config)) {
        ALOGE("ToElWifiEnterpriseConfig: new CWifiEnterpriseConfig fail!");
        return false;
    }

    AutoPtr<IParcel> parcel;
    Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);

    jclass parcelClass = env->FindClass("android/os/Parcel");
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig FindClass: Parcel : %d", __LINE__);

    jmethodID m = env->GetStaticMethodID(parcelClass, "obtain", "()Landroid/os/Parcel;");
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig GetStaticMethodID: obtain : %d", __LINE__);

    jobject jsource = env->CallStaticObjectMethod(parcelClass, m);
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallStaticObjectMethod: obtain : %d", __LINE__);

    jmethodID mReadInt = env->GetMethodID(parcelClass, "readInt", "()I");
    jmethodID mReadString = env->GetMethodID(parcelClass, "readString", "()Ljava/lang/String;");
    jmethodID mReadByteArray = env->GetMethodID(parcelClass, "readByteArray", "([B)V");

    jclass cfgKlass = env->FindClass("android/net/wifi/WifiEnterpriseConfig");
    CheckErrorAndLog(env, "FindClass: WifiEnterpriseConfig : %d!\n", __LINE__);

    m = env->GetMethodID(cfgKlass, "writeToParcel", "(Landroid/os/Parcel;I)V");
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig GetMethodID: writeToParcel line: %d", __LINE__);

    env->CallVoidMethod(jconfig, m, jsource, 0);
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallObjectMethod: writeToParcel line: %d", __LINE__);

    jmethodID mSetDataPosition = env->GetMethodID(parcelClass, "setDataPosition", "(I)V");
    env->CallVoidMethod(jsource, mSetDataPosition, 0);
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallVoidMethod: setDataPosition", __LINE__);

    Int32 count = env->CallIntMethod(jsource, mReadInt);
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallIntMethod: readInt", __LINE__);
    parcel->WriteInt32(count);
    for (Int32 i = 0; i < count; i++) {
        jstring jkey = (jstring)env->CallObjectMethod(jsource, mReadString);
        CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallObjectMethod: readString", __LINE__);

        jstring jvalue = (jstring)env->CallObjectMethod(jsource, mReadString);
        CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallObjectMethod: readString", __LINE__);

        parcel->WriteString(ToElString(env, jkey));
        parcel->WriteString(ToElString(env, jvalue));
        env->DeleteLocalRef(jkey);
        env->DeleteLocalRef(jvalue);
    }

    Int32 len = env->CallIntMethod(jsource, mReadInt);
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallIntMethod: readInt", __LINE__);
    parcel->WriteInt32(len);
    if (len > 0) {
        jbyteArray jbytearray = env->NewByteArray((jsize)len);
        CheckErrorAndLog(env, "NewByteArray: %d!\n", __LINE__);
        env->CallVoidMethod(jsource, mReadByteArray, jbytearray);
        CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallVoidMethod: readByteArray", __LINE__);
        AutoPtr<ArrayOf<Byte> > byteArray;
        ToElByteArray(env, jbytearray, (ArrayOf<Byte>**)&byteArray);
        parcel->WriteArrayOf((Handle32)byteArray.Get());
        env->DeleteLocalRef(jbytearray);
    }

    len = env->CallIntMethod(jsource, mReadInt);
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallIntMethod: readInt", __LINE__);
    parcel->WriteInt32(len);
    if (len > 0) {
        jbyteArray jbytearray = env->NewByteArray((jsize)len);
        CheckErrorAndLog(env, "NewByteArray: %d!\n", __LINE__);
        env->CallVoidMethod(jsource, mReadByteArray, jbytearray);
        CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallVoidMethod: readByteArray", __LINE__);
        AutoPtr<ArrayOf<Byte> > byteArray;
        ToElByteArray(env, jbytearray, (ArrayOf<Byte>**)&byteArray);
        parcel->WriteArrayOf((Handle32)byteArray.Get());
        env->DeleteLocalRef(jbytearray);

        jstring jalgorithm = (jstring)env->CallObjectMethod(jsource, mReadString);
        CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallObjectMethod: readString", __LINE__);
        parcel->WriteString(ToElString(env, jalgorithm));
        env->DeleteLocalRef(jalgorithm);
    }

    len = env->CallIntMethod(jsource, mReadInt);
    CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallIntMethod: readInt", __LINE__);
    parcel->WriteInt32(len);
    if (len > 0) {
        jbyteArray jbytearray = env->NewByteArray((jsize)len);
        CheckErrorAndLog(env, "NewByteArray: %d!\n", __LINE__);
        env->CallVoidMethod(jsource, mReadByteArray, jbytearray);
        CheckErrorAndLog(env, "ToElWifiEnterpriseConfig CallVoidMethod: readByteArray", __LINE__);
        AutoPtr<ArrayOf<Byte> > byteArray;
        ToElByteArray(env, jbytearray, (ArrayOf<Byte>**)&byteArray);
        parcel->WriteArrayOf((Handle32)byteArray.Get());
        env->DeleteLocalRef(jbytearray);
    }

    env->DeleteLocalRef(parcelClass);
    env->DeleteLocalRef(jsource);
    env->DeleteLocalRef(cfgKlass);

    parcel->SetDataPosition(0);
    IParcelable::Probe(*config)->ReadFromParcel(parcel);

    return true;
}

bool ElUtil::ToElIpConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconfig,
    /* [out] */ IIpConfiguration** config)
{
    if (env == NULL || jconfig == NULL || config == NULL) {
        ALOGE("ToElIpConfiguration: Invalid argumenet!");
        return false;
    }

    if (NOERROR != CIpConfiguration::New(config)) {
        ALOGE("ToElIpConfiguration: new CIpConfiguration fail!");
        return false;
    }

    jclass cfgKlass = env->FindClass("android/net/IpConfiguration");
    CheckErrorAndLog(env, "FindClass: IpConfiguration : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(cfgKlass, "ipAssignment", "Landroid/net/IpConfiguration$IpAssignment;");
    CheckErrorAndLog(env, "GetFieldID: ipAssignment : %d!\n", __LINE__);

    jobject jipAssignment = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField ipAssignment : %d!\n", __LINE__);

    jclass ipaClass = env->FindClass("android/net/IpConfiguration$IpAssignment");
    ElUtil::CheckErrorAndLog(env, "ToElIpConfiguration Fail FindClass IpAssignment %d", __LINE__);

    jmethodID m = env->GetMethodID(ipaClass, "ordinal", "()I");
    CheckErrorAndLog(env, "GetMethodID: ordinal : %d!\n", __LINE__);

    jint jvalue = env->CallIntMethod(jipAssignment, m);
    (*config)->SetIpAssignment((Elastos::Droid::Net::IpConfigurationIpAssignment)jvalue);

    env->DeleteLocalRef(jipAssignment);
    env->DeleteLocalRef(ipaClass);

    f = env->GetFieldID(cfgKlass, "proxySettings", "Landroid/net/IpConfiguration$ProxySettings;");
    CheckErrorAndLog(env, "GetFieldID: proxySettings : %d!\n", __LINE__);

    jobject jproxySettings = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField proxySettings : %d!\n", __LINE__);

    jclass psClass = env->FindClass("android/net/IpConfiguration$ProxySettings");
    ElUtil::CheckErrorAndLog(env, "ToElIpConfiguration Fail FindClass ProxySettings %d", __LINE__);

    m = env->GetMethodID(psClass, "ordinal", "()I");
    CheckErrorAndLog(env, "GetMethodID: ordinal : %d!\n", __LINE__);

    jvalue = env->CallIntMethod(jproxySettings, m);
    (*config)->SetIpAssignment((Elastos::Droid::Net::IpConfigurationProxySettings)jvalue);

    env->DeleteLocalRef(jproxySettings);
    env->DeleteLocalRef(psClass);

    f = env->GetFieldID(cfgKlass, "staticIpConfiguration", "Landroid/net/IpConfiguration$ProxySettings;");
    CheckErrorAndLog(env, "GetFieldID: staticIpConfiguration : %d!\n", __LINE__);

    jobject jstaticIpConfiguration = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField staticIpConfiguration : %d!\n", __LINE__);
    if (jstaticIpConfiguration != NULL) {
        AutoPtr<IStaticIpConfiguration> staticIpConfiguration;
        if (ToElStaticIpConfiguration(env, jstaticIpConfiguration, (IStaticIpConfiguration**)&staticIpConfiguration)) {
            (*config)->SetStaticIpConfiguration(staticIpConfiguration);
        }
        else {
            ALOGE("ToElIpConfiguration: ToElStaticIpConfiguration fail!");
        }
        env->DeleteLocalRef(jstaticIpConfiguration);
    }

    f = env->GetFieldID(cfgKlass, "httpProxy", "Landroid/net/IpConfiguration$ProxySettings;");
    CheckErrorAndLog(env, "GetFieldID: httpProxy : %d!\n", __LINE__);

    jobject jhttpProxy = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField httpProxy : %d!\n", __LINE__);
    if (jhttpProxy != NULL) {
        AutoPtr<IProxyInfo> httpProxy;
        if (ToElProxyInfo(env, jhttpProxy, (IProxyInfo**)&httpProxy)) {
            (*config)->SetHttpProxy(httpProxy);
        }
        else {
            ALOGE("ToElIpConfiguration: ToElProxyInfo fail!");
        }
        env->DeleteLocalRef(jhttpProxy);
    }

    env->DeleteLocalRef(cfgKlass);

    return true;
}

bool ElUtil::ToElStaticIpConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconfig,
    /* [out] */ IStaticIpConfiguration** config)
{
    if (env == NULL || jconfig == NULL || config == NULL) {
        ALOGE("ToElStaticIpConfiguration: Invalid argumenet!");
        return false;
    }

    if (NOERROR != CStaticIpConfiguration::New(config)) {
        ALOGE("ToElStaticIpConfiguration: new CStaticIpConfiguration fail!");
        return false;
    }

    jclass cfgKlass = env->FindClass("android/net/StaticIpConfiguration");
    CheckErrorAndLog(env, "FindClass: StaticIpConfiguration : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(cfgKlass, "ipAddress", "Landroid/net/LinkAddress;");
    CheckErrorAndLog(env, "GetFieldID: ipAddress : %d!\n", __LINE__);

    jobject jipAddress = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField ipAddress : %d!\n", __LINE__);

    if (jipAddress != NULL) {
        AutoPtr<ILinkAddress> ipAddress;
        if (ToElLinkAddress(env, jipAddress, (ILinkAddress**)&ipAddress)) {
            (*config)->SetIpAddress(ipAddress);
        }
        else {
            ALOGE("ToElStaticIpConfiguration: ToElLinkAddress fail!");
        }
        env->DeleteLocalRef(jipAddress);
    }

    f = env->GetFieldID(cfgKlass, "gateway", "Ljava/net/InetAddress;");
    CheckErrorAndLog(env, "GetFieldID: gateway : %d!\n", __LINE__);

    jobject jgateway = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField gateway : %d!\n", __LINE__);

    if (jgateway != NULL) {
        AutoPtr<IInetAddress> gateway;
        if (ToElInetAddress(env, jgateway, (IInetAddress**)&gateway)) {
            (*config)->SetGateway(gateway);
        }
        else {
            ALOGE("ToElStaticIpConfiguration: ToElInetAddress fail!");
        }
        env->DeleteLocalRef(jgateway);
    }

    f = env->GetFieldID(cfgKlass, "dnsServers", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: dnsServers : %d!\n", __LINE__);

    jobject jdnsServers = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField dnsServers : %d!\n", __LINE__);

    if (jdnsServers != NULL) {
        jclass alistKlass = env->FindClass("java/util/ArrayList");
        CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID mSize = env->GetMethodID(alistKlass, "size", "()I");
        CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

        jint jsize = env->CallIntMethod(jdnsServers, mSize);
        CheckErrorAndLog(env, "CallIntMethod: jdnsServers : %d!\n", __LINE__);

        if (jsize > 0) {
            jmethodID mGet = env->GetMethodID(alistKlass, "get", "(I)Ljava/lang/Object;");
            CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

            AutoPtr<IArrayList> dnsServers;
            (*config)->GetDnsServers((IArrayList**)&dnsServers);
            for (jint i = 0; i < jsize; i++) {
                jobject jdnsServer = env->CallObjectMethod(jdnsServers, mGet, i);
                CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                AutoPtr<IInetAddress> dnsServer;
                if (ToElInetAddress(env, jdnsServer, (IInetAddress**)&dnsServer)) {
                    dnsServers->Add(dnsServer);
                }
                else {
                    ALOGE("ToElStaticIpConfiguration: jdnsServer ToElInetAddress fail!");
                }
                env->DeleteLocalRef(jdnsServer);
            }
        }
        env->DeleteLocalRef(alistKlass);
        env->DeleteLocalRef(jdnsServers);
    }

    env->DeleteLocalRef(cfgKlass);

    return true;
}

bool ElUtil::ToElProxyInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IProxyInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElProxyInfo: Invalid argumenet!");
        return false;
    }


    jclass piKlass = env->FindClass("android/net/ProxyInfo");
    CheckErrorAndLog(env, "FindClass: ProxyInfo : %d!\n", __LINE__);

    String host = GetJavaStringField(env, piKlass, jinfo, "mHost", "ToElProxyInfo");
    Int32 port = GetJavaIntField(env, piKlass, jinfo, "mPort", "ToElProxyInfo");
    String exclusionList = GetJavaStringField(env, piKlass, jinfo, "mExclusionList", "ToElProxyInfo");

    if (NOERROR != CProxyInfo::New(host, port, exclusionList, info)) {
        ALOGE("ToElProxyInfo: new CProxyInfo fail!");
        env->DeleteLocalRef(piKlass);
        return false;
    }

    jfieldID f = env->GetFieldID(piKlass, "mParsedExclusionList", "[Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: mParsedExclusionList : %d!\n", __LINE__);

    jobjectArray jparsedExclusionList = (jobjectArray)env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField mParsedExclusionList : %d!\n", __LINE__);

    if (jparsedExclusionList != NULL) {
        AutoPtr<ArrayOf<String> > parsedExclusionList;
        if (ToElStringArray(env, jparsedExclusionList, (ArrayOf<String>**)&parsedExclusionList)) {
            (*info)->SetExclusionList(parsedExclusionList);
        }
        else {
            ALOGE("ToElProxyInfo: ToElStringArray fail!");
        }
        env->DeleteLocalRef(jparsedExclusionList);
    }

    f = env->GetFieldID(piKlass, "mPacFileUrl", "[Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: mPacFileUrl : %d!\n", __LINE__);

    jobjectArray jpacFileUrl = (jobjectArray)env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField mPacFileUrl : %d!\n", __LINE__);

    if (jpacFileUrl != NULL) {
        AutoPtr<IUri> pacFileUrl;
        if (ToElUri(env, jpacFileUrl, (IUri**)&pacFileUrl)) {
            (*info)->SetPacFileUrl(pacFileUrl);
        }
        else {
            ALOGE("ToElProxyInfo: ToElUri fail!");
        }
        env->DeleteLocalRef(jpacFileUrl);
    }

    env->DeleteLocalRef(piKlass);

    return true;
}

bool ElUtil::ToElBitSet(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jbitset,
    /* [out] */ IBitSet** bitset)
{
    if (env == NULL || jbitset == NULL || bitset == NULL) {
        ALOGE("ToElBitSet: Invalid argumenet!");
        return false;
    }

    jclass bitsetKlass = env->FindClass("java/util/BitSet");
    CheckErrorAndLog(env, "FindClass: BitSet : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(bitsetKlass, "toByteArray", "()[B");
    CheckErrorAndLog(env, "GetStaticMethodID: valueOf : %d!\n", __LINE__);
    env->DeleteLocalRef(bitsetKlass);

    jbyteArray jbyteArr = (jbyteArray)env->CallObjectMethod(jbitset, m);
    CheckErrorAndLog(env, "CallObjectMethod: toByteArray(): %d!\n", __LINE__);
    if (jbyteArr != NULL) {
        jint size = env->GetArrayLength(jbyteArr);
        CheckErrorAndLog(env, "GetArrayLength:(): %d!\n", __LINE__);

        jbyte* jpayload = (jbyte*)malloc(size);
        env->GetByteArrayRegion(jbyteArr, 0, size, jpayload);
        CheckErrorAndLog(env, "GetByteArrayRegion:(): %d!\n", __LINE__);

        AutoPtr<ArrayOf<Byte> > byteArr = ArrayOf<Byte>::Alloc((Byte*)jpayload, size);

        AutoPtr<IBitSetHelper> helper;
        CBitSetHelper::AcquireSingleton((IBitSetHelper**)&helper);
        helper->ValueOf(byteArr, bitset);

        free(jpayload);
        env->DeleteLocalRef(jbyteArr);

        return true;
    }
    else {
        ALOGE("ToElBitSet: jbyteArr is NULL!");
    }

    return false;
}

bool ElUtil::ToElLinkProperties(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jlinkProperties,
    /* [out] */ ILinkProperties** linkProperties)
{
    if (env == NULL || jlinkProperties == NULL || linkProperties == NULL) {
        ALOGE("ToElLinkProperties: Invalid argumenet!");
        return false;
    }

    jclass linkpKlass = env->FindClass("android/net/LinkProperties");
    CheckErrorAndLog(env, "FindClass: LinkProperties : %d!\n", __LINE__);

    if (NOERROR != CLinkProperties::New(linkProperties)) {
        ALOGE("ToElLinkProperties: new CLinkProperties fail!");
        env->DeleteLocalRef(linkpKlass);
        return false;
    }

    jfieldID f = env->GetFieldID(linkpKlass, "mIfaceName", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: mIfaceName : %d!\n", __LINE__);

    jstring jmIfaceName = (jstring)env->GetObjectField(jlinkProperties, f);
    CheckErrorAndLog(env, "GetObjectField mIfaceName : %d!\n", __LINE__);
    String mIfaceName = ElUtil::ToElString(env, jmIfaceName);
    env->DeleteLocalRef(jmIfaceName);
    (*linkProperties)->SetInterfaceName(mIfaceName);

    jclass colKlass = env->FindClass("java/util/ArrayList");
    CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID mtoArray = env->GetMethodID(colKlass, "toArray", "()[Ljava/lang/Object;");
    CheckErrorAndLog(env, "GetMethodID: toArray : %d!\n", __LINE__);

    f = env->GetFieldID(linkpKlass, "mLinkAddresses", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: ArrayList : %d!\n", __LINE__);

    jobject jmLinkAddresses = env->GetObjectField(jlinkProperties, f);
    CheckErrorAndLog(env, "GetObjectField mLinkAddresses : %d!\n", __LINE__);
    if (jmLinkAddresses != NULL) {
        jobjectArray jmLinkAddressesArr = (jobjectArray)env->CallObjectMethod(jmLinkAddresses, mtoArray);
        CheckErrorAndLog(env, "ToElLinkProperties CallObjectMethod: mtoArray : %d!\n", __LINE__);
        env->DeleteLocalRef(jmLinkAddresses);

        int count = env->GetArrayLength(jmLinkAddressesArr);
        for (int i = 0; i < count; i++) {
            jobject jmLinkAddr = env->GetObjectArrayElement(jmLinkAddressesArr, i);
            CheckErrorAndLog(env, "GetObjectArrayElement: jmLinkAddressesArr : %d!\n", __LINE__);

            if (jmLinkAddr != NULL) {
                AutoPtr<ILinkAddress> linkAddr;
                if (ElUtil::ToElLinkAddress(env, jmLinkAddr, (ILinkAddress**)&linkAddr)) {
                    Boolean result;
                    (*linkProperties)->AddLinkAddress(linkAddr, &result);
                }
                else {
                    ALOGE("ToElLinkProperties ToElLinkAddress fail!!");
                }

                env->DeleteLocalRef(jmLinkAddr);
            }
        }

        env->DeleteLocalRef(jmLinkAddressesArr);
    }

    f = env->GetFieldID(linkpKlass, "mDnses", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: ArrayList : %d!\n", __LINE__);

    jobject jmDnses = env->GetObjectField(jlinkProperties, f);
    CheckErrorAndLog(env, "GetObjectField mDnses : %d!\n", __LINE__);
    if (jmDnses != NULL) {
        jobjectArray jmDnsesArr = (jobjectArray)env->CallObjectMethod(jmDnses, mtoArray);
        CheckErrorAndLog(env, "ToElLinkProperties CallObjectMethod: mtoArray : %d!\n", __LINE__);
        env->DeleteLocalRef(jmDnses);

        int count = env->GetArrayLength(jmDnsesArr);
        for (int i = 0; i < count; i++) {
            jobject jmDnses = env->GetObjectArrayElement(jmDnsesArr, i);
            CheckErrorAndLog(env, "GetObjectArrayElement: jmDnsesArr : %d!\n", __LINE__);

            if (jmDnses != NULL) {
                AutoPtr<IInetAddress> dns;
                if (ElUtil::ToElInetAddress(env, jmDnses, (IInetAddress**)&dns)) {
                    Boolean result;
                    (*linkProperties)->AddDnsServer(dns, &result);
                }
                else {
                    ALOGE("ToElLinkProperties ToElInetAddress fail!!");
                }

                env->DeleteLocalRef(jmDnses);
            }
        }
        env->DeleteLocalRef(jmDnsesArr);
    }

    String domains = GetJavaStringField(env, linkpKlass, jlinkProperties, "mDomains", "ToElLinkProperties");
    (*linkProperties)->SetDomains(domains);

    f = env->GetFieldID(linkpKlass, "mRoutes", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "GetFieldID: ArrayList : %d!\n", __LINE__);

    jobject jmRoutes = env->GetObjectField(jlinkProperties, f);
    CheckErrorAndLog(env, "GetObjectField mRoutes : %d!\n", __LINE__);
    if (jmRoutes != NULL) {
        jobjectArray jmRoutesArr = (jobjectArray)env->CallObjectMethod(jmRoutes, mtoArray);
        CheckErrorAndLog(env, "ToElLinkProperties CallObjectMethod: mtoArray : %d!\n", __LINE__);
        env->DeleteLocalRef(jmRoutes);

        int count = env->GetArrayLength(jmRoutesArr);
        for (int i = 0; i < count; i++) {
            jobject jmRoutes = env->GetObjectArrayElement(jmRoutesArr, i);
            CheckErrorAndLog(env, "GetObjectArrayElement: jmRoutesArr : %d!\n", __LINE__);

            if (jmRoutes != NULL) {
                AutoPtr<IRouteInfo> route;
                if (ElUtil::ToElRouteInfo(env, jmRoutes, (IRouteInfo**)&route)) {
                    Boolean result;
                    (*linkProperties)->AddRoute(route, &result);
                }
                else {
                    ALOGE("ToElLinkProperties ToElRouteInfo fail!!");
                }

                env->DeleteLocalRef(jmRoutes);
            }
        }
        env->DeleteLocalRef(jmRoutesArr);
    }

    f = env->GetFieldID(linkpKlass, "mHttpProxy", "Landroid/net/ProxyInfo;");
    CheckErrorAndLog(env, "GetFieldID: ProxyInfo : %d!\n", __LINE__);

    jobject jmHttpProxy = env->GetObjectField(jlinkProperties, f);
    CheckErrorAndLog(env, "GetObjectField mHttpProxy : %d!\n", __LINE__);
    if (jmHttpProxy != NULL) {
        AutoPtr<IProxyInfo> httpProxy;
        if (ElUtil::ToElProxyInfo(env, jmHttpProxy, (IProxyInfo**)&httpProxy)) {
            (*linkProperties)->SetHttpProxy(httpProxy);
        }
        else {
            ALOGE("ToElLinkProperties ToElProxyInfo fail!!");
        }

        env->DeleteLocalRef(jmHttpProxy);
    }

    jint jmtu = GetJavaIntField(env, linkpKlass, jlinkProperties, "mMtu", "ToElLinkProperties");
    (*linkProperties)->SetMtu(jmtu);

    String tcpBufferSizes = GetJavaStringField(env, linkpKlass, jlinkProperties, "mTcpBufferSizes", "ToElLinkProperties");
    (*linkProperties)->SetTcpBufferSizes(tcpBufferSizes);

    jmethodID m = env->GetMethodID(linkpKlass, "getStackedLinks", "()Ljava/util/List;");
    CheckErrorAndLog(env, "GetMethodID: getStackedLinks : %d!\n", __LINE__);

    jobject jstackedLinks = env->CallObjectMethod(jlinkProperties, m);
    CheckErrorAndLog(env, "ToElLinkProperties CallObjectMethod: %d!\n", __LINE__);

    if (jstackedLinks != NULL) {
        jclass listKlass = env->FindClass("java/util/List");
        CheckErrorAndLog(env, "FindClass: List : %d!\n", __LINE__);

        mtoArray = env->GetMethodID(listKlass, "toArray", "()[Ljava/lang/Object;");
        CheckErrorAndLog(env, "GetMethodID: toArray : %d!\n", __LINE__);

        jobjectArray jstackedLinksArr = (jobjectArray)env->CallObjectMethod(jstackedLinks, mtoArray);
        CheckErrorAndLog(env, "ToElLinkProperties CallObjectMethod: mtoArray : %d!\n", __LINE__);
        env->DeleteLocalRef(jstackedLinks);

        int count = env->GetArrayLength(jstackedLinksArr);
        for (int i = 0; i < count; i++) {
            jobject jstackedLink = env->GetObjectArrayElement(jstackedLinksArr, i);
            CheckErrorAndLog(env, "GetObjectArrayElement: jstackedLinksArr : %d!\n", __LINE__);

            if (jstackedLink != NULL) {
                AutoPtr<ILinkProperties> stackedLink;
                if (ElUtil::ToElLinkProperties(env, jstackedLink, (ILinkProperties**)&stackedLink)) {
                    Boolean result;
                    (*linkProperties)->AddStackedLink(stackedLink, &result);
                }
                else {
                    ALOGE("ToElLinkProperties ToElLinkProperties fail!!");
                }

                env->DeleteLocalRef(jstackedLink);
            }
        }

        env->DeleteLocalRef(listKlass);
        env->DeleteLocalRef(jstackedLinksArr);
    }

    env->DeleteLocalRef(linkpKlass);
    env->DeleteLocalRef(colKlass);

    return true;
}

jobject ElUtil::GetJavaDhcpInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IDhcpInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaDhcpInfo: Invalid argumenet!");
        return NULL;
    }

    jobject jinfo = NULL;

    jclass dhcpKlass = env->FindClass("android/net/DhcpInfo");
    CheckErrorAndLog(env, "GetJavaDhcpInfo FindClass: DhcpInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(dhcpKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaDhcpInfo GetMethodID: DhcpInfo : %d!\n", __LINE__);

    jinfo = env->NewObject(dhcpKlass, m);
    CheckErrorAndLog(env, "GetJavaDhcpInfo NewObject: DhcpInfo : %d!\n", __LINE__);

    Int32 tempInt;
    info->GetIpAddress(&tempInt);
    jfieldID f = env->GetFieldID(dhcpKlass, "ipAddress", "I");
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail GetFieldID: ipAddress : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail SetIntField: ipAddress : %d!\n", __LINE__);

    info->GetGateway(&tempInt);
    f = env->GetFieldID(dhcpKlass, "gateway", "I");
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail GetFieldID: gateway : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail SetIntField: gateway : %d!\n", __LINE__);

    info->GetNetmask(&tempInt);
    f = env->GetFieldID(dhcpKlass, "netmask", "I");
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail GetFieldID: netmask : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail SetIntField: netmask : %d!\n", __LINE__);

    info->GetDns1(&tempInt);
    f = env->GetFieldID(dhcpKlass, "dns1", "I");
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail GetFieldID: dns1 : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail SetIntField: dns1 : %d!\n", __LINE__);

    info->GetDns2(&tempInt);
    f = env->GetFieldID(dhcpKlass, "dns2", "I");
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail GetFieldID: dns2 : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail SetIntField: dns2 : %d!\n", __LINE__);

    info->GetServerAddress(&tempInt);
    f = env->GetFieldID(dhcpKlass, "serverAddress", "I");
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail GetFieldID: serverAddress : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail SetIntField: serverAddress : %d!\n", __LINE__);

    info->GetLeaseDuration(&tempInt);
    f = env->GetFieldID(dhcpKlass, "leaseDuration", "I");
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail GetFieldID: leaseDuration : %d!\n", __LINE__);

    env->SetIntField(jinfo, f, tempInt);
    CheckErrorAndLog(env, "GetJavaDhcpInfo Fail SetIntField: leaseDuration : %d!\n", __LINE__);

    env->DeleteLocalRef(dhcpKlass);

    return jinfo;
}

jobject ElUtil::GetJavaLinkProperties(
    /* [in] */ JNIEnv* env,
    /* [in] */ ILinkProperties* properties)
{
    if (env == NULL || properties == NULL) {
        ALOGE("GetJavaLinkProperties: Invalid argumenet!");
        return NULL;
    }

    jobject jproperties = NULL;

    jclass lProtiesKlass = env->FindClass("android/net/LinkProperties");
    CheckErrorAndLog(env, "GetJavaLinkProperties FindClass: LinkProperties : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(lProtiesKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaLinkProperties GetMethodID: LinkProperties : %d!\n", __LINE__);

    jproperties = env->NewObject(lProtiesKlass, m);
    CheckErrorAndLog(env, "GetJavaLinkProperties NewObject: LinkProperties : %d!\n", __LINE__);

    String ifaceName;
    properties->GetInterfaceName(&ifaceName);
    if (!ifaceName.IsNull()) {
        jfieldID f = env->GetFieldID(lProtiesKlass, "mIfaceName", "Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: mIfaceName : %d!\n", __LINE__);

        jstring jifaceName = ElUtil::GetJavaString(env, ifaceName);
        env->SetObjectField(jproperties, f, jifaceName);
        ElUtil::CheckErrorAndLog(env, "GetObjectField: SetObjectField : %d!\n", __LINE__);
        env->DeleteLocalRef(jifaceName);
    }

    AutoPtr<IList> addresses;
    properties->GetLinkAddresses((IList**)&addresses);
    if (addresses != NULL) {
        Int32 count = 0;
        addresses->GetSize(&count);
        if (count > 0) {
            jmethodID mAdd = env->GetMethodID(lProtiesKlass, "addLinkAddress", "(Landroid/net/LinkAddress;)Z");
            CheckErrorAndLog(env, "GetMethodID: addLinkAddress : %d!\n", __LINE__);

            jclass laddrKlass = env->FindClass("android/net/LinkAddress");
            CheckErrorAndLog(env, "FindClass: LinkAddress : %d!\n", __LINE__);

            m = env->GetMethodID(laddrKlass, "<init>", "(Ljava/net/InetAddress;III)Z");
            CheckErrorAndLog(env, "GetMethodID: appInfoKlass : %d!\n", __LINE__);

            for (Int32 i = 0; i < count; i++) {
                AutoPtr<IInterface> obj;
                addresses->Get(i, (IInterface**)&obj);
                jobject jladdr = GetJavaLinkAddress(env, ILinkAddress::Probe(obj));

                env->CallBooleanMethod(jproperties, mAdd, jladdr);
                CheckErrorAndLog(env, "CallBooleanMethod: addLinkAddress : %d!\n", __LINE__);
                env->DeleteLocalRef(jladdr);
            }

            env->DeleteLocalRef(laddrKlass);
        }
    }

    AutoPtr<IList> dnses;
    properties->GetDnsServers((IList**)&dnses);
    if (dnses != NULL) {
        Int32 count = 0;
        dnses->GetSize(&count);
        if (count > 0) {
            jmethodID mAdd = env->GetMethodID(lProtiesKlass, "addDns", "(Ljava/net/InetAddress;)Z");
            CheckErrorAndLog(env, "GetMethodID: addDns : %d!\n", __LINE__);

            for (Int32 i = 0; i < count; i++) {
                AutoPtr<IInterface> obj;
                dnses->Get(i, (IInterface**)&obj);
                AutoPtr<IInetAddress> iaddr = IInetAddress::Probe(obj);

                jobject jiaddr = GetJavaInetAddress(env, iaddr);

                env->CallBooleanMethod(jproperties, mAdd, jiaddr);
                CheckErrorAndLog(env, "CallBooleanMethod: addDns : %d!\n", __LINE__);

                env->DeleteLocalRef(jiaddr);
            }
        }
    }

    String str;
    properties->GetDomains(&str);
    SetJavaStringField(env, lProtiesKlass, jproperties, str, "mDomains", "GetJavaLinkProperties");

    Int32 mtu;
    properties->GetMtu(&mtu);
    SetJavaIntField(env, lProtiesKlass, jproperties, mtu, "mMtu", "GetJavaLinkProperties");

    properties->GetTcpBufferSizes(&str);
    SetJavaStringField(env, lProtiesKlass, jproperties, str, "mTcpBufferSizes", "GetJavaLinkProperties");

    AutoPtr<IList> routes;
    properties->GetRoutes((IList**)&routes);
    if (routes != NULL) {
        Int32 count = 0;
        routes->GetSize(&count);
        if (count > 0) {
            jmethodID mAdd = env->GetMethodID(lProtiesKlass, "addRoute", "(Landroid/net/RouteInfo;)Z");
            CheckErrorAndLog(env, "GetMethodID: addRoute : %d!\n", __LINE__);

            for (Int32 i = 0; i < count; i++) {
                AutoPtr<IInterface> obj;
                routes->Get(i, (IInterface**)&obj);
                AutoPtr<IRouteInfo> route = IRouteInfo::Probe(obj);

                jobject jroute = GetJavaRouteInfo(env, route);

                env->CallBooleanMethod(jproperties, mAdd, jroute);
                CheckErrorAndLog(env, "CallBooleanMethod: addRoute : %d!\n", __LINE__);

                env->DeleteLocalRef(jroute);
            }
        }
    }

    AutoPtr<IProxyInfo> httpProxy;
    properties->GetHttpProxy((IProxyInfo**)&httpProxy);
    if (httpProxy != NULL) {
        jobject jhttpProxy = GetJavaProxyInfo(env, httpProxy);

        jmethodID m = env->GetMethodID(lProtiesKlass, "setHttpProxy", "(Landroid/net/ProxyInfo;)V");
        CheckErrorAndLog(env, "GetMethodID: setHttpProxy : %d!\n", __LINE__);

        env->CallVoidMethod(jproperties, m, jhttpProxy);
        CheckErrorAndLog(env, "CallVoidMethod: setHttpProxy : %d!\n", __LINE__);
        env->DeleteLocalRef(jhttpProxy);
    }

    AutoPtr<IList> stackedLinks;
    properties->GetStackedLinks((IList**)&stackedLinks);
    if (stackedLinks != NULL) {
        Int32 count = 0;
        stackedLinks->GetSize(&count);
        if (count > 0) {
            jmethodID mAdd = env->GetMethodID(lProtiesKlass, "addStackedLink", "(Landroid/net/LinkProperties;)Z");
            CheckErrorAndLog(env, "GetMethodID: addStackedLink : %d!\n", __LINE__);

            for (Int32 i = 0; i < count; i++) {
                AutoPtr<IInterface> obj;
                stackedLinks->Get(i, (IInterface**)&obj);
                AutoPtr<ILinkProperties> lps = ILinkProperties::Probe(obj);

                jobject jlps = GetJavaLinkProperties(env, lps);

                env->CallBooleanMethod(jproperties, mAdd, jlps);
                CheckErrorAndLog(env, "CallBooleanMethod: addStackedLink : %d!\n", __LINE__);

                env->DeleteLocalRef(jlps);
            }
        }
    }

    env->DeleteLocalRef(lProtiesKlass);
    return jproperties;
}

jobject ElUtil::GetJavaProxyInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IProxyInfo* proxyInfo)
{
    if (env == NULL || proxyInfo == NULL){
        ALOGE("GetJavaProxyInfo() Invalid argumenet!");
        return NULL;
    }

    jclass piKlass = env->FindClass("android/net/ProxyInfo");
    CheckErrorAndLog(env, "GetJavaProxyInfo FindClass: ProxyInfo : %d!\n", __LINE__);

    String host;
    proxyInfo->GetHost(&host);
    jstring jhost = GetJavaString(env, host);

    Int32 port;
    proxyInfo->GetPort(&port);

    String exclList;
    proxyInfo->GetExclusionListAsString(&exclList);
    jstring jexclList = GetJavaString(env, exclList);

    AutoPtr<ArrayOf<String> > parsedExclusionList;
    proxyInfo->GetExclusionList((ArrayOf<String>**)&parsedExclusionList);
    jobjectArray jparsedExclusionList = GetJavaStringArray(env, parsedExclusionList);

    AutoPtr<IUri> pacFileUrl;
    proxyInfo->GetPacFileUrl((IUri**)&pacFileUrl);

    jobject jpacFileUrl = GetJavaUri(env, pacFileUrl);

    jmethodID m = env->GetMethodID(piKlass, "<init>", "(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetJavaProxyInfo GetMethodID: ProxyInfo : %d!\n", __LINE__);

    jobject jproxyInfo = env->NewObject(piKlass, m, jhost, (jint)port, jexclList, jparsedExclusionList);
    CheckErrorAndLog(env, "GetJavaProxyInfo NewObject: ProxyInfo : %d!\n", __LINE__);

    if (jpacFileUrl != NULL) {
        jfieldID f = env->GetFieldID(piKlass, "mPacFileUrl", "Landroid/net/Uri;");
        CheckErrorAndLog(env, "GetJavaProxyInfo GetFieldID: mPacFileUrl : %d!\n", __LINE__);

        env->SetObjectField(jproxyInfo, f, jpacFileUrl);
        CheckErrorAndLog(env, "GetJavaProxyInfo Fail SetObjectField: mPacFileUrl : %d!\n", __LINE__);
        env->DeleteLocalRef(jpacFileUrl);
    }

    env->DeleteLocalRef(piKlass);
    env->DeleteLocalRef(jhost);
    env->DeleteLocalRef(jexclList);
    env->DeleteLocalRef(jparsedExclusionList);
    return jproxyInfo;
}

jobject ElUtil::GetJavaRouteInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IRouteInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaRouteInfo(), invalid param.");
        return NULL;
    }

    jclass riKlass = env->FindClass("android/net/RouteInfo");
    CheckErrorAndLog(env, "FindClass: RouteInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(riKlass, "<init>", "(Landroid/net/IpPrefix;Ljava/net/InetAddress;Ljava/lang/String;I)V");
    CheckErrorAndLog(env, "GetMethodID: RouteInfo : %d!\n", __LINE__);

    AutoPtr<IIpPrefix> destination;
    info->GetDestination((IIpPrefix**)&destination);
    jobject jdestination = NULL;
    if (destination != NULL) {
        jdestination = ElUtil::GetJavaIpPrefix(env, destination);
    }

    AutoPtr<IInetAddress> gateway;
    info->GetGateway((IInetAddress**)&gateway);
    jobject jgateway = NULL;
    if (gateway != NULL) {
        jgateway = ElUtil::GetJavaInetAddress(env, gateway);
    }

    String iface;
    info->GetInterface(&iface);
    jstring jiface = ElUtil::GetJavaString(env, iface);

    Int32 type;
    info->GetType(&type);

    jobject jinfo = env->NewObject(riKlass, m, jdestination, jgateway, jiface, (jint)type);
    CheckErrorAndLog(env, "NewObject: RouteInfo : %d!\n", __LINE__);

    env->DeleteLocalRef(riKlass);
    env->DeleteLocalRef(jiface);
    return jinfo;
}

jobject ElUtil::GetJavaIpPrefix(
    /* [in] */ JNIEnv* env,
    /* [in] */ IIpPrefix* ipPrefix)
{
    if (env == NULL || ipPrefix == NULL) {
        ALOGE("GetJavaIpPrefix() Invalid argumenet!");
        return NULL;
    }

    jclass ipKlass = env->FindClass("android/net/IpPrefix");
    CheckErrorAndLog(env, "GetJavaIpPrefix FindClass: IpPrefix : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(ipKlass, "<init>", "([BI)V");
    CheckErrorAndLog(env, "GetJavaIpPrefix GetMethodID: IpPrefix : %d!\n", __LINE__);

    AutoPtr<ArrayOf<Byte> > address;
    ipPrefix->GetRawAddress((ArrayOf<Byte>**)&address);
    jbyteArray jaddress = NULL;
    if (address != NULL) {
        jaddress = GetJavaByteArray(env, address);
    }

    Int32 prefixLength = 0;
    ipPrefix->GetPrefixLength(&prefixLength);

    jobject jipPrefix = env->NewObject(ipKlass, m, jaddress, (jint)prefixLength);
    CheckErrorAndLog(env, "GetJavaIpPrefix NewObject: IpPrefix : %d!\n", __LINE__);

    env->DeleteLocalRef(ipKlass);
    env->DeleteLocalRef(jaddress);
    return jipPrefix;
}

jobject ElUtil::GetJavaProviderProperties(
    /* [in] */ JNIEnv* env,
    /* [in] */ IProviderProperties* properties)
{
    if (env == NULL || properties == NULL) {
        ALOGE("GetJavaProviderProperties(), invalid param.");
        return NULL;
    }

    Boolean bValue = FALSE;
    Int32 intValue = 0;
    jboolean requiresNetwork;
    jboolean requiresSatellite;
    jboolean requiresCell;
    jboolean hasMonetaryCost;
    jboolean supportsAltitude;
    jboolean supportsSpeed;
    jboolean supportsBearing;
    jint powerRequirement;
    jint accuracy;

    properties->GetRequiresNetwork(&bValue);
    requiresNetwork = bValue;
    properties->GetRequiresSatellite(&bValue);
    requiresSatellite = bValue;
    properties->GetRequiresCell(&bValue);
    requiresCell = bValue;
    properties->GetHasMonetaryCost(&bValue);
    hasMonetaryCost = bValue;
    properties->GetSupportsAltitude(&bValue);
    supportsAltitude = bValue;
    properties->GetSupportsSpeed(&bValue);
    supportsSpeed = bValue;
    properties->GetSupportsBearing(&bValue);
    supportsBearing = bValue;
    properties->GetPowerRequirement(&intValue);
    powerRequirement = intValue;
    properties->GetAccuracy(&intValue);
    accuracy = intValue;

    jclass ppKlass = env->FindClass("com/android/internal/location/ProviderProperties");
    CheckErrorAndLog(env, "GetJavaProviderProperties Error FindClass: ProviderProperties : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(ppKlass, "<init>", "(ZZZZZZZII)V");
    CheckErrorAndLog(env, "GetJavaProviderProperties GetMethodID: ProviderProperties : %d!\n", __LINE__);

    jobject jproviderProperties = env->NewObject(ppKlass, m, requiresNetwork, requiresSatellite, requiresCell, hasMonetaryCost,
            supportsAltitude, supportsSpeed, supportsBearing, powerRequirement, accuracy);
    CheckErrorAndLog(env, "GetJavaProviderProperties NewObject: ProviderProperties : %d!\n", __LINE__);

    env->DeleteLocalRef(ppKlass);
    return jproviderProperties;
}

bool ElUtil::ToElProviderProperties(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jproperties,
    /* [in] */ IProviderProperties** properties)
{
    if (env == NULL || jproperties == NULL || properties == NULL) {
        ALOGE("ToElProviderProperties() invalid param!");
        return false;
    }

    jclass klass = env->FindClass("com/android/internal/location/ProviderProperties");
    CheckErrorAndLog(env, "ToElProviderProperties Error FindClass: LocationRequest : %d!\n", __LINE__);

    Boolean mRequiresNetwork = (Boolean)GetJavaBoolField(env, klass, jproperties, "mRequiresNetwork", "ToElProviderProperties");
    Boolean mRequiresSatellite = (Boolean)GetJavaBoolField(env, klass, jproperties, "mRequiresSatellite", "ToElProviderProperties");
    Boolean mRequiresCell = (Boolean)GetJavaBoolField(env, klass, jproperties, "mRequiresCell", "ToElProviderProperties");
    Boolean mHasMonetaryCost = (Boolean)GetJavaBoolField(env, klass, jproperties, "mHasMonetaryCost", "ToElProviderProperties");
    Boolean mSupportsAltitude = (Boolean)GetJavaBoolField(env, klass, jproperties, "mSupportsAltitude", "ToElProviderProperties");
    Boolean mSupportsSpeed = (Boolean)GetJavaBoolField(env, klass, jproperties, "mSupportsSpeed", "ToElProviderProperties");
    Boolean mSupportsBearing = (Boolean)GetJavaBoolField(env, klass, jproperties, "mSupportsBearing", "ToElProviderProperties");
    Int32 mPowerRequirement = (Int32)GetJavaIntField(env, klass, jproperties, "mPowerRequirement", "ToElProviderProperties");
    Int32 mAccuracy = (Int32)GetJavaIntField(env, klass, jproperties, "mAccuracy", "ToElProviderProperties");

    CProviderProperties::New(mRequiresNetwork, mRequiresSatellite, mRequiresCell, mHasMonetaryCost, mSupportsAltitude, mSupportsSpeed,
        mSupportsBearing, mPowerRequirement, mAccuracy, properties);

    env->DeleteLocalRef(klass);

    return true;
}

bool ElUtil::ToElLocationRequest(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jrequest,
    /* [out] */ ILocationRequest** locationRequest)
{
    if (jrequest ==  NULL) {
        ALOGE("ToElLocationRequest() invalid param: jrequest");
        *locationRequest = NULL;
        return false;
    }

    CLocationRequest::New(locationRequest);

    jclass lrKlass = env->FindClass("android/location/LocationRequest");
    CheckErrorAndLog(env, "ToElLocationRequest Error FindClass: LocationRequest : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(lrKlass, "<init>", "()V");
    CheckErrorAndLog(env, "ToElLocationRequest GetMethodID: LocationRequest : %d!\n", __LINE__);

    Int32 quality = GetJavaIntField(env, lrKlass, jrequest, "mQuality", "ToElLocationRequest");
    Int64 interval = GetJavalongField(env, lrKlass, jrequest, "mInterval", "ToElLocationRequest");
    Int64 fastestInterval = GetJavalongField(env, lrKlass, jrequest, "mFastestInterval", "ToElLocationRequest");
    Int64 expireAt = GetJavalongField(env, lrKlass, jrequest, "mExpireAt", "ToElLocationRequest");
    Int32 numUpdates = GetJavaIntField(env, lrKlass, jrequest, "mNumUpdates", "ToElLocationRequest");
    Float smallestDisplacement = GetJavafloatField(env, lrKlass, jrequest, "mSmallestDisplacement", "ToElLocationRequest");
    Boolean hideFromAppOps = GetJavaBoolField(env, lrKlass, jrequest, "mHideFromAppOps", "ToElLocationRequest");
    String provider = GetJavaStringField(env, lrKlass, jrequest, "mProvider", "ToElLocationRequest");

    (*locationRequest)->SetQuality(quality);
    (*locationRequest)->SetInterval(interval);
    (*locationRequest)->SetFastestInterval(fastestInterval);
    (*locationRequest)->SetExpireAt(expireAt);
    (*locationRequest)->SetNumUpdates(numUpdates);
    (*locationRequest)->SetSmallestDisplacement(smallestDisplacement);
    (*locationRequest)->SetHideFromAppOps(hideFromAppOps);
    (*locationRequest)->SetProvider(provider);

    jfieldID f = env->GetFieldID(lrKlass, "mWorkSource", "Landroid/os/WorkSource;");
    ElUtil::CheckErrorAndLog(env, "ToElLocationRequest GetFieldID: mWorkSource", __LINE__);

    jobject jworkSource = env->GetObjectField(jrequest, f);
    ElUtil::CheckErrorAndLog(env, "ToElLocationRequest GetObjectField: mWorkSource", __LINE__);

    if (jworkSource != NULL) {
        AutoPtr<IWorkSource> workSource;
        if (ToElWorkSource(env, jworkSource, (IWorkSource**)&workSource)) {
            (*locationRequest)->SetWorkSource(workSource);
        }
        env->DeleteLocalRef(jworkSource);
    }

    env->DeleteLocalRef(lrKlass);

    return true;
}

jobject ElUtil::GetJavaLocation(
    /* [in] */ JNIEnv* env,
    /* [in] */ ILocation* location)
{
    if (env == NULL || location == NULL) {
        ALOGD("GetJavaLocation Invalid arguments!");
        return NULL;
    }

    jclass locationKlass = env->FindClass("android/location/Location");
    CheckErrorAndLog(env, "GetJavaLocation Error FindClass: Location : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(locationKlass, "<init>", "(Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetJavaLocation GetMethodID: Location() : %d!\n", __LINE__);

    String mProvider("");
    Int64 mTime = 0;
    Int64 mElapsedRealtimeNanos = 0;
    Double mLatitude = 0.0;
    Double mLongitude = 0.0;
    Boolean mHasAltitude = FALSE;
    Double mAltitude = 0.0f;
    Boolean mHasSpeed = FALSE;
    Float mSpeed = 0.0f;
    Boolean mHasBearing = FALSE;
    Float mBearing = 0.0f;
    Boolean mHasAccuracy = FALSE;
    Float mAccuracy = 0.0f;
    AutoPtr<IBundle> mExtras;
    Boolean mIsFromMockProvider = FALSE;

    location->GetProvider(&mProvider);
    location->GetTime(&mTime);
    location->GetElapsedRealtimeNanos(&mElapsedRealtimeNanos);
    location->GetLatitude(&mLatitude);
    location->GetLongitude(&mLongitude);
    location->HasAltitude(&mHasAltitude);
    location->GetAltitude(&mAltitude);
    location->HasSpeed(&mHasSpeed);
    location->GetSpeed(&mSpeed);
    location->HasBearing(&mHasBearing);
    location->GetBearing(&mBearing);
    location->HasAccuracy(&mHasAccuracy);
    location->GetAccuracy(&mAccuracy);
    location->GetExtras((IBundle**)&mExtras);
    location->IsFromMockProvider(&mIsFromMockProvider);

    jstring jprovider = GetJavaString(env, mProvider);

    jobject jlocation = env->NewObject(locationKlass, m, jprovider);
    env->DeleteLocalRef(jprovider);

    SetJavalongField(env, locationKlass, jlocation, mTime, "mTime", "GetJavaLocation");
    SetJavalongField(env, locationKlass, jlocation, mElapsedRealtimeNanos, "mElapsedRealtimeNanos", "GetJavaLocation");
    SetJavadoubleField(env, locationKlass, jlocation, mLatitude, "mLatitude", "GetJavaLocation");
    SetJavadoubleField(env, locationKlass, jlocation, mLongitude, "mLongitude", "GetJavaLocation");
    SetJavaBoolField(env, locationKlass, jlocation, mHasAltitude, "mHasAltitude", "GetJavaLocation");
    SetJavadoubleField(env, locationKlass, jlocation, mAltitude, "mAltitude", "GetJavaLocation");
    SetJavaBoolField(env, locationKlass, jlocation, mHasSpeed, "mHasSpeed", "GetJavaLocation");
    SetJavafloatField(env, locationKlass, jlocation, mSpeed, "mSpeed", "GetJavaLocation");
    SetJavaBoolField(env, locationKlass, jlocation, mHasBearing, "mHasBearing", "GetJavaLocation");
    SetJavafloatField(env, locationKlass, jlocation, mBearing, "mBearing", "GetJavaLocation");
    SetJavaBoolField(env, locationKlass, jlocation, mHasAccuracy, "mHasAccuracy", "GetJavaLocation");
    SetJavafloatField(env, locationKlass, jlocation, mAccuracy, "mAccuracy", "GetJavaLocation");
    SetJavaBoolField(env, locationKlass, jlocation, mIsFromMockProvider, "mIsFromMockProvider", "GetJavaLocation");

    jobject jextras = GetJavaBundle(env, mExtras);
    if (jextras != NULL) {
        jfieldID f = env->GetFieldID(locationKlass, "mExtras", "Landroid/os/Bundle;");
        CheckErrorAndLog(env, "GetJavaLocation Fail GetFieldID: mExtras : %d!\n", __LINE__);

        env->SetObjectField(jlocation, f, jextras);
        CheckErrorAndLog(env, "GetJavaLocation Fail SetObjectField: jextras : %d!\n", __LINE__);

        env->DeleteLocalRef(jextras);
    }
    else {
        ALOGD("GetJavaLocation Error: jextras is NULL!");
    }

    env->DeleteLocalRef(locationKlass);
    return jlocation;
}

bool ElUtil::ToElLocation(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jlocation,
    /* [out] */ ILocation** location)
{
    if (location == NULL) {
        ALOGE("ToElLocation(), location param is null");
        return false;
    }

    *location = NULL;

    if (jlocation == NULL) {
        ALOGE("ToElLocation(), jlocation is null");
        return false;
    }

    jclass locationKlass = env->FindClass("android/location/Location");
    CheckErrorAndLog(env, "GetJavaLocation Error FindClass: Location : %d!\n", __LINE__);

    String provider = ElUtil::GetJavaStringField(env, locationKlass, jlocation, "mProvider", "ToElLocation");

    CLocation::New(provider, location);

    if (*location == NULL) {
        ALOGE("ToElLocation(), create ILocation return null!");
    }
    else{
        (*location)->SetTime((Int64)ElUtil::GetJavalongField(env, locationKlass, jlocation, "mTime", "ToElLocation"));
        (*location)->SetElapsedRealtimeNanos((Int64)ElUtil::GetJavalongField(env, locationKlass, jlocation, "mElapsedRealtimeNanos", "ToElLocation"));

        (*location)->SetLatitude((Double)ElUtil::GetJavadoubleField(env, locationKlass, jlocation, "mLatitude", "ToElLocation"));
        (*location)->SetLongitude((Double)ElUtil::GetJavadoubleField(env, locationKlass, jlocation, "mLongitude", "ToElLocation"));

        if (ElUtil::GetJavaBoolField(env, locationKlass, jlocation, "mHasAltitude", "ToElLocation")) {
            (*location)->SetAltitude(ElUtil::GetJavadoubleField(env, locationKlass, jlocation, "mAltitude", "ToElLocation"));
        }

        if (ElUtil::GetJavaBoolField(env, locationKlass, jlocation, "mHasSpeed", "ToElLocation")) {
            (*location)->SetSpeed(ElUtil::GetJavafloatField(env, locationKlass, jlocation, "mSpeed", "ToElLocation"));
        }

        if (ElUtil::GetJavaBoolField(env, locationKlass, jlocation, "mHasBearing", "ToElLocation")) {
            (*location)->SetBearing(ElUtil::GetJavafloatField(env, locationKlass, jlocation, "mBearing", "ToElLocation"));
        }

        if (ElUtil::GetJavaBoolField(env, locationKlass, jlocation, "mHasAccuracy", "ToElLocation")) {
            (*location)->SetAccuracy(ElUtil::GetJavafloatField(env, locationKlass, jlocation, "mAccuracy", "ToElLocation"));
        }

        (*location)->SetIsFromMockProvider(ElUtil::GetJavaBoolField(env, locationKlass, jlocation, "mIsFromMockProvider", "ToElLocation"));

        AutoPtr<IBundle> extras;

        jfieldID f = env->GetFieldID(locationKlass, "mExtras", "Landroid/os/Bundle;");
        CheckErrorAndLog(env, "GetJavaLocation Fail GetFieldID: mExtras : %d!\n", __LINE__);
        jobject jExtras = env->GetObjectField(jlocation, f);
        CheckErrorAndLog(env, "GetJavaLocation Fail SetObjectField: jextras : %d!\n", __LINE__);

        ToElBundle(env, jExtras, (IBundle**)&extras);
        (*location)->SetExtras(extras);
        env->DeleteLocalRef(jExtras);
    }

    env->DeleteLocalRef(locationKlass);

    return true;
}

bool ElUtil::ToElGeofence(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jGeofence,
    /* [out] */ IGeofence** geofence)
{
    if (geofence == NULL) {
        ALOGE("ToElGeofence(), geofence param is null");
        return false;
    }

    *geofence = NULL;

    if (jGeofence == NULL) {
        ALOGE("ToElGeofence(), jGeofence is null");
        return false;
    }

    jclass geofenceKlass = env->FindClass("android/location/Geofence");
    CheckErrorAndLog(env, "GetJavaLocation Error FindClass: Geofence : %d!\n", __LINE__);

    Double latitude = ElUtil::GetJavadoubleField(env, geofenceKlass, jGeofence, "mLatitude", "ToElGeofence");
    Double longitude = ElUtil::GetJavadoubleField(env, geofenceKlass, jGeofence, "mLongitude", "ToElGeofence");
    Float radius = ElUtil::GetJavafloatField(env, geofenceKlass, jGeofence, "mRadius", "ToElGeofence");

    AutoPtr<IGeofenceHelper> gHelper;
    CGeofenceHelper::AcquireSingleton((IGeofenceHelper**)&gHelper);
    gHelper->CreateCircle(latitude, longitude, radius, geofence);

    env->DeleteLocalRef(geofenceKlass);

    return true;
}

bool ElUtil::ToElCriteria(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jCriteria,
    /* [out] */ ICriteria** criteria)
{
    if (criteria == NULL) {
        ALOGE("ToElGeofence(), criteria param is null");
        return false;
    }

    *criteria = NULL;

    if (jCriteria == NULL) {
        ALOGE("ToElGeofence(), jCriteria is null");
        return false;
    }

    jclass criteriaKlass = env->FindClass("android/location/Criteria");
    CheckErrorAndLog(env, "GetJavaLocation Error FindClass: Criteria : %d!\n", __LINE__);

    Int32 mHorizontalAccuracy = ElUtil::GetJavaIntField(env, criteriaKlass, jCriteria, "mHorizontalAccuracy", "ToElCriteria");
    Int32 mVerticalAccuracy = ElUtil::GetJavaIntField(env, criteriaKlass, jCriteria, "mVerticalAccuracy", "ToElCriteria");
    Int32 mSpeedAccuracy = ElUtil::GetJavaIntField(env, criteriaKlass, jCriteria, "mSpeedAccuracy", "ToElCriteria");
    Int32 mBearingAccuracy = ElUtil::GetJavaIntField(env, criteriaKlass, jCriteria, "mBearingAccuracy", "ToElCriteria");
    Int32 mPowerRequirement = ElUtil::GetJavaIntField(env, criteriaKlass, jCriteria, "mPowerRequirement", "ToElCriteria");
    Boolean mAltitudeRequired = ElUtil::GetJavaBoolField(env, criteriaKlass, jCriteria, "mAltitudeRequired", "ToElCriteria");
    Boolean mBearingRequired = ElUtil::GetJavaBoolField(env, criteriaKlass, jCriteria, "mBearingRequired", "ToElCriteria");
    Boolean mSpeedRequired = ElUtil::GetJavaBoolField(env, criteriaKlass, jCriteria, "mSpeedRequired", "ToElCriteria");
    Boolean mCostAllowed = ElUtil::GetJavaBoolField(env, criteriaKlass, jCriteria, "mCostAllowed", "ToElCriteria");

    env->DeleteLocalRef(criteriaKlass);

    CCriteria::New(criteria);

    (*criteria)->SetHorizontalAccuracy(mHorizontalAccuracy);
    (*criteria)->SetVerticalAccuracy(mVerticalAccuracy);
    (*criteria)->SetSpeedAccuracy(mSpeedAccuracy);
    (*criteria)->SetBearingAccuracy(mBearingAccuracy);
    (*criteria)->SetPowerRequirement(mPowerRequirement);
    (*criteria)->SetAltitudeRequired(mAltitudeRequired);
    (*criteria)->SetBearingRequired(mBearingRequired);
    (*criteria)->SetSpeedRequired(mSpeedRequired);
    (*criteria)->SetCostAllowed(mCostAllowed);

    return true;
}

bool ElUtil::ToElGeocoderParams(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jgeocoderParams,
    /* [out] */ IGeocoderParams** params)
{
    if (env == NULL || jgeocoderParams == NULL || params == NULL) {
        ALOGE("ToElGeocoderParams Invalid arguments!");
        return false;
    }

    jclass klass = env->FindClass("android/location/GeocoderParams");
    CheckErrorAndLog(env, "ToElGeocoderParams Fail find class: GeocoderParams : %d!\n", __LINE__);
    jfieldID f = env->GetFieldID(klass, "mLocale", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "ToElGeocoderParams Fail GetFieldID: mLocale : %d!\n", __LINE__);
    jobject jlocale = env->GetObjectField(jgeocoderParams, f);
    CheckErrorAndLog(env, "ToElGeocoderParams Fail GetObjectField: mLocale : %d!\n", __LINE__);
    jclass localKlass = env->FindClass("java/util/Locale");
    CheckErrorAndLog(env, "ToElGeocoderParams Fail FindClass: Locale : %d!\n", __LINE__);

    String countryCode = GetJavaStringField(env, localKlass, jlocale, "countryCode", "ToElGeocoderParams");
    String languageCode = GetJavaStringField(env, localKlass, jlocale, "languageCode", "ToElGeocoderParams");
    String variantCode = GetJavaStringField(env, localKlass, jlocale, "variantCode", "ToElGeocoderParams");
    String packageName = GetJavaStringField(env, klass, jgeocoderParams, "mPackageName", "ToElGeocoderParams");
    env->DeleteLocalRef(klass);
    env->DeleteLocalRef(localKlass);

    AutoPtr<IParcel> parcel;
    CParcel::New((IParcel**)&parcel);

    parcel->WriteString(languageCode);
    parcel->WriteString(countryCode);
    parcel->WriteString(variantCode);
    parcel->WriteString(packageName);
    parcel->SetDataPosition(0);

    CGeocoderParams::New(params);
    AutoPtr<IParcelable> paramsParcel = IParcelable::Probe(*params);


    if (paramsParcel != NULL) {
        paramsParcel->ReadFromParcel(parcel);
    }
    else {
        ALOGE("Get IParcelable ptr from IGeocoderParams failed!");
    }

    return true;
}

bool ElUtil::ToElAddress(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jaddress,
    /* [in] */ IAddress** address)
{
    if (env == NULL || jaddress == NULL || address == NULL) {
        ALOGE("ToElAddress Invalid arguments!");
        return false;
    }

    jclass klass = env->FindClass("android/location/Address");
    CheckErrorAndLog(env, "ToElAddress Fail find class: Address : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(klass, "mLocale", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "ToElAddress Fail GetFieldID: mLocale : %d!\n", __LINE__);
    jobject jlocale = env->GetObjectField(jaddress, f);
    CheckErrorAndLog(env, "ToElAddress Fail GetObjectField: mLocale : %d!\n", __LINE__);
    jclass localKlass = env->FindClass("java/util/Locale");
    CheckErrorAndLog(env, "ToElAddress Fail FindClass: Locale : %d!\n", __LINE__);

    String countryCode = GetJavaStringField(env, localKlass, jlocale, "countryCode", "ToElAddress");
    String languageCode = GetJavaStringField(env, localKlass, jlocale, "languageCode", "ToElAddress");

    env->DeleteLocalRef(jlocale);
    env->DeleteLocalRef(localKlass);

    AutoPtr<ILocale> locale;
    !countryCode.IsNullOrEmpty() ? CLocale::New(languageCode, countryCode, (ILocale**)&locale) : CLocale::New(languageCode, (ILocale**)&locale);

    CAddress::New(locale, address);

    if (*address == NULL) {
        ALOGE("Create CAddress object failed!");
        return false;
    }

    // Translate HashMap: mAddressLines
    jfieldID fmAddressLines = env->GetFieldID(klass, "mAddressLines", "Ljava/util/HashMap;");
    CheckErrorAndLog(env, "ToElAddress Fail GetFieldID: mAddressLines : %d!\n", __LINE__);
    jobject jmAddressLines = env->GetObjectField(jaddress, fmAddressLines);
    CheckErrorAndLog(env, "ToElAddress Fail GetObjectField: mAddressLines : %d!\n", __LINE__);

    if (jmAddressLines != NULL) {
        jclass mapKlass = env->FindClass("java/util/HashMap");
        CheckErrorAndLog(env, "FindClass: HashMap : %d!", __LINE__);

        jmethodID m = env->GetMethodID(mapKlass, "keySet", "()Ljava/util/Set;");
        CheckErrorAndLog(env, "GetMethodID: keySet : %d!", __LINE__);

        jobject jKeySet = env->CallObjectMethod(jmAddressLines, m);
        CheckErrorAndLog(env, "CallObjectMethod: jKeySet : %d!", __LINE__);

        if (jKeySet != NULL) {
            jclass setKlass = env->FindClass("java/util/Set");
            CheckErrorAndLog(env, "FindClass: java/util/Set : %d!\n", __LINE__);

            m = env->GetMethodID(setKlass, "iterator", "()Ljava/util/Iterator;");
            CheckErrorAndLog(env, "GetMethodID: iterator ()Ljava/util/Iterator; : %d!\n", __LINE__);

            env->DeleteLocalRef(setKlass);

            jobject jIterator = env->CallObjectMethod(jKeySet, m);
            CheckErrorAndLog(env, "CallObjectMethod: jIterator : %d!\n", __LINE__);

            if (jIterator != NULL) {
                jclass iteratorKlass = env->FindClass("java/util/Iterator");
                CheckErrorAndLog(env, "FindClass: java/util/Iterator : %d!\n", __LINE__);

                jmethodID jHasNext = env->GetMethodID(iteratorKlass, "hasNext", "()Z");
                CheckErrorAndLog(env, "GetMethodID: hasNext : %d!\n", __LINE__);

                jmethodID jNext = env->GetMethodID(iteratorKlass, "next", "()Ljava/lang/Object;");
                CheckErrorAndLog(env, "GetMethodID: next : %d!\n", __LINE__);

                env->DeleteLocalRef(iteratorKlass);

                jboolean hasNext = env->CallBooleanMethod(jIterator, jHasNext);
                while (hasNext) {
                    jobject jKey = env->CallObjectMethod(jIterator, jNext);

                    jclass jintegerCls = env->FindClass("java/lang/Integer");
                    CheckErrorAndLog(env, "ToElAddress(): FindClass: Integer : %d!\n", __LINE__);
                    jmethodID toIntMid = env->GetMethodID(jintegerCls, "intValue", "()I");
                    CheckErrorAndLog(env, "ToElAddress(): FindMethod: toValue: %d!\n", __LINE__);
                    jint jkeyValue = env->CallIntMethod(jKey, toIntMid);
                    CheckErrorAndLog(env, "ToElAddress: call method toValue: %d!\n", __LINE__);
                    env->DeleteLocalRef(jintegerCls);

                    m = env->GetMethodID(mapKlass, "get", "(Ljava/lang/Object;)Ljava/lang/Object;");
                    CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

                    jobject jValue = env->CallObjectMethod(jmAddressLines, m, jKey);
                    CheckErrorAndLog(env, "CallObjectMethod: jValue : %d!\n", __LINE__);

                    if (jValue != NULL) {
                        String value = ElUtil::ToElString(env, (jstring)jValue);
                        (*address)->SetAddressLine(jkeyValue, value);
                        env->DeleteLocalRef(jValue);
                    }

                    env->DeleteLocalRef(jKey);

                    hasNext = env->CallBooleanMethod(jIterator, jHasNext);
                }
                env->DeleteLocalRef(jIterator);
            }
            env->DeleteLocalRef(jKeySet);
        }

        env->DeleteLocalRef(mapKlass);
    }

    String mFeatureName = GetJavaStringField(env, klass, jaddress, "mFeatureName", "ToElAddress");
    (*address)->SetFeatureName(mFeatureName);
    String mAdminArea = GetJavaStringField(env, klass, jaddress, "mAdminArea", "ToElAddress");
    (*address)->SetAdminArea(mAdminArea);
    String mSubAdminArea = GetJavaStringField(env, klass, jaddress, "mSubAdminArea", "ToElAddress");
    (*address)->SetSubAdminArea(mSubAdminArea);
    String mLocality = GetJavaStringField(env, klass, jaddress, "mLocality", "ToElAddress");
    (*address)->SetLocality(mLocality);
    String mSubLocality = GetJavaStringField(env, klass, jaddress, "mSubLocality", "ToElAddress");
    (*address)->SetSubLocality(mSubLocality);
    String mThoroughfare = GetJavaStringField(env, klass, jaddress, "mThoroughfare", "ToElAddress");
    (*address)->SetThoroughfare(mThoroughfare);
    String mSubThoroughfare = GetJavaStringField(env, klass, jaddress, "mSubThoroughfare", "ToElAddress");
    (*address)->SetSubThoroughfare(mSubThoroughfare);
    String mPremises = GetJavaStringField(env, klass, jaddress, "mPremises", "ToElAddress");
    (*address)->SetPremises(mPremises);
    String mPostalCode = GetJavaStringField(env, klass, jaddress, "mPostalCode", "ToElAddress");
    (*address)->SetPostalCode(mPostalCode);
    String mCountryCode = GetJavaStringField(env, klass, jaddress, "mCountryCode", "ToElAddress");
    (*address)->SetCountryCode(mCountryCode);
    String mCountryName = GetJavaStringField(env, klass, jaddress, "mCountryName", "ToElAddress");
    (*address)->SetCountryName(mCountryName);
    String mPhone = GetJavaStringField(env, klass, jaddress, "mPhone", "ToElAddress");
    (*address)->SetPhone(mPhone);
    String mUrl = GetJavaStringField(env, klass, jaddress, "mUrl", "ToElAddress");
    (*address)->SetUrl(mUrl);

    jboolean mHasLatitude = GetJavaBoolField(env, klass, jaddress, "mHasLatitude", "ToElAddress");
    if (mHasLatitude) {
        jdouble latitude = GetJavadoubleField(env, klass, jaddress, "mLatitude", "ToElAddress");
        (*address)->SetLatitude((Double)latitude);
    }

    jboolean mHasLongitude = GetJavaBoolField(env, klass, jaddress, "mHasLongitude", "ToElAddress");
    if (mHasLatitude) {
        jdouble longitude = GetJavadoubleField(env, klass, jaddress, "mLongitude", "ToElAddress");
        (*address)->SetLongitude((Double)longitude);
    }

    f = env->GetFieldID(klass, "mExtras", "Landroid/os/Bundle;");
    CheckErrorAndLog(env, "GetFieldID: mExtras Landroid/os/Bundle; : %d!\n", __LINE__);

    jobject jBundle = env->GetObjectField(jaddress, f);
    CheckErrorAndLog(env, "GetObjectField: jintent : %d!\n", __LINE__);
    if (jBundle != NULL) {
        AutoPtr<IBundle> extras;
        ToElBundle(env, jBundle, (IBundle**)&extras);
        (*address)->SetExtras(extras);
        env->DeleteLocalRef(jBundle);
    }

    env->DeleteLocalRef(klass);

    return true;
}

jobject ElUtil::GetJavaWifiConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiConfiguration* config)
{
    if (env == NULL || config == NULL) {
        ALOGE("GetJavaWifiConfiguration Invalid arguments!");
        return NULL;
    }

    jclass cfgKlass = env->FindClass("android/net/wifi/WifiConfiguration");
    CheckErrorAndLog(env, "GetJavaWifiConfiguration FindClass: WifiConfiguration : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(cfgKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaWifiConfiguration GetMethodID: WifiConfiguration() : %d!\n", __LINE__);

    jobject jconfig = env->NewObject(cfgKlass, m);
    CheckErrorAndLog(env, "GetJavaWifiConfiguration GetMethodID: WifiConfiguration() : %d!\n", __LINE__);

    Int32 tempInt;
    config->GetNetworkId(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "networkId", "GetJavaWifiConfiguration");

    config->GetStatus(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "status", "GetJavaWifiConfiguration");

    config->GetDisableReason(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "disableReason", "GetJavaWifiConfiguration");

    String tempString;
    config->GetSSID(&tempString);
    ElUtil::SetJavaStringField(env, cfgKlass, jconfig, tempString, "SSID", "GetJavaWifiConfiguration");

    config->GetBSSID(&tempString);
    ElUtil::SetJavaStringField(env, cfgKlass, jconfig, tempString, "BSSID", "GetJavaWifiConfiguration");

    String autoJoinBSSID;
    config->GetAutoJoinBSSID(&autoJoinBSSID);
    ElUtil::SetJavaStringField(env, cfgKlass, jconfig, autoJoinBSSID, "autoJoinBSSID", "GetJavaWifiConfiguration");

    String FQDN;
    config->GetFQDN(&FQDN);
    ElUtil::SetJavaStringField(env, cfgKlass, jconfig, FQDN, "FQDN", "GetJavaWifiConfiguration");

    String naiRealm;
    config->GetNaiRealm(&naiRealm);
    ElUtil::SetJavaStringField(env, cfgKlass, jconfig, naiRealm, "naiRealm", "GetJavaWifiConfiguration");

    config->GetPreSharedKey(&tempString);
    ElUtil::SetJavaStringField(env, cfgKlass, jconfig, tempString, "preSharedKey", "GetJavaWifiConfiguration");

    AutoPtr<ArrayOf<String> > wepKeys;
    config->GetWepKeys((ArrayOf<String>**)&wepKeys);
    if (wepKeys != NULL) {
        jobjectArray jwepKeys = ElUtil::GetJavaStringArray(env, wepKeys);

        if (jwepKeys != NULL) {
            jfieldID f = env->GetFieldID(cfgKlass, "wepKeys", "[Ljava/lang/String;");
            CheckErrorAndLog(env, "GetFieldID: wepKeys : %d!\n", __LINE__);

            env->SetObjectField(jconfig, f, jwepKeys);
            CheckErrorAndLog(env, "SetObjectField: wepKeys : %d!\n", __LINE__);

            env->DeleteLocalRef(jwepKeys);
        }
        else {
            ALOGE("GetJavaWifiConfiguration: jwepKeys is NULL!");
        }
    }

    config->GetWepTxKeyIndex(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "wepTxKeyIndex", "GetJavaWifiConfiguration");

    config->GetPriority(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "priority", "GetJavaWifiConfiguration");

    Boolean tempBool;
    config->GetHiddenSSID(&tempBool);
    ElUtil::SetJavaBoolField(env, cfgKlass, jconfig, tempBool, "hiddenSSID", "GetJavaWifiConfiguration");

    config->GetIsIBSS(&tempBool);
    ElUtil::SetJavaBoolField(env, cfgKlass, jconfig, tempBool, "isIBSS", "GetJavaWifiConfiguration");

    config->GetFrequency(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "frequency", "GetJavaWifiConfiguration");

    config->GetRequirePMF(&tempBool);
    ElUtil::SetJavaBoolField(env, cfgKlass, jconfig, tempBool, "requirePMF", "GetJavaWifiConfiguration");

    String updateIdentifier;
    config->GetUpdateIdentifier(&updateIdentifier);
    SetJavaStringField(env, cfgKlass, jconfig, updateIdentifier, "updateIdentifier", "GetJavaWifiConfiguration");

    AutoPtr<IBitSet> allowedKeyManagement;
    config->GetAllowedKeyManagement((IBitSet**)&allowedKeyManagement);
    if (allowedKeyManagement != NULL) {
        jobject jallowedKeyManagement = ElUtil::GetJavaBitSet(env, allowedKeyManagement);
        if (jallowedKeyManagement != NULL) {
            jfieldID f = env->GetFieldID(cfgKlass, "allowedKeyManagement", "Ljava/util/BitSet;");
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail GetFieldID: allowedKeyManagement : %d!\n", __LINE__);

            env->SetObjectField(jconfig, f, jallowedKeyManagement);
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail SetObjectField: jallowedKeyManagement : %d!\n", __LINE__);

            env->DeleteLocalRef(jallowedKeyManagement);
        }
        else {
            ALOGE("GetJavaWifiConfiguration Error: jallowedKeyManagement is NULL!");
        }
    }

    AutoPtr<IBitSet> allowedProtocols;
    config->GetAllowedProtocols((IBitSet**)&allowedProtocols);
    if (allowedProtocols != NULL) {
        jobject jallowedProtocols = ElUtil::GetJavaBitSet(env, allowedProtocols);
        if (jallowedProtocols != NULL) {
            jfieldID f = env->GetFieldID(cfgKlass, "allowedProtocols", "Ljava/util/BitSet;");
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail GetFieldID: allowedProtocols : %d!\n", __LINE__);

            env->SetObjectField(jconfig, f, jallowedProtocols);
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail SetObjectField: allowedProtocols : %d!\n", __LINE__);

            env->DeleteLocalRef(jallowedProtocols);
        }
        else {
            ALOGE("GetJavaWifiConfiguration Error: jallowedProtocols is NULL!");
        }
    }

    AutoPtr<IBitSet> allowedAuthAlgorithms;
    config->GetAllowedAuthAlgorithms((IBitSet**)&allowedAuthAlgorithms);
    if (allowedAuthAlgorithms != NULL) {
        jobject jallowedAuthAlgorithms = ElUtil::GetJavaBitSet(env, allowedAuthAlgorithms);
        if (jallowedAuthAlgorithms != NULL) {
            jfieldID f = env->GetFieldID(cfgKlass, "allowedAuthAlgorithms", "Ljava/util/BitSet;");
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail GetFieldID: allowedAuthAlgorithms : %d!\n", __LINE__);

            env->SetObjectField(jconfig, f, jallowedAuthAlgorithms);
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail SetObjectField: allowedAuthAlgorithms : %d!\n", __LINE__);

            env->DeleteLocalRef(jallowedAuthAlgorithms);
        }
        else {
            ALOGE("GetJavaWifiConfiguration Error: jallowedAuthAlgorithms is NULL!");
        }
    }

    AutoPtr<IBitSet> allowedPairwiseCiphers;
    config->GetAllowedPairwiseCiphers((IBitSet**)&allowedPairwiseCiphers);
    if (allowedPairwiseCiphers != NULL) {
        jobject jallowedPairwiseCiphers = ElUtil::GetJavaBitSet(env, allowedPairwiseCiphers);
        if (jallowedPairwiseCiphers != NULL) {
            jfieldID f = env->GetFieldID(cfgKlass, "allowedPairwiseCiphers", "Ljava/util/BitSet;");
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail GetFieldID: allowedPairwiseCiphers : %d!\n", __LINE__);

            env->SetObjectField(jconfig, f, jallowedPairwiseCiphers);
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail SetObjectField: allowedPairwiseCiphers : %d!\n", __LINE__);

            env->DeleteLocalRef(jallowedPairwiseCiphers);
        }
        else {
            ALOGE("GetJavaWifiConfiguration Error: jallowedPairwiseCiphers is NULL!");
        }
    }

    AutoPtr<IBitSet> allowedGroupCiphers;
    config->GetAllowedGroupCiphers((IBitSet**)&allowedGroupCiphers);
    if (allowedGroupCiphers != NULL) {
        jobject jallowedGroupCiphers = ElUtil::GetJavaBitSet(env, allowedGroupCiphers);
        if (jallowedGroupCiphers != NULL) {
            jfieldID f = env->GetFieldID(cfgKlass, "allowedGroupCiphers", "Ljava/util/BitSet;");
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail GetFieldID: allowedGroupCiphers : %d!\n", __LINE__);

            env->SetObjectField(jconfig, f, jallowedGroupCiphers);
            CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail SetObjectField: allowedGroupCiphers : %d!\n", __LINE__);

            env->DeleteLocalRef(jallowedGroupCiphers);
        }
        else {
            ALOGE("GetJavaWifiConfiguration Error: jallowedGroupCiphers is NULL!");
        }
    }

    AutoPtr<IWifiEnterpriseConfig> enterpriseConfig;
    config->GetEnterpriseConfig((IWifiEnterpriseConfig**)&enterpriseConfig);
    if (enterpriseConfig != NULL) {
        jobject jenterpriseConfig = ElUtil::GetJavaWifiEnterpriseConfig(env, enterpriseConfig);

        jfieldID f = env->GetFieldID(cfgKlass, "enterpriseConfig", "Landroid/net/wifi/WifiEnterpriseConfig;");
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail GetFieldID: enterpriseConfig : %d!\n", __LINE__);

        env->SetObjectField(jconfig, f, jenterpriseConfig);
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail SetObjectField: enterpriseConfig : %d!\n", __LINE__);

        env->DeleteLocalRef(jenterpriseConfig);
    }

    AutoPtr<IIpConfiguration> ipConfiguration;
    config->GetIpConfiguration((IIpConfiguration**)&ipConfiguration);
    if (ipConfiguration != NULL) {
        jobject jipConfiguration = ElUtil::GetJavaIpConfiguration(env, ipConfiguration);

        jfieldID f = env->GetFieldID(cfgKlass, "mIpConfiguration", "Landroid/net/IpConfiguration;");
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail GetFieldID: mIpConfiguration : %d!\n", __LINE__);

        env->SetObjectField(jconfig, f, jipConfiguration);
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiConfiguration Fail SetObjectField: mIpConfiguration : %d!\n", __LINE__);

        env->DeleteLocalRef(jipConfiguration);
    }

    String dhcpServer;
    config->GetDhcpServer(&dhcpServer);
    SetJavaStringField(env, cfgKlass, jconfig, dhcpServer, "dhcpServer", "GetJavaWifiConfiguration");

    String defaultGwMacAddress;
    config->GetDefaultGwMacAddress(&defaultGwMacAddress);
    SetJavaStringField(env, cfgKlass, jconfig, defaultGwMacAddress, "defaultGwMacAddress", "GetJavaWifiConfiguration");

    config->GetAutoJoinStatus(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "autoJoinStatus", "GetJavaWifiConfiguration");

    config->GetSelfAdded(&tempBool);
    ElUtil::SetJavaBoolField(env, cfgKlass, jconfig, tempBool, "selfAdded", "GetJavaWifiConfiguration");

    config->GetDidSelfAdd(&tempBool);
    ElUtil::SetJavaBoolField(env, cfgKlass, jconfig, tempBool, "didSelfAdd", "GetJavaWifiConfiguration");

    config->GetNoInternetAccess(&tempBool);
    ElUtil::SetJavaBoolField(env, cfgKlass, jconfig, tempBool, "noInternetAccess", "GetJavaWifiConfiguration");

    config->GetCreatorUid(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "creatorUid", "GetJavaWifiConfiguration");

    config->GetLastConnectUid(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "lastConnectUid", "GetJavaWifiConfiguration");

    config->GetLastUpdateUid(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "lastUpdateUid", "GetJavaWifiConfiguration");

    Int64 blackListTimestamp;
    config->GetBlackListTimestamp(&blackListTimestamp);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, blackListTimestamp, "blackListTimestamp", "GetJavaWifiConfiguration");

    Int64 lastConnectionFailure;
    config->GetLastConnectionFailure(&lastConnectionFailure);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, lastConnectionFailure, "lastConnectionFailure", "GetJavaWifiConfiguration");

    config->GetNumConnectionFailures(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numConnectionFailures", "GetJavaWifiConfiguration");

    config->GetNumIpConfigFailures(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numIpConfigFailures", "GetJavaWifiConfiguration");

    config->GetNumAuthFailures(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numAuthFailures", "GetJavaWifiConfiguration");

    config->GetNumScorerOverride(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numScorerOverride", "GetJavaWifiConfiguration");

    config->GetNumScorerOverrideAndSwitchedNetwork(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numScorerOverrideAndSwitchedNetwork", "GetJavaWifiConfiguration");

    config->GetNumAssociation(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numAssociation", "GetJavaWifiConfiguration");

    config->GetNumUserTriggeredWifiDisableLowRSSI(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numUserTriggeredWifiDisableLowRSSI", "GetJavaWifiConfiguration");

    config->GetNumUserTriggeredWifiDisableBadRSSI(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numUserTriggeredWifiDisableBadRSSI", "GetJavaWifiConfiguration");

    config->GetNumUserTriggeredWifiDisableNotHighRSSI(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numUserTriggeredWifiDisableNotHighRSSI", "GetJavaWifiConfiguration");

    config->GetNumTicksAtLowRSSI(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numTicksAtLowRSSI", "GetJavaWifiConfiguration");

    config->GetNumTicksAtBadRSSI(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numTicksAtBadRSSI", "GetJavaWifiConfiguration");

    config->GetNumTicksAtNotHighRSSI(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numTicksAtNotHighRSSI", "GetJavaWifiConfiguration");

    config->GetNumUserTriggeredJoinAttempts(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "numUserTriggeredJoinAttempts", "GetJavaWifiConfiguration");

    config->GetAutoJoinUseAggressiveJoinAttemptThreshold(&tempInt);
    ElUtil::SetJavaIntField(env, cfgKlass, jconfig, tempInt, "autoJoinUseAggressiveJoinAttemptThreshold", "GetJavaWifiConfiguration");

    config->GetAutoJoinBailedDueToLowRssi(&tempBool);
    ElUtil::SetJavaBoolField(env, cfgKlass, jconfig, tempBool, "autoJoinBailedDueToLowRssi", "GetJavaWifiConfiguration");

    env->DeleteLocalRef(cfgKlass);

    return jconfig;
}

jobject ElUtil::GetJavaWifiEnterpriseConfig(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiEnterpriseConfig* config)
{
    if (env == NULL || config == NULL) {
        ALOGE("GetJavaWifiEnterpriseConfig Invalid arguments!");
        return NULL;
    }

    jclass parcelClass = env->FindClass("android/os/Parcel");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig FindClass: Parcel : %d", __LINE__);

    jmethodID m = env->GetStaticMethodID(parcelClass, "obtain", "()Landroid/os/Parcel;");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig GetStaticMethodID: obtain : %d", __LINE__);

    jobject jparcel = env->CallStaticObjectMethod(parcelClass, m);
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallStaticObjectMethod: obtain : %d", __LINE__);

    jmethodID mWriteInt = env->GetMethodID(parcelClass, "writeInt", "(I)V");
    jmethodID mWriteString = env->GetMethodID(parcelClass, "writeString", "(Ljava/lang/String;)V");
    jmethodID mWriteByteArray = env->GetMethodID(parcelClass, "writeByteArray", "([B)V");

    AutoPtr<IParcel> source;
    CParcel::New((IParcel**)&source);
    IParcelable::Probe(config)->WriteToParcel(source);
    source->SetDataPosition(0);

    Int32 count;
    source->ReadInt32(&count);
    env->CallVoidMethod(jparcel, mWriteInt, count);
    CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeInt", __LINE__);
    for (Int32 i = 0; i < count; i++) {
        String key, value;
        source->ReadString(&key);
        source->ReadString(&value);
        jstring jkey = GetJavaString(env, key);
        jstring jvalue = GetJavaString(env, value);
        env->CallVoidMethod(jparcel, mWriteString, jkey);
        CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeString", __LINE__);
        env->CallVoidMethod(jparcel, mWriteString, jvalue);
        CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeString", __LINE__);
        env->DeleteLocalRef(jkey);
        env->DeleteLocalRef(jvalue);
    }

    Int32 len;
    source->ReadInt32(&len);
    env->CallVoidMethod(jparcel, mWriteInt, len);
    CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeInt", __LINE__);
    if (len > 0) {
        AutoPtr<ArrayOf<Byte> > byteArray;
        source->ReadArrayOf((Handle32*)&byteArray);
        jbyteArray jbytearray = GetJavaByteArray(env, byteArray);
        env->CallVoidMethod(jparcel, mWriteByteArray, jbytearray);
        CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeByteArray", __LINE__);
        env->DeleteLocalRef(jbytearray);
    }

    source->ReadInt32(&len);
    env->CallVoidMethod(jparcel, mWriteInt, len);
    CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeInt", __LINE__);
    if (len > 0) {
        AutoPtr<ArrayOf<Byte> > byteArray;
        source->ReadArrayOf((Handle32*)&byteArray);
        jbyteArray jbytearray = GetJavaByteArray(env, byteArray);
        env->CallVoidMethod(jparcel, mWriteByteArray, jbytearray);
        CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeByteArray", __LINE__);
        env->DeleteLocalRef(jbytearray);
        String algorithm;
        source->ReadString(&algorithm);
        jstring jalgorithm = GetJavaString(env, algorithm);
        env->CallVoidMethod(jparcel, mWriteString, jalgorithm);
        CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeString", __LINE__);
        env->DeleteLocalRef(jalgorithm);
    }

    source->ReadInt32(&len);
    env->CallVoidMethod(jparcel, mWriteInt, len);
    CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeInt", __LINE__);
    if (len > 0) {
        AutoPtr<ArrayOf<Byte> > byteArray;
        source->ReadArrayOf((Handle32*)&byteArray);
        jbyteArray jbytearray = GetJavaByteArray(env, byteArray);
        env->CallVoidMethod(jparcel, mWriteByteArray, jbytearray);
        CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: writeByteArray", __LINE__);
        env->DeleteLocalRef(jbytearray);
    }

    jmethodID mSetDataPosition = env->GetMethodID(parcelClass, "setDataPosition", "(I)V");
    env->CallVoidMethod(jparcel, mSetDataPosition, 0);
    CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig CallVoidMethod: setDataPosition", __LINE__);

    jclass wecKlass = env->FindClass("android/net/wifi/WifiEnterpriseConfig");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig FindClass: WifiEnterpriseConfig line: %d", __LINE__);

    jfieldID f = env->GetStaticFieldID(wecKlass, "CREATOR", "Landroid/os/Parcelable$Creator;");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig Fail GetStaticFieldID: SIMPLIFIED_CHINESE : %d", __LINE__);

    jobject jcreater = env->GetStaticObjectField(wecKlass, f);
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig Fail GetStaticObjectField: %d", __LINE__);

    jclass cKlass = env->GetObjectClass(jcreater);
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig GetObjectClass: Parcelable$Creator line: %d", __LINE__);

    m = env->GetMethodID(cKlass, "createFromParcel", "(Landroid/os/Parcel;)Landroid/net/wifi/WifiEnterpriseConfig;");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig GetMethodID: WifiEnterpriseConfig line: %d", __LINE__);

    jobject jconfig = env->CallObjectMethod(jcreater, m, jparcel);
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiEnterpriseConfig NewObject: WifiEnterpriseConfig line: %d", __LINE__);

    env->DeleteLocalRef(parcelClass);
    env->DeleteLocalRef(jparcel);
    env->DeleteLocalRef(wecKlass);
    env->DeleteLocalRef(jcreater);
    env->DeleteLocalRef(cKlass);
    return jconfig;
}

jobject ElUtil::GetJavaIpConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ IIpConfiguration* config)
{
    if (env == NULL || config == NULL) {
        ALOGE("GetJavaIpConfiguration Invalid arguments!");
        return NULL;
    }

    jclass ipaClass = env->FindClass("android/net/IpConfiguration$IpAssignment");
    ElUtil::CheckErrorAndLog(env, "GetJavaIpConfiguration Fail FindClass IpAssignment %d", __LINE__);

    Elastos::Droid::Net::IpConfigurationIpAssignment ipAssignment;
    config->GetIpAssignment(&ipAssignment);
    jobject jipAssignment = NULL;
    switch (ipAssignment) {
        case Elastos::Droid::Net::STATIC_IpAssignment: {
            jfieldID f = env->GetStaticFieldID(ipaClass, "STATIC", "Landroid/net/IpConfiguration$IpAssignment;");
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticFieldID: STATIC : %d!\n", __LINE__);

            jipAssignment = env->GetStaticObjectField(ipaClass, f);
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticObjectField: STATIC : %d!\n", __LINE__);
            break;
        }
        case Elastos::Droid::Net::DHCP_IpAssignment: {
            jfieldID f = env->GetStaticFieldID(ipaClass, "DHCP", "Landroid/net/IpConfiguration$IpAssignment;");
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticFieldID: DHCP : %d!\n", __LINE__);

            jipAssignment = env->GetStaticObjectField(ipaClass, f);
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticObjectField: DHCP : %d!\n", __LINE__);
            break;
        }
        default: {
            jfieldID f = env->GetStaticFieldID(ipaClass, "UNASSIGNED", "Landroid/net/IpConfiguration$IpAssignment;");
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticFieldID: UNASSIGNED : %d!\n", __LINE__);

            jipAssignment = env->GetStaticObjectField(ipaClass, f);
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticObjectField: UNASSIGNED : %d!\n", __LINE__);
            break;
        }
    }

    jclass psClass = env->FindClass("android/net/IpConfiguration$ProxySettings");
    ElUtil::CheckErrorAndLog(env, "GetJavaIpConfiguration Fail FindClass ProxySettings %d", __LINE__);

    Elastos::Droid::Net::IpConfigurationProxySettings proxySettings;
    config->GetIpAssignment(&proxySettings);
    jobject jproxySettings = NULL;
    switch(proxySettings) {
        case Elastos::Droid::Net::NONE_ProxySettings: {
            jfieldID f = env->GetStaticFieldID(psClass, "NONE", "Landroid/net/IpConfiguration$ProxySettings;");
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticFieldID: NONE : %d!\n", __LINE__);

            jproxySettings = env->GetStaticObjectField(psClass, f);
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticObjectField: STATIC : %d!\n", __LINE__);
            break;
        }
        case Elastos::Droid::Net::STATIC_ProxySettings: {
            jfieldID f = env->GetStaticFieldID(psClass, "STATIC", "Landroid/net/IpConfiguration$ProxySettings;");
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticFieldID: STATIC : %d!\n", __LINE__);

            jproxySettings = env->GetStaticObjectField(psClass, f);
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticObjectField: STATIC : %d!\n", __LINE__);
            break;
        }
        case Elastos::Droid::Net::PAC_ProxySettings: {
            jfieldID f = env->GetStaticFieldID(psClass, "PAC", "Landroid/net/IpConfiguration$ProxySettings;");
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticFieldID: PAC : %d!\n", __LINE__);

            jproxySettings = env->GetStaticObjectField(psClass, f);
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticObjectField: PAC : %d!\n", __LINE__);
            break;
        }
        default: {
            jfieldID f = env->GetStaticFieldID(psClass, "UNASSIGNED", "Landroid/net/IpConfiguration$ProxySettings;");
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticFieldID: UNASSIGNED : %d!\n", __LINE__);

            jproxySettings = env->GetStaticObjectField(psClass, f);
            CheckErrorAndLog(env,"GetJavaIpConfiguration GetStaticObjectField: UNASSIGNED : %d!\n", __LINE__);
            break;
        }
    }

    AutoPtr<IStaticIpConfiguration> staticIpConfiguration;
    config->GetStaticIpConfiguration((IStaticIpConfiguration**)&staticIpConfiguration);
    jobject jstaticIpConfiguration = GetJavaStaticIpConfiguration(env, staticIpConfiguration);

    AutoPtr<IProxyInfo> httpProxy;
    config->GetHttpProxy((IProxyInfo**)&httpProxy);
    jobject jhttpProxy = ElUtil::GetJavaProxyInfo(env, httpProxy);

    jclass icKlass = env->FindClass("android/net/IpConfiguration");
    ElUtil::CheckErrorAndLog(env, "GetJavaIpConfiguration FindClass: IpConfiguration line: %d", __LINE__);

    jmethodID m = env->GetMethodID(icKlass, "<init>", "(Landroid/net/IpConfiguration$IpAssignment;"
        "Landroid/net/IpConfiguration$ProxySettings;Landroid/net/StaticIpConfiguration;Landroid/net/ProxyInfo)V");
    ElUtil::CheckErrorAndLog(env, "GetJavaIpConfiguration GetMethodID: IpConfiguration line: %d", __LINE__);

    jobject jconfig = env->NewObject(icKlass, m, jipAssignment, jproxySettings, jstaticIpConfiguration, jhttpProxy);
    ElUtil::CheckErrorAndLog(env, "GetJavaIpConfiguration NewObject: IpConfiguration line: %d", __LINE__);

    env->DeleteLocalRef(icKlass);
    env->DeleteLocalRef(ipaClass);
    env->DeleteLocalRef(psClass);
    env->DeleteLocalRef(jipAssignment);
    env->DeleteLocalRef(jproxySettings);
    env->DeleteLocalRef(jstaticIpConfiguration);
    env->DeleteLocalRef(jhttpProxy);

    return jconfig;
}

jobject ElUtil::GetJavaStaticIpConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ IStaticIpConfiguration* config)
{
    if (env == NULL || config == NULL) {
        ALOGE("GetJavaStaticIpConfiguration Invalid arguments!");
        return NULL;
    }

    jclass sicKlass = env->FindClass("android/net/StaticIpConfiguration");
    ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration FindClass: StaticIpConfiguration line: %d", __LINE__);

    jmethodID m = env->GetMethodID(sicKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration GetMethodID: StaticIpConfiguration line: %d", __LINE__);

    jobject jconfig = env->NewObject(sicKlass, m);
    ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration NewObject: StaticIpConfiguration line: %d", __LINE__);

    AutoPtr<ILinkAddress> ipAddress;
    config->GetIpAddress((ILinkAddress**)&ipAddress);

    jobject jipAddress = GetJavaLinkAddress(env, ipAddress);
    jfieldID f = env->GetFieldID(sicKlass, "ipAddress", "Landroid/net/LinkAddress;");
    ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration Fail GetFieldID: ipAddress : %d!\n", __LINE__);

    env->SetObjectField(jconfig, f, jipAddress);
    ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration Fail SetObjectField: ipAddress : %d!\n", __LINE__);

    AutoPtr<IInetAddress> gateway;
    config->GetGateway((IInetAddress**)&gateway);

    jobject jgateway = GetJavaInetAddress(env, gateway);
    f = env->GetFieldID(sicKlass, "gateway", "Ljava/net/InetAddress;");
    ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration Fail GetFieldID: gateway : %d!\n", __LINE__);

    env->SetObjectField(jconfig, f, jgateway);
    ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration Fail SetObjectField: gateway : %d!\n", __LINE__);

    AutoPtr<IArrayList> dnsServers;
    config->GetDnsServers((IArrayList**)&dnsServers);
    Int32 size = 0;
    if (dnsServers)
        dnsServers->GetSize(&size);
    if (size > 0) {
        jfieldID f = env->GetFieldID(sicKlass, "dnsServers", "Ljava/util/ArrayList;");
        ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration GetFieldID: dnsServers line: %d", __LINE__);

        jobject jdnsServers = env->GetObjectField(jconfig, f);
        ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration GetObjectField: dnsServers line: %d", __LINE__);

        jclass listKlass = env->FindClass("java/util/ArrayList");
        CheckErrorAndLog(env, "GetJavaStaticIpConfiguration FindClass: ArrayList : %d!", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        CheckErrorAndLog(env, "GetJavaStaticIpConfiguration GetMethodID: add : %d!", __LINE__);

        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> item;
            dnsServers->Get(i, (IInterface**)&item);
            jobject jitem = GetJavaInetAddress(env, IInetAddress::Probe(item));
            env->CallBooleanMethod(jdnsServers, mAdd, jitem);
            ElUtil::CheckErrorAndLog(env, "GetJavaStaticIpConfiguration CallBooleanMethod: add : %d!\n", __LINE__);
            env->DeleteLocalRef(jitem);
        }
        env->DeleteLocalRef(jdnsServers);
        env->DeleteLocalRef(listKlass);
    }

    String domains;
    config->GetDomains(&domains);
    SetJavaStringField(env, sicKlass, jconfig, domains, "domains", "GetJavaStaticIpConfiguration");

    env->DeleteLocalRef(sicKlass);
    env->DeleteLocalRef(jipAddress);
    env->DeleteLocalRef(jgateway);

    return jconfig;
}

jobject ElUtil::GetJavaBitSet(
    /* [in] */ JNIEnv* env,
    /* [in] */ IBitSet* bitset)
{
    if (env == NULL || bitset == NULL) {
        ALOGE("GetJavaBitSet Invalid arguments!");
        return NULL;
    }

    jobject jbitset = NULL;

    jclass bitsetKlass = env->FindClass("java/util/BitSet");
    CheckErrorAndLog(env, "FindClass: BitSet : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(bitsetKlass, "<init>", "([J)V");
    CheckErrorAndLog(env, "GetStaticMethodID: valueOf : %d!\n", __LINE__);

    AutoPtr<ArrayOf<Int64> > int64Arr;
    bitset->ToArray((ArrayOf<Int64>**)&int64Arr);
    if (int64Arr != NULL) {
        Int32 count = int64Arr->GetLength();

        jlongArray jlongArr = env->NewLongArray(count);
        CheckErrorAndLog(env, "NewLongArray: %d!\n", __LINE__);

        jlong* longArrInd = (jlong*)int64Arr->GetPayload();

        env->SetLongArrayRegion(jlongArr,0, count, longArrInd);
        CheckErrorAndLog(env, "SetLongArrayRegion: %d!\n", __LINE__);

        jbitset = env->NewObject(bitsetKlass, m, jlongArr);
        CheckErrorAndLog(env, "GetJavaLinkProperties NewObject: BitSet : %d!\n", __LINE__);
        env->DeleteLocalRef(jlongArr);
    }

    env->DeleteLocalRef(bitsetKlass);

    return jbitset;
}

jobject ElUtil::GetJavaPermissionInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IPermissionInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaPermissionInfo Invalid arguments!");
        return NULL;
    }

    jobject jinfo = NULL;

    jclass perKlass = env->FindClass("android/content/pm/PermissionInfo");
    CheckErrorAndLog(env, "GetJavaPermissionInfo FindClass: PermissionInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(perKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaPermissionInfo GetStaticMethodID: valueOf : %d!\n", __LINE__);

    jinfo = env->NewObject(perKlass, m);
    CheckErrorAndLog(env, "GetJavaPermissionInfo NewObject: PermissionInfo : %d!\n", __LINE__);

    Int32 tempInt;
    info->GetProtectionLevel(&tempInt);
    ElUtil::SetJavaIntField(env, perKlass, jinfo, tempInt, "protectionLevel", "GetJavaPermissionInfo");

    info->GetFlags(&tempInt);
    ElUtil::SetJavaIntField(env, perKlass, jinfo, tempInt, "flags", "GetJavaPermissionInfo");

    String tempString;
    info->GetGroup(&tempString);
    ElUtil::SetJavaStringField(env, perKlass, jinfo, tempString, "group", "GetJavaPermissionInfo");

    info->GetDescriptionRes(&tempInt);
    ElUtil::SetJavaIntField(env, perKlass, jinfo, tempInt, "descriptionRes", "GetJavaPermissionInfo");


    AutoPtr<IPackageItemInfo> pkgInfo = IPackageItemInfo::Probe(info);
    SetJavaPackageItemInfo(env, perKlass, jinfo, pkgInfo);

    env->DeleteLocalRef(perKlass);

    return jinfo;
}

jobject ElUtil::GetJavaPermissionGroupInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IPermissionGroupInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaPermissionGroupInfo Invalid arguments!");
        return NULL;
    }

    jobject jinfo = NULL;

    jclass pergKlass = env->FindClass("android/content/pm/PermissionGroupInfo");
    CheckErrorAndLog(env, "GetJavaPermissionGroupInfo FindClass: PermissionGroupInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(pergKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaPermissionGroupInfo GetStaticMethodID: valueOf : %d!\n", __LINE__);

    jinfo = env->NewObject(pergKlass, m);
    CheckErrorAndLog(env, "GetJavaPermissionGroupInfo NewObject: PermissionGroupInfo : %d!\n", __LINE__);

    Int32 tempInt;
    info->GetDescriptionRes(&tempInt);
    ElUtil::SetJavaIntField(env, pergKlass, jinfo, tempInt, "descriptionRes", "GetJavaPermissionGroupInfo");

    AutoPtr<ICharSequence> nonLocalizedDescription;
    info->GetNonLocalizedDescription((ICharSequence**)&nonLocalizedDescription);
    if (nonLocalizedDescription != NULL) {
        String snonLocalizedLabel;
        nonLocalizedDescription->ToString(&snonLocalizedLabel);
        jstring jnonLocalizedLabel = ElUtil::GetJavaString(env, snonLocalizedLabel);

        jfieldID f = env->GetFieldID(pergKlass, "nonLocalizedDescription", "Ljava/lang/CharSequence;");
        ElUtil::CheckErrorAndLog(env, "GetJavaPermissionGroupInfo Fail GetFieldID: CharSequence : %d!\n", __LINE__);

        env->SetObjectField(jinfo, f, jnonLocalizedLabel);
        ElUtil::CheckErrorAndLog(env, "GetJavaPermissionGroupInfo Fail SetObjectField: jnonLocalizedLabel : %d!\n", __LINE__);
        env->DeleteLocalRef(jnonLocalizedLabel);
    }

    info->GetFlags(&tempInt);
    ElUtil::SetJavaIntField(env, pergKlass, jinfo, tempInt, "flags", "GetJavaPermissionGroupInfo");

    info->GetPriority(&tempInt);
    ElUtil::SetJavaIntField(env, pergKlass, jinfo, tempInt, "priority", "GetJavaPermissionGroupInfo");


    AutoPtr<IPackageItemInfo> pkgInfo = IPackageItemInfo::Probe(info);
    SetJavaPackageItemInfo(env, pergKlass, jinfo, pkgInfo);

    env->DeleteLocalRef(pergKlass);

    return jinfo;
}

bool ToElAudioAttributes(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jattribute,
    /* [out] */ IAudioAttributes** attributes)
{
    if (env == NULL || jattribute == NULL || attributes == NULL) {
        ALOGE("ToElAudioAttributes: Invalid argumenet!");
        *attributes = NULL;
        return false;
    }
    *attributes = NULL;

    jclass jAudioAttributesClass = env->FindClass("android/media/AudioAttributes");
    ElUtil::CheckErrorAndLog(env, "ToElAudioAttributes FindClass: AudioAttributes : %d!\n", __LINE__);

    AutoPtr<IAudioAttributesBuilder> aaBuilder;
    if (NOERROR != CAudioAttributesBuilder::New((IAudioAttributesBuilder**)&aaBuilder)) {
        ALOGE("ToElAudioAttributes: create CAudioAttributesBuilder fail!");
        return false;
    }

    Int32 usage = ElUtil::GetJavaIntField(env, jAudioAttributesClass, jattribute, "mUsage", "ToElAudioAttributes");
    aaBuilder->SetUsage(usage);
    Int32 contentType = ElUtil::GetJavaIntField(env, jAudioAttributesClass, jattribute, "mContentType", "ToElAudioAttributes");
    aaBuilder->SetContentType(contentType);
    Int32 source = ElUtil::GetJavaIntField(env, jAudioAttributesClass, jattribute, "mSource", "ToElAudioAttributes");
    if (source != IMediaRecorderAudioSource::AUDIO_SOURCE_INVALID)
        aaBuilder->SetCapturePreset(source);
    Int32 flags = ElUtil::GetJavaIntField(env, jAudioAttributesClass, jattribute, "mFlags", "ToElAudioAttributes");
    aaBuilder->SetFlags(flags);
    String formattedTags = ElUtil::GetJavaStringField(env, jAudioAttributesClass, jattribute, "mFormattedTags", "ToElAudioAttributes");

    jfieldID ftags = env->GetFieldID(jAudioAttributesClass, "mTags", "Ljava/util/HashSet;");
    ElUtil::CheckErrorAndLog(env, "ToElAudioAttributes GetFieldID: mTags: %d!\n", __LINE__);
    jobject jtags = env->GetObjectField(jattribute, ftags);
    ElUtil::CheckErrorAndLog(env, "ToElAudioAttributes GetObjectField mTags: %d!\n", __LINE__);
    if (jtags != NULL) {
        jclass setKlass = env->FindClass("java/util/HashSet");
        jmethodID m = env->GetMethodID(setKlass, "iterator", "()Ljava/util/Iterator;");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: iterator ()Ljava/util/Iterator; : %d!\n", __LINE__);
        env->DeleteLocalRef(setKlass);

        jobject jIterator = env->CallObjectMethod(jtags, m);
        ElUtil::CheckErrorAndLog(env, "CallObjectMethod: jIterator : %d!\n", __LINE__);

        if (jIterator != NULL) {
            jclass iteratorKlass = env->FindClass("java/util/Iterator");
            ElUtil::CheckErrorAndLog(env, "FindClass: java/util/Iterator : %d!\n", __LINE__);

            jmethodID jHasNext = env->GetMethodID(iteratorKlass, "hasNext", "()Z");
            ElUtil::CheckErrorAndLog(env, "GetMethodID: hasNext : %d!\n", __LINE__);

            jmethodID jNext = env->GetMethodID(iteratorKlass, "next", "()Ljava/lang/Object;");
            ElUtil::CheckErrorAndLog(env, "GetMethodID: next : %d!\n", __LINE__);

            env->DeleteLocalRef(iteratorKlass);

            jboolean hasNext = env->CallBooleanMethod(jIterator, jHasNext);
            while (hasNext) {
                jobject jValue = env->CallObjectMethod(jIterator, jNext);
                String valueClassName = ElUtil::GetClassName(env, jValue);

                String value;
                if (valueClassName.Equals("java.lang.String")) {
                    value = ElUtil::ToElString(env, (jstring)jValue);
                    aaBuilder->AddTag(value);
                }
                else {
                    ALOGE("ToElAudioAttributes() wrong Value type: %s", valueClassName.string());
                    env->DeleteLocalRef(jValue);
                    env->DeleteLocalRef(jIterator);
                    return false;
                }
                env->DeleteLocalRef(jValue);
                hasNext = env->CallBooleanMethod(jIterator, jHasNext);
            }
            env->DeleteLocalRef(jIterator);
        }
        env->DeleteLocalRef(jtags);
    }

    aaBuilder->Build(attributes);
    env->DeleteLocalRef(jAudioAttributesClass);
    return true;
}

bool ToElNotificationAction(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jnotiaction,
    /* [out] */ INotificationAction** notiaction)
{
    if (env == NULL || jnotiaction == NULL || notiaction == NULL) {
        ALOGE("ToElNotification : Invalid argumenet!");
        *notiaction = NULL;
        return false;
    }
    *notiaction = NULL;

    jclass jnotificationActionClass = env->FindClass("android/app/Notification$Action");
    ElUtil::CheckErrorAndLog(env, "ToElNotification FindClass: Notification$Action: %d!\n", __LINE__);

    AutoPtr<ICharSequence> title;
    jfieldID fTitle = env->GetFieldID(jnotificationActionClass , "title", "Ljava/lang/CharSequence;");
    ElUtil::CheckErrorAndLog(env, "ToElNotification GetFieldID: title: %d!\n", __LINE__);
    jobject jtitle= env->GetObjectField(jnotiaction, fTitle);
    ElUtil::CheckErrorAndLog(env, "ToElNotification GetObjectField title: %d!\n", __LINE__);
    if (jtitle != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        ElUtil::CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jstr = (jstring)env->CallObjectMethod(jtitle, m);
        ElUtil::CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String actiontitle = ElUtil::ToElString(env, jstr);
        env->DeleteLocalRef(jstr);

        CString::New(actiontitle, (ICharSequence**)&title);

        env->DeleteLocalRef(csClass);
    }
    env->DeleteLocalRef(jtitle);

    Int32 icon = ElUtil::GetJavaIntField(env, jnotificationActionClass, jnotiaction, "icon", "ToElNotificationAction");

    AutoPtr<IPendingIntent> actionIntent;
    jfieldID factionIntent = env->GetFieldID(jnotificationActionClass , "actionIntent", "Landroid/app/PendingIntent;");
    ElUtil::CheckErrorAndLog(env, "ToElNotification GetFieldID: actionIntent: %d!\n", __LINE__);
    jobject jactionIntent = env->GetObjectField(jnotiaction, factionIntent);
    ElUtil::CheckErrorAndLog(env, "ToElNotification GetObjectField actionIntent: %d!\n", __LINE__);
    if (jactionIntent != NULL) {
        if (!ElUtil::ToElPendingIntent(env, jactionIntent, (IPendingIntent**)&actionIntent)) {
            ALOGE("ToElNotificationAction () ToElPendingIntent fail!, line:%d", __LINE__);
        }
    }
    env->DeleteLocalRef(jactionIntent);

    AutoPtr<INotificationActionBuilder> notiActBuilder;
    if (NOERROR != CNotificationActionBuilder::New(icon, title, actionIntent, (INotificationActionBuilder**)&notiActBuilder)) {
        ALOGE("ToElNotificationAction : create CNotificationActionBuilder fail!");
        return false;
    }
    else {
        AutoPtr<IBundle> extras;
        jfieldID fExtras = env->GetFieldID(jnotificationActionClass , "mExtras", "Landroid/os/Bundle;");
        ElUtil::CheckErrorAndLog(env, "ToElNotification GetFieldID: mExtras : %d!\n", __LINE__);
        jobject jextras = env->GetObjectField(jnotiaction, fExtras);
        ElUtil::CheckErrorAndLog(env, "ToElNotification GetObjectField mExtras : %d!\n", __LINE__);
        if (jextras != NULL) {
            ElUtil::ToElBundle(env, jextras, (IBundle**)&extras);
            notiActBuilder->AddExtras(extras);
        }
        env->DeleteLocalRef(jextras);

        AutoPtr<ArrayOf<IRemoteInput*> > remoteInputs;
        jfieldID fremoteInputs = env->GetFieldID(jnotificationActionClass , "mRemoteInputs", "[Landroid/app/RemoteInput;");
        ElUtil::CheckErrorAndLog(env, "ToElNotification GetFieldID: mRemoteInputs: %d!\n", __LINE__);
        jobject jremoteInputs = env->GetObjectField(jnotiaction, fremoteInputs);
        ElUtil::CheckErrorAndLog(env, "ToElNotification GetObjectField mRemoteInputs: %d!\n", __LINE__);
        if (jremoteInputs != NULL) {
            ALOGE("TODO ToElNotificationAction ()  mRemoteInputs !, line:%d", __LINE__);
            //TODO fetch every RemoteInput, and notiActBuilder->AddRemoteInput()
        }
        env->DeleteLocalRef(jremoteInputs);
    }

    notiActBuilder->Build(notiaction);

    env->DeleteLocalRef(jnotificationActionClass);
    return true;
}
bool ToElNotificationActionArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobjectArray jarr,
    /* [out] */ ArrayOf<INotificationAction*>** actionArray)
{
    if (env == NULL || jarr == NULL || actionArray == NULL) {
        ALOGE("ToElNotificationActionArray() Invalid arguments!");
        return false;
    }

    int size = env->GetArrayLength(jarr);
    ElUtil::CheckErrorAndLog(env, "ToElNotificationActionArray(); GetArrayLength failed: %d!\n", __LINE__);

    AutoPtr<ArrayOf<INotificationAction*> > elNotiActArr = ArrayOf<INotificationAction*>::Alloc(size);

    for(int i = 0; i < size; i++) {
        jobject jnotact = env->GetObjectArrayElement(jarr, i);
        ElUtil::CheckErrorAndLog(env, "ToElNotificationActionArray(); GetObjectArrayelement failed : %d!\n", __LINE__);

        AutoPtr<INotificationAction> notiAction;
        ToElNotificationAction(env, jnotact, (INotificationAction**)&notiAction);

        if (notiAction != NULL) {
            elNotiActArr->Set(i, notiAction);
        }
        else {
            ALOGE("ToElNotificationActionArray() %d element is wrong!, line:%d", i, __LINE__);
        }
        env->DeleteLocalRef(jnotact);
    }

    *actionArray = elNotiActArr.Get();
    (*actionArray)->AddRef();
    return true;
}

bool ElUtil::ToElNotification(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jnotification,
    /* [out] */ INotification** notification)
{
    if (env == NULL || jnotification == NULL || notification == NULL) {
        ALOGE("ToElNotification: Invalid argumenet!");
        return false;
    }

    jclass notifKlass = env->FindClass("android/app/Notification");
    CheckErrorAndLog(env, "FindClass: Notification : %d!\n", __LINE__);

    if (NOERROR != CNotification::New(notification)) {
        ALOGD("ToElNotification: create CNotification fail!");
        return false;
    }

    Int64 tempLong;
    tempLong = ElUtil::GetJavalongField(env, notifKlass, jnotification, "when", "ToElNotification");
    (*notification)->SetWhen(tempLong);

    Int32 tempInt;
    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "icon", "ToElNotification");
    (*notification)->SetIcon(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "iconLevel", "ToElNotification");
    (*notification)->SetIconLevel(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "number", "ToElNotification");
    (*notification)->SetNumber(tempInt);

    jfieldID f = env->GetFieldID(notifKlass, "contentIntent", "Landroid/app/PendingIntent;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: contentIntent : %d!\n", __LINE__);

    jobject jcontentIntent = env->GetObjectField(jnotification, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: contentIntent : %d!\n", __LINE__);
    if (jcontentIntent != NULL) {
        AutoPtr<IPendingIntent> contentIntent;
        if (ElUtil::ToElPendingIntent(env, jcontentIntent, (IPendingIntent**)&contentIntent)) {
            (*notification)->SetContentIntent(contentIntent);
        }
        else {
            ALOGE("ToElNotification ToElPendingIntent fail!");
        }

        env->DeleteLocalRef(jcontentIntent);
    }

    f = env->GetFieldID(notifKlass, "deleteIntent", "Landroid/app/PendingIntent;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: deleteIntent : %d!\n", __LINE__);

    jobject jdeleteIntent = env->GetObjectField(jnotification, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: deleteIntent : %d!\n", __LINE__);
    if (jdeleteIntent != NULL) {
        AutoPtr<IPendingIntent> deleteIntent;
        if (ElUtil::ToElPendingIntent(env, jdeleteIntent, (IPendingIntent**)&deleteIntent)) {
            (*notification)->SetDeleteIntent(deleteIntent);
        }
        else {
            ALOGE("ToElNotification ToElPendingIntent fail!");
        }

        env->DeleteLocalRef(jdeleteIntent);
    }

    f = env->GetFieldID(notifKlass, "fullScreenIntent", "Landroid/app/PendingIntent;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: fullScreenIntent : %d!\n", __LINE__);

    jobject jfullScreenIntent = env->GetObjectField(jnotification, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: fullScreenIntent : %d!\n", __LINE__);
    if (jfullScreenIntent != NULL) {
        AutoPtr<IPendingIntent> fullScreenIntent;
        if (ElUtil::ToElPendingIntent(env, jfullScreenIntent, (IPendingIntent**)&fullScreenIntent)) {
            (*notification)->SetFullScreenIntent(fullScreenIntent);
        }
        else {
            ALOGE("ToElNotification ToElPendingIntent fail!");
        }

        env->DeleteLocalRef(jfullScreenIntent);
    }

    f = env->GetFieldID(notifKlass, "tickerText", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID tickerText : %d!\n", __LINE__);

    jobject jtickerText= env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField tickerText : %d!\n", __LINE__);
    AutoPtr<ICharSequence> tickerText;
    if (jtickerText != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jstickerText = (jstring)env->CallObjectMethod(jtickerText, m);
        CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String stickerText = ElUtil::ToElString(env, jstickerText);
        env->DeleteLocalRef(jstickerText);

        CString::New(stickerText, (ICharSequence**)&tickerText);

        (*notification)->SetTickerText(tickerText);

        env->DeleteLocalRef(jtickerText);
        env->DeleteLocalRef(csClass);
    }

    f = env->GetFieldID(notifKlass, "tickerView", "Landroid/widget/RemoteViews;");
    CheckErrorAndLog(env, "GetFieldID tickerView : %d!\n", __LINE__);

    jobject jtickerView= env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField tickerView : %d!\n", __LINE__);
    if (jtickerView != NULL) {
        AutoPtr<IRemoteViews> tickerView;
        if (ElUtil::ToElRemoteViews(env, jtickerView, (IRemoteViews**)&tickerView)) {
            (*notification)->SetTickerView(tickerView);
        }
        else {
            ALOGE("ToElNotification tickerView ToElRemoteViews fail!");
        }

        env->DeleteLocalRef(jtickerView);
    }

    f = env->GetFieldID(notifKlass, "contentView", "Landroid/widget/RemoteViews;");
    CheckErrorAndLog(env, "GetFieldID contentView : %d!\n", __LINE__);

    jobject jcontentView= env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField contentView : %d!\n", __LINE__);
    if (jcontentView != NULL) {
        AutoPtr<IRemoteViews> contentView;
        if (ElUtil::ToElRemoteViews(env, jcontentView, (IRemoteViews**)&contentView)) {
            (*notification)->SetContentView(contentView);
        }
        else {
            ALOGE("ToElNotification contentView ToElRemoteViews fail!");
        }

        env->DeleteLocalRef(jcontentView);
    }

    f = env->GetFieldID(notifKlass, "bigContentView", "Landroid/widget/RemoteViews;");
    CheckErrorAndLog(env, "GetFieldID bigContentView : %d!\n", __LINE__);

    jobject jbigContentView= env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField bigContentView : %d!\n", __LINE__);
    if (jbigContentView != NULL) {
        AutoPtr<IRemoteViews> bigContentView;
        if (ElUtil::ToElRemoteViews(env, jbigContentView, (IRemoteViews**)&bigContentView)) {
            (*notification)->SetBigContentView(bigContentView);
        }
        else {
            ALOGE("ToElNotification bigContentView ToElRemoteViews fail!");
        }
        env->DeleteLocalRef(jbigContentView);
    }

    f = env->GetFieldID(notifKlass, "largeIcon", "Landroid/graphics/Bitmap;");
    CheckErrorAndLog(env, "GetFieldID largeIcon : %d!\n", __LINE__);

    jobject jlargeIcon= env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField largeIcon : %d!\n", __LINE__);
    if (jlargeIcon != NULL) {
        AutoPtr<IBitmap> largeIcon;
        if (ElUtil::ToElBitmap(env, jlargeIcon, (IBitmap**)&largeIcon)) {
            (*notification)->SetLargeIcon(largeIcon);
        }
        else {
            ALOGE("ToElNotification ToElBitmap fail!");
        }
        env->DeleteLocalRef(jlargeIcon);
    }

    f = env->GetFieldID(notifKlass, "sound", "Landroid/net/Uri;");
    CheckErrorAndLog(env, "GetFieldID sound : %d!\n", __LINE__);

    jobject jsound= env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField sound : %d!\n", __LINE__);
    if (jsound != NULL) {
        AutoPtr<IUri> sound;
        if (ElUtil::ToElUri(env, jfullScreenIntent, (IUri**)&sound)) {
            (*notification)->SetSound(sound);
        }
        else {
            ALOGE("ToElNotification ToElUri fail!");
        }

        env->DeleteLocalRef(jsound);
    }

    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "audioStreamType", "ToElNotification");
    (*notification)->SetAudioStreamType(tempInt);

    f = env->GetFieldID(notifKlass, "audioAttributes", "Landroid/media/AudioAttributes;");
    CheckErrorAndLog(env, "GetFieldID audioAttributes : %d!\n", __LINE__);

    jobject jaudioAttributes = env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField: audioAttributes : %d!\n", __LINE__);
    if (jaudioAttributes != NULL) {
        //ALOGE("TODO: ToElNotification audioAttributes is not NULL");
        AutoPtr<IAudioAttributes> audioAttributes;
        if (!ToElAudioAttributes(env, jaudioAttributes, (IAudioAttributes**)&audioAttributes)) {
             ALOGE("ToElNotification: ToElAudioAttributes fail");
        }
        else {
            (*notification)->SetAudioAttributes(audioAttributes);
        }
        env->DeleteLocalRef(jaudioAttributes);
    }

    f = env->GetFieldID(notifKlass, "vibrate", "[J");
    CheckErrorAndLog(env, "GetFieldID vibrate : %d!\n", __LINE__);

    jlongArray jvibrate = (jlongArray)env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField: vibrate : %d!\n", __LINE__);
    if (jvibrate != NULL) {
        int size = env->GetArrayLength(jvibrate);
        CheckErrorAndLog(env, "ToElNotification(); GetArrayLength failed: %d!\n", __LINE__);

        AutoPtr<ArrayOf<Int64> > vibrate = ArrayOf<Int64>::Alloc(size);

        jboolean isCopy;
        jlong* jpayload = env->GetLongArrayElements(jvibrate, &isCopy);
        CheckErrorAndLog(env, "ToElNotification(); GetLongArrayElements failed: %d!\n", __LINE__);
        for(Int32 i = 0; i < size; i++) {
            vibrate->Set(i, jpayload[i]);
        }

        env->ReleaseLongArrayElements(jvibrate, jpayload, 0);
        CheckErrorAndLog(env, "ToElNotification(); ReleaseLongArrayElements failed: %d!\n", __LINE__);

        env->DeleteLocalRef(jvibrate);
    }

    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "ledARGB", "ToElNotification");
    (*notification)->SetLedARGB(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "ledOnMS", "ToElNotification");
    (*notification)->SetLedOnMS(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "ledOffMS", "ToElNotification");
    (*notification)->SetLedOffMS(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "defaults", "ToElNotification");
    (*notification)->SetDefaults(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "flags", "ToElNotification");
    (*notification)->SetFlags(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, notifKlass, jnotification, "priority", "ToElNotification");
    (*notification)->SetPriority(tempInt);

    String category = ElUtil::GetJavaStringField(env, notifKlass, jnotification, "category", "ToElNotification");
    (*notification)->SetCategory(category);

    String mGroupKey = ElUtil::GetJavaStringField(env, notifKlass, jnotification, "mGroupKey", "ToElNotification");
    (*notification)->SetGroup(mGroupKey);

    String mSortKey = ElUtil::GetJavaStringField(env, notifKlass, jnotification, "mSortKey", "ToElNotification");
    (*notification)->SetSortKey(mSortKey);

    f = env->GetFieldID(notifKlass, "extras", "Landroid/os/Bundle;");
    CheckErrorAndLog(env, "GetFieldID: extras Landroid/os/Bundle; : %d!\n", __LINE__);

    jobject jextras = env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField: jextras : %d!\n", __LINE__);

    if (jextras != NULL) {
        AutoPtr<IBundle> extras;
        if (ToElBundle(env, jextras, (IBundle**)&extras)) {
            (*notification)->SetExtras(extras);
        }
        else {
            ALOGE("ToElNotification: ToElBundle() fail : %d!\n", __LINE__);
        }

        env->DeleteLocalRef(jextras);
    }

    f = env->GetFieldID(notifKlass, "actions", "[Landroid/app/Notification$Action;");
    CheckErrorAndLog(env, "GetFieldID: actions : %d!\n", __LINE__);

    jobjectArray jactions = (jobjectArray)env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField: jactions : %d!\n", __LINE__);

    if (jactions != NULL) {
        AutoPtr<ArrayOf<INotificationAction*> > actions;
        ToElNotificationActionArray(env, jactions, (ArrayOf<INotificationAction*>**)&actions);
        (*notification)->SetActions(actions);
        env->DeleteLocalRef(jactions);
    }

    f = env->GetFieldID(notifKlass, "headsUpContentView", "Landroid/widget/RemoteViews;");
    CheckErrorAndLog(env, "GetFieldID: headsUpContentView : %d!\n", __LINE__);

    jobject jheadsUpContentView = env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField: jheadsUpContentView : %d!\n", __LINE__);

    if (jheadsUpContentView != NULL) {
        AutoPtr<IRemoteViews> headsUpContentView;
        if (ToElRemoteViews(env, jheadsUpContentView, (IRemoteViews**)&headsUpContentView)) {
            (*notification)->SetHeadsUpContentView(headsUpContentView);
        }
        else {
            ALOGE("ToElNotification: ToElRemoteViews() fail : %d!\n", __LINE__);
        }

        env->DeleteLocalRef(jheadsUpContentView);
    }

    Int32 visibility = GetJavaIntField(env, notifKlass, jnotification, "visibility", "ToElNotification");
    (*notification)->SetVisibility(visibility);

    f = env->GetFieldID(notifKlass, "publicVersion", "Landroid/app/Notification;");
    CheckErrorAndLog(env, "GetFieldID: publicVersion : %d!\n", __LINE__);

    jobject jpublicVersion = env->GetObjectField(jnotification, f);
    CheckErrorAndLog(env, "GetObjectField: jpublicVersion : %d!\n", __LINE__);

    if (jpublicVersion != NULL) {
        AutoPtr<INotification> publicVersion;
        if (ToElNotification(env, jpublicVersion, (INotification**)&publicVersion)) {
            (*notification)->SetPublicVersion(publicVersion);
        }
        else {
            ALOGE("ToElNotification: ToElNotification() fail : %d!\n", __LINE__);
        }

        env->DeleteLocalRef(jpublicVersion);
    }

    Int32 color = GetJavaIntField(env, notifKlass, jnotification, "color", "ToElNotification");
    (*notification)->SetColor(color);

    env->DeleteLocalRef(notifKlass);

    return true;
}

bool ElUtil::ToElPendingIntent(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jpintent,
    /* [out] */ IPendingIntent** pintent)
{
    if (env == NULL || jpintent == NULL || pintent == NULL) {
        ALOGE("ToElPendingIntent: Invalid argumenet!");
        return false;
    }

    jclass notifKlass = env->FindClass("android/app/PendingIntent");
    CheckErrorAndLog(env, "FindClass: PendingIntent : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(notifKlass, "mTarget", "Landroid/content/IIntentSender;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: IIntentSender : %d!\n", __LINE__);
    env->DeleteLocalRef(notifKlass);

    jobject jtarget = env->GetObjectField(jpintent, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: IIntentSender : %d!\n", __LINE__);
    if (jtarget == NULL) {
        ALOGE("ToElPendingIntent mTarget is NULL!");
        return false;
    }

    AutoPtr<IIIntentSender> target;
    if (!ElUtil::ToElIIntentSender(env, jtarget, (IIIntentSender**)&target)) {
        ALOGE("ToElPendingIntent ToElIIntentSender fail!");
        env->DeleteLocalRef(jtarget);
        return false;
    }

    if (NOERROR != CPendingIntent::New(target, pintent)) {
        ALOGE("ToElPendingIntent: create CPendingIntent fail!");
        env->DeleteLocalRef(jtarget);
        return false;
    }

    env->DeleteLocalRef(jtarget);
    return true;
}

bool ElUtil::ToElIIntentSender(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jiiSender,
    /* [out] */ IIIntentSender** iiSender)
{
    if (env == NULL || jiiSender == NULL || iiSender == NULL) {
        ALOGE("ToElPendingIntent: Invalid argumenet!");
        return false;
    }

    String tarGetClassName = ElUtil::GetClassName(env, jiiSender);

    if (tarGetClassName.Equals("android.content.ElIIntentSenderProxy")) {
        jclass c = env->GetObjectClass(jiiSender);
        ElUtil::CheckErrorAndLog(env, "GetObjectClass: ElIIntentSenderProxy : %d!\n", __LINE__);

        *iiSender = (IIIntentSender*)ElUtil::GetJavalongField(env, c, jiiSender, "mNativeProxy", "nativeStartActivityIntentSender()");
        (*iiSender)->AddRef();
        env->DeleteLocalRef(c);
    }
    else{
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jiiSender);

        if (NOERROR != CIIntentSenderNative::New((Handle64)jvm, (Handle64)jInstance, iiSender)) {
            ALOGE("ToElIIntentSender()  new CIIntentSenderNative fail!\n");
        }
    }

    return true;
}

bool ElUtil::ToElIntentSender(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jiSender,
    /* [out] */ IIntentSender** iSender)
{
    if (env == NULL || jiSender == NULL || iSender == NULL) {
        ALOGE("ToElIntentSender: Invalid argumenet!");
        return false;
    }

     jclass intentSenderKlass = env->GetObjectClass(jiSender);
    ElUtil::CheckErrorAndLog(env, "GetFieldID: IIntentSender : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(intentSenderKlass, "mTarget", "Landroid/content/IIntentSender;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: IIntentSender : %d!\n", __LINE__);

    jobject jtarget = env->GetObjectField(jiSender, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: IIntentSender : %d!\n", __LINE__);

    bool result = true;
    AutoPtr<IIIntentSender> iiSender;
    if (!ElUtil::ToElIIntentSender(env, jtarget, (IIIntentSender**)&iiSender)) {
        ALOGE("ToElIntentSender() ToElIIntentSender fail!");
        result = false;
        goto __EXIT__;
    }

    if (NOERROR != CIntentSender::New(iiSender, iSender)) {
        ALOGE(" ToElIntentSender()  new CIntentSender fail!\n");
        result = false;
        goto __EXIT__;
    }

__EXIT__:
    env->DeleteLocalRef(intentSenderKlass);
    env->DeleteLocalRef(jtarget);
    return result;
}

jobject ElUtil::GetJavaPendingIntent(
    /* [in] */ JNIEnv* env,
    /* [in] */ IPendingIntent* pendingIntent)
{
    if (env == NULL || pendingIntent == NULL) {
        ALOGE("GetJavaPendingIntent: Invalid argumenet!");
        return NULL;
    }

    AutoPtr<IIIntentSender> iIntentSender;

    pendingIntent->GetTarget((IIIntentSender**)&iIntentSender);

    if (iIntentSender == NULL) {
        ALOGE("GetJavaPendingIntent, Got iIntentSender is NULL");
        return NULL;
    }

    jobject jpendingIntentSender = GetJavaIIntentSender(env, iIntentSender);

    jclass klass = env->FindClass("android/app/PendingIntent");
    CheckErrorAndLog(env, "FindClass: PendingIntent : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(Landroid/content/IIntentSender;)V");
    CheckErrorAndLog(env, "GetJavaPendingIntent GetMethodID: PendingIntent(IIntentSender) : %d!\n", __LINE__);

    jobject jpendingIntent = env->NewObject(klass, m, jpendingIntentSender);
    CheckErrorAndLog(env, "GetJavaPendingIntent NewObject: PendingIntent : %d!\n", __LINE__);

    env->DeleteLocalRef(klass);
    env->DeleteLocalRef(jpendingIntentSender);

    return jpendingIntent;
}

Int32 ElUtil::GetJavaHashCode(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject obj)
{
    if (obj == NULL) {
        ALOGW("ElUtil::GetJavaHashCode(), obj is null");
        return 0;
    }

    jclass klass = env->GetObjectClass(obj);
    jmethodID mid = env->GetMethodID(klass, "hashCode", "()I");
    CheckErrorAndLog(env, "GetJavaHashCode(): FindMethod: hashCode: %d!\n", __LINE__);
    jint value = env->CallIntMethod(obj, mid);
    CheckErrorAndLog(env, "GetJavaHashCode: call method hashCode: %d!\n", __LINE__);
    env->DeleteLocalRef(klass);
    return value;
}

jobject ElUtil::GetJavaNetworkInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ INetworkInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaNetworkInfo: Invalid argumenet!");
        return NULL;
    }

    jclass netInfoKlass = env->FindClass("android/net/NetworkInfo");
    CheckErrorAndLog(env, "GetJavaNetworkInfo FindClass: NetworkInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(netInfoKlass, "<init>", "(IILjava/lang/String;Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetJavaNetworkInfo GetMethodID: NetworkInfo : %d!\n", __LINE__);

    Int32 networktype;
    info->GetType(&networktype);
    Int32 subtype;
    info->GetSubtype(&subtype);
    String typeName;
    info->GetTypeName(&typeName);
    jstring jtypeName = ElUtil::GetJavaString(env, typeName);
    String subtypeName;
    info->GetSubtypeName(&subtypeName);
    jstring jsubtypeName = ElUtil::GetJavaString(env, subtypeName);

    jobject jinfo = env->NewObject(netInfoKlass, m, (jint)networktype, (jint)subtype, jtypeName, jsubtypeName);
    CheckErrorAndLog(env, "GetJavaNetworkInfo NewObject: NetworkInfo : %d!\n", __LINE__);
    env->DeleteLocalRef(jtypeName);
    env->DeleteLocalRef(jsubtypeName);

    NetworkInfoState state;
    info->GetState(&state);

    jclass stateKlass = env->FindClass("android/net/NetworkInfo$State");
    CheckErrorAndLog(env, "GetJavaNetworkInfo FindClass: NetworkInfo$State : %d!\n", __LINE__);

    jfieldID f = NULL;

    switch(state) {
        case Elastos::Droid::Net::NetworkInfoState_CONNECTING: {
            f = env->GetStaticFieldID(stateKlass, "CONNECTING", "Landroid/net/NetworkInfo$State;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoState_CONNECTED: {
            f = env->GetStaticFieldID(stateKlass, "CONNECTED", "Landroid/net/NetworkInfo$State;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoState_SUSPENDED: {
            f = env->GetStaticFieldID(stateKlass, "SUSPENDED", "Landroid/net/NetworkInfo$State;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoState_DISCONNECTING: {
            f = env->GetStaticFieldID(stateKlass, "DISCONNECTING", "Landroid/net/NetworkInfo$State;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoState_DISCONNECTED: {
            f = env->GetStaticFieldID(stateKlass, "DISCONNECTED", "Landroid/net/NetworkInfo$State;");
            break;
        }
        default: {
            f = env->GetStaticFieldID(stateKlass, "UNKNOWN", "Landroid/net/NetworkInfo$State;");
            break;
        }
    }
    CheckErrorAndLog(env, "GetStaticFieldID: %d!\n", __LINE__);

    jobject jstate = env->GetStaticObjectField(stateKlass, f);
    CheckErrorAndLog(env, "GetJavaNetworkInfo GetStaticObjectField: %d!\n", __LINE__);

    f = env->GetFieldID(netInfoKlass, "mState", "Landroid/net/NetworkInfo$State;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: NetworkInfo$State : %d!\n", __LINE__);

    env->SetObjectField(jinfo, f, jstate);
    ElUtil::CheckErrorAndLog(env, "GetJavaNetworkInfo SetObjectField: jstate : %d!\n", __LINE__);
    env->DeleteLocalRef(jstate);
    env->DeleteLocalRef(stateKlass);

    NetworkInfoDetailedState detailedState;
    info->GetDetailedState(&detailedState);

    jclass detailStateKlass = env->FindClass("android/net/NetworkInfo$DetailedState");
    CheckErrorAndLog(env, "GetJavaNetworkInfo FindClass: NetworkInfo$DetailedState : %d!\n", __LINE__);

    switch(detailedState) {
        case Elastos::Droid::Net::NetworkInfoDetailedState_IDLE: {
            f = env->GetStaticFieldID(detailStateKlass, "IDLE", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_SCANNING: {
            f = env->GetStaticFieldID(detailStateKlass, "SCANNING", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_CONNECTING: {
            f = env->GetStaticFieldID(detailStateKlass, "CONNECTING", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_AUTHENTICATING: {
            f = env->GetStaticFieldID(detailStateKlass, "AUTHENTICATING", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_OBTAINING_IPADDR: {
            f = env->GetStaticFieldID(detailStateKlass, "OBTAINING_IPADDR", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_CONNECTED: {
            f = env->GetStaticFieldID(detailStateKlass, "CONNECTED", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_SUSPENDED: {
            f = env->GetStaticFieldID(detailStateKlass, "SUSPENDED", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_DISCONNECTING: {
            f = env->GetStaticFieldID(detailStateKlass, "DISCONNECTING", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_DISCONNECTED: {
            f = env->GetStaticFieldID(detailStateKlass, "DISCONNECTED", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_FAILED: {
            f = env->GetStaticFieldID(detailStateKlass, "FAILED", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_BLOCKED: {
            f = env->GetStaticFieldID(detailStateKlass, "BLOCKED", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_VERIFYING_POOR_LINK: {
            f = env->GetStaticFieldID(detailStateKlass, "VERIFYING_POOR_LINK", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        case Elastos::Droid::Net::NetworkInfoDetailedState_CAPTIVE_PORTAL_CHECK: {
            f = env->GetStaticFieldID(detailStateKlass, "CAPTIVE_PORTAL_CHECK", "Landroid/net/NetworkInfo$DetailedState;");
            break;
        }
        default: {
            assert(0);
            break;
        }
    }
    CheckErrorAndLog(env, "GetStaticFieldID: %d!\n", __LINE__);

    jobject jdetailState = env->GetStaticObjectField(detailStateKlass, f);
    CheckErrorAndLog(env, "GetJavaNetworkInfo GetStaticObjectField: %d!\n", __LINE__);

    f = env->GetFieldID(netInfoKlass, "mDetailedState", "Landroid/net/NetworkInfo$DetailedState;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: NetworkInfo$DetailedState : %d!\n", __LINE__);

    env->SetObjectField(jinfo, f, jdetailState);
    ElUtil::CheckErrorAndLog(env, "GetJavaNetworkInfo SetObjectField: jdetailState : %d!\n", __LINE__);
    env->DeleteLocalRef(jdetailState);
    env->DeleteLocalRef(detailStateKlass);

    Boolean booleanTemp;
    info->IsFailover(&booleanTemp);
    ElUtil::SetJavaBoolField(env, netInfoKlass, jinfo, booleanTemp, "mIsFailover", "GetJavaNetworkInfo");

    info->IsAvailable(&booleanTemp);
    ElUtil::SetJavaBoolField(env, netInfoKlass, jinfo, booleanTemp, "mIsAvailable", "GetJavaNetworkInfo");

    info->IsRoaming(&booleanTemp);
    ElUtil::SetJavaBoolField(env, netInfoKlass, jinfo, booleanTemp, "mIsRoaming", "GetJavaNetworkInfo");

    info->IsConnectedToProvisioningNetwork(&booleanTemp);
    ElUtil::SetJavaBoolField(env, netInfoKlass, jinfo, booleanTemp, "mIsConnectedToProvisioningNetwork", "GetJavaNetworkInfo");

    String reason;
    info->GetReason(&reason);
    if (!reason.IsNull()) {
        jstring jreason = GetJavaString(env, reason);

        f = env->GetFieldID(netInfoKlass, "mReason", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaNetworkInfo GetFieldID: mReason : %d!\n", __LINE__);

        env->SetObjectField(jinfo, f, jreason);
        CheckErrorAndLog(env, "GetJavaNetworkInfo SetObjectField: jreason : %d!\n", __LINE__);
        env->DeleteLocalRef(jreason);
    }

    String extraInfo;
    info->GetExtraInfo(&extraInfo);
    if (!extraInfo.IsNull()) {
        jstring jextraInfo = GetJavaString(env, extraInfo);

        f = env->GetFieldID(netInfoKlass, "mExtraInfo", "Ljava/lang/String;");
        CheckErrorAndLog(env, "GetJavaNetworkInfo GetFieldID: mExtraInfo : %d!\n", __LINE__);

        env->SetObjectField(jinfo, f, jextraInfo);
        CheckErrorAndLog(env, "GetJavaNetworkInfo SetObjectField: jextraInfo : %d!\n", __LINE__);
        env->DeleteLocalRef(jextraInfo);
    }

    env->DeleteLocalRef(netInfoKlass);

    return jinfo;
}

jobject ElUtil::GetJavaStorageVolume(
    /* [in] */ JNIEnv* env,
    /* [in] */ IStorageVolume* volume)
{
    if (env == NULL || volume == NULL) {
        ALOGE("GetJavaStorageVolume: Invalid argumenet!");
        return NULL;
    }

    jclass svolKlass = env->FindClass("android/os/storage/StorageVolume");
    ElUtil::CheckErrorAndLog(env, "GetJavaStorageVolume FindClass: StorageVolume : %d!\n", __LINE__);

    String path;
    volume->GetPath(&path);
    jstring jpath = ElUtil::GetJavaString(env, path);

    jclass fileKlass = env->FindClass("java/io/File");
    ElUtil::CheckErrorAndLog(env, "GetJavaStorageVolume FindClass: File : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(fileKlass, "<init>", "(Ljava/lang/String;)V");
    ElUtil::CheckErrorAndLog(env, "GetJavaStorageVolume GetMethodID: UserInfo : %d!\n", __LINE__);

    jobject jfile = env->NewObject(fileKlass, m, jpath);
    ElUtil::CheckErrorAndLog(env, "GetJavaStorageVolume NewObject: File : %d!\n", __LINE__);
    env->DeleteLocalRef(jpath);
    env->DeleteLocalRef(fileKlass);

    Int32 descriptionId;
    volume->GetDescriptionId(&descriptionId);

    Boolean primary;
    volume->IsPrimary(&primary);

    Boolean removable;
    volume->IsRemovable(&removable);

    Boolean emulated;
    volume->IsEmulated(&emulated);

    Int32 mtpReserveSpace;
    volume->GetMtpReserveSpace(&mtpReserveSpace);

    Boolean allowMassStorage;
    volume->AllowMassStorage(&allowMassStorage);

    m = env->GetMethodID(svolKlass, "<init>", "(Ljava/io/File;IZZZIZJLandroid/os/UserHandle;)V");
    ElUtil::CheckErrorAndLog(env, "GetJavaStorageVolume GetMethodID: StorageVolume : %d!\n", __LINE__);

    Int64 maxFileSize;
    volume->GetMaxFileSize(&maxFileSize);

    AutoPtr<IUserHandle> owner;
    volume->GetOwner((IUserHandle**)&owner);
    jobject jowner = NULL;
    if (owner != NULL) {
        jowner = ElUtil::GetJavaUserHandle(env, owner);
    }

    jobject jvolume = env->NewObject(svolKlass, m, jfile, descriptionId, (jboolean)primary, (jboolean)removable,
            (jboolean)emulated, mtpReserveSpace, (jboolean)allowMassStorage, maxFileSize, jowner);
    ElUtil::CheckErrorAndLog(env, "GetJavaStorageVolume NewObject: StorageVolume : %d!\n", __LINE__);

    env->DeleteLocalRef(jowner);

    Int32 storageId;
    volume->GetStorageId(&storageId);
    ElUtil::SetJavaIntField(env, svolKlass, jvolume, storageId, "mStorageId", "GetJavaStorageVolume");

    String uuid;
    volume->GetUuid(&uuid);
    ElUtil::SetJavaStringField(env, svolKlass, jvolume, uuid, "mUuid", "GetJavaStorageVolume");

    String userLabel;
    volume->GetUserLabel(&userLabel);
    ElUtil::SetJavaStringField(env, svolKlass, jvolume, userLabel, "mUserLabel", "GetJavaStorageVolume");

    String state;
    volume->GetState(&state);
    ElUtil::SetJavaStringField(env, svolKlass, jvolume, state, "mState", "GetJavaStorageVolume");

    env->DeleteLocalRef(svolKlass);
    env->DeleteLocalRef(jfile);
    return jvolume;
}

jobject ElUtil::GetJavaUserHandle(
    /* [in] */ JNIEnv* env,
    /* [in] */ IUserHandle* uHandle)
{
    if (env == NULL || uHandle == NULL) {
        ALOGE("GetJavaUserHandle: Invalid argumenet!");
        return NULL;
    }

    jclass hdlKlass = env->FindClass("android/os/UserHandle");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserHandle FindClass: UserHandle : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(hdlKlass, "<init>", "(I)V");
    ElUtil::CheckErrorAndLog(env, "GetJavaUserHandle GetMethodID: UserHandle : %d!\n", __LINE__);

    Int32 identifier;
    uHandle->GetIdentifier(&identifier);

    jobject jhandle = env->NewObject(hdlKlass, m, (jint)identifier);
    ElUtil::CheckErrorAndLog(env, "GetJavaUserHandle NewObject: UserHandle : %d!\n", __LINE__);

    env->DeleteLocalRef(hdlKlass);
    return jhandle;
}

bool ElUtil::ToElIServiceConnection(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconnection,
    /* [out] */ IIServiceConnection** connection)
{
    if (env == NULL || jconnection == NULL || connection == NULL) {
        ALOGE("ToElIServiceConnection: Invalid argumenet!");
        return false;
    }

    jclass connProxyClass = env->FindClass("android/app/ElIServiceConnectionProxy");
    ElUtil::CheckErrorAndLog(env, "ToElIServiceConnection FindClass: ElIServiceConnectionProxy : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(connProxyClass, "<init>", "(Landroid/app/IServiceConnection;)V");
    ElUtil::CheckErrorAndLog(env, "ToElIServiceConnection GetMethodID: ElIServiceConnectionProxy : %d!\n", __LINE__);

    jobject jconnProxy = env->NewObject(connProxyClass, m, jconnection);
    ElUtil::CheckErrorAndLog(env, "ToElIServiceConnection NewObject: ElIServiceConnectionProxy : %d!\n", __LINE__);
    env->DeleteLocalRef(connProxyClass);

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jconnProxy);
    env->DeleteLocalRef(jconnProxy);

    if (NOERROR != CIServiceConnectionNative::New((Handle64)jvm, (Handle64)jInstance, connection)) {
        ALOGE("ToElIServiceConnection new CIServiceConnectionNative fail!\n");
        return false;
    }

    return true;
}

jobject ElUtil::GetJavaConfigurationInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IConfigurationInfo* cfgInfo)
{
    if (env == NULL || cfgInfo == NULL) {
        ALOGE("GetJavaConfigurationInfo: Invalid argumenet!");
        return NULL;
    }

    jobject jcfgInfo = NULL;

    jclass configInfoKlass = env->FindClass("android/content/pm/ConfigurationInfo");
    CheckErrorAndLog(env, "GetJavaConfiguration Fail FindClass: ConfigurationInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(configInfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaConfiguration Fail GetMethodID: ConfigurationInfo : %d!\n", __LINE__);

    jcfgInfo = env->NewObject(configInfoKlass, m);
    CheckErrorAndLog(env, "GetJavaConfiguration Fail NewObject: ConfigurationInfo : %d!\n", __LINE__);

    Int32 tempInt;
    cfgInfo->GetReqTouchScreen(&tempInt);
    ElUtil::SetJavaIntField(env, configInfoKlass, jcfgInfo, tempInt, "reqTouchScreen", "GetJavaConfigurationInfo");

    cfgInfo->GetReqKeyboardType(&tempInt);
    ElUtil::SetJavaIntField(env, configInfoKlass, jcfgInfo, tempInt, "reqKeyboardType", "GetJavaConfigurationInfo");

    cfgInfo->GetReqNavigation(&tempInt);
    ElUtil::SetJavaIntField(env, configInfoKlass, jcfgInfo, tempInt, "reqNavigation", "GetJavaConfigurationInfo");

    cfgInfo->GetReqInputFeatures(&tempInt);
    ElUtil::SetJavaIntField(env, configInfoKlass, jcfgInfo, tempInt, "reqInputFeatures", "GetJavaConfigurationInfo");

    cfgInfo->GetReqGlEsVersion(&tempInt);
    ElUtil::SetJavaIntField(env, configInfoKlass, jcfgInfo, tempInt, "reqGlEsVersion", "GetJavaConfigurationInfo");

    env->DeleteLocalRef(configInfoKlass);
    return jcfgInfo;
}

void ElUtil::SetJavaRect(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jrect,
    /* [in] */ IRect* rect)
{
    if (env == NULL || jrect == NULL || rect == NULL) {
        ALOGD("SetJavaRect() Invalid arguments!");
        return;
    }

    jclass rectKlass = env->FindClass("android/graphics/Rect");
    CheckErrorAndLog(env, "SetJavaRect FindClass: Rect : %d!\n", __LINE__);

    Int32 tempInt;
    rect->GetLeft(&tempInt);
    ElUtil::SetJavaIntField(env, rectKlass, jrect, tempInt, "left", "SetJavaRect");
    rect->GetTop(&tempInt);
    ElUtil::SetJavaIntField(env, rectKlass, jrect, tempInt, "top", "SetJavaRect");
    rect->GetRight(&tempInt);
    ElUtil::SetJavaIntField(env, rectKlass, jrect, tempInt, "right", "SetJavaRect");
    rect->GetBottom(&tempInt);
    ElUtil::SetJavaIntField(env, rectKlass, jrect, tempInt, "bottom", "SetJavaRect");
    env->DeleteLocalRef(rectKlass);
}

bool ElUtil::ToElWorkSource(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jws,
    /* [out] */ IWorkSource** ws)
{
    if (env == NULL || jws == NULL || ws == NULL) {
        ALOGE("ToElWorkSource: Invalid argumenet!");
        return false;
    }

    if (NOERROR != CWorkSource::New(ws)) {
        ALOGE("ToElWorkSource: create CWorkSource fail!");
        return false;
    }


    jclass wsKlass = env->FindClass("android/os/WorkSource");
    CheckErrorAndLog(env, "FindClass: WorkSource : %d!\n", __LINE__);

    Int32 num = ElUtil::GetJavaIntField(env, wsKlass, jws, "mNum", "ToElWorkSource");
    if (num > 0) {
        jmethodID m = env->GetMethodID(wsKlass, "get", "(I)I");
        CheckErrorAndLog(env, "ToElWorkSource GetMethodID: get : %d!\n", __LINE__);

        jmethodID mgetName = env->GetMethodID(wsKlass, "getName", "(I)Ljava/lang/String;");
        CheckErrorAndLog(env, "ToElWorkSource GetMethodID: get : %d!\n", __LINE__);

        for (jint i = 0; i < num; i++) {
            jint uid = env->CallIntMethod(jws, m, i);
            CheckErrorAndLog(env, "ToElWorkSource CallIntMethod: get : %d!\n", __LINE__);

            jstring jname = (jstring)env->CallObjectMethod(jws, mgetName, i);
            Boolean res;
            (*ws)->Add((Int32)uid, ToElString(env, jname), &res);
            env->DeleteLocalRef(jname);
        }
    }
    env->DeleteLocalRef(wsKlass);

    return true;
}

jobject ElUtil::GetJavaRunningAppProcessInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityManagerRunningAppProcessInfo* procInfo)
{
    if (env == NULL || procInfo == NULL) {
        ALOGE("GetJavaConfigurationInfo: Invalid argumenet!");
        return NULL;
    }

    jobject jprocInfo = NULL;

    jclass procInfoKlass = env->FindClass("android/app/ActivityManager$RunningAppProcessInfo");
    CheckErrorAndLog(env, "GetJavaRunningAppProcessInfo FindClass: RunningAppProcessInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(procInfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaRunningAppProcessInfo GetMethodID: RunningAppProcessInfo : %d!\n", __LINE__);

    jprocInfo = env->NewObject(procInfoKlass, m);
    CheckErrorAndLog(env, "GetJavaRunningAppProcessInfo NewObject: RunningAppProcessInfo : %d!\n", __LINE__);

    GetJavaRunningAppProcessInfo(env, jprocInfo, procInfo);

    env->DeleteLocalRef(procInfoKlass);
    return jprocInfo;
}

void ElUtil::GetJavaRunningAppProcessInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jprocInfo,
    /* [in] */ IActivityManagerRunningAppProcessInfo* procInfo)
{
    if (env == NULL || jprocInfo== NULL || procInfo == NULL) {
        ALOGE("GetJavaConfigurationInfo: Invalid argumenet!");
        return;
    }

    jclass procInfoKlass = env->FindClass("android/app/ActivityManager$RunningAppProcessInfo");
    CheckErrorAndLog(env, "GetJavaRunningAppProcessInfo FindClass: RunningAppProcessInfo : %d!\n", __LINE__);

    String processName;
    procInfo->GetProcessName(&processName);
    ElUtil::SetJavaStringField(env, procInfoKlass, jprocInfo, processName, "processName", "GetJavaRunningAppProcessInfo");

    Int32 tempInt;
    procInfo->GetPid(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "pid", "GetJavaRunningAppProcessInfo");

    procInfo->GetUid(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "uid", "GetJavaRunningAppProcessInfo");

    AutoPtr<ArrayOf<String> > pkgList;
    procInfo->GetPkgList((ArrayOf<String>**)&pkgList);
    if (pkgList != NULL) {
        jobjectArray jpkgList = ElUtil::GetJavaStringArray(env, pkgList);

        if (jpkgList != NULL) {
            jfieldID f = env->GetFieldID(procInfoKlass, "pkgList", "[Ljava/lang/String;");
            CheckErrorAndLog(env, "GetFieldID: pkgList : %d!\n", __LINE__);

            env->SetObjectField(jprocInfo, f, jpkgList);
            CheckErrorAndLog(env, "SetObjectField: pkgList : %d!\n", __LINE__);

            env->DeleteLocalRef(jpkgList);
        }
        else {
            ALOGE("GetJavaRunningAppProcessInfo: jpkgList is NULL!");
        }
    }

    procInfo->GetFlags(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "flags", "GetJavaRunningAppProcessInfo");

    procInfo->GetLastTrimLevel(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "lastTrimLevel", "GetJavaRunningAppProcessInfo");

    procInfo->GetImportance(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "importance", "GetJavaRunningAppProcessInfo");

    procInfo->GetLru(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "lru", "GetJavaRunningAppProcessInfo");

    procInfo->GetImportanceReasonCode(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "importanceReasonCode", "GetJavaRunningAppProcessInfo");

    procInfo->GetImportanceReasonPid(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "importanceReasonPid", "GetJavaRunningAppProcessInfo");

    AutoPtr<IComponentName> importanceReasonComponent;
    procInfo->GetImportanceReasonComponent((IComponentName**)&importanceReasonComponent);
    if (importanceReasonComponent != NULL) {
        jobject jimportanceReasonComponent = ElUtil::GetJavaComponentName(env, importanceReasonComponent);
        if (jimportanceReasonComponent != NULL) {
            jfieldID f = env->GetFieldID(procInfoKlass, "importanceReasonComponent", "Landroid/content/ComponentName;");
            CheckErrorAndLog(env, "GetJavaRunningAppProcessInfo GetFieldID: ComponentName : %d!\n", __LINE__);

            env->SetObjectField(jprocInfo, f, jimportanceReasonComponent);
            CheckErrorAndLog(env, "GetJavaRunningAppProcessInfo SetObjectField: jimportanceReasonComponent : %d!\n", __LINE__);

            env->DeleteLocalRef(jimportanceReasonComponent);
        }
    }

    procInfo->GetImportanceReasonImportance(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "importanceReasonImportance", "GetJavaRunningAppProcessInfo");

    procInfo->GetProcessState(&tempInt);
    ElUtil::SetJavaIntField(env, procInfoKlass, jprocInfo, tempInt, "processState", "GetJavaRunningAppProcessInfo");

    env->DeleteLocalRef(procInfoKlass);
}

bool ElUtil::ToElRemoteViews(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jremoteview,
    /* [in] */ IRemoteViews** remoteview)
{
    if (env == NULL || jremoteview == NULL || remoteview == NULL) {
        ALOGE("ToElRemoteViews: Invalid argumenet!");
        return FALSE;
    }

    jclass remoteviewClass = env->FindClass("android/widget/RemoteViews");

    jfieldID mLandscapeField = env->GetFieldID(remoteviewClass, "mLandscape", "Landroid/widget/RemoteViews;");
    CheckErrorAndLog(env, "%s: Fail get RemoteViews fieldid: %s : %d!\n", "ToElRemoteViews", "mLandscape", __LINE__);
    jobject jmLandscape = env->GetObjectField(jremoteview, mLandscapeField);
    CheckErrorAndLog(env, "%s: Fail get RemoteViews field: %s : %d!\n", "ToElRemoteViews", "mLandscape", __LINE__);

    jfieldID mPortraitField = env->GetFieldID(remoteviewClass, "mPortrait", "Landroid/widget/RemoteViews;");
    CheckErrorAndLog(env, "%s: Fail get RemoteViews fieldid: %s : %d!\n", "ToElRemoteViews", "mPortrait", __LINE__);
    jobject jmPortrait = env->GetObjectField(jremoteview, mPortraitField);
    CheckErrorAndLog(env, "%s: Fail get RemoteViews field: %s : %d!\n", "ToElRemoteViews", "mPortrait", __LINE__);

    jfieldID mApplicationField = env->GetFieldID(remoteviewClass, "mApplication", "Landroid/content/pm/ApplicationInfo;");
    CheckErrorAndLog(env, "%s: Fail get RemoteViews fieldid: %s : %d!\n", "ToElRemoteViews", "mApplication ", __LINE__);
    jobject jmApplication = env->GetObjectField(jremoteview, mApplicationField);
    CheckErrorAndLog(env, "%s: Fail get RemoteViews field: %s : %d!\n", "ToElRemoteViews", "mApplication ", __LINE__);

    if (jmLandscape != NULL && jmPortrait != NULL) {
        AutoPtr<IRemoteViews> landscape, portrait;
        ToElRemoteViews(env, jmLandscape, (IRemoteViews**)&landscape);
        ToElRemoteViews(env, jmPortrait, (IRemoteViews**)&portrait);
        CRemoteViews::New(landscape, portrait, remoteview);
        env->DeleteLocalRef(remoteviewClass);
        env->DeleteLocalRef(jmLandscape);
        env->DeleteLocalRef(jmPortrait);
        return TRUE;
    }

    AutoPtr<IApplicationInfo> application;
    if (!ToElApplicationInfo(env, jmApplication, (IApplicationInfo**)&application)) {
        ALOGE("ToElRemoteViews() ToElApplicationInfo fail!");
    }
    Int32 mLayoutId = GetJavaIntField(env, remoteviewClass, jremoteview, "mLayoutId", "ToElRemoteViews");
    CRemoteViews::New(application, mLayoutId, remoteview);

    jboolean mIsRoot = GetJavaBoolField(env, remoteviewClass, jremoteview, "mIsRoot", "ToElRemoteViews");

    if (!mIsRoot) {
        (*remoteview)->SetNotRoot();
    }

    jboolean isWidgetCollectionChild = GetJavaBoolField(env, remoteviewClass, jremoteview, "mIsWidgetCollectionChild", "ToElRemoteViews");
    (*remoteview)->SetIsWidgetCollectionChild(isWidgetCollectionChild);

    // Process actions
    jfieldID mActionField = env->GetFieldID(remoteviewClass, "mActions", "Ljava/util/ArrayList;");
    CheckErrorAndLog(env, "%s: Fail get ArrayList fieldid: %s : %d!\n", "ToElRemoteViews", "mActions", __LINE__);
    jobject jactions = env->GetObjectField(jremoteview, mActionField);
    CheckErrorAndLog(env, "%s: Fail get ArrayList field: %s : %d!\n", "ToElRemoteViews", "mActions", __LINE__);

    if (jactions != NULL) {
        jclass alistKlass = env->FindClass("java/util/ArrayList");
        CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID mSize = env->GetMethodID(alistKlass, "size", "()I");
        CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

        jmethodID mGet = env->GetMethodID(alistKlass, "get", "(I)Ljava/lang/Object;");
        CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);
        env->DeleteLocalRef(alistKlass);

        jint jsize = env->CallIntMethod(jactions, mSize);
        CheckErrorAndLog(env, "CallIntMethod: mActions : %d!\n", __LINE__);
        if (jsize > 0) {
            // ALOGD("ElUtil::ToElRemoteViews action size = :%d", jsize);
            for (jint i = 0; i < jsize; i++) {
                jobject jaction = env->CallObjectMethod(jactions, mGet, i);
                CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);
                // Get TAG field to checkout action type
                jclass klass = env->GetObjectClass(jaction);
                CheckErrorAndLog(env, "%s: Fail get object class. %s%d!\n", "ToElRemoteViews", "", __LINE__);
                jfieldID fTag = env->GetStaticFieldID(klass, "TAG", "I");
                CheckErrorAndLog(env, "%s: Fail GetStaticFieldID. %s%d!\n", "ToElRemoteViews", "TAG", __LINE__);
                Int32 tag = env->GetStaticIntField(klass, fTag);
                CheckErrorAndLog(env, "%s: Fail GetStaticIntField. %s%d!\n", "ToElRemoteViews", "TAG", __LINE__);
                Int32 viewId = GetJavaIntField(env, klass, jaction, "viewId", "ToElRemoteViews");

                switch(tag) {
                    case 1: //SetOnClickPendingIntent.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a SetOnClickPendingIntent action.");
                        jfieldID f = env->GetFieldID(klass, "pendingIntent", "Landroid/app/PendingIntent;");
                        ElUtil::CheckErrorAndLog(env, "GetFieldID: pendingIntent : %d!\n", __LINE__);
                        jobject jpendingIntent = env->GetObjectField(jaction, f);
                        ElUtil::CheckErrorAndLog(env, "GetObjectField: pendingIntent : %d!\n", __LINE__);
                        AutoPtr<IPendingIntent> pendingIntent;
                        ToElPendingIntent(env, jpendingIntent, (IPendingIntent**)&pendingIntent);
                        env->DeleteLocalRef(jpendingIntent);

                        (*remoteview)->SetOnClickPendingIntent(viewId, pendingIntent);
                        break;
                    }
                    case 3: //SetDrawableParameters.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a SetDrawableParameters action.");
                        Boolean targetBackground = (Boolean)GetJavaBoolField(env, klass, jaction, "targetBackground", "ToElRemoteViews");
                        Int32 alpha = GetJavaIntField(env, klass, jaction, "alpha", "ToElRemoteViews");
                        Int32 colorFilter = GetJavaIntField(env, klass, jaction, "colorFilter", "ToElRemoteViews");
                        Int32 level = GetJavaIntField(env, klass, jaction, "level", "ToElRemoteViews");

                        // Translate mode
                        jfieldID modeField = env->GetFieldID(klass, "filterMode", "Landroid/graphics/PorterDuff$Mode;");
                        ElUtil::CheckErrorAndLog(env, "GetFieldID: filterMode : %d!\n", __LINE__);
                        jobject jmode = env->GetObjectField(jaction, modeField);
                        ElUtil::CheckErrorAndLog(env, "GetObjectField: filterMode : %d!\n", __LINE__);

                        Int32 mode = -1;
                        if (jmode != NULL) {
                            jclass c = env->GetObjectClass(jmode);
                            mode = GetJavaIntField(env, c, jmode, "nativeInt", "ToElRemoteViews");
                            ALOGE("ToElRemoteViews, SetDrawableParameters, PorterDuff.Mode mode = %d", mode);
                            env->DeleteLocalRef(jmode);
                            env->DeleteLocalRef(c);
                        }
                        else{
                            // ALOGE("ToElRemoteViews, SetDrawableParameters, PorterDuff.Mode mode = NULL");
                        }

                        (*remoteview)->SetDrawableParameters(viewId, targetBackground, alpha, colorFilter, PorterDuffMode(mode), level);
                        break;
                    }
                    case 2: //ReflectionAction.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a ReflectionAction action.");
                        String methodName = GetJavaStringField(env, klass, jaction, "methodName", "ToElRemoteViews");
                        if (!methodName.IsNullOrEmpty()) {
                            methodName = methodName.ToUpperCase(0, 1);
                        }
                        Int32 type = GetJavaIntField(env, klass, jaction, "type", "ToElRemoteViews");
                        jfieldID f = env->GetFieldID(klass, "value", "Ljava/lang/Object;");
                        ElUtil::CheckErrorAndLog(env, "GetFieldID: value : %d!\n", __LINE__);
                        jobject jvalue = env->GetObjectField(jaction, f);
                        ElUtil::CheckErrorAndLog(env, "GetObjectField: value : %d!\n", __LINE__);
                        jclass c = NULL;
                        if (jvalue != NULL) {
                            c = env->GetObjectClass(jvalue);
                            ElUtil::CheckErrorAndLog(env, "GetObjectClass: jvalue : %d!\n", __LINE__);
                        }
                        // int viewId, String methodName, int type, Object value

                        switch (type) {
                            case 1: // BOOLEAN
                            {
                                Boolean value = (Boolean)GetJavaBoolField(env, c, jvalue, "value", "ToElRemoteViews");
                                (*remoteview)->SetBoolean(viewId, methodName, value);
                                break;
                            }
                            case 2: // BYTE
                            {
                                Byte value = (Byte)GetJavabyteField(env, c, jvalue, "value", "ToElRemoteViews");
                                (*remoteview)->SetByte(viewId, methodName, value);
                                break;
                            }
                            case 3: // SHORT
                            {
                                Int16 value = (Int16)GetJavaShortField(env, c, jvalue, "value", "ToElRemoteViews");
                                (*remoteview)->SetInt16(viewId, methodName, value);
                                break;
                            }
                            case 4: // INT
                            {
                                Int32 value = (Int32)GetJavaIntField(env, c, jvalue, "value", "ToElRemoteViews");
                                (*remoteview)->SetInt32(viewId, methodName, value);
                                break;
                            }
                            case 5: // LONG
                            {
                                Int64 value = (Int64)GetJavalongField(env, c, jvalue, "value", "ToElRemoteViews");
                                (*remoteview)->SetInt64(viewId, methodName, value);
                                break;
                            }
                            case 6: // FLOAT
                            {
                                Float value = (Float)GetJavafloatField(env, c, jvalue, "value", "ToElRemoteViews");
                                (*remoteview)->SetFloat(viewId, methodName, value);
                                break;
                            }
                            case 7: // DOUBLE
                            {
                                Double value = (Double)GetJavadoubleField(env, c, jvalue, "value", "ToElRemoteViews");
                                (*remoteview)->SetDouble(viewId, methodName, value);
                                break;
                            }
                            case 8: // CHAR
                            {
                                Char32 value = (Char32)GetJavaCharField(env, c, jvalue, "value", "ToElRemoteViews");
                                (*remoteview)->SetChar(viewId, methodName, value);
                                break;
                            }
                            case 9: // STRING
                            {
                                String value = ToElString(env, (jstring)jvalue);
                                (*remoteview)->SetString(viewId, methodName, value);
                                break;
                            }
                            case 10: // CHAR_SEQUENCE
                            {
                                AutoPtr<ICharSequence> value;
                                ToElCharSequence(env, jvalue, (ICharSequence**)&value);
                                (*remoteview)->SetCharSequence(viewId, methodName, value);
                                break;
                            }
                            case 11: // URI
                            {
                                AutoPtr<IUri> value;
                                if (jvalue != NULL)
                                    ToElUri(env, jvalue, (IUri**)&value);
                                (*remoteview)->SetUri(viewId, methodName, value);
                                break;
                            }
                            // BITMAP actions are never stored in the list of actions. They are only used locally
                            // to implement BitmapReflectionAction, which eliminates duplicates using BitmapCache.
                            case 12: // BITMAP
                            {
                                AutoPtr<IBitmap> value;
                                if (jvalue != NULL)
                                    ToElBitmap(env, jvalue, (IBitmap**)&value);
                                (*remoteview)->SetBitmap(viewId, methodName, value);
                                break;
                            }
                            case 13: // BUNDLE
                            {
                                AutoPtr<IBundle> value;
                                if (jvalue != NULL)
                                    ToElBundle(env, jvalue, (IBundle**)&value);
                                (*remoteview)->SetBundle(viewId, methodName, value);
                                break;
                            }
                            case 14: // INTENT
                            {
                                AutoPtr<IIntent> value;
                                if (jvalue != NULL)
                                    ToElIntent(env, jvalue, (IIntent**)&value);
                                (*remoteview)->SetIntent(viewId, methodName, value);
                                break;
                            }
                            default:
                            {
                                break;
                            }
                        }
                        env->DeleteLocalRef(jvalue);
                        env->DeleteLocalRef(c);
                        break;
                    }
                    case 4: // ViewGroupAction.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a ViewGroupAction action.");
                        jfieldID f = env->GetFieldID(klass, "nestedViews", "Landroid/widget/RemoteViews;");
                        ElUtil::CheckErrorAndLog(env, "GetFieldID: nestedViews : %d!\n", __LINE__);
                        jobject jnestedViews = env->GetObjectField(jaction, f);
                        ElUtil::CheckErrorAndLog(env, "GetObjectField: nestedViews : %d!\n", __LINE__);

                        AutoPtr<IRemoteViews> nestedViews;
                        if (jnestedViews != NULL) {
                            ToElRemoteViews(env, jnestedViews, (IRemoteViews**)&nestedViews);
                            env->DeleteLocalRef(jnestedViews);
                        }
                        else{
                            ALOGW("ToElRemoteViews: found a ViewGroupAction action. jnestedViews is null");
                        }

                        if (nestedViews == NULL) {
                            (*remoteview)->RemoveAllViews(viewId);
                        }
                        else{
                            (*remoteview)->AddView(viewId, nestedViews);
                        }
                        break;
                    }
                    case 5: // ReflectionActionWithoutParams.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a ReflectionActionWithoutParams action.");
                        String methodName = GetJavaStringField(env, klass, jaction, "methodName", "ToElRemoteViews");
                        if (!methodName.IsNullOrEmpty()) {
                            methodName = methodName.ToUpperCase(0, 1);
                        }

                        if (methodName.Equals(String("showNext"))) {
                            (*remoteview)->ShowNext(viewId);
                        }
                        else if (methodName.Equals(String("showPrevious"))) {
                            (*remoteview)->ShowPrevious(viewId);
                        }

                        break;
                    }
                    case 6: // SetEmptyView.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a SetEmptyView action.");
                        Int32 emptyViewId = GetJavaIntField(env, klass, jaction, "emptyViewId", "ToElRemoteViews");
                        (*remoteview)->SetEmptyView(viewId, emptyViewId);
                        break;
                    }
                    case 8: // SetPendingIntentTemplate.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a SetPendingIntentTemplate action.");
                        jfieldID f = env->GetFieldID(klass, "pendingIntentTemplate", "Landroid/app/PendingIntent;");
                        ElUtil::CheckErrorAndLog(env, "GetFieldID: pendingIntentTemplate : %d!\n", __LINE__);
                        jobject jpendingIntent = env->GetObjectField(jaction, f);
                        ElUtil::CheckErrorAndLog(env, "GetObjectField: pendingIntentTemplate : %d!\n", __LINE__);
                        AutoPtr<IPendingIntent> pendingIntent;
                        ToElPendingIntent(env, jpendingIntent, (IPendingIntent**)&pendingIntent);
                        env->DeleteLocalRef(jpendingIntent);
                        (*remoteview)->SetPendingIntentTemplate(viewId, pendingIntent);
                        break;
                    }
                    case 9: // SetOnClickFillInIntent.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a SetOnClickFillInIntent action.");
                        jfieldID f = env->GetFieldID(klass, "fillInIntent", "Landroid/content/Intent;");
                        ElUtil::CheckErrorAndLog(env, "GetFieldID: fillInIntent : %d!\n", __LINE__);
                        jobject jintent = env->GetObjectField(jaction, f);
                        ElUtil::CheckErrorAndLog(env, "GetObjectField: fillInIntent : %d!\n", __LINE__);
                        AutoPtr<IIntent> intent;
                        ToElIntent(env, jintent, (IIntent**)&intent);
                        env->DeleteLocalRef(jintent);
                        (*remoteview)->SetOnClickFillInIntent(viewId, intent);
                        break;
                    }
                    case 10: // SetRemoteViewsAdapterIntent.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a SetRemoteViewsAdapterIntent action.");
                        jfieldID f = env->GetFieldID(klass, "intent", "Landroid/content/Intent;");
                        ElUtil::CheckErrorAndLog(env, "GetFieldID: intent : %d!\n", __LINE__);
                        jobject jintent = env->GetObjectField(jaction, f);
                        ElUtil::CheckErrorAndLog(env, "GetObjectField: intent : %d!\n", __LINE__);
                        AutoPtr<IIntent> intent;
                        ToElIntent(env, jintent, (IIntent**)&intent);
                        env->DeleteLocalRef(jintent);
                        (*remoteview)->SetRemoteAdapter(viewId, intent);
                        break;
                    }
                    case 11: // TextViewDrawableAction.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a TextViewDrawableAction action.");
                        Boolean isRelative = GetJavaBoolField(env, klass, jaction, "isRelative", "ToElRemoteViews");
                        Int32 d1 = GetJavaIntField(env, klass, jaction, "d1", "ToElRemoteViews");
                        Int32 d2 = GetJavaIntField(env, klass, jaction, "d2", "ToElRemoteViews");
                        Int32 d3 = GetJavaIntField(env, klass, jaction, "d3", "ToElRemoteViews");
                        Int32 d4 = GetJavaIntField(env, klass, jaction, "d4", "ToElRemoteViews");

                        if (isRelative) {
                            (*remoteview)->SetTextViewCompoundDrawablesRelative(viewId, d1, d2, d3, d4);
                        }
                        else{
                            (*remoteview)->SetTextViewCompoundDrawables(viewId, d1, d2, d3, d4);
                        }
                        break;
                    }
                    case 13: // TextViewSizeAction.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a TextViewSizeAction action.");
                        Int32 units = GetJavaIntField(env, klass, jaction, "units", "ToElRemoteViews");
                        Float size = (Float)GetJavafloatField(env, klass, jaction, "size", "ToElRemoteViews");
                        (*remoteview)->SetTextViewTextSize(viewId, units, size);
                        break;
                    }
                    case 14: // ViewPaddingAction.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a ViewPaddingAction action.");
                        Int32 left = GetJavaIntField(env, klass, jaction, "left", "ToElRemoteViews");
                        Int32 top = GetJavaIntField(env, klass, jaction, "top", "ToElRemoteViews");
                        Int32 right = GetJavaIntField(env, klass, jaction, "right", "ToElRemoteViews");
                        Int32 bottom = GetJavaIntField(env, klass, jaction, "bottom", "ToElRemoteViews");
                        (*remoteview)->SetViewPadding(viewId, left, top, right, bottom);
                        break;
                    }
                    case 12: // BitmapReflectionAction.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a BitmapReflectionAction action.");
                        String methodName = GetJavaStringField(env, klass, jaction, "methodName", "ToElRemoteViews");
                        if (!methodName.IsNullOrEmpty()) {
                            methodName = methodName.ToUpperCase(0, 1);
                        }

                        jfieldID f = env->GetFieldID(klass, "bitmap", "Landroid/graphics/Bitmap;");
                        ElUtil::CheckErrorAndLog(env, "GetFieldID: bitmap : %d!\n", __LINE__);
                        jobject jbitmap = env->GetObjectField(jaction, f);
                        ElUtil::CheckErrorAndLog(env, "GetObjectField: bitmap : %d!\n", __LINE__);
                        AutoPtr<IBitmap> bitmap;
                        ToElBitmap(env, jbitmap, (IBitmap**)&bitmap);
                        env->DeleteLocalRef(jbitmap);

                        (*remoteview)->SetBitmap(viewId, methodName, bitmap);
                        break;
                    }
                    case 15: // SetRemoteViewsAdapterList.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a SetRemoteViewsAdapterList action.");
                        Int32 viewTypeCount = GetJavaIntField(env, klass, jaction, "viewTypeCount", "ToElRemoteViews");

                        jfieldID f = env->GetFieldID(klass, "list", "Ljava/util/ArrayList;");
                        CheckErrorAndLog(env, "GetFieldID: list : %d!\n", __LINE__);
                        jobject jlist = env->GetObjectField(jaction, f);
                        CheckErrorAndLog(env, "GetObjectField: list : %d!\n", __LINE__);
                        AutoPtr<IArrayList> list;
                        if (jlist != NULL){
                            jint jsize = env->CallIntMethod(jlist, mSize);
                            CheckErrorAndLog(env, "CallIntMethod: list : %d!\n", __LINE__);
                            if (jsize > 0)
                                CParcelableList::New((IArrayList**)&list);
                            for (jint j = 0; j < jsize; j++) {
                                jobject jitem = env->CallObjectMethod(jlist, mGet, j);
                                CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);
                                AutoPtr<IRemoteViews> item;
                                if (ToElRemoteViews(env, jitem, (IRemoteViews**)&item)){
                                    list->Add(item);
                                }
                                else {
                                    ALOGE("ToElRemoteViews SetRemoteViewsAdapterList.list item %d to IRemoteViews failed", j);
                                }
                                env->DeleteLocalRef(jitem);
                            }
                            env->DeleteLocalRef(jlist);
                        }

                        (*remoteview)->SetRemoteAdapter(viewId, list, viewTypeCount);
                        break;
                    }
                    case 17: // TextViewDrawableColorFilterAction.TAG:
                    {
                        // ALOGD("ToElRemoteViews: found a TextViewDrawableColorFilterAction action.");
                        Boolean isRelative = GetJavaBoolField(env, klass, jaction, "isRelative", "ToElRemoteViews");
                        if (!isRelative)
                            ALOGE("ToElRemoteViews: TextViewDrawableColorFilterAction isRelative is FALSE!");
                        Int32 index = GetJavaIntField(env, klass, jaction, "index", "ToElRemoteViews");
                        Int32 color = GetJavaIntField(env, klass, jaction, "color", "ToElRemoteViews");

                        jfieldID modeField = env->GetFieldID(klass, "mode", "Landroid/graphics/PorterDuff$Mode;");
                        CheckErrorAndLog(env, "GetFieldID: mode : %d!\n", __LINE__);
                        jobject jmode = env->GetObjectField(jaction, modeField);
                        CheckErrorAndLog(env, "GetObjectField: mode : %d!\n", __LINE__);

                        Int32 mode = -1;
                        if (jmode != NULL) {
                            jclass c = env->GetObjectClass(jmode);
                            mode = GetJavaIntField(env, c, jmode, "nativeInt", "ToElRemoteViews");
                            env->DeleteLocalRef(c);
                            env->DeleteLocalRef(jmode);
                            ALOGE("ToElRemoteViews, TextViewDrawableColorFilterAction, PorterDuff.Mode mode = %d", mode);
                        }
                        else {
                            ALOGE("ToElRemoteViews, TextViewDrawableColorFilterAction, PorterDuff.Mode mode = NULL");
                        }

                        (*remoteview)->SetTextViewCompoundDrawablesRelativeColorFilter(viewId, index, color, PorterDuffMode(mode));
                        break;
                    }
                    default:
                        break;
                        // throw new ActionException("Tag " + tag + " not found");
                }

                env->DeleteLocalRef(jaction);
                env->DeleteLocalRef(klass);
            }
        }

        env->DeleteLocalRef(jactions);
    }
    else{
        ALOGW("ToElRemoteViews, mActions is null");
    }

    env->DeleteLocalRef(remoteviewClass);

    return TRUE;
}

jobject ElUtil::GetJavaRemoteViews(
    /* [in] */ JNIEnv* env,
    /* [in] */ IRemoteViews* obj)
{
    if (env == NULL || obj == NULL) {
        ALOGW("GetJavaRemoteViews: Invalid argumenet!");
        return NULL;
    }

    AutoPtr<IParcel> source;
    CParcel::New((IParcel**)&source);

    AutoPtr<IParcelable> parcelable = IParcelable::Probe(obj);
    assert(parcelable != NULL);
    parcelable->WriteToParcel(source);

    // Try to read the values to init java remoteviews
    source->SetDataPosition(0);
    Int32 mode = 0;
    source->ReadInt32(&mode);

    if (mode != 0) {
        ALOGE("GetJavaRemoteViews() read mode = %d", mode);
        assert(0 && "TODO");
        return NULL;
    }

    // Read bitmapcache
    AutoPtr<IBitmapCache> bitmapCache;
    CRemoteViewsBitmapCache::New(source, (IBitmapCache**)&bitmapCache);

    AutoPtr<IApplicationInfo> application;
    Int32 layoutId = 0;
    source->ReadInterfacePtr((Handle32*)&application);
    source->ReadInt32(&layoutId);

    jobject japplication = GetJavaApplicationInfo(env, application);

    jclass klass = env->FindClass("android/widget/RemoteViews");
    CheckErrorAndLog(env, "GetJavaRemoteViews Fail FindClass: RemoteViews : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(Landroid/content/pm/ApplicationInfo;I)V");
    CheckErrorAndLog(env, "GetJavaRemoteViews Fail GetMethodID: RemoteViews() : %d!\n", __LINE__);

    jobject jremoteview = env->NewObject(klass, m, japplication, (jint)layoutId);
    CheckErrorAndLog(env, "GetJavaRemoteViews Fail NewObject: RemoteViews : %d!\n", __LINE__);

    env->DeleteLocalRef(japplication);

    Int32 tmp;
    source->ReadInt32(&tmp);
    // Set bool field mIsWidgetCollectionChild
    SetJavaBoolField(env, klass, jremoteview, tmp == 1, "mIsWidgetCollectionChild", "GetJavaRemoteViews");

    Int32 count = 0;
    source->ReadInt32(&count);
    // ALOGD("GetJavaRemoteViews(), read action count = %d", count);
    if (count > 0) {
        for (Int32 i = 0; i < count; i++) {
            Int32 tag = 0;
            source->ReadInt32(&tag);
            switch (tag) {
                case 1: // RemoteViewsSetOnClickPendingIntent::TAG:
                {
                    // ALOGD("GetJavaRemoteViews(), read SetOnClickPendingIntent action");
                    Int32 mViewId = 0;
                    source->ReadInt32(&mViewId);
                    AutoPtr<IInterface> item;
                    AutoPtr<IPendingIntent> pendingIntent;
                    Int32 temp = 0;
                    source->ReadInt32(&temp);

                    if (temp == 1) {
                        source->ReadInterfacePtr((Handle32*)&item);
                    }

                    if (item != NULL) {
                        pendingIntent = IPendingIntent::Probe(item);
                        jobject jpendingIntent = GetJavaPendingIntent(env, pendingIntent);
                        jmethodID method = env->GetMethodID(klass, "setOnClickPendingIntent", "(ILandroid/app/PendingIntent;)V");
                        CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setOnClickPendingIntent: %d!\n", __LINE__);
                        env->CallVoidMethod(jremoteview, method, (jint)mViewId, jpendingIntent);
                        CheckErrorAndLog(env, "GetJavaRemoteViews: call method setOnClickPendingIntent: %d!\n", __LINE__);
                        env->DeleteLocalRef(jpendingIntent);
                    }
                    break;
                }
                case 3: // RemoteViewsSetDrawableParameters::TAG:
                {
                    // ALOGD("GetJavaRemoteViews(), read setDrawableParameters action");
                    Int32 mViewId = 0;
                    Int32 mAlpha = 0;
                    Int32 mColorFilter = 0;
                    Int32 mLevel = 0;

                    source->ReadInt32(&mViewId);
                    Int32 background = 0;
                    source->ReadInt32(&background);
                    Boolean mTargetBackground = background != 0;
                    source->ReadInt32(&mAlpha);
                    source->ReadInt32(&mColorFilter);
                    Int32 mode = 0;
                    source->ReadInt32(&mode);
                    Boolean hasMode = mode != 0;
                    if (hasMode) {
                        assert(0 && "TODO");
                    }
                    else {
                        // mFilterMode = Elastos::Droid::Graphics::PorterDuffMode_CLEAR;
                    }
                    source->ReadInt32(&mLevel);

                    jobject jPorterDuffMode = GetJavaPorterDuffMode(env, mode);
                    jmethodID method = env->GetMethodID(klass, "setDrawableParameters", "(IZIILandroid/graphics/PorterDuff$Mode;I)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setDrawableParameters: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, (jint)mViewId, (jboolean)mTargetBackground, mAlpha, mColorFilter, jPorterDuffMode, mLevel);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setDrawableParameters: %d!\n", __LINE__);
                    env->DeleteLocalRef(jPorterDuffMode);
                    break;
                }
                case 2: // ReflectionAction::TAG:
                {
                    // ALOGD("GetJavaRemoteViews(), read ReflectionAction action");
                    Int32 mViewId = 0;
                    String mMethodName;
                    Int32 mType = 0;
                    source->ReadInt32(&mViewId);
                    source->ReadString(&mMethodName);
                    if (!mMethodName.IsNullOrEmpty()) {
                        mMethodName = mMethodName.ToLowerCase(0, 1);
                    }
                    source->ReadInt32(&mType);

                    jstring jMethodName = GetJavaString(env, mMethodName);
                    // ALOGD("GetJavaRemoteViews(), read ReflectionAction action, type = %d", mType);
                    switch (mType) {
                        case 1: // BOOLEAN:
                        {
                            Int32 res = 0;
                            source->ReadInt32(&res);
                            Boolean result = FALSE;
                            result = res != 0;

                            jmethodID method = env->GetMethodID(klass, "setBoolean", "(ILjava/lang/String;Z)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setBoolean: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, (jboolean)result);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setBoolean: %d!\n", __LINE__);
                            break;
                        }
                        case 2: // BYTE:
                        {
                            Byte byte;
                            source->ReadByte(&byte);
                            jmethodID method = env->GetMethodID(klass, "setByte", "(ILjava/lang/String;B)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setByte: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, (jbyte)byte);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setByte: %d!\n", __LINE__);
                            break;
                        }
                        case 3: // SHORT:
                        {
                            Int16 s = 0;
                            source->ReadInt16(&s);
                            jmethodID method = env->GetMethodID(klass, "setShort", "(ILjava/lang/String;S)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setShort: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, (jshort)s);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setShort: %d!\n", __LINE__);
                            break;
                        }
                        case 4: // INT:
                        {
                            Int32 i = 0;
                            source->ReadInt32(&i);
                            jmethodID method = env->GetMethodID(klass, "setInt", "(ILjava/lang/String;I)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setInt: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, (jint)i);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setInt: %d!\n", __LINE__);
                            break;
                        }
                        case 5: // LONG:
                        {
                            Int64 l = 0;
                            source->ReadInt64(&l);
                            jmethodID method = env->GetMethodID(klass, "setLong", "(ILjava/lang/String;J)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setLong: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, (jlong)l);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setLong: %d!\n", __LINE__);
                            break;
                        }
                        case 6: // FLOAT:
                        {
                            Float f = 0;
                            source->ReadFloat(&f);
                            jmethodID method = env->GetMethodID(klass, "setFloat", "(ILjava/lang/String;F)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setFloat: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, (jfloat)f);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setFloat: %d!\n", __LINE__);
                            break;
                        }
                        case 7: // DOUBLE:
                        {
                            Double d = 0;
                            source->ReadDouble(&d);
                            jmethodID method = env->GetMethodID(klass, "setDouble", "(ILjava/lang/String;F)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setDouble: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, (jdouble)d);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setDouble: %d!\n", __LINE__);
                            break;
                        }
                        case 8: // CHAR:
                        {
                            Int32 c;
                            source->ReadInt32(&c);
                            Char32 ch = (Char32)c;
                            jmethodID method = env->GetMethodID(klass, "setChar", "(ILjava/lang/String;C)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setChar: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, (jchar)ch);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setChar: %d!\n", __LINE__);
                            break;
                        }
                        case 9: // STRING:
                        {
                            String str;
                            source->ReadString(&str);
                            jstring jstr = GetJavaString(env, str);
                            jmethodID method = env->GetMethodID(klass, "setString", "(ILjava/lang/String;Ljava/lang/String;)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setString: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, jstr);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setString: %d!\n", __LINE__);
                            env->DeleteLocalRef(jstr);
                            break;
                        }
                        case 10: // CHAR_SEQUENCE:
                        {
                            // @See RemoteViews::ReflectionAction::ReadFromParcel
                            // @See TextUtils::CHAR_SEQUENCE_CREATOR::CreateFromParcel(source);
                            AutoPtr<ICharSequence> charCsq;
                            // charCsq = TextUtils::CHAR_SEQUENCE_CREATOR::CreateFromParcel(source);
                            Int32 kind;
                            source->ReadInt32(&kind);
                            String string;
                            source->ReadString(&string);

                            if (!string.IsNull()) {
                                if (kind == 1) {
                                    // CString::New(string, (ICharSequence**)&charCsq);
                                }
                                else{
                                    ALOGE("GetJavaRemoteViews(), something may wrong, file: %s,\n line:%d", __FILE__, __LINE__);
                                }
                            }

                            jstring jstr = GetJavaString(env, string);
                            jmethodID method = env->GetMethodID(klass, "setCharSequence", "(ILjava/lang/String;Ljava/lang/CharSequence;)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setCharSequence: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, jstr);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setCharSequence: %d!\n", __LINE__);
                            env->DeleteLocalRef(jstr);
                            break;
                        }
                        case 11: // URI:
                        {
                            Int32 flag = 0;
                            source->ReadInt32(&flag);
                            if (flag != 0) {
                                AutoPtr<IUri> uri;
                                source->ReadInterfacePtr((Handle32*)&uri);
                                if (uri != NULL) {
                                    jobject jUri = GetJavaUri(env, uri);
                                    jmethodID method = env->GetMethodID(klass, "setUri", "(ILjava/lang/String;Landroid/net/Uri;)V");
                                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setUri: %d!\n", __LINE__);
                                    env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, jUri);
                                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setUri: %d!\n", __LINE__);
                                    env->DeleteLocalRef(jUri);
                                }
                                else{
                                    ALOGE("GetJavaRemoteViews, read uri null");
                                }
                            }
                            break;
                        }
                        case 12: // BITMAP:
                        {
                            Int32 bitmap = 0;
                            source->ReadInt32(&bitmap);
                            if (bitmap != 0) {
                                AutoPtr<IBitmap> bmp;
                                CBitmap::New((IBitmap**)&bmp);
                                AutoPtr<IParcelable> bitmapParcel = IParcelable::Probe(bmp);
                                bitmapParcel->ReadFromParcel(source);

                                jobject jbitmap = GetJavaBitmap(env, bmp);
                                jmethodID method = env->GetMethodID(klass, "setBitmap", "(ILjava/lang/String;Landroid/graphics/Bitmap;)V");
                                CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setBitmap: %d!\n", __LINE__);
                                env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, jbitmap);
                                CheckErrorAndLog(env, "GetJavaRemoteViews: call method setBitmap: %d!\n", __LINE__);
                                env->DeleteLocalRef(jbitmap);
                            }
                            break;
                        }
                        case 13: // BUNDLE:
                        {
                            AutoPtr<IBundle> bundle;
                            CBundle::New((IBundle**)&bundle);
                            // AutoPtr<CBundle> cBundle = (CBundle*)bundle.Get();
                            AutoPtr<IParcelable> bundleParcel = IParcelable::Probe(bundle);
                            bundleParcel->ReadFromParcel(source);

                            jobject jbundle = GetJavaBundle(env, bundle);
                            jmethodID method = env->GetMethodID(klass, "setBundle", "(ILjava/lang/String;Landroid/os/Bundle;)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setBundle: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, jbundle);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setBundle: %d!\n", __LINE__);
                            env->DeleteLocalRef(jbundle);
                            break;
                        }
                        case 14: // INTENT:
                        {
                            Int32 intent = 0;
                            source->ReadInt32(&intent);
                            if (intent) {
                                AutoPtr<IIntent> intent;
                                CIntent::New((IIntent**)&intent);
                                AutoPtr<IParcelable> parcel = IParcelable::Probe(intent);
                                // AutoPtr<CIntent> cIntent = (CIntent*)intent.Get();
                                parcel->ReadFromParcel(source);

                                jobject jintent = GetJavaIntent(env, intent);
                                jmethodID method = env->GetMethodID(klass, "setIntent", "(ILjava/lang/String;Landroid/content/Intent;)V");
                                CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setIntent: %d!\n", __LINE__);
                                env->CallVoidMethod(jremoteview, method, (jint)mViewId, jMethodName, jintent);
                                CheckErrorAndLog(env, "GetJavaRemoteViews: call method setIntent: %d!\n", __LINE__);
                                env->DeleteLocalRef(jintent);
                            }
                            break;
                        }
                        default:
                            break;
                    }

                    env->DeleteLocalRef(jMethodName);
                    break;
                }
                case 4: // ViewGroupAction::TAG:
                {
                    ALOGE("GetJavaRemoteViews(), read ViewGroupAction action, not finished");
                    Int32 viewId;
                    source->ReadInt32(&viewId);
                    Int32 res = 0;
                    source->ReadInt32(&res);
                    Boolean nestedViewsNull = res == 0;
                    if (!nestedViewsNull) {
                        // addView
                        ALOGE("GetJavaRemoteViews(), read ViewGroupAction action not finished, Please check RemoteViews::ViewGroupAction::WriteToParcel()");

                        AutoPtr<IRemoteViews> remoteviews;
                        // Need to read a remoteview here
                        jobject jremoteviews = GetJavaRemoteViews(env, remoteviews);

                        if (jremoteviews != NULL) {
                            jmethodID method = env->GetMethodID(klass, "addView", "(ILandroid/widget/RemoteViews;)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews() FindMethod: addView: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)viewId, jremoteviews);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method addView: %d!\n", __LINE__);

                            env->DeleteLocalRef(jremoteviews);
                        }
                    }
                    else {
                        // removeAllViews
                        jmethodID method = env->GetMethodID(klass, "removeAllViews", "(I)V");
                        CheckErrorAndLog(env, "GetJavaRemoteViews(), FindMethod: removeAllViews: %d!\n", __LINE__);
                        env->CallVoidMethod(jremoteview, method, (jint)viewId);
                        CheckErrorAndLog(env, "GetJavaRemoteViews: call method removeAllViews: %d!\n", __LINE__);
                    }
                    break;
                }
                case 5: // ReflectionActionWithoutParams::TAG:
                {
                    ALOGE("GetJavaRemoteViews(), read ReflectionActionWithoutParams action");
                    Int32 viewId;
                    String methodName;
                    source->ReadInt32(&viewId);
                    source->ReadString(&methodName);
                    if (!methodName.IsNullOrEmpty()) {
                        methodName = methodName.ToLowerCase(0, 1);
                    }
                    jclass jrawp = env->FindClass("android/widget/RemoteViews$ReflectionActionWithoutParams");
                    CheckErrorAndLog(env, "GetJavaRemoteViews GetMethodID: ReflectionActionWithoutParams(I,String): %d!\n", __LINE__);
                    jmethodID mconstructor = env->GetMethodID(jrawp, "<init>", "(II)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews GetMethodID: ReflectionActionWithoutParams(I,String) : %d!\n", __LINE__);
                    jstring jMethodName = GetJavaString(env, methodName);
                    jobject jaction = env->NewObject(jrawp, mconstructor, (jint)viewId, jMethodName);
                    CheckErrorAndLog(env, "GetJavaRemoteViews NewObject: ReflectionActionWithoutParams : %d!\n", __LINE__);
                    jmethodID method = env->GetMethodID(klass, "addAction", "(android/widget/RemoteViews$Action)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: addAction: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, jaction);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method addAction: %d!\n", __LINE__);

                    env->DeleteLocalRef(jrawp);
                    env->DeleteLocalRef(jMethodName);
                    env->DeleteLocalRef(jaction);
                    break;
                }
                case 6: // RemoteViewsSetEmptyView::TAG:
                {
                    // ALOGD("GetJavaRemoteViews(), read RemoteViewsSetEmptyView action");
                    Int32 viewId;
                    Int32 emptyViewId;
                    source->ReadInt32(&viewId);
                    source->ReadInt32(&emptyViewId);

                    jmethodID method = env->GetMethodID(klass, "setEmptyView", "(II)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setEmptyView: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, (jint)viewId, (jint)emptyViewId);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setEmptyView: %d!\n", __LINE__);
                    break;
                }
                case 8: // RemoteViewsSetPendingIntentTemplate::TAG:
                {
                    // ALOGD("GetJavaRemoteViews(), read RemoteViewsSetPendingIntentTemplate action");
                    Int32 viewId;
                    source->ReadInt32(&viewId);
                    AutoPtr<IPendingIntent> pendingIntent;
                    source->ReadInterfacePtr((Handle32*)&pendingIntent);

                    jobject jpendingIntent = GetJavaPendingIntent(env, pendingIntent);

                    jmethodID method = env->GetMethodID(klass, "setPendingIntentTemplate", "(ILandroid/app/PendingIntent;)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setPendingIntentTemplate: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, (jint)viewId, jpendingIntent);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setPendingIntentTemplate: %d!\n", __LINE__);
                    env->DeleteLocalRef(jpendingIntent);
                    break;
                }
                case 9: // RemoteViewsSetOnClickFillInIntent::TAG:
                {
                    // ALOGD("GetJavaRemoteViews(), read RemoteViewsSetOnClickFillInIntent action");
                    Int32 viewId;
                    source->ReadInt32(&viewId);
                    AutoPtr<IIntent> intent;
                    source->ReadInterfacePtr((Handle32*)&intent);
                    jobject jintent = GetJavaIntent(env, intent);
                    jmethodID method = env->GetMethodID(klass, "setOnClickFillInIntent", "(ILandroid/content/Intent;)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setOnClickFillInIntent: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, (jint)viewId, jintent);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setOnClickFillInIntent: %d!\n", __LINE__);
                    env->DeleteLocalRef(jintent);
                    break;
                }
                case 10: // SetRemoteViewsAdapterIntent::TAG:
                {
                    // ALOGD("GetJavaRemoteViews(), read SetRemoteViewsAdapterIntent action");
                    Int32 viewId;
                    source->ReadInt32(&viewId);
                    AutoPtr<IIntent> intent;
                    source->ReadInterfacePtr((Handle32*)&intent);
                    jobject jintent = GetJavaIntent(env, intent);
                    jmethodID method = env->GetMethodID(klass, "setRemoteAdapter", "(ILandroid/content/Intent;)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setRemoteAdapter: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, (jint)viewId, jintent);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setRemoteAdapter: %d!\n", __LINE__);
                    env->DeleteLocalRef(jintent);
                    break;
                }
                case 11: // TextViewDrawableAction::TAG:
                {
                    // ALOGD("GetJavaRemoteViews(), read TextViewDrawableAction action");
                    Int32 viewId;
                    source->ReadInt32(&viewId);
                    Int32 res = 0;
                    source->ReadInt32(&res);
                    Boolean isRelative = res != 0;
                    Int32 d1, d2, d3, d4;
                    source->ReadInt32(&d1);
                    source->ReadInt32(&d2);
                    source->ReadInt32(&d3);
                    source->ReadInt32(&d4);

                    jmethodID method;
                    if (isRelative) {
                        method = env->GetMethodID(klass, "setTextViewCompoundDrawablesRelative", "(IIIII)V");
                        CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setTextViewCompoundDrawablesRelative: %d!\n", __LINE__);
                    }
                    else{
                        method = env->GetMethodID(klass, "setTextViewCompoundDrawables", "(IIIII)V");
                        CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setTextViewCompoundDrawables: %d!\n", __LINE__);
                    }

                    env->CallVoidMethod(jremoteview, method, (jint)viewId, (jint)d1, (jint)d2, (jint)d3, (jint)d4);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setRemoteAdapter: %d!\n", __LINE__);
                    break;
                }
                case 13: // TextViewSizeAction::TAG:
                {
                    // ALOGD("GetJavaRemoteViews(), read TextViewSizeAction action");
                    Int32 viewId;
                    Int32 units;
                    Float size;
                    source->ReadInt32(&viewId);
                    source->ReadInt32(&units);
                    source->ReadFloat(&size);

                    jmethodID method = env->GetMethodID(klass, "setTextViewTextSize", "(IIF)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setTextViewTextSize: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, (jint)viewId, (jint)units, (jfloat)size);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setTextViewTextSize: %d!\n", __LINE__);
                    break;
                }
                case 14: // ViewPaddingAction::TAG:
                {
                    ALOGD("GetJavaRemoteViews(), read ViewPaddingAction action");
                    Int32 viewId;
                    Int32 left;
                    Int32 top;
                    Int32 right;
                    Int32 bottom;
                    source->ReadInt32(&viewId);
                    source->ReadInt32(&left);
                    source->ReadInt32(&top);
                    source->ReadInt32(&right);
                    source->ReadInt32(&bottom);
                    jmethodID method = env->GetMethodID(klass, "setViewPadding", "(IIIII)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setViewPadding: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, (jint)viewId, (jint)left, (jint)top, (jint)right, (jint)bottom);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setViewPadding: %d!\n", __LINE__);
                    break;
                }
                case 12: // BitmapReflectionAction::TAG:
                {
                    Int32 viewId;
                    String methodName;
                    Int32 bitmapId;
                    source->ReadInt32(&viewId);
                    source->ReadString(&methodName);
                    if (!methodName.IsNullOrEmpty()) {
                        methodName = methodName.ToLowerCase(0, 1);
                    }
                    source->ReadInt32(&bitmapId);

                    if (bitmapCache != NULL) {
                        AutoPtr<IBitmap> bitmap;
                        bitmapCache->GetBitmapForId(bitmapId, (IBitmap**)&bitmap);

                        if (bitmap != NULL) {
                            ALOGE("GetJavaRemoteViews(), read BitmapReflectionAction action, Found bitmap in bitmapCache");
                            Int32 width = 0;
                            Int32 height = 0;
                            bitmap->GetWidth(&width);
                            bitmap->GetHeight(&height);
                            ALOGE("GetJavaRemoteViews(), read BitmapReflectionAction action, Found bitmap GetWidth = %d; getHeight = %d", width, height);
                            jobject jbitmap = GetJavaBitmap(env, bitmap);
                            jstring jmethodName = GetJavaString(env, methodName);
                            ALOGE("ElUtil::GetJavaRemoteViews(): found a BitmapReflectionAction action, methodName = %s", methodName.string());

                            jmethodID method = env->GetMethodID(klass, "setBitmap", "(ILjava/lang/String;Landroid/graphics/Bitmap;)V");
                            CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setBitmap: %d!\n", __LINE__);
                            env->CallVoidMethod(jremoteview, method, (jint)viewId, jmethodName, jbitmap);
                            CheckErrorAndLog(env, "GetJavaRemoteViews: call method setBitmap: %d!\n", __LINE__);

                            env->DeleteLocalRef(jbitmap);
                            env->DeleteLocalRef(jmethodName);
                        }
                        else{
                            ALOGE("GetJavaRemoteViews(), read BitmapReflectionAction action, did not found bitmap in bitmapCache, bitmapId = %d", bitmapId);
                        }
                    }
                    else{
                        ALOGW("GetJavaRemoteViews(), read BitmapReflectionAction action, But bitmapCache is null");
                    }

                    break;
                }
                case 15: // SetRemoteViewsAdapterList::TAG:
                {
                    jclass listKlass = env->FindClass("java/util/ArrayList");
                    CheckErrorAndLog(env, "GetJavaRemoteViews NewObject: ArrayList line: %d", __LINE__);

                    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews GetMethodID: ArrayList line: %d", __LINE__);

                    jobject jlist = env->NewObject(listKlass, m);
                    CheckErrorAndLog(env, "GetJavaRemoteViews NewObject: ArrayList line: %d", __LINE__);

                    jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
                    CheckErrorAndLog(env, "GetJavaRemoteViews GetMethodID: add line: %d", __LINE__);

                    Int32 viewId, viewTypeCount;
                    source->ReadInt32(&viewId);
                    source->ReadInt32(&viewTypeCount);
                    Int32 count;
                    source->ReadInt32(&count);
                    for (Int32 i = 0; i < count; i++) {
                        AutoPtr<IRemoteViews> rv;
                        CRemoteViews::New((IRemoteViews**)&rv);
                        IParcelable::Probe(rv)->ReadFromParcel(source);
                        jobject jrv = GetJavaRemoteViews(env, rv);
                        env->CallBooleanMethod(jlist, mAdd, jrv);
                        CheckErrorAndLog(env, "GetJavaRemoteViews CallBooleanMethod: add line: %d", __LINE__);
                        env->DeleteLocalRef(jrv);
                    }
                    jmethodID method = env->GetMethodID(klass, "setRemoteAdapter", "(ILjava/util/ArrayList;I)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setRemoteAdapter: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, (jint)viewId, jlist, (jint)viewTypeCount);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setRemoteAdapter: %d!\n", __LINE__);
                    env->DeleteLocalRef(listKlass);
                    env->DeleteLocalRef(jlist);
                    break;
                }
                case 17: // TextViewDrawableColorFilterAction::TAG:
                {
                    Int32 viewId, index, color, mode;
                    source->ReadInt32(&viewId);
                    Int32 isRelative;
                    source->ReadInt32(&isRelative);
                    source->ReadInt32(&index);
                    source->ReadInt32(&color);
                    source->ReadInt32(&mode);
                    jobject jPorterDuffMode = GetJavaPorterDuffMode(env, mode);
                    jmethodID method = env->GetMethodID(klass, "setTextViewCompoundDrawablesRelativeColorFilter",
                        "(IIILandroid/graphics/PorterDuff$Mode;)V");
                    CheckErrorAndLog(env, "GetJavaRemoteViews(): FindMethod: setTextViewCompoundDrawablesRelativeColorFilter: %d!\n", __LINE__);
                    env->CallVoidMethod(jremoteview, method, (jint)viewId, (jint)index, (jint)color, jPorterDuffMode);
                    CheckErrorAndLog(env, "GetJavaRemoteViews: call method setTextViewCompoundDrawablesRelativeColorFilter: %d!\n", __LINE__);
                    env->DeleteLocalRef(jPorterDuffMode);
                    break;
                }
                default:
                    break;
            }
        }
    }

    env->DeleteLocalRef(klass);

    return jremoteview;
}


jobject ElUtil::GetJavaPorterDuffMode(
    /* [in] */ JNIEnv* env,
    /* [in] */ PorterDuffMode mode)
{
    jclass cPorterDuffMode = env->FindClass("android/graphics/PorterDuff$Mode");
    CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail FindClass: PorterDuff$Mode : %d!\n", __LINE__);
    jfieldID f;
    switch (mode) {
        case Elastos::Droid::Graphics::PorterDuffMode_NONE:
            f = env->GetStaticFieldID(cPorterDuffMode, "NONE", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: NONE : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_CLEAR:
            f = env->GetStaticFieldID(cPorterDuffMode, "CLEAR", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: CLEAR : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_SRC:
            f = env->GetStaticFieldID(cPorterDuffMode, "SRC", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: SRC : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_DST:
            f = env->GetStaticFieldID(cPorterDuffMode, "DST", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: DST : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_SRC_OVER:
            f = env->GetStaticFieldID(cPorterDuffMode, "SRC_OVER", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: SRC_OVER : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_DST_OVER:
            f = env->GetStaticFieldID(cPorterDuffMode, "DST_OVER", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: DST_OVER : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_SRC_IN:
            f = env->GetStaticFieldID(cPorterDuffMode, "SRC_IN", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: SRC_IN : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_DST_IN:
            f = env->GetStaticFieldID(cPorterDuffMode, "DST_IN", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: DST_IN : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_SRC_OUT:
            f = env->GetStaticFieldID(cPorterDuffMode, "SRC_OUT", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: SRC_OUT : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_DST_OUT:
            f = env->GetStaticFieldID(cPorterDuffMode, "DST_OUT", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: DST_OUT : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_SRC_ATOP:
            f = env->GetStaticFieldID(cPorterDuffMode, "SRC_ATOP", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: SRC_ATOP : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_DST_ATOP:
            f = env->GetStaticFieldID(cPorterDuffMode, "DST_ATOP", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: DST_ATOP : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_XOR:
            f = env->GetStaticFieldID(cPorterDuffMode, "XOR", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: XOR : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_DARKEN:
            f = env->GetStaticFieldID(cPorterDuffMode, "DARKEN", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: DARKEN : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_LIGHTEN:
            f = env->GetStaticFieldID(cPorterDuffMode, "LIGHTEN", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: LIGHTEN : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_MULTIPLY:
            f = env->GetStaticFieldID(cPorterDuffMode, "MULTIPLY", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: MULTIPLY : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_SCREEN:
            f = env->GetStaticFieldID(cPorterDuffMode, "SCREEN", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: SCREEN : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_ADD:
            f = env->GetStaticFieldID(cPorterDuffMode, "ADD", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: ADD : %d!\n", __LINE__);
            break;
        case Elastos::Droid::Graphics::PorterDuffMode_OVERLAY:
            f = env->GetStaticFieldID(cPorterDuffMode, "OVERLAY", "Landroid/graphics/PorterDuff$Mode;");
            CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticFieldID: OVERLAY : %d!\n", __LINE__);
            break;
        default:
            break;
    }

    jobject jPorterDuffMode = env->GetStaticObjectField(cPorterDuffMode, f);
    CheckErrorAndLog(env, "GetJavaPorterDuffMode() Fail GetStaticObjectField: : %d!\n", __LINE__);
    env->DeleteLocalRef(cPorterDuffMode);
    return jPorterDuffMode;
}

jobject ElUtil::GetJavaAppWidgetProviderInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IAppWidgetProviderInfo* obj)
{
    if (env == NULL || obj == NULL) {
        ALOGW("GetJavaAppWidgetProviderInfo: Invalid argumenet!");
        return NULL;
    }

    jclass klass = env->FindClass("android/appwidget/AppWidgetProviderInfo");
    ElUtil::CheckErrorAndLog(env, "GetJavaAppWidgetProviderInfo Fail FindClass: AppWidgetProviderInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "GetJavaAppWidgetProviderInfo Fail GetMethodID: AppWidgetProviderInfo() : %d!\n", __LINE__);

    jobject jproviderInfo = env->NewObject(klass, m);
    ElUtil::CheckErrorAndLog(env, "GetJavaAppWidgetProviderInfo Fail NewObject: AppWidgetProviderInfo : %d!\n", __LINE__);

    AutoPtr<IComponentName> componentName;
    obj->GetProvider((IComponentName**)&componentName);

    jobject jcomponentName = GetJavaComponentName(env, componentName);

    if (jcomponentName != NULL) {
        jfieldID f = env->GetFieldID(klass, "provider", "Landroid/content/ComponentName;");
        ElUtil::CheckErrorAndLog(env, "GetJavaAppWidgetProviderInfo Fail GetFieldID: provider : %d!\n", __LINE__);

        env->SetObjectField(jproviderInfo, f, jcomponentName);
        ElUtil::CheckErrorAndLog(env, "GetJavaAppWidgetProviderInfo Fail SetObjectField: provider : %d!\n", __LINE__);
        env->DeleteLocalRef(jcomponentName);
    }
    else {
        ALOGD("GetJavaAppWidgetProviderInfo() WARN: jcomponentName is NULL!");
    }

    Int32 tempInt = 0;
    obj->GetMinWidth(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "minWidth", "GetJavaAppWidgetProviderInfo");

    obj->GetMinHeight(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "minHeight", "GetJavaAppWidgetProviderInfo");

    obj->GetMinResizeWidth(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "minResizeWidth", "GetJavaAppWidgetProviderInfo");

    obj->GetMinResizeHeight(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "minResizeHeight", "GetJavaAppWidgetProviderInfo");

    obj->GetUpdatePeriodMillis(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "updatePeriodMillis", "GetJavaAppWidgetProviderInfo");

    obj->GetInitialLayout(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "initialLayout", "GetJavaAppWidgetProviderInfo");

    obj->GetInitialKeyguardLayout(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "initialKeyguardLayout", "GetJavaAppWidgetProviderInfo");

    AutoPtr<IComponentName> configure;
    obj->GetConfigure((IComponentName**)&configure);

    jobject jconfigure = GetJavaComponentName(env, configure);

    if (jconfigure != NULL) {
        jfieldID f = env->GetFieldID(klass, "configure", "Landroid/content/ComponentName;");
        ElUtil::CheckErrorAndLog(env, "GetJavaAppWidgetProviderInfo Fail GetFieldID: configure : %d!\n", __LINE__);

        env->SetObjectField(jproviderInfo, f, jconfigure);
        ElUtil::CheckErrorAndLog(env, "GetJavaAppWidgetProviderInfo Fail SetObjectField: configure : %d!\n", __LINE__);
        env->DeleteLocalRef(jconfigure);
    }
    else {
        ALOGD("GetJavaAppWidgetProviderInfo() WARN: jconfigure is NULL!");
    }

    String label;
    obj->GetLabel(&label);
    SetJavaStringField(env, klass, jproviderInfo, label, "label", "GetJavaAppWidgetProviderInfo");

    obj->GetIcon(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "icon", "GetJavaAppWidgetProviderInfo");

    obj->GetAutoAdvanceViewId(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "autoAdvanceViewId", "GetJavaAppWidgetProviderInfo");

    obj->GetPreviewImage(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "previewImage", "GetJavaAppWidgetProviderInfo");

    obj->GetResizeMode(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "resizeMode", "GetJavaAppWidgetProviderInfo");

    obj->GetWidgetCategory(&tempInt);
    SetJavaIntField(env, klass, jproviderInfo, tempInt, "widgetCategory", "GetJavaAppWidgetProviderInfo");

    AutoPtr<IActivityInfo> aInfo;
    obj->GetProviderInfo((IActivityInfo**)&aInfo);

    jobject jaInfo = GetJavaActivityInfo(env, aInfo);
    jfieldID f = env->GetFieldID(klass, "providerInfo", "Landroid/content/pm/ActivityInfo;");
    ElUtil::CheckErrorAndLog(env, "GetJavaAppWidgetProviderInfo GetFieldID: providerInfo", __LINE__);

    env->SetObjectField(jproviderInfo, f, jaInfo);
    ElUtil::CheckErrorAndLog(env, "GetJavaAppWidgetProviderInfo SetObjectField: providerInfo", __LINE__);

    env->DeleteLocalRef(klass);
    env->DeleteLocalRef(jaInfo);

    return jproviderInfo;
}

jobject ElUtil::GetJavaAuthenticatorDescription(
    /* [in] */ JNIEnv* env,
    /* [in] */ IAuthenticatorDescription* authDesc)
{
    if (env == NULL || authDesc == NULL) {
        ALOGE("GetJavaAuthenticatorDescription: Invalid argumenet!");
        return NULL;
    }

    jclass authlass = env->FindClass("android/accounts/AuthenticatorDescription");
    ElUtil::CheckErrorAndLog(env, "GetJavaAuthenticatorDescription FindClass: AuthenticatorDescription : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(authlass, "<init>", "(Ljava/lang/String;Ljava/lang/String;IIIIZ)V");
    ElUtil::CheckErrorAndLog(env, "GetJavaAuthenticatorDescription GetMethodID: AuthenticatorDescription() : %d!\n", __LINE__);

    String type;
    authDesc->GetType(&type);
    jstring jtype = ElUtil::GetJavaString(env, type);

    String packageName;
    authDesc->GetPackageName(&packageName);
    jstring jpackageName = ElUtil::GetJavaString(env, packageName);

    Int32 labelId = 0;
    authDesc->GetLabelId(&labelId);

    Int32 iconId = 0;
    authDesc->GetIconId(&iconId);

    Int32 smallIconId = 0;
    authDesc->GetSmallIconId(&smallIconId);

    Int32 accountPreferencesId = 0;
    authDesc->GetAccountPreferencesId(&accountPreferencesId);

    Boolean customTokens = 0;
    authDesc->GetCustomTokens(&customTokens);

    jobject jauthDesc = env->NewObject(authlass, m, jtype, jpackageName, (jint)labelId,
        (jint)iconId, (jint)smallIconId, (jint)accountPreferencesId, (jboolean)customTokens);
    ElUtil::CheckErrorAndLog(env, "GetJavaAuthenticatorDescription NewObject: AuthenticatorDescription : %d!\n", __LINE__);

    env->DeleteLocalRef(authlass);
    env->DeleteLocalRef(jtype);
    env->DeleteLocalRef(jpackageName);
    return jauthDesc;
}

jobject ElUtil::GetJavaSpellCheckerInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ ISpellCheckerInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaSpellCheckerInfo: Invalid argumenet!");
        return NULL;
    }

    ALOGE("GetJavaSpellCheckerInfo() E_NOT_IMPLEMENTED!");

    return NULL;
}

jobject ElUtil::GetJavaSpellCheckerSubtype(
    /* [in] */ JNIEnv* env,
    /* [in] */ ISpellCheckerSubtype* subType)
{
    if (env == NULL || subType == NULL) {
        ALOGE("GetJavaSpellCheckerSubtype: Invalid argumenet!");
        return NULL;
    }

    jclass stlass = env->FindClass("android/view/textservice/SpellCheckerSubtype");
    ElUtil::CheckErrorAndLog(env, "GetJavaSpellCheckerSubtype FindClass: SpellCheckerSubtype : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(stlass, "<init>", "(ILjava/lang/String;Ljava/lang/String;)V");
    ElUtil::CheckErrorAndLog(env, "GetJavaSpellCheckerSubtype GetMethodID: SpellCheckerSubtype() : %d!\n", __LINE__);

    Int32 nameResId = 0;
    subType->GetNameResId(&nameResId);

    String locale;
    subType->GetLocale(&locale);
    jstring jlocale = ElUtil::GetJavaString(env, locale);

    String extraValue;
    subType->GetExtraValue(&extraValue);
    jstring jextraValue = ElUtil::GetJavaString(env, extraValue);

    jobject jsubType = env->NewObject(stlass, m, (jint)nameResId, jlocale, jextraValue);
    ElUtil::CheckErrorAndLog(env, "GetJavaSpellCheckerSubtype NewObject: SpellCheckerSubtype : %d!\n", __LINE__);

    env->DeleteLocalRef(stlass);
    env->DeleteLocalRef(jlocale);
    env->DeleteLocalRef(jextraValue);
    return jsubType;
}

bool ElUtil::ToElAppWidgetProviderInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jproviderInfo,
    /* [in] */ IAppWidgetProviderInfo** providerInfo)
{
    if (env == NULL || jproviderInfo == NULL || providerInfo == NULL) {
        ALOGE("ToElAppWidgetProviderInfo: Invalid argumenet!");
        return FALSE;
    }

    CAppWidgetProviderInfo::New(providerInfo);

    jclass klass = env->FindClass("android/appwidget/AppWidgetProviderInfo");
    ElUtil::CheckErrorAndLog(env, "ToElAppWidgetProviderInfo Fail FindClass: AppWidgetProviderInfo : %d!\n", __LINE__);

    jfieldID fprovider = env->GetFieldID(klass, "provider", "Landroid/content/ComponentName;");
    ElUtil::CheckErrorAndLog(env, "ToElAppWidgetProviderInfo Fail GetFieldID: provider : %d!\n", __LINE__);

    jobject jcomponentName = env->GetObjectField(jproviderInfo, fprovider);
    ElUtil::CheckErrorAndLog(env, "Fail GetObjectField: provider : %d!\n", __LINE__);

    AutoPtr<IComponentName> componentName;
    ToElComponentName(env, jcomponentName, (IComponentName**)&componentName);
    env->DeleteLocalRef(jcomponentName);
    (*providerInfo)->SetProvider(componentName);

    Int32 tempInt = 0;
    tempInt = GetJavaIntField(env, klass, jproviderInfo, "minWidth", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetMinWidth(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "minHeight", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetMinHeight(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "minResizeWidth", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetMinResizeWidth(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "minResizeHeight", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetMinResizeHeight(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "updatePeriodMillis", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetUpdatePeriodMillis(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "initialLayout", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetInitialLayout(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "initialKeyguardLayout", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetInitialKeyguardLayout(tempInt);

    jfieldID fconfigure = env->GetFieldID(klass, "configure", "Landroid/content/ComponentName;");
    ElUtil::CheckErrorAndLog(env, "ToElAppWidgetProviderInfo Fail GetFieldID: configure : %d!\n", __LINE__);

    jobject jconfigure = env->GetObjectField(jproviderInfo, fprovider);
    ElUtil::CheckErrorAndLog(env, "Fail GetObjectField: configure : %d!\n", __LINE__);

    if (jconfigure != NULL) {
        AutoPtr<IComponentName> configure;
        ToElComponentName(env, jconfigure, (IComponentName**)&configure);
        env->DeleteLocalRef(jconfigure);
        (*providerInfo)->SetConfigure(configure);
    }

    String label = GetJavaStringField(env, klass, jproviderInfo, "label", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetLabel(label);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "icon", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetIcon(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "autoAdvanceViewId", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetAutoAdvanceViewId(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "previewImage", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetPreviewImage(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "resizeMode", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetResizeMode(tempInt);

    tempInt = GetJavaIntField(env, klass, jproviderInfo, "widgetCategory", "ToElAppWidgetProviderInfo");
    (*providerInfo)->SetWidgetCategory(tempInt);

    jfieldID fproviderInfo = env->GetFieldID(klass, "providerInfo", "Landroid/content/pm/ActivityInfo;");
    ElUtil::CheckErrorAndLog(env, "ToElAppWidgetProviderInfo Fail GetFieldID: providerInfo : %d!\n", __LINE__);

    jobject jpInfo = env->GetObjectField(jpInfo, fprovider);
    ElUtil::CheckErrorAndLog(env, "Fail GetObjectField: providerInfo : %d!\n", __LINE__);

    if (jpInfo != NULL) {
        AutoPtr<IActivityInfo> pInfo;
        ToElActivityInfo(env, jpInfo, (IActivityInfo**)&pInfo);
        (*providerInfo)->SetProviderInfo(pInfo);
        env->DeleteLocalRef(jpInfo);
    }

    env->DeleteLocalRef(klass);

    return TRUE;
}

bool ElUtil::ToElContentProviderOperation(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject joperation,
    /* [in] */ IContentProviderOperation** operation)
{
    if (env == NULL || joperation == NULL || operation == NULL) {
        ALOGE("ToElContentProviderOperation: Invalid argumenet!");
        return FALSE;
    }

    jclass cpoklass = env->FindClass("android/content/ContentProviderOperation");
    CheckErrorAndLog(env, "FindClass: ContentProviderOperation : %d!\n", __LINE__);

    Int32 type = GetJavaIntField(env, cpoklass, joperation, "mType", "ToElContentProviderOperation");
    jfieldID fUri = env->GetFieldID(cpoklass, "mUri", "Landroid/net/Uri;");
    CheckErrorAndLog(env, "%s: Fail get ContentProviderOperation fieldid: %s : %d!\n", "ToElContentProviderOperation", "mUri", __LINE__);
    jobject jUri = env->GetObjectField(joperation, fUri);
    CheckErrorAndLog(env, "%s: Fail get ContentProviderOperation field: %s : %d!\n", "ToElContentProviderOperation", "mUri", __LINE__);

    AutoPtr<IUri> uri;
    if (!ToElUri(env, jUri, (IUri**)&uri)) {
        ALOGE("ToElContentProviderOperation: Process uri failed!");
        env->DeleteLocalRef(jUri);
        env->DeleteLocalRef(cpoklass);
        return FALSE;
    }

    env->DeleteLocalRef(jUri);
    AutoPtr<IContentProviderOperationBuilder> operationBuilder;
    CContentProviderOperationBuilder::New(type, uri, (IContentProviderOperationBuilder**)&operationBuilder);

    if (operationBuilder == NULL) {
        ALOGE("ToElContentProviderOperation, create CContentProviderOperationBuilder failed!");
        env->DeleteLocalRef(cpoklass);
        return FALSE;
    }

    // Process operation params
    jfieldID fContentValues = env->GetFieldID(cpoklass, "mValues", "Landroid/content/ContentValues;");
    CheckErrorAndLog(env, "%s: Fail get ContentProviderOperation fieldid: %s : %d!\n", "ToElContentProviderOperation", "mValues", __LINE__);
    jobject jContentValues = env->GetObjectField(joperation, fContentValues);
    CheckErrorAndLog(env, "%s: Fail get ContentProviderOperation field: %s : %d!\n", "ToElContentProviderOperation", "mValues", __LINE__);
    if (jContentValues != NULL) {
        AutoPtr<IContentValues> contentValues;
        ToElContentValues(env, jContentValues, (IContentValues**)&contentValues);
        operationBuilder->WithValues(contentValues);
        env->DeleteLocalRef(jContentValues);
    }

    String selection = GetJavaStringField(env, cpoklass, joperation, "mSelection", "ToElContentProviderOperation");
    if (!selection.IsNullOrEmpty()) {
        jfieldID fSelectionArgs = env->GetFieldID(cpoklass, "mSelectionArgs", "[Ljava/lang/String;");
        CheckErrorAndLog(env, "GetFieldID mMimeTypes : %d!\n", __LINE__);

        jobjectArray jSelectionArgs = (jobjectArray)env->GetObjectField(joperation, fSelectionArgs);
        CheckErrorAndLog(env, "GetObjectField mSelectionArgs : %d!\n", __LINE__);

        AutoPtr<ArrayOf<String> > selectionArgs;
        if (jSelectionArgs != NULL) {
            if (!ElUtil::ToElStringArray(env, jSelectionArgs, (ArrayOf<String>**)&selectionArgs)) {
                ALOGE("ToElContentProviderOperation() ToElStringArray fail!");
            }

            env->DeleteLocalRef(jSelectionArgs);
        }

        operationBuilder->WithSelection(selection, selectionArgs.Get());
    }

    if (type == IContentProviderOperation::TYPE_UPDATE || type == IContentProviderOperation::TYPE_DELETE
            || type == IContentProviderOperation::TYPE_ASSERT) {
        Int32 expectedCount = GetJavaIntegerField(env, cpoklass, joperation, "mExpectedCount", 0, "ToElContentProviderOperation");
        operationBuilder->WithExpectedCount(expectedCount);
    }

    jfieldID fmValuesBackReferences = env->GetFieldID(cpoklass, "mValuesBackReferences", "Landroid/content/ContentValues;");
    CheckErrorAndLog(env, "%s: Fail get ContentProviderOperation fieldid: %s : %d!\n", "ToElContentProviderOperation", "mValuesBackReferences", __LINE__);
    jobject jmValuesBackReferences = env->GetObjectField(joperation, fmValuesBackReferences);
    CheckErrorAndLog(env, "%s: Fail get ContentProviderOperation field: %s : %d!\n", "ToElContentProviderOperation", "mValuesBackReferences", __LINE__);
    if (jmValuesBackReferences != NULL) {
        AutoPtr<IContentValues> contentValues;
        ToElContentValues(env, jmValuesBackReferences, (IContentValues**)&contentValues);
        operationBuilder->WithValueBackReferences(contentValues);
        env->DeleteLocalRef(jmValuesBackReferences);
    }

    jfieldID fmSelectionArgsBackReferences = env->GetFieldID(cpoklass, "mSelectionArgsBackReferences", "Ljava/util/Map;");
    CheckErrorAndLog(env, "%s: Fail get ContentProviderOperation fieldid: %s : %d!\n", "ToElContentProviderOperation", "mSelectionArgsBackReferences", __LINE__);
    jobject jmSelectionArgsBackReferences = env->GetObjectField(joperation, fmSelectionArgsBackReferences);
    CheckErrorAndLog(env, "%s: Fail get ContentProviderOperation field: %s : %d!\n", "ToElContentProviderOperation", "mSelectionArgsBackReferences", __LINE__);
    if (jmSelectionArgsBackReferences != NULL) {
        if (type != IContentProviderOperation::TYPE_UPDATE && type != IContentProviderOperation::TYPE_DELETE
                && type != IContentProviderOperation::TYPE_ASSERT) {
            ALOGE("ToElContentProviderOperation(), mSelectionArgsBackReferences not null, but got invalid type:%d", type);
        }
        else {
            jclass mapKlass = env->FindClass("java/util/Map");
            CheckErrorAndLog(env, "FindClass: Map : %d!\n", __LINE__);

            jmethodID mEntrySet = env->GetMethodID(mapKlass, "entrySet", "()Ljava/util/Set;");
            CheckErrorAndLog(env, "ToElContentProviderOperation GetMethodID: map.entrySet() : %d!\n", __LINE__);
            env->DeleteLocalRef(mapKlass);

            jobject jentrySet = env->CallObjectMethod(jmSelectionArgsBackReferences, mEntrySet);
            CheckErrorAndLog(env, "ToElContentProviderOperation failed call method: map.entrySet() : %d!\n", __LINE__);

            jclass setKlass = env->FindClass("java/util/Set");
            CheckErrorAndLog(env, "FindClass: Map : %d!\n", __LINE__);

            jmethodID mToArray = env->GetMethodID(setKlass, "toArray", "()[Ljava/lang/Object;");
            CheckErrorAndLog(env, "ToElContentProviderOperation GetMethodID: map.entrySet() : %d!\n", __LINE__);

            jobjectArray jentryArray = (jobjectArray)env->CallObjectMethod(jentrySet, mToArray);
            CheckErrorAndLog(env, "ToElContentProviderOperation failed call method: map.entrySet() : %d!\n", __LINE__);
            env->DeleteLocalRef(jentrySet);

            int size = env->GetArrayLength(jentryArray);
            CheckErrorAndLog(env, "ToElContentProviderOperation GetArrayLength of jentryArray %d!\n", __LINE__);
            env->DeleteLocalRef(setKlass);

            jclass mapEntryKlass = env->FindClass("java/util/Map$Entry");
            jmethodID mGetKey = env->GetMethodID(mapEntryKlass, "getKey", "()Ljava/lang/Object;");
            CheckErrorAndLog(env, "ToElContentProviderOperation GetMethodID: map.entry.getKey() : %d!\n", __LINE__);
            jmethodID mGetValue = env->GetMethodID(mapEntryKlass, "getValue", "()Ljava/lang/Object;");
            CheckErrorAndLog(env, "ToElContentProviderOperation GetMethodID: map.entry.getValue() : %d!\n", __LINE__);
            for(int i = 0; i < size; i++) {
                jobject jentry = env->GetObjectArrayElement(jentryArray, i);
                CheckErrorAndLog(env, "ToElContentProviderOperation GetObjectArrayElement: jmSelectionArgsBackReferences%d", __LINE__);

                jobject jselectionArgIndex = env->CallObjectMethod(jentry, mGetKey); // Integer
                CheckErrorAndLog(env, "ToElContentProviderOperation failed call method: map.entry.getKey() : %d!\n", __LINE__);
                jobject jpreviousResult = env->CallObjectMethod(jentry, mGetValue); // Integer
                CheckErrorAndLog(env, "ToElContentProviderOperation failed call method: map.entry.getValue() : %d!\n", __LINE__);

                jclass saiCls = env->GetObjectClass(jselectionArgIndex);
                jclass prCls = env->GetObjectClass(jpreviousResult);
                Int32 selectionArgIndex = GetJavaIntField(env, saiCls, jselectionArgIndex, "value", "ToElContentProviderOperation");
                Int32 previousResult = GetJavaIntField(env, prCls, jpreviousResult, "value", "ToElContentProviderOperation");
                env->DeleteLocalRef(saiCls);
                env->DeleteLocalRef(prCls);
                env->DeleteLocalRef(jentry);
                env->DeleteLocalRef(jselectionArgIndex);
                env->DeleteLocalRef(jpreviousResult);

                operationBuilder->WithSelectionBackReference(selectionArgIndex, previousResult);
            }

            env->DeleteLocalRef(jentryArray);
            env->DeleteLocalRef(mapEntryKlass);
        }

        env->DeleteLocalRef(jmSelectionArgsBackReferences);
    }

    jboolean  mYieldAllowed = GetJavaBoolField(env, cpoklass, joperation, "mYieldAllowed", "ToElContentProviderOperation");
    operationBuilder->WithYieldAllowed((Boolean)mYieldAllowed);

    env->DeleteLocalRef(cpoklass);
    operationBuilder->Build(operation);
    return TRUE;
}

jobject ElUtil::GetJavaContentProviderResult(
    /* [in] */ JNIEnv* env,
    /* [in] */ IContentProviderResult* result)
{
    if (result == NULL) {
        ALOGW("GetJavaContentProviderResult(), result = NULL");
        return NULL;
    }

    jobject jresult = NULL;

    jclass cprklass = env->FindClass("android/content/ContentProviderResult");
    CheckErrorAndLog(env, "FindClass: ContentProviderResult : %d!\n", __LINE__);

    AutoPtr<IUri> uri;
    result->GetUri((IUri**)&uri);
    if (uri != NULL) {
        jmethodID m = env->GetMethodID(cprklass, "<init>", "(Landroid/net/Uri;)V");
        CheckErrorAndLog(env, "GetJavaContentProviderResult GetMethodID: ContentProviderResult(Uri) : %d!\n", __LINE__);
        jobject juri = GetJavaUri(env, uri);
        jresult = env->NewObject(cprklass, m, juri);
        env->DeleteLocalRef(juri);
    }
    else {
        Int32 count;
        result->GetCount(&count);
        jmethodID m = env->GetMethodID(cprklass, "<init>", "(I)V");
        CheckErrorAndLog(env, "GetJavaContentProviderResult GetMethodID: ContentProviderResult(I) : %d!\n", __LINE__);
        jresult = env->NewObject(cprklass, m, count);
    }

    env->DeleteLocalRef(cprklass);

    return jresult;
}

bool ElUtil::ToElIntentShortcutIconResource(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jresource,
    /* [in] */ IIntentShortcutIconResource** resource)
{
    if (env == NULL || jresource == NULL || resource == NULL) {
        ALOGE("ToElIntentShortcutIconResource: Invalid argumenet!");
        return FALSE;
    }

    CIntentShortcutIconResource::New(resource);

    jclass klass = env->FindClass("android/content/Intent$ShortcutIconResource");
    ElUtil::CheckErrorAndLog(env, "ToElIntentShortcutIconResource Fail FindClass: Intent$ShortcutIconResource : %d!\n", __LINE__);

    String packageName = GetJavaStringField(env, klass, jresource, "packageName", "ToElIntentShortcutIconResource");
    (*resource)->SetPackageName(packageName);

    String resName = GetJavaStringField(env, klass, jresource, "resourceName", "ToElIntentShortcutIconResource");
    (*resource)->SetResourceName(resName);
    env->DeleteLocalRef(klass);

    return TRUE;
}

bool ElUtil::ToElManifestDigest(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jmanifestDigest,
    /* [out] */ IManifestDigest** manifestDigest)
{
    if (env == NULL || jmanifestDigest == NULL || manifestDigest == NULL) {
        ALOGE("ToElManifestDigest: Invalid argumenet!");
        return FALSE;
    }

    jclass manifestDigestClass = env->FindClass("android/content/pm/ManifestDigest");

    jfieldID mDigestField = env->GetFieldID(manifestDigestClass, "mDigest", "[B");
    CheckErrorAndLog(env, "%s: Fail get ManifestDigest fieldid: %s : %d!\n", "ToElManifestDigest", "mDigest", __LINE__);

    jbyteArray jmDigest = (jbyteArray)env->GetObjectField(jmanifestDigest, mDigestField);
    CheckErrorAndLog(env, "%s: Fail get ManifestDigest field: %s : %d!\n", "ToElManifestDigest", "mDigest", __LINE__);

    AutoPtr<ArrayOf<Byte> > mDigest;
    if (!ToElByteArray(env, jmDigest, (ArrayOf<Byte>**)&mDigest)) {
        ALOGE("ToElManifestDigest ToElByteArray failed !");
    }

    env->DeleteLocalRef(manifestDigestClass);
    env->DeleteLocalRef(jmDigest);

    CManifestDigest::New(mDigest, manifestDigest);
    if (*manifestDigest == NULL) {
        ALOGE("CManifestDigest::New failed!");
        return FALSE;
    }

    return TRUE;
}

bool ElUtil::ToElVerificationParams(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jverificationParams,
    /* [out] */ IVerificationParams** verificationParams)
{
    if (env == NULL || jverificationParams == NULL || verificationParams == NULL) {
        ALOGE("ToElVerificationParams: Invalid argumenet!");
        return FALSE;
    }

    jclass verificationParamsClass = env->FindClass("android/content/pm/VerificationParams");
    CheckErrorAndLog(env, "ToElVerificationParams(): FindClass: VerificationParams failed: %d!\n", __LINE__);

    jfieldID mVerificationURIField = env->GetFieldID(verificationParamsClass, "mVerificationURI", "Landroid/net/Uri;");
    CheckErrorAndLog(env, "%s: Fail get VerificationParams fieldid: %s : %d!\n", "ToElVerificationParams", "mVerificationURI", __LINE__);
    jobject jmVerificationURI = env->GetObjectField(jverificationParams, mVerificationURIField);
    CheckErrorAndLog(env, "%s: Fail get VerificationParams field: %s : %d!\n", "ToElVerificationParams", "mVerificationURI", __LINE__);
    AutoPtr<IUri> mVerificationURI;
    if (jmVerificationURI != NULL && !ToElUri(env, jmVerificationURI, (IUri**)&mVerificationURI)) {
        ALOGE("ToElVerificationParams jmVerificationURI ToElUri fail!");
    }
    env->DeleteLocalRef(jmVerificationURI);

    jfieldID mOriginatingURIField = env->GetFieldID(verificationParamsClass, "mOriginatingURI", "Landroid/net/Uri;");
    CheckErrorAndLog(env, "%s: Fail get VerificationParams fieldid: %s : %d!\n", "ToElVerificationParams", "mOriginatingURI", __LINE__);
    jobject jmOriginatingURI = env->GetObjectField(jverificationParams, mOriginatingURIField);
    CheckErrorAndLog(env, "%s: Fail get VerificationParams field: %s : %d!\n", "ToElVerificationParams", "mOriginatingURI", __LINE__);
    AutoPtr<IUri> mOriginatingURI;
    if (jmOriginatingURI != NULL && !ToElUri(env, jmOriginatingURI, (IUri**)&mOriginatingURI)) {
        ALOGE("ToElVerificationParams jmOriginatingURI ToElUri fail!");
    }
    env->DeleteLocalRef(jmOriginatingURI);

    jfieldID mReferrerField = env->GetFieldID(verificationParamsClass, "mReferrer", "Landroid/net/Uri;");
    CheckErrorAndLog(env, "%s: Fail get VerificationParams fieldid: %s : %d!\n", "ToElVerificationParams", "mReferrer", __LINE__);
    jobject jmReferrer = env->GetObjectField(jverificationParams, mReferrerField);
    CheckErrorAndLog(env, "%s: Fail get VerificationParams field: %s : %d!\n", "ToElVerificationParams", "mReferrer", __LINE__);
    AutoPtr<IUri> mReferrer;
    if (jmReferrer != NULL && !ToElUri(env, jmReferrer, (IUri**)&mReferrer)) {
        ALOGE("ToElVerificationParams jmReferrer ToElUri fail!");
    }
    env->DeleteLocalRef(jmReferrer);

    Int32 mOriginatingUid = GetJavaIntField(env, verificationParamsClass, jverificationParams, "mOriginatingUid", "ToElVerificationParams");

    Int32 mInstallerUid = GetJavaIntField(env, verificationParamsClass, jverificationParams, "mOriginatingUid", "ToElVerificationParams");

    jfieldID mManifestDigestField = env->GetFieldID(verificationParamsClass, "mManifestDigest", "Landroid/content/pm/ManifestDigest;");
    CheckErrorAndLog(env, "%s: Fail get VerificationParams fieldid: %s : %d!\n", "ToElVerificationParams", "mManifestDigest", __LINE__);
    jobject jmManifestDigest = env->GetObjectField(jverificationParams, mManifestDigestField);
    CheckErrorAndLog(env, "%s: Fail get VerificationParams field: %s : %d!\n", "ToElVerificationParams", "mManifestDigest", __LINE__);
    AutoPtr<IManifestDigest> mManifestDigest;
    if (jmManifestDigest != NULL && !ToElManifestDigest(env, jmManifestDigest, (IManifestDigest**)&mManifestDigest)) {
        ALOGE("ToElVerificationParams jmManifestDigest ToElManifestDigest fail!");
    }
    env->DeleteLocalRef(jmManifestDigest);

    env->DeleteLocalRef(verificationParamsClass);

    CVerificationParams::New(mVerificationURI, mOriginatingURI, mReferrer, mOriginatingUid, mManifestDigest, verificationParams);

    if (*verificationParams == NULL) {
        ALOGE("CVerificationParams::New fail!");
        return FALSE;
    }

    (*verificationParams)->SetInstallerUid(mInstallerUid);

    return TRUE;
}

bool ElUtil::ToElNetworkInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ INetworkInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElNetworkInfo: Invalid argumenet!");
        return false;
    }

    jclass netInfoKlass = env->FindClass("android/net/NetworkInfo");
    CheckErrorAndLog(env, "FindClass: NetworkInfo : %d!\n", __LINE__);

    Int32 type = ElUtil::GetJavaIntField(env, netInfoKlass, jinfo, "mNetworkType", "ToElNetworkInfo");
    Int32 subtype = ElUtil::GetJavaIntField(env, netInfoKlass, jinfo, "mSubtype", "ToElNetworkInfo");
    String typeName = ElUtil::GetJavaStringField(env, netInfoKlass, jinfo, "mTypeName", "ToElNetworkInfo");
    String subtypeName = ElUtil::GetJavaStringField(env, netInfoKlass, jinfo, "mSubtypeName", "ToElNetworkInfo");

    if (NOERROR != CNetworkInfo::New(type, subtype, typeName, subtypeName, info)) {
        ALOGD("ToElNetworkInfo: create CNetworkInfo fail!");
        return false;
    }

    jfieldID f = env->GetFieldID(netInfoKlass, "mDetailedState", "Landroid/net/NetworkInfo$DetailedState;");
    CheckErrorAndLog(env, "GetFieldID: mState : %d!\n", __LINE__);

    jobject jdetailedState = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField mState : %d!\n", __LINE__);
    if (jdetailedState != NULL) {
        jclass niClass = env->FindClass("android/net/NetworkInfo$DetailedState");
        CheckErrorAndLog(env, "FindClass: NetworkInfo$DetailedState : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "IDLE", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: IDLE : %d!\n", __LINE__);

        jobject jIDLE = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: IDLE : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "SCANNING", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: SCANNING : %d!\n", __LINE__);

        jobject jSCANNING = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: SCANNING : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "CONNECTING", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: CONNECTING : %d!\n", __LINE__);

        jobject jCONNECTING = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: jCONNECTING : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "AUTHENTICATING", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: AUTHENTICATING : %d!\n", __LINE__);

        jobject jAUTHENTICATING = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: AUTHENTICATING : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "OBTAINING_IPADDR", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: OBTAINING_IPADDR : %d!\n", __LINE__);

        jobject jOBTAINING_IPADDR = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: OBTAINING_IPADDR : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "CONNECTED", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: CONNECTED : %d!\n", __LINE__);

        jobject jCONNECTED = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: jCONNECTED : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "SUSPENDED", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: SUSPENDED : %d!\n", __LINE__);

        jobject jSUSPENDED = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: jSUSPENDED : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "DISCONNECTING", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: DISCONNECTING : %d!\n", __LINE__);

        jobject jDISCONNECTING = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: jDISCONNECTING : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "DISCONNECTED", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: DISCONNECTED : %d!\n", __LINE__);

        jobject jDISCONNECTED = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: DISCONNECTED : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "FAILED", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: FAILED : %d!\n", __LINE__);

        jobject jFAILED = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: FAILED : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "BLOCKED", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: BLOCKED : %d!\n", __LINE__);

        jobject jBLOCKED = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: BLOCKED : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "VERIFYING_POOR_LINK", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: VERIFYING_POOR_LINK : %d!\n", __LINE__);

        jobject jVERIFYING_POOR_LINK = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: VERIFYING_POOR_LINK : %d!\n", __LINE__);

        f = env->GetStaticFieldID(niClass, "CAPTIVE_PORTAL_CHECK", "Landroid/net/NetworkInfo$DetailedState;");
        CheckErrorAndLog(env, "GetStaticFieldID: CAPTIVE_PORTAL_CHECK : %d!\n", __LINE__);

        jobject jCAPTIVE_PORTAL_CHECK = env->GetStaticObjectField(niClass, f);
        CheckErrorAndLog(env, "GetStaticObjectField: CAPTIVE_PORTAL_CHECK : %d!\n", __LINE__);

        String reason = ElUtil::GetJavaStringField(env, netInfoKlass, jinfo, "mReason", "NetworkInfo");
        String extraInfo = ElUtil::GetJavaStringField(env, netInfoKlass, jinfo, "mExtraInfo", "NetworkInfo");


        if (env->IsSameObject(jdetailedState, jIDLE)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_IDLE, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jSCANNING)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_SCANNING, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jCONNECTING)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_CONNECTING, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jAUTHENTICATING)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_AUTHENTICATING, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jOBTAINING_IPADDR)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_OBTAINING_IPADDR, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jCONNECTED)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_CONNECTED, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jSUSPENDED)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_SUSPENDED, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jDISCONNECTING)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_DISCONNECTING, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jDISCONNECTED)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_DISCONNECTED, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jFAILED)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_FAILED, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jBLOCKED)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_BLOCKED, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jVERIFYING_POOR_LINK)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_VERIFYING_POOR_LINK, reason, extraInfo);
        }
        else if (env->IsSameObject(jdetailedState, jCAPTIVE_PORTAL_CHECK)) {
            (*info)->SetDetailedState(Elastos::Droid::Net::NetworkInfoDetailedState_CAPTIVE_PORTAL_CHECK, reason, extraInfo);
        }
        else {
            ALOGE("ToElNetworkInfo() jdetailedState unknown type!");
        }

        env->DeleteLocalRef(niClass);
        env->DeleteLocalRef(jdetailedState);
        env->DeleteLocalRef(jIDLE);
        env->DeleteLocalRef(jSCANNING);
        env->DeleteLocalRef(jCONNECTING);
        env->DeleteLocalRef(jAUTHENTICATING);
        env->DeleteLocalRef(jOBTAINING_IPADDR);
        env->DeleteLocalRef(jCONNECTED);
        env->DeleteLocalRef(jSUSPENDED);
        env->DeleteLocalRef(jDISCONNECTING);
        env->DeleteLocalRef(jDISCONNECTED);
        env->DeleteLocalRef(jFAILED);
        env->DeleteLocalRef(jBLOCKED);
        env->DeleteLocalRef(jVERIFYING_POOR_LINK);
        env->DeleteLocalRef(jCAPTIVE_PORTAL_CHECK);
    }

    Boolean isFailover = GetJavaBoolField(env, netInfoKlass, jinfo, "mIsFailover", "ToElNetworkInfo");
    (*info)->SetFailover(isFailover);

    Boolean isRoaming = GetJavaBoolField(env, netInfoKlass, jinfo, "mIsRoaming", "ToElNetworkInfo");
    (*info)->SetRoaming(isRoaming);

    Boolean isConnectedToProvisioningNetwork = GetJavaBoolField(env, netInfoKlass,
        jinfo, "mIsConnectedToProvisioningNetwork", "ToElNetworkInfo");
    (*info)->SetIsConnectedToProvisioningNetwork(isConnectedToProvisioningNetwork);

    Boolean isAvailable = GetJavaBoolField(env, netInfoKlass, jinfo, "mIsAvailable", "ToElNetworkInfo");
    (*info)->SetIsAvailable(isAvailable);

    env->DeleteLocalRef(netInfoKlass);

    return true;
}

bool ElUtil::ToElWifiInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IWifiInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElWifiInfo: Invalid argumenet!");
        return false;
    }

    jclass wifiInfoKlass = env->FindClass("android/net/wifi/WifiInfo");
    CheckErrorAndLog(env, "FindClass: WifiInfo : %d!\n", __LINE__);

    if (NOERROR != CWifiInfo::New(info)) {
        ALOGD("ToElWifiInfo: create CWifiInfo fail!");
        return false;
    }

    Int32 tempInt;
    tempInt = ElUtil::GetJavaIntField(env, wifiInfoKlass, jinfo, "mNetworkId", "ToElWifiInfo");
    (*info)->SetNetworkId(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, wifiInfoKlass, jinfo, "mRssi", "ToElWifiInfo");
    (*info)->SetRssi(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, wifiInfoKlass, jinfo, "mLinkSpeed", "ToElWifiInfo");
    (*info)->SetLinkSpeed(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, wifiInfoKlass, jinfo, "mFrequency", "ToElWifiInfo");
    (*info)->SetFrequency(tempInt);

    jfieldID f = env->GetFieldID(wifiInfoKlass, "mIpAddress", "Ljava/net/InetAddress;");
    CheckErrorAndLog(env, "GetFieldID: InetAddress : %d!\n", __LINE__);

    jobject jipAddress = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField mIpAddress : %d!\n", __LINE__);
    if (jipAddress != NULL) {
        AutoPtr<IInetAddress> ipAddress;
        if (!ElUtil::ToElInetAddress(env, jipAddress, (IInetAddress**)&ipAddress)) {
            ALOGD("ToElWifiInfo: ToElInetAddress fail!");
        }
        else {
            (*info)->SetInetAddress(ipAddress);
        }
        env->DeleteLocalRef(jipAddress);
    }

    f = env->GetFieldID(wifiInfoKlass, "mWifiSsid", "Landroid/net/wifi/WifiSsid;");
    CheckErrorAndLog(env, "GetFieldID: WifiSsid : %d!\n", __LINE__);

    jobject jwifiSsid = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField mWifiSsid : %d!\n", __LINE__);
    if (jwifiSsid != NULL) {
        AutoPtr<IWifiSsid> wifiSsid;
        if (!ElUtil::ToElWifiSsid(env, jwifiSsid, (IWifiSsid**)&wifiSsid)) {
            ALOGD("ToElWifiInfo: ToElInetAddress fail!");
        }
        else {
            (*info)->SetSSID(wifiSsid);
        }
        env->DeleteLocalRef(jwifiSsid);
    }

    String mBSSID = ElUtil::GetJavaStringField(env, wifiInfoKlass, jinfo, "mBSSID", "ToElWifiInfo");
    (*info)->SetBSSID(mBSSID);

    String mMacAddress = ElUtil::GetJavaStringField(env, wifiInfoKlass, jinfo, "mMacAddress", "ToElWifiInfo");
    (*info)->SetMacAddress(mMacAddress);

    Boolean hint = ElUtil::GetJavaBoolField(env, wifiInfoKlass, jinfo, "mMeteredHint", "ToElWifiInfo");
    (*info)->SetMeteredHint(hint);

    tempInt = ElUtil::GetJavaIntField(env, wifiInfoKlass, jinfo, "score", "ToElWifiInfo");
    (*info)->SetScore(tempInt);

    Double tempDouble;
    tempDouble = ElUtil::GetJavadoubleField(env, wifiInfoKlass, jinfo, "txSuccessRate", "ToElWifiInfo");
    (*info)->SetTxSuccessRate(tempDouble);

    tempDouble = ElUtil::GetJavadoubleField(env, wifiInfoKlass, jinfo, "txRetriesRate", "ToElWifiInfo");
    (*info)->SetTxRetriesRate(tempDouble);

    tempDouble = ElUtil::GetJavadoubleField(env, wifiInfoKlass, jinfo, "txBadRate", "ToElWifiInfo");
    (*info)->SetTxBadRate(tempDouble);

    tempDouble = ElUtil::GetJavadoubleField(env, wifiInfoKlass, jinfo, "rxSuccessRate", "ToElWifiInfo");
    (*info)->SetRxSuccessRate(tempDouble);

    tempInt = ElUtil::GetJavaIntField(env, wifiInfoKlass, jinfo, "badRssiCount", "ToElWifiInfo");
    (*info)->SetBadRssiCount(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, wifiInfoKlass, jinfo, "lowRssiCount", "ToElWifiInfo");
    (*info)->SetLowRssiCount(tempInt);

    f = env->GetFieldID(wifiInfoKlass, "mSupplicantState", "Landroid/net/wifi/SupplicantState;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: mSupplicantState : %d!\n", __LINE__);

    jobject jsupplicantState = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: mSupplicantState : %d!\n", __LINE__);
    if (jsupplicantState != NULL) {
        AutoPtr<ISupplicantState> supplicantState;
        if (!ElUtil::ToElSupplicantState(env, jsupplicantState, (ISupplicantState**)&supplicantState)) {
            ALOGE("ToElWifiInfo() ToElSupplicantState fail!");
        }
        else {
            (*info)->SetSupplicantState(supplicantState);
        }
        env->DeleteLocalRef(jsupplicantState);
    }

    env->DeleteLocalRef(wifiInfoKlass);

    return true;
}

bool ElUtil::ToElSupplicantState(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ SupplicantState* state)
{
    if (env == NULL || jinfo == NULL || state == NULL) {
        ALOGE("ToElSupplicantState: Invalid argumenet!");
        return false;
    }

    jclass ssKlass = env->FindClass("android/net/wifi/SupplicantState");
    CheckErrorAndLog(env, "FindClass: SupplicantState : %d!\n", __LINE__);

    jfieldID f = env->GetStaticFieldID(ssKlass, "DISCONNECTED", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: DISCONNECTED : %d!\n", __LINE__);

    jobject jDISCONNECTED = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: DISCONNECTED : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jDISCONNECTED)) {
        *state = Elastos::Droid::Wifi::SupplicantState_DISCONNECTED;
        env->DeleteLocalRef(jDISCONNECTED);
        return true;
    }
    env->DeleteLocalRef(jDISCONNECTED);

    f = env->GetStaticFieldID(ssKlass, "INTERFACE_DISABLED", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: INTERFACE_DISABLED : %d!\n", __LINE__);

    jobject jINTERFACE_DISABLED = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: INTERFACE_DISABLED : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jINTERFACE_DISABLED)) {
        *state = Elastos::Droid::Wifi::SupplicantState_INTERFACE_DISABLED;
        env->DeleteLocalRef(jINTERFACE_DISABLED);
        return true;
    }
    env->DeleteLocalRef(jINTERFACE_DISABLED);

    f = env->GetStaticFieldID(ssKlass, "INACTIVE", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: INACTIVE : %d!\n", __LINE__);

    jobject jINACTIVE = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: INACTIVE : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jINACTIVE)) {
        *state = Elastos::Droid::Wifi::SupplicantState_INACTIVE;
        env->DeleteLocalRef(jINACTIVE);
        return true;
    }
    env->DeleteLocalRef(jINACTIVE);

    f = env->GetStaticFieldID(ssKlass, "SCANNING", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: SCANNING : %d!\n", __LINE__);

    jobject jSCANNING = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: SCANNING : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jSCANNING)) {
        *state = Elastos::Droid::Wifi::SupplicantState_SCANNING;
        env->DeleteLocalRef(jSCANNING);
        return true;
    }
    env->DeleteLocalRef(jSCANNING);

    f = env->GetStaticFieldID(ssKlass, "AUTHENTICATING", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: AUTHENTICATING : %d!\n", __LINE__);

    jobject jAUTHENTICATING = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: AUTHENTICATING : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jAUTHENTICATING)) {
        *state = Elastos::Droid::Wifi::SupplicantState_AUTHENTICATING;
        env->DeleteLocalRef(jAUTHENTICATING);
        return true;
    }
    env->DeleteLocalRef(jAUTHENTICATING);

    f = env->GetStaticFieldID(ssKlass, "ASSOCIATING", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: ASSOCIATING : %d!\n", __LINE__);

    jobject jASSOCIATING = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: ASSOCIATING : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jASSOCIATING)) {
        *state = Elastos::Droid::Wifi::SupplicantState_ASSOCIATING;
        env->DeleteLocalRef(jASSOCIATING);
        return true;
    }
    env->DeleteLocalRef(jASSOCIATING);

    f = env->GetStaticFieldID(ssKlass, "ASSOCIATED", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: ASSOCIATED : %d!\n", __LINE__);

    jobject jASSOCIATED = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: ASSOCIATED : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jASSOCIATED)) {
        *state = Elastos::Droid::Wifi::SupplicantState_ASSOCIATED;
        env->DeleteLocalRef(jASSOCIATED);
        return true;
    }
    env->DeleteLocalRef(jASSOCIATED);

    f = env->GetStaticFieldID(ssKlass, "FOUR_WAY_HANDSHAKE", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: FOUR_WAY_HANDSHAKE : %d!\n", __LINE__);

    jobject jFOUR_WAY_HANDSHAKE = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: FOUR_WAY_HANDSHAKE : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jFOUR_WAY_HANDSHAKE)) {
        *state = Elastos::Droid::Wifi::SupplicantState_FOUR_WAY_HANDSHAKE;
        env->DeleteLocalRef(jFOUR_WAY_HANDSHAKE);
        return true;
    }
    env->DeleteLocalRef(jFOUR_WAY_HANDSHAKE);

    f = env->GetStaticFieldID(ssKlass, "GROUP_HANDSHAKE", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: GROUP_HANDSHAKE : %d!\n", __LINE__);

    jobject jGROUP_HANDSHAKE = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: GROUP_HANDSHAKE : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jGROUP_HANDSHAKE)) {
        *state = Elastos::Droid::Wifi::SupplicantState_GROUP_HANDSHAKE;
        env->DeleteLocalRef(jGROUP_HANDSHAKE);
        return true;
    }
    env->DeleteLocalRef(jGROUP_HANDSHAKE);

    f = env->GetStaticFieldID(ssKlass, "COMPLETED", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: COMPLETED : %d!\n", __LINE__);

    jobject jCOMPLETED = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: COMPLETED : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jCOMPLETED)) {
        *state = Elastos::Droid::Wifi::SupplicantState_COMPLETED;
        env->DeleteLocalRef(jCOMPLETED);
        return true;
    }
    env->DeleteLocalRef(jCOMPLETED);

    f = env->GetStaticFieldID(ssKlass, "DORMANT", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: DORMANT : %d!\n", __LINE__);

    jobject jDORMANT = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: DORMANT : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jDORMANT)) {
        *state = Elastos::Droid::Wifi::SupplicantState_DORMANT;
        env->DeleteLocalRef(jDORMANT);
        return true;
    }
    env->DeleteLocalRef(jDORMANT);

    f = env->GetStaticFieldID(ssKlass, "UNINITIALIZED", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: UNINITIALIZED : %d!\n", __LINE__);

    jobject jUNINITIALIZED = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: UNINITIALIZED : %d!\n", __LINE__);

    if (env->IsSameObject(jinfo, jUNINITIALIZED)) {
        *state = Elastos::Droid::Wifi::SupplicantState_UNINITIALIZED;
        env->DeleteLocalRef(jUNINITIALIZED);
        return true;
    }
    env->DeleteLocalRef(jUNINITIALIZED);

    f = env->GetStaticFieldID(ssKlass, "INVALID", "Landroid/net/wifi/SupplicantState;");
    CheckErrorAndLog(env, "GetStaticFieldID: INVALID : %d!\n", __LINE__);

    jobject jINVALID = env->GetStaticObjectField(ssKlass, f);
    CheckErrorAndLog(env, "GetStaticObjectField: INVALID : %d!\n", __LINE__);
    env->DeleteLocalRef(ssKlass);

    if (env->IsSameObject(jinfo, jINVALID)) {
        *state = Elastos::Droid::Wifi::SupplicantState_INVALID;
        env->DeleteLocalRef(jINVALID);
        return true;
    }
    env->DeleteLocalRef(jINVALID);

    return false;
}

bool ElUtil::ToElSupplicantState(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jstate,
    /* [out] */ ISupplicantState** state)
{
    if (env == NULL || jstate == NULL || state == NULL) {
        ALOGE("ToElSupplicantState: Invalid argumenet!");
        *state = NULL;
        return false;
    }

    SupplicantState suppState;
    if (!ElUtil::ToElSupplicantState(env, jstate, &suppState)) {
        ALOGE("ToElSupplicantState()) ToElSupplicantState fail!");
        *state = NULL;
        return false;
    }

    if (NOERROR != CSupplicantState::New(suppState, state)) {
        ALOGE("ToElSupplicantState: Invalid argumenet!");
        *state = NULL;
        return false;
    }

    return true;
}

bool ElUtil::ToElInetAddress(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jaddr,
    /* [out] */ IInetAddress** addr)
{
    if (env == NULL || jaddr == NULL || addr == NULL) {
        ALOGE("ToElInetAddress: Invalid argumenet!");
        *addr = NULL;
        return false;
    }

    jclass inaddKlass = env->FindClass("java/net/InetAddress");
    CheckErrorAndLog(env, "ToElInetAddress FindClass: InetAddress : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(inaddKlass, "getAddress", "()[B");
    CheckErrorAndLog(env, "ToElInetAddress GetMethodID getAddress : %d!\n", __LINE__);

    jbyteArray jipaddress = (jbyteArray)env->CallObjectMethod(jaddr, m);
    CheckErrorAndLog(env, "ToElInetAddress CallObjectMethod getAddress : %d!\n", __LINE__);

    AutoPtr<ArrayOf<Byte> > ipAddress;
    if (!ElUtil::ToElByteArray(env, jipaddress, (ArrayOf<Byte>**)&ipAddress)) {
        ALOGE("ToElInetAddress: ToElByteArray fail!");
    }
    env->DeleteLocalRef(jipaddress);

    jfieldID f = env->GetFieldID(inaddKlass, "hostName", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetMethodID hostName : %d!\n", __LINE__);
    env->DeleteLocalRef(inaddKlass);

    jstring jhostName = (jstring)env->GetObjectField(jaddr, f);
    CheckErrorAndLog(env, "GetObjectField token : %d!\n", __LINE__);

    String hostName = ElUtil::ToElString(env, jhostName);
    env->DeleteLocalRef(jhostName);

    AutoPtr<IInetAddressHelper> helper;
    CInetAddressHelper::AcquireSingleton((IInetAddressHelper**)&helper);
    if (NOERROR != helper->GetByAddress(hostName, ipAddress, addr)) {
        ALOGE("ToElInetAddress: GetByAddress fail!!");
        return false;
    }

    return true;
}

bool ElUtil::ToElWifiSsid(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jwifiSsid,
    /* [out] */ IWifiSsid** wifiSsid)
{
    if (env == NULL || jwifiSsid == NULL || wifiSsid == NULL) {
        ALOGE("ToElWifiSsid: Invalid argumenet!");
        *wifiSsid = NULL;
        return false;
    }

    jclass ssidKlass = env->FindClass("android/net/wifi/WifiSsid");
    CheckErrorAndLog(env, "ToElWifiSsid FindClass: WifiSsid : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(ssidKlass, "getHexString", "()Ljava/lang/String;");
    CheckErrorAndLog(env, "ToElWifiSsid GetMethodID: getHexString : %d!\n", __LINE__);

    jstring jhexString = (jstring)env->CallObjectMethod(jwifiSsid, m);
    CheckErrorAndLog(env, "ToElWifiSsid CallObjectMethod getHexString : %d!\n", __LINE__);

    String hexString = ElUtil::ToElString(env, jhexString);

    AutoPtr<IWifiSsidHelper> helper;
    CWifiSsidHelper::AcquireSingleton((IWifiSsidHelper**)&helper);

    helper->CreateFromHex(hexString, wifiSsid);

    env->DeleteLocalRef(ssidKlass);
    env->DeleteLocalRef(jhexString);

    return true;
}

bool ElUtil::ToElLinkAddress(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jlinkAddr,
    /* [out] */ ILinkAddress** linkAddr)
{
    if (env == NULL || jlinkAddr == NULL || linkAddr == NULL) {
        ALOGE("ToElLinkAddress: Invalid argumenet!");
        *linkAddr = NULL;
        return false;
    }

    jclass linkaddrKlass = env->FindClass("android/net/LinkAddress");
    CheckErrorAndLog(env, "ToElLinkAddress FindClass: LinkAddress : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(linkaddrKlass, "address", "Ljava/net/InetAddress;");
    CheckErrorAndLog(env, "ToElLinkAddress GetFieldID: address : %d!\n", __LINE__);

    jobject jaddress = env->GetObjectField(jlinkAddr, f);
    CheckErrorAndLog(env, "ToElLinkAddress GetObjectField address : %d!\n", __LINE__);
    AutoPtr<IInetAddress> address;
    if (jaddress != NULL) {
        if (!ElUtil::ToElInetAddress(env, jaddress, (IInetAddress**)&address)) {
            ALOGE("ToElLinkAddress ToElInetAddress fail!");
        }
        env->DeleteLocalRef(jaddress);
    }

    Int32 prefixLength = GetJavaIntField(env, linkaddrKlass, jlinkAddr, "prefixLength", "ToElLinkAddress");
    Int32 flags = GetJavaIntField(env, linkaddrKlass, jlinkAddr, "flags", "ToElLinkAddress");
    Int32 scope = GetJavaIntField(env, linkaddrKlass, jlinkAddr, "scope", "ToElLinkAddress");

    env->DeleteLocalRef(linkaddrKlass);

    if (NOERROR != CLinkAddress::New(address, prefixLength, flags, scope, linkAddr)) {
        ALOGE("ToElLinkAddress: create CLinkAddress fail!");
        return false;
    }

    return true;
}

jobject ElUtil::GetJavaRunningTaskInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityManagerRunningTaskInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaRunningTaskInfo() Invalid arguments!");
        return NULL;
    }

    jobject jInfo = NULL;

    jclass rtInfoKlass = env->FindClass("android/app/ActivityManager$RunningTaskInfo");
    CheckErrorAndLog(env, "GetJavaRunningTaskInfo FindClass: RunningTaskInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(rtInfoKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaRunningTaskInfo GetMethodID: RunningTaskInfo : %d!\n", __LINE__);

    jInfo = env->NewObject(rtInfoKlass, m);
    CheckErrorAndLog(env, "GetJavaRunningTaskInfo NewObject: RunningTaskInfo : %d!\n", __LINE__);

    Int32 tempInt;
    info->GetId(&tempInt);
    SetJavaIntField(env, rtInfoKlass, jInfo, tempInt, "id", "GetJavaRunningTaskInfo");

    AutoPtr<IComponentName> baseActivity;
    info->GetBaseActivity((IComponentName**)&baseActivity);
    if (baseActivity != NULL) {
        jobject jbaseActivity = ElUtil::GetJavaComponentName(env, baseActivity);

        jfieldID f = env->GetFieldID(rtInfoKlass, "baseActivity", "Landroid/content/ComponentName;");
        ElUtil::CheckErrorAndLog(env, "GetJavaRunningTaskInfo GetFieldID: baseActivity : %d!\n", __LINE__);

        env->SetObjectField(jInfo, f, jbaseActivity);
        ElUtil::CheckErrorAndLog(env, "GetJavaRunningTaskInfo Fail SetObjectField: baseActivity : %d!\n", __LINE__);
        env->DeleteLocalRef(jbaseActivity);
    }

    AutoPtr<IComponentName> topActivity;
    info->GetTopActivity((IComponentName**)&topActivity);
    if (topActivity != NULL) {
        jobject jtopActivity = ElUtil::GetJavaComponentName(env, topActivity);

        jfieldID f = env->GetFieldID(rtInfoKlass, "topActivity", "Landroid/content/ComponentName;");
        ElUtil::CheckErrorAndLog(env, "GetJavaRunningTaskInfo GetFieldID: topActivity : %d!\n", __LINE__);

        env->SetObjectField(jInfo, f, jtopActivity);
        ElUtil::CheckErrorAndLog(env, "GetJavaRunningTaskInfo Fail SetObjectField: topActivity : %d!\n", __LINE__);
        env->DeleteLocalRef(jtopActivity);
    }

    AutoPtr<IBitmap> thumbnail;
    info->GetThumbnail((IBitmap**)&thumbnail);
    if (thumbnail != NULL) {
        jobject jthumbnail = ElUtil::GetJavaBitmap(env, thumbnail);

        jfieldID f = env->GetFieldID(rtInfoKlass, "thumbnail", "Landroid/graphics/Bitmap;");
        ElUtil::CheckErrorAndLog(env, "GetJavaRunningTaskInfo GetFieldID: thumbnail : %d!\n", __LINE__);

        env->SetObjectField(jInfo, f, jthumbnail);
        ElUtil::CheckErrorAndLog(env, "GetJavaRunningTaskInfo Fail SetObjectField: thumbnail : %d!\n", __LINE__);
        env->DeleteLocalRef(jthumbnail);
    }

    AutoPtr<ICharSequence> description;
    info->GetDescription((ICharSequence**)&description);
    if (description != NULL) {
        String sdescription;
        description->ToString(&sdescription);
        jstring jdescription = ElUtil::GetJavaString(env, sdescription);

        jfieldID f = env->GetFieldID(rtInfoKlass, "description", "Ljava/lang/CharSequence;");
        ElUtil::CheckErrorAndLog(env, "GetJavaRunningTaskInfo GetFieldID: CharSequence : %d!\n", __LINE__);

        env->SetObjectField(jInfo, f, jdescription);
        ElUtil::CheckErrorAndLog(env, "GetJavaRunningTaskInfo SetObjectField: jdescription : %d!\n", __LINE__);
        env->DeleteLocalRef(jdescription);
    }

    info->GetNumActivities(&tempInt);
    SetJavaIntField(env, rtInfoKlass, jInfo, tempInt, "numActivities", "GetJavaRunningTaskInfo");

    info->GetNumRunning(&tempInt);
    SetJavaIntField(env, rtInfoKlass, jInfo, tempInt, "numRunning", "GetJavaRunningTaskInfo");

    env->DeleteLocalRef(rtInfoKlass);

    return jInfo;
}

bool ElUtil::ToElExtractedText(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jert,
    /* [out] */ IExtractedText** ert)
{
    if (env == NULL || jert == NULL || ert == NULL) {
        ALOGE("ToElExtractedText: Invalid argumenet!");
        *ert = NULL;
        return false;
    }

    jclass ertKlass = env->FindClass("android/view/inputmethod/ExtractedText");
    CheckErrorAndLog(env, "ToElExtractedText FindClass: ExtractedText : %d!\n", __LINE__);

    if (NOERROR != CExtractedText::New(ert)) {
        ALOGE("ToElExtractedText: create CExtractedText fail!");
        *ert = NULL;
        return false;
    }

    jfieldID f = env->GetFieldID(ertKlass, "text", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "ToElExtractedText GetFieldID text : %d!\n", __LINE__);

    jobject jtext= env->GetObjectField(jert, f);
    CheckErrorAndLog(env, "ToElExtractedText GetObjectField text : %d!\n", __LINE__);
    AutoPtr<ICharSequence> text;
    if (jtext != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        CheckErrorAndLog(env, "ToElExtractedText FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        CheckErrorAndLog(env, "ToElExtractedText GetMethodID toString : %d!\n", __LINE__);

        jstring jstext = (jstring)env->CallObjectMethod(jtext, m);
        CheckErrorAndLog(env, "ToElExtractedText CallObjectMethod toString : %d!\n", __LINE__);

        String stext = ElUtil::ToElString(env, jstext);
        CString::New(stext, (ICharSequence**)&text);

        (*ert)->SetText(text);
        env->DeleteLocalRef(csClass);
        env->DeleteLocalRef(jtext);
        env->DeleteLocalRef(jstext);
    }

    Int32 tempInt = GetJavaIntField(env, ertKlass, jert, "startOffset", "ToElExtractedText");
    (*ert)->SetStartOffset(tempInt);

    tempInt = GetJavaIntField(env, ertKlass, jert, "partialStartOffset", "ToElExtractedText");
    (*ert)->SetPartialStartOffset(tempInt);

    tempInt = GetJavaIntField(env, ertKlass, jert, "partialEndOffset", "ToElExtractedText");
    (*ert)->SetPartialEndOffset(tempInt);

    tempInt = GetJavaIntField(env, ertKlass, jert, "selectionStart", "ToElExtractedText");
    (*ert)->SetSelectionStart(tempInt);

    tempInt = GetJavaIntField(env, ertKlass, jert, "selectionEnd", "ToElExtractedText");
    (*ert)->SetSelectionEnd(tempInt);

    tempInt = GetJavaIntField(env, ertKlass, jert, "flags", "ToElExtractedText");
    (*ert)->SetFlags(tempInt);

    env->DeleteLocalRef(ertKlass);

    return true;
}

bool ElUtil::ToElExtractedTextRequest(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jertq,
    /* [out] */ IExtractedTextRequest** ertq)
{
    if (env == NULL || jertq == NULL || ertq == NULL) {
        ALOGE("ToElExtractedTextRequest: Invalid argumenet!");
        *ertq = NULL;
        return false;
    }

    jclass ertKlass = env->FindClass("android/view/inputmethod/ExtractedTextRequest");
    CheckErrorAndLog(env, "ToElExtractedTextRequest FindClass: ExtractedTextRequest : %d!\n", __LINE__);

    if (NOERROR != CExtractedTextRequest::New(ertq)) {
        ALOGE("ToElExtractedTextRequest: create CExtractedText fail!");
        *ertq = NULL;
        return false;
    }

    Int32 tempInt = GetJavaIntField(env, ertKlass, jertq, "token", "ToElExtractedTextRequest");
    (*ertq)->SetToken(tempInt);

    tempInt = GetJavaIntField(env, ertKlass, jertq, "flags", "ToElExtractedTextRequest");
    (*ertq)->SetFlags(tempInt);

    tempInt = GetJavaIntField(env, ertKlass, jertq, "hintMaxLines", "ToElExtractedTextRequest");
    (*ertq)->SetHintMaxLines(tempInt);

    tempInt = GetJavaIntField(env, ertKlass, jertq, "hintMaxChars", "ToElExtractedTextRequest");
    (*ertq)->SetHintMaxChars(tempInt);

    env->DeleteLocalRef(ertKlass);

    return true;
}

bool ElUtil::ToElCorrectionInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jcorrection,
    /* [out] */ ICorrectionInfo** correction)
{
    if (env == NULL || jcorrection == NULL || correction == NULL) {
        ALOGE("ToElCorrectionInfo: Invalid argumenet!");
        *correction = NULL;
        return false;
    }

    jclass ciKlass = env->FindClass("android/view/inputmethod/CorrectionInfo");
    CheckErrorAndLog(env, "ToElCorrectionInfo FindClass: CorrectionInfo : %d!\n", __LINE__);

    Int32 offset = GetJavaIntField(env, ciKlass, jcorrection, "mOffset", "ToElCorrectionInfo");

    jclass csClass = env->FindClass("java/lang/CharSequence");
    CheckErrorAndLog(env, "ToElCorrectionInfo FindClass CharSequence : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
    CheckErrorAndLog(env, "ToElCorrectionInfo GetMethodID toString : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(ciKlass, "mOldText", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "ToElCorrectionInfo GetFieldID mOldText : %d!\n", __LINE__);

    jobject joldText = env->GetObjectField(jcorrection, f);
    CheckErrorAndLog(env, "ToElCorrectionInfo GetObjectField mOldText : %d!\n", __LINE__);

    AutoPtr<ICharSequence> oldText;
    if (joldText != NULL) {
        jstring jsoldText = (jstring)env->CallObjectMethod(joldText, m);
        CheckErrorAndLog(env, "ToElCorrectionInfo CallObjectMethod toString : %d!\n", __LINE__);

        String soldText = ElUtil::ToElString(env, jsoldText);
        env->DeleteLocalRef(jsoldText);

        CString::New(soldText, (ICharSequence**)&oldText);
        env->DeleteLocalRef(joldText);
    }

    f = env->GetFieldID(ciKlass, "mNewText", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "ToElCorrectionInfo GetFieldID mNewText : %d!\n", __LINE__);

    jobject jnewText = env->GetObjectField(jcorrection, f);
    CheckErrorAndLog(env, "ToElCorrectionInfo GetObjectField mNewText : %d!\n", __LINE__);

    AutoPtr<ICharSequence> newText;
    if (jnewText != NULL) {
        jstring jsnewText = (jstring)env->CallObjectMethod(jnewText, m);
        CheckErrorAndLog(env, "ToElCorrectionInfo CallObjectMethod toString : %d!\n", __LINE__);

        String snewText = ElUtil::ToElString(env, jsnewText);
        env->DeleteLocalRef(jsnewText);

        CString::New(snewText, (ICharSequence**)&newText);
        env->DeleteLocalRef(jnewText);
    }

    if (NOERROR != CCorrectionInfo::New(offset, oldText, newText, correction)) {
        ALOGE("ToElCorrectionInfo: create CCorrectionInfo fail!");
        *correction = NULL;
        return false;
    }

    env->DeleteLocalRef(ciKlass);
    env->DeleteLocalRef(csClass);

    return true;
}

static ECode TextUtils_WriteWhere(
    /* [in] */ IParcel* p,
    /* [in] */ ISpanned* sp,
    /* [in] */ IInterface* o)
{
    Int32 start, end, flags;
    sp->GetSpanStart(o, &start);
    FAIL_RETURN(p->WriteInt32(start));
    sp->GetSpanEnd(o, &end);
    FAIL_RETURN(p->WriteInt32(end));
    sp->GetSpanFlags(o, &flags);
    FAIL_RETURN(p->WriteInt32(flags));
    return NOERROR;
}

static ECode TextUtils_WriteToParcel(
    /* [in] */ ICharSequence* cs,
    /* [in] */ IParcel* p)
{
    ISpanned* sp = ISpanned::Probe(cs);
    if (sp) {
        FAIL_RETURN(p->WriteInt32(0));
        String str;
        cs->ToString(&str);
        FAIL_RETURN(p->WriteString(str));


        AutoPtr<ISpanned> sp = ISpanned::Probe(cs);
        AutoPtr<ArrayOf<IInterface*> > os;
        Int32 len;
        cs->GetLength(&len);
        sp->GetSpans(0, len, EIID_IInterface, (ArrayOf<IInterface*>**)&os);

        // note to people adding to this: check more specific types
        // before more generic types.  also notice that it uses
        // "if" instead of "else if" where there are interfaces
        // so one object can be several.

        len = os->GetLength();
        for (Int32 i = 0; i < len; i++) {
            AutoPtr<IInterface> o = (*os)[i];
            AutoPtr<IInterface> prop = (*os)[i];

            AutoPtr<ICharacterStyle> temp = ICharacterStyle::Probe(prop);
            if (temp != NULL) {
                prop = NULL;
                temp->GetUnderlying((ICharacterStyle**)&prop);
            }

            AutoPtr<IParcelableSpan> ps = IParcelableSpan::Probe(prop);
            if (ps != NULL) {
                Int32 typeId;
                ps->GetSpanTypeId(&typeId);
                FAIL_RETURN(p->WriteInt32(typeId));
                FAIL_RETURN(IParcelable::Probe(ps)->WriteToParcel(p));
                FAIL_RETURN(TextUtils_WriteWhere(p, sp, o));
            }
        }

        FAIL_RETURN(p->WriteInt32(0));
   }
   else {
       FAIL_RETURN(p->WriteInt32(1));
       if (cs != NULL) {
            String str;
            cs->ToString(&str);
            FAIL_RETURN(p->WriteString(str));
       }
       else {
            FAIL_RETURN(p->WriteString(String(NULL)));
       }
   }

   return NOERROR;
}

bool ElUtil::ToElClipData(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jclip,
    /* [out] */ IClipData** clip)
{
    if (env == NULL || jclip == NULL || clip == NULL) {
        ALOGE("ToElClipData: Invalid argumenet!");
        *clip = NULL;
        return false;
    }

    jclass clipKlass = env->FindClass("android/content/ClipData");
    CheckErrorAndLog(env, "ToElClipData FindClass: ClipData : %d!\n", __LINE__);

    if (NOERROR != CClipData::New(clip)) {
        ALOGE("ToElClipData: create CCorrectionInfo fail!");
        *clip = NULL;
        return false;
    }

    AutoPtr<IParcel> source;
    CParcel::New((IParcel**)&source);

    jfieldID f = env->GetFieldID(clipKlass, "mClipDescription", "Landroid/content/ClipDescription;");
    CheckErrorAndLog(env, "GetFieldID mClipDescription : %d!\n", __LINE__);

    jobject jclipDescription = env->GetObjectField(jclip, f);
    CheckErrorAndLog(env, "GetObjectField mClipDescription : %d!\n", __LINE__);

    AutoPtr<IClipDescription> clipDescription;
    if (jclipDescription != NULL) {
        if (!ElUtil::ToElClipDescription(env, jclipDescription, (IClipDescription**)&clipDescription)) {
            ALOGE("ToElClipData() ToElClipDescription fail!");
        }
    }

    source->WriteInterfacePtr(clipDescription);

    f = env->GetFieldID(clipKlass, "mIcon", "Landroid/graphics/Bitmap;");
    CheckErrorAndLog(env, "GetFieldID mIcon : %d!\n", __LINE__);

    jobject jicon= env->GetObjectField(jclip, f);
    CheckErrorAndLog(env, "GetObjectField mIcon : %d!\n", __LINE__);

    AutoPtr<IBitmap> icon;
    if (jicon != NULL) {
        if (!ElUtil::ToElBitmap(env, jicon, (IBitmap**)&icon)) {
            ALOGE("ToElClipData ToElBitmap fail!");
        }

        source->WriteInt32(1);
        source->WriteInterfacePtr(icon);
        env->DeleteLocalRef(jicon);
    }
    else {
        source->WriteInt32(0);
    }

    f = env->GetFieldID(clipKlass, "mItems", "Ljava/util/ArrayList;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: mItems %d", __LINE__);

    jobject jitems = env->GetObjectField(jclip, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: mItems %d", __LINE__);

    if (jitems != NULL) {
        jclass alistKlass = env->FindClass("java/util/ArrayList");
        CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(alistKlass, "size", "()I");
        CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

        jint jsize = env->CallIntMethod(jitems, m);
        CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

        source->WriteInt32((Int32)jsize);

        jmethodID mGet = env->GetMethodID(alistKlass, "get", "(I)Ljava/lang/Object;");
        CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);
        if (jsize > 0) {
            for (jint i = 0; i < jsize; i++) {
                jobject jitem = env->CallObjectMethod(jitems, mGet, i);
                CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

                AutoPtr<IClipDataItem> item;
                if (!ElUtil::ToElClipDataItem(env, jitem, (IClipDataItem**)&item)) {
                    ALOGE("ToElClipData() ToElClipDataItem fail!");
                }

                AutoPtr<ICharSequence> text;
                item->GetText((ICharSequence**)&text);
                TextUtils_WriteToParcel(text, source); // TextUtils::WriteToParcel(text, source);

                String htmlText;
                item->GetHtmlText(&htmlText);
                source->WriteString(htmlText);

                AutoPtr<IIntent> intent;
                item->GetIntent((IIntent**)&intent);
                if (intent != NULL) {
                    source->WriteInt32(1);
                    source->WriteInterfacePtr(intent);
                }
                else {
                    source->WriteInt32(0);
                }

                AutoPtr<IUri> uri;
                item->GetUri((IUri**)&uri);
                if (uri != NULL) {
                    source->WriteInt32(1);
                    IParcelable::Probe(uri)->WriteToParcel(source);
                }
                else {
                    source->WriteInt32(0);
                }

                env->DeleteLocalRef(jitem);
            }
        }
        env->DeleteLocalRef(alistKlass);
        env->DeleteLocalRef(jitems);
    }
    else {
        source->WriteInt32(0);
    }

    source->SetDataPosition(0);
    AutoPtr<IParcelable> clipParcel = IParcelable::Probe(*clip);
    if (clipParcel != NULL) {
        clipParcel->ReadFromParcel(source);
    }
    else {
        ALOGE("ToElClipData() Get IParcelable  from IClipData failed!");
        return false;
    }

    env->DeleteLocalRef(clipKlass);
    return true;
}

bool ElUtil::ToElClipDataItem(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jitem,
    /* [out] */ IClipDataItem** item)
{
    if (env == NULL || jitem == NULL || item == NULL) {
        ALOGE("ToElClipDataItem: Invalid argumenet!");
        *item = NULL;
        return false;
    }

    jclass cdiKlass = env->FindClass("android/content/ClipData$Item");
    CheckErrorAndLog(env, "FindClass: ClipData$Item : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(cdiKlass, "mText", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID mText : %d!\n", __LINE__);

    jobject jtext = env->GetObjectField(jitem, f);
    CheckErrorAndLog(env, "GetObjectField mText : %d!\n", __LINE__);

    AutoPtr<ICharSequence> text;
    if (jtext != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jstext = (jstring)env->CallObjectMethod(jtext, m);
        CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String stext = ElUtil::ToElString(env, jstext);
        CString::New(stext, (ICharSequence**)&text);

        env->DeleteLocalRef(csClass);
        env->DeleteLocalRef(jstext);
        env->DeleteLocalRef(jtext);
    }

    String htmlText = ElUtil::GetJavaStringField(env, cdiKlass, jitem, "mHtmlText", "ToElClipDataItem");

    f = env->GetFieldID(cdiKlass, "mIntent", "Landroid/content/Intent;");
    CheckErrorAndLog(env, "GetFieldID mIntent : %d!\n", __LINE__);

    jobject jintent = env->GetObjectField(jitem, f);
    CheckErrorAndLog(env, "GetObjectField mIntent : %d!\n", __LINE__);

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("ToElClipDataItem() ToElIntent fail!");
        }
        env->DeleteLocalRef(jintent);
    }

    f = env->GetFieldID(cdiKlass, "mUri", "Landroid/net/Uri;");
    CheckErrorAndLog(env, "GetFieldID mUri : %d!\n", __LINE__);

    jobject juri = env->GetObjectField(jitem, f);
    CheckErrorAndLog(env, "GetObjectField mUri : %d!\n", __LINE__);

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("ToElClipDataItem() ToElUri fail!");
        }
        env->DeleteLocalRef(juri);
    }

    if (NOERROR != CClipDataItem::New(text, htmlText, intent, uri, item)) {
        ALOGE("ToElClipDataItem: create CClipDataItem fail!");
        *item = NULL;
        return false;
    }

    env->DeleteLocalRef(cdiKlass);
    return true;
}

bool ElUtil::ToElClipDescription(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jclipDesc,
    /* [out] */ IClipDescription** clipDesc)
{
    if (env == NULL || jclipDesc == NULL || clipDesc == NULL) {
        ALOGE("ToElClipDescription: Invalid argumenet!");
        *clipDesc = NULL;
        return false;
    }

    jclass cdKlass = env->FindClass("android/content/ClipDescription");
    CheckErrorAndLog(env, "FindClass: ClipDescription : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(cdKlass, "mLabel", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID mLabel : %d!\n", __LINE__);

    jobject jlabel = env->GetObjectField(jclipDesc, f);
    CheckErrorAndLog(env, "GetObjectField mLabel : %d!\n", __LINE__);

    AutoPtr<ICharSequence> label;
    if (jlabel != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jslabel = (jstring)env->CallObjectMethod(jlabel, m);
        CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String slabel = ElUtil::ToElString(env, jslabel);
        CString::New(slabel, (ICharSequence**)&label);

        env->DeleteLocalRef(csClass);
        env->DeleteLocalRef(jslabel);
        env->DeleteLocalRef(jlabel);
    }

    f = env->GetFieldID(cdKlass, "mMimeTypes", "[Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID mMimeTypes : %d!\n", __LINE__);

    jobjectArray jmimeTypes = (jobjectArray)env->GetObjectField(jclipDesc, f);
    CheckErrorAndLog(env, "GetObjectField mMimeTypes : %d!\n", __LINE__);

    AutoPtr<ArrayOf<String> > mimeTypes;
    if (jmimeTypes != NULL) {
        if (!ElUtil::ToElStringArray(env, jmimeTypes, (ArrayOf<String>**)&mimeTypes)) {
            ALOGE("ToElClipDescription() ToElStringArray fail!");
        }
    }

    if (NOERROR != CClipDescription::New(label, mimeTypes, clipDesc)) {
        ALOGE("ToElClipDescription: create CClipDescription fail!");
        *clipDesc = NULL;
        return false;
    }

    env->DeleteLocalRef(cdKlass);
    return true;
}

jobject ElUtil::GetJavaClipDescription(
    /* [in] */ JNIEnv* env,
    /* [in] */ IClipDescription* clipDesc)
{
    if (env == NULL || clipDesc == NULL) {
        ALOGE("GetJavaClipDescription() Invalid arguments!");
        return NULL;
    }

    jclass cdKlass = env->FindClass("android/content/ClipDescription");
    CheckErrorAndLog(env, "FindClass: ClipDescription : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(cdKlass, "<init>", "(Ljava/lang/CharSequence;[Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetMethodID: ClipDescription : %d!\n", __LINE__);

    AutoPtr<ICharSequence> label;
    clipDesc->GetLabel((ICharSequence**)&label);
    jobject jlabel = NULL;
    if (label != NULL) {
        String slabel;
        label->ToString(&slabel);
        jlabel = GetJavaString(env, slabel);
    }

    Int32 count = 0;
    clipDesc->GetMimeTypeCount(&count);
    jobjectArray jmimeTypes = NULL;
    if (count > 0) {
        AutoPtr<ArrayOf<String> > mimeTypes = ArrayOf<String>::Alloc(count);
        for (Int32 i = 0; i < count; i++) {
            String mimeType;
            clipDesc->GetMimeType(i, &mimeType);
            (*mimeTypes)[i] = mimeType;
        }

        jmimeTypes = ElUtil::GetJavaStringArray(env, mimeTypes);
    }

    jobject jclipDesc = env->NewObject(cdKlass, m, jlabel, jmimeTypes);
    CheckErrorAndLog(env, "NewObject: ClipDescription : %d!\n", __LINE__);

    env->DeleteLocalRef(cdKlass);
    return jclipDesc;
}

jobject ElUtil::GetJavaSearchableInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ ISearchableInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaSearchableInfo() Invalid arguments!");
        return NULL;
    }

    AutoPtr<IParcel> source;
    CParcel::New((IParcel**)&source);

    IParcelable::Probe(info)->WriteToParcel(source);
    source->SetDataPosition(0);

    jclass parcelClass = env->FindClass("android/os/Parcel");
    ElUtil::CheckErrorAndLog(env, "FindClass: Parcel : %d!\n", __LINE__);

    jmethodID m = env->GetStaticMethodID(parcelClass, "obtain", "()Landroid/os/Parcel;");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: obtain : %d!\n", __LINE__);

    jobject jparcel = env->CallStaticObjectMethod(parcelClass, m);
    ElUtil::CheckErrorAndLog(env, "CallStaticObjectMethod: obtain : %d!\n", __LINE__);

    Int64 nativePtr = ElUtil::GetJavalongField(env, parcelClass, jparcel, "mNativePtr", "GetJavaSearchableInfo");
    android::Parcel* parcel = reinterpret_cast< android::Parcel*>(nativePtr);

    Int32 temp;
    //mLabelId
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mSearchActivity
    String str;
    AutoPtr<ArrayOf<Char16> > char16s;
    source->ReadString(&str);
    if (str != NULL) {
        char16s = str.GetChar16s();
        parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
        source->ReadString(&str);
        char16s = str.GetChar16s();
        parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
    }
    else {
        parcel->writeString16(NULL, 0);
    }
    //mHintId
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mSearchMode
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mIconId
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mSearchButtonText
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mSearchInputType
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mSearchImeOptions
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mIncludeInGlobalSearch
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mQueryAfterZeroResults
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mAutoUrlDetect
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mSettingsDescriptionId
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mSuggestAuthority
    source->ReadString(&str);
    char16s = str.GetChar16s();
    parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
    //mSuggestPath
    source->ReadString(&str);
    char16s = str.GetChar16s();
    parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
    //mSuggestSelection
    source->ReadString(&str);
    char16s = str.GetChar16s();
    parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
    //mSuggestIntentAction
    source->ReadString(&str);
    char16s = str.GetChar16s();
    parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
    //mSuggestIntentData
    source->ReadString(&str);
    char16s = str.GetChar16s();
    parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
    //mSuggestThreshold
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);

    Int32 count;
    source->ReadInt32(&count);
    parcel->writeInt32(count);
    for(; count > 0; count--) {
        // ActionKeyInfo.mKeyCode
        source->ReadInt32(&temp);
        parcel->writeInt32(temp);
        // ActionKeyInfo.mQueryActionMsg
        source->ReadString(&str);
        char16s = str.GetChar16s();
        parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
        // ActionKeyInfo.mSuggestActionMsg
        source->ReadString(&str);
        char16s = str.GetChar16s();
        parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
        // ActionKeyInfo.mSuggestActionMsgColumn
        source->ReadString(&str);
        char16s = str.GetChar16s();
        parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
    }

    //mSuggestProviderPackage
    source->ReadString(&str);
    char16s = str.GetChar16s();
    parcel->writeString16(char16s->GetPayload(), char16s->GetLength());
    //mVoiceSearchMode
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mVoiceLanguageModeId
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mVoicePromptTextId
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mVoiceLanguageId
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);
    //mVoiceMaxResults
    source->ReadInt32(&temp);
    parcel->writeInt32(temp);

    parcel->setDataPosition(0);

    jclass siKlass = env->FindClass("android/app/SearchableInfo");
    CheckErrorAndLog(env, "FindClass: SearchableInfo : %d!\n", __LINE__);

    m = env->GetMethodID(siKlass, "<init>", "(Landroid/os/Parcel;)V");
    CheckErrorAndLog(env, "GetMethodID: SearchableInfo : %d!\n", __LINE__);

    jobject jinfo = env->NewObject(siKlass, m, jparcel);
    CheckErrorAndLog(env, "NewObject: SearchableInfo : %d!\n", __LINE__);

    env->DeleteLocalRef(parcelClass);
    env->DeleteLocalRef(jparcel);
    env->DeleteLocalRef(siKlass);

    return jinfo;
}

bool ElUtil::ToElContainerEncryptionParams(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jparams,
    /* [out] */ IContainerEncryptionParams** params)
{
    if (env == NULL || jparams == NULL || params == NULL) {
        ALOGE("ToElContainerEncryptionParams: Invalid argumenet!");
        *params = NULL;
        return false;
    }

    jclass clipKlass = env->FindClass("android/content/pm/ContainerEncryptionParams");
    CheckErrorAndLog(env, "ToElContainerEncryptionParams FindClass: ContainerEncryptionParams : %d!\n", __LINE__);

    String encryptionAlgorithm = ElUtil::GetJavaStringField(env, clipKlass, jparams, "mEncryptionAlgorithm", "ToElContainerEncryptionParams");

    jfieldID f = env->GetFieldID(clipKlass, "mEncryptionSpec", "Ljava/security/spec/AlgorithmParameterSpec;");
    CheckErrorAndLog(env, "GetFieldID mEncryptionSpec : %d!\n", __LINE__);

    jobject jencryptionSpec = env->GetObjectField(jparams, f);
    CheckErrorAndLog(env, "GetObjectField mEncryptionSpec : %d!\n", __LINE__);

    AutoPtr<IAlgorithmParameterSpec> encryptionSpec;
    if (jencryptionSpec != NULL) {
        ALOGE("ToElContainerEncryptionParams() jencryptionSpec NOT NULL!");
    }

    f = env->GetFieldID(clipKlass, "mEncryptionKey", "Ljavax/crypto/SecretKey;");
    CheckErrorAndLog(env, "GetFieldID mEncryptionKey : %d!\n", __LINE__);

    jobject jencryptionKey = env->GetObjectField(jparams, f);
    CheckErrorAndLog(env, "GetObjectField mEncryptionKey : %d!\n", __LINE__);

    AutoPtr<Elastosx::Crypto::ISecretKey> encryptionKey;
    if (jencryptionKey != NULL) {
        ALOGE("ToElContainerEncryptionParams() jencryptionKey NOT NULL!");
    }

    String macAlgorithm = ElUtil::GetJavaStringField(env, clipKlass, jparams, "mMacAlgorithm", "ToElContainerEncryptionParams");

    f = env->GetFieldID(clipKlass, "mMacSpec", "Ljava/security/spec/AlgorithmParameterSpec;");
    CheckErrorAndLog(env, "GetFieldID mMacSpec : %d!\n", __LINE__);

    jobject jmacSpec = env->GetObjectField(jparams, f);
    CheckErrorAndLog(env, "GetObjectField mMacSpec : %d!\n", __LINE__);

    AutoPtr<Elastos::Security::Spec::IAlgorithmParameterSpec> macSpec;
    if (jmacSpec != NULL) {
        ALOGE("ToElContainerEncryptionParams() jmacSpec NOT NULL!");
    }

    f = env->GetFieldID(clipKlass, "mMacKey", "Ljavax/crypto/SecretKey;");
    CheckErrorAndLog(env, "GetFieldID mMacKey : %d!\n", __LINE__);

    jobject jmacKey = env->GetObjectField(jparams, f);
    CheckErrorAndLog(env, "GetObjectField mMacKey : %d!\n", __LINE__);

    AutoPtr<Elastosx::Crypto::ISecretKey> macKey;
    if (jmacKey != NULL) {
        ALOGE("ToElContainerEncryptionParams() jmacKey NOT NULL!");
    }

    f = env->GetFieldID(clipKlass, "mMacTag", "[B");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: mMacTag %d", __LINE__);

    jbyteArray jmacTag = (jbyteArray)env->GetObjectField(jparams, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: mMacTag %d", __LINE__);

    AutoPtr<ArrayOf<Byte> > macTag;
    if (jmacTag != NULL) {
        if (!ElUtil::ToElByteArray(env, jmacTag, (ArrayOf<Byte>**)&macTag)) {
            ALOGE("ToElContainerEncryptionParams ToElByteArray fail!");
        }

        env->DeleteLocalRef(jmacTag);
    }

    Int64 authenticatedDataStart = ElUtil::GetJavalongField(env, clipKlass, jparams, "mAuthenticatedDataStart", "ToElContainerEncryptionParams");
    Int64 encryptedDataStart = ElUtil::GetJavalongField(env, clipKlass, jparams, "mEncryptedDataStart", "ToElContainerEncryptionParams");
    Int64 dataEnd = ElUtil::GetJavalongField(env, clipKlass, jparams, "mDataEnd", "ToElContainerEncryptionParams");

    env->DeleteLocalRef(clipKlass);

    if (NOERROR != CContainerEncryptionParams::New(encryptionAlgorithm, encryptionSpec, encryptionKey, macAlgorithm, macSpec,
             macKey, macTag, authenticatedDataStart, encryptedDataStart, dataEnd, params)) {
        ALOGE("ToElContainerEncryptionParams: create CContainerEncryptionParams fail!");
        *params = NULL;
        return false;
    }

    return false;
}

bool ElUtil::ToElIvParameterSpec(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jspec,
    /* [out] */ IIvParameterSpec** spec)
{
    if (env == NULL || jspec == NULL || spec == NULL) {
        ALOGE("ToElIvParameterSpec: Invalid argumenet!");
        *spec = NULL;
        return false;
    }

    jclass ipsKlass = env->FindClass("javax/crypto/spec/IvParameterSpec");
    CheckErrorAndLog(env, "FindClass: IvParameterSpec : %d!", __LINE__);

    jfieldID f = env->GetFieldID(ipsKlass, "iv", "[B");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: iv %d", __LINE__);

    jbyteArray jiv = (jbyteArray)env->GetObjectField(jspec, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: iv %d", __LINE__);

    AutoPtr<ArrayOf<Byte> > iv;
    if (jiv != NULL) {
        if (!ElUtil::ToElByteArray(env, jiv, (ArrayOf<Byte>**)&iv)) {
            ALOGE("ToElIvParameterSpec ToElByteArray fail!");
        }

        env->DeleteLocalRef(jiv);
    }

    if (NOERROR != CIvParameterSpec::New(iv, spec)) {
        ALOGE("ToElIvParameterSpec create CIvParameterSpec fail! %d", __LINE__);
    }

    env->DeleteLocalRef(ipsKlass);
    return false;
}

bool ElUtil::ToElParcelFileDescriptor(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jfDescriptor,
    /* [out] */ IParcelFileDescriptor** fDescriptor)
{
    if (env == NULL || jfDescriptor == NULL || fDescriptor == NULL) {
        ALOGE("ToElParcelFileDescriptor: Invalid argumenet!");
        *fDescriptor = NULL;
        return false;
    }

    jclass pfdKlass = env->FindClass("android/os/ParcelFileDescriptor");
    CheckErrorAndLog(env, "ToElParcelFileDescriptor FindClass: ParcelFileDescriptor : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(pfdKlass, "mFileDescriptor", "Ljava/io/FileDescriptor;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: mFileDescriptor %d", __LINE__);

    jobject jfd = env->GetObjectField(jfDescriptor, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: mFileDescriptor %d", __LINE__);

    AutoPtr<IFileDescriptor> fd;
    if (!ElUtil::ToElFileDescriptor(env, jfd, (IFileDescriptor**)&fd)) {
        ALOGE("ToElParcelFileDescriptor() ToElFileDescriptor() fail %d", __LINE__);
    }

    env->DeleteLocalRef(jfd);

    if (NOERROR != CParcelFileDescriptor::New(fd, fDescriptor)) {
        ALOGE("ToElParcelFileDescriptor() create CParcelFileDescriptor fail!");
        return false;
    }

    env->DeleteLocalRef(pfdKlass);
    return true;
}

bool ElUtil::ToElFileDescriptor(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jfDescptor,
    /* [out] */ IFileDescriptor** fDescptor) {
    if (env == NULL || jfDescptor == NULL || fDescptor == NULL) {
        ALOGE("ToElParcelFileDescriptor: Invalid argumenet!");
        *fDescptor = NULL;
        return false;
    }

    if (NOERROR != CFileDescriptor::New(fDescptor)) {
        ALOGE("ToElFileDescriptor() create CFileDescriptor fail!");
        return false;
    }

    jclass fdKlass = env->FindClass("java/io/FileDescriptor");
    ElUtil::CheckErrorAndLog(env, "FindClass: FileDescriptor %d", __LINE__);

    Int32 descriptor = ElUtil::GetJavaIntField(env, fdKlass, jfDescptor, "descriptor", "ToElFileDescriptor");

    Int32 fd = dup(descriptor);

    (*fDescptor)->SetDescriptor(fd);
    env->DeleteLocalRef(fdKlass);
    return true;
}

jobject ElUtil::GetJavaPackageInfoLite(
    /* [in] */ JNIEnv* env,
    /* [in] */ IPackageInfoLite* pkgLite)
{
    if (env == NULL || pkgLite == NULL) {
        ALOGE("GetJavaPackageInfoLite() Invalid arguments!");
        return NULL;
    }

    jclass pilKlass = env->FindClass("android/content/pm/PackageInfoLite");
    ElUtil::CheckErrorAndLog(env, "FindClass: PackageInfoLite : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(pilKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: PackageInfoLite() : %d!\n", __LINE__);

    jobject jpkgLite = env->NewObject(pilKlass, m);
    ElUtil::CheckErrorAndLog(env, "NewObject: PackageInfoLite : %d!\n", __LINE__);

    String packageName;
    pkgLite->GetPackageName(&packageName);
    ElUtil::SetJavaStringField(env, pilKlass, jpkgLite, packageName, "packageName", "GetJavaPackageInfoLite");

    Int32 tempInt = 0;
    pkgLite->GetVersionCode(&tempInt);
    ElUtil::SetJavaIntField(env, pilKlass, jpkgLite, tempInt, "versionCode", "GetJavaPackageInfoLite");

    pkgLite->GetRecommendedInstallLocation(&tempInt);
    ElUtil::SetJavaIntField(env, pilKlass, jpkgLite, tempInt, "recommendedInstallLocation", "GetJavaPackageInfoLite");

    pkgLite->GetInstallLocation(&tempInt);
    ElUtil::SetJavaIntField(env, pilKlass, jpkgLite, tempInt, "installLocation", "GetJavaPackageInfoLite");

    Boolean multiArch;
    pkgLite->GetMultiArch(&multiArch);
    ElUtil::SetJavaBoolField(env, pilKlass, jpkgLite, multiArch, "multiArch", "GetJavaPackageInfoLite");

    Boolean isTheme;
    pkgLite->GetIsTheme(&isTheme);
    ElUtil::SetJavaBoolField(env, pilKlass, jpkgLite, isTheme, "isTheme", "GetJavaPackageInfoLite");

    AutoPtr<ArrayOf<IVerifierInfo*> > verifiers;
    pkgLite->GetVerifiers((ArrayOf<IVerifierInfo*>**)&verifiers);
    if (verifiers != NULL) {
        jclass viKlass = env->FindClass("android/content/pm/VerifierInfo");
        ElUtil::CheckErrorAndLog(env, "FindClass: VerifierInfo : %d!\n", __LINE__);

        Int32 count = verifiers->GetLength();
        jobjectArray jverifiers = env->NewObjectArray((jsize)count, viKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: VerifierInfo : %d!\n", __LINE__);

        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IVerifierInfo> verifier = (*verifiers)[i];

            jobject jverifier = ElUtil::GetJavaVerifierInfo(env, verifier);

            env->SetObjectArrayElement(jverifiers, i, jverifier);
            CheckErrorAndLog(env, "SetObjectArrayElement: VerifierInfo : %d!\n", __LINE__);

            env->DeleteLocalRef(jverifier);
        }

        jfieldID f = env->GetFieldID(pilKlass, "verifiers", "Landroid/content/pm/VerifierInfo;");
        CheckErrorAndLog(env, "GetFieldID verifiers : %d!\n", __LINE__);

        env->SetObjectField(jpkgLite, f, jverifiers);
        CheckErrorAndLog(env, "SetObjectField verifiers : %d!\n", __LINE__);

        env->DeleteLocalRef(viKlass);
    }

    env->DeleteLocalRef(pilKlass);
    return jpkgLite;
}

jobject ElUtil::GetJavaObbInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IObbInfo* obb)
{
    if (env == NULL || obb == NULL) {
        ALOGE("GetJavaObbInfo() Invalid arguments!");
        return NULL;
    }

    jclass oiKlass = env->FindClass("android/content/res/ObbInfo");
    ElUtil::CheckErrorAndLog(env, "FindClass: ObbInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(oiKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: ObbInfo() : %d!\n", __LINE__);

    jobject jobb = env->NewObject(oiKlass, m);
    ElUtil::CheckErrorAndLog(env, "NewObject: ObbInfo : %d!\n", __LINE__);

    String filename;
    obb->GetFilename(&filename);
    ElUtil::SetJavaStringField(env, oiKlass, jobb, filename, "filename", "GetJavaObbInfo");

    String packageName;
    obb->GetPackageName(&packageName);
    ElUtil::SetJavaStringField(env, oiKlass, jobb, packageName, "packageName", "GetJavaObbInfo");

    Int32 tempInt = 0;
    obb->GetVersion(&tempInt);
    ElUtil::SetJavaIntField(env, oiKlass, jobb, tempInt, "version", "GetJavaObbInfo");

    obb->GetFlags(&tempInt);
    ElUtil::SetJavaIntField(env, oiKlass, jobb, tempInt, "flags", "GetJavaObbInfo");

    AutoPtr<ArrayOf<Byte> > salt;
    obb->GetSalt((ArrayOf<Byte>**)&salt);
    if (salt != NULL) {
        jbyteArray jsalt = GetJavaByteArray(env, salt);

        jfieldID f = env->GetFieldID(oiKlass, "salt", "[B");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: salt : %d!", __LINE__);

        env->SetObjectField(jobb, f, jsalt);
        ElUtil::CheckErrorAndLog(env, "SetObjectField: salt : %d!", __LINE__);

        env->DeleteLocalRef(jsalt);
    }

    env->DeleteLocalRef(oiKlass);
    return jobb;
}

bool ElUtil::ToElStorageVolume(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jvolume,
    /* [out] */ IStorageVolume** volume)
{
    if (env == NULL || jvolume == NULL || volume == NULL) {
        ALOGE("ToElStorageVolume: Invalid argumenet!");
        *volume = NULL;
        return false;
    }

    jclass volKlass = env->FindClass("android/os/storage/StorageVolume");
    CheckErrorAndLog(env, "ToElStorageVolume FindClass: StorageVolume : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(volKlass, "mPath", "Ljava/io/File;");
    CheckErrorAndLog(env, "ToElStorageVolume GetFieldID File : %d!\n", __LINE__);

    jobject jpath = env->GetObjectField(jvolume, f);
    CheckErrorAndLog(env, "ToElStorageVolume GetObjectField File : %d!\n", __LINE__);

    AutoPtr<IFile> path;
    if (jpath != NULL) {
        if (!ElUtil::ToElFile(env, jpath, (IFile**)&path)) {
            ALOGE("ToElStorageVolume ToElFile fail!");
        }
        env->DeleteLocalRef(jpath);
    }

    Int32 descriptionId = ElUtil::GetJavaIntField(env, volKlass, jvolume, "mDescriptionId", "ToElStorageVolume");
    Boolean primary = ElUtil::GetJavaBoolField(env, volKlass, jvolume, "mPrimary", "ToElStorageVolume");
    Boolean removable = ElUtil::GetJavaBoolField(env, volKlass, jvolume, "mRemovable", "ToElStorageVolume");
    Boolean emulated = ElUtil::GetJavaBoolField(env, volKlass, jvolume, "mEmulated", "ToElStorageVolume");
    Int32 mtpReserveSpace = ElUtil::GetJavaIntField(env, volKlass, jvolume, "mMtpReserveSpace", "ToElStorageVolume");
    Boolean allowMassStorage = ElUtil::GetJavaBoolField(env, volKlass, jvolume, "mAllowMassStorage", "ToElStorageVolume");
    Int64 maxFileSize = ElUtil::GetJavalongField(env, volKlass, jvolume, "mMaxFileSize", "ToElStorageVolume");

    f = env->GetFieldID(volKlass, "mOwner", "Landroid/os/UserHandle;");
    CheckErrorAndLog(env, "ToElStorageVolume GetFieldID UserHandle : %d!\n", __LINE__);

    jobject jowner = env->GetObjectField(jvolume, f);
    CheckErrorAndLog(env, "ToElStorageVolume GetObjectField UserHandle : %d!\n", __LINE__);

    AutoPtr<IUserHandle> owner;
    if (jowner != NULL) {
        if (!ElUtil::ToElUserHandle(env, jowner, (IUserHandle**)&owner)) {
            ALOGE("ToElStorageVolume ToElUserHandle fail!");
        }
        env->DeleteLocalRef(jowner);
    }

    if (NOERROR != CStorageVolume::New(path, descriptionId, primary, removable, emulated, mtpReserveSpace,
            allowMassStorage, maxFileSize, owner, volume)) {
        ALOGE("ToElStorageVolume: create CStorageVolume fail!");
        *volume = NULL;
        return false;
    }

    Int32 storageId = GetJavaIntField(env, volKlass, jvolume, "mStorageId", "ToElStorageVolume");
    (*volume)->SetStorageId(storageId);

    String uuid = GetJavaStringField(env, volKlass, jvolume, "mUuid", "ToElStorageVolume");
    (*volume)->SetUuid(uuid);

    String userLabel = GetJavaStringField(env, volKlass, jvolume, "mUserLabel", "ToElStorageVolume");
    (*volume)->SetUserLabel(userLabel);

    String state = GetJavaStringField(env, volKlass, jvolume, "mState", "ToElStorageVolume");
    (*volume)->SetState(state);

    env->DeleteLocalRef(volKlass);

    return true;
}

bool ElUtil::ToElFile(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jfile,
    /* [out] */ IFile** file)
{
    if (env == NULL || jfile == NULL || file == NULL) {
        ALOGE("ToElFile: Invalid argumenet!");
        *file = NULL;
        return false;
    }

    jclass fileKlass = env->FindClass("java/io/File");
    CheckErrorAndLog(env, "ToElFile FindClass: File : %d!\n", __LINE__);

    String path = ElUtil::GetJavaStringField(env, fileKlass, jfile, "path", "ToElFile");

    env->DeleteLocalRef(fileKlass);

    if (NOERROR != CFile::New(path, file)) {
        ALOGE("ToElFile: create CFile fail!");
        *file = NULL;
        return false;
    }

    return true;
}

bool ElUtil::ToElUserHandle(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jhandle,
    /* [out] */ IUserHandle** handle)
{
    if (env == NULL || jhandle == NULL || handle == NULL) {
        ALOGE("ToElFile: Invalid argumenet!");
        *handle = NULL;
        return false;
    }

    jclass hdleKlass = env->FindClass("android/os/UserHandle");
    CheckErrorAndLog(env, "ToElFile FindClass: UserHandle : %d!\n", __LINE__);

    Int32 ihandle = ElUtil::GetJavaIntField(env, hdleKlass, jhandle, "mHandle", "ToElUserHandle");

    env->DeleteLocalRef(hdleKlass);

    if (NOERROR != CUserHandle::New(ihandle, handle)) {
        ALOGE("ToElFile: create CFile fail!");
        *handle = NULL;
        return false;
    }

    return true;
}

bool ElUtil::ToElApplicationInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jappInfo,
    /* [out] */ IApplicationInfo** appInfo)
{
    if (env == NULL || jappInfo == NULL || appInfo == NULL) {
        ALOGE("ToElApplicationInfo() Invalid arguments!");
        return FALSE;
    }

    if (NOERROR != CApplicationInfo::New(appInfo)) {
        ALOGE("ToElApplicationInfo() Create CApplicationInfo fail!");
        return FALSE;
    }

    jclass appInfoKlass = env->FindClass("android/content/pm/ApplicationInfo");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo FindClass: ApplicationInfo %d", __LINE__);

    jfieldID f = env->GetFieldID(appInfoKlass, "taskAffinity", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: taskAffinity %d", __LINE__);

    jstring jtaskAffinity = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jtaskAffinity %d", __LINE__);
    if (jtaskAffinity != NULL) {
        String taskAffinity = ElUtil::ToElString(env, jtaskAffinity);
        env->DeleteLocalRef(jtaskAffinity);
        (*appInfo)->SetTaskAffinity(taskAffinity);
    }

    f = env->GetFieldID(appInfoKlass, "permission", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: permission %d", __LINE__);

    jstring jpermission = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jpermission %d", __LINE__);
    if (jpermission != NULL) {
        String permission = ElUtil::ToElString(env, jpermission);
        env->DeleteLocalRef(jpermission);
        (*appInfo)->SetPermission(permission);
    }

    f = env->GetFieldID(appInfoKlass, "processName", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: processName %d", __LINE__);

    jstring jprocessName = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jprocessName %d", __LINE__);
    if (jprocessName != NULL) {
        String processName = ElUtil::ToElString(env, jprocessName);
        env->DeleteLocalRef(jprocessName);
        (*appInfo)->SetProcessName(processName);
    }

    f = env->GetFieldID(appInfoKlass, "className", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: className %d", __LINE__);

    jstring jclassName = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jclassName %d", __LINE__);
    if (jclassName != NULL) {
        String className = ElUtil::ToElString(env, jclassName);
        env->DeleteLocalRef(jclassName);
        (*appInfo)->SetClassName(className);
    }

    f = env->GetFieldID(appInfoKlass, "theme", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: theme %d", __LINE__);

    jint jtheme = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: theme %d", __LINE__);
    (*appInfo)->SetTheme((Int32)jtheme);

    f = env->GetFieldID(appInfoKlass, "flags", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: flags %d", __LINE__);

    jint jflags = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: flags %d", __LINE__);
    (*appInfo)->SetFlags((Int32)jflags);

    f = env->GetFieldID(appInfoKlass, "requiresSmallestWidthDp", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: requiresSmallestWidthDp %d", __LINE__);

    jint jrequiresSmallestWidthDp = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: requiresSmallestWidthDp %d", __LINE__);
    (*appInfo)->SetRequiresSmallestWidthDp((Int32)jrequiresSmallestWidthDp);

    f = env->GetFieldID(appInfoKlass, "compatibleWidthLimitDp", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: compatibleWidthLimitDp %d", __LINE__);

    jint jcompatibleWidthLimitDp = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: compatibleWidthLimitDp %d", __LINE__);
    (*appInfo)->SetCompatibleWidthLimitDp((Int32)jcompatibleWidthLimitDp);

    f = env->GetFieldID(appInfoKlass, "largestWidthLimitDp", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: largestWidthLimitDp %d", __LINE__);

    jint jlargestWidthLimitDp = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: largestWidthLimitDp %d", __LINE__);
    (*appInfo)->SetLargestWidthLimitDp((Int32)jlargestWidthLimitDp);

    f = env->GetFieldID(appInfoKlass, "scanSourceDir", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: scanSourceDir %d", __LINE__);

    jstring jscanSourceDir = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetObjectField: jscanSourceDir %d", __LINE__);
    if (jscanSourceDir != NULL) {
        String scanSourceDir = ElUtil::ToElString(env, jscanSourceDir);
        env->DeleteLocalRef(jscanSourceDir);
        (*appInfo)->SetScanSourceDir(scanSourceDir);
    }

    f = env->GetFieldID(appInfoKlass, "scanPublicSourceDir", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: scanPublicSourceDir %d", __LINE__);

    jstring jscanPublicSourceDir = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetObjectField: jscanPublicSourceDir %d", __LINE__);
    if (jscanPublicSourceDir != NULL) {
        String scanPublicSourceDir = ElUtil::ToElString(env, jscanPublicSourceDir);
        env->DeleteLocalRef(jscanPublicSourceDir);
        (*appInfo)->SetScanPublicSourceDir(scanPublicSourceDir);
    }

    f = env->GetFieldID(appInfoKlass, "sourceDir", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: sourceDir %d", __LINE__);

    jstring jsourceDir = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jsourceDir %d", __LINE__);
    if (jsourceDir != NULL) {
        String sourceDir = ElUtil::ToElString(env, jsourceDir);
        env->DeleteLocalRef(jsourceDir);
        (*appInfo)->SetSourceDir(sourceDir);
    }

    f = env->GetFieldID(appInfoKlass, "publicSourceDir", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: publicSourceDir %d", __LINE__);

    jstring jpublicSourceDir = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jpublicSourceDir %d", __LINE__);
    if (jpublicSourceDir != NULL) {
        String publicSourceDir = ElUtil::ToElString(env, jpublicSourceDir);
        env->DeleteLocalRef(jpublicSourceDir);
        (*appInfo)->SetPublicSourceDir(publicSourceDir);
    }

    f = env->GetFieldID(appInfoKlass, "splitSourceDirs", "[Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: splitSourceDirs %d", __LINE__);

    jobjectArray jsplitSourceDirs = (jobjectArray)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetObjectField: jsplitSourceDirs %d", __LINE__);
    if (jsplitSourceDirs != NULL) {
        AutoPtr<ArrayOf<String> > splitSourceDirs;
        ToElStringArray(env, jsplitSourceDirs, (ArrayOf<String>**)&splitSourceDirs);
        if (splitSourceDirs != NULL) {
            (*appInfo)->SetSplitSourceDirs(splitSourceDirs);
        }

        env->DeleteLocalRef(jsplitSourceDirs);
    }

    f = env->GetFieldID(appInfoKlass, "splitPublicSourceDirs", "[Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: splitPublicSourceDirs %d", __LINE__);

    jobjectArray jsplitPublicSourceDirs = (jobjectArray)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetObjectField: jsplitPublicSourceDirs %d", __LINE__);
    if (jsplitPublicSourceDirs != NULL) {
        AutoPtr<ArrayOf<String> > splitPublicSourceDirs;
        ToElStringArray(env, jsplitPublicSourceDirs, (ArrayOf<String>**)&splitPublicSourceDirs);
        if (splitPublicSourceDirs != NULL) {
            (*appInfo)->SetSplitPublicSourceDirs(splitPublicSourceDirs);
        }

        env->DeleteLocalRef(jsplitPublicSourceDirs);
    }

    f = env->GetFieldID(appInfoKlass, "nativeLibraryDir", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: nativeLibraryDir %d", __LINE__);

    jstring jnativeLibraryDir = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jnativeLibraryDir %d", __LINE__);
    if (jnativeLibraryDir != NULL) {
        String nativeLibraryDir = ElUtil::ToElString(env, jnativeLibraryDir);
        env->DeleteLocalRef(jnativeLibraryDir);
        (*appInfo)->SetNativeLibraryDir(nativeLibraryDir);
    }


    f = env->GetFieldID(appInfoKlass, "secondaryNativeLibraryDir", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: secondaryNativeLibraryDir %d", __LINE__);

    jstring jsecondaryNativeLibraryDir = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetObjectField: jsecondaryNativeLibraryDir %d", __LINE__);
    if (jsecondaryNativeLibraryDir != NULL) {
        String secondaryNativeLibraryDir = ElUtil::ToElString(env, jsecondaryNativeLibraryDir);
        env->DeleteLocalRef(jsecondaryNativeLibraryDir);
        (*appInfo)->SetSecondaryNativeLibraryDir(secondaryNativeLibraryDir);
    }

    f = env->GetFieldID(appInfoKlass, "nativeLibraryRootDir", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: nativeLibraryRootDir %d", __LINE__);

    jstring jnativeLibraryRootDir = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetObjectField: jnativeLibraryRootDir %d", __LINE__);
    if (jnativeLibraryRootDir != NULL) {
        String nativeLibraryRootDir = ElUtil::ToElString(env, jnativeLibraryRootDir);
        env->DeleteLocalRef(jnativeLibraryRootDir);
        (*appInfo)->SetNativeLibraryRootDir(nativeLibraryRootDir);
    }

    f = env->GetFieldID(appInfoKlass, "nativeLibraryRootRequiresIsa", "Z");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: nativeLibraryRootRequiresIsa %d", __LINE__);

    jboolean jnativeLibraryRootRequiresIsa = env->GetBooleanField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetIntField: nativeLibraryRootRequiresIsa %d", __LINE__);
    (*appInfo)->SetNativeLibraryRootRequiresIsa((Boolean)jnativeLibraryRootRequiresIsa);

    f = env->GetFieldID(appInfoKlass, "primaryCpuAbi", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: primaryCpuAbi %d", __LINE__);

    jstring jprimaryCpuAbi = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetObjectField: jprimaryCpuAbi %d", __LINE__);
    if (jprimaryCpuAbi != NULL) {
        String primaryCpuAbi = ElUtil::ToElString(env, jprimaryCpuAbi);
        env->DeleteLocalRef(jprimaryCpuAbi);
        (*appInfo)->SetPrimaryCpuAbi(primaryCpuAbi);
    }

    f = env->GetFieldID(appInfoKlass, "secondaryCpuAbi", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: secondaryCpuAbi %d", __LINE__);

    jstring jsecondaryCpuAbi = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetObjectField: jsecondaryCpuAbi %d", __LINE__);
    if (jsecondaryCpuAbi != NULL) {
        String secondaryCpuAbi = ElUtil::ToElString(env, jsecondaryCpuAbi);
        env->DeleteLocalRef(jsecondaryCpuAbi);
        (*appInfo)->SetSecondaryCpuAbi(secondaryCpuAbi);
    }

    f = env->GetFieldID(appInfoKlass, "resourceDirs", "[Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: resourceDirs %d", __LINE__);

    jobjectArray jresourceDirs = (jobjectArray)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jresourceDirs %d", __LINE__);
    if (jresourceDirs != NULL) {
        jint jcount = env->GetArrayLength(jresourceDirs);
        if (jcount > 0) {
            AutoPtr<ArrayOf<String> > resourceDirs = ArrayOf<String>::Alloc((Int32)jcount);
            if (resourceDirs != NULL) {
                for (Int32 i = 0; i < jcount; i++) {
                    jstring jresourceDir = (jstring)env->GetObjectArrayElement(jresourceDirs, i);
                    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectArrayElement: jresourceDir%d", __LINE__);
                    String resourceDir = ElUtil::ToElString(env, jresourceDir);
                    env->DeleteLocalRef(jresourceDir);
                    resourceDirs->Set(i, resourceDir);
                }
                (*appInfo)->SetResourceDirs(resourceDirs);
            }
        }
        env->DeleteLocalRef(jresourceDirs);
    }

    f = env->GetFieldID(appInfoKlass, "seinfo", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: seinfo %d", __LINE__);

    jstring jseinfo = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetObjectField: jseinfo %d", __LINE__);
    if (jseinfo != NULL) {
        String seinfo = ElUtil::ToElString(env, jseinfo);
        env->DeleteLocalRef(jseinfo);
        (*appInfo)->SetSeinfo(seinfo);
    }

    f = env->GetFieldID(appInfoKlass, "sharedLibraryFiles", "[Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: sharedLibraryFiles %d", __LINE__);

    jobjectArray jsharedLibraryFiles = (jobjectArray)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jsharedLibraryFiles %d", __LINE__);
    if (jsharedLibraryFiles != NULL) {
        jint jcount = env->GetArrayLength(jsharedLibraryFiles);
        if (jcount > 0) {
            AutoPtr<ArrayOf<String> > sharedLibraryFiles = ArrayOf<String>::Alloc((Int32)jcount);
            if (sharedLibraryFiles != NULL) {
                for (Int32 i = 0; i < jcount; i++) {
                    jstring jsharedLibraryFile = (jstring)env->GetObjectArrayElement(jsharedLibraryFiles, i);
                    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectArrayElement: jsharedLibraryFile%d", __LINE__);
                    String sharedLibraryFile = ElUtil::ToElString(env, jsharedLibraryFile);
                    env->DeleteLocalRef(jsharedLibraryFile);
                    sharedLibraryFiles->Set(i, sharedLibraryFile);
                }
                (*appInfo)->SetResourceDirs(sharedLibraryFiles);
            }
        }
        env->DeleteLocalRef(jsharedLibraryFiles);
    }

    f = env->GetFieldID(appInfoKlass, "dataDir", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: dataDir %d", __LINE__);

    jstring jdataDir = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jdataDir %d", __LINE__);
    if (jdataDir != NULL) {
        String dataDir = ElUtil::ToElString(env, jdataDir);
        env->DeleteLocalRef(jdataDir);
        (*appInfo)->SetDataDir(dataDir);
    }

    f = env->GetFieldID(appInfoKlass, "uid", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: uid %d", __LINE__);

    jint juid = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: uid %d", __LINE__);
    (*appInfo)->SetUid((Int32)juid);

    f = env->GetFieldID(appInfoKlass, "targetSdkVersion", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: targetSdkVersion %d", __LINE__);

    jint jtargetSdkVersion = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: targetSdkVersion %d", __LINE__);
    (*appInfo)->SetTargetSdkVersion((Int32)jtargetSdkVersion);

    f = env->GetFieldID(appInfoKlass, "versionCode", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: versionCode %d", __LINE__);

    jint jversionCode = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetIntField: versionCode %d", __LINE__);
    (*appInfo)->SetVersionCode((Int32)jversionCode);

    f = env->GetFieldID(appInfoKlass, "enabled", "Z");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: enabled %d", __LINE__);

    jboolean jenabled = env->GetBooleanField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: enabled %d", __LINE__);
    (*appInfo)->SetEnabled((Boolean)jenabled);

    f = env->GetFieldID(appInfoKlass, "enabledSetting", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: enabledSetting %d", __LINE__);

    jint jenabledSetting = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: enabledSetting %d", __LINE__);
    (*appInfo)->SetEnabledSetting((Int32)jenabledSetting);

    f = env->GetFieldID(appInfoKlass, "installLocation", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: installLocation %d", __LINE__);

    jint jinstallLocation = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: installLocation %d", __LINE__);
    (*appInfo)->SetInstallLocation((Int32)jinstallLocation);

    f = env->GetFieldID(appInfoKlass, "manageSpaceActivityName", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: manageSpaceActivityName %d", __LINE__);

    jstring jmanageSpaceActivityName = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jmanageSpaceActivityName %d", __LINE__);
    if (jmanageSpaceActivityName != NULL) {
        String manageSpaceActivityName = ElUtil::ToElString(env, jmanageSpaceActivityName);
        env->DeleteLocalRef(jmanageSpaceActivityName);
        (*appInfo)->SetManageSpaceActivityName(manageSpaceActivityName);
    }

    f = env->GetFieldID(appInfoKlass, "descriptionRes", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: descriptionRes %d", __LINE__);

    jint jdescriptionRes = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: descriptionRes %d", __LINE__);
    (*appInfo)->SetDescriptionRes((Int32)jdescriptionRes);

    f = env->GetFieldID(appInfoKlass, "uiOptions", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: uiOptions %d", __LINE__);

    jint juiOptions = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: uiOptions %d", __LINE__);
    (*appInfo)->SetUiOptions((Int32)juiOptions);

    f = env->GetFieldID(appInfoKlass, "backupAgentName", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: backupAgentName %d", __LINE__);

    jstring jbackupAgentName = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jbackupAgentName %d", __LINE__);
    if (jbackupAgentName != NULL) {
        String backupAgentName = ElUtil::ToElString(env, jbackupAgentName);
        env->DeleteLocalRef(jbackupAgentName);
        (*appInfo)->SetBackupAgentName(backupAgentName);
    }

    f = env->GetFieldID(appInfoKlass, "protect", "Z");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: protect %d", __LINE__);

    jboolean jprotect = env->GetBooleanField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetIntField: protect %d", __LINE__);
    (*appInfo)->SetProtect((Boolean)jprotect);

    f = env->GetFieldID(appInfoKlass, "isThemeable", "Z");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: isThemeable %d", __LINE__);

    jboolean jisThemeable = env->GetBooleanField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetIntField: isThemeable %d", __LINE__);
    (*appInfo)->SetIsThemeable((Boolean)jisThemeable);

    // ----------------------------------- PackageItemInfo ---------------------------------------------------

    IPackageItemInfo* pkgInfo = IPackageItemInfo::Probe(*appInfo);

    f = env->GetFieldID(appInfoKlass, "name", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: name %d", __LINE__);

    jstring jname = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: jname %d", __LINE__);
    if (jname != NULL) {
        String name = ElUtil::ToElString(env, jname);
        env->DeleteLocalRef(jname);
        pkgInfo->SetName(name);
    }

    f = env->GetFieldID(appInfoKlass, "packageName", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: packageName %d", __LINE__);

    jstring jpackageName = (jstring)env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: jpackageName %d", __LINE__);
    if (jpackageName != NULL) {
        String packageName = ElUtil::ToElString(env, jpackageName);
        env->DeleteLocalRef(jpackageName);
        pkgInfo->SetPackageName(packageName);
    }

    f = env->GetFieldID(appInfoKlass, "labelRes", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: labelRes %d", __LINE__);

    jint jlabelRes = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: labelRes %d", __LINE__);
    pkgInfo->SetLabelRes((Int32)jlabelRes);

    f = env->GetFieldID(appInfoKlass, "nonLocalizedLabel", "Ljava/lang/CharSequence;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: CharSequence%d", __LINE__);

    jobject jcsnonLocalizedLabel = env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jcsnonLocalizedLabel%d", __LINE__);
    if (jcsnonLocalizedLabel != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo FindClass CharSequence %d", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetMethodID toString %d", __LINE__);

        jstring jnonLocalizedLabel = (jstring)env->CallObjectMethod(jcsnonLocalizedLabel, m);
        ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo CallObjectMethod toString %d", __LINE__);
        env->DeleteLocalRef(jcsnonLocalizedLabel);

        String snonLocalizedLabel = ElUtil::ToElString(env, jnonLocalizedLabel);
        env->DeleteLocalRef(jnonLocalizedLabel);

        AutoPtr<ICharSequence> nonLocalizedLabel;
        CString::New(snonLocalizedLabel, (ICharSequence**)&nonLocalizedLabel);

        pkgInfo->SetNonLocalizedLabel(nonLocalizedLabel);

        env->DeleteLocalRef(csClass);
    }

    f = env->GetFieldID(appInfoKlass, "icon", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: icon %d", __LINE__);

    jint jicon = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: icon %d", __LINE__);
    pkgInfo->SetIcon((Int32)jicon);

    f = env->GetFieldID(appInfoKlass, "logo", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: logo %d", __LINE__);

    jint jlogo = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetIntField: logo %d", __LINE__);
    pkgInfo->SetLogo((Int32)jlogo);

    f = env->GetFieldID(appInfoKlass, "metaData", "Landroid/os/Bundle;");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetFieldID: metaData%d", __LINE__);

    jobject jmetaData = env->GetObjectField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo GetObjectField: jmetaData%d", __LINE__);
    if (jmetaData != NULL) {
        AutoPtr<IBundle> metaData;
        if (ElUtil::ToElBundle(env, jmetaData, (IBundle**)&metaData)) {
            pkgInfo->SetMetaData(metaData);
        }
        else {
            ALOGE("ToElApplicationInfo() ToElBundle fail!");
        }

        env->DeleteLocalRef(jmetaData);
    }

    f = env->GetFieldID(appInfoKlass, "banner", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: banner %d", __LINE__);

    jint jbanner = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetIntField: banner %d", __LINE__);
    pkgInfo->SetBanner((Int32)jbanner);

    f = env->GetFieldID(appInfoKlass, "showUserIcon", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: showUserIcon %d", __LINE__);

    jint jshowUserIcon = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetIntField: showUserIcon %d", __LINE__);
    pkgInfo->SetShowUserIcon((Int32)jshowUserIcon);

    f = env->GetFieldID(appInfoKlass, "themedIcon", "I");
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetFieldID: themedIcon %d", __LINE__);

    jint jthemedIcon = env->GetIntField(jappInfo, f);
    ElUtil::CheckErrorAndLog(env, "ToElApplicationInfo Fail GetIntField: themedIcon %d", __LINE__);
    pkgInfo->SetThemedIcon((Int32)jthemedIcon);

    env->DeleteLocalRef(appInfoKlass);

    return TRUE;
}

jobject ElUtil::GetJavaParcelFileDescriptor(
    /* [in] */ JNIEnv* env,
    /* [in] */ IParcelFileDescriptor* descptor)
{
    if (env == NULL || descptor == NULL) {
        ALOGD("GetJavaParcelFileDescriptor() Invalid arguments!");
        return NULL;
    }

    jobject jdescptor = NULL;

    jclass fdKlass = env->FindClass("android/os/ParcelFileDescriptor");
    CheckErrorAndLog(env, "GetJavaParcelFileDescriptor FindClass: ParcelFileDescriptor : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(fdKlass, "<init>", "(Ljava/io/FileDescriptor;)V");
    CheckErrorAndLog(env, "GetJavaParcelFileDescriptor GetMethodID: ParcelFileDescriptor : %d!\n", __LINE__);

    AutoPtr<IFileDescriptor> fDescriptor;
    descptor->GetFileDescriptor((IFileDescriptor**)&fDescriptor);
    if (fDescriptor != NULL) {
        jobject jfDescriptor = ElUtil::GetJavaFileDescriptor(env, fDescriptor);

        jdescptor = env->NewObject(fdKlass, m, jfDescriptor);
        CheckErrorAndLog(env, "GetJavaParcelFileDescriptor NewObject: ParcelFileDescriptor : %d!\n", __LINE__);
        env->DeleteLocalRef(jfDescriptor);
    }
    else {
        ALOGE("GetJavaParcelFileDescriptor() fDescriptor cannot NULL");
    }

    descptor->Close();

    env->DeleteLocalRef(fdKlass);

    return jdescptor;
}

jobject ElUtil::GetJavaFileDescriptor(
    /* [in] */ JNIEnv* env,
    /* [in] */ IFileDescriptor* descptor)
{
    if (env == NULL || descptor == NULL) {
        ALOGD("GetJavaFileDescriptor() Invalid arguments!");
        return NULL;
    }

    jobject jdescptor = NULL;

    jclass fdKlass = env->FindClass("java/io/FileDescriptor");
    CheckErrorAndLog(env, "GetJavaFileDescriptor FindClass: FileDescriptor %d!", __LINE__);

    jmethodID m = env->GetMethodID(fdKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaFileDescriptor GetMethodID: FileDescriptor %d!", __LINE__);

    jdescptor = env->NewObject(fdKlass, m);
    CheckErrorAndLog(env, "GetJavaFileDescriptor NewObject: FileDescriptor %d!", __LINE__);

    Int32 origfd = -1;
    descptor->GetDescriptor(&origfd);

    int fd = dup(origfd);
    if (fd < 0) {
        ALOGE("GetJavaFileDescriptor() dup fail!");
        return jdescptor;
    }

    env->DeleteLocalRef(fdKlass);
    return jniCreateFileDescriptor(env, fd);
}

jobject ElUtil::GetJavaWallpaperInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWallpaperInfo* wallpaper)
{
    if (env == NULL || wallpaper == NULL) {
        ALOGD("GetJavaWallpaperInfo() Invalid arguments!");
        return NULL;
    }

    jobject jwallpaper = NULL;

    jclass wallKlass = env->FindClass("android/app/WallpaperInfo");
    CheckErrorAndLog(env, "GetJavaWallpaperInfo FindClass: WallpaperInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(wallKlass, "<init>", "(Landroid/content/pm/ResolveInfo;Ljava/lang/String;III)V");
    CheckErrorAndLog(env, "GetJavaWallpaperInfo GetMethodID: WallpaperInfo : %d!\n", __LINE__);

    AutoPtr<IResolveInfo> service;
    wallpaper->GetService((IResolveInfo**)&service);
    jobject jservice = NULL;
    if (service != NULL) {
        jservice = ElUtil::GetJavaResolveInfo(env, service);
    }

    String settingsActivityName;
    wallpaper->GetSettingsActivity(&settingsActivityName);
    jstring jsettingsActivityName = ElUtil::GetJavaString(env, settingsActivityName);

    Int32 thumbnailRes;
    wallpaper->GetThumbnailResource(&thumbnailRes);
    Int32 authorRes;
    wallpaper->GetAuthorResource(&authorRes);
    Int32 descriptionRes;
    wallpaper->GetDescriptionResource(&descriptionRes);

    jwallpaper = env->NewObject(wallKlass, m, jservice, jsettingsActivityName,
        (jint)authorRes, (jint)authorRes, (jint)descriptionRes);
    CheckErrorAndLog(env, "GetJavaWallpaperInfo NewObject: WallpaperInfo : %d!\n", __LINE__);

    env->DeleteLocalRef(wallKlass);
    env->DeleteLocalRef(jservice);
    env->DeleteLocalRef(jsettingsActivityName);
    return jwallpaper;
}

bool ElUtil::ToElLocale(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jlocale,
    /* [out] */ ILocale** locale)
{
    if (env == NULL || jlocale == NULL || locale == NULL) {
        ALOGE("ToElLocale: Invalid argumenet!");
        return false;
    }

    jclass localeKlass = env->FindClass("java/util/Locale");
    CheckErrorAndLog(env, "FindClass: Locale : %d!\n", __LINE__);

    AutoPtr<ILocaleHelper> helper;
    CLocaleHelper::AcquireSingleton((ILocaleHelper**)&helper);

    jfieldID f = env->GetStaticFieldID(localeKlass, "SIMPLIFIED_CHINESE", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "ToElLocale GetStaticFieldID: SIMPLIFIED_CHINESE : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(localeKlass, "equals", "(Ljava/lang/Object;)Z");
    CheckErrorAndLog(env, "ToElLocale GetMethodID: equals : %d!\n", __LINE__);

    jobject jSIMPLIFIED_CHINESE = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jboolean jIsSame = env->CallBooleanMethod(jlocale, m, jSIMPLIFIED_CHINESE);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetSIMPLIFIED_CHINESE(locale);
        env->DeleteLocalRef(jSIMPLIFIED_CHINESE);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "TAIWAN", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "ToElLocale Fail GetStaticFieldID: TAIWAN : %d!\n", __LINE__);

    jobject jTAIWAN = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jTAIWAN);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetTAIWAN(locale);
        env->DeleteLocalRef(jTAIWAN);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "TRADITIONAL_CHINESE", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: TRADITIONAL_CHINESE : %d!\n", __LINE__);

    jobject jTRADITIONAL_CHINESE = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jTRADITIONAL_CHINESE);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetTRADITIONAL_CHINESE(locale);
        env->DeleteLocalRef(jTRADITIONAL_CHINESE);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "CHINA", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: CHINA : %d!\n", __LINE__);

    jobject jCHINA = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jCHINA);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetCHINA(locale);
        env->DeleteLocalRef(jCHINA);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "CHINESE", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: CHINESE : %d!\n", __LINE__);

    jobject jCHINESE = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jCHINESE);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetCHINESE(locale);
        env->DeleteLocalRef(jCHINESE);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "US", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: US : %d!\n", __LINE__);

    jobject jUS = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jUS);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetUS(locale);
        env->DeleteLocalRef(jUS);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "UK", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: UK : %d!\n", __LINE__);

    jobject jUK = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jUK);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetUK(locale);
        env->DeleteLocalRef(jUK);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "ENGLISH", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: ENGLISH : %d!\n", __LINE__);

    jobject jENGLISH = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jENGLISH);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetENGLISH(locale);
        env->DeleteLocalRef(jENGLISH);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "CANADA", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: CANADA : %d!\n", __LINE__);

    jobject jCANADA = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jCANADA);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetCANADA(locale);
        env->DeleteLocalRef(jCANADA);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "CANADA_FRENCH", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: CANADA_FRENCH : %d!\n", __LINE__);

    jobject jCANADA_FRENCH = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jCANADA_FRENCH);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetCANADA_FRENCH(locale);
        env->DeleteLocalRef(jCANADA_FRENCH);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "FRANCE", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: FRANCE : %d!\n", __LINE__);

    jobject jFRANCE = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jFRANCE);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetFRANCE(locale);
        env->DeleteLocalRef(jFRANCE);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "GERMAN", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: GERMAN : %d!\n", __LINE__);

    jobject jGERMAN = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jGERMAN);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetGERMAN(locale);
        env->DeleteLocalRef(jGERMAN);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "GERMANY", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: GERMANY : %d!\n", __LINE__);

    jobject jGERMANY = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jGERMANY);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetGERMANY(locale);
        env->DeleteLocalRef(jGERMANY);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "ITALIAN", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: ITALIAN : %d!\n", __LINE__);

    jobject jITALIAN = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jITALIAN);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetITALIAN(locale);
        env->DeleteLocalRef(jITALIAN);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "ITALY", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: ITALY : %d!\n", __LINE__);
    jobject jITALY = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jITALY);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetITALY(locale);
        env->DeleteLocalRef(jITALY);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "JAPAN", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: JAPAN : %d!\n", __LINE__);
    jobject jJAPAN = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jJAPAN);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetJAPAN(locale);
        env->DeleteLocalRef(jJAPAN);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "JAPANESE", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: JAPANESE : %d!\n", __LINE__);
    jobject jJAPANESE = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jJAPANESE);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetJAPANESE(locale);
        env->DeleteLocalRef(jJAPANESE);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "KOREA", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: KOREA : %d!\n", __LINE__);
    jobject jKOREA = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jKOREA);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetKOREA(locale);
        env->DeleteLocalRef(jKOREA);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "KOREAN", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: KOREAN : %d!\n", __LINE__);
    jobject jKOREAN = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jKOREAN);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetKOREAN(locale);
        env->DeleteLocalRef(jKOREAN);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "PRC", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: PRC : %d!\n", __LINE__);
    jobject jPRC = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jPRC);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetPRC(locale);
        env->DeleteLocalRef(jPRC);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    f = env->GetStaticFieldID(localeKlass, "ROOT", "Ljava/util/Locale;");
    CheckErrorAndLog(env, "GetJavaLocale Fail GetStaticFieldID: ROOT : %d!\n", __LINE__);
    jobject jROOT = env->GetStaticObjectField(localeKlass, f);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    jIsSame = env->CallBooleanMethod(jlocale, m, jROOT);
    CheckErrorAndLog(env, "ToElLocale GetStaticObjectField: INVALID : %d!\n", __LINE__);
    if (jIsSame) {
        helper->GetROOT(locale);
        env->DeleteLocalRef(jROOT);
        env->DeleteLocalRef(localeKlass);
        return true;
    }

    String languageCode = ElUtil::GetJavaStringField(env, localeKlass, jlocale, "languageCode", "ToElLocale");
    String countryCode = ElUtil::GetJavaStringField(env, localeKlass, jlocale, "countryCode", "ToElLocale");

    env->DeleteLocalRef(localeKlass);
    ALOGE("ToElLocale Unknown Locale languageCode: %s countryCode: %s", languageCode.string(), countryCode.string());
    return false;
}

jobject ElUtil::GetJavaAssetFileDescriptor(
    /* [in] */ JNIEnv* env,
    /* [in] */ IAssetFileDescriptor* asdescptor)
{
    if (env == NULL || asdescptor == NULL) {
        ALOGD("GetJavaAssetFileDescriptor() Invalid arguments!");
        return NULL;
    }

    jclass asdKlass = env->FindClass("android/content/res/AssetFileDescriptor");
    CheckErrorAndLog(env, "GetJavaAssetFileDescriptor FindClass: AssetFileDescriptor : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(asdKlass, "<init>", "(Landroid/os/ParcelFileDescriptor;JJLandroid/os/Bundle;)V");
    CheckErrorAndLog(env, "GetJavaAssetFileDescriptor GetMethodID: AssetFileDescriptor : %d!\n", __LINE__);

    AutoPtr<IParcelFileDescriptor> fd;
    asdescptor->GetParcelFileDescriptor((IParcelFileDescriptor**)&fd);
    jobject jfd = NULL;
    if (fd != NULL) {
        jfd = ElUtil::GetJavaParcelFileDescriptor(env, fd);
    }

    Int64 length;
    asdescptor->GetDeclaredLength(&length);
    Int64 startOffset;
    asdescptor->GetStartOffset(&startOffset);
    AutoPtr<IBundle> extras;
    asdescptor->GetExtras((IBundle**)&extras);
    jobject jextras = NULL;
    if (extras != NULL) {
        jextras = GetJavaBundle(env, extras);
    }

    jobject jasdescptor = env->NewObject(asdKlass, m, jfd, (jlong)startOffset, (jlong)length, jextras);
    CheckErrorAndLog(env, "GetJavaWallpaperInfo NewObject: WallpaperInfo : %d!\n", __LINE__);
    env->DeleteLocalRef(asdKlass);
    env->DeleteLocalRef(jfd);
    env->DeleteLocalRef(jextras);

    return jasdescptor;
}

bool ElUtil::ToElApplicationErrorReportCrashInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IApplicationErrorReportCrashInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElApplicationErrorReportCrashInfo: Invalid argumenet!");
        return false;
    }

    jclass crKlass = env->FindClass("android/app/ApplicationErrorReport$CrashInfo");
    CheckErrorAndLog(env, "ToElApplicationErrorReportCrashInfo FindClass: CrashInfo : %d!\n", __LINE__);

    if (NOERROR != CApplicationErrorReportCrashInfo::New(info)) {
        ALOGE("ToElApplicationErrorReportCrashInfo create CApplicationErrorReportCrashInfo fail!");
        return false;
    }

    String tempString;
    tempString = ElUtil::GetJavaStringField(env, crKlass, jinfo, "exceptionClassName", "ToElApplicationErrorReportCrashInfo");
    (*info)->SetExceptionClassName(tempString);
    tempString = ElUtil::GetJavaStringField(env, crKlass, jinfo, "exceptionMessage", "ToElApplicationErrorReportCrashInfo");
    (*info)->SetExceptionMessage(tempString);
    tempString = ElUtil::GetJavaStringField(env, crKlass, jinfo, "throwFileName", "ToElApplicationErrorReportCrashInfo");
    (*info)->SetThrowFileName(tempString);
    tempString = ElUtil::GetJavaStringField(env, crKlass, jinfo, "throwClassName", "ToElApplicationErrorReportCrashInfo");
    (*info)->SetThrowClassName(tempString);
    tempString = ElUtil::GetJavaStringField(env, crKlass, jinfo, "throwMethodName", "ToElApplicationErrorReportCrashInfo");
    (*info)->SetThrowMethodName(tempString);
    tempString = ElUtil::GetJavaStringField(env, crKlass, jinfo, "stackTrace", "ToElApplicationErrorReportCrashInfo");
    (*info)->SetStackTrace(tempString);

    Int32 throwLineNumber = ElUtil::GetJavaIntField(env, crKlass, jinfo, "throwLineNumber", "ToElApplicationErrorReportCrashInfo");
    (*info)->SetThrowLineNumber(throwLineNumber);
    env->DeleteLocalRef(crKlass);
    return true;
}

bool ElUtil::ToElContentValues(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jContentValues,
    /* [out] */ IContentValues** contentValues)
{
    if (env == NULL || contentValues == NULL) {
        ALOGD("ToElContentValues: Invalid argumenet!");
        return false;
    }

    jclass contentValuesKlass = env->FindClass("android/content/ContentValues");
    CheckErrorAndLog(env, "FindClass: android/content/ContentValues : %d!\n", __LINE__);

    if (NOERROR != CContentValues::New(contentValues)) {
        ALOGD("ToElContentValues: create CContentValues fail!");
        return false;
    }

    jmethodID m = env->GetMethodID(contentValuesKlass, "keySet", "()Ljava/util/Set;");
    CheckErrorAndLog(env, "GetMethodID: keySet : %d!\n", __LINE__);

    jobject jKeySet = env->CallObjectMethod(jContentValues, m);
    CheckErrorAndLog(env, "CallObjectMethod: jKeySet : %d!\n", __LINE__);

    if (jKeySet != NULL) {
        jclass setKlass = env->FindClass("java/util/Set");
        CheckErrorAndLog(env, "FindClass: java/util/Set : %d!\n", __LINE__);

        m = env->GetMethodID(setKlass, "iterator", "()Ljava/util/Iterator;");
        CheckErrorAndLog(env, "GetMethodID: iterator ()Ljava/util/Iterator; : %d!\n", __LINE__);

        jobject jIterator = env->CallObjectMethod(jKeySet, m);
        CheckErrorAndLog(env, "CallObjectMethod: jIterator : %d!\n", __LINE__);

        env->DeleteLocalRef(setKlass);
        env->DeleteLocalRef(jKeySet);

        if (jIterator != NULL) {
            jclass iteratorKlass = env->FindClass("java/util/Iterator");
            CheckErrorAndLog(env, "FindClass: java/util/Iterator : %d!\n", __LINE__);

            jmethodID jHasNext = env->GetMethodID(iteratorKlass, "hasNext", "()Z");
            CheckErrorAndLog(env, "GetMethodID: hasNext : %d!\n", __LINE__);

            jmethodID jNext = env->GetMethodID(iteratorKlass, "next", "()Ljava/lang/Object;");
            CheckErrorAndLog(env, "GetMethodID: next : %d!\n", __LINE__);
            env->DeleteLocalRef(iteratorKlass);

            jclass numberKlass = env->FindClass("java/lang/Number");
            CheckErrorAndLog(env, "FindClass: java/lang/Number : %d!\n", __LINE__);

            jboolean hasNext = env->CallBooleanMethod(jIterator, jHasNext);
            while (hasNext) {
                jstring jKey = (jstring)env->CallObjectMethod(jIterator, jNext);
                String ckey = ToElString(env, jKey);

                //call ContentValues.get(String)
                m = env->GetMethodID(contentValuesKlass, "get", "(Ljava/lang/String;)Ljava/lang/Object;");
                CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

                jobject jValue = env->CallObjectMethod(jContentValues, m, jKey);
                CheckErrorAndLog(env, "CallObjectMethod: jValue : %d!\n", __LINE__);
                env->DeleteLocalRef(jKey);

                if (jValue != NULL) {
                    String objClassName = GetClassName(env, jValue);
                    if (objClassName.Equals("java.lang.String")) {
                        String cvalue = ToElString(env, (jstring)jValue);
                        AutoPtr<ICharSequence> cs;
                        CString::New(cvalue, (ICharSequence**)&cs);
                        (*contentValues)->Put(ckey, cs);
                    }
                    else if (objClassName.Equals("java.lang.Byte")) {
                        jmethodID m1 = env->GetMethodID(numberKlass, "byteValue", "()B");
                        CheckErrorAndLog(env, "GetMethodID: byteValue : %d!\n", __LINE__);
                        jbyte value = env->CallByteMethod(jValue, m1);
                        CheckErrorAndLog(env, "CallByteMethod: value : %d!\n", __LINE__);
                        AutoPtr<IByte> v;
                        CByte::New(value, (IByte**)&v);
                        (*contentValues)->Put(ckey, v);
                    }
                    else if (objClassName.Equals("java.lang.Short")) {
                        jmethodID m1 = env->GetMethodID(numberKlass, "shortValue", "()S");
                        CheckErrorAndLog(env, "GetMethodID: shortValue : %d!\n", __LINE__);
                        jshort value = env->CallShortMethod(jValue, m1);
                        CheckErrorAndLog(env, "CallShortMethod: value : %d!\n", __LINE__);
                        AutoPtr<IInteger16> v;
                        CInteger16::New(value, (IInteger16**)&v);
                        (*contentValues)->Put(ckey, v);
                    }
                    else if (objClassName.Equals("java.lang.Integer")) {
                        jmethodID m1 = env->GetMethodID(numberKlass, "intValue", "()I");
                        CheckErrorAndLog(env, "GetMethodID: intValue : %d!\n", __LINE__);
                        jint value = env->CallIntMethod(jValue, m1);
                        CheckErrorAndLog(env, "CallIntMethod: value : %d!\n", __LINE__);
                        AutoPtr<IInteger32> v;
                        CInteger32::New(value, (IInteger32**)&v);
                        (*contentValues)->Put(ckey, v);
                    }
                    else if (objClassName.Equals("java.lang.Long")) {
                        jmethodID m1 = env->GetMethodID(numberKlass, "longValue", "()J");
                        CheckErrorAndLog(env, "GetMethodID: longValue : %d!\n", __LINE__);
                        jlong value = env->CallLongMethod(jValue, m1);
                        CheckErrorAndLog(env, "CallLongMethod: value : %d!\n", __LINE__);
                        AutoPtr<IInteger64> v;
                        CInteger64::New(value, (IInteger64**)&v);
                        (*contentValues)->Put(ckey, v);
                    }
                    else if (objClassName.Equals("java.lang.Float")) {
                        jmethodID m1 = env->GetMethodID(numberKlass, "floatValue", "()F");
                        CheckErrorAndLog(env, "GetMethodID: floatValue : %d!\n", __LINE__);
                        jfloat value = env->CallFloatMethod(jValue, m1);
                        CheckErrorAndLog(env, "CallFloatMethod: value : %d!\n", __LINE__);
                        AutoPtr<IFloat> v;
                        CFloat::New(value, (IFloat**)&v);
                        (*contentValues)->Put(ckey, v);
                    }
                    else if (objClassName.Equals("java.lang.Double")) {
                        jmethodID m1 = env->GetMethodID(numberKlass, "doubleValue", "()D");
                        CheckErrorAndLog(env, "GetMethodID: doubleValue : %d!\n", __LINE__);
                        jdouble value = env->CallDoubleMethod(jValue, m1);
                        CheckErrorAndLog(env, "CallDoubleMethod: value : %d!\n", __LINE__);
                        AutoPtr<IDouble> v;
                        CDouble::New(value, (IDouble**)&v);
                        (*contentValues)->Put(ckey, v);
                    }
                    else if (objClassName.Equals("java.lang.Boolean")) {
                        jclass booleanKlass = env->FindClass("java/lang/Boolean");
                        CheckErrorAndLog(env, "FindClass: java/lang/Boolean : %d!\n", __LINE__);
                        jmethodID m1 = env->GetMethodID(booleanKlass, "booleanValue", "()Z");
                        CheckErrorAndLog(env, "GetMethodID: booleanValue : %d!\n", __LINE__);
                        jboolean value = env->CallBooleanMethod(jValue, m1);
                        CheckErrorAndLog(env, "CallBooleanMethod: value : %d!\n", __LINE__);
                        AutoPtr<IBoolean> v;
                        CBoolean::New(value, (IBoolean**)&v);
                        (*contentValues)->Put(ckey, v);
                        env->DeleteLocalRef(booleanKlass);
                    }
                    else if (objClassName.Equals("[B")) {
                        jbyteArray jbyteArrayValue = (jbyteArray)jValue;

                        AutoPtr<ArrayOf<Byte> > byteArrayValue;
                        if (ElUtil::ToElByteArray(env, jbyteArrayValue, (ArrayOf<Byte>**)&byteArrayValue)) {
                            Int32 size = byteArrayValue->GetLength();
                            AutoPtr<IArrayOf> array;
                            CArrayOf::New(EIID_IByte, size, (IArrayOf**)&array);
                            for (Int32 i = 0; i < size; i++) {
                                AutoPtr<IByte> v;
                                CByte::New((*byteArrayValue)[i], (IByte**)&v);
                                array->Set(i, v);
                            }
                            (*contentValues)->Put(ckey, array);

                        }
                        else {
                            ALOGE("ToElContentValues() ToElByteArray fail! Line: %d", __LINE__);
                        }
                    }
                }
                else {
                    (*contentValues)->PutNull(ckey);
                }

                env->DeleteLocalRef(jValue);
                hasNext = env->CallBooleanMethod(jIterator, jHasNext);
            }

            env->DeleteLocalRef(numberKlass);
            env->DeleteLocalRef(jIterator);
        }
    }

    env->DeleteLocalRef(contentValuesKlass);
    return true;
}

bool ElUtil::ToElDropBoxManagerEntry(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jentry,
    /* [out] */ IDropBoxManagerEntry** entry)
{
    if (env == NULL || jentry == NULL || entry == NULL) {
        ALOGE("ToElDropBoxManagerEntry: Invalid argumenet!");
        return false;
    }

    // jclass dbmeKlass = env->FindClass("android/os/DropBoxManager$Entry");
    // CheckErrorAndLog(env, "ToElDropBoxManagerEntry FindClass: Entry : %d!\n", __LINE__);

    // String tag = ElUtil::GetJavaStringField(env, dbmeKlass, jentry, "mTag", "ToElDropBoxManagerEntry");
    // Int64 millis = ElUtil::GetJavalongField(env, dbmeKlass, jentry, "mTimeMillis", "ToElDropBoxManagerEntry");
    // Int32 flags = ElUtil::GetJavaIntField(env, dbmeKlass, jentry, "mFlags", "ToElDropBoxManagerEntry");

    // jfieldID f = env->GetFieldID(dbmeKlass, "mData", "[B");
    // ElUtil::CheckErrorAndLog(env, "ToElDropBoxManagerEntry GetFieldID: mData %d", __LINE__);

    // jbyteArray jdata = (jbyteArray)env->GetObjectField(jentry, f);
    // ElUtil::CheckErrorAndLog(env, "ToElDropBoxManagerEntry GetObjectField: mData %d", __LINE__);

    // AutoPtr<ArrayOf<Byte> > data;
    // if (jdata != NULL) {
    //     if (!ElUtil::ToElByteArray(env, jdata, (ArrayOf<Byte>**)&data)) {
    //         ALOGE("ToElDropBoxManagerEntry ToElByteArray fail!");
    //     }

    //     if (NOERROR == CDropBoxManagerEntry::New(tag, millis, data, flags, entry)) {
    //         env->DeleteLocalRef(dbmeKlass);
    //         env->DeleteLocalRef(jdata);
    //         return true;
    //     }
    //     else {
    //         ALOGE("ToElDropBoxManagerEntry create CDropBoxManagerEntry fail! %d", __LINE__);
    //     }
    //     env->DeleteLocalRef(jdata);
    // }

    // f = env->GetFieldID(dbmeKlass, "mFileDescriptor", "Landroid/os/ParcelFileDescriptor;");
    // ElUtil::CheckErrorAndLog(env, "ToElDropBoxManagerEntry GetFieldID: mFileDescriptor %d", __LINE__);

    // jobject jfileDescriptor = env->GetObjectField(jentry, f);
    // ElUtil::CheckErrorAndLog(env, "ToElDropBoxManagerEntry GetObjectField: mFileDescriptor %d", __LINE__);

    // AutoPtr<IParcelFileDescriptor> fileDescriptor;
    // if (jfileDescriptor != NULL) {
    //     if (!ElUtil::ToElParcelFileDescriptor(env, jfileDescriptor, (IParcelFileDescriptor**)&fileDescriptor)) {
    //         ALOGE("ToElDropBoxManagerEntry ToElByteArray fail!");
    //     }

    //     if (NOERROR == CDropBoxManagerEntry::New(tag, millis, fileDescriptor, flags, entry)) {
    //         env->DeleteLocalRef(dbmeKlass);
    //         env->DeleteLocalRef(jfileDescriptor);
    //         return true;
    //     }
    //     else {
    //         ALOGE("ToElDropBoxManagerEntry create CDropBoxManagerEntry fail! %d", __LINE__);
    //     }
    //     env->DeleteLocalRef(jfileDescriptor);
    // }

    // if (NOERROR != CDropBoxManagerEntry::New(tag, millis, entry)) {
    //     ALOGE("ToElDropBoxManagerEntry create CDropBoxManagerEntry fail! %d", __LINE__);
    // }

    // env->DeleteLocalRef(dbmeKlass);
    assert(0);

    return true;
}

jobject ElUtil::GetJavaDropBoxManagerEntry(
    /* [in] */ JNIEnv* env,
    /* [in] */ IDropBoxManagerEntry* entry)
{
    if (env == NULL || entry == NULL) {
        ALOGD("GetJavaDropBoxManagerEntry() Invalid arguments!");
        return NULL;
    }

    jclass dbmeKlass = env->FindClass("android/os/DropBoxManager$Entry");
    CheckErrorAndLog(env, "GetJavaDropBoxManagerEntry FindClass: Entry : %d!\n", __LINE__);

    String tag;
    entry->GetTag(&tag);
    jstring jtag = ElUtil::GetJavaString(env, tag);
    Int64 millis;
    entry->GetTimeMillis(&millis);
    Int32 flags;
    entry->GetFlags(&flags);

    AutoPtr<IInputStream> stream;
    entry->GetInputStream((IInputStream**)&stream);
    jobject jstream = NULL;
    if (stream != NULL) {
        if (IParcelFileDescriptorAutoCloseInputStream::Probe(stream) != NULL) {
            AutoPtr<IFileInputStream> fStream = IFileInputStream::Probe(stream);
            AutoPtr<IFileDescriptor> fd;
            fStream->GetFD((IFileDescriptor**)&fd);

            jobject jfd = ElUtil::GetJavaFileDescriptor(env, fd);

            jmethodID m = env->GetMethodID(dbmeKlass, "<init>", "(Ljava/lang/String;JLandroid/os/ParcelFileDescriptor;I)V");
            CheckErrorAndLog(env, "GetJavaDropBoxManagerEntry GetMethodID: Entry : %d!\n", __LINE__);

            jstream = env->NewObject(dbmeKlass, m, jtag, (jlong)millis, jfd, (jint)flags);
            CheckErrorAndLog(env, "GetJavaWallpaperInfo NewObject: Entry : %d!\n", __LINE__);

            env->DeleteLocalRef(jfd);
        }
        else if (IByteArrayInputStream::Probe(stream) != NULL) {
            AutoPtr<IInputStream> bStream = IInputStream::Probe(stream);

            AutoPtr<IByteArrayOutputStream> outStream;
            CByteArrayOutputStream::New((IByteArrayOutputStream**)&outStream);

            AutoPtr<ArrayOf<Byte> > temp;
            ArrayOf<Byte>::Alloc(BUFFER_SIZE);

            Int32 count = -1;
            while (((bStream->Read(temp, 0, BUFFER_SIZE, &count)), count) != -1) {
                IOutputStream::Probe(outStream)->Write(temp, 0, count);
            }

            AutoPtr<ArrayOf<Byte> > data;
            outStream->ToByteArray((ArrayOf<Byte>**)&data);
            if (data != NULL) {
                jbyteArray jdata = ElUtil::GetJavaByteArray(env, data);

                jmethodID m = env->GetMethodID(dbmeKlass, "<init>", "(Ljava/lang/String;J[BI)V");
                CheckErrorAndLog(env, "GetJavaDropBoxManagerEntry GetMethodID: Entry : %d!\n", __LINE__);

                jstream = env->NewObject(dbmeKlass, m, jtag, (jlong)millis, jdata, (jint)flags);
                CheckErrorAndLog(env, "GetJavaWallpaperInfo NewObject: Entry : %d!\n", __LINE__);

                env->DeleteLocalRef(jdata);
            }
            else {
                ALOGE("GetJavaDropBoxManagerEntry() ReadBytes fail!");
            }
        }
        else if (IGZIPInputStream::Probe(stream) != NULL) {
            ALOGE("GetJavaDropBoxManagerEntry() IGZIPInputStream E_NOT_IMPLEMENTED!");
        }
        else {
            ALOGE("GetJavaDropBoxManagerEntry() Unknown stream!");
        }

    }
    else {
        ALOGW("GetJavaDropBoxManagerEntry() stream is NULL!");

        jmethodID m = env->GetMethodID(dbmeKlass, "<init>", "(Ljava/lang/String;J)V");
        CheckErrorAndLog(env, "GetJavaDropBoxManagerEntry GetMethodID: Entry : %d!\n", __LINE__);

        jstream = env->NewObject(dbmeKlass, m, jtag, (jlong)millis);
        CheckErrorAndLog(env, "GetJavaWallpaperInfo NewObject: Entry : %d!\n", __LINE__);
    }

    env->DeleteLocalRef(dbmeKlass);
    env->DeleteLocalRef(jtag);

    return jstream;
}

bool ElUtil::ToElFragmentManagerState(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jstate,
    /* [in] */ IFragmentManagerState** state)
{
    if (env == NULL || jstate == NULL || state == NULL) {
        ALOGE("ToElFragmentManagerState: Invalid argumenet!");
        return false;
    }

    ALOGE("ToElFragmentManagerState() E_NOT_IMPLEMENTED");
    return false;
}

bool ElUtil::ToElHashMap(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jmap,
    /* [out] */ IHashMap** map)
{
    if (env == NULL || jmap == NULL || map == NULL) {
        ALOGE("ToElHashMap: Invalid argumenet!");
        return false;
    }

    CHashMap::New(map);

    jclass mapKlass = env->FindClass("java/util/HashMap");
    CheckErrorAndLog(env, "FindClass: HashMap : %d!", __LINE__);

    jmethodID m = env->GetMethodID(mapKlass, "keySet", "()Ljava/util/Set;");
    CheckErrorAndLog(env, "GetMethodID: keySet : %d!", __LINE__);

    jobject jKeySet = env->CallObjectMethod(jmap, m);
    CheckErrorAndLog(env, "CallObjectMethod: jKeySet : %d!", __LINE__);

    if (jKeySet != NULL) {
        jclass setKlass = env->FindClass("java/util/Set");
        CheckErrorAndLog(env, "FindClass: java/util/Set : %d!\n", __LINE__);

        m = env->GetMethodID(setKlass, "iterator", "()Ljava/util/Iterator;");
        CheckErrorAndLog(env, "GetMethodID: iterator ()Ljava/util/Iterator; : %d!\n", __LINE__);

        env->DeleteLocalRef(setKlass);

        jobject jIterator = env->CallObjectMethod(jKeySet, m);
        CheckErrorAndLog(env, "CallObjectMethod: jIterator : %d!\n", __LINE__);

        if (jIterator != NULL) {
            jclass iteratorKlass = env->FindClass("java/util/Iterator");
            CheckErrorAndLog(env, "FindClass: java/util/Iterator : %d!\n", __LINE__);

            jmethodID jHasNext = env->GetMethodID(iteratorKlass, "hasNext", "()Z");
            CheckErrorAndLog(env, "GetMethodID: hasNext : %d!\n", __LINE__);

            jmethodID jNext = env->GetMethodID(iteratorKlass, "next", "()Ljava/lang/Object;");
            CheckErrorAndLog(env, "GetMethodID: next : %d!\n", __LINE__);

            env->DeleteLocalRef(iteratorKlass);

            jboolean hasNext = env->CallBooleanMethod(jIterator, jHasNext);
            String keyClassName;
            while (hasNext) {
                jobject jKey = env->CallObjectMethod(jIterator, jNext);
                if (keyClassName.IsNullOrEmpty())
                    keyClassName = GetClassName(env, jKey);

                AutoPtr<ICharSequence> key;
                if (keyClassName.Equals("java.lang.String")) {
                    CString::New(ElUtil::ToElString(env, (jstring)jKey), (ICharSequence**)&key);
                }
                else {
                    ALOGE("ToElHashMap() Unsupported Key type: %s", keyClassName.string());
                    env->DeleteLocalRef(jKey);
                    env->DeleteLocalRef(jIterator);
                    env->DeleteLocalRef(jKeySet);
                    env->DeleteLocalRef(mapKlass);
                    return false;
                }

                m = env->GetMethodID(mapKlass, "get", "(Ljava/lang/Object;)Ljava/lang/Object;");
                CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

                jobject jValue = env->CallObjectMethod(jmap, m, jKey);
                CheckErrorAndLog(env, "CallObjectMethod: jValue : %d!\n", __LINE__);

                if (jValue != NULL) {
                    String objClassName = GetClassName(env, jValue);
                    if (objClassName.Equals("java.lang.String")) {
                        String value = ToElString(env, (jstring)jValue);
                        AutoPtr<ICharSequence> cs;
                        CString::New(value, (ICharSequence**)&cs);
                        (*map)->Put(key, cs);
                    }
                    else {
                        jclass serClass = env->FindClass("java/io/Serializable");
                        ElUtil::CheckErrorAndLog(env, "FindClass Serializable fail : %d!\n", __LINE__);

                        if (env->IsInstanceOf(jValue, serClass)) {
                            AutoPtr<ISerializable> serializable;
                            if (ElUtil::ToElSerializable(env, jValue, (ISerializable**)&serializable)) {
                                (*map)->Put(key, serializable);
                            }
                            else {
                                ALOGE("ToElHashMap ToElSerializable fail");
                            }
                        }
                        else {
                            ALOGE("ToElHashMap() Unsupported value type: %s", objClassName.string());
                        }
                        env->DeleteLocalRef(serClass);
                    }

                    env->DeleteLocalRef(jValue);
                }
                else {
                    (*map)->Put(key, NULL);
                }

                env->DeleteLocalRef(jKey);

                hasNext = env->CallBooleanMethod(jIterator, jHasNext);
            }
            env->DeleteLocalRef(jIterator);
        }
        env->DeleteLocalRef(jKeySet);
    }

    env->DeleteLocalRef(mapKlass);

    return true;
}

bool ElUtil::ToElVpnProfile(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jprofile,
    /* [out] */ IVpnProfile** profile)
{
    if (env == NULL || jprofile == NULL || profile == NULL) {
        ALOGE("ToElHashMap: Invalid argumenet!");
        return false;
    }

    jclass vpKlass = env->FindClass("com/android/internal/net/VpnProfile");
    ElUtil::CheckErrorAndLog(env, "ToElVpnProfile FindClass: VpnProfile %d", __LINE__);

    String key = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "key", "ToElVpnProfile");

    if (NOERROR != CVpnProfile::New(key, profile)) {
        ALOGE("ToElVpnProfile() Create CVpnProfile fail!");
        return FALSE;
    }

    String tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "name", "ToElVpnProfile");
    (*profile)->SetName(tempString);

    Int32 tempInt = ElUtil::GetJavaIntField(env, vpKlass, jprofile, "type", "ToElVpnProfile");
    (*profile)->SetType(tempInt);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "server", "ToElVpnProfile");
    (*profile)->SetServer(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "username", "ToElVpnProfile");
    (*profile)->SetUsername(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "password", "ToElVpnProfile");
    (*profile)->SetPassword(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "dnsServers", "ToElVpnProfile");
    (*profile)->SetDnsServers(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "searchDomains", "ToElVpnProfile");
    (*profile)->SetSearchDomains(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "routes", "ToElVpnProfile");
    (*profile)->SetRoutes(tempString);

    Boolean tempBool = ElUtil::GetJavaBoolField(env, vpKlass, jprofile, "mppe", "ToElVpnProfile");
    (*profile)->SetMppe(tempBool);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "l2tpSecret", "ToElVpnProfile");
    (*profile)->SetL2tpSecret(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "ipsecIdentifier", "ToElVpnProfile");
    (*profile)->SetIpsecIdentifier(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "ipsecSecret", "ToElVpnProfile");
    (*profile)->SetIpsecSecret(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "ipsecUserCert", "ToElVpnProfile");
    (*profile)->SetIpsecUserCert(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "ipsecCaCert", "ToElVpnProfile");
    (*profile)->SetIpsecCaCert(tempString);

    tempString = ElUtil::GetJavaStringField(env, vpKlass, jprofile, "ipsecServerCert", "ToElVpnProfile");
    (*profile)->SetIpsecServerCert(tempString);

    tempBool = ElUtil::GetJavaBoolField(env, vpKlass, jprofile, "saveLogin", "ToElVpnProfile");
    (*profile)->SetSaveLogin(tempBool);

    env->DeleteLocalRef(vpKlass);

    return true;
}

jobject ElUtil::GetJavaRecentTaskInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityManagerRecentTaskInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGW("GetJavaRecentTaskInfo: Invalid argumenet!");
        return NULL;
    }

    jclass klass = env->FindClass("android/app/ActivityManager$RecentTaskInfo");
    CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail FindClass: RecentTaskInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail GetMethodID: RecentTaskInfo constructor : %d!\n", __LINE__);

    jobject jtask = env->NewObject(klass, m);
    CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail NewObject: RecentTaskInfo : %d!\n", __LINE__);

    Int32 id;
    info->GetId(&id);
    SetJavaIntField(env, klass, jtask, id, "id", "GetJavaRecentTaskInfo");

    Int32 persistentId;
    info->GetPersistentId(&persistentId);
    SetJavaIntField(env, klass, jtask, persistentId, "persistentId", "GetJavaRecentTaskInfo");

    AutoPtr<IIntent> baseIntent;
    info->GetBaseIntent((IIntent**)&baseIntent);
    if (baseIntent != NULL) {
        jobject jintent = GetJavaIntent(env, baseIntent);
        jfieldID fbaseIntent = env->GetFieldID(klass, "baseIntent", "Landroid/content/Intent;");
        CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail get field: baseIntent : %d!\n", __LINE__);
        env->SetObjectField(jtask, fbaseIntent, jintent);
        CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail set field: baseIntent : %d!\n", __LINE__);
        env->DeleteLocalRef(jintent);
    }
    else{
        ALOGW("GetJavaRecentTaskInfo, baseIntent is null");
    }

    AutoPtr<IComponentName> origActivity;
    info->GetOrigActivity((IComponentName**)&origActivity);
    if (origActivity != NULL) {
        jobject jorigActivity = GetJavaComponentName (env, origActivity);
        jfieldID forigActivity = env->GetFieldID(klass, "origActivity", "Landroid/content/ComponentName;");
        CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail get field: origActivity : %d!\n", __LINE__);
        env->SetObjectField(jtask, forigActivity, jorigActivity);
        CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail set field: origActivity : %d!\n", __LINE__);
        env->DeleteLocalRef(jorigActivity);
    }
    else{
        ALOGW("GetJavaRecentTaskInfo, origActivity is null");
    }

    AutoPtr<ICharSequence> csDescription;
    info->GetDescription((ICharSequence**)&csDescription);
    if (csDescription != NULL) {
        String description;
        csDescription->ToString(&description);
        jstring jdescription = GetJavaString(env, description);
        jfieldID fdescription = env->GetFieldID(klass, "description", "Ljava/lang/CharSequence;");
        CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail get field: description : %d!\n", __LINE__);
        env->SetObjectField(jtask, fdescription, jdescription);
        CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail set field: description : %d!\n", __LINE__);
        env->DeleteLocalRef(jdescription);
    }

    AutoPtr<IActivityManagerTaskDescription> taskDescription;
    info->GetTaskDescription((IActivityManagerTaskDescription**)&taskDescription);
    if (taskDescription != NULL) {
        jobject jtaskDescription = GetJavaTaskDescription(env, taskDescription);
        jfieldID taskDescription = env->GetFieldID(klass, "taskDescription", "Landroid/app/ActivityManager$TaskDescription;");
        CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail get field: taskDescription : %d!\n", __LINE__);

        env->SetObjectField(jtask, taskDescription, jtaskDescription);
        CheckErrorAndLog(env, "GetJavaRecentTaskInfo Fail set field: taskDescription : %d!\n", __LINE__);
        env->DeleteLocalRef(jtaskDescription);
    }

    Int32 stackId;
    info->GetStackId(&stackId);
    SetJavaIntField(env, klass, jtask, stackId, "stackId", "GetJavaRecentTaskInfo");

    Int32 userId;
    info->GetUserId(&userId);
    SetJavaIntField(env, klass, jtask, userId, "userId", "GetJavaRecentTaskInfo");

    Int64 firstActiveTime;
    info->GetFirstActiveTime(&firstActiveTime);
    SetJavalongField(env, klass, jtask, firstActiveTime, "firstActiveTime", "GetJavaRecentTaskInfo");

    Int64 lastActiveTime;
    info->GetLastActiveTime(&lastActiveTime);
    SetJavalongField(env, klass, jtask, lastActiveTime, "lastActiveTime", "GetJavaRecentTaskInfo");

    Int32 affiliatedTaskId;
    info->GetAffiliatedTaskId(&affiliatedTaskId);
    SetJavaIntField(env, klass, jtask, affiliatedTaskId, "affiliatedTaskId", "GetJavaRecentTaskInfo");

    Int32 affiliatedTaskColor;
    info->GetAffiliatedTaskColor(&affiliatedTaskColor);
    SetJavaIntField(env, klass, jtask, affiliatedTaskColor, "affiliatedTaskColor", "GetJavaRecentTaskInfo");

    env->DeleteLocalRef(klass);

    return jtask;
}

jobject ElUtil::GetJavaTaskDescription(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityManagerTaskDescription* taskDescription)
{
    if (env == NULL || taskDescription == NULL) {
        ALOGW("GetJavaTaskDescription: Invalid argumenet!");
        return NULL;
    }

    String label;
    taskDescription->GetLabel(&label);
    jstring jlabel = GetJavaString(env, label);

    AutoPtr<IBitmap> icon;
    taskDescription->GetIcon((IBitmap**)&icon);

    jobject jicon = NULL;
    if (icon != NULL) {
        jicon = GetJavaBitmap(env, icon);
    }

    String iconFilename;
    taskDescription->GetIconFilename(&iconFilename);
    jstring jiconFilename = GetJavaString(env, iconFilename);

    Int32 colorPrimary;
    taskDescription->GetPrimaryColor(&colorPrimary);

    jclass klass = env->FindClass("android/app/ActivityManager$TaskDescription");
    CheckErrorAndLog(env, "GetJavaTaskDescription Fail FindClass: TaskDescription : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(Ljava/lang/String;ILjava/lang/String;)V");
    CheckErrorAndLog(env, "GetJavaTaskDescription Fail GetMethodID: TaskDescription constructor : %d!\n", __LINE__);

    jobject jtaskDescription = env->NewObject(klass, m, jlabel, colorPrimary, jiconFilename);
    CheckErrorAndLog(env, "GetJavaTaskDescription Fail NewObject: RecentTaskInfo : %d!\n", __LINE__);

    m = env->GetMethodID(klass, "setIcon", "(Landroid/graphics/Bitmap;)V");
    CheckErrorAndLog(env, "GetJavaTaskDescription Fail GetMethodID setIcon %d!\n", __LINE__);

    env->CallVoidMethod(jtaskDescription, m, jicon);
    CheckErrorAndLog(env, "GetJavaTaskDescription Fail CallVoidMethod setIcon %d!\n", __LINE__);

    env->DeleteLocalRef(klass);

    return jtaskDescription;
}

jobject ElUtil::GetJavaPeriodicSync(
    /* [in] */ JNIEnv* env,
    /* [in] */ IPeriodicSync* periodicSync)
{
    if (env == NULL || periodicSync == NULL) {
        ALOGW("GetJavaPeriodicSync: Invalid argumenet!");
        return NULL;
    }

    AutoPtr<IAccount> account;
    periodicSync->GetAccount((IAccount**)&account);
    jobject jaccount = GetJavaAccount(env, account);

    String authority;
    periodicSync->GetAuthority(&authority);
    jstring jauthority = GetJavaString(env, authority);

    AutoPtr<IBundle> extras;
    periodicSync->GetExtras((IBundle**)&extras);
    jobject jextras = GetJavaBundle(env, extras);

    Int64 period;
    periodicSync->GetPeriod(&period);

    Int64 flexTime;
    periodicSync->GetFlexTime(&flexTime);

    jclass klass = env->FindClass("android/content/PeriodicSync");
    CheckErrorAndLog(env, "FindClass: PeriodicSync : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;JJ)V");
    CheckErrorAndLog(env, "GetMethodID: PeriodicSync constructor: %d!\n", __LINE__);

    jobject jperiodicSync = env->NewObject(klass, m, jaccount, jauthority, jextras, (jlong)period, flexTime);
    CheckErrorAndLog(env, "NewObject: PeriodicSync : %d!\n", __LINE__);

    env->DeleteLocalRef(jaccount);
    env->DeleteLocalRef(jauthority);
    env->DeleteLocalRef(jextras);
    env->DeleteLocalRef(klass);
    return jperiodicSync;
}

jobject ElUtil::GetJavaSyncInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ ISyncInfo* syncInfo)
{
    if (env == NULL || syncInfo == NULL) {
        ALOGW("GetJavaSyncInfo: Invalid argumenet!");
        return NULL;
    }

    AutoPtr<IAccount> account;
    syncInfo->GetAccount((IAccount**)&account);
    jobject jaccount = GetJavaAccount(env, account);

    String authority;
    syncInfo->GetAuthority(&authority);
    jstring jauthority = GetJavaString(env, authority);

    Int32 jauthorityId;
    syncInfo->GetAuthorityId(&jauthorityId);

    Int64 jstartTime;
    syncInfo->GetStartTime(&jstartTime);

    jclass klass = env->FindClass("android/content/SyncInfo");
    CheckErrorAndLog(env, "FindClass: SyncInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(ILandroid/accounts/Account;Ljava/lang/String;J)V");
    CheckErrorAndLog(env, "GetMethodID: SyncInfo constructor: %d!\n", __LINE__);

    jobject jsyncInfo = env->NewObject(klass, m, jauthorityId, jaccount, jauthority, jstartTime);
    CheckErrorAndLog(env, "NewObject: SyncInfo : %d!\n", __LINE__);

    env->DeleteLocalRef(jaccount);
    env->DeleteLocalRef(jauthority);
    env->DeleteLocalRef(klass);
    return jsyncInfo;
}

jobject ElUtil::GetJavaSyncAdapterType(
    /* [in] */ JNIEnv* env,
    /* [in] */ ISyncAdapterType* type)
{
    if (env == NULL || type == NULL) {
        ALOGW("GetJavaSyncAdapterType: Invalid argumenet!");
        return NULL;
    }

    String authority;
    type->GetAuthority(&authority);
    if (authority.IsNullOrEmpty()) {
        ALOGE("GetJavaSyncAdapterType: the authority must not be empty");
        return NULL;
    }

    String accountType;
    type->GetAccountType(&accountType);
    if (accountType.IsNullOrEmpty()) {
        ALOGE("GetJavaSyncAdapterType: the accountType must not be empty");
        return NULL;
    }

    jstring jauthority = GetJavaString(env, authority);
    jstring jaccountType = GetJavaString(env, accountType);

    Boolean isKey;
    type->IsKey(&isKey);
    if (isKey) {
        ALOGE("GetJavaSyncAdapterType: isKey = TRUE");
    }

    Boolean isSupport;
    type->SupportsUploading(&isSupport);

    Boolean isUserVisible;
    type->IsUserVisible(&isUserVisible);

    Boolean isAllow;
    type->AllowParallelSyncs(&isAllow);

    Boolean isAlways;
    type->IsAlwaysSyncable(&isAlways);

    String settingsActivity;
    type->GetSettingsActivity(&settingsActivity);
    jstring jsettingsActivity = GetJavaString(env, settingsActivity);

    jclass klass = env->FindClass("android/content/SyncAdapterType");
    CheckErrorAndLog(env, "FindClass: SyncAdapterType : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;)V");
    CheckErrorAndLog(env, "GetMethodID: SyncAdapterType constructor: %d!\n", __LINE__);

    jobject jsyncAdapterType = env->NewObject(klass, m, jauthority, jaccountType, isUserVisible, isSupport, isAlways, isAllow, jsettingsActivity);
    CheckErrorAndLog(env, "NewObject: SyncAdapterType : %d!\n", __LINE__);

    env->DeleteLocalRef(jauthority);
    env->DeleteLocalRef(jaccountType);
    env->DeleteLocalRef(jsettingsActivity);
    env->DeleteLocalRef(klass);
    return jsyncAdapterType;
}

jobject ElUtil::GetJavaSyncStatusInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ ISyncStatusInfo* type)
{
    if (env == NULL || type == NULL) {
        ALOGW("GetJavaSyncStatusInfo: Invalid argumenet!");
        return NULL;
    }

    Int32 authorityId;
    type->GetAuthorityId(&authorityId);

    Int64 totalElapsedTime;
    type->GetTotalElapsedTime(&totalElapsedTime);

    Int32 numSyncs;
    type->GetNumSyncs(&numSyncs);

    Int32 numSourcePoll;
    type->GetNumSourcePoll(&numSourcePoll);

    Int32 numSourceServer;
    type->GetNumSourceServer(&numSourceServer);

    Int32 numSourceLocal;
    type->GetNumSourceLocal(&numSourceLocal);

    Int32 numSourceUser;
    type->GetNumSourceUser(&numSourceUser);

    Int32 numSourcePeriodic;
    type->GetNumSourcePeriodic(&numSourcePeriodic);

    Int64 lastSuccessTime;
    type->GetLastSuccessTime(&lastSuccessTime);

    Int32 lastSuccessSource;
    type->GetLastSuccessSource(&lastSuccessSource);

    Int64 lastFailureTime;
    type->GetLastFailureTime(&lastFailureTime);

    Int32 lastFailureSource;
    type->GetLastFailureSource(&lastFailureSource);

    String lastFailureMesg;
    type->GetLastFailureMesg(&lastFailureMesg);

    Int64 initialFailureTime;
    type->GetInitialFailureTime(&initialFailureTime);

    Boolean pending;
    type->GetPending(&pending);

    Boolean initialize;
    type->GetInitialize(&initialize);

    AutoPtr<ArrayOf<Int64> > periodicSyncTimes;
    ALOGE("GetJavaSyncStatusInfo ISyncStatusInfo::GetPeriodicSyncTimes is not implemented!");
    // type->GetPeriodicSyncTimes((ArrayOf<Int64>**)&periodicSyncTimes);

    jclass klass = env->FindClass("android/content/SyncStatusInfo");
    CheckErrorAndLog(env, "FindClass: SyncStatusInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(I)V");
    CheckErrorAndLog(env, "GetMethodID: SyncStatusInfo constructor: %d!\n", __LINE__);

    jobject jsyncAdapterType = env->NewObject(klass, m, (jint)authorityId);
    CheckErrorAndLog(env, "NewObject: SyncStatusInfo : %d!\n", __LINE__);

    SetJavalongField(env, klass, jsyncAdapterType, (jlong)totalElapsedTime, "totalElapsedTime", "GetJavaSyncStatusInfo");
    SetJavaIntField(env, klass, jsyncAdapterType, (jint)numSyncs, "numSyncs", "GetJavaSyncStatusInfo");
    SetJavaIntField(env, klass, jsyncAdapterType, (jint)numSourcePoll, "numSourcePoll", "GetJavaSyncStatusInfo");
    SetJavaIntField(env, klass, jsyncAdapterType, (jint)numSourceServer, "numSourceServer", "GetJavaSyncStatusInfo");
    SetJavaIntField(env, klass, jsyncAdapterType, (jint)numSourceLocal, "numSourceLocal", "GetJavaSyncStatusInfo");
    SetJavaIntField(env, klass, jsyncAdapterType, (jint)numSourceUser, "numSourceUser", "GetJavaSyncStatusInfo");
    SetJavaIntField(env, klass, jsyncAdapterType, (jint)numSourcePeriodic, "numSourcePeriodic", "GetJavaSyncStatusInfo");
    SetJavalongField(env, klass, jsyncAdapterType, (jlong)lastSuccessTime, "lastSuccessTime", "GetJavaSyncStatusInfo");
    SetJavaIntField(env, klass, jsyncAdapterType, (jint)lastSuccessSource, "lastSuccessSource", "GetJavaSyncStatusInfo");
    SetJavalongField(env, klass, jsyncAdapterType, (jlong)lastFailureTime, "lastFailureTime", "GetJavaSyncStatusInfo");
    SetJavaIntField(env, klass, jsyncAdapterType, (jint)lastFailureSource, "lastFailureSource", "GetJavaSyncStatusInfo");
    SetJavaStringField(env, klass, jsyncAdapterType, lastFailureMesg, "lastFailureMesg", "GetJavaSyncStatusInfo");
    SetJavalongField(env, klass, jsyncAdapterType, (jlong)initialFailureTime, "initialFailureTime", "GetJavaSyncStatusInfo");
    SetJavaBoolField(env, klass, jsyncAdapterType, (jboolean)pending, "pending", "GetJavaSyncStatusInfo");
    SetJavaBoolField(env, klass, jsyncAdapterType, (jboolean)initialize, "initialize", "GetJavaSyncStatusInfo");

    if (periodicSyncTimes != NULL) {
        Int32 length = periodicSyncTimes->GetLength();

        jclass listKlass = env->FindClass("java/util/ArrayList");
        CheckErrorAndLog(env, "GetJavaSyncStatusInfo Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        CheckErrorAndLog(env, "GetJavaSyncStatusInfo Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jobject jlist = env->NewObject(listKlass, m);
        CheckErrorAndLog(env, "GetJavaSyncStatusInfo Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        CheckErrorAndLog(env, "GetJavaSyncStatusInfo Fail GetMethodID: add : %d!\n", __LINE__);

        for(Int32 i = 0; i < length; i++) {
            env->CallBooleanMethod(jlist, mAdd, (jlong)(*periodicSyncTimes)[i]);
            CheckErrorAndLog(env, "GetJavaSyncStatusInfo Fail CallObjectMethod: add() : %d!\n", __LINE__);
        }

        jfieldID field = env->GetFieldID(klass, "periodicSyncTimes", "Ljava/util/ArrayList;");
        ElUtil::CheckErrorAndLog(env, "GetJavaSyncStatusInfo Fail GetFieldID: periodicSyncTimes : %d!\n", __LINE__);

        env->SetObjectField(jsyncAdapterType, field, jlist);
        ElUtil::CheckErrorAndLog(env, "GetJavaSyncStatusInfo Fail SetObjectField: periodicSyncTimes : %d!\n", __LINE__);

        env->DeleteLocalRef(listKlass);
        env->DeleteLocalRef(jlist);
    }

    env->DeleteLocalRef(klass);
    return jsyncAdapterType;
}

jobject ElUtil::GetJavaFeatureInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IFeatureInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGW("GetJavaFeatureInfo: Invalid argumenet!");
        return NULL;
    }

    jclass fiKlass = env->FindClass("android/content/pm/FeatureInfo");
    CheckErrorAndLog(env, "FindClass: FeatureInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(fiKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: FeatureInfo : %d!\n", __LINE__);

    jobject jinfo = env->NewObject(fiKlass, m);
    CheckErrorAndLog(env, "NewObject: FeatureInfo : %d!\n", __LINE__);

    String name;
    info->GetName(&name);
    ElUtil::SetJavaStringField(env, fiKlass, jinfo, name, "name", "GetJavaFeatureInfo");

    Int32 tempInt;
    info->GetReqGlEsVersion(&tempInt);
    SetJavaIntField(env, fiKlass, jinfo, tempInt, "reqGlEsVersion", "GetJavaFeatureInfo");

    info->GetFlags(&tempInt);
    SetJavaIntField(env, fiKlass, jinfo, tempInt, "flags", "GetJavaFeatureInfo");

    env->DeleteLocalRef(fiKlass);
    return jinfo;
}

jobject ElUtil::GetJavaFeatureGroupInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IFeatureGroupInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGW("GetJavaFeatureGroupInfo: Invalid argumenet!");
        return NULL;
    }

    jclass fgiKlass = env->FindClass("android/content/pm/FeatureGroupInfo");
    CheckErrorAndLog(env, "FindClass: FeatureGroupInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(fgiKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: FeatureGroupInfo : %d!\n", __LINE__);

    jobject jinfo = env->NewObject(fgiKlass, m);
    CheckErrorAndLog(env, "NewObject: FeatureGroupInfo : %d!\n", __LINE__);

    AutoPtr<ArrayOf<IFeatureInfo*> > features;
    info->GetFeatures((ArrayOf<IFeatureInfo*>**)&features);
    if (features != NULL) {
        Int32 count = features->GetLength();

        jclass fiKlass = env->FindClass("android/content/pm/FeatureInfo");
        CheckErrorAndLog(env, "FindClass: FeatureInfo : %d!\n", __LINE__);

        jobjectArray jfeatures = env->NewObjectArray((jsize)count, fiKlass, 0);
        CheckErrorAndLog(env, "NewObjectArray: FeatureInfo : %d!\n", __LINE__);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jfeature = ElUtil::GetJavaFeatureInfo(env, (*features)[i]);
            if (jfeature != NULL) {
                env->SetObjectArrayElement(jfeatures, i, jfeature);
                env->DeleteLocalRef(jfeature);
            }
            else {
                ALOGE("GetJavaPackageInfo() GetJavaFeatureInfo fail!");
            }
        }

        jfieldID f = env->GetFieldID(fgiKlass, "features", "[Landroid/content/pm/FeatureInfo;");
        CheckErrorAndLog(env, "GetFieldID: features : %d!\n", __LINE__);

        env->SetObjectField(jinfo, f, jfeatures);
        CheckErrorAndLog(env, "SetObjectField: features : %d!\n", __LINE__);

        env->DeleteLocalRef(fiKlass);
        env->DeleteLocalRef(jfeatures);
    }

    env->DeleteLocalRef(fgiKlass);
    return jinfo;
}

bool ElUtil::ToElCharArrayBuffer(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jbuffer,
    /* [out] */ ICharArrayBuffer** buffer)
{
    if (env == NULL || jbuffer == NULL || buffer == NULL) {
        ALOGE("ToElCharArrayBuffer: Invalid argumenet!");
        return FALSE;
    }

    jclass cbKlass = env->FindClass("android/database/CharArrayBuffer");
    ElUtil::CheckErrorAndLog(env, "FindClass: CharArrayBuffer %d", __LINE__);

    jfieldID f = env->GetFieldID(cbKlass, "data", "[C");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: data %d", __LINE__);

    jcharArray jdata = (jcharArray)env->GetObjectField(jbuffer, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: data %d", __LINE__);

    AutoPtr<ArrayOf<Char32> > data;
    if (!ElUtil::ToElCharArray(env, jdata, (ArrayOf<Char32>**)&data)) {
        ALOGE("ToElCharArrayBuffer() ToElCharArray fail!");
        env->DeleteLocalRef(jdata);
        return FALSE;
    }

    if (NOERROR != CCharArrayBuffer::New(data, buffer)) {
        ALOGE("ToElVpnProfile() Create CVpnProfile fail!");
        env->DeleteLocalRef(jdata);
        return FALSE;
    }

    Int32 sizeCopied = GetJavaIntField(env, cbKlass, jbuffer, "sizeCopied", "ToElCharArrayBuffer");
    (*buffer)->SetSizeCopied(sizeCopied);

    env->DeleteLocalRef(cbKlass);
    env->DeleteLocalRef(jdata);
    return TRUE;
}

jobject ElUtil::GetJavaPackageCleanItem(
    /* [in] */ JNIEnv* env,
    /* [in] */ IPackageCleanItem* pkgCItem)
{
    if (env == NULL || pkgCItem == NULL) {
        ALOGE("GetJavaPackageCleanItem: Invalid argumenet!");
        return NULL;
    }

    Int32 userId;
    pkgCItem->GetUserId(&userId);

    String packageName;
    pkgCItem->GetPackageName(&packageName);
    jstring jpackageName = ElUtil::GetJavaString(env, packageName);

    Boolean andCode;
    pkgCItem->GetAndCode(&andCode);

    jclass pciKlass = env->FindClass("android/content/pm/PackageCleanItem");
    CheckErrorAndLog(env, "FindClass: PackageCleanItem : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(pciKlass, "<init>", "(ILjava/lang/String;Z)V");
    CheckErrorAndLog(env, "GetMethodID: PackageCleanItem : %d!\n", __LINE__);

    jobject jpkgCItem = env->NewObject(pciKlass, m, (jint)userId, jpackageName, (jboolean)andCode);
    CheckErrorAndLog(env, "NewObject: PackageCleanItem : %d!\n", __LINE__);

    env->DeleteLocalRef(jpackageName);
    env->DeleteLocalRef(pciKlass);

    return jpkgCItem;
}

bool ElUtil::ToElPackageCleanItem(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jpkgCItem,
    /* [out] */ IPackageCleanItem** pkgCItem)
{
    if (env == NULL || jpkgCItem == NULL || pkgCItem == NULL) {
        ALOGE("ToElPackageCleanItem: Invalid argumenet!");
        return FALSE;
    }

    jclass pciKlass = env->FindClass("android/content/pm/PackageCleanItem");
    CheckErrorAndLog(env, "FindClass: PackageCleanItem : %d!\n", __LINE__);

    Int32 userId = ElUtil::GetJavaIntField(env, pciKlass, jpkgCItem, "userId", "ToElPackageCleanItem");
    String packageName = ElUtil::GetJavaStringField(env, pciKlass, jpkgCItem, "packageName", "ToElPackageCleanItem");
    Boolean andCode = ElUtil::GetJavaBoolField(env, pciKlass, jpkgCItem, "andCode", "ToElPackageCleanItem");

    if (NOERROR != CPackageCleanItem::New(userId, packageName, andCode, pkgCItem)) {
        ALOGE("ToElPackageCleanItem() Create CPackageCleanItem fail!");
        return FALSE;
    }

    env->DeleteLocalRef(pciKlass);
    return TRUE;
}

jobject ElUtil::GetJavaWaitResult(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityManagerWaitResult* amResult)
{
    if (env == NULL || amResult == NULL) {
        ALOGE("GetJavaWaitResult: Invalid argumenet!");
        return NULL;
    }

    jclass wrKlass = env->FindClass("android/app/IActivityManager$WaitResult");
    CheckErrorAndLog(env, "FindClass: WaitResult : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(wrKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: WaitResult : %d!\n", __LINE__);

    jobject jamResult = env->NewObject(wrKlass, m);
    CheckErrorAndLog(env, "NewObject: WaitResult : %d!\n", __LINE__);

    Int32 result = 0;
    amResult->GetResult(&result);
    SetJavaIntField(env, wrKlass, jamResult, result, "result", "GetJavaWaitResult");

    Boolean timeout = FALSE;
    amResult->GetTimeout(&timeout);
    ElUtil::SetJavaBoolField(env, wrKlass, jamResult, timeout, "timeout", "GetJavaWaitResult");

    AutoPtr<IComponentName> who;
    amResult->GetWho((IComponentName**)&who);
    if (who != NULL) {
        jobject jwho = GetJavaComponentName(env, who);
        if (jwho != NULL) {
            jfieldID f = env->GetFieldID(wrKlass, "who", "Landroid/content/ComponentName;");
            ElUtil::CheckErrorAndLog(env, "GetFieldID: who : %d!\n", __LINE__);

            env->SetObjectField(jamResult, f, jwho);
            ElUtil::CheckErrorAndLog(env, "SetObjectField: who : %d!\n", __LINE__);
            env->DeleteLocalRef(jwho);
        }
    }

    Int64 tempLong = 0;
    amResult->GetThisTime(&tempLong);
    ElUtil::SetJavalongField(env, wrKlass, jamResult, tempLong, "thisTime", "GetJavaWaitResult");

    amResult->GetTotalTime(&tempLong);
    ElUtil::SetJavalongField(env, wrKlass, jamResult, tempLong, "totalTime", "GetJavaWaitResult");

    env->DeleteLocalRef(wrKlass);
    return jamResult;
}

jobject ElUtil::GetJavaTaskThumbnail(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityManagerTaskThumbnail* thumbnail)
{
    if (env == NULL || thumbnail == NULL) {
        ALOGE("GetJavaTaskThumbnail: Invalid argumenet!");
        return NULL;
    }

    jclass ttKlass = env->FindClass("android/app/ActivityManager$TaskThumbnails");
    CheckErrorAndLog(env, "FindClass: TaskThumbnails : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(ttKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: TaskThumbnails : %d!\n", __LINE__);

    jobject jthumbnail = env->NewObject(ttKlass, m);
    CheckErrorAndLog(env, "NewObject: TaskThumbnails : %d!\n", __LINE__);

    AutoPtr<IBitmap> mainThumbnail;
    thumbnail->GetMainThumbnail((IBitmap**)&mainThumbnail);
    if (mainThumbnail != NULL) {
        jobject jmainThumbnail = ElUtil::GetJavaBitmap(env, mainThumbnail);
        if (jmainThumbnail != NULL) {
            jfieldID f = env->GetFieldID(ttKlass, "mainThumbnail", "Landroid/graphics/Bitmap;");
            ElUtil::CheckErrorAndLog(env, "GetFieldID: mainThumbnail : %d!\n", __LINE__);

            env->SetObjectField(jthumbnail, f, jmainThumbnail);
            ElUtil::CheckErrorAndLog(env, "SetObjectField: mainThumbnail : %d!\n", __LINE__);
            env->DeleteLocalRef(jmainThumbnail);
        }
    }

    AutoPtr<IParcelFileDescriptor> thumbnailFileDescriptor;
    thumbnail->GetThumbnailFileDescriptor((IParcelFileDescriptor**)&thumbnailFileDescriptor);
    if (thumbnailFileDescriptor != NULL) {
        jobject jthumbnailFileDescriptor = GetJavaParcelFileDescriptor(env, thumbnailFileDescriptor);

        jfieldID f = env->GetFieldID(ttKlass, "thumbnailFileDescriptor", "Landroid/graphics/Bitmap;");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: thumbnailFileDescriptor : %d!\n", __LINE__);

        env->SetObjectField(jthumbnail, f, jthumbnailFileDescriptor);
        ElUtil::CheckErrorAndLog(env, "SetObjectField: thumbnailFileDescriptor : %d!\n", __LINE__);

        env->DeleteLocalRef(jthumbnailFileDescriptor);
    }

    env->DeleteLocalRef(ttKlass);
    return jthumbnail;
}

jobject ElUtil::GetJavaProcessErrorStateInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityManagerProcessErrorStateInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaProcessErrorStateInfo: Invalid argumenet!");
        return NULL;
    }

    jclass pesiKlass = env->FindClass("android/app/ActivityManager$ProcessErrorStateInfo");
    CheckErrorAndLog(env, "FindClass: ProcessErrorStateInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(pesiKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: ProcessErrorStateInfo : %d!\n", __LINE__);

    jobject jinfo = env->NewObject(pesiKlass, m);
    CheckErrorAndLog(env, "NewObject: ProcessErrorStateInfo : %d!\n", __LINE__);

    Int32 tempInt = 0;
    info->GetCondition(&tempInt);
    SetJavaIntField(env, pesiKlass, jinfo, tempInt, "condition", "GetJavaProcessErrorStateInfo");

    String processName;
    info->GetProcessName(&processName);
    ElUtil::SetJavaStringField(env, pesiKlass, jinfo, processName, "processName", "GetJavaProcessErrorStateInfo");

    info->GetPid(&tempInt);
    SetJavaIntField(env, pesiKlass, jinfo, tempInt, "pid", "GetJavaProcessErrorStateInfo");

    info->GetUid(&tempInt);
    SetJavaIntField(env, pesiKlass, jinfo, tempInt, "uid", "GetJavaProcessErrorStateInfo");

    String tag;
    info->GetTag(&tag);
    ElUtil::SetJavaStringField(env, pesiKlass, jinfo, tag, "tag", "GetJavaProcessErrorStateInfo");

    String shortMsg;
    info->GetShortMsg(&shortMsg);
    ElUtil::SetJavaStringField(env, pesiKlass, jinfo, shortMsg, "shortMsg", "GetJavaProcessErrorStateInfo");

    String longMsg;
    info->GetLongMsg(&longMsg);
    ElUtil::SetJavaStringField(env, pesiKlass, jinfo, longMsg, "longMsg", "GetJavaProcessErrorStateInfo");

    String stackTrace;
    info->GetStackTrace(&stackTrace);
    ElUtil::SetJavaStringField(env, pesiKlass, jinfo, stackTrace, "stackTrace", "GetJavaProcessErrorStateInfo");

    AutoPtr<ArrayOf<Byte> > crashData;
    info->GetCrashData((ArrayOf<Byte> **)&crashData);
    if (crashData != NULL) {
        jbyteArray jcrashData = GetJavaByteArray(env, crashData);

        jfieldID f = env->GetFieldID(pesiKlass, "crashData", "[B");
        ElUtil::CheckErrorAndLog(env, "GetFieldID: crashData : %d!\n", __LINE__);

        env->SetObjectField(jinfo, f, jcrashData);
        ElUtil::CheckErrorAndLog(env, "SetObjectField: crashData : %d!\n", __LINE__);

        env->DeleteLocalRef(jcrashData);
    }

    env->DeleteLocalRef(pesiKlass);
    return jinfo;
}

bool ElUtil::ToElStrictModeViolationInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IStrictModeViolationInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElStrictModeViolationInfo: Invalid argumenet!");
        return FALSE;
    }

    *info = NULL;

    AutoPtr<IParcel> parcel;
    CParcel::New((IParcel**)&parcel);

    jclass smvKlass = env->FindClass("android/os/StrictMode$ViolationInfo");
    CheckErrorAndLog(env, "FindClass: ViolationInfo : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(smvKlass, "crashInfo", "Landroid/app/ApplicationErrorReport$CrashInfo;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: crashInfo : %d!\n", __LINE__);

    jobject jcrashInfo = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: crashInfo : %d!\n", __LINE__);

    AutoPtr<IApplicationErrorReportCrashInfo> crashInfo;
    ToElApplicationErrorReportCrashInfo(env, jcrashInfo, (IApplicationErrorReportCrashInfo**)&crashInfo);
    parcel->WriteInterfacePtr(crashInfo);

    Int32 policy = GetJavaIntField(env, smvKlass, jinfo, "policy", "ToElStrictModeViolationInfo");
    parcel->WriteInt32(policy);

    Int32 durationMillis = GetJavaIntField(env, smvKlass, jinfo, "durationMillis", "ToElStrictModeViolationInfo");
    parcel->WriteInt32(durationMillis);

    Int32 violationNumThisLoop = GetJavaIntField(env, smvKlass, jinfo, "violationNumThisLoop", "ToElStrictModeViolationInfo");
    parcel->WriteInt32(violationNumThisLoop);

    Int32 numAnimationsRunning = GetJavaIntField(env, smvKlass, jinfo, "numAnimationsRunning", "ToElStrictModeViolationInfo");
    parcel->WriteInt32(numAnimationsRunning);

    Int64 violationUptimeMillis = GetJavalongField(env, smvKlass, jinfo, "violationUptimeMillis", "ToElStrictModeViolationInfo");
    parcel->WriteInt64(violationUptimeMillis);

    Int64 numInstances = GetJavalongField(env, smvKlass, jinfo, "numInstances", "ToElStrictModeViolationInfo");
    parcel->WriteInt64(numInstances);

    String broadcastIntentAction = GetJavaStringField(env, smvKlass, jinfo, "broadcastIntentAction", "ToElStrictModeViolationInfo");
    parcel->WriteString(broadcastIntentAction);

    f = env->GetFieldID(smvKlass, "tags", "[Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: tags : %d!\n", __LINE__);

    jobjectArray jtags = (jobjectArray)env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: tags : %d!\n", __LINE__);

    AutoPtr<ArrayOf<String> > tags;
    if (jtags != NULL)
        ToElStringArray(env, jtags, (ArrayOf<String>**)&tags);
    parcel->WriteArrayOfString(tags);

    env->DeleteLocalRef(smvKlass);
    env->DeleteLocalRef(jcrashInfo);
    env->DeleteLocalRef(jtags);

    parcel->SetDataPosition(0);
    if (NOERROR != CStrictModeViolationInfo::New(parcel, info)) {
        ALOGE("ToElPackageCleanItem() Create CPackageCleanItem fail!");
        return FALSE;
    }

    return TRUE;
}

bool ElUtil::ToElPermissionInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IPermissionInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElPermissionInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass piKlass = env->FindClass("android/content/pm/PermissionInfo");
    CheckErrorAndLog(env, "FindClass: PermissionInfo : %d!\n", __LINE__);

    if (NOERROR != CPermissionInfo::New(info)) {
        ALOGE("ToElPermissionInfo() Create CPermissionInfo fail!");
        return FALSE;
    }

    Int32 tempInt = ElUtil::GetJavaIntField(env, piKlass, jinfo, "protectionLevel", "ToElPermissionInfo");
    (*info)->SetProtectionLevel(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, piKlass, jinfo, "flags", "ToElPermissionInfo");
    (*info)->SetFlags(tempInt);

    String group = ElUtil::GetJavaStringField(env, piKlass, jinfo, "group", "ToElPermissionInfo");
    (*info)->SetGroup(group);

    tempInt = ElUtil::GetJavaIntField(env, piKlass, jinfo, "descriptionRes", "ToElPermissionInfo");
    (*info)->SetDescriptionRes(tempInt);

    jfieldID f = env->GetFieldID(piKlass, "nonLocalizedDescription", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID nonLocalizedDescription : %d!\n", __LINE__);

    jobject jnonLocalizedDescription = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField nonLocalizedDescription : %d!\n", __LINE__);
    AutoPtr<ICharSequence> nonLocalizedDescription;
    if (jnonLocalizedDescription != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jsnonLocalizedDescription = (jstring)env->CallObjectMethod(jnonLocalizedDescription, m);
        CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String snonLocalizedDescription = ElUtil::ToElString(env, jsnonLocalizedDescription);
        CString::New(snonLocalizedDescription, (ICharSequence**)&nonLocalizedDescription);

        (*info)->SetNonLocalizedDescription(nonLocalizedDescription);

        env->DeleteLocalRef(csClass);
        env->DeleteLocalRef(jsnonLocalizedDescription);
        env->DeleteLocalRef(jnonLocalizedDescription);
    }

    AutoPtr<IPackageItemInfo> pkgItemInfo = IPackageItemInfo::Probe(*info);
    SetElPackageItemInfo(env, piKlass, jinfo, pkgItemInfo);

    env->DeleteLocalRef(piKlass);
    return TRUE;
}

void ElUtil::SetElPackageItemInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass c,
    /* [in] */ jobject jinfo,
    /* [in] */ IPackageItemInfo* info)
{
    if (env == NULL || c == NULL || jinfo == NULL || info == NULL) {
        ALOGE("SetElPackageItemInfo: Invalid argumenet!");
        return;
    }

    String name = ElUtil::GetJavaStringField(env, c, jinfo, "name", "SetElPackageItemInfo");
    info->SetName(name);

    String packageName = ElUtil::GetJavaStringField(env, c, jinfo, "packageName", "SetElPackageItemInfo");
    info->SetPackageName(packageName);

    Int32 tempInt = ElUtil::GetJavaIntField(env, c, jinfo, "labelRes", "SetElPackageItemInfo");
    info->SetLabelRes(tempInt);

    jfieldID f = env->GetFieldID(c, "nonLocalizedLabel", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID nonLocalizedLabel : %d!\n", __LINE__);

    jobject jnonLocalizedLabel = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField nonLocalizedDescription : %d!\n", __LINE__);
    AutoPtr<ICharSequence> nonLocalizedLabel;
    if (jnonLocalizedLabel != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jsnonLocalizedLabel = (jstring)env->CallObjectMethod(jnonLocalizedLabel, m);
        CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String snonLocalizedLabel = ElUtil::ToElString(env, jsnonLocalizedLabel);
        CString::New(snonLocalizedLabel, (ICharSequence**)&nonLocalizedLabel);

        info->SetNonLocalizedLabel(nonLocalizedLabel);

        env->DeleteLocalRef(csClass);
        env->DeleteLocalRef(jsnonLocalizedLabel);
        env->DeleteLocalRef(jnonLocalizedLabel);
    }

    tempInt = ElUtil::GetJavaIntField(env, c, jinfo, "icon", "SetElPackageItemInfo");
    info->SetIcon(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, c, jinfo, "logo", "SetElPackageItemInfo");
    info->SetLogo(tempInt);

    f = env->GetFieldID(c, "metaData", "Landroid/os/Bundle;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: metaData %d", __LINE__);

    jobject jmetaData = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: metaData %d", __LINE__);
    if (jmetaData != NULL) {
        AutoPtr<IBundle> metaData;
        if (ElUtil::ToElBundle(env, jmetaData, (IBundle**)&metaData)) {
            info->SetMetaData(metaData);
        }
        else {
            ALOGE("SetElPackageItemInfo() ToElBundle fail!");
        }

        env->DeleteLocalRef(jmetaData);
    }

    tempInt = GetJavaIntField(env, c, jinfo, "banner", "SetElPackageItemInfo");
    info->SetBanner(tempInt);

    tempInt = GetJavaIntField(env, c, jinfo, "showUserIcon", "SetElPackageItemInfo");
    info->SetShowUserIcon(tempInt);

    tempInt = GetJavaIntField(env, c, jinfo, "themedIcon", "SetElPackageItemInfo");
    info->SetThemedIcon(tempInt);
}

jobject ElUtil::GetJavaInstrumentationInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IInstrumentationInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaInstrumentationInfo: Invalid argumenet!");
        return NULL;
    }

    jclass iiKlass = env->FindClass("android/content/pm/InstrumentationInfo");
    CheckErrorAndLog(env, "FindClass: InstrumentationInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(iiKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: InstrumentationInfo : %d!\n", __LINE__);

    jobject jinfo = env->NewObject(iiKlass, m);
    CheckErrorAndLog(env, "NewObject: InstrumentationInfo : %d!\n", __LINE__);

    String targetPackage;
    info->GetTargetPackage(&targetPackage);
    ElUtil::SetJavaStringField(env, iiKlass, jinfo, targetPackage, "targetPackage", "GetJavaInstrumentationInfo");

    String sourceDir;
    info->GetSourceDir(&sourceDir);
    ElUtil::SetJavaStringField(env, iiKlass, jinfo, sourceDir, "sourceDir", "GetJavaInstrumentationInfo");

    String publicSourceDir;
    info->GetPublicSourceDir(&publicSourceDir);
    ElUtil::SetJavaStringField(env, iiKlass, jinfo, publicSourceDir, "publicSourceDir", "GetJavaInstrumentationInfo");

    String dataDir;
    info->GetDataDir(&dataDir);
    ElUtil::SetJavaStringField(env, iiKlass, jinfo, dataDir, "dataDir", "GetJavaInstrumentationInfo");

    String nativeLibraryDir;
    info->GetNativeLibraryDir(&nativeLibraryDir);
    ElUtil::SetJavaStringField(env, iiKlass, jinfo, nativeLibraryDir, "nativeLibraryDir", "GetJavaInstrumentationInfo");

    Boolean tempBool;
    info->GetHandleProfiling(&tempBool);
    ElUtil::SetJavaBoolField(env, iiKlass, jinfo, tempBool, "handleProfiling", "GetJavaInstrumentationInfo");

    info->GetFunctionalTest(&tempBool);
    ElUtil::SetJavaBoolField(env, iiKlass, jinfo, tempBool, "functionalTest", "GetJavaInstrumentationInfo");

    AutoPtr<IPackageItemInfo> pkgInfo = IPackageItemInfo::Probe(info);
    SetJavaPackageItemInfo(env, iiKlass, jinfo, pkgInfo);

    env->DeleteLocalRef(iiKlass);
    return jinfo;
}

jobject ElUtil::GetJavaVerifierDeviceIdentity(
    /* [in] */ JNIEnv* env,
    /* [in] */ IVerifierDeviceIdentity* identity)
{
    if (env == NULL || identity == NULL) {
        ALOGE("GetJavaVerifierDeviceIdentity: Invalid argumenet!");
        return NULL;
    }

    AutoPtr<IParcelable> parcelable = IParcelable::Probe(identity);
    if (parcelable == NULL) {
        ALOGE("GetJavaVerifierDeviceIdentity: Invalid IParcelable!");
        return NULL;
    }

    AutoPtr<IParcel> source;
    CParcel::New((IParcel**)&source);

    parcelable->WriteToParcel(source);
    source->SetDataPosition(0);

    Int64 id = 0;
    source->ReadInt64(&id);

    jclass vdiKlass = env->FindClass("android/content/pm/VerifierDeviceIdentity");
    CheckErrorAndLog(env, "FindClass: VerifierDeviceIdentity : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(vdiKlass, "<init>", "(J)V");
    CheckErrorAndLog(env, "GetMethodID: VerifierDeviceIdentity : %d!\n", __LINE__);

    jobject jidentity = env->NewObject(vdiKlass, m, (jlong)id);
    CheckErrorAndLog(env, "NewObject: VerifierDeviceIdentity : %d!\n", __LINE__);

    env->DeleteLocalRef(vdiKlass);
    return jidentity;
}

bool ElUtil::ToElWifiP2pWfdInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IWifiP2pWfdInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElWifiP2pWfdInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass piKlass = env->FindClass("android/net/wifi/p2p/WifiP2pWfdInfo");
    CheckErrorAndLog(env, "FindClass: WifiP2pWfdInfo : %d!\n", __LINE__);

    Int32 deviceInfo = ElUtil::GetJavaIntField(env, piKlass, jinfo, "mDeviceInfo", "ToElWifiP2pWfdInfo");
    Int32 ctrlPort = ElUtil::GetJavaIntField(env, piKlass, jinfo, "mCtrlPort", "ToElWifiP2pWfdInfo");
    Int32 maxThroughput = ElUtil::GetJavaIntField(env, piKlass, jinfo, "mMaxThroughput", "ToElWifiP2pWfdInfo");

    if (NOERROR != CWifiP2pWfdInfo::New(deviceInfo, ctrlPort, maxThroughput, info)) {
        ALOGE("ToElWifiP2pWfdInfo() Create CWifiP2pWfdInfo fail!");
        return FALSE;
    }

    Boolean wfdEnabled = ElUtil::GetJavaBoolField(env, piKlass, jinfo, "mWfdEnabled", "ToElWifiP2pWfdInfo");
    (*info)->SetWfdEnabled(wfdEnabled);

    env->DeleteLocalRef(piKlass);
    return TRUE;
}

bool ElUtil::ToElWifiP2pDevice(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jdev,
    /* [out] */ IWifiP2pDevice** dev)
{
    if (env == NULL || jdev == NULL || dev == NULL) {
        ALOGE("ToElWifiP2pDevice: Invalid argumenet!");
        return FALSE;
    }

    jclass wpdKlass = env->FindClass("android/net/wifi/p2p/WifiP2pDevice");
    CheckErrorAndLog(env, "FindClass: WifiP2pDevice : %d!\n", __LINE__);

    if (NOERROR != CWifiP2pDevice::New(dev)) {
        ALOGE("ToElWifiP2pDevice() Create CWifiP2pWfdInfo fail!");
        return FALSE;
    }

    String deviceName = ElUtil::GetJavaStringField(env, wpdKlass, jdev, "deviceName", "ToElWifiP2pDevice");
    (*dev)->SetDeviceName(deviceName);

    String deviceAddress = ElUtil::GetJavaStringField(env, wpdKlass, jdev, "deviceAddress", "ToElWifiP2pDevice");
    (*dev)->SetDeviceAddress(deviceAddress);

    String primaryDeviceType = ElUtil::GetJavaStringField(env, wpdKlass, jdev, "primaryDeviceType", "ToElWifiP2pDevice");
    (*dev)->SetPrimaryDeviceType(primaryDeviceType);

    String secondaryDeviceType = ElUtil::GetJavaStringField(env, wpdKlass, jdev, "secondaryDeviceType", "ToElWifiP2pDevice");
    (*dev)->SetSecondaryDeviceType(secondaryDeviceType);

    Int32 tempInt = ElUtil::GetJavaIntField(env, wpdKlass, jdev, "wpsConfigMethodsSupported", "ToElWifiP2pDevice");
    (*dev)->SetWpsConfigMethodsSupported(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, wpdKlass, jdev, "deviceCapability", "ToElWifiP2pDevice");
    (*dev)->SetDeviceCapability(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, wpdKlass, jdev, "groupCapability", "ToElWifiP2pDevice");
    (*dev)->SetGroupCapability(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, wpdKlass, jdev, "status", "ToElWifiP2pDevice");
    (*dev)->SetStatus(tempInt);

    jfieldID f = env->GetFieldID(wpdKlass, "wfdInfo", "Landroid/net/wifi/p2p/WifiP2pWfdInfo;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: wfdInfo %d", __LINE__);

    jobject jwfdInfo = env->GetObjectField(jdev, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: wfdInfo %d", __LINE__);
    if (jwfdInfo != NULL) {
        AutoPtr<IWifiP2pWfdInfo> wfdInfo;
        if (ElUtil::ToElWifiP2pWfdInfo(env, jwfdInfo, (IWifiP2pWfdInfo**)&wfdInfo)) {
            (*dev)->SetWfdInfo(wfdInfo);
        }
        else {
            ALOGE("ToElWifiP2pDevice() ToElWifiP2pWfdInfo fail!");
        }

        env->DeleteLocalRef(jwfdInfo);
    }

    env->DeleteLocalRef(wpdKlass);
    return TRUE;
}

bool ElUtil::ToElWifiP2pInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IWifiP2pInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElWifiP2pInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass wpKlass = env->FindClass("android/net/wifi/p2p/WifiP2pInfo");
    CheckErrorAndLog(env, "FindClass: WifiP2pInfo : %d!\n", __LINE__);

    if (NOERROR != CWifiP2pInfo::New(info)) {
        ALOGE("ToElWifiP2pInfo() Create CWifiP2pInfo fail!");
        return FALSE;
    }

    Boolean tempBool = ElUtil::GetJavaBoolField(env, wpKlass, jinfo, "groupFormed", "ToElWifiP2pInfo");
    (*info)->SetGroupFormed(tempBool);

    tempBool = ElUtil::GetJavaBoolField(env, wpKlass, jinfo, "isGroupOwner", "ToElWifiP2pInfo");
    (*info)->SetIsGroupOwner(tempBool);

    jfieldID f = env->GetFieldID(wpKlass, "groupOwnerAddress", "Ljava/net/InetAddress;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: groupOwnerAddress %d", __LINE__);

    jobject jgroupOwnerAddress = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: groupOwnerAddress %d", __LINE__);
    if (jgroupOwnerAddress != NULL) {
        AutoPtr<IInetAddress> groupOwnerAddress;
        if (ElUtil::ToElInetAddress(env, jgroupOwnerAddress, (IInetAddress**)&groupOwnerAddress)) {
            (*info)->SetGroupOwnerAddress(groupOwnerAddress);
        }
        else {
            ALOGE("ToElWifiP2pDevice() ToElInetAddress fail!");
        }

        env->DeleteLocalRef(jgroupOwnerAddress);
    }

    env->DeleteLocalRef(wpKlass);
    return TRUE;
}

bool ElUtil::ToElWifiP2pConfig(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconfig,
    /* [out] */ IWifiP2pConfig** config)
{
    if (env == NULL || jconfig == NULL || config == NULL) {
        ALOGE("ToElWifiP2pConfig: Invalid argumenet!");
        return FALSE;
    }

    jclass wpcKlass = env->FindClass("android/net/wifi/p2p/WifiP2pConfig");
    CheckErrorAndLog(env, "FindClass: WifiP2pConfig : %d!\n", __LINE__);

    if (NOERROR != CWifiP2pConfig::New(config)) {
        ALOGE("ToElWifiP2pConfig() Create CWifiP2pConfig fail!");
        return FALSE;
    }

    String deviceAddress = ElUtil::GetJavaStringField(env, wpcKlass, jconfig, "deviceAddress", "ToElWifiP2pConfig");
    (*config)->SetDeviceAddress(deviceAddress);

    jfieldID f = env->GetFieldID(wpcKlass, "wps", "Landroid/net/wifi/WpsInfo;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: wps %d", __LINE__);

    jobject jwps = env->GetObjectField(jconfig, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: wps %d", __LINE__);
    if (jwps != NULL) {
        AutoPtr<IWpsInfo> wps;
        if (ElUtil::ToElWpsInfo(env, jwps, (IWpsInfo**)&wps)) {
            (*config)->SetWps(wps);
        }
        else {
            ALOGE("ToElWifiP2pConfig() ToElWpsInfo fail!");
        }

        env->DeleteLocalRef(jwps);
    }

    Int32 tempInt = ElUtil::GetJavaIntField(env, wpcKlass, jconfig, "groupOwnerIntent", "ToElWifiP2pConfig");
    (*config)->SetGroupOwnerIntent(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, wpcKlass, jconfig, "netId", "ToElWifiP2pConfig");
    (*config)->SetNetId(tempInt);

    env->DeleteLocalRef(wpcKlass);
    return TRUE;
}

bool ElUtil::ToElWpsInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IWpsInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElWpsInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass wpsKlass = env->FindClass("android/net/wifi/WpsInfo");
    CheckErrorAndLog(env, "FindClass: WifiP2pConfig : %d!\n", __LINE__);

    if (NOERROR != CWpsInfo::New(info)) {
        ALOGE("ToElWpsInfo() Create CWpsInfo fail!");
        return FALSE;
    }

    Int32 setup = ElUtil::GetJavaIntField(env, wpsKlass, jinfo, "setup", "ToElWpsInfo");
    (*info)->SetSetup(setup);

    String BSSID = ElUtil::GetJavaStringField(env, wpsKlass, jinfo, "BSSID", "ToElWpsInfo");
    (*info)->SetBSSID(BSSID);

    String pin = ElUtil::GetJavaStringField(env, wpsKlass, jinfo, "pin", "ToElWpsInfo");
    (*info)->SetPin(pin);

    env->DeleteLocalRef(wpsKlass);
    return TRUE;
}

bool ElUtil::ToElCompatibilityInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ ICompatibilityInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElCompatibilityInfo: Invalid argumenet!");
        return FALSE;
    }

    if (NOERROR != CCompatibilityInfo::New(info)) {
        ALOGE("ToElCompatibilityInfo() Create CWpsInfo fail!");
        return FALSE;
    }

    jclass ciKlass = env->FindClass("android/content/res/CompatibilityInfo");
    CheckErrorAndLog(env, "FindClass: CompatibilityInfo : %d!\n", __LINE__);

    Int32 compFlags = ElUtil::GetJavaIntField(env, ciKlass, jinfo, "mCompatibilityFlags", "ToElCompatibilityInfo");
    Int32 dens = ElUtil::GetJavaIntField(env, ciKlass, jinfo, "applicationDensity", "ToElCompatibilityInfo");
    Float scale = ElUtil::GetJavafloatField(env, ciKlass, jinfo, "applicationScale", "ToElCompatibilityInfo");
    Float invertedScale = ElUtil::GetJavafloatField(env, ciKlass, jinfo, "applicationInvertedScale", "ToElCompatibilityInfo");
    Boolean isThemeable = ElUtil::GetJavaBoolField(env, ciKlass, jinfo, "isThemeable", "ToElCompatibilityInfo");

    AutoPtr<IParcel> parcel;
    Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);
    parcel->WriteInt32(compFlags);
    parcel->WriteInt32(dens);
    parcel->WriteFloat(scale);
    parcel->WriteFloat(invertedScale);
    parcel->WriteBoolean(isThemeable);
    parcel->SetDataPosition(0);

    IParcelable::Probe(parcel)->ReadFromParcel(parcel);

    env->DeleteLocalRef(ciKlass);
    return TRUE;
}

jobject ElUtil::GetJavaWindowInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWindowInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaWindowInfo: Invalid argumenet!");
        return NULL;
    }

    jclass wiKlass = env->FindClass("android/view/WindowInfo");
    CheckErrorAndLog(env, "FindClass: WindowInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(wiKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: WindowInfo : %d!\n", __LINE__);

    jobject jinfo = env->NewObject(wiKlass, m);
    CheckErrorAndLog(env, "NewObject: WindowInfo : %d!\n", __LINE__);

    ALOGE("GetJavaWindowInfo() E_NOT_IMPLEMENTED!");

    env->DeleteLocalRef(wiKlass);
    return jinfo;
}

jobject ElUtil::GetJavaWifiDisplay(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiDisplay* display)
{
    if (env == NULL || display == NULL) {
        ALOGE("GetJavaWifiDisplay: Invalid argumenet!");
        return NULL;
    }

    jclass wdKlass = env->FindClass("android/hardware/display/WifiDisplay");
    CheckErrorAndLog(env, "FindClass: WifiDisplay : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(wdKlass, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V");
    CheckErrorAndLog(env, "GetMethodID: WifiDisplay : %d!\n", __LINE__);

    String deviceAddress;
    display->GetDeviceAddress(&deviceAddress);
    jstring jdeviceAddress = ElUtil::GetJavaString(env, deviceAddress);

    String deviceName;
    display->GetDeviceName(&deviceName);
    jstring jdeviceName = ElUtil::GetJavaString(env, deviceName);

    String deviceAlias;
    display->GetDeviceAlias(&deviceAlias);
    jstring jdeviceAlias = ElUtil::GetJavaString(env, deviceAlias);

    Boolean isAvailable;
    display->IsAvailable(&isAvailable);

    Boolean canConnect;
    display->CanConnect(&canConnect);

    Boolean isRemembered;
    display->IsRemembered(&isRemembered);

    jobject jdisplay = env->NewObject(wdKlass, m, jdeviceAddress, jdeviceName, jdeviceAlias,
        isAvailable, canConnect, isRemembered);
    CheckErrorAndLog(env, "NewObject: WifiDisplay : %d!\n", __LINE__);

    env->DeleteLocalRef(wdKlass);
    return jdisplay;
}

jobject ElUtil::GetJavaWifiDisplayStatus(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiDisplayStatus* status)
{
    if (env == NULL || status == NULL) {
        ALOGE("GetJavaWifiDisplayStatus: Invalid argumenet!");
        return NULL;
    }

    Int32 featureState;
    status->GetFeatureState(&featureState);

    Int32 scanState;
    status->GetScanState(&scanState);

    Int32 activeDisplayState;
    status->GetActiveDisplayState(&activeDisplayState);

    AutoPtr<IWifiDisplay> activeDisplay;
    status->GetActiveDisplay((IWifiDisplay**)&activeDisplay);
    jobject jactiveDisplay = NULL;
    if (activeDisplay != NULL) {
        jactiveDisplay = GetJavaWifiDisplay(env, activeDisplay);
    }

    jclass wifiDisplayKlass = env->FindClass("android/hardware/display/WifiDisplay");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiDisplayStatus FindClass: WifiDisplay %d", __LINE__);

    AutoPtr<ArrayOf<IWifiDisplay*> > displays;
    status->GetDisplays((ArrayOf<IWifiDisplay*>**)&displays);

    jobjectArray jdisplays = NULL;
    if (displays != NULL){
        int size = displays->GetLength();
        jdisplays = env->NewObjectArray((jsize)size, wifiDisplayKlass, 0);
        ElUtil::CheckErrorAndLog(env, "GetJavaWifiDisplayStatus new object array: WifiDisplayStatus %d", __LINE__);

        for(Int32 i = 0; i < size; i++ ) {
            jobject jwifiDisplay = GetJavaWifiDisplay(env, (*displays)[i]);
            if (jwifiDisplay != NULL) {
                env->SetObjectArrayElement(jdisplays, i, jwifiDisplay);
                ElUtil::CheckErrorAndLog(env, "GetJavaWifiDisplayStatus SetObjectArrayElement fail %d", __LINE__);
                env->DeleteLocalRef(jwifiDisplay);
            }
        }
    }

    AutoPtr<IWifiDisplaySessionInfo> sessionInfo;
    status->GetSessionInfo((IWifiDisplaySessionInfo**)&sessionInfo);

    jobject jsessionInfo = NULL;
    if (sessionInfo != NULL) {
        jsessionInfo = GetJavaWifiDisplaySessionInfo(env, sessionInfo);
    }

    env->DeleteLocalRef(wifiDisplayKlass);

    jclass wifiDisplayStatusKlass = env->FindClass("android/hardware/display/WifiDisplayStatus");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiDisplayStatus FindClass: WifiDisplayStatus %d", __LINE__);

    jmethodID m = env->GetMethodID(wifiDisplayStatusKlass, "<init>", "(IIILandroid/hardware/display/WifiDisplay;"
        "[Landroid/hardware/display/WifiDisplay;Landroid/hardware/display/WifiDisplaySessionInfo;)V");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiDisplayStatus GetMethodID: WifiDisplayStatus() line: %d", __LINE__);

    jobject jstatus = env->NewObject(wifiDisplayStatusKlass, m, featureState, scanState, activeDisplayState,
        jactiveDisplay, jdisplays, jsessionInfo);

    env->DeleteLocalRef(wifiDisplayStatusKlass);
    env->DeleteLocalRef(jactiveDisplay);
    env->DeleteLocalRef(jdisplays);
    env->DeleteLocalRef(jsessionInfo);

    return jstatus;
}

jobject ElUtil::GetJavaWifiDisplaySessionInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiDisplaySessionInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaWifiDisplaySessionInfo Invalid arguments!");
        return NULL;
    }

    Boolean isClient;
    info->IsClient(&isClient);
    Int32 id;
    info->GetSessionId(&id);
    String gId;
    info->GetGroupId(&gId);
    jstring jgId = GetJavaString(env, gId);
    String pp;
    info->GetPassphrase(&pp);
    jstring jpp = GetJavaString(env, pp);
    String ip;
    info->GetIP(&ip);
    jstring jip = GetJavaString(env, ip);

    jclass infoKlass = env->FindClass("android/hardware/display/WifiDisplaySessionInfo");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiDisplaySessionInfo FindClass: WifiDisplaySessionInfo %d", __LINE__);

    jmethodID m = env->GetMethodID(infoKlass, "<init>", "(ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
    ElUtil::CheckErrorAndLog(env, "GetJavaWifiDisplaySessionInfo GetMethodID: WifiDisplaySessionInfo() line: %d", __LINE__);

    jobject jinfo = env->NewObject(infoKlass, m, isClient, id, jgId, jpp, jip);

    env->DeleteLocalRef(infoKlass);
    env->DeleteLocalRef(jgId);
    env->DeleteLocalRef(jpp);
    env->DeleteLocalRef(jip);

    return jinfo;
}

bool ElUtil::ToElInputMethodInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IInputMethodInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElInputMethodInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass imiKlass = env->FindClass("android/view/inputmethod/InputMethodInfo");
    CheckErrorAndLog(env, "FindClass: InputMethodInfo : %d!\n", __LINE__);


    if (NOERROR != CInputMethodInfo::New(info)) {
        ALOGE("ToElInputMethodInfo() Create CInputMethodInfo fail!");
        return FALSE;
    }

    AutoPtr<IParcel> parcel;
    Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);

    String id = ElUtil::GetJavaStringField(env, imiKlass, jinfo, "mId", "ToElInputMethodInfo");
    parcel->WriteString(id);

    String settingsActivityName = ElUtil::GetJavaStringField(env, imiKlass, jinfo, "mSettingsActivityName", "ToElInputMethodInfo");
    parcel->WriteString(settingsActivityName);

    Int32 isDefaultResId = ElUtil::GetJavaIntField(env, imiKlass, jinfo, "mIsDefaultResId", "ToElInputMethodInfo");
    parcel->WriteInt32(isDefaultResId);

    Boolean isAuxIme = ElUtil::GetJavaBoolField(env, imiKlass, jinfo, "mIsAuxIme", "ToElInputMethodInfo");
    parcel->WriteBoolean(isAuxIme);

    Boolean supportsSwitchingToNextInputMethod = ElUtil::GetJavaBoolField(
        env, imiKlass, jinfo, "mSupportsSwitchingToNextInputMethod", "ToElInputMethodInfo");
    parcel->WriteBoolean(supportsSwitchingToNextInputMethod);

    jfieldID f = env->GetFieldID(imiKlass, "mService", "Landroid/content/pm/ResolveInfo;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: mService %d", __LINE__);

    jobject jservice = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: mService %d", __LINE__);

    AutoPtr<IResolveInfo> service;
    if (jservice != NULL) {
        if (!ElUtil::ToElResolveInfo(env, jservice, (IResolveInfo**)&service)) {
            ALOGE("ToElInputMethodInfo() ToElResolveInfo fail!");
        }

        env->DeleteLocalRef(jservice);
    }
    parcel->WriteInterfacePtr((IInterface*)(IResolveInfo*)service.Get());

    f = env->GetFieldID(imiKlass, "mSubtypes", "Landroid/view/inputmethod/InputMethodSubtypeArray;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: mSubtypes %d", __LINE__);

    jobject jsubtypes = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: mSubtypes %d", __LINE__);

    if (jsubtypes != NULL) {
        jclass imsaKlass = env->FindClass("android/view/inputmethod/InputMethodSubtypeArray");
        CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(imsaKlass, "getCount", "()I");
        CheckErrorAndLog(env, "GetMethodID: getCount : %d!\n", __LINE__);

        jint jsize = env->CallIntMethod(jsubtypes, m);
        CheckErrorAndLog(env, "CallIntMethod: mActions : %d!\n", __LINE__);

        AutoPtr<IList> list;
        CParcelableList::New((IList**)&list);

        jmethodID mGet = env->GetMethodID(imsaKlass, "get", "(I)Landroid/view/inputmethod/InputMethodSubtype;");
        CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

        for (jint i = 0; i < jsize; i++) {
            jobject jsubtype = env->CallObjectMethod(jsubtypes, mGet, i);
            CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

            AutoPtr<IInputMethodSubtype> subtype;
            if (!ElUtil::ToElInputMethodSubtype(env, jsubtype, (IInputMethodSubtype**)&subtype)) {
                ALOGE("ToElInputMethodInfo() ToElInputMethodSubtype fail!");
            }
            else
                list->Add(subtype);
            env->DeleteLocalRef(jsubtype);
        }
        AutoPtr<IInputMethodSubtypeArray> subtypes;
        CInputMethodSubtypeArray::New(list, (IInputMethodSubtypeArray**)&subtypes);
        IParcelable::Probe(subtypes)->WriteToParcel(parcel);
        env->DeleteLocalRef(imsaKlass);
        env->DeleteLocalRef(jsubtypes);
    }
    else {
        parcel->WriteInt32(0);
    }

    parcel->SetDataPosition(0);
    IParcelable::Probe(*info)->ReadFromParcel(parcel);

    env->DeleteLocalRef(imiKlass);
    return TRUE;
}

bool ElUtil::ToElResolveInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IResolveInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElResolveInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass riKlass = env->FindClass("android/content/pm/ResolveInfo");
    CheckErrorAndLog(env, "FindClass: ResolveInfo : %d!\n", __LINE__);

    if (NOERROR != CResolveInfo::New(info)) {
        ALOGE("ToElResolveInfo() Create CResolveInfo fail!");
        return FALSE;
    }

    jfieldID f = env->GetFieldID(riKlass, "activityInfo", "Landroid/content/pm/ActivityInfo;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: activityInfo %d", __LINE__);

    jobject jactivityInfo = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: activityInfo %d", __LINE__);

    if (jactivityInfo != NULL) {
        AutoPtr<IActivityInfo> activityInfo;
        if (!ElUtil::ToElActivityInfo(env, jactivityInfo, (IActivityInfo**)&activityInfo)) {
            ALOGE("ToElResolveInfo() ToElActivityInfo fail!");
        }

        (*info)->SetActivityInfo(activityInfo);
        env->DeleteLocalRef(jactivityInfo);
    }

    f = env->GetFieldID(riKlass, "serviceInfo", "Landroid/content/pm/ServiceInfo;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: serviceInfo %d", __LINE__);

    jobject jserviceInfo = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: serviceInfo %d", __LINE__);

    if (jserviceInfo != NULL) {
        AutoPtr<IServiceInfo> serviceInfo;
        if (!ElUtil::ToElServiceInfo(env, jserviceInfo, (IServiceInfo**)&serviceInfo)) {
            ALOGE("ToElResolveInfo() ToElServiceInfo fail!");
        }

        (*info)->SetServiceInfo(serviceInfo);
        env->DeleteLocalRef(jserviceInfo);
    }

    f = env->GetFieldID(riKlass, "providerInfo", "Landroid/content/pm/ProviderInfo;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: providerInfo %d", __LINE__);

    jobject jproviderInfo = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: providerInfo %d", __LINE__);

    if (jproviderInfo != NULL) {
        AutoPtr<IProviderInfo> providerInfo;
        if (!ElUtil::ToElProviderInfo(env, jproviderInfo, (IProviderInfo**)&providerInfo)) {
            ALOGE("ToElResolveInfo() ToElProviderInfo fail!");
        }

        (*info)->SetProviderInfo(providerInfo);
        env->DeleteLocalRef(jproviderInfo);
    }

    f = env->GetFieldID(riKlass, "filter", "Landroid/content/IntentFilter;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: filter %d", __LINE__);

    jobject jfilter = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: filter %d", __LINE__);

    if (jfilter != NULL) {
        AutoPtr<IIntentFilter> filter;
        if (!ElUtil::ToElIntentFilter(env, jfilter, (IIntentFilter**)&filter)) {
            ALOGE("ToElResolveInfo() ToElIntentFilter fail!");
        }

        (*info)->SetFilter(filter);
        env->DeleteLocalRef(jfilter);
    }

    Int32 tempInt = ElUtil::GetJavaIntField(env, riKlass, jinfo, "priority", "ToElResolveInfo");
    (*info)->SetPriority(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, riKlass, jinfo, "preferredOrder", "ToElResolveInfo");
    (*info)->SetPreferredOrder(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, riKlass, jinfo, "match", "ToElResolveInfo");
    (*info)->SetMatch(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, riKlass, jinfo, "specificIndex", "ToElResolveInfo");
    (*info)->SetSpecificIndex(tempInt);

    Boolean tempBool = ElUtil::GetJavaBoolField(env, riKlass, jinfo, "isDefault", "ToElResolveInfo");
    (*info)->SetIsDefault(tempBool);

    tempInt = ElUtil::GetJavaIntField(env, riKlass, jinfo, "labelRes", "ToElResolveInfo");
    (*info)->SetLabelRes(tempInt);

    f = env->GetFieldID(riKlass, "nonLocalizedLabel", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "GetFieldID nonLocalizedLabel : %d!\n", __LINE__);

    jobject jnonLocalizedLabel = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField nonLocalizedDescription : %d!\n", __LINE__);
    if (jnonLocalizedLabel != NULL) {
        AutoPtr<ICharSequence> nonLocalizedLabel;
        if (!ElUtil::ToElCharSequence(env, jfilter, (ICharSequence**)&nonLocalizedLabel)) {
            ALOGE("ToElResolveInfo() ToElCharSequence fail!");
        }

        (*info)->SetNonLocalizedLabel(nonLocalizedLabel);
        env->DeleteLocalRef(jnonLocalizedLabel);
    }

    tempInt = ElUtil::GetJavaIntField(env, riKlass, jinfo, "icon", "ToElResolveInfo");
    (*info)->SetIcon(tempInt);

    String resolvePackageName = ElUtil::GetJavaStringField(env, riKlass, jinfo, "resolvePackageName", "ToElResolveInfo");
    (*info)->SetResolvePackageName(resolvePackageName);

    tempInt = ElUtil::GetJavaIntField(env, riKlass, jinfo, "targetUserId", "ToElResolveInfo");
    (*info)->SetTargetUserId(tempInt);

    tempBool = ElUtil::GetJavaBoolField(env, riKlass, jinfo, "system", "ToElResolveInfo");
    (*info)->SetSystem(tempBool);

    tempBool = ElUtil::GetJavaBoolField(env, riKlass, jinfo, "noResourceId", "ToElResolveInfo");
    (*info)->SetNoResourceId(tempBool);

    env->DeleteLocalRef(riKlass);
    return TRUE;
}

bool ElUtil::ToElInputMethodSubtype(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jsubtype,
    /* [out] */ IInputMethodSubtype** subtype)
{
    if (env == NULL || jsubtype == NULL || subtype == NULL) {
        ALOGE("ToElInputMethodSubtype: Invalid argumenet!");
        return FALSE;
    }

    jclass imsKlass = env->FindClass("android/view/inputmethod/InputMethodSubtype");
    CheckErrorAndLog(env, "FindClass: InputMethodSubtype : %d!\n", __LINE__);

    if (NOERROR != CInputMethodSubtype::New(subtype)) {
        ALOGE("ToElInputMethodSubtype() Create CInputMethodSubtype fail!");
        return FALSE;
    }

    AutoPtr<IParcel> parcel;
    Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);

    Int32 tempInt = ElUtil::GetJavaIntField(env, imsKlass, jsubtype, "mSubtypeNameResId", "ToElInputMethodSubtype");
    parcel->WriteInt32(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, imsKlass, jsubtype, "mSubtypeIconResId", "ToElInputMethodSubtype");
    parcel->WriteInt32(tempInt);

    String subtypeLocale = GetJavaStringField(env, imsKlass, jsubtype, "mSubtypeLocale", "ToElInputMethodSubtype");
    parcel->WriteString(subtypeLocale);

    String subtypeMode = GetJavaStringField(env, imsKlass, jsubtype, "mSubtypeMode", "ToElInputMethodSubtype");
    parcel->WriteString(subtypeMode);

    String subtypeExtraValue = GetJavaStringField(env, imsKlass, jsubtype, "mSubtypeExtraValue", "ToElInputMethodSubtype");
    parcel->WriteString(subtypeExtraValue);

    Boolean tempBool = ElUtil::GetJavaBoolField(env, imsKlass, jsubtype, "mIsAuxiliary", "ToElInputMethodSubtype");
    parcel->WriteBoolean(tempBool);

    tempBool = ElUtil::GetJavaBoolField(env, imsKlass, jsubtype, "mOverridesImplicitlyEnabledSubtype", "ToElInputMethodSubtype");
    parcel->WriteBoolean(tempBool);

    tempInt = ElUtil::GetJavaIntField(env, imsKlass, jsubtype, "mSubtypeHashCode", "ToElInputMethodSubtype");
    parcel->WriteInt32(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, imsKlass, jsubtype, "mSubtypeHashCode", "ToElInputMethodSubtype");
    parcel->WriteInt32(tempInt);

    tempBool = ElUtil::GetJavaBoolField(env, imsKlass, jsubtype, "mIsAsciiCapable", "ToElInputMethodSubtype");
    parcel->WriteBoolean(tempBool);

    parcel->SetDataPosition(0);
    IParcelable::Probe(*subtype)->ReadFromParcel(parcel);

    env->DeleteLocalRef(imsKlass);
    return TRUE;
}

bool ElUtil::ToElActivityInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IActivityInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElActivityInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass aiKlass = env->FindClass("android/content/pm/ActivityInfo");
    CheckErrorAndLog(env, "FindClass: ActivityInfo : %d!\n", __LINE__);

    if (NOERROR != CActivityInfo::New(info)) {
        ALOGE("ToElActivityInfo() Create CActivityInfo fail!");
        return FALSE;
    }

    AutoPtr<IComponentInfo> compInfo = IComponentInfo::Probe(*info);
    ElUtil::SetElComponentInfo(env, aiKlass, jinfo, compInfo);

    Int32 tempInt = ElUtil::GetJavaIntField(env, aiKlass, jinfo, "theme", "ToElActivityInfo");
    (*info)->SetTheme(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, aiKlass, jinfo, "launchMode", "ToElActivityInfo");
    (*info)->SetLaunchMode(tempInt);

    String permission = GetJavaStringField(env, aiKlass, jinfo, "permission", "ToElActivityInfo");
    (*info)->SetPermission(permission);

    String taskAffinity = GetJavaStringField(env, aiKlass, jinfo, "taskAffinity", "ToElActivityInfo");
    (*info)->SetTaskAffinity(taskAffinity);

    String targetActivity = GetJavaStringField(env, aiKlass, jinfo, "targetActivity", "ToElActivityInfo");
    (*info)->SetTargetActivity(targetActivity);

    tempInt = ElUtil::GetJavaIntField(env, aiKlass, jinfo, "flags", "ToElActivityInfo");
    (*info)->SetFlags(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, aiKlass, jinfo, "screenOrientation", "ToElActivityInfo");
    (*info)->SetScreenOrientation(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, aiKlass, jinfo, "configChanges", "ToElActivityInfo");
    (*info)->SetConfigChanges(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, aiKlass, jinfo, "softInputMode", "ToElActivityInfo");
    (*info)->SetSoftInputMode(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, aiKlass, jinfo, "uiOptions", "ToElActivityInfo");
    (*info)->SetUiOptions(tempInt);

    String parentActivityName = GetJavaStringField(env, aiKlass, jinfo, "parentActivityName", "ToElActivityInfo");
    (*info)->SetParentActivityName(parentActivityName);

    tempInt = ElUtil::GetJavaIntField(env, aiKlass, jinfo, "persistableMode", "ToElActivityInfo");
    (*info)->SetPersistableMode(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, aiKlass, jinfo, "maxRecents", "ToElActivityInfo");
    (*info)->SetMaxRecents(tempInt);

    env->DeleteLocalRef(aiKlass);
    return TRUE;
}

bool ElUtil::ToElServiceInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IServiceInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElServiceInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass siKlass = env->FindClass("android/content/pm/ServiceInfo");
    CheckErrorAndLog(env, "FindClass: ActivityInfo : %d!\n", __LINE__);

    if (NOERROR != CServiceInfo::New(info)) {
        ALOGE("ToElServiceInfo() Create CServiceInfo fail!");
        return FALSE;
    }

    AutoPtr<IComponentInfo> compInfo = IComponentInfo::Probe(*info);
    SetElComponentInfo(env, siKlass, jinfo, compInfo);

    String permission = GetJavaStringField(env, siKlass, jinfo, "permission", "ToElServiceInfo");
    (*info)->SetPermission(permission);

    Int32 flags = ElUtil::GetJavaIntField(env, siKlass, jinfo, "flags", "ToElServiceInfo");
    (*info)->SetFlags(flags);

    env->DeleteLocalRef(siKlass);
    return TRUE;
}

void ElUtil::SetElComponentInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jclass c,
    /* [in] */ jobject jinfo,
    /* [in] */ IComponentInfo* info)
{
    if (env == NULL || c == NULL  || jinfo == NULL || info == NULL) {
        ALOGE("SetElComponentInfo: Invalid argumenet!");
        return;
    }

    AutoPtr<IPackageItemInfo> pkgItemInfo = IPackageItemInfo::Probe(info);
    SetElPackageItemInfo(env, c, jinfo, pkgItemInfo);

    jfieldID f = env->GetFieldID(c, "applicationInfo", "Landroid/content/pm/ApplicationInfo;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: applicationInfo %d", __LINE__);

    jobject japplicationInfo = env->GetObjectField(jinfo, f);
    ElUtil::CheckErrorAndLog(env, "GetObjectField: applicationInfo %d", __LINE__);

    if (japplicationInfo != NULL) {
        AutoPtr<IApplicationInfo> applicationInfo;
        if (!ElUtil::ToElApplicationInfo(env, japplicationInfo, (IApplicationInfo**)&applicationInfo)) {
            ALOGE("SetElComponentInfo() ToElApplicationInfo fail!");
        }

        info->SetApplicationInfo(applicationInfo);
        env->DeleteLocalRef(japplicationInfo);
    }

    String processName = GetJavaStringField(env, c, jinfo, "processName", "SetElComponentInfo");
    info->SetProcessName(processName);

    Int32 descriptionRes = ElUtil::GetJavaIntField(env, c, jinfo, "descriptionRes", "SetElComponentInfo");
    info->SetDescriptionRes(descriptionRes);

    Boolean enabled = ElUtil::GetJavaBoolField(env, c, jinfo, "enabled", "SetElComponentInfo");
    info->SetEnabled(enabled);

    Boolean exported = ElUtil::GetJavaBoolField(env, c, jinfo, "exported", "SetElComponentInfo");
    info->SetExported(exported);
}

bool ElUtil::ToElSuggestionSpan(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jspan,
    /* [out] */ ISuggestionSpan** span)
{
    if (env == NULL || jspan == NULL || span == NULL) {
        ALOGE("ToElSuggestionSpan: Invalid argumenet!");
        return FALSE;
    }

    jclass ssKlass = env->FindClass("android/text/style/SuggestionSpan");
    CheckErrorAndLog(env, "FindClass: SuggestionSpan : %d!", __LINE__);

    jfieldID f = env->GetFieldID(ssKlass, "mSuggestions", "[Ljava/lang/String;");
    CheckErrorAndLog(env, "GetFieldID: mSuggestions : %d!", __LINE__);

    jobjectArray jsuggestions = (jobjectArray)env->GetObjectField(jspan, f);
    CheckErrorAndLog(env, "GetObjectField mSuggestions : %d!", __LINE__);

    if (NOERROR != CSuggestionSpan::New((ILocale*)NULL, NULL, 0, span)) {
        ALOGE("ToElSuggestionSpan() Create CSuggestionSpan fail!");
        return FALSE;
    }

    AutoPtr<IParcel> parcel;
    Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);

    AutoPtr<ArrayOf<String> > suggestions;
    if (jsuggestions != NULL) {
        if (!ElUtil::ToElStringArray(env, jsuggestions, (ArrayOf<String>**)&suggestions)) {
            ALOGE("ToElSuggestionSpan() ToElStringArray fail!");
        }
        env->DeleteLocalRef(jsuggestions);
    }
    parcel->WriteArrayOfString(suggestions);

    Int32 tempInt = ElUtil::GetJavaIntField(env, ssKlass, jspan, "mFlags", "ToElSuggestionSpan");
    parcel->WriteInt32(tempInt);

    String localeString = GetJavaStringField(env, ssKlass, jspan, "mLocaleString", "ToElSuggestionSpan");
    parcel->WriteString(localeString);

    String notificationTargetClassName = GetJavaStringField(env, ssKlass, jspan, "mNotificationTargetClassName", "ToElSuggestionSpan");
    parcel->WriteString(notificationTargetClassName);

    String notificationTargetPackageName = GetJavaStringField(env, ssKlass, jspan, "mNotificationTargetPackageName", "ToElSuggestionSpan");
    parcel->WriteString(notificationTargetPackageName);

    tempInt = ElUtil::GetJavaIntField(env, ssKlass, jspan, "mHashCode", "ToElSuggestionSpan");
    parcel->WriteInt32(tempInt);

    tempInt = ElUtil::GetJavaIntField(env, ssKlass, jspan, "mEasyCorrectUnderlineColor", "ToElSuggestionSpan");
    parcel->WriteInt32(tempInt);

    Float tempFloat = ElUtil::GetJavafloatField(env, ssKlass, jspan, "mEasyCorrectUnderlineThickness", "ToElSuggestionSpan");
    parcel->WriteFloat(tempFloat);

    tempInt = ElUtil::GetJavaIntField(env, ssKlass, jspan, "mMisspelledUnderlineColor", "ToElSuggestionSpan");
    parcel->WriteInt32(tempInt);

    tempFloat = ElUtil::GetJavafloatField(env, ssKlass, jspan, "mMisspelledUnderlineThickness", "ToElSuggestionSpan");
    parcel->WriteFloat(tempFloat);

    tempInt = ElUtil::GetJavaIntField(env, ssKlass, jspan, "mAutoCorrectionUnderlineColor", "ToElSuggestionSpan");
    parcel->WriteInt32(tempInt);

    tempFloat = ElUtil::GetJavafloatField(env, ssKlass, jspan, "mAutoCorrectionUnderlineThickness", "ToElSuggestionSpan");
    parcel->WriteFloat(tempFloat);

    parcel->SetDataPosition(0);
    IParcelable::Probe(*span)->ReadFromParcel(parcel);

    env->DeleteLocalRef(ssKlass);
    return TRUE;
}

jobject ElUtil::GetJavaKeyboardLayout(
    /* [in] */ JNIEnv* env,
    /* [in] */ IKeyboardLayout* kbdlayout)
{
    if (env == NULL || kbdlayout == NULL) {
        ALOGE("GetJavaKeyboardLayout: Invalid argumenet!");
        return NULL;
    }

    AutoPtr<IParcel> parcel;
    Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);
    IParcelable::Probe(kbdlayout)->WriteToParcel(parcel);
    parcel->SetDataPosition(0);

    String label, descriptor, collection;
    parcel->ReadString(&label);
    parcel->ReadString(&collection);
    parcel->ReadString(&descriptor);

    Int32 priority;
    parcel->ReadInt32(&priority);

    jstring jlabel = GetJavaString(env, label);
    jstring jdescriptor = GetJavaString(env, descriptor);
    jstring jcollection = GetJavaString(env, collection);

    jclass kbdCls = env->FindClass("android/hardware/input/KeyboardLayout");
    CheckErrorAndLog(env, "FindClass: KeyboardLayout : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(kbdCls, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetMethodID: KeyboardLayout : %d!\n", __LINE__);

    jobject jkbdlayout = env->NewObject(kbdCls, m, jdescriptor, jlabel, jcollection, priority);
    CheckErrorAndLog(env, "NewObject: KeyboardLayout : %d!\n", __LINE__);

    env->DeleteLocalRef(jlabel);
    env->DeleteLocalRef(jdescriptor);
    env->DeleteLocalRef(jcollection);
    env->DeleteLocalRef(kbdCls);

    return jkbdlayout;
}

jobject ElUtil::GetJavaUsbAccessory(
    /* [in] */ JNIEnv* env,
    /* [in] */ IUsbAccessory* accessory)
{
    if (env == NULL || accessory == NULL) {
        ALOGE("GetJavaUsbAccessory: Invalid argumenet!");
        return NULL;
    }

    jclass uaKlass = env->FindClass("android/hardware/usb/UsbAccessory");
    CheckErrorAndLog(env, "FindClass: UsbAccessory : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(uaKlass, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetMethodID: UsbAccessory : %d!\n", __LINE__);

    String manufacturer;
    accessory->GetManufacturer(&manufacturer);
    jstring jmanufacturer = ElUtil::GetJavaString(env, manufacturer);

    String model;
    accessory->GetModel(&model);
    jstring jmodel = ElUtil::GetJavaString(env, model);

    String description;
    accessory->GetDescription(&description);
    jstring jdescription = ElUtil::GetJavaString(env, description);

    String version;
    accessory->GetVersion(&version);
    jstring jversion = ElUtil::GetJavaString(env, version);

    String uri;
    accessory->GetUri(&uri);
    jstring juri = ElUtil::GetJavaString(env, uri);

    String serial;
    accessory->GetSerial(&serial);
    jstring jserial = ElUtil::GetJavaString(env, serial);

    jobject jaccessory = env->NewObject(uaKlass, m, jmanufacturer, jmodel, jdescription, jversion, juri, jserial);
    CheckErrorAndLog(env, "NewObject: UsbAccessory : %d!\n", __LINE__);

    env->DeleteLocalRef(uaKlass);
    env->DeleteLocalRef(jmanufacturer);
    env->DeleteLocalRef(jmodel);
    env->DeleteLocalRef(jdescription);
    env->DeleteLocalRef(jversion);
    env->DeleteLocalRef(juri);
    env->DeleteLocalRef(jserial);
    return jaccessory;
}

bool ElUtil::ToElUsbAccessory(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jaccessory,
    /* [out] */ IUsbAccessory** accessory)
{
    if (env == NULL || jaccessory == NULL || accessory == NULL) {
        ALOGE("ToElUsbAccessory: Invalid argumenet!");
        return FALSE;
    }

    jclass uaKlass = env->FindClass("android/hardware/usb/UsbAccessory");
    CheckErrorAndLog(env, "FindClass: UsbAccessory : %d!\n", __LINE__);

    String manufacturer = GetJavaStringField(env, uaKlass, jaccessory, "mManufacturer", "ToElUsbAccessory");
    String model = GetJavaStringField(env, uaKlass, jaccessory, "mModel", "ToElUsbAccessory");
    String description = GetJavaStringField(env, uaKlass, jaccessory, "mDescription", "ToElUsbAccessory");
    String version = GetJavaStringField(env, uaKlass, jaccessory, "mVersion", "ToElUsbAccessory");
    String uri = GetJavaStringField(env, uaKlass, jaccessory, "mUri", "ToElUsbAccessory");
    String serial = GetJavaStringField(env, uaKlass, jaccessory, "mSerial", "ToElUsbAccessory");

    if (NOERROR != CUsbAccessory::New(manufacturer, model, description, version, uri, serial, accessory)) {
        ALOGE("ToElUsbAccessory() Create CUsbAccessory fail!");
        return FALSE;
    }

    env->DeleteLocalRef(uaKlass);
    return FALSE;
}

bool ElUtil::ToElUsbDevice(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jdevice,
    /* [out] */ IUsbDevice** device)
{
    if (env == NULL || jdevice == NULL || device == NULL) {
        ALOGE("ToElUsbDevice: Invalid argumenet!");
        return FALSE;
    }

    jclass udKlass = env->FindClass("android/hardware/usb/UsbDevice");
    CheckErrorAndLog(env, "FindClass: UsbDevice : %d!\n", __LINE__);

    String name = GetJavaStringField(env, udKlass, jdevice, "mName", "ToElUsbDevice");
    Int32 vendorId = GetJavaIntField(env, udKlass, jdevice, "mVendorId", "ToElUsbDevice");
    Int32 productId = GetJavaIntField(env, udKlass, jdevice, "mProductId", "ToElUsbDevice");
    Int32 cls = GetJavaIntField(env, udKlass, jdevice, "mClass", "ToElUsbDevice");
    Int32 subclass = GetJavaIntField(env, udKlass, jdevice, "mSubclass", "ToElUsbDevice");
    Int32 protocol = GetJavaIntField(env, udKlass, jdevice, "mProtocol", "ToElUsbDevice");
    String manufacturerName = GetJavaStringField(env, udKlass, jdevice, "mManufacturerName", "ToElUsbDevice");
    String productName = GetJavaStringField(env, udKlass, jdevice, "mProductName", "ToElUsbDevice");
    String serialNumber = GetJavaStringField(env, udKlass, jdevice, "mSerialNumber", "ToElUsbDevice");

    jfieldID f = env->GetFieldID(udKlass, "mConfigurations", "[Landroid/os/Parcelable;");
    CheckErrorAndLog(env, "GetFieldID: mConfigurations : %d!\n", __LINE__);

    jobjectArray jconfigurations = (jobjectArray)env->GetObjectField(jdevice, f);
    CheckErrorAndLog(env, "GetObjectField mConfigurations : %d!\n", __LINE__);

    env->DeleteLocalRef(udKlass);

    if (NOERROR != CUsbDevice::New(name, vendorId, productId, cls, subclass, protocol,
        manufacturerName, productName, serialNumber, device)) {
        ALOGE("ToElUsbDevice() Create CUsbDevice fail!");
        return FALSE;
    }

    if (jconfigurations != NULL) {
        jint jcount = env->GetArrayLength(jconfigurations);
        if (jcount > 0) {
            AutoPtr<ArrayOf<IParcelable*> > configurations = ArrayOf<IParcelable*>::Alloc(jcount);
            for (Int32 i = 0; i < jcount; i++) {
                jobject jconfiguration = env->GetObjectArrayElement(jconfigurations, i);
                ElUtil::CheckErrorAndLog(env, "ToElUsbDevice GetObjectArrayElement: jconfiguration %d", __LINE__);
                AutoPtr<IUsbConfiguration> configuration;
                if (ElUtil::ToElUsbConfiguration(env, jconfiguration, (IUsbConfiguration**)&configuration))
                    configurations->Set(i, IParcelable::Probe(configuration));
                else
                    ALOGE("ToElUsbDevice: ToElUsbConfiguration fail!");
                env->DeleteLocalRef(jconfiguration);
            }
            (*device)->SetConfigurations(configurations);
        }
        env->DeleteLocalRef(jconfigurations);
    }
    return TRUE;
}

bool ElUtil::ToElUsbConfiguration(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconfiguration,
    /* [out] */ IUsbConfiguration** configuration)
{
    if (env == NULL || jconfiguration == NULL || configuration == NULL) {
        ALOGE("ToElUsbConfiguration: Invalid argumenet!");
        return FALSE;
    }

    jclass ucKlass = env->FindClass("android/hardware/usb/UsbConfiguration");
    CheckErrorAndLog(env, "FindClass: UsbConfiguration : %d!\n", __LINE__);

    Int32 id = GetJavaIntField(env, ucKlass, jconfiguration, "mId", "ToElUsbConfiguration");
    String name = GetJavaStringField(env, ucKlass, jconfiguration, "mName", "ToElUsbConfiguration");
    Int32 attributes = GetJavaIntField(env, ucKlass, jconfiguration, "mAttributes", "ToElUsbConfiguration");
    Int32 maxPower = GetJavaIntField(env, ucKlass, jconfiguration, "mMaxPower", "ToElUsbConfiguration");

    jfieldID f = env->GetFieldID(ucKlass, "mInterfaces", "[Landroid/os/Parcelable;");
    CheckErrorAndLog(env, "GetFieldID: mInterfaces : %d!\n", __LINE__);

    jobjectArray jinterfaces = (jobjectArray)env->GetObjectField(jconfiguration, f);
    CheckErrorAndLog(env, "GetObjectField mInterfaces : %d!\n", __LINE__);

    env->DeleteLocalRef(ucKlass);

    if (NOERROR != CUsbConfiguration::New(id, name, attributes, maxPower, configuration)) {
        ALOGE("ToElUsbConfiguration() Create CUsbConfiguration fail!");
        return FALSE;
    }

    if (jinterfaces != NULL) {
        jint jcount = env->GetArrayLength(jinterfaces);
        if (jcount > 0) {
            AutoPtr<ArrayOf<IParcelable*> > interfaces = ArrayOf<IParcelable*>::Alloc(jcount);
            for (Int32 i = 0; i < jcount; i++) {
                jobject jinterface = env->GetObjectArrayElement(jinterfaces, i);
                ElUtil::CheckErrorAndLog(env, "ToElUsbConfiguration GetObjectArrayElement: jinterface %d", __LINE__);
                AutoPtr<IUsbInterface> uinterface;
                if (ElUtil::ToElUsbInterface(env, jinterface, (IUsbInterface**)&uinterface))
                    interfaces->Set(i, IParcelable::Probe(uinterface));
                else
                    ALOGE("ToElUsbConfiguration: ToElUsbInterface fail!");
                env->DeleteLocalRef(jinterface);
            }
            (*configuration)->SetInterfaces(interfaces);
        }
        env->DeleteLocalRef(jinterfaces);
    }

    return TRUE;
}

bool ElUtil::ToElUsbInterface(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jusbInterface,
    /* [out] */ IUsbInterface** usbInterface)
{
    if (env == NULL || jusbInterface == NULL || usbInterface == NULL) {
        ALOGE("ToElUsbInterface: Invalid argumenet!");
        return FALSE;
    }

    jclass uiKlass = env->FindClass("android/hardware/usb/UsbInterface");
    CheckErrorAndLog(env, "FindClass: UsbInterface : %d!\n", __LINE__);

    Int32 id = GetJavaIntField(env, uiKlass, jusbInterface, "mId", "ToElUsbInterface");
    Int32 alternateSetting = GetJavaIntField(env, uiKlass, jusbInterface, "mAlternateSetting", "ToElUsbInterface");
    String name = GetJavaStringField(env, uiKlass, jusbInterface, "mName", "ToElUsbInterface");
    Int32 cls = GetJavaIntField(env, uiKlass, jusbInterface, "mClass", "ToElUsbInterface");
    Int32 subclass = GetJavaIntField(env, uiKlass, jusbInterface, "mSubclass", "ToElUsbInterface");
    Int32 protocol = GetJavaIntField(env, uiKlass, jusbInterface, "mProtocol", "ToElUsbInterface");

    jfieldID f = env->GetFieldID(uiKlass, "mEndpoints", "[Landroid/os/Parcelable;");
    CheckErrorAndLog(env, "GetFieldID: mEndpoints : %d!\n", __LINE__);

    jobjectArray jendpoints = (jobjectArray)env->GetObjectField(jusbInterface, f);
    CheckErrorAndLog(env, "GetObjectField mEndpoints : %d!\n", __LINE__);

    env->DeleteLocalRef(uiKlass);

    if (NOERROR != CUsbInterface::New(id, alternateSetting, name, cls, subclass, protocol, usbInterface)) {
        ALOGE("ToElUsbInterface() Create CUsbInterface fail!");
        return FALSE;
    }

    if (jendpoints != NULL) {
        jint jcount = env->GetArrayLength(jendpoints);
        if (jcount > 0) {
            AutoPtr<ArrayOf<IParcelable*> > endpoints = ArrayOf<IParcelable*>::Alloc(jcount);
            for (Int32 i = 0; i < jcount; i++) {
                jobject jendpoint = env->GetObjectArrayElement(jendpoints, i);
                ElUtil::CheckErrorAndLog(env, "ToElUsbInterface GetObjectArrayElement: jendpoint %d", __LINE__);
                AutoPtr<IUsbEndpoint> endpoint;
                if (ElUtil::ToElUsbEndpoint(env, jendpoint, (IUsbEndpoint**)&endpoint))
                    endpoints->Set(i, IParcelable::Probe(endpoint));
                else
                    ALOGE("ToElUsbInterface: ToElUsbEndpoint fail!");
                env->DeleteLocalRef(jendpoint);
            }
            (*usbInterface)->SetEndpoints(endpoints);
        }
        env->DeleteLocalRef(jendpoints);
    }
    return TRUE;
}

bool ElUtil::ToElUsbEndpoint(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jendpoint,
    /* [out] */ IUsbEndpoint** endpoint)
{
    if (env == NULL || jendpoint == NULL || endpoint == NULL) {
        ALOGE("ToElUsbEndpoint: Invalid argumenet!");
        return FALSE;
    }

    jclass uiKlass = env->FindClass("android/hardware/usb/UsbEndpoint");
    CheckErrorAndLog(env, "FindClass: UsbEndpoint : %d!\n", __LINE__);

    Int32 address = GetJavaIntField(env, uiKlass, jendpoint, "mAddress", "ToElUsbEndpoint");;
    Int32 attributes = GetJavaIntField(env, uiKlass, jendpoint, "mAttributes", "ToElUsbEndpoint");;
    Int32 maxPacketSize = GetJavaIntField(env, uiKlass, jendpoint, "mMaxPacketSize", "ToElUsbEndpoint");;
    Int32 interval = GetJavaIntField(env, uiKlass, jendpoint, "mInterval", "ToElUsbEndpoint");;

    env->DeleteLocalRef(uiKlass);

    if (NOERROR != CUsbEndpoint::New(address, attributes, maxPacketSize, interval, endpoint)) {
        ALOGE("ToElUsbEndpoint() Create CUsbEndpoint fail!");
        return FALSE;
    }

    return TRUE;
}

jobject ElUtil::GetJavaNetworkState(
    /* [in] */ JNIEnv* env,
    /* [in] */ INetworkState* state)
{
    if (env == NULL || state == NULL) {
        ALOGE("GetJavaNetworkState() Invalid argumenet!");
        return NULL;
    }

    jclass nsKlass = env->FindClass("android/net/NetworkState");
    CheckErrorAndLog(env, "FindClass: NetworkState : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(nsKlass, "<init>", "(Landroid/net/NetworkInfo;Landroid/net/LinkProperties;Landroid/net/NetworkCapabilities;Ljava/lang/String;Ljava/lang/String;)V");
    CheckErrorAndLog(env, "GetMethodID: NetworkState : %d!\n", __LINE__);

    AutoPtr<INetworkInfo> networkInfo;
    state->GetNetworkInfo((INetworkInfo**)&networkInfo);
    jobject jnetworkInfo = NULL;
    if (networkInfo != NULL) {
        jnetworkInfo = ElUtil::GetJavaNetworkInfo(env, networkInfo);
    }

    AutoPtr<ILinkProperties> linkProperties;
    state->GetLinkProperties((ILinkProperties**)&linkProperties);
    jobject jlinkProperties = NULL;
    if (linkProperties != NULL) {
        jlinkProperties = ElUtil::GetJavaLinkProperties(env, linkProperties);
    }

    AutoPtr<INetworkCapabilities> networkCapabilities;
    state->GetNetworkCapabilities((INetworkCapabilities**)&networkCapabilities);
    jobject jnetworkCapabilities = NULL;
    if (networkCapabilities != NULL) {
        jnetworkCapabilities = ElUtil::GetJavaNetworkCapabilities(env, networkCapabilities);
    }

    String subscriberId;
    state->GetSubscriberId(&subscriberId);
    jstring jsubscriberId = ElUtil::GetJavaString(env, subscriberId);

    String networkId;
    state->GetNetworkId(&networkId);
    jstring jnetworkId = ElUtil::GetJavaString(env, networkId);

    jobject jstate = env->NewObject(nsKlass, m, jnetworkInfo, jlinkProperties, jnetworkCapabilities, jsubscriberId, jnetworkId);
    CheckErrorAndLog(env, "NewObject: NetworkState : %d!\n", __LINE__);

    env->DeleteLocalRef(nsKlass);
    env->DeleteLocalRef(jnetworkInfo);
    env->DeleteLocalRef(jlinkProperties);
    env->DeleteLocalRef(jnetworkCapabilities);
    env->DeleteLocalRef(jsubscriberId);
    env->DeleteLocalRef(jnetworkId);
    return jstate;
}

jobject ElUtil::GetJavaNetworkQuotaInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ INetworkQuotaInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaNetworkQuotaInfo() Invalid argumenet!");
        return NULL;
    }

    jclass nqiKlass = env->FindClass("android/net/NetworkQuotaInfo");
    CheckErrorAndLog(env, "FindClass: NetworkQuotaInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(nqiKlass, "<init>", "(JJJ)V");
    CheckErrorAndLog(env, "GetMethodID: NetworkQuotaInfo : %d!\n", __LINE__);

    Int64 estimatedBytes;
    info->GetEstimatedBytes(&estimatedBytes);
    Int64 softLimitBytes;
    info->GetSoftLimitBytes(&softLimitBytes);
    Int64 hardLimitBytes;
    info->GetHardLimitBytes(&hardLimitBytes);

    jobject jinfo = env->NewObject(nqiKlass, m, (jlong)estimatedBytes, (jlong)softLimitBytes, (jlong)hardLimitBytes);
    CheckErrorAndLog(env, "NewObject: NetworkQuotaInfo : %d!\n", __LINE__);

    env->DeleteLocalRef(nqiKlass);
    return jinfo;
}

jobject ElUtil::GetJavaNetworkCapabilities(
    /* [in] */ JNIEnv* env,
    /* [in] */ INetworkCapabilities* capabilities)
{
    if (env == NULL || capabilities == NULL) {
        ALOGE("GetJavaNetworkCapabilities() Invalid argumenet!");
        return NULL;
    }

    jclass lcKlass = env->FindClass("android/net/NetworkCapabilities");
    CheckErrorAndLog(env, "FindClass: NetworkCapabilities : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(lcKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: NetworkCapabilities : %d!\n", __LINE__);

    jobject jcapabilities = env->NewObject(lcKlass, m);
    CheckErrorAndLog(env, "NewObject: NetworkCapabilities : %d!\n", __LINE__);

    AutoPtr<IParcel> parcel;
    Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);
    IParcelable::Probe(capabilities)->WriteToParcel(parcel);
    parcel->SetDataPosition(0);

    Int64 transportTypes;
    parcel->ReadInt64(&transportTypes);
    SetJavalongField(env, lcKlass, jcapabilities, transportTypes, "mTransportTypes", "GetJavaNetworkCapabilities");

    Int32 linkUpBandwidthKbps;
    parcel->ReadInt32(&linkUpBandwidthKbps);
    SetJavaIntField(env, lcKlass, jcapabilities, linkUpBandwidthKbps, "mLinkUpBandwidthKbps", "GetJavaNetworkCapabilities");

    Int32 linkDownBandwidthKbps;
    parcel->ReadInt32(&linkDownBandwidthKbps);
    SetJavaIntField(env, lcKlass, jcapabilities, linkDownBandwidthKbps, "mLinkDownBandwidthKbps", "GetJavaNetworkCapabilities");

    String networkSpecifier;
    parcel->ReadString(&networkSpecifier);
    SetJavaStringField(env, lcKlass, jcapabilities, networkSpecifier, "mNetworkSpecifier", "GetJavaNetworkCapabilities");

    Int64 networkCapabilities;
    parcel->ReadInt64(&networkCapabilities);
    SetJavalongField(env, lcKlass, jcapabilities, networkCapabilities, "mNetworkCapabilities", "GetJavaNetworkCapabilities");

    env->DeleteLocalRef(lcKlass);
    return jcapabilities;
}

bool ElUtil::ToElVpnConfig(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconfig,
    /* [out] */ IVpnConfig** config)
{
    if (env == NULL || jconfig == NULL || config == NULL) {
        ALOGE("ToElVpnConfig: Invalid argumenet!");
        return FALSE;
    }

    jclass vcKlass = env->FindClass("com/android/internal/net/VpnConfig");
    CheckErrorAndLog(env, "FindClass: VpnConfig : %d!\n", __LINE__);

    if (NOERROR != CVpnConfig::New(config)) {
        ALOGE("ToElVpnConfig() Create CVpnConfig fail!");
        return FALSE;
    }

    String user = GetJavaStringField(env, vcKlass, jconfig, "user", "ToElVpnConfig");
    (*config)->SetUser(user);

    String interfaze = GetJavaStringField(env, vcKlass, jconfig, "interfaze", "ToElVpnConfig");
    (*config)->SetInterfaze(interfaze);

    String session = GetJavaStringField(env, vcKlass, jconfig, "session", "ToElVpnConfig");
    (*config)->SetSession(session);

    Int32 mtu = GetJavaIntField(env, vcKlass, jconfig, "mtu", "ToElVpnConfig");
    (*config)->SetMtu(mtu);

    jclass listKlass = env->FindClass("java/util/List");
    ElUtil::CheckErrorAndLog(env, "FindClass: List : %d!\n", __LINE__);

    jmethodID mSize = env->GetMethodID(listKlass, "size","()I");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: size : %d!\n", __LINE__);

    jmethodID mGet = env->GetMethodID(listKlass, "get","(I)Ljava/lang/Object;");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(vcKlass, "addresses", "Ljava/util/List;");
    CheckErrorAndLog(env, "GetFieldID: addresses : %d!\n", __LINE__);

    jobject jaddresses = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField addresses : %d!\n", __LINE__);

    if (jaddresses != NULL) {
        AutoPtr<IList> addresses;
        CParcelableList::New((IList**)&addresses);

        jint size = env->CallIntMethod(jaddresses, mSize);
        ElUtil::CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

        for (jint i = 0; i < size; i++) {
            jstring jaddress = (jstring)env->CallObjectMethod(jaddresses, mGet, i);
            ElUtil::CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

            AutoPtr<ILinkAddress> address;
            if (ElUtil::ToElLinkAddress(env, jaddress, (ILinkAddress**)&address))
                addresses->Add(address);
            else
                ALOGE("ToElVpnConfig: ToElLinkAddress fail!");

            env->DeleteLocalRef(jaddress);
        }

        (*config)->SetAddresses(addresses);

        env->DeleteLocalRef(jaddresses);
    }

    f = env->GetFieldID(vcKlass, "routes", "Ljava/util/List;");
    CheckErrorAndLog(env, "GetFieldID: routes : %d!\n", __LINE__);

    jobject jroutes = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField routes : %d!\n", __LINE__);

    if (jroutes != NULL) {
        AutoPtr<IList> routes;
        CParcelableList::New((IList**)&routes);

        jint size = env->CallIntMethod(jroutes, mSize);
        ElUtil::CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

        for (jint i = 0; i < size; i++) {
            jstring jroute = (jstring)env->CallObjectMethod(jroutes, mGet, i);
            ElUtil::CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

            AutoPtr<IRouteInfo> route;
            if (ElUtil::ToElRouteInfo(env, jroute, (IRouteInfo**)&route))
                routes->Add(route);
            else
                ALOGE("ToElVpnConfig: ToElRouteInfo fail!");

            env->DeleteLocalRef(jroute);
        }

        (*config)->SetRoutes(routes);

        env->DeleteLocalRef(jroutes);
    }

    f = env->GetFieldID(vcKlass, "dnsServers", "Ljava/util/List;");
    CheckErrorAndLog(env, "GetFieldID: dnsServers : %d!\n", __LINE__);

    jobject jdnsServers = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField dnsServers : %d!\n", __LINE__);
    if (jdnsServers != NULL) {
        AutoPtr<IList> dnsServers;
        CParcelableList::New((IList**)&dnsServers);

        jint size = env->CallIntMethod(jdnsServers, mSize);
        ElUtil::CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

        for (jint i = 0; i < size; i++) {
            jstring jdnsServer = (jstring)env->CallObjectMethod(jdnsServers, mGet, i);
            ElUtil::CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

            String dnsServer = ElUtil::ToElString(env, jdnsServer);
            AutoPtr<ICharSequence> csdnsServer;
            CString::New(dnsServer, (ICharSequence**)&csdnsServer);
            dnsServers->Add(csdnsServer);

            env->DeleteLocalRef(jdnsServer);
        }

        (*config)->SetDnsServers(dnsServers);

        env->DeleteLocalRef(jdnsServers);
    }

    f = env->GetFieldID(vcKlass, "searchDomains", "Ljava/util/List;");
    CheckErrorAndLog(env, "GetFieldID: searchDomains : %d!\n", __LINE__);

    jobject jsearchDomains = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField searchDomains : %d!\n", __LINE__);
    if (jsearchDomains != NULL) {
        AutoPtr<IList> searchDomains;
        CParcelableList::New((IList**)&searchDomains);

        jint size = env->CallIntMethod(jsearchDomains, mSize);
        ElUtil::CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

        for (jint i = 0; i < size; i++) {
            jstring jsearchDomain = (jstring)env->CallObjectMethod(jsearchDomains, mGet, i);
            ElUtil::CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

            String searchDomain = ElUtil::ToElString(env, jsearchDomain);
            AutoPtr<ICharSequence> cssearchDomain;
            CString::New(searchDomain, (ICharSequence**)&cssearchDomain);
            searchDomains->Add(cssearchDomain);

            env->DeleteLocalRef(jsearchDomain);
        }

        (*config)->SetSearchDomains(searchDomains);

        env->DeleteLocalRef(jsearchDomains);
    }

    f = env->GetFieldID(vcKlass, "allowedApplications", "Ljava/util/List;");
    CheckErrorAndLog(env, "GetFieldID: allowedApplications : %d!\n", __LINE__);

    jobject jallowedApplications = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField allowedApplications : %d!\n", __LINE__);
    if (jallowedApplications != NULL) {
        AutoPtr<IList> allowedApplications;
        CParcelableList::New((IList**)&allowedApplications);

        jint size = env->CallIntMethod(jallowedApplications, mSize);
        ElUtil::CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

        for (jint i = 0; i < size; i++) {
            jstring jallowedApplication = (jstring)env->CallObjectMethod(jallowedApplications, mGet, i);
            ElUtil::CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

            String allowedApplication = ElUtil::ToElString(env, jallowedApplication);
            AutoPtr<ICharSequence> csallowedApplication;
            CString::New(allowedApplication, (ICharSequence**)&csallowedApplication);
            allowedApplications->Add(csallowedApplication);

            env->DeleteLocalRef(jallowedApplication);
        }

        (*config)->SetAllowedApplications(allowedApplications);

        env->DeleteLocalRef(jallowedApplications);
    }

    f = env->GetFieldID(vcKlass, "disallowedApplications", "Ljava/util/List;");
    CheckErrorAndLog(env, "GetFieldID: disallowedApplications : %d!\n", __LINE__);

    jobject jdisallowedApplications = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField disallowedApplications : %d!\n", __LINE__);
    if (jdisallowedApplications != NULL) {
        AutoPtr<IList> disallowedApplications;
        CParcelableList::New((IList**)&disallowedApplications);

        jint size = env->CallIntMethod(jdisallowedApplications, mSize);
        ElUtil::CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

        for (jint i = 0; i < size; i++) {
            jstring jdisallowedApplication = (jstring)env->CallObjectMethod(jdisallowedApplications, mGet, i);
            ElUtil::CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

            String disallowedApplication = ElUtil::ToElString(env, jdisallowedApplication);
            AutoPtr<ICharSequence> csdisallowedApplication;
            CString::New(disallowedApplication, (ICharSequence**)&csdisallowedApplication);
            disallowedApplications->Add(csdisallowedApplication);

            env->DeleteLocalRef(jdisallowedApplication);
        }

        (*config)->SetDisallowedApplications(disallowedApplications);

        env->DeleteLocalRef(jdisallowedApplications);
    }

    f = env->GetFieldID(vcKlass, "configureIntent", "Landroid/app/PendingIntent;");
    CheckErrorAndLog(env, "GetFieldID configureIntent : %d!", __LINE__);

    jobject jconfigureIntent = env->GetObjectField(jconfig, f);
    CheckErrorAndLog(env, "GetObjectField configureIntent : %d!\n", __LINE__);
    if (jconfigureIntent != NULL) {
        AutoPtr<IPendingIntent> configureIntent;
        if (ElUtil::ToElPendingIntent(env, jconfigureIntent, (IPendingIntent**)&configureIntent)) {
            (*config)->SetConfigureIntent(configureIntent);
        }
        else {
            ALOGE("ToElVpnConfig() ToElPendingIntent fail!");
        }
    }

    Int64 startTime = ElUtil::GetJavalongField(env, vcKlass, jconfig, "startTime", "ToElVpnConfig");
    (*config)->SetStartTime(startTime);

    Boolean legacy = ElUtil::GetJavaBoolField(env, vcKlass, jconfig, "legacy", "ToElVpnConfig");
    (*config)->SetLegacy(legacy);

    Boolean blocking = ElUtil::GetJavaBoolField(env, vcKlass, jconfig, "blocking", "ToElVpnConfig");
    (*config)->SetBlocking(blocking);

    Boolean allowBypass = ElUtil::GetJavaBoolField(env, vcKlass, jconfig, "allowBypass", "ToElVpnConfig");
    (*config)->SetAllowBypass(allowBypass);

    Boolean allowIPv4 = ElUtil::GetJavaBoolField(env, vcKlass, jconfig, "allowIPv4", "ToElVpnConfig");
    (*config)->SetAllowIPv4(allowIPv4);

    Boolean allowIPv6 = ElUtil::GetJavaBoolField(env, vcKlass, jconfig, "allowIPv6", "ToElVpnConfig");
    (*config)->SetAllowIPv6(allowIPv6);

    env->DeleteLocalRef(vcKlass);
    env->DeleteLocalRef(listKlass);

    return FALSE;
}

jobject  ElUtil::GetJavaLegacyVpnInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ ILegacyVpnInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaLegacyVpnInfo() Invalid argumenet!");
        return NULL;
    }

    jclass lviKlass = env->FindClass("com/android/internal/net/LegacyVpnInfo");
    CheckErrorAndLog(env, "FindClass: LegacyVpnInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(lviKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: LegacyVpnInfo : %d!\n", __LINE__);

    jobject jinfo = env->NewObject(lviKlass, m);
    CheckErrorAndLog(env, "NewObject: LegacyVpnInfo : %d!\n", __LINE__);

    String key;
    info->GetKey(&key);
    ElUtil::SetJavaStringField(env, lviKlass, jinfo, key, "key", "GetJavaLegacyVpnInfo");

    Int32 state = -1;
    info->GetState(&state);
    ElUtil::SetJavaIntField(env, lviKlass, jinfo, state, "state", "GetJavaLegacyVpnInfo");

    AutoPtr<IPendingIntent> intent;
    info->GetIntent((IPendingIntent**)&intent);
    if (intent != NULL) {
        jobject jintent = GetJavaPendingIntent(env, intent);

        jfieldID f = env->GetFieldID(lviKlass, "intent", "Landroid/app/PendingIntent;");
        CheckErrorAndLog(env, "GetFieldID intent : %d!", __LINE__);

        env->SetObjectField(jinfo, f, jintent);
        CheckErrorAndLog(env, "SetObjectField intent : %d!", __LINE__);

        env->DeleteLocalRef(jintent);
    }

    env->DeleteLocalRef(lviKlass);
    return jinfo;
}

jobject ElUtil::GetJavaLinkAddress(
    /* [in] */ JNIEnv* env,
    /* [in] */ ILinkAddress* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaLinkAddress() Invalid argumenet!");
        return NULL;
    }

    jclass laKlass = env->FindClass("android/net/LinkAddress");
    CheckErrorAndLog(env, "FindClass: LinkAddress : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(laKlass, "<init>", "(Ljava/net/InetAddress;III)V");
    CheckErrorAndLog(env, "GetMethodID: LinkAddress : %d!\n", __LINE__);

    AutoPtr<IInetAddress> address;
    info->GetAddress((IInetAddress**)&address);
    jobject jaddress = NULL;
    if (address != NULL) {
        jaddress = ElUtil::GetJavaInetAddress(env, address);
    }

    Int32 prefixLength = 0;
    info->GetNetworkPrefixLength(&prefixLength);

    Int32 flags;
    info->GetFlags(&flags);

    Int32 scope;
    info->GetScope(&scope);

    jobject jinfo = env->NewObject(laKlass, m, jaddress, (jint)prefixLength, flags, scope);
    CheckErrorAndLog(env, "NewObject: LinkAddress : %d!\n", __LINE__);

    env->DeleteLocalRef(laKlass);
    return jinfo;
}

jobject ElUtil::GetJavaVerifierInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IVerifierInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaVerifierInfo() Invalid argumenet!");
        return NULL;
    }

    jclass viKlass = env->FindClass("android/content/pm/VerifierInfo");
    CheckErrorAndLog(env, "FindClass: VerifierInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(viKlass, "<init>", "(Ljava/lang/String;Ljava/security/PublicKey;)V");
    CheckErrorAndLog(env, "GetMethodID: VerifierInfo : %d!\n", __LINE__);

    String packageName;
    info->GetPackageName(&packageName);
    jstring jpackageName = ElUtil::GetJavaString(env, packageName);

    AutoPtr<Elastos::Security::IPublicKey> publicKey;
    info->GetPublicKey((IPublicKey**)&publicKey);
    jobject jpublicKey = NULL;
    if (publicKey != NULL) {
        jpublicKey = ElUtil::GetJavaPublicKey(env, publicKey);
    }

    jobject jinfo = env->NewObject(viKlass, m, jpackageName, jpublicKey);
    CheckErrorAndLog(env, "NewObject: VerifierInfo : %d!\n", __LINE__);

    env->DeleteLocalRef(viKlass);
    return jinfo;
}

jobject ElUtil::GetJavaPublicKey(
    /* [in] */ JNIEnv* env,
    /* [in] */ IPublicKey* pkey)
{
    if (env == NULL || pkey == NULL) {
        ALOGE("GetJavaPublicKey() Invalid argumenet!");
        return NULL;
    }

    ALOGE("GetJavaPublicKey() E_NOT_IMPLEMENTED");
    return NULL;
}

bool ElUtil::ToElRouteInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IRouteInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElRouteInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass riKlass = env->FindClass("android/net/RouteInfo");
    CheckErrorAndLog(env, "FindClass: RouteInfo : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(riKlass, "mDestination", "Landroid/net/IpPrefix;");
    CheckErrorAndLog(env, "GetFieldID IpPrefix : %d!", __LINE__);

    jobject jdestination = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField IpPrefix : %d!\n", __LINE__);
    AutoPtr<IIpPrefix> destination;
    if (jdestination != NULL) {
        if (!ElUtil::ToElIpPrefix(env, jdestination, (IIpPrefix**)&destination)) {
            ALOGE("ToElRouteInfo() ToElIpPrefix fail!");
        }
        env->DeleteLocalRef(jdestination);
    }

    f = env->GetFieldID(riKlass, "mGateway", "Ljava/net/InetAddress;");
    CheckErrorAndLog(env, "GetFieldID InetAddress : %d!", __LINE__);

    jobject jgateway = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField InetAddress : %d!\n", __LINE__);
    AutoPtr<IInetAddress> gateway;
    if (jgateway != NULL) {
        if (!ElUtil::ToElInetAddress(env, jgateway, (IInetAddress**)&gateway)) {
            ALOGE("ToElRouteInfo() ToElInetAddress fail!");
        }
        env->DeleteLocalRef(jgateway);
    }

    String strInterface = GetJavaStringField(env, riKlass, jinfo, "mInterface", "ToElRouteInfo");

    Int32 type = GetJavaIntField(env, riKlass, jinfo, "mType", "ToElRouteInfo");

    env->DeleteLocalRef(riKlass);

    if (NOERROR != CRouteInfo::New(destination, gateway, strInterface, type, info)) {
        ALOGE("ToElRouteInfo() Create CRouteInfo fail!");
        return FALSE;
    }

    return TRUE;
}

bool ElUtil::ToElIpPrefix(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jipPrefix,
    /* [out] */ IIpPrefix** ipPrefix)
{
    if (env == NULL || jipPrefix == NULL || ipPrefix == NULL) {
        ALOGE("ToElIpPrefix: Invalid argumenet!");
        return FALSE;
    }

    jclass ipKlass = env->FindClass("android/net/IpPrefix");
    CheckErrorAndLog(env, "FindClass: IpPrefix : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(ipKlass, "address", "[B");
    CheckErrorAndLog(env, "GetFieldID IpPrefix : %d!", __LINE__);

    jbyteArray jaddress = (jbyteArray)env->GetObjectField(jipPrefix, f);
    CheckErrorAndLog(env, "GetObjectField IpPrefix : %d!\n", __LINE__);
    AutoPtr<ArrayOf<Byte> > address;
    if (jaddress != NULL) {
        if (!ElUtil::ToElByteArray(env, jaddress, (ArrayOf<Byte>**)&address)) {
            ALOGE("ToElIpPrefix() ToElByteArray fail!");
        }
        env->DeleteLocalRef(jaddress);
    }

    Int32 prefixLength = GetJavaIntField(env, ipKlass, jipPrefix, "prefixLength", "ToElIpPrefix");

    env->DeleteLocalRef(ipKlass);

    if (NOERROR != CIpPrefix::New(address, prefixLength, ipPrefix)) {
        ALOGE("ToElIpPrefix() Create CIpPrefix fail!");
        return FALSE;
    }

    return TRUE;
}

bool ElUtil::ToElContentProviderHolder(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jholder,
    /* [out] */ IContentProviderHolder** holder)
{
    if (env == NULL || jholder == NULL || holder == NULL) {
        ALOGE("ToElContentProviderHolder: Invalid argumenet!");
        return FALSE;
    }

    jclass cphKlass = env->FindClass("android/app/IActivityManager$ContentProviderHolder");
    CheckErrorAndLog(env, "FindClass: ContentProviderHolder : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(cphKlass, "info", "Landroid/content/pm/ProviderInfo;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: ProviderInfo %d", __LINE__);

    jobject jinfo = env->GetObjectField(jholder, f);
    CheckErrorAndLog(env, "GetObjectField ProviderInfo : %d!\n", __LINE__);

    AutoPtr<IProviderInfo> info;
    if (jinfo != NULL) {
        if (!ElUtil::ToElProviderInfo(env, jinfo, (IProviderInfo**)&info)) {
            ALOGE("ToElContentProviderHolder() ToElProviderInfo fail!");
        }
    }

    if (NOERROR != CContentProviderHolder::New(info, holder)) {
        ALOGE("ToElContentProviderHolder() Create CContentProviderHolder fail!");
        return FALSE;
    }

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);

    f = env->GetFieldID(cphKlass, "provider", "Landroid/content/IContentProvider;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: IContentProvider %d", __LINE__);

    jobject jprovider = env->GetObjectField(jholder, f);
    CheckErrorAndLog(env, "GetObjectField ProviderInfo : %d!", __LINE__);

    if (jprovider != NULL) {
        jobject jInstance = env->NewGlobalRef(jprovider);

        AutoPtr<IIContentProvider> provider;
        if (NOERROR == CContentProviderNative::New((Handle64)jvm, (Handle64)jInstance, (IIContentProvider**)&provider)) {
            (*holder)->SetContentProvider(provider);
        }
        else {
            ALOGE("ToElContentProviderHolder new CContentProviderNative fail!");
        }

        env->DeleteLocalRef(jprovider);
    }

    f = env->GetFieldID(cphKlass, "connection", "Landroid/os/IBinder;");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: IContentProvider %d", __LINE__);

    jobject jconnection = env->GetObjectField(jholder, f);
    CheckErrorAndLog(env, "GetObjectField ProviderInfo : %d!", __LINE__);

    if (jconnection != NULL) {
        jobject jInstance = env->NewGlobalRef(jconnection);

        AutoPtr<Elastos::Droid::Os::IBinder> connection;
        if (NOERROR == CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (Elastos::Droid::Os::IBinder**)&connection)) {
            (*holder)->SetConnection(connection);
        }
        else {
            ALOGE("ToElContentProviderHolder new CBinderNative fail!");
        }

        env->DeleteLocalRef(jconnection);
    }

    Boolean noReleaseNeeded = ElUtil::GetJavaBoolField(env, cphKlass, jholder, "noReleaseNeeded", "ToElContentProviderHolder");
     (*holder)->SetNoReleaseNeeded(noReleaseNeeded);

    env->DeleteLocalRef(cphKlass);
    return TRUE;
}

bool ElUtil::ToElProviderInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IProviderInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElProviderInfo: Invalid argumenet!");
        return FALSE;
    }

    jclass piKlass = env->FindClass("android/content/pm/ProviderInfo");
    CheckErrorAndLog(env, "FindClass: ProviderInfo : %d!\n", __LINE__);

    if (NOERROR != CProviderInfo::New(info)) {
        ALOGE("ToElProviderInfo() Create CProviderInfo fail!");
        return FALSE;
    }

    AutoPtr<IComponentInfo> compInfo = IComponentInfo::Probe(*info);
    ElUtil::SetElComponentInfo(env, piKlass, jinfo, compInfo);

    String authority = GetJavaStringField(env, piKlass, jinfo, "authority", "ToElProviderInfo");
    (*info)->SetAuthority(authority);

    String readPermission = GetJavaStringField(env, piKlass, jinfo, "readPermission", "ToElProviderInfo");
    (*info)->SetReadPermission(readPermission);

    String writePermission = GetJavaStringField(env, piKlass, jinfo, "writePermission", "ToElProviderInfo");
    (*info)->SetWritePermission(writePermission);

    Boolean tempBool = ElUtil::GetJavaBoolField(env, piKlass, jinfo, "grantUriPermissions", "ToElProviderInfo");
    (*info)->SetGrantUriPermissions(tempBool);

    jfieldID  f = env->GetFieldID(piKlass, "uriPermissionPatterns", "[Landroid/os/PatternMatcher;");
    CheckErrorAndLog(env, "GetFieldID: PatternMatcher : %d!", __LINE__);

    jobjectArray juriPermissionPatterns = (jobjectArray)env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField PatternMatcher : %d!", __LINE__);
    if (juriPermissionPatterns != NULL) {
        int jcount = env->GetArrayLength(juriPermissionPatterns);

        if (jcount > 0) {
            AutoPtr<ArrayOf<IPatternMatcher*> > uriPermissionPatterns = ArrayOf<IPatternMatcher*>::Alloc((Int32)jcount);
            for (Int32 i = 0; i < jcount; i++) {
                jobject juriPermissionPattern = env->GetObjectArrayElement(juriPermissionPatterns, i);
                ElUtil::CheckErrorAndLog(env, "GetObjectArrayElement: juriPermissionPatterns %d", __LINE__);

                AutoPtr<IPatternMatcher> uriPermissionPattern;
                if (ElUtil::ToElPatternMatcher(env, juriPermissionPattern, (IPatternMatcher**)&uriPermissionPattern)) {
                    uriPermissionPatterns->Set(i, uriPermissionPattern.Get());
                }
                else {
                    ALOGE("ToElProviderInfo() ToElPatternMatcher fail!");
                }

                env->DeleteLocalRef(juriPermissionPattern);
            }

            (*info)->SetUriPermissionPatterns(uriPermissionPatterns);
        }
        env->DeleteLocalRef(juriPermissionPatterns);
    }

    f = env->GetFieldID(piKlass, "pathPermissions", "[Landroid/content/pm/PathPermission;");
    CheckErrorAndLog(env, "GetFieldID: PathPermission : %d!", __LINE__);

    jobjectArray jpathPermissions = (jobjectArray)env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField PathPermission : %d!", __LINE__);
    if (jpathPermissions != NULL) {
        int jcount = env->GetArrayLength(jpathPermissions);

        if (jcount > 0) {
            AutoPtr<ArrayOf<IPathPermission*> > pathPermissions = ArrayOf<IPathPermission*>::Alloc((Int32)jcount);
            for (Int32 i = 0; i < jcount; i++) {
                jobject jpathPermission = env->GetObjectArrayElement(jpathPermissions, i);
                ElUtil::CheckErrorAndLog(env, "GetObjectArrayElement: jpathPermissions %d", __LINE__);

                AutoPtr<IPathPermission> pathPermission;
                if (ElUtil::ToElPathPermission(env, jpathPermission, (IPathPermission**)&pathPermission)) {
                    pathPermissions->Set(i, pathPermission.Get());
                }
                else {
                    ALOGE("ToElProviderInfo() ToElPathPermission fail!");
                }

                env->DeleteLocalRef(jpathPermission);
            }

            (*info)->SetPathPermissions(pathPermissions);
        }
        env->DeleteLocalRef(jpathPermissions);
    }

    tempBool = ElUtil::GetJavaBoolField(env, piKlass, jinfo, "multiprocess", "ToElProviderInfo");
    (*info)->SetMultiprocess(tempBool);

    Int32 tempInt = GetJavaIntField(env, piKlass, jinfo, "initOrder", "ToElProviderInfo");
    (*info)->SetInitOrder(tempInt);

    tempInt = GetJavaIntField(env, piKlass, jinfo, "flags", "ToElProviderInfo");
    (*info)->SetFlags(tempInt);

    tempBool = ElUtil::GetJavaBoolField(env, piKlass, jinfo, "isSyncable", "ToElProviderInfo");
    (*info)->SetIsSyncable(tempBool);

    SetElComponentInfo(env, piKlass, jinfo, IComponentInfo::Probe(*info));

    env->DeleteLocalRef(piKlass);
    return TRUE;
}

bool ElUtil::ToElAuthorityEntry(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jauthorityEntry,
    /* [out] */ IIntentFilterAuthorityEntry** authorityEntry)
{
    if (env == NULL || jauthorityEntry == NULL || authorityEntry == NULL) {
        ALOGE("ToElAuthorityEntry: Invalid argumenet!");
        return FALSE;
    }

    jclass aeKlass = env->FindClass("android/content/IntentFilter$AuthorityEntry");
    CheckErrorAndLog(env, "FindClass: ProxyProperties : %d!\n", __LINE__);

    String origHost = GetJavaStringField(env, aeKlass, jauthorityEntry, "mOrigHost", "ToElAuthorityEntry");
    String host = GetJavaStringField(env, aeKlass, jauthorityEntry, "mHost", "ToElAuthorityEntry");
    Boolean wild = GetJavaBoolField(env, aeKlass, jauthorityEntry, "mWild", "ToElAuthorityEntry");
    Int32 port = GetJavaIntField(env, aeKlass, jauthorityEntry, "mPort", "ToElAuthorityEntry");

    if (NOERROR != CIntentFilterAuthorityEntry::New(authorityEntry)) {
        ALOGE("ToElAuthorityEntry() Create CIntentFilterAuthorityEntry fail!");
        return FALSE;
    }

    AutoPtr<IParcel> parcel;
    Elastos::Droid::Os::CParcel::New((IParcel**)&parcel);
    parcel->WriteString(origHost);
    parcel->WriteString(host);
    parcel->WriteBoolean(wild);
    parcel->WriteInt32(port);
    parcel->SetDataPosition(0);

    IParcelable::Probe(*authorityEntry)->ReadFromParcel(parcel);
    env->DeleteLocalRef(aeKlass);

    return TRUE;
}

bool ElUtil::ToElPatternMatcher(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jpmatcher,
    /* [out] */ IPatternMatcher** pmatcher)
{
    if (env == NULL || jpmatcher == NULL || pmatcher == NULL) {
        ALOGE("ToElPatternMatcher: Invalid argumenet!");
        return FALSE;
    }

    jclass pmKlass = env->FindClass("android/os/PatternMatcher");
    CheckErrorAndLog(env, "FindClass: PatternMatcher : %d!", __LINE__);

    String pattern = ElUtil::GetJavaStringField(env, pmKlass, jpmatcher, "mPattern", "ToElPatternMatcher");
    Int32 type = GetJavaIntField(env, pmKlass, jpmatcher, "mType", "ToElPatternMatcher");

    if (NOERROR != CPatternMatcher::New(pattern, type, pmatcher)) {
        ALOGE("ToElPatternMatcher() Create CPatternMatcher fail!");
        return FALSE;
    }

    env->DeleteLocalRef(pmKlass);
    return TRUE;
}

bool ElUtil::ToElPathPermission(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jpperm,
    /* [out] */ IPathPermission** pperm)
{
    if (env == NULL || jpperm == NULL || pperm == NULL) {
        ALOGE("ToElPathPermission: Invalid argumenet!");
        return FALSE;
    }

    jclass ppKlass = env->FindClass("android/content/pm/PathPermission");
    CheckErrorAndLog(env, "FindClass: PathPermission : %d!", __LINE__);

    String pattern = ElUtil::GetJavaStringField(env, ppKlass, jpperm, "mPattern", "ToElPathPermission");
    Int32 type = GetJavaIntField(env, ppKlass, jpperm, "mType", "ToElPathPermission");
    String readPermission = ElUtil::GetJavaStringField(env, ppKlass, jpperm, "mReadPermission", "ToElPathPermission");
    String writePermission = ElUtil::GetJavaStringField(env, ppKlass, jpperm, "mWritePermission", "ToElPathPermission");

    if (NOERROR != CPathPermission::New(pattern, type, readPermission, writePermission, pperm)) {
        ALOGE("ToElPathPermission() Create CPathPermission fail!");
        return FALSE;
    }

    env->DeleteLocalRef(ppKlass);
    return TRUE;
}

jobject ElUtil::GetJavaDebugMemoryInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IDebugMemoryInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaDebugMemoryInfo() Invalid argumenet!");
        return NULL;
    }

    jclass dmiKlass = env->FindClass("android/os/Debug$MemoryInfo");
    CheckErrorAndLog(env, "FindClass: Debug$MemoryInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(dmiKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: Debug$MemoryInfo : %d!\n", __LINE__);

    jobject jinfo = env->NewObject(dmiKlass, m);
    CheckErrorAndLog(env, "NewObject: Debug$MemoryInfo : %d!\n", __LINE__);

    AutoPtr<IParcelable> parcelable = IParcelable::Probe(info);
    if (parcelable == NULL) {
        ALOGE("GetJavaDebugMemoryInfo: Invalid IParcelable!");
        return NULL;
    }

    AutoPtr<IParcel> source;
    CParcel::New((IParcel**)&source);

    parcelable->WriteToParcel(source);

    Int32 tempInt = 0;
    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "dalvikPss", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "dalvikSwappablePss", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "dalvikPrivateDirty", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "dalvikSharedDirty", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "dalvikPrivateClean", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "dalvikSharedClean", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "dalvikSwappedOut", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "nativePss", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "nativeSwappablePss", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "nativePrivateDirty", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "nativeSharedDirty", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "nativePrivateClean", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "nativeSharedClean", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "nativeSwappedOut", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "otherPss", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "otherSwappablePss", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "otherPrivateDirty", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "otherSharedDirty", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "otherPrivateClean", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "otherSharedClean", "GetJavaDebugMemoryInfo");

    source->ReadInt32(&tempInt);
    ElUtil::SetJavaIntField(env, dmiKlass, jinfo, tempInt, "otherSwappedOut", "GetJavaDebugMemoryInfo");

    AutoPtr<ArrayOf<Int32> > otherStats;
    source->ReadArrayOf((Handle32*)(&otherStats));
    if (otherStats != NULL) {
        jintArray jotherStats = GetJavaIntArray(env, otherStats);
        jfieldID f = env->GetFieldID(dmiKlass, "otherStats", "[I");
        CheckErrorAndLog(env, "GetFieldID: otherStats : %d!\n", __LINE__);

        env->SetObjectField(jinfo, f, jotherStats);
        CheckErrorAndLog(env, "SetIntField: otherStats : %d!\n", __LINE__);

        env->DeleteLocalRef(jotherStats);
    }

    env->DeleteLocalRef(dmiKlass);
    return jinfo;
}

bool ElUtil::JavaObjectToElByteArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jobj,
    /* [out, callee] */ ArrayOf<Byte>** objArray)
{
    if (env == NULL || jobj == NULL || objArray == NULL) {
        ALOGE("JavaObjectToElByteArray() Invalid argumenet!");
        return FALSE;
    }

    jclass baosKlass = env->FindClass("java/io/ByteArrayOutputStream");
    CheckErrorAndLog(env, "FindClass: ByteArrayOutputStream : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(baosKlass, "<init>", "()V");
    CheckErrorAndLog(env, "GetMethodID: ByteArrayOutputStream : %d!\n", __LINE__);

    jobject jbaos = env->NewObject(baosKlass, m);
    CheckErrorAndLog(env, "NewObject: ByteArrayOutputStream : %d!\n", __LINE__);

    jclass oosKlass = env->FindClass("java/io/ObjectOutputStream");
    CheckErrorAndLog(env, "FindClass: ObjectOutputStream : %d!\n", __LINE__);

    m = env->GetMethodID(oosKlass, "<init>", "(Ljava/io/OutputStream;)V");
    CheckErrorAndLog(env, "GetMethodID: ObjectOutputStream : %d!\n", __LINE__);

    jobject joos = env->NewObject(oosKlass, m, jbaos);
    CheckErrorAndLog(env, "NewObject: ObjectOutputStream : %d!\n", __LINE__);

    m = env->GetMethodID(oosKlass, "writeObject", "(Ljava/lang/Object;)V");
    CheckErrorAndLog(env, "GetMethodID: writeObject : %d!\n", __LINE__);

    env->CallVoidMethod(joos, m, jobj);
    CheckErrorAndLog(env, "CallVoidMethod: writeObject: %d!\n", __LINE__);

    m = env->GetMethodID(oosKlass, "close", "()V");
    CheckErrorAndLog(env, "GetMethodID: close : %d!\n", __LINE__);

    env->CallVoidMethod(joos, m);
    CheckErrorAndLog(env, "CallVoidMethod: close: %d!\n", __LINE__);

    m = env->GetMethodID(baosKlass, "toByteArray", "()[B");
    CheckErrorAndLog(env, "GetMethodID: toByteArray : %d!\n", __LINE__);

    jbyteArray jbArray = (jbyteArray)env->CallObjectMethod(jbaos, m);
    CheckErrorAndLog(env, "CallObjectMethod: toByteArray: %d!\n", __LINE__);

    env->DeleteLocalRef(baosKlass);
    env->DeleteLocalRef(oosKlass);
    env->DeleteLocalRef(jbaos);
    env->DeleteLocalRef(joos);

    if (!ElUtil::ToElByteArray(env, jbArray, objArray)) {
        ALOGE("JavaObjectToElByteArray() ToElByteArray fail!");
        env->DeleteLocalRef(jbArray);
        return FALSE;
    }

    env->DeleteLocalRef(jbArray);
    return TRUE;
}

bool ElUtil::ToElParcelable(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jparcelable,
    /* [out] */ IParcelable** parcelable)
{
    if (env == NULL || jparcelable == NULL || parcelable == NULL) {
        ALOGE("ToElParcelable() Invalid argumenet!");
        return FALSE;
    }

    jclass parableClass = env->FindClass("android/os/Parcelable");
    ElUtil::CheckErrorAndLog(env, "FindClass Parcelable fail : %d!\n", __LINE__);

    if (!env->IsInstanceOf(jparcelable, parableClass)) {
        ALOGE("ToElParcelable java object is not Parcelable!");
        env->DeleteLocalRef(parableClass);
        return FALSE;
    }

    jclass parcelClass = env->FindClass("android/os/Parcel");
    ElUtil::CheckErrorAndLog(env, "FindClass: Parcel : %d!\n", __LINE__);

    jmethodID m = env->GetStaticMethodID(parcelClass, "obtain", "()Landroid/os/Parcel;");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: obtain : %d!\n", __LINE__);

    jobject jparcel = env->CallStaticObjectMethod(parcelClass, m);
    ElUtil::CheckErrorAndLog(env, "CallStaticObjectMethod: obtain : %d!\n", __LINE__);

    m = env->GetMethodID(parcelClass, "writeParcelable", "(Landroid/os/Parcelable;I)V");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: writeParcelable : %d!\n", __LINE__);

    env->CallVoidMethod(jparcel, m, jparcelable, 0);
    ElUtil::CheckErrorAndLog(env, "CallVoidMethod: writeParcelable : %d!\n", __LINE__);

    m = env->GetMethodID(parcelClass, "setDataPosition", "(I)V");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: setDataPosition : %d!\n", __LINE__);

    env->CallVoidMethod(jparcel, m, 0);
    ElUtil::CheckErrorAndLog(env, "CallVoidMethod: setDataPosition : %d!\n", __LINE__);

    Int64 nativePtr = ElUtil::GetJavalongField(env, parcelClass, jparcel, "mNativePtr", "ToElParcelable");
    android::Parcel* parcel = reinterpret_cast< android::Parcel*>(nativePtr);

    if (parcel->objectsCount()) {
        ALOGE("ToElParcelable() Parcelable objectsCount <= 0");
        return FALSE;
    }

    AutoPtr<ArrayOf<Byte> > bArray = ArrayOf<Byte>::Alloc(parcel->dataSize());
    if (bArray != NULL) {
        jbyte* array = (jbyte*)bArray->GetPayload();
        memcpy(array, parcel->data(), parcel->dataSize());
    }

    String pkgPath = ElUtil::FindPackageForObject(env, jparcelable);

    if (NOERROR != CIParcelableNative::New(pkgPath, bArray, parcelable)) {
        ALOGE("ToElParcelable new CIParcelableNative fail!\n");
        return FALSE;
    }

    env->DeleteLocalRef(parableClass);
    env->DeleteLocalRef(parcelClass);
    env->DeleteLocalRef(jparcel);
    return TRUE;
}

bool ElUtil::ToElSerializable(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jserializable,
    /* [out] */ ISerializable** serializable)
{
    if (env == NULL || jserializable == NULL || serializable == NULL) {
        ALOGE("FindPackageForObject() Invalid argumenet!");
        return FALSE;
    }

    // AutoPtr<ArrayOf<Byte> > array;
    // if (!ElUtil::JavaObjectToElByteArray(env, jserializable, (ArrayOf<Byte>**)&array)) {
    //     ALOGE("ToElSerializable() JavaObjectToElByteArray fail!");
    //     return FALSE;
    // }

    // jclass objectClass = env->FindClass("java/lang/Object");
    // ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

    // jmethodID m = env->GetMethodID(objectClass, "getClass", "()Ljava/lang/Class;");
    // ElUtil::CheckErrorAndLog(env, "GetMethodID getClass : %d!\n", __LINE__);

    // jobject jklass = env->CallObjectMethod(jserializable, m);

    // jclass classClass = env->FindClass("java/lang/Class");
    // ElUtil::CheckErrorAndLog(env, "FindClass Class : %d!\n", __LINE__);

    // m = env->GetMethodID(classClass, "getClassLoader", "()Ljava/lang/ClassLoader;");
    // ElUtil::CheckErrorAndLog(env, "GetMethodID getClassLoader : %d!\n", __LINE__);

    // jobject jclassLoader = env->CallObjectMethod(jklass, m);

    // Boolean isDexClassLoader = FALSE;
    // String dexPath, optimizedDirectory;
    // String objClassName = GetClassName(env, jclassLoader);
    // if (objClassName.Equals("dalvik.system.PathClassLoader")) {
    //     jclass pathClassLoaderClass = env->GetObjectClass(jclassLoader);
    //     ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);
    //     dexPath = ElUtil::GetJavaStringField(env, pathClassLoaderClass, jclassLoader, "originalPath", "ToElSerializable");
    //     env->DeleteLocalRef(pathClassLoaderClass);
    // }
    // else if (objClassName.Equals("dalvik.system.DexClassLoader")) {
    //     isDexClassLoader = TRUE;
    //     jclass dexClassLoaderClass = env->GetObjectClass(jclassLoader);
    //     ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);
    //     dexPath = ElUtil::GetJavaStringField(env, dexClassLoaderClass, jclassLoader, "originalPath", "ToElSerializable");
    //     // optimizedDirectory = ElUtil::GetJavaStringField(env, dexClassLoaderClass, jclassLoader, "mOptimizedDirectory", "ToElSerializable");
    //     jobject jloader = env->NewGlobalRef(jclassLoader);
    //     optimizedDirectory.AppendFormat("%d", (Int32)jloader);
    //     env->DeleteLocalRef(dexClassLoaderClass);
    // }
    // else {
    //     ALOGE("ToElSerializable() Unknown ClassLoader name = %s", objClassName.string());
    //     dexPath = ElUtil::FindPackageForObject(env, jserializable);
    // }

    // env->DeleteLocalRef(objectClass);
    // env->DeleteLocalRef(jklass);
    // env->DeleteLocalRef(classClass);
    // env->DeleteLocalRef(jclassLoader);

    // if (NOERROR != CISerializableNative::New(isDexClassLoader, dexPath, optimizedDirectory, array, serializable)) {
    //     ALOGE("ToElSerializable new CISerializableNative fail!\n");
    //     return FALSE;
    // }

    // return TRUE;
    return FALSE;
}

String ElUtil::FindPackageForObject(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jobject)
{
    if (env == NULL || jobject == NULL) {
        ALOGE("FindPackageForObject() Invalid argumenet!");
        return String(NULL);
    }

    jclass ppKlass = env->FindClass("android/os/ElParcelableProxy");
    CheckErrorAndLog(env, "FindClass: ElParcelableProxy : %d!\n", __LINE__);

    jmethodID m = env->GetStaticMethodID(ppKlass, "getCurrentApkPath", "()Ljava/lang/String;");
    CheckErrorAndLog(env, "GetStaticMethodID: getCurrentApkPath%d!\n", __LINE__);

    jstring jpkgPath = (jstring)env->CallStaticObjectMethod(ppKlass, m);
    CheckErrorAndLog(env, "CallStaticObjectMethod():  %d!\n", __LINE__);

    String pkgPath = ElUtil::ToElString(env, jpkgPath);

    env->DeleteLocalRef(ppKlass);
    env->DeleteLocalRef(jpkgPath);
    return pkgPath;
}

jobject ElUtil::GetJavaCountry(
    /* [in] */ JNIEnv* env,
    /* [in] */ ICountry* country)
{
    if (env == NULL || country == NULL) {
        ALOGE("GetJavaCountry() Invalid argumenet!");
        return NULL;
    }

    jclass cKlass = env->FindClass("android/location/Country");
    CheckErrorAndLog(env, "FindClass: Country : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(cKlass, "<init>", "(Ljava/lang/String;IJ)V");
    CheckErrorAndLog(env, "GetMethodID: Country : %d!\n", __LINE__);

    String countryIso;
    country->GetCountryIso(&countryIso);
    jstring jcountryIso = ElUtil::GetJavaString(env, countryIso);

    Int32 source = 0;
    country->GetSource(&source);

    Int64 timestamp = 0;
    country->GetTimestamp(&timestamp);

    jobject jcountry = env->NewObject(cKlass, m, jcountryIso, (jint)source, (jlong)timestamp);
    CheckErrorAndLog(env, "NewObject: Country : %d!\n", __LINE__);

    env->DeleteLocalRef(cKlass);
    env->DeleteLocalRef(jcountryIso);
    return jcountry;
}

bool ElUtil::ToElAlarmClockInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IAlarmClockInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElAlarmClockInfo: Invalid argumenet!");
        return false;
    }

    jclass aciKlass = env->FindClass("android/app/AlarmManager$AlarmClockInfo");
    CheckErrorAndLog(env, "FindClass: AlarmClockInfo : %d!\n", __LINE__);

    Int64 triggerTime = ElUtil::GetJavalongField(env, aciKlass, jinfo, "mTriggerTime", "ToElAlarmClockInfo");

    jfieldID f = env->GetFieldID(aciKlass, "mShowIntent", "Landroid/app/PendingIntent;");
    CheckErrorAndLog(env, "GetFieldID: InetAddress : %d!\n", __LINE__);

    jobject jshowIntent = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField mShowIntent : %d!\n", __LINE__);

    AutoPtr<IPendingIntent> showIntent;
    if (jshowIntent != NULL) {
        if (!ElUtil::ToElPendingIntent(env, jshowIntent, (IPendingIntent**)&showIntent)) {
            ALOGD("ToElAlarmClockInfo: ToElPendingIntent fail!");
        }

        env->DeleteLocalRef(jshowIntent);
    }

    env->DeleteLocalRef(aciKlass);

    if (NOERROR != CAlarmClockInfo::New(triggerTime, showIntent, info)) {
        ALOGD("ToElAlarmClockInfo: create CAlarmClockInfo fail!");
        return false;
    }

    return true;
}

jobject ElUtil::GetJavaAlarmClockInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IAlarmClockInfo* info)
{
    if (env == NULL || info == NULL) {
        ALOGE("GetJavaAlarmClockInfo() Invalid arguments!");
        return NULL;
    }

    jclass aciKlass = env->FindClass("android/app/AlarmManager$AlarmClockInfo");
    CheckErrorAndLog(env, "FindClass: AlarmClockInfo : %d!\n", __LINE__);

    Int64 triggerTime;
    info->GetTriggerTime(&triggerTime);

    AutoPtr<IPendingIntent> showIntent;
    info->GetShowIntent((IPendingIntent**)&showIntent);

    jmethodID m = env->GetMethodID(aciKlass, "<init>", "(JLandroid/app/PendingIntent;)V");
    CheckErrorAndLog(env, "GetMethodID: ObbInfo() : %d!\n", __LINE__);

    jobject jshowIntent = GetJavaPendingIntent(env, showIntent);

    jobject jinfo = env->NewObject(aciKlass, m, triggerTime, jshowIntent);
    CheckErrorAndLog(env, "NewObject: ObbInfo : %d!\n", __LINE__);

    env->DeleteLocalRef(aciKlass);
    env->DeleteLocalRef(jshowIntent);

    return jinfo;
}

bool ElUtil::ToElProfilerInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jinfo,
    /* [out] */ IProfilerInfo** info)
{
    if (env == NULL || jinfo == NULL || info == NULL) {
        ALOGE("ToElProfilerInfo: Invalid argumenet!");
        return false;
    }

    jclass infoKlass = env->FindClass("android/app/ProfilerInfo");
    CheckErrorAndLog(env, "FindClass: ProfilerInfo : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(infoKlass, "profileFd", "Landroid/os/ParcelFileDescriptor;");
    CheckErrorAndLog(env, "GetFieldID: profileFd : %d!\n", __LINE__);

    jobject jprofileFd = env->GetObjectField(jinfo, f);
    CheckErrorAndLog(env, "GetObjectField profileFd : %d!\n", __LINE__);

    AutoPtr<IParcelFileDescriptor> profileFd;
    if (jprofileFd != NULL) {
        if (!ElUtil::ToElParcelFileDescriptor(env, jprofileFd, (IParcelFileDescriptor**)&profileFd)) {
            ALOGD("ToElProfilerInfo: ToElParcelFileDescriptor fail!");
        }

        env->DeleteLocalRef(jprofileFd);
    }

    String profileFile = ElUtil::GetJavaStringField(env, infoKlass, jinfo, "profileFile", "ToElProfilerInfo");
    Int32 samplingInterval = ElUtil::GetJavaIntField(env, infoKlass, jinfo, "samplingInterval", "ToElProfilerInfo");
    Boolean autoStopProfiler = ElUtil::GetJavaBoolField(env, infoKlass, jinfo, "autoStopProfiler", "ToElProfilerInfo");

    env->DeleteLocalRef(infoKlass);

    if (NOERROR != CProfilerInfo::New(profileFile, profileFd, samplingInterval, autoStopProfiler, info)) {
        ALOGD("ToElProfilerInfo: create CProfilerInfo fail!");
        return false;
    }

    return true;
}

bool ElUtil::ToElPersistableBundle(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jbundle,
    /* [out] */ IPersistableBundle** bundle)
{
    if (env == NULL || bundle == NULL) {
        ALOGD("ToElPersistableBundle: Invalid argumenet!");
        return false;
    }

    if (NOERROR != CPersistableBundle::New(bundle)) {
        ALOGD("ToElPersistableBundle: create CIntent fail!");
        return false;
    }

    return SetElBaseBundle(env, jbundle, IBaseBundle::Probe(*bundle));
}

jobject ElUtil::GetJavaIAppTask(
    /* [in] */ JNIEnv* env,
    /* [in] */ IIAppTask* appTask)
{
    ALOGE("ElUtil::GetJavaIAppTask is not implemented!");
    assert(0);
    return NULL;
}

bool ElUtil::ToElTaskDescription(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jdescription,
    /* [out] */ IActivityManagerTaskDescription** description)
{
    if (env == NULL || jdescription == NULL || description == NULL) {
        ALOGE("ToElTaskDescription: Invalid argumenet!");
        return false;
    }

    jclass descriptionKlass = env->FindClass("android/app/ActivityManager$TaskDescription");
    CheckErrorAndLog(env, "FindClass: TaskDescription : %d!\n", __LINE__);

    String label = GetJavaStringField(env, descriptionKlass, jdescription, "mLabel", "ToElTaskDescription");

    jfieldID f = env->GetFieldID(descriptionKlass, "mIcon", "Landroid/graphics/Bitmap;");
    CheckErrorAndLog(env, "GetFieldID: mIcon : %d!\n", __LINE__);

    jobject jicon = env->GetObjectField(jdescription, f);
    CheckErrorAndLog(env, "GetObjectField mIcon : %d!\n", __LINE__);

    AutoPtr<IBitmap> icon;
    if (jicon != NULL) {
        if (!ElUtil::ToElBitmap(env, jicon, (IBitmap**)&icon)) {
            ALOGD("ToElTaskDescription: ToElParcelFileDescriptor fail!");
        }

        env->DeleteLocalRef(jicon);
    }

    String iconFilename = GetJavaStringField(env, descriptionKlass, jdescription, "mIconFilename", "ToElTaskDescription");
    Int32 colorPrimary = ElUtil::GetJavaIntField(env, descriptionKlass, jdescription, "mColorPrimary", "ToElTaskDescription");

    env->DeleteLocalRef(descriptionKlass);

    if (icon != NULL) {
        if (NOERROR != CActivityManagerTaskDescription::New(label, icon, colorPrimary, description)) {
            ALOGD("ToElTaskDescription: create CActivityManagerTaskDescription fail!");
            return false;
        }
    }
    else {
        if (NOERROR != CActivityManagerTaskDescription::New(label, colorPrimary, iconFilename, description)) {
            ALOGD("ToElTaskDescription: create CActivityManagerTaskDescription fail!");
            return false;
        }
    }

    return true;
}

bool ElUtil::ToElPoint(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jpoint,
    /* [out] */ IPoint** point)
{
    if (env == NULL || jpoint == NULL || point == NULL) {
        ALOGE("ToElPoint: Invalid argumenet!");
        return false;
    }

    jclass pointKlass = env->FindClass("android/graphics/Point");
    CheckErrorAndLog(env, "ToElPoint Fail FindClass: Point : %d!\n", __LINE__);

    Int32 x = ElUtil::GetJavaIntField(env, pointKlass, jpoint, "x", "ToElPoint");
    Int32 y = ElUtil::GetJavaIntField(env, pointKlass, jpoint, "y", "ToElPoint");

    env->DeleteLocalRef(pointKlass);

    if (NOERROR != CPoint::New(x, y, point)) {
        ALOGD("ToElPoint: create CPoint fail!");
        return false;
    }

    return true;
}

jobject ElUtil::GetJavaPoint(
    /* [in] */ JNIEnv* env,
    /* [in] */ IPoint* point)
{
    if (env == NULL || point == NULL) {
        ALOGD("GetJavaPoint() Invalid arguments!");
        return NULL;
    }

    jobject jpoint = NULL;

    jclass pointKlass = env->FindClass("android/graphics/Point");
    CheckErrorAndLog(env, "GetJavaPoint Fail FindClass: Point : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(pointKlass, "<init>", "(II)V");
    CheckErrorAndLog(env, "GetJavaPoint Fail GetMethodID: Point() : %d!\n", __LINE__);

    Int32 x;
    point->GetX(&x);
    Int32 y;
    point->GetY(&y);

    jpoint = env->NewObject(pointKlass, m, x, y);
    CheckErrorAndLog(env, "GetJavaPoint Fail NewObject: Point : %d!\n", __LINE__);

    env->DeleteLocalRef(pointKlass);

    return jpoint;
}

jobject ElUtil::GetJavaStackInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityManagerStackInfo* stackInfo)
{
    ALOGE("ElUtil::GetJavaStackInfo is not implemented!");
    assert(0);
    return NULL;
}

jobject ElUtil::GetJavaUriPermission(
    /* [in] */ JNIEnv* env,
    /* [in] */ IUriPermission* uriPermission)
{
    ALOGE("ElUtil::GetJavaUriPermission is not implemented!");
    assert(0);
    return NULL;
}

bool ElUtil::ToElActivityOptions(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject joptions,
    /* [out] */ IActivityOptions** options)
{
    return true;
}

jobject ElUtil::GetJavaActivityOptions(
    /* [in] */ JNIEnv* env,
    /* [in] */ IActivityOptions* options)
{
    if (env == NULL || options == NULL) {
        ALOGE("GetJavaActivityOptions() Invalid arguments!");
        return NULL;
    }

    AutoPtr<IBundle> data;
    options->ToBundle((IBundle**)&data);
    return ElUtil::GetJavaBundle(env, data);
}

jobject ElUtil::GetJavaIActivityContainer(
    /* [in] */ JNIEnv* env,
    /* [in] */ IIActivityContainer* container)
{
    ALOGE("ElUtil::GetJavaIActivityContainer is not implemented!");
    assert(0);
    return NULL;
}

bool ElUtil::ToElStringList(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jlist,
    /* [out] */ IList** list)
{
    if (env == NULL || jlist == NULL || list == NULL) {
        ALOGE("ToElStringList() Invalid arguments!");
        return FALSE;
    }

    if (NOERROR != CParcelableList::New(list)) {
        return FALSE;
    }

    jclass listKlass = env->FindClass("java/util/List");
    ElUtil::CheckErrorAndLog(env, "FindClass: List : %d!\n", __LINE__);

    jmethodID mSize = env->GetMethodID(listKlass, "size", "()I");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: size : %d!\n", __LINE__);

    jint size = env->CallIntMethod(jlist, mSize);
    ElUtil::CheckErrorAndLog(env, "CallIntMethod: size : %d!\n", __LINE__);

    jmethodID mGet = env->GetMethodID(listKlass, "get", "(I)Ljava/lang/Object;");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

    for (jint i = 0; i < size; i++) {
        jstring jstr = (jstring)env->CallObjectMethod(jlist, mGet, i);
        ElUtil::CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);

        String str = ElUtil::ToElString(env, jstr);
        AutoPtr<ICharSequence> csstr;
        CString::New(str, (ICharSequence**)&csstr);
        (*list)->Add(csstr);

        env->DeleteLocalRef(jstr);
    }
    env->DeleteLocalRef(listKlass);

    return true;
}

jobject ElUtil::GetJavaStringList(
    /* [in] */ JNIEnv* env,
    /* [in] */ IList* list)
{
    if (env == NULL || list == NULL) {
        ALOGE("GetJavaStringList() Invalid arguments!");
        return NULL;
    }

    jclass arrayListCls = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID constructor = env->GetMethodID(arrayListCls, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "GetJavaStringList Failed GetMethodID <init>: %d!\n", __LINE__);

    jmethodID mid_add = env->GetMethodID(arrayListCls, "add", "(Ljava/lang/Object;)Z");
    ElUtil::CheckErrorAndLog(env, "GetJavaStringList Failed GetMethodID add : %d!\n", __LINE__);

    jobject arrayList = env->NewObject(arrayListCls, constructor);
    ElUtil::CheckErrorAndLog(env, "GetJavaStringList Failed NewObject : %d!\n", __LINE__);
    env->DeleteLocalRef(arrayListCls);

    Int32 size;
    list->GetSize(&size);
    for (Int32 i = 0; i < size; i++) {
        AutoPtr<IInterface> item;
        list->Get(i, (IInterface**)&item);
        jstring jstr = NULL;
        if (ICharSequence::Probe(item) != NULL) {
            String str;
            ICharSequence::Probe(item)->ToString(&str);
            jstr = GetJavaString(env, str);
        }
        env->CallBooleanMethod(arrayList, mid_add, jstr);
        env->DeleteLocalRef(jstr);
    }

    env->DeleteLocalRef(arrayListCls);

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetActiveAdmins");

    return arrayList;
}

jobject ElUtil::GetJavaStatusBarNotification(
    /* [in] */ JNIEnv* env,
    /* [in] */ IStatusBarNotification* notification)
{
    ALOGE("ElUtil::GetJavaStatusBarNotification is not implemented!");
    assert(0);
    return NULL;
}

bool ElUtil::ToElZenModeConfig(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconfig,
    /* [out] */ IZenModeConfig** config)
{
    ALOGE("ElUtil::ToElZenModeConfig is not implemented!");
    assert(0);
    return true;
}

jobject ElUtil::GetJavaZenModeConfig(
    /* [in] */ JNIEnv* env,
    /* [in] */ IZenModeConfig* config)
{
    ALOGE("ElUtil::GetJavaZenModeConfig is not implemented!");
    assert(0);
    return NULL;
}

bool ElUtil::ToElCondition(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jcondition,
    /* [out] */ ICondition** condition)
{
    if (env == NULL || jcondition == NULL || condition == NULL) {
        ALOGE("ToElCondition: Invalid argumenet!");
        return false;
    }

    jclass conditionKlass = env->FindClass("android/service/notification/Condition");
    CheckErrorAndLog(env, "FindClass: Condition : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(conditionKlass, "id", "Landroid/net/Uri;");
    CheckErrorAndLog(env, "GetFieldID: id : %d!\n", __LINE__);

    jobject jid = env->GetObjectField(jcondition, f);
    CheckErrorAndLog(env, "GetObjectField id : %d!\n", __LINE__);

    AutoPtr<IUri> id;
    if (jid != NULL) {
        if (!ElUtil::ToElUri(env, jid, (IUri**)&id)) {
            ALOGD("ToElCondition: ToElUri fail!");
        }

        env->DeleteLocalRef(jid);
    }

    String summary = ElUtil::GetJavaStringField(env, conditionKlass, jcondition, "summary", "ToElCondition");
    String line1 = ElUtil::GetJavaStringField(env, conditionKlass, jcondition, "line1", "ToElCondition");
    String line2 = ElUtil::GetJavaStringField(env, conditionKlass, jcondition, "line2", "ToElCondition");
    Int32 icon = ElUtil::GetJavaIntField(env, conditionKlass, jcondition, "icon", "ToElCondition");
    Int32 state = ElUtil::GetJavaIntField(env, conditionKlass, jcondition, "state", "ToElCondition");
    Int32 flags = ElUtil::GetJavaIntField(env, conditionKlass, jcondition, "flags", "ToElCondition");

    env->DeleteLocalRef(conditionKlass);

    if (NOERROR != CCondition::New(id, summary, line1, line2, icon, state, flags, condition)) {
        ALOGD("ToElCondition: create CCondition fail!");
        return false;
    }

    return true;
}

jobject ElUtil::GetJavaCondition(
    /* [in] */ JNIEnv* env,
    /* [in] */ ICondition* condition)
{
    if (env == NULL || condition == NULL) {
        ALOGE("GetJavaCondition() Invalid arguments!");
        return NULL;
    }

    AutoPtr<IUri> id;
    condition->GetId((IUri**)&id);
    jobject jid = GetJavaUri(env, id);

    String summary;
    condition->GetSummary(&summary);
    jstring jsummary = GetJavaString(env, summary);

    String line1;
    condition->GetLine1(&line1);
    jstring jline1 = GetJavaString(env, line1);

    String line2;
    condition->GetLine2(&line2);
    jstring jline2 = GetJavaString(env, line2);

    Int32 icon;
    condition->GetIcon(&icon);

    Int32 state;
    condition->GetState(&state);

    Int32 flags;
    condition->GetFlags(&flags);

    jclass conditionKlass = env->FindClass("android/service/notification/Condition");
    CheckErrorAndLog(env, "FindClass: Condition : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(conditionKlass, "<init>", "(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V");
    CheckErrorAndLog(env, "GetMethodID: Condition() : %d!\n", __LINE__);

    jobject jcondition = env->NewObject(conditionKlass, m, jid, jsummary, jline1, jline2, icon, state, flags);
    CheckErrorAndLog(env, "NewObject: Condition : %d!\n", __LINE__);

    env->DeleteLocalRef(conditionKlass);
    env->DeleteLocalRef(jid);
    env->DeleteLocalRef(jsummary);
    env->DeleteLocalRef(jline1);
    env->DeleteLocalRef(jline2);

    return jcondition;
}

jobject ElUtil::GetJavaIntentSender(
    /* [in] */ JNIEnv* env,
    /* [in] */ IIntentSender* intentSender)
{
    if (env == NULL || intentSender == NULL) {
        ALOGE("GetJavaIntentSender: Invalid argumenet!");
        return NULL;
    }

    AutoPtr<IIIntentSender> iIntentSender;

    intentSender->GetTarget((IIIntentSender**)&iIntentSender);

    if (iIntentSender == NULL){
        ALOGE("GetJavaIntentSender, Got iIntentSender is NULL");
        return NULL;
    }

    jobject jintentSenderSender = GetJavaIIntentSender(env, iIntentSender);

    jclass klass = env->FindClass("android/content/IntentSender");
    CheckErrorAndLog(env, "FindClass: IntentSender : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(Landroid/content/IIntentSender;)V");
    CheckErrorAndLog(env, "GetJavaIntentSender GetMethodID: IntentSender(IIntentSender) : %d!\n", __LINE__);

    jobject jintentSender = env->NewObject(klass, m, jintentSenderSender);
    CheckErrorAndLog(env, "GetJavaIntentSender NewObject: PendingIntent : %d!\n", __LINE__);

    env->DeleteLocalRef(klass);
    env->DeleteLocalRef(jintentSenderSender);
    return jintentSender;
}

jobject ElUtil::GetJavaIIntentSender(
    /* [in] */ JNIEnv* env,
    /* [in] */ IIIntentSender* isender)
{
    if (env == NULL || isender == NULL) {
        ALOGE("GetJavaIIntentSender: Invalid argumenet!");
        return NULL;
    }

    jclass pergKlass = env->FindClass("android/content/ElIIntentSenderProxy");
    CheckErrorAndLog(env, "GetJavaIIntentSender FindClass: ElIIntentSenderProxy : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(pergKlass, "<init>", "(J)V");
    CheckErrorAndLog(env, "GetJavaIIntentSender GetMethodID: ElIIntentSenderProxy : %d!\n", __LINE__);

    jobject jisender = env->NewObject(pergKlass, m, (jlong)isender);
    CheckErrorAndLog(env, "GetJavaIIntentSender NewObject: ElIIntentSenderProxy : %d!\n", __LINE__);
    isender->AddRef();

    env->DeleteLocalRef(pergKlass);
    return jisender;
}

bool ElUtil::ToElSyncRequest(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jrequest,
    /* [out] */ ISyncRequest** request)
{
    ALOGE("ElUtil::ToElSyncRequest is not implemented!");
    assert(0);
    return true;
}

bool ElUtil::ToElKeySet(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jkeySet,
    /* [out] */ IKeySet** keySet)
{
    ALOGE("ElUtil::ToElKeySet is not implemented!");
    assert(0);
    return true;
}

jobject ElUtil::GetJavaKeySet(
    /* [in] */ JNIEnv* env,
    /* [in] */ IKeySet* keySet)
{
    ALOGE("ElUtil::GetJavaKeySet is not implemented!");
    assert(0);
    return NULL;
}

jobject ElUtil::GetJavaComposedIconInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IComposedIconInfo* info)
{
    ALOGE("ElUtil::GetJavaComposedIconInfo is not implemented!");
    assert(0);
    return NULL;
}

bool ElUtil::ToElInputDeviceIdentifier(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jidentifier,
    /* [out] */ IInputDeviceIdentifier** identifier)
{
    ALOGE("ElUtil::ToElInputDeviceIdentifier is not implemented!");
    assert(0);
    return true;
}

bool ElUtil::ToElTouchCalibration(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jcalibration,
    /* [out] */ ITouchCalibration** calibration)
{
    ALOGE("ElUtil::ToElTouchCalibration is not implemented!");
    assert(0);
    return true;
}

jobject ElUtil::GetJavaTouchCalibration(
    /* [in] */ JNIEnv* env,
    /* [in] */ ITouchCalibration* calibration)
{
    ALOGE("ElUtil::GetJavaTouchCalibration is not implemented!");
    assert(0);
    return NULL;
}

jobject ElUtil::GetJavaWindowContentFrameStats(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWindowContentFrameStats* stats)
{
    ALOGE("ElUtil::GetJavaWindowContentFrameStats is not implemented!");
    assert(0);
    return NULL;
}

jobject ElUtil::GetJavaWifiActivityEnergyInfo(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiActivityEnergyInfo* info)
{
    ALOGE("ElUtil::GetJavaWifiActivityEnergyInfo is not implemented!");
    assert(0);
    return NULL;
}

jobject ElUtil::GetJavaWifiChannel(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiChannel* channel)
{
    ALOGE("ElUtil::GetJavaWifiChannel is not implemented!");
    assert(0);
    return NULL;
}

bool ElUtil::ToElScanSettings(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jscanSettings,
    /* [out] */ IScanSettings** scanSettings)
{
    ALOGE("ElUtil::ToElScanSettings is not implemented!");
    assert(0);
    return true;
}

bool ElUtil::ToElBatchedScanSettings(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jsettings,
    /* [out] */ IBatchedScanSettings** settings)
{
    ALOGE("ElUtil::ToElBatchedScanSettings is not implemented!");
    assert(0);
    return true;
}

bool ElUtil::ToElMediaMetadata(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jmetadata,
    /* [out] */ IMediaMetadata** metadata)
{
    if (env == NULL || jmetadata == NULL || metadata == NULL) {
        ALOGE("ToElMediaMetadata: Invalid argumenet!");
        return false;
    }

    jclass c = env->FindClass("android/media/MediaMetadata");
    ElUtil::CheckErrorAndLog(env, "ToElMediaMetadata FindClass: MediaMetadata : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(c, "mBundle", "Landroid/os/Bundle;");
    CheckErrorAndLog(env, "ToElMediaMetadata GetFieldID: mBundle: %d!\n", __LINE__);

    jobject jBundle = env->GetObjectField(jmetadata, f);
    CheckErrorAndLog(env, "ToElMediaMetadata GetObjectField: jBundle : %d!\n", __LINE__);

    if (jBundle != NULL) {
        AutoPtr<IBundle> elBundle;
        if (ToElBundle(env, jBundle, (IBundle**)&elBundle)) {
            CMediaMetadata::New(elBundle, metadata);
        }
        else {
            ALOGE("ToElMediaMetadata : ToElBundle(jBundle) fail : %d!\n", __LINE__);
        }
        env->DeleteLocalRef(jBundle);
    }
    env->DeleteLocalRef(c);
    if (*metadata != NULL)
        return true;
    else
    {
        ALOGE("ToElMediaMetadata: CMediaMetadata::New fail : %d!\n", __LINE__);
        return false;
    }
}

bool ElUtil::ToElPlaybackState(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jstate,
    /* [out] */ IPlaybackState** _state)
{
    if (env == NULL || jstate == NULL || _state == NULL) {
        ALOGE("ToElPlaybackState: Invalid argumenet!");
        return false;
    }

    jclass c = env->FindClass("android/media/session/PlaybackState");
    ElUtil::CheckErrorAndLog(env, "ToElPlaybackState FindClass: PlaybackState: %d!\n", __LINE__);

    jint state = GetJavaIntField(env, c, jstate, "mState", "ToElPlaybackState");
    jlong position = GetJavalongField(env, c, jstate, "mPosition", "ToElPlaybackState");
    jlong bufferedPosition = GetJavalongField(env, c, jstate, "mBufferedPosition", "ToElPlaybackState");
    jfloat speed = GetJavafloatField(env, c, jstate, "mSpeed", "ToElPlaybackState");
    jlong actions= GetJavalongField(env, c, jstate, "mActions", "ToElPlaybackState");

    jfieldID f = env->GetFieldID(c, "mErrorMessage", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "ToElPlaybackState GetFieldID mErrorMessage: %d!\n", __LINE__);
    jobject jerrorMessage = env->GetObjectField(jstate, f);
    CheckErrorAndLog(env, "ToElPlaybackState GetObjectField mErrorMessage: %d!\n", __LINE__);
    AutoPtr<ICharSequence> errorMessage;
    ToElCharSequence(env, jerrorMessage, (ICharSequence**)&errorMessage);
    env->DeleteLocalRef(jerrorMessage);

    f = env->GetFieldID(c, "mCustomActions", "Ljava/util/List;");
    CheckErrorAndLog(env, "ToElPlaybackState GetFieldID: mCustomActions: %d!\n", __LINE__);

    jobject jcustomActions = env->GetObjectField(jstate, f);
    CheckErrorAndLog(env, "ToElPlaybackState GetObjectField: mCustomActions: %d!\n", __LINE__);

    AutoPtr<IList> customActions;
    CArrayList::New((IList**)&customActions);
    if (jcustomActions != NULL) {
        jclass listClass = env->FindClass("java/util/List");
        CheckErrorAndLog(env, "ToElPlaybackState FindClass List: %d!\n", __LINE__);
        jmethodID mSize = env->GetMethodID(listClass, "size", "()I");
        CheckErrorAndLog(env, "ToElPlaybackState GetMethodID: size: %d!\n", __LINE__);
        jint jsize = env->CallIntMethod(jcustomActions, mSize);
        CheckErrorAndLog(env, "ToElPlaybackState CallIntMethod: %d!\n", __LINE__);

        if (jsize > 0) {
            jmethodID mGet = env->GetMethodID(listClass, "get", "(I)Ljava/lang/Object;");
            CheckErrorAndLog(env, "ToElPlaybackState GetMethodID: get : %d!\n", __LINE__);
            for (jint i = 0; i < jsize; i++) {
                jobject jcustomAction = env->CallObjectMethod(jcustomActions, mGet, i);
                CheckErrorAndLog(env, "ToElPlaybackState CallObjectMethod: get : %d!\n", __LINE__);

                if (jcustomAction != NULL) {
                    AutoPtr<IPlaybackStateCustomAction> customAction;
                    if(!ElUtil::ToElPlaybackStateCustomAction(env, jcustomAction, (IPlaybackStateCustomAction**)&customAction)) {
                        ALOGE("ToElPlaybackState , ToElPlaybackStateCustomAction fail!");
                    }
                    if (customAction != NULL) {
                        customActions->Add(customAction);
                    }
                }
                env->DeleteLocalRef(jcustomAction);
            }
        }

        env->DeleteLocalRef(listClass);
        env->DeleteLocalRef(jcustomActions);
    }

    jlong updateTime = GetJavalongField(env, c, jstate, "mUpdateTime", "ToElPlaybackState");
    jlong activeItemId = GetJavalongField(env, c, jstate, "mActiveItemId", "ToElPlaybackState");

    env->DeleteLocalRef(c);
    CPlaybackState::New((Int32)state, (Int64)position, (Int64)updateTime,
                        (Float)speed, (Int64)bufferedPosition,
                        (Int64)actions, customActions,
                        (Int64)activeItemId, errorMessage, (IPlaybackState**)&_state);
    if (*_state != NULL)
        return true;
    else
    {
        ALOGE("ToElPlaybackState: CPlaybackState::New fail : %d!\n", __LINE__);
        return false;
    }
}

bool ElUtil::ToElPlaybackStateCustomAction(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jcustomeAction,
    /* [out] */ IPlaybackStateCustomAction** customAction)
{
    if (env == NULL || jcustomeAction == NULL || customAction == NULL) {
        ALOGE("ToElPlaybackState: Invalid argumenet!");
        return false;
    }

    jclass c = env->FindClass("android/media/session/PlaybackState$CustomAction");
    ElUtil::CheckErrorAndLog(env, "ToElPlaybackStateCustomAction FindClass: PlaybackState$CustomAction: %d!\n", __LINE__);

    String action = ElUtil::GetJavaStringField(env, c, jcustomeAction, "mAction", "ToElPlaybackStateCustomAction");

    jfieldID f = env->GetFieldID(c, "mName", "Ljava/lang/CharSequence;");
    CheckErrorAndLog(env, "ToElPlaybackStateCustomAction GetFieldID : %d!\n", __LINE__);
    jobject jname = env->GetObjectField(jcustomeAction, f);
    CheckErrorAndLog(env, "ToElPlaybackStateCustomAction GetObjectField : %d!\n", __LINE__);
    AutoPtr<ICharSequence> name;
    ToElCharSequence(env, jname, (ICharSequence**)&name);
    env->DeleteLocalRef(jname);

    jint icon = GetJavaIntField(env, c, jcustomeAction, "mIcon", "ToElPlaybackStateCustomAction");
    f = env->GetFieldID(c, "mExtras", "Landroid/os/Bundle;");
    CheckErrorAndLog(env, "ToElPlaybackStateCustomAction GetFieldID: mExtras: %d!\n", __LINE__);
    jobject jBundle = env->GetObjectField(jcustomeAction, f);
    CheckErrorAndLog(env, "ToElPlaybackStateCustomAction GetObjectField: jBundle : %d!\n", __LINE__);

    AutoPtr<IBundle> elBundle;
    if (jBundle != NULL) {
        if (!ToElBundle(env, jBundle, (IBundle**)&elBundle)) {
            ALOGE("ToElPlaybackStateCustomAction : ToElBundle(jBundle) fail : %d!\n", __LINE__);
        }
        env->DeleteLocalRef(jBundle);
    }

    env->DeleteLocalRef(c);

    CPlaybackStateCustomAction::New(action, name, icon, elBundle, (IPlaybackStateCustomAction**)&customAction);
    if (*customAction != NULL)
        return true;
    else
    {
        ALOGE("ToElPlaybackStateCustomAction : CPlaybackStateCustomAction::New fail : %d!\n", __LINE__);
        return false;
    }
}

jobject ElUtil::GetJavaBatchedScanSettings(
    /* [in] */ JNIEnv* env,
    /* [in] */ IBatchedScanSettings* settings)
{
    ALOGE("ElUtil::GetJavaBatchedScanSettings is not implemented!");
    assert(0);
    return NULL;
}

jobject ElUtil::GetJavaWifiConnectionStatistics(
    /* [in] */ JNIEnv* env,
    /* [in] */ IWifiConnectionStatistics* settings)
{
    ALOGE("ElUtil::GetJavaWifiConnectionStatistics is not implemented!");
    assert(0);
    return NULL;
}

bool ElUtil::ToElNetworkTemplate(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jtempl,
    /* [out] */ INetworkTemplate** templ)
{
    if (env == NULL || jtempl == NULL || templ == NULL) {
        ALOGE("ToElNetworkTemplate: Invalid argumenet!");
        return false;
    }

    jclass netTemplateKlass = env->FindClass("android/net/NetworkTemplate");
    CheckErrorAndLog(env, "FindClass: NetworkTemplate : %d!\n", __LINE__);

    Int32 matchRule = ElUtil::GetJavaIntField(env, netTemplateKlass, jtempl, "mMatchRule", "ToElNetworkTemplate");
    String subscriberId = ElUtil::GetJavaStringField(env, netTemplateKlass, jtempl, "mSubscriberId", "ToElNetworkTemplate");
    String networkId = ElUtil::GetJavaStringField(env, netTemplateKlass, jtempl, "mNetworkId", "ToElNetworkTemplate");

    if (NOERROR != CNetworkTemplate::New(matchRule, subscriberId, networkId, templ)) {
        ALOGD("ToElNetworkTemplate: create CNetworkTemplate fail!");
        return false;
    }

    env->DeleteLocalRef(netTemplateKlass);

    return true;
}

jobject ElUtil::GetJavaNetworkStats(
    /* [in] */ JNIEnv* env,
    /* [in] */ INetworkStats* stats)
{
    if (env == NULL || stats == NULL) {
        ALOGE("GetJavaNetworkStats() Invalid argumenet!");
        return NULL;
    }

    jclass nsKlass = env->FindClass("android/net/NetworkStats");
    CheckErrorAndLog(env, "FindClass: NetworkStats : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(nsKlass, "<init>", "(JI)V");
    CheckErrorAndLog(env, "GetMethodID: NetworkStats : %d!\n", __LINE__);

    Int64 elapsedRealtime;
    stats->GetElapsedRealtime(&elapsedRealtime);
    Int32 size;
    stats->GetSize(&size);

    jobject jstats = env->NewObject(nsKlass, m, elapsedRealtime, size);
    CheckErrorAndLog(env, "NewObject: NetworkStats : %d!\n", __LINE__);

    m = env->GetMethodID(nsKlass, "addValues", "(Ljava/lang/String;IIIJJJJJ)Landroid/net/NetworkStats;");
    CheckErrorAndLog(env, "GetMethodID: addValues : %d!\n", __LINE__);

    String iface;
    Int32 uid, set, tag;
    Int64 rxBytes, rxPackets, txBytes, txPackets, operations;
    AutoPtr<INetworkStatsEntry> entry, temp;
    for (Int32 i = 0; i < size; i++) {
        if (i == 0)
            stats->GetValues(i, NULL, (INetworkStatsEntry**)&entry);
        else
            stats->GetValues(i, entry, (INetworkStatsEntry**)&temp);

        entry->GetIface(&iface);
        entry->GetUid(&uid);
        entry->GetSet(&set);
        entry->GetTag(&tag);
        entry->GetRxBytes(&rxBytes);
        entry->GetRxPackets(&rxPackets);
        entry->GetTxBytes(&txBytes);
        entry->GetTxPackets(&txPackets);
        entry->GetOperations(&operations);
        jstring jiface = GetJavaString(env, iface);
        env->CallObjectMethod(nsKlass, m, jiface, uid, set, tag, rxBytes,
            rxPackets, txBytes, txPackets, operations);
        CheckErrorAndLog(env, "CallObjectMethod: addValues : %d!\n", __LINE__);
        env->DeleteLocalRef(jiface);
    }

    env->DeleteLocalRef(nsKlass);
    return jstats;
}

jobject ElUtil::GetJavaNetworkStatsHistory(
    /* [in] */ JNIEnv* env,
    /* [in] */ INetworkStatsHistory* statsHistory)
{
    if (env == NULL || statsHistory == NULL) {
        ALOGE("GetJavaNetworkStatsHistory() Invalid argumenet!");
        return NULL;
    }

    jclass nsKlass = env->FindClass("android/net/NetworkStatsHistory");
    CheckErrorAndLog(env, "FindClass: NetworkStatsHistory : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(nsKlass, "<init>", "(JI)V");
    CheckErrorAndLog(env, "GetMethodID: NetworkStatsHistory : %d!\n", __LINE__);

    Int64 bucketDuration;
    statsHistory->GetBucketDuration(&bucketDuration);
    Int32 size;
    statsHistory->GetSize(&size);

    jobject jstatsHistory = env->NewObject(nsKlass, m, bucketDuration, size);
    CheckErrorAndLog(env, "NewObject: NetworkStatsHistory : %d!\n", __LINE__);

    AutoPtr<ArrayOf<Int64> > bucketStart = ArrayOf<Int64>::Alloc(size);
    AutoPtr<ArrayOf<Int64> > activeTime = ArrayOf<Int64>::Alloc(size);
    AutoPtr<ArrayOf<Int64> > rxBytes = ArrayOf<Int64>::Alloc(size);
    AutoPtr<ArrayOf<Int64> > rxPackets = ArrayOf<Int64>::Alloc(size);
    AutoPtr<ArrayOf<Int64> > txBytes = ArrayOf<Int64>::Alloc(size);
    AutoPtr<ArrayOf<Int64> > txPackets = ArrayOf<Int64>::Alloc(size);
    AutoPtr<ArrayOf<Int64> > operations = ArrayOf<Int64>::Alloc(size);

    Int64 value;
    AutoPtr<INetworkStatsHistoryEntry> entry, temp;
    for (Int32 i = 0; i < size; i++) {
        if (i == 0)
            statsHistory->GetValues(i, NULL, (INetworkStatsHistoryEntry**)&entry);
        else
            statsHistory->GetValues(i, entry, (INetworkStatsHistoryEntry**)&temp);

        entry->GetBucketStart(&value);
        (*bucketStart)[i] = value;
        entry->GetActiveTime(&value);
        (*activeTime)[i] = value;
        entry->GetRxBytes(&value);
        (*rxBytes)[i] = value;
        entry->GetRxPackets(&value);
        (*rxPackets)[i] = value;
        entry->GetTxBytes(&value);
        (*txBytes)[i] = value;
        entry->GetTxPackets(&value);
        (*txPackets)[i] = value;
        entry->GetOperations(&value);
        (*operations)[i] = value;
    }

    Int64 totalBytes;
    statsHistory->GetTotalBytes(&totalBytes);

    SetJavaIntField(env, nsKlass, jstatsHistory, size, "bucketCount", "GetJavaNetworkStatsHistory");

    jlongArray jbucketStart = GetJavaLongArray(env, bucketStart);
    jfieldID f = env->GetFieldID(nsKlass, "bucketStart", "[J");
    CheckErrorAndLog(env, "GetFieldID: bucketStart : %d!\n", __LINE__);

    env->SetObjectField(jstatsHistory, f, jbucketStart);
    CheckErrorAndLog(env, "SetIntField: bucketStart : %d!\n", __LINE__);

    jlongArray jactiveTime = GetJavaLongArray(env, activeTime);
    f = env->GetFieldID(nsKlass, "activeTime", "[J");
    CheckErrorAndLog(env, "GetFieldID: activeTime : %d!\n", __LINE__);

    env->SetObjectField(jstatsHistory, f, jactiveTime);
    CheckErrorAndLog(env, "SetIntField: activeTime : %d!\n", __LINE__);

    jlongArray jrxBytes = GetJavaLongArray(env, rxBytes);
    f = env->GetFieldID(nsKlass, "rxBytes", "[J");
    CheckErrorAndLog(env, "GetFieldID: rxBytes : %d!\n", __LINE__);

    env->SetObjectField(jstatsHistory, f, jrxBytes);
    CheckErrorAndLog(env, "SetIntField: rxBytes : %d!\n", __LINE__);

    jlongArray jrxPackets = GetJavaLongArray(env, rxPackets);
    f = env->GetFieldID(nsKlass, "rxPackets", "[J");
    CheckErrorAndLog(env, "GetFieldID: rxPackets : %d!\n", __LINE__);

    env->SetObjectField(jstatsHistory, f, jrxPackets);
    CheckErrorAndLog(env, "SetIntField: rxPackets : %d!\n", __LINE__);

    jlongArray jtxBytes = GetJavaLongArray(env, txBytes);
    f = env->GetFieldID(nsKlass, "txBytes", "[J");
    CheckErrorAndLog(env, "GetFieldID: txBytes : %d!\n", __LINE__);

    env->SetObjectField(jstatsHistory, f, jtxBytes);
    CheckErrorAndLog(env, "SetIntField: txBytes : %d!\n", __LINE__);

    jlongArray jtxPackets = GetJavaLongArray(env, txPackets);
    f = env->GetFieldID(nsKlass, "txPackets", "[J");
    CheckErrorAndLog(env, "GetFieldID: txPackets : %d!\n", __LINE__);

    env->SetObjectField(jstatsHistory, f, jtxPackets);
    CheckErrorAndLog(env, "SetIntField: txPackets : %d!\n", __LINE__);

    jlongArray joperations = GetJavaLongArray(env, operations);
    f = env->GetFieldID(nsKlass, "operations", "[J");
    CheckErrorAndLog(env, "GetFieldID: operations : %d!\n", __LINE__);

    env->SetObjectField(jstatsHistory, f, joperations);
    CheckErrorAndLog(env, "SetIntField: operations : %d!\n", __LINE__);

    SetJavalongField(env, nsKlass, jstatsHistory, totalBytes, "totalBytes", "GetJavaNetworkStatsHistory");

    env->DeleteLocalRef(jbucketStart);
    env->DeleteLocalRef(jactiveTime);
    env->DeleteLocalRef(jrxBytes);
    env->DeleteLocalRef(jrxPackets);
    env->DeleteLocalRef(jtxBytes);
    env->DeleteLocalRef(jtxPackets);
    env->DeleteLocalRef(joperations);
    env->DeleteLocalRef(nsKlass);
    return jstatsHistory;
}

bool ElUtil::ToElSparseArray(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jarray,
    /* [out] */ ISparseArray** array)
{
    if (env == NULL || jarray == NULL) {
        ALOGE("ToElSparseArray: Invalid argumenet!");
        return false;
    }

    CSparseArray::New(array);

    jclass arrayKlass = env->FindClass("android/util/SparseArray");
    CheckErrorAndLog(env, "FindClass: SparseArray : %d!", __LINE__);

    jmethodID m = env->GetMethodID(arrayKlass, "size", "()I");
    CheckErrorAndLog(env, "GetMethodID: size : %d!", __LINE__);

    jint jsize = env->CallIntMethod(jarray, m);
    CheckErrorAndLog(env, "CallObjectMethod: jsize : %d!", __LINE__);
    m = env->GetMethodID(arrayKlass, "get", "(I)Ljava/lang/Object;");
    CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);
    for (int i = 0; i < jsize; i++) {
        jobject jValue = env->CallObjectMethod(jarray, m, i);
        CheckErrorAndLog(env, "CallObjectMethod: jValue : %d!\n", __LINE__);

        if (jValue != NULL) {
            String objClassName = GetClassName(env, jValue);
            if (objClassName.Equals("java.lang.String")) {
                String value = ToElString(env, (jstring)jValue);
                AutoPtr<ICharSequence> cs;
                CString::New(value, (ICharSequence**)&cs);
                (*array)->Put(i, cs);
            }
            else {
                jclass serClass = env->FindClass("java/io/Serializable");
                ElUtil::CheckErrorAndLog(env, "FindClass Serializable fail : %d!\n", __LINE__);

                jclass parClass = env->FindClass("android/os/Parcelable");
                ElUtil::CheckErrorAndLog(env, "FindClass Parcelable fail : %d!\n", __LINE__);

                if (env->IsInstanceOf(jValue, serClass)) {
                    AutoPtr<ISerializable> serializable;
                    if (ElUtil::ToElSerializable(env, jValue, (ISerializable**)&serializable)) {
                        (*array)->Put(i, serializable);
                    }
                    else {
                        ALOGE("ToElSparseArray ToElSerializable fail");
                    }
                }
                else if (env->IsInstanceOf(jValue, parClass)) {
                    AutoPtr<IParcelable> parcelable;
                    if (ElUtil::ToElParcelable(env, jValue, (IParcelable**)&parcelable)) {
                        (*array)->Put(i, parcelable);
                    }
                    else {
                        ALOGE("ToElSparseArray ToElParcelable fail");
                    }
                }
                else {
                    ALOGE("ToElSparseArray() Unsupported type:%s; Key:%s", objClassName.string());
                }

                env->DeleteLocalRef(serClass);
                env->DeleteLocalRef(parClass);
            }

            env->DeleteLocalRef(jValue);
        }
        else {
            (*array)->Put(i, NULL);
        }
    }

    env->DeleteLocalRef(arrayKlass);

    return true;
}


jobject ElUtil::GetJavaAudioRoutesInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IAudioRoutesInfo* event) {
    if (env == NULL || event == NULL) {
        ALOGE("ElUtil::GetJavaAudioRoutesInfo() invalid param!");
        return NULL;
    }

    jclass klass = env->FindClass("android/media/AudioRoutesInfo");
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo Fail FindClass: AudioRoutesInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo Fail GetMethodID: AudioRoutesInfo() : %d!\n", __LINE__);

    jobject jaudioRoutesInfo = env->NewObject(klass, m);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo Fail NewObject: AudioRoutesInfo : %d!\n", __LINE__);

    // Set mBluetoothName
    AutoPtr<ICharSequence> csBluetoothName;
    event->GetBluetoothName((ICharSequence**)&csBluetoothName);
    if (csBluetoothName != NULL) {
        String blueToothName;
        csBluetoothName->ToString(&blueToothName);
        jstring jbluetoothName = ElUtil::GetJavaString(env, blueToothName);

        jfieldID f = env->GetFieldID(klass, "mBluetoothName", "Ljava/lang/CharSequence;");
        CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail GetFieldID: CharSequence:mBluetoothName", __LINE__);

        env->SetObjectField(jaudioRoutesInfo, f, jbluetoothName);
        CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: mBluetoothName", __LINE__);
        env->DeleteLocalRef(jbluetoothName);
    }

    // Set mMainType
    Int32 mainType;
    event->GetMainType(&mainType);
    jfieldID f = env->GetFieldID(klass, "mMainType", "I");
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail GetFieldID: int:mMainType", __LINE__);

    env->SetIntField(jaudioRoutesInfo, f, (jint)mainType);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: mMainType", __LINE__);

    env->DeleteLocalRef(klass);
    return jaudioRoutesInfo;
}

bool ElUtil::ToElAudioPolicyConfig(
    /* [in] */ JNIEnv* env,
    /* [in] */ jobject jconfig,
    /* [out] */ IAudioPolicyConfig** config)
{
    if (config == NULL) {
        ALOGD("config: Invalid argumenet!");
        return false;
    }

    ALOGE("ElUtil::ToElAudioPolicyConfig is not implemented!");
    assert(0);

    return true;
}

jobject ElUtil::GetJavaMediaRouterClientState(
        /* [in] */ JNIEnv* env,
        /* [in] */ IMediaRouterClientState* state)
{
    if (env == NULL || state == NULL) {
        ALOGE("ElUtil::GetJavaMediaRouterClientState() invalid param!");
        return NULL;
    }

    jclass klass = env->FindClass("android/media/MediaRouterClientState");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientState Fail FindClass: MediaRouterClientState : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "()V");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientState Fail GetMethodID: MediaRouterClientState() : %d!\n", __LINE__);

    jobject jstate = env->NewObject(klass, m);
    CheckErrorAndLog(env, "GetJavaMediaRouterClientState Fail NewObject: MediaRouterClientState : %d!\n", __LINE__);

    AutoPtr<IArrayList> routes;
    state->GetRoutes((IArrayList**)&routes);
    if (routes != NULL) {
        Int32 count = 0;
        routes->GetSize(&count);
        if (count > 0) {
            jfieldID f = env->GetFieldID(klass, "routes", "Ljava/util/ArrayList;");
            CheckErrorAndLog(env, "GetJavaMediaRouterClientState, Fail GetFieldID: routes", __LINE__);

            jobject jroutes = env->GetObjectField(jstate, f);
            CheckErrorAndLog(env, "GetJavaMediaRouterClientState, Fail GetObjectField: routes", __LINE__);

            jclass listKlass = env->FindClass("Ljava/util/ArrayList;");
            CheckErrorAndLog(env, "GetJavaMediaRouterClientState Fail FindClass: ArrayList : %d!\n", __LINE__);

            jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
            CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);

            for (Int32 i = 0; i < count; i++) {
                AutoPtr<IInterface> obj;
                routes->Get(i, (IInterface**)&obj);
                AutoPtr<IMediaRouterClientStateRouteInfo> route = IMediaRouterClientStateRouteInfo::Probe(obj);

                jobject jroute = GetJavaMediaRouterClientStateRouteInfo(env, route);

                env->CallBooleanMethod(jroutes, mAdd, jroute);
                CheckErrorAndLog(env, "CallBooleanMethod: add : %d!\n", __LINE__);

                env->DeleteLocalRef(jroute);
            }
            env->DeleteLocalRef(jroutes);
            env->DeleteLocalRef(listKlass);
        }
    }

    env->DeleteLocalRef(klass);
    return jstate;
}

jobject ElUtil::GetJavaMediaRouterClientStateRouteInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IMediaRouterClientStateRouteInfo* routeInfo)
{
    if (env == NULL || routeInfo == NULL) {
        ALOGE("ElUtil::GetJavaMediaRouterClientStateRouteInfo() invalid param!");
        return NULL;
    }

    jclass klass = env->FindClass("android/media/MediaRouterClientState$RouteInfo");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo Fail FindClass: MediaRouterClientState$RouteInfo : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(Ljava/lang/String)V");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo Fail GetMethodID: MediaRouterClientState$RouteInfo() : %d!\n", __LINE__);

    String id;
    routeInfo->GetId(&id);
    jstring jid = GetJavaString(env, id);

    jobject jrouteInfo = env->NewObject(klass, m, jid);
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo Fail NewObject: MediaRouterClientState$RouteInfo : %d!\n", __LINE__);

    String name;
    routeInfo->GetName(&name);
    jstring jname = GetJavaString(env, name);
    jfieldID f = env->GetFieldID(klass, "name", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: String:name", __LINE__);

    env->SetObjectField(jrouteInfo, f, jname);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: name", __LINE__);

    String description;
    routeInfo->GetDescription(&description);
    jstring jdescription = GetJavaString(env, description);
        f = env->GetFieldID(klass, "description", "Ljava/lang/String;");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: String:description", __LINE__);

    env->SetObjectField(jrouteInfo, f, jdescription);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: description", __LINE__);

    Int32 supportedTypes;
    routeInfo->GetSupportedTypes(&supportedTypes);
    f = env->GetFieldID(klass, "supportedTypes", "I");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: int:supportedTypes", __LINE__);

    env->SetIntField(jrouteInfo, f, (jint)supportedTypes);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: supportedTypes", __LINE__);

    Boolean enabled;
    routeInfo->GetEnabled(&enabled);
    f = env->GetFieldID(klass, "enabled", "I");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: boolean:enabled", __LINE__);

    env->SetBooleanField(jrouteInfo, f, (jboolean)enabled);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: enabled", __LINE__);

    Int32 statusCode;
    routeInfo->GetStatusCode(&statusCode);
    f = env->GetFieldID(klass, "statusCode", "I");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: int:statusCode", __LINE__);

    env->SetIntField(jrouteInfo, f, (jint)statusCode);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: statusCode", __LINE__);

    Int32 playbackType;
    routeInfo->GetPlaybackType(&playbackType);
    f = env->GetFieldID(klass, "playbackType", "I");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: int:playbackType", __LINE__);

    env->SetIntField(jrouteInfo, f, (jint)playbackType);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: playbackType", __LINE__);

    Int32 playbackStream;
    routeInfo->GetPlaybackStream(&playbackStream);
    f = env->GetFieldID(klass, "playbackStream", "I");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: int:playbackStream", __LINE__);

    env->SetIntField(jrouteInfo, f, (jint)playbackStream);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: playbackStream", __LINE__);

    Int32 volume;
    routeInfo->GetVolume(&volume);
    f = env->GetFieldID(klass, "volume", "I");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: int:volume", __LINE__);

    env->SetIntField(jrouteInfo, f, (jint)volume);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: volume", __LINE__);

    Int32 volumeMax;
    routeInfo->GetVolumeMax(&volumeMax);
    f = env->GetFieldID(klass, "volumeMax", "I");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: int:volumeMax", __LINE__);

    env->SetIntField(jrouteInfo, f, (jint)volumeMax);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: volumeMax", __LINE__);

    Int32 volumeHandling;
    routeInfo->GetVolumeHandling(&volumeHandling);
    f = env->GetFieldID(klass, "volumeHandling", "I");
    CheckErrorAndLog(env, "GetJavaMediaRouterClientStateRouteInfo, Fail GetFieldID: int:volumeHandling", __LINE__);

    env->SetIntField(jrouteInfo, f, (jint)volumeHandling);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: volumeHandling", __LINE__);

    Int32 presentationDisplayId;
    routeInfo->GetPresentationDisplayId(&presentationDisplayId);
    f = env->GetFieldID(klass, "presentationDisplayId", "I");
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, GetJavaMediaRouterClientStateRouteInfo GetFieldID: int:presentationDisplayId", __LINE__);

    env->SetIntField(jrouteInfo, f, (jint)presentationDisplayId);
    CheckErrorAndLog(env, "GetJavaAudioRoutesInfo, Fail SetObjectField: presentationDisplayId", __LINE__);

    env->DeleteLocalRef(klass);
    return jrouteInfo;
}

jobject ElUtil::GetJavaMediaMetadata(
    /* [in] */ JNIEnv* env,
    /* [in] */ IMediaMetadata* data)
{
    if (env == NULL || data == NULL) {
        ALOGE("ElUtil::GetJavaMediaMetadata() invalid param!");
        return NULL;
    }
    AutoPtr<IParcel> source;
    CParcel::New((IParcel**)&source);
    IParcelable::Probe(data)->WriteToParcel(source);
    source->SetDataPosition(0);

    AutoPtr<IInterface> obj;
    source->ReadInterfacePtr((Handle32*)&obj);
    jobject jbundle = GetJavaBundle(env, IBundle::Probe(obj));

    jclass klass = env->FindClass("android/media/MediaMetadata");
    CheckErrorAndLog(env, "GetJavaMediaMetadata Fail FindClass: MediaMetadata : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(klass, "<init>", "(Landroid/os/Bundle;)V");
    CheckErrorAndLog(env, "GetJavaMediaMetadata Fail GetMethodID: MediaMetadata() : %d!\n", __LINE__);

    jobject jdata = env->NewObject(klass, m, jbundle);
    CheckErrorAndLog(env, "GetJavaMediaMetadata Fail NewObject: MediaMetadata : %d!\n", __LINE__);

    env->DeleteLocalRef(klass);
    env->DeleteLocalRef(jbundle);

    return jdata;
}

} //namespace android
