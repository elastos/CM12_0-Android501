
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Hardware.h>
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Hardware::Usb::IIUsbManager;
using Elastos::Droid::Hardware::Usb::IUsbAccessory;
using Elastos::Droid::Hardware::Usb::IUsbDevice;

static void android_hardware_usb_ElUsbManagerProxy_nativeGetDeviceList(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jdevices)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeGetDeviceList()");

    IIUsbManager* um = (IIUsbManager*)jproxy;
    AutoPtr<IBundle> devices;
    ECode ec = um->GetDeviceList((IBundle**)&devices);
    if (FAILED(ec))
        ALOGE("nativeGetDeviceList() ec: 0x%08x ", ec);

    if (devices != NULL) {
        if (!ElUtil::SetJavaBaseBundle(env, IBaseBundle::Probe(devices), jdevices)) {
            ALOGE("nativeGetDeviceList() GetJavaBundle fail!");
        }
    }

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeGetDeviceList()");
}

static jboolean android_hardware_usb_ElUsbManagerProxy_nativeHasDefaults(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint juserId)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeHasDefaults()");

    if (jproxy == 0) {
        ALOGE("TODO: new CUsbManagerService");
        return FALSE;
    }
    String packageName = ElUtil::ToElString(env, jpackageName);

    IIUsbManager* um = (IIUsbManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = um->HasDefaults(packageName, (Int32)juserId, &result);
    if (FAILED(ec))
        ALOGE("nativeHasDefaults() ec: 0x%08x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeHasDefaults()");
    return (jboolean)result;
}

static jobject android_hardware_usb_ElUsbManagerProxy_nativeOpenDevice(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jdeviceName)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeOpenDevice()");

    String deviceName = ElUtil::ToElString(env, jdeviceName);

    IIUsbManager* um = (IIUsbManager*)jproxy;
    AutoPtr<IParcelFileDescriptor> devices;
    ECode ec = um->OpenDevice(deviceName, (IParcelFileDescriptor**)&devices);
    if (FAILED(ec))
        ALOGE("nativeOpenDevice() ec: 0x%x ", ec);

    jobject jdevices = NULL;
    if (devices != NULL) {
        jdevices = ElUtil::GetJavaParcelFileDescriptor(env, devices);
    }

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeOpenDevice()");
    return jdevices;
}

static jobject android_hardware_usb_ElUsbManagerProxy_nativeGetCurrentAccessory(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeGetCurrentAccessory()");

    IIUsbManager* um = (IIUsbManager*)jproxy;
    AutoPtr<IUsbAccessory> accessory;
    ECode ec = um->GetCurrentAccessory((IUsbAccessory**)&accessory);
    if (FAILED(ec))
        ALOGE("nativeGetCurrentAccessory() ec: 0x%x ", ec);

    jobject jaccessory = NULL;
    if (accessory != NULL) {
        jaccessory = ElUtil::GetJavaUsbAccessory(env, accessory);
    }

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeGetCurrentAccessory()");
    return jaccessory;
}

static jobject android_hardware_usb_ElUsbManagerProxy_nativeOpenAccessory(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccessory)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeOpenAccessory()");

    AutoPtr<IUsbAccessory> accessory;
    if (jaccessory != NULL) {
        if (!ElUtil::ToElUsbAccessory(env, jaccessory, (IUsbAccessory**)&accessory)) {
            ALOGE("nativeOpenAccessory() ToElUsbAccessory fail!");
        }
    }

    IIUsbManager* um = (IIUsbManager*)jproxy;
    AutoPtr<IParcelFileDescriptor> pfd;
    ECode ec = um->OpenAccessory(accessory, (IParcelFileDescriptor**)&pfd);
    if (FAILED(ec))
        ALOGE("nativeOpenAccessory() ec: 0x%x ", ec);

    jobject jpfd = NULL;
    if (pfd != NULL) {
        jpfd = ElUtil::GetJavaParcelFileDescriptor(env, pfd);
    }

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeOpenAccessory()");
    return jpfd;
}

static void android_hardware_usb_ElUsbManagerProxy_naitveSetDevicePackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jdevice, jstring jpackageName, jint juserId)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeOpenAccessory()");

    AutoPtr<IUsbDevice> device;
    if (jdevice != NULL) {
        if (!ElUtil::ToElUsbDevice(env, jdevice, (IUsbDevice**)&device)) {
            ALOGE("naitveSetDevicePackage() ToElUsbDevice fail!");
        }
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->SetDevicePackage(device, packageName, (Int32)juserId);
    if (FAILED(ec))
        ALOGE("naitveSetDevicePackage() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_naitveSetDevicePackage()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeSetAccessoryPackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccessory, jstring jpackageName, jint juserId)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeOpenAccessory()");

    AutoPtr<IUsbAccessory> accessory;
    if (jaccessory != NULL) {
        if (!ElUtil::ToElUsbAccessory(env, jaccessory, (IUsbAccessory**)&accessory)) {
            ALOGE("nativeSetAccessoryPackage() ToElUsbAccessory fail!");
        }
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->SetAccessoryPackage(accessory, packageName, (Int32)juserId);
    if (FAILED(ec))
        ALOGE("nativeSetAccessoryPackage() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeSetAccessoryPackage()");
}

static jboolean android_hardware_usb_ElUsbManagerProxy_nativeHasDevicePermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jdevice)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeHasDevicePermission()");

    AutoPtr<IUsbDevice> device;
    if (jdevice != NULL) {
        if (!ElUtil::ToElUsbDevice(env, jdevice, (IUsbDevice**)&device)) {
            ALOGE("nativeHasDevicePermission() ToElUsbDevice fail!");
        }
    }

    IIUsbManager* um = (IIUsbManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = um->HasDevicePermission(device, &result);
    if (FAILED(ec))
        ALOGE("nativeHasDevicePermission() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeHasDevicePermission()");
    return (jboolean)result;
}

static jboolean android_hardware_usb_ElUsbManagerProxy_nativeHasAccessoryPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccessory)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeHasAccessoryPermission()");

    AutoPtr<IUsbAccessory> accessory;
    if (jaccessory != NULL) {
        if (!ElUtil::ToElUsbAccessory(env, jaccessory, (IUsbAccessory**)&accessory)) {
            ALOGE("nativeHasAccessoryPermission() ToElUsbAccessory fail!");
        }
    }

    IIUsbManager* um = (IIUsbManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = um->HasAccessoryPermission(accessory, &result);
    if (FAILED(ec))
        ALOGE("nativeHasAccessoryPermission() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeHasAccessoryPermission()");
    return (jboolean)result;
}

static void android_hardware_usb_ElUsbManagerProxy_nativeRequestDevicePermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jdevice, jstring jpackageName, jobject jpi)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeRequestDevicePermission()");

    AutoPtr<IUsbDevice> device;
    if (jdevice != NULL) {
        if (!ElUtil::ToElUsbDevice(env, jdevice, (IUsbDevice**)&device)) {
            ALOGE("nativeRequestDevicePermission() ToElUsbDevice fail!");
        }
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IPendingIntent> pi;
    if (jpi != NULL) {
        if (!ElUtil::ToElPendingIntent(env, jpi, (IPendingIntent**)&pi)) {
            ALOGE("nativeRequestDevicePermission() ToElPendingIntent fail!");
        }
    }

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->RequestDevicePermission(device, packageName, pi);
    if (FAILED(ec))
        ALOGE("nativeRequestDevicePermission() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeRequestDevicePermission()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeRequestAccessoryPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccessory, jstring jpackageName, jobject jpi)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeRequestAccessoryPermission()");

    AutoPtr<IUsbAccessory> accessory;
    if (jaccessory != NULL) {
        if (!ElUtil::ToElUsbAccessory(env, jaccessory, (IUsbAccessory**)&accessory)) {
            ALOGE("nativeRequestAccessoryPermission() ToElUsbAccessory fail!");
        }
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IPendingIntent> pi;
    if (jpi != NULL) {
        if (!ElUtil::ToElPendingIntent(env, jpi, (IPendingIntent**)&pi)) {
            ALOGE("nativeRequestAccessoryPermission() ToElPendingIntent fail!");
        }
    }

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->RequestAccessoryPermission(accessory, packageName, pi);
    if (FAILED(ec))
        ALOGE("nativeRequestAccessoryPermission() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeRequestAccessoryPermission()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeGrantDevicePermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jdevice, jint juid)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeGrantDevicePermission()");

    AutoPtr<IUsbDevice> device;
    if (jdevice != NULL) {
        if (!ElUtil::ToElUsbDevice(env, jdevice, (IUsbDevice**)&device)) {
            ALOGE("nativeGrantDevicePermission() ToElUsbDevice fail!");
        }
    }

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->GrantDevicePermission(device, (Int32)juid);
    if (FAILED(ec))
        ALOGE("nativeGrantDevicePermission() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeGrantDevicePermission()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeGrantAccessoryPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccessory, jint juid)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeGrantDevicePermission()");

    AutoPtr<IUsbAccessory> accessory;
    if (jaccessory != NULL) {
        if (!ElUtil::ToElUsbAccessory(env, jaccessory, (IUsbAccessory**)&accessory)) {
            ALOGE("nativeGrantAccessoryPermission() ToElUsbAccessory fail!");
        }
    }

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->GrantAccessoryPermission(accessory, (Int32)juid);
    if (FAILED(ec))
        ALOGE("nativeGrantAccessoryPermission() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeGrantAccessoryPermission()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeClearDefaults(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint juserId)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeClearDefaults()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->ClearDefaults(packageName, (Int32)juserId);
    if (FAILED(ec))
        ALOGE("nativeClearDefaults() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeClearDefaults()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeSetCurrentFunction(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jfunction, jboolean jmakeDefault)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeSetCurrentFunction()");

    String function = ElUtil::ToElString(env, jfunction);

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->SetCurrentFunction(function, (Boolean)jmakeDefault);
    if (FAILED(ec))
        ALOGE("nativeSetCurrentFunction() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeSetCurrentFunction()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeSetMassStorageBackingFile(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpath)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeSetMassStorageBackingFile()");

    String path = ElUtil::ToElString(env, jpath);

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->SetMassStorageBackingFile(path);
    if (FAILED(ec))
        ALOGE("nativeSetMassStorageBackingFile() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeSetMassStorageBackingFile()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeAllowUsbDebugging(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jalwaysAllow, jstring jpublicKey)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeAllowUsbDebugging()");

    String publicKey = ElUtil::ToElString(env, jpublicKey);

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->AllowUsbDebugging((Boolean)jalwaysAllow, publicKey);
    if (FAILED(ec))
        ALOGE("nativeAllowUsbDebugging() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeAllowUsbDebugging()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeDenyUsbDebugging(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeDenyUsbDebugging()");

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->DenyUsbDebugging();
    if (FAILED(ec))
        ALOGE("nativeDenyUsbDebugging() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeDenyUsbDebugging()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeClearUsbDebuggingKeys(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeClearUsbDebuggingKeys()");

    IIUsbManager* um = (IIUsbManager*)jproxy;
    ECode ec = um->ClearUsbDebuggingKeys();
    if (FAILED(ec))
        ALOGE("nativeClearUsbDebuggingKeys() ec: 0x%x ", ec);

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeClearUsbDebuggingKeys()");
}

static void android_hardware_usb_ElUsbManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_hardware_usb_ElUsbManagerProxy_nativeDestroy()");

    IIUsbManager* obj = (IIUsbManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_hardware_usb_ElUsbManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeFinalize },
    { "nativeGetDeviceList",    "(JLandroid/os/Bundle;)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeGetDeviceList },
    { "nativeHasDefaults",    "(JLjava/lang/String;I)Z",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeHasDefaults },
    { "nativeOpenDevice",    "(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeOpenDevice },
    { "nativeGetCurrentAccessory",    "(J)Landroid/hardware/usb/UsbAccessory;",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeGetCurrentAccessory },
    { "nativeOpenAccessory",    "(JLandroid/hardware/usb/UsbAccessory;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeOpenAccessory },
    { "naitveSetDevicePackage",    "(JLandroid/hardware/usb/UsbDevice;Ljava/lang/String;I)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_naitveSetDevicePackage },
    { "nativeSetAccessoryPackage",    "(JLandroid/hardware/usb/UsbAccessory;Ljava/lang/String;I)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeSetAccessoryPackage },
    { "nativeHasDevicePermission",    "(JLandroid/hardware/usb/UsbDevice;)Z",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeHasDevicePermission },
    { "nativeHasAccessoryPermission",    "(JLandroid/hardware/usb/UsbAccessory;)Z",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeHasAccessoryPermission },
    { "nativeRequestDevicePermission",    "(JLandroid/hardware/usb/UsbDevice;Ljava/lang/String;Landroid/app/PendingIntent;)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeRequestDevicePermission },
    { "nativeRequestAccessoryPermission",    "(JLandroid/hardware/usb/UsbAccessory;Ljava/lang/String;Landroid/app/PendingIntent;)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeRequestAccessoryPermission },
    { "nativeGrantDevicePermission",    "(JLandroid/hardware/usb/UsbDevice;I)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeGrantDevicePermission },
    { "nativeGrantAccessoryPermission",    "(JLandroid/hardware/usb/UsbAccessory;I)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeGrantAccessoryPermission },
    { "nativeClearDefaults",    "(JLjava/lang/String;I)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeClearDefaults },
    { "nativeSetCurrentFunction",    "(JLjava/lang/String;Z)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeSetCurrentFunction },
    { "nativeSetMassStorageBackingFile",    "(JLjava/lang/String;)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeSetMassStorageBackingFile },
    { "nativeAllowUsbDebugging",    "(JZLjava/lang/String;)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeAllowUsbDebugging },
    { "nativeDenyUsbDebugging",    "(J)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeDenyUsbDebugging },
    { "nativeClearUsbDebuggingKeys",    "(J)V",
            (void*) android_hardware_usb_ElUsbManagerProxy_nativeClearUsbDebuggingKeys },
};

int register_android_hardware_usb_ElUsbManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/hardware/usb/ElUsbManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

