#define LOG_TAG "ElInstrumentationWatcherProxyJNI"

#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Os.h>
#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::App::IInstrumentationWatcher;

static void android_app_ElInstrumentationWatcherProxy_nativeInstrumentationStatus(JNIEnv* env, jclass clazz, jlong nativePtr, jobject name, jint resultCode, jobject results)
{
    IInstrumentationWatcher* watcher = (IInstrumentationWatcher*)nativePtr;
    if (watcher != NULL) {
        AutoPtr<IComponentName> elname;
        if (name != NULL) {
            if(!ElUtil::ToElComponentName(env, name, (IComponentName**)&elname)) {
                ALOGE("nativeInstrumentationStatus() ToElComponentName fail!");
            }
        }

        AutoPtr<IBundle> elresults;
        if (results != NULL) {
            if (!ElUtil::ToElBundle(env, results, (IBundle**)&elresults)) {
                ALOGE("nativeInstrumentationStatus() ToElBundle fail!");
            }
        }

        watcher->InstrumentationStatus(elname, (Int32)resultCode, elresults);
    }
}

static void android_app_ElInstrumentationWatcherProxy_nativeInstrumentationFinished(JNIEnv* env, jclass clazz, jlong nativePtr, jobject name, jint resultCode, jobject results)
{
    IInstrumentationWatcher* watcher = reinterpret_cast<IInstrumentationWatcher*>(nativePtr);
    if (watcher != NULL) {
        AutoPtr<IComponentName> elname;
        if (name != NULL) {
            if(!ElUtil::ToElComponentName(env, name, (IComponentName**)&elname)) {
                ALOGE("nativeInstrumentationStatus() ToElComponentName fail!");
            }
        }

        AutoPtr<IBundle> elresults;
        if (results != NULL) {
            if (!ElUtil::ToElBundle(env, results, (IBundle**)&elresults)) {
                ALOGE("nativeInstrumentationStatus() ToElBundle fail!");
            }
        }

        watcher->InstrumentationFinished(elname, (Int32)resultCode, elresults);
    }
}

static void android_app_ElInstrumentationWatcherProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jint nativePtr)
{
    // ALOGD("+ android_app_ElInstrumentationWatcherProxy_nativeFinalize()");

    IInstrumentationWatcher* obj = reinterpret_cast<IInstrumentationWatcher*>(nativePtr);
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_app_ElInstrumentationWatcherProxy_nativeFinalize()");
}

namespace android
{
static JNINativeMethod gMethods[] = {
    { "nativeInstrumentationStatus",     "(JLandroid/content/ComponentName;ILandroid/os/Bundle;)V",
            (void*) android_app_ElInstrumentationWatcherProxy_nativeInstrumentationStatus },
    { "nativeInstrumentationFinished",   "(JLandroid/content/ComponentName;ILandroid/os/Bundle;)V",
            (void*) android_app_ElInstrumentationWatcherProxy_nativeInstrumentationFinished },
    { "nativeFinalize",      "(J)V",
            (void*) android_app_ElInstrumentationWatcherProxy_nativeFinalize }
};

int register_android_app_ElInstrumentationWatcherProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/app/ElInstrumentationWatcherProxy",
        gMethods, NELEM(gMethods));
}

};