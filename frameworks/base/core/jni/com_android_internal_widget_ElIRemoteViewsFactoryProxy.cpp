#define LOG_TAG "ElIRemoteViewsFactoryProxyJNI"

#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Widget.h>

#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::Content::IIntent;
using Elastos::Droid::Internal::Widget::IIRemoteViewsFactory;
using Elastos::Droid::Widget::IRemoteViews;


static void com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeOnDataSetChanged(JNIEnv* env, jobject instance, jlong proxy){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;
	factory->OnDataSetChanged();
}

static void com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeOnDataSetChangedAsync(JNIEnv* env, jobject instance, jlong proxy){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;
	factory->OnDataSetChangedAsync();
}

static void com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeOnDestroy(JNIEnv* env, jobject instance, jlong proxy, jobject jintent){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;

	AutoPtr<IIntent> intent;
	ElUtil::ToElIntent(env, jintent, (IIntent**)&intent);
	factory->OnDestroy(intent);
}

static jint com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetCount(JNIEnv* env, jobject instance, jlong proxy){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;
	Int32 count = 0;
	factory->GetCount(&count);
	return count;
}

static jobject com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetViewAt(JNIEnv* env, jobject instance, jlong proxy, jint jposition){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;
	AutoPtr<IParcelable> parcel;
	factory->GetViewAt((Int32)jposition, (IParcelable**)&parcel);
	ALOGD("nativeGetViewAt(), remoteViews = %p", parcel.Get());
	AutoPtr<IRemoteViews> remoteViews = IRemoteViews::Probe(parcel.Get());
    jobject jremoteViews = NULL;

	if(remoteViews != NULL){
		jremoteViews = ElUtil::GetJavaRemoteViews(env, remoteViews);
	}else{
		ALOGE("nativeGetViewAt(), got RemoteViews obj null");
	}
	return jremoteViews;
}

static jobject com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetLoadingView(JNIEnv* env, jobject instance, jlong proxy){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;
	AutoPtr<IParcelable> parcel;
	factory->GetLoadingView((IParcelable**)&parcel);
	AutoPtr<IRemoteViews> remoteViews = IRemoteViews::Probe(parcel.Get());
    jobject jremoteViews = NULL;

	if(remoteViews != NULL){
		jremoteViews = ElUtil::GetJavaRemoteViews(env, remoteViews);
	}else{
		ALOGE("nativeGetLoadingView(), got RemoteViews obj null");
	}

	return jremoteViews;
}

static jint com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetViewTypeCount(JNIEnv* env, jobject instance, jlong proxy){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;
	Int32 count = 0;
	factory->GetViewTypeCount(&count);
	return count;
}

static jlong com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetItemId(JNIEnv* env, jobject instance, jlong proxy, jint jposition){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;
	Int64 id;
	factory->GetItemId(jposition, &id);
	return id;
}

static jboolean com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeHasStableIds(JNIEnv* env, jobject instance, jlong proxy){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;
	Boolean result;
	factory->HasStableIds(&result);
	return (jboolean)result;
}

static jboolean com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeIsCreated(JNIEnv* env, jobject instance, jlong proxy){
	IIRemoteViewsFactory* factory = (IIRemoteViewsFactory*)proxy;
	Boolean isCreated;
	factory->IsCreated(&isCreated);
	return (jboolean)isCreated;
}

static void com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeDestroy()");

    IIRemoteViewsFactory* obj = (IIRemoteViewsFactory*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeFinalize },
    { "nativeOnDataSetChanged", "(J)V",
        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeOnDataSetChanged },
    { "nativeOnDataSetChangedAsync", "(J)V",
        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeOnDataSetChangedAsync },
    { "nativeOnDestroy", "(JLandroid/content/Intent;)V",
        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeOnDestroy },
    { "nativeGetCount", "(J)I",
        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetCount },
    { "nativeGetViewAt", "(JI)Landroid/widget/RemoteViews;",
        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetViewAt },
	{ "nativeGetLoadingView", "(J)Landroid/widget/RemoteViews;",
	        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetLoadingView },
    { "nativeGetViewTypeCount", "(J)I",
        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetViewTypeCount },
    { "nativeGetItemId", "(JI)J",
        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeGetItemId },
    { "nativeHasStableIds", "(J)Z",
        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeHasStableIds },
    { "nativeIsCreated", "(J)Z",
        (void*) com_android_internal_widget_ElIRemoteViewsFactoryProxy_nativeIsCreated },
};

int register_com_android_internal_widget_ElIRemoteViewsFactoryProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/internal/widget/ElIRemoteViewsFactoryProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android
