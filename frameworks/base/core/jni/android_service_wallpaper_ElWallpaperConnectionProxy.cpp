
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Service.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <elastos/utility/etl/HashMap.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Content::IServiceConnection;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Os::IParcelFileDescriptor;
using Elastos::Droid::Service::Wallpaper::IIWallpaperConnection;
using Elastos::Droid::Service::Wallpaper::IIWallpaperEngine;
using Elastos::Droid::JavaProxy::CIWallpaperEngineNative;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, IIWallpaperEngine*> sWallPaperEngine;
static Mutex sWallPaperEngineLock;

static void android_service_wallpaper_ElWallpaperConnectionProxy_nativeAttachEngine(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jengine)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperConnectionProxy_nativeAttachEngine()");

    AutoPtr<IIWallpaperEngine> engine;
    if (jengine != NULL) {
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jengine);
        CIWallpaperEngineNative::New((Handle64)jvm, (Handle64)jInstance, (IIWallpaperEngine**)&engine);
        engine->AddRef();

        Int32 hashCode = ElUtil::GetJavaHashCode(env, jengine);
        Mutex::Autolock lock(sWallPaperEngineLock);
        sWallPaperEngine[hashCode] = engine;
    }

    IIWallpaperConnection* iwc = (IIWallpaperConnection*)jproxy;

    iwc->AttachEngine(engine);

    // ALOGD("- android_service_wallpaper_ElWallpaperConnectionProxy_nativeAttachEngine()");
}

static void android_service_wallpaper_ElWallpaperConnectionProxy_nativeEngineShown(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jengine)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperConnectionProxy_nativeEngineShown()");

    AutoPtr<IIWallpaperEngine> engine;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jengine);

    {
        Mutex::Autolock lock(sWallPaperEngineLock);
        if (sWallPaperEngine.Find(hashCode) != sWallPaperEngine.End()) {
            engine = sWallPaperEngine[hashCode];
            sWallPaperEngine.Erase(hashCode);
        }
    }

    if (NULL == engine) {
        ALOGE("nativeEngineShown() Invalid IIWallpaperEngine!\n");
        env->ExceptionDescribe();
    }

    IIWallpaperConnection* iwc = (IIWallpaperConnection*)jproxy;

    iwc->EngineShown(engine);

    engine->Release();

    // ALOGD("- android_service_wallpaper_ElWallpaperConnectionProxy_nativeEngineShown()");
}

static jobject android_service_wallpaper_ElWallpaperConnectionProxy_nativeSetWallpaper(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname)
{
    ALOGD("+ android_service_wallpaper_ElWallpaperConnectionProxy_nativeSetWallpaper()");

    String name = ElUtil::ToElString(env, jname);

    IIWallpaperConnection* iwc = (IIWallpaperConnection*)jproxy;

    AutoPtr<IParcelFileDescriptor> fd;
    ECode ec = iwc->SetWallpaper(name, (IParcelFileDescriptor**)&fd);
    ALOGE("nativeSetWallpaper() ec: 0x%08x", ec);
    jobject jfd = NULL;
    if (fd != NULL) {
        jfd = ElUtil::GetJavaParcelFileDescriptor(env, fd);
    }

    ALOGD("- android_service_wallpaper_ElWallpaperConnectionProxy_nativeSetWallpaper()");
    return jfd;
}

static void android_service_wallpaper_ElWallpaperConnectionProxy_nativeOnServiceConnected(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jname, jobject jservice)
{
    ALOGD("+ android_service_wallpaper_ElWallpaperConnectionProxy_nativeOnServiceConnected()");

    AutoPtr<IComponentName> name;
    if (jname != NULL) {
        if (!ElUtil::ToElComponentName(env, jname, (IComponentName**)&name)) {
            ALOGE("nativeOnServiceConnected() ToElComponentName fail!");
        }
    }

    AutoPtr<IBinder> service;
    if (jservice != NULL) {
        ALOGE("ElWallpaperConnectionProxy_nativeOnServiceConnected() jservice not NULL");
        jclass c = env->FindClass("java/lang/Object");

        jmethodID m = env->GetMethodID(c, "toString", "()Ljava/lang/String;");

        jstring jstr = (jstring)env->CallObjectMethod(jservice, m);

        String str = ElUtil::ToElString(env, jstr);
        ALOGE("ElWallpaperConnectionProxy_nativeOnServiceConnected() jservice not NULL: %s", str.string());

        env->DeleteLocalRef(c);
        env->DeleteLocalRef(jstr);
    }

    IIWallpaperConnection* iwc = (IIWallpaperConnection*)jproxy;
    IServiceConnection* isc = IServiceConnection::Probe(iwc);

    ECode ec = isc->OnServiceConnected(name, service);
    ALOGE("nativeOnServiceConnected() ec: 0x%08x", ec);

    ALOGD("- android_service_wallpaper_ElWallpaperConnectionProxy_nativeOnServiceConnected()");
}

static void android_service_wallpaper_ElWallpaperConnectionProxy_nativeOnServiceDisconnected(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jname)
{
    ALOGD("+ android_service_wallpaper_ElWallpaperConnectionProxy_nativeOnServiceDisconnected()");

    AutoPtr<IComponentName> name;
    if (name != NULL) {
        if (!ElUtil::ToElComponentName(env, jname, (IComponentName**)&name)) {
            ALOGE("nativeOnServiceConnected() ToElComponentName fail!");
        }
    }

    IIWallpaperConnection* iwc = (IIWallpaperConnection*)jproxy;
    IServiceConnection* isc = IServiceConnection::Probe(iwc);

    ECode ec = isc->OnServiceDisconnected(name);
    ALOGE("nativeOnServiceDisconnected() ec: 0x%08x", ec);

    ALOGD("- android_service_wallpaper_ElWallpaperConnectionProxy_nativeOnServiceDisconnected()");
}

static void android_service_wallpaper_ElWallpaperConnectionProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperConnectionProxy_nativeDestroy()");

    IIWallpaperConnection* obj = (IIWallpaperConnection*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_service_wallpaper_ElWallpaperConnectionProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_service_wallpaper_ElWallpaperConnectionProxy_nativeFinalize },
    { "nativeAttachEngine",    "(JLandroid/service/wallpaper/IWallpaperEngine;)V",
            (void*) android_service_wallpaper_ElWallpaperConnectionProxy_nativeAttachEngine },
    { "nativeEngineShown",    "(JLandroid/service/wallpaper/IWallpaperEngine;)V",
            (void*) android_service_wallpaper_ElWallpaperConnectionProxy_nativeEngineShown },
    { "nativeSetWallpaper",    "(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_service_wallpaper_ElWallpaperConnectionProxy_nativeSetWallpaper },
    { "nativeOnServiceConnected",    "(JLandroid/content/ComponentName;Landroid/os/IBinder;)V",
            (void*) android_service_wallpaper_ElWallpaperConnectionProxy_nativeOnServiceConnected },
    { "nativeOnServiceDisconnected",    "(JLandroid/content/ComponentName;)V",
            (void*) android_service_wallpaper_ElWallpaperConnectionProxy_nativeOnServiceDisconnected },
};

int register_android_service_wallpaper_ElWallpaperConnectionProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/service/wallpaper/ElWallpaperConnectionProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

