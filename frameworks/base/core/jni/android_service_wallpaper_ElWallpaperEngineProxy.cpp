
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Graphics.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.View.h>
#include <Elastos.Droid.Service.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Service::Wallpaper::IIWallpaperEngine;

static void android_service_wallpaper_ElWallpaperEngineProxy_nativeSetDesiredSize(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jwidth, jint jheight)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperEngineProxy_nativeSetDesiredSize()");

    IIWallpaperEngine* iwe = (IIWallpaperEngine*)jproxy;

    iwe->SetDesiredSize(jwidth, jheight);

    // ALOGD("- android_service_wallpaper_ElWallpaperEngineProxy_nativeSetDesiredSize()");
}

static void android_service_wallpaper_ElWallpaperEngineProxy_nativeSetDisplayPadding(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jpadding)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperEngineProxy_nativeSetDisplayPadding()");

    AutoPtr<IRect> padding;
    if (jpadding != NULL) {
        if (!ElUtil::ToElRect(env, jpadding, (IRect**)&padding))
            ALOGE("nativeSetDisplayPadding() ToElRect fail");
    }

    IIWallpaperEngine* iwe = (IIWallpaperEngine*)jproxy;

    iwe->SetDisplayPadding(padding);

    // ALOGD("- android_service_wallpaper_ElWallpaperEngineProxy_nativeSetDisplayPadding()");
}

static void android_service_wallpaper_ElWallpaperEngineProxy_nativeSetVisibility(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean jvisible)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperEngineProxy_nativeSetVisibility()");

    IIWallpaperEngine* iwe = (IIWallpaperEngine*)jproxy;

    iwe->SetVisibility(jvisible);

    // ALOGD("- android_service_wallpaper_ElWallpaperEngineProxy_nativeSetVisibility()");
}

static void android_service_wallpaper_ElWallpaperEngineProxy_nativeDispatchPointer(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jevent)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperEngineProxy_nativeDispatchPointer()");

    AutoPtr<IMotionEvent> event;
    if (jevent) {
        if (!ElUtil::ToElMotionEvent(env, jevent, (IMotionEvent**)&event)) {
            ALOGE("nativeDispatchPointer ToElMotionEvent failed!");
        }
    }
    IIWallpaperEngine* iwe = (IIWallpaperEngine*)jproxy;

    iwe->DispatchPointer(event);

    // ALOGD("- android_service_wallpaper_ElWallpaperEngineProxy_nativeDispatchPointer()");
}

static void android_service_wallpaper_ElWallpaperEngineProxy_nativeDispatchWallpaperCommand(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jaction, jint jx, jint jy, jint jz, jobject jextras)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperEngineProxy_nativeDispatchWallpaperCommand()");

    String action = ElUtil::ToElString(env, jaction);
    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("nativeDispatchWallpaperCommand ToElBundle failed!");
        }
    }
    IIWallpaperEngine* iwe = (IIWallpaperEngine*)jproxy;

    iwe->DispatchWallpaperCommand(action, jx, jy, jz, extras);

    // ALOGD("- android_service_wallpaper_ElWallpaperEngineProxy_nativeDispatchWallpaperCommand()");
}

static void android_service_wallpaper_ElWallpaperEngineProxy_nativeDestroy(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperEngineProxy_nativeDestroy()");

    IIWallpaperEngine* iwe = (IIWallpaperEngine*)jproxy;

    iwe->Destroy();

    // ALOGD("- android_service_wallpaper_ElWallpaperEngineProxy_nativeDestroy()");
}

static void android_service_wallpaper_ElWallpaperEngineProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperEngineProxy_nativeDestroy()");

    IIWallpaperEngine* obj = (IIWallpaperEngine*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_service_wallpaper_ElWallpaperEngineProxy_nativeDestroy()");
}

namespace android
{
static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_service_wallpaper_ElWallpaperEngineProxy_nativeFinalize },
    { "nativeSetDesiredSize",    "(JII)V",
            (void*) android_service_wallpaper_ElWallpaperEngineProxy_nativeSetDesiredSize },
    { "nativeSetDisplayPadding",    "(JLandroid/graphics/Rect;)V",
            (void*) android_service_wallpaper_ElWallpaperEngineProxy_nativeSetDisplayPadding },
    { "nativeSetVisibility",    "(JZ)V",
            (void*) android_service_wallpaper_ElWallpaperEngineProxy_nativeSetVisibility },
    { "nativeDispatchPointer",    "(JLandroid/view/MotionEvent;)V",
            (void*) android_service_wallpaper_ElWallpaperEngineProxy_nativeDispatchPointer },
    { "nativeDispatchWallpaperCommand",    "(JLjava/lang/String;IIILandroid/os/Bundle;)V",
            (void*) android_service_wallpaper_ElWallpaperEngineProxy_nativeDispatchWallpaperCommand },
    { "nativeDestroy",    "(J)V",
            (void*) android_service_wallpaper_ElWallpaperEngineProxy_nativeDestroy },
};

int register_android_service_wallpaper_ElWallpaperEngineProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/service/wallpaper/ElWallpaperEngineProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

