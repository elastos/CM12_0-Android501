
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Database.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Database::IDataSetObserver;

static void android_database_ElDataSetObserverProxy_naitveOnChanged(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElDataSetObserverProxy_naitveOnChanged()");

    IDataSetObserver* observer = (IDataSetObserver*)jproxy;
    observer->OnChanged();

    // ALOGD("- android_database_ElDataSetObserverProxy_naitveOnChanged()");
}

static void android_database_ElDataSetObserverProxy_naitveOnInvalidated(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElDataSetObserverProxy_naitveOnInvalidated()");

    IDataSetObserver* observer = (IDataSetObserver*)jproxy;
    observer->OnInvalidated();

    // ALOGD("- android_database_ElDataSetObserverProxy_naitveOnInvalidated()");
}

static void android_content_ElDataSetObserverProxy_nativeFinalize(
    JNIEnv* env, jobject clazz,jlong jproxy)
{
    // ALOGD("+ android_content_ElDataSetObserverProxy_nativeDestroy()");

    IDataSetObserver* obj = (IDataSetObserver*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_content_ElDataSetObserverProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_content_ElDataSetObserverProxy_nativeFinalize },
    { "naitveOnChanged",    "(J)V",
            (void*) android_database_ElDataSetObserverProxy_naitveOnChanged },
    { "nativeOnInvalidated",    "(J)V",
            (void*) android_database_ElDataSetObserverProxy_naitveOnInvalidated },
};

int register_android_database_ElDataSetObserverProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/database/ElDataSetObserverProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

