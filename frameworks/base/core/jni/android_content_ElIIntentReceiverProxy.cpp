
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Content::IIntent;
using Elastos::Droid::Content::IIntentReceiver;

static void android_content_ElIIntentReceiverProxy_nativePerformReceive(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jintent, jint jresultCode, jstring jdata, jobject jextras, jboolean jordered, jboolean jsticky, jint jsendingUser)
{
    // ALOGD("+ android_content_ElIIntentReceiverProxy_nativePerformReceive()");

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativePerformReceive ToElIntent fail!");
        }
    }

    String data = ElUtil::ToElString(env, jdata);

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("nativePerformReceive ToElBundle fail!");
        }
    }

    IIntentReceiver* receiver = (IIntentReceiver*)jproxy;
    receiver->PerformReceive(intent, (Int32)jresultCode, data, extras, (Boolean)jordered, (Boolean)jsticky, (Int32)jsendingUser);

    // ALOGD("- android_content_ElIIntentReceiverProxy_nativePerformReceive()");
}

static void android_content_ElIIntentReceiverProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jint token)
{
    // ALOGD("+ android_content_ElIIntentReceiverProxy_nativeFinalize()");

    IIntentReceiver* osb = (IIntentReceiver*)token;
    if (osb != NULL) {
        osb->Release();
    }

    // ALOGD("- android_content_ElIIntentReceiverProxy_nativeFinalize()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_content_ElIIntentReceiverProxy_nativeFinalize },
    { "nativePerformReceive",    "(JLandroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V",
            (void*) android_content_ElIIntentReceiverProxy_nativePerformReceive },
};

int register_android_content_ElIIntentReceiverProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/content/ElIIntentReceiverProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

