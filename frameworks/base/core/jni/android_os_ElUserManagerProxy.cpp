
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Graphics.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.CoreLibrary.Utility.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Os::IIUserManager;

static jint android_os_ElUserManagerProxy_nativeGetUserSerialNumber(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetUserSerialNumber()");

    IIUserManager* um = (IIUserManager*)jproxy;

    Int32 serialNo = 0;
    um->GetUserSerialNumber((Int32)juserHandle, &serialNo);

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetUserSerialNumber()");
    return (jint)serialNo;
}

static jobject android_os_ElUserManagerProxy_nativeCreateUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname, jint jflags)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeCreateUser()");

    String name = ElUtil::ToElString(env, jname);

    IIUserManager* um = (IIUserManager*)jproxy;
    AutoPtr<IUserInfo> user;
    ECode ec = um->CreateUser(name, (Int32)jflags, (IUserInfo**)&user);
    if (FAILED(ec))
        ALOGE("nativeCreateUser() ec: 0x%08x", ec);

    jobject juser = NULL;
    if (user != NULL) {
        juser = ElUtil::GetJavaUserInfo(env, user);
    }

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetUserSerialNumber()");
    return juser;
}

static jobject android_os_ElUserManagerProxy_nativeCreateProfileForUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname, jint jflags, jint juserHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeCreateProfileForUser()");

    String name = ElUtil::ToElString(env, jname);

    IIUserManager* um = (IIUserManager*)jproxy;
    AutoPtr<IUserInfo> user;
    ECode ec = um->CreateProfileForUser(name, (Int32)jflags, juserHandle, (IUserInfo**)&user);
    if (FAILED(ec))
        ALOGE("nativeCreateUser() ec: 0x%08x", ec);

    jobject juser = NULL;
    if (user != NULL) {
        juser = ElUtil::GetJavaUserInfo(env, user);
    }

    // ALOGD("- android_os_ElUserManagerProxy_nativeCreateProfileForUser()");
    return juser;
}

static void android_os_ElUserManagerProxy_nativeSetUserEnabled(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeSetUserEnabled()");

    IIUserManager* um = (IIUserManager*)jproxy;
    ECode ec = um->SetUserEnabled((Int32)juserHandle);
    if (FAILED(ec))
        ALOGE("nativeSetUserEnabled() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeSetUserEnabled()");
}

static jint android_os_ElUserManagerProxy_nativeRemoveUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeRemoveUser()");

    IIUserManager* um = (IIUserManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = um->RemoveUser((Int32)juserHandle, &result);
    if (FAILED(ec))
        ALOGE("nativeRemoveUser() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeRemoveUser()");
    return (jboolean)result;
}

static void android_os_ElUserManagerProxy_nativeSetUserName(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle, jstring jname)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeSetUserName()");

    String name = ElUtil::ToElString(env, jname);

    IIUserManager* um = (IIUserManager*)jproxy;
    ECode ec = um->SetUserName((Int32)juserHandle, name);
    if (FAILED(ec))
        ALOGE("nativeSetUserName() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeSetUserName()");
}

static void android_os_ElUserManagerProxy_nativeSetUserIcon(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle, jobject jicon)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeSetUserIcon()");

    AutoPtr<IBitmap> icon;
    if (jicon != NULL) {
        if (!ElUtil::ToElBitmap(env, jicon, (IBitmap**)&icon)) {
            ALOGE("nativeSetUserIcon() ToElBitmap Fail!");
        }
    }

    IIUserManager* um = (IIUserManager*)jproxy;
    ECode ec = um->SetUserIcon((Int32)juserHandle, icon);
    if (FAILED(ec))
        ALOGE("nativeSetUserIcon() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeSetUserIcon()");
}

static jobject android_os_ElUserManagerProxy_nativeGetUserIcon(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetUserIcon()");

    IIUserManager* um = (IIUserManager*)jproxy;
    AutoPtr<IBitmap> icon;
    ECode ec = um->GetUserIcon((Int32)juserHandle, (IBitmap**)&icon);
    if (FAILED(ec))
        ALOGE("nativeGetUserIcon() ec: 0x%08x", ec);

    jobject jicon = NULL;
    if (icon != NULL) {
        jicon = ElUtil::GetJavaBitmap(env, icon);
    }

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetUserIcon()");
    return jicon;
}

static jobject android_os_ElUserManagerProxy_nativeGetUsers(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jexcludeDying)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetUsers()");

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetUsers Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetUsers Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jusers = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetUsers Fail NewObject: ArrayList : %d!\n", __LINE__);

    IIUserManager* um = (IIUserManager*)jproxy;

    AutoPtr<IList> users;
    um->GetUsers((Boolean)jexcludeDying, (IList**)&users);
    if (users != NULL) {
        Int32 size = 0;
        users->GetSize(&size);
        if (size > 0) {
            jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
            ElUtil::CheckErrorAndLog(env, "nativeGetUsers GetMethodID: add : %d!\n", __LINE__);

            for (Int32 i = 0; i < size; i++) {
                AutoPtr<IInterface> obj;
                users->Get(i, (IInterface**)&obj);
                AutoPtr<IUserInfo> user = IUserInfo::Probe(obj);

                jobject juser = ElUtil::GetJavaUserInfo(env, user);

                env->CallBooleanMethod(jusers, mAdd, juser);
                ElUtil::CheckErrorAndLog(env, "nativeGetUsers CallBooleanMethod: add : %d!\n", __LINE__);

                env->DeleteLocalRef(juser);
            }
        }
    }

    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetUsers()");
    return jusers;
}

static jobject android_os_ElUserManagerProxy_nativeGetProfiles(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle, jboolean enabledOnly)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetProfiles()");

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetProfiles Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetProfiles Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jusers = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetProfiles Fail NewObject: ArrayList : %d!\n", __LINE__);

    IIUserManager* um = (IIUserManager*)jproxy;

    AutoPtr<IList> users;
    um->GetProfiles(juserHandle, enabledOnly, (IList**)&users);
    if (users != NULL) {
        Int32 size = 0;
        users->GetSize(&size);
        if (size > 0) {
            jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
            ElUtil::CheckErrorAndLog(env, "nativeGetProfiles GetMethodID: add : %d!\n", __LINE__);

            for (Int32 i = 0; i < size; i++) {
                AutoPtr<IInterface> obj;
                users->Get(i, (IInterface**)&obj);
                AutoPtr<IUserInfo> user = IUserInfo::Probe(obj);

                jobject juser = ElUtil::GetJavaUserInfo(env, user);

                env->CallBooleanMethod(jusers, mAdd, juser);
                ElUtil::CheckErrorAndLog(env, "nativeGetProfiles CallBooleanMethod: add : %d!\n", __LINE__);

                env->DeleteLocalRef(juser);
            }
        }
    }

    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetProfiles()");
    return jusers;
}

static jobject android_os_ElUserManagerProxy_nativeGetProfileParent(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetProfileParent()");

    IIUserManager* um = (IIUserManager*)jproxy;
    AutoPtr<IUserInfo> info;
    ECode ec = um->GetProfileParent((Int32)juserHandle, (IUserInfo**)&info);
    if (FAILED(ec))
        ALOGE("nativeGetProfileParent() ec: 0x%08x", ec);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaUserInfo(env, info);
    }

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetProfileParent()");
    return jinfo;
}

static jobject android_os_ElUserManagerProxy_nativeGetUserInfo(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetUserInfo()");

    IIUserManager* um = (IIUserManager*)jproxy;
    AutoPtr<IUserInfo> info;
    ECode ec = um->GetUserInfo((Int32)juserHandle, (IUserInfo**)&info);
    if (FAILED(ec))
        ALOGE("nativeGetUserInfo() ec: 0x%08x", ec);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaUserInfo(env, info);
    }

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetUserInfo()");
    return jinfo;
}

static jboolean android_os_ElUserManagerProxy_nativeIsRestricted(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jenable)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeIsRestricted()");

    IIUserManager* um = (IIUserManager*)jproxy;
    Boolean isEnabled = FALSE;
    ECode ec = um->IsRestricted(&isEnabled);
    if (FAILED(ec))
        ALOGE("nativeIsRestricted() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeIsRestricted()");
    return (jboolean)isEnabled;
}

static jint android_os_ElUserManagerProxy_nativeGetUserHandle(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserSerialNumber)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetUserHandle()");

    IIUserManager* um = (IIUserManager*)jproxy;
    Int32 userHandle = 0;
    ECode ec = um->GetUserHandle((Int32)juserSerialNumber, &userHandle);
    if (FAILED(ec))
        ALOGE("nativeGetUserHandle() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetUserHandle()");
    return (jint)userHandle;
}

static jobject android_os_ElUserManagerProxy_nativeGetUserRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy, jint userHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetUserRestrictions()");

    IIUserManager* um = (IIUserManager*)jproxy;
    AutoPtr<IBundle> bundle;
    ECode ec = um->GetUserRestrictions(userHandle, (IBundle**)&bundle);
    if (FAILED(ec))
        ALOGE("nativeGetUserRestrictions ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetUserRestrictions()");
    return ElUtil::GetJavaBundle(env, bundle);
}

static jboolean android_os_ElUserManagerProxy_nativeHasUserRestriction(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jrestrictionKey, jint userHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeHasUserRestriction()");

    String restrictionKey = ElUtil::ToElString(env, jrestrictionKey);

    IIUserManager* um = (IIUserManager*)jproxy;
    Boolean result;
    ECode ec = um->HasUserRestriction(restrictionKey, userHandle, &result);
    if (FAILED(ec))
        ALOGE("nativeHasUserRestriction ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeHasUserRestriction()");
    return result;
}

static void android_os_ElUserManagerProxy_nativeSetUserRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrestrictions, jint userHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeSetUserRestrictions()");

    AutoPtr<IBundle> restrictions;
    if (jrestrictions != NULL) {
        if (!ElUtil::ToElBundle(env, jrestrictions, (IBundle**)&restrictions))
            ALOGE("nativeSetUserRestrictions: ToElBundle fail");
    }
    IIUserManager* um = (IIUserManager*)jproxy;
    ECode ec = um->SetUserRestrictions(restrictions, userHandle);
    if (FAILED(ec))
        ALOGE("nativeSetUserRestrictions ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeSetUserRestrictions()");
}

static void android_os_ElUserManagerProxy_nativeSetApplicationRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jobject jrestrictions, jint userHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeSetApplicationRestrictions()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IBundle> restrictions;
    if (jrestrictions != NULL) {
        if (!ElUtil::ToElBundle(env, jrestrictions, (IBundle**)&restrictions))
            ALOGE("nativeSetApplicationRestrictions: ToElBundle fail");
    }
    IIUserManager* um = (IIUserManager*)jproxy;
    ECode ec = um->SetApplicationRestrictions(packageName, restrictions, userHandle);
    if (FAILED(ec))
        ALOGE("nativeSetApplicationRestrictions ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeSetApplicationRestrictions()");
}

static jobject android_os_ElUserManagerProxy_nativeGetApplicationRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetApplicationRestrictions()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIUserManager* um = (IIUserManager*)jproxy;
    AutoPtr<IBundle> bundle;
    ECode ec = um->GetApplicationRestrictions(packageName, (IBundle**)&bundle);
    if (FAILED(ec))
        ALOGE("nativeGetApplicationRestrictions ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetApplicationRestrictions()");
    return ElUtil::GetJavaBundle(env, bundle);
}

static jobject android_os_ElUserManagerProxy_nativeGetApplicationRestrictionsForUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jint userHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetApplicationRestrictionsForUser()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIUserManager* um = (IIUserManager*)jproxy;
    AutoPtr<IBundle> bundle;
    ECode ec = um->GetApplicationRestrictionsForUser(packageName, userHandle, (IBundle**)&bundle);
    if (FAILED(ec))
        ALOGE("nativeGetApplicationRestrictionsForUser ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetApplicationRestrictionsForUser()");
    return ElUtil::GetJavaBundle(env, bundle);
}

static jboolean android_os_ElUserManagerProxy_nativeSetRestrictionsChallenge(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jnewPin)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeSetRestrictionsChallenge()");

    String newPin = ElUtil::ToElString(env, jnewPin);

    IIUserManager* um = (IIUserManager*)jproxy;
    Boolean result;
    ECode ec = um->SetRestrictionsChallenge(newPin, &result);
    if (FAILED(ec))
        ALOGE("nativeSetRestrictionsChallenge ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeSetRestrictionsChallenge()");
    return result;
}

static jint android_os_ElUserManagerProxy_nativeCheckRestrictionsChallenge(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpin)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeCheckRestrictionsChallenge()");

    String pin = ElUtil::ToElString(env, jpin);

    IIUserManager* um = (IIUserManager*)jproxy;
    Int32 result;
    ECode ec = um->CheckRestrictionsChallenge(pin, &result);
    if (FAILED(ec))
        ALOGE("nativeCheckRestrictionsChallenge ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeCheckRestrictionsChallenge()");
    return result;
}

static jboolean android_os_ElUserManagerProxy_nativeHasRestrictionsChallenge(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeHasRestrictionsChallenge()");

    IIUserManager* um = (IIUserManager*)jproxy;
    Boolean result;
    ECode ec = um->HasRestrictionsChallenge(&result);
    if (FAILED(ec))
        ALOGE("nativeHasRestrictionsChallenge ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeHasRestrictionsChallenge()");
    return result;
}

static void android_os_ElUserManagerProxy_nativeRemoveRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeRemoveRestrictions()");

    IIUserManager* um = (IIUserManager*)jproxy;
    ECode ec = um->RemoveRestrictions();
    if (FAILED(ec))
        ALOGE("nativeRemoveRestrictions ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeRemoveRestrictions()");
}

static void android_os_ElUserManagerProxy_nativeSetDefaultGuestRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrestrictions)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeSetDefaultGuestRestrictions()");

    AutoPtr<IBundle> restrictions;
    if (jrestrictions != NULL) {
        if (!ElUtil::ToElBundle(env, jrestrictions, (IBundle**)&restrictions))
            ALOGE("nativeSetDefaultGuestRestrictions: ToElBundle fail");
    }
    IIUserManager* um = (IIUserManager*)jproxy;
    ECode ec = um->SetDefaultGuestRestrictions(restrictions);
    if (FAILED(ec))
        ALOGE("nativeSetDefaultGuestRestrictions ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeSetDefaultGuestRestrictions()");
}

static jobject android_os_ElUserManagerProxy_nativeGetDefaultGuestRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeGetDefaultGuestRestrictions()");

    IIUserManager* um = (IIUserManager*)jproxy;
    AutoPtr<IBundle> bundle;
    ECode ec = um->GetDefaultGuestRestrictions((IBundle**)&bundle);
    if (FAILED(ec))
        ALOGE("nativeGetDefaultGuestRestrictions ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeGetDefaultGuestRestrictions()");
    return ElUtil::GetJavaBundle(env, bundle);
}

static jboolean android_os_ElUserManagerProxy_nativeMarkGuestForDeletion(
    JNIEnv* env, jobject clazz, jlong jproxy, jint userHandle)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeMarkGuestForDeletion()");

    IIUserManager* um = (IIUserManager*)jproxy;
    Boolean result;
    ECode ec = um->MarkGuestForDeletion(userHandle, &result);
    if (FAILED(ec))
        ALOGE("nativeMarkGuestForDeletion ec: 0x%08x", ec);

    // ALOGD("- android_os_ElUserManagerProxy_nativeMarkGuestForDeletion()");
    return result;
}

static void android_os_ElUserManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_os_ElUserManagerProxy_nativeDestroy()");

    IIUserManager* obj = (IIUserManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_os_ElUserManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_os_ElUserManagerProxy_nativeFinalize },
    { "nativeGetUserSerialNumber",    "(JI)I",
            (void*) android_os_ElUserManagerProxy_nativeGetUserSerialNumber },
    { "nativeCreateUser",    "(JLjava/lang/String;I)Landroid/content/pm/UserInfo;",
            (void*) android_os_ElUserManagerProxy_nativeCreateUser },
    { "nativeCreateProfileForUser",    "(JLjava/lang/String;II)Landroid/content/pm/UserInfo;",
            (void*) android_os_ElUserManagerProxy_nativeCreateProfileForUser },
    { "nativeSetUserEnabled",    "(JI)V",
            (void*) android_os_ElUserManagerProxy_nativeSetUserEnabled },
    { "nativeRemoveUser",    "(JI)Z",
            (void*) android_os_ElUserManagerProxy_nativeRemoveUser },
    { "nativeSetUserName",    "(JILjava/lang/String;)V",
            (void*) android_os_ElUserManagerProxy_nativeSetUserName },
    { "nativeSetUserIcon",    "(JILandroid/graphics/Bitmap;)V",
            (void*) android_os_ElUserManagerProxy_nativeSetUserIcon },
    { "nativeGetUserIcon",    "(JI)Landroid/graphics/Bitmap;",
            (void*) android_os_ElUserManagerProxy_nativeGetUserIcon },
    { "nativeGetUsers",    "(JZ)Ljava/util/List;",
            (void*) android_os_ElUserManagerProxy_nativeGetUsers },
    { "nativeGetProfiles",    "(JIZ)Ljava/util/List;",
            (void*) android_os_ElUserManagerProxy_nativeGetProfiles },
    { "nativeGetProfileParent",    "(JI)Landroid/content/pm/UserInfo;",
            (void*) android_os_ElUserManagerProxy_nativeGetProfileParent },
    { "nativeGetUserInfo",    "(JI)Landroid/content/pm/UserInfo;",
            (void*) android_os_ElUserManagerProxy_nativeGetUserInfo },
    { "nativeIsRestricted",    "(J)Z",
            (void*) android_os_ElUserManagerProxy_nativeIsRestricted },
    { "nativeGetUserHandle",    "(JI)I",
            (void*) android_os_ElUserManagerProxy_nativeGetUserHandle },
    { "nativeGetUserRestrictions",    "(JI)Landroid/os/Bundle;",
            (void*) android_os_ElUserManagerProxy_nativeGetUserRestrictions },
    { "nativeHasUserRestriction",    "(JLjava/lang/String;I)Z",
            (void*) android_os_ElUserManagerProxy_nativeHasUserRestriction },
    { "nativeSetUserRestrictions",    "(JLandroid/os/Bundle;I)V",
            (void*) android_os_ElUserManagerProxy_nativeSetUserRestrictions },
    { "nativeSetApplicationRestrictions",    "(JLjava/lang/String;Landroid/os/Bundle;I)V",
            (void*) android_os_ElUserManagerProxy_nativeSetApplicationRestrictions },
    { "nativeGetApplicationRestrictions",    "(JLjava/lang/String;)Landroid/os/Bundle;",
            (void*) android_os_ElUserManagerProxy_nativeGetApplicationRestrictions },
    { "nativeGetApplicationRestrictionsForUser",    "(JLjava/lang/String;I)Landroid/os/Bundle;",
            (void*) android_os_ElUserManagerProxy_nativeGetApplicationRestrictionsForUser },
    { "nativeSetRestrictionsChallenge",    "(JLjava/lang/String;)Z",
            (void*) android_os_ElUserManagerProxy_nativeSetRestrictionsChallenge },
    { "nativeCheckRestrictionsChallenge",    "(JLjava/lang/String;)I",
            (void*) android_os_ElUserManagerProxy_nativeCheckRestrictionsChallenge },
    { "nativeHasRestrictionsChallenge",    "(J)Z",
            (void*) android_os_ElUserManagerProxy_nativeHasRestrictionsChallenge },
    { "nativeRemoveRestrictions",    "(J)V",
            (void*) android_os_ElUserManagerProxy_nativeRemoveRestrictions },
    { "nativeSetDefaultGuestRestrictions",    "(JLandroid/os/Bundle;)V",
            (void*) android_os_ElUserManagerProxy_nativeSetDefaultGuestRestrictions },
    { "nativeGetDefaultGuestRestrictions",    "(J)Landroid/os/Bundle;",
            (void*) android_os_ElUserManagerProxy_nativeGetDefaultGuestRestrictions },
    { "nativeMarkGuestForDeletion",    "(JI)Z",
            (void*) android_os_ElUserManagerProxy_nativeMarkGuestForDeletion },
};

int register_android_os_ElUserManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/os/ElUserManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

