
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::App::Backup::IIBackupManager;
using Elastos::Droid::App::Backup::IIFullBackupRestoreObserver;
using Elastos::Droid::App::Backup::IIRestoreSession;
using Elastos::Droid::Content::IIntent;
using Elastos::Droid::JavaProxy::CIBackupAgentNative;
using Elastos::Droid::JavaProxy::CIFullBackupRestoreObserverNative;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Os::IParcelFileDescriptor;

static void android_app_backup_ElBackupManagerProxy_nativeDataChanged(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpackageName)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeDataChanged()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    bm->DataChanged(packageName);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeDataChanged()");
}

static void android_app_backup_ElBackupManagerProxy_nativeClearBackupData(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtransportName, jstring jpackageName)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeClearBackupData()");

    String transportName = ElUtil::ToElString(env, jtransportName);
    String packageName = ElUtil::ToElString(env, jpackageName);

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    bm->ClearBackupData(transportName, packageName);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeClearBackupData()");
}

static void android_app_backup_ElBackupManagerProxy_nativeAgentConnected(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpackageName, jobject jagent)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeAgentConnected()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IBinder> agent;
    if (jagent != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jagent);

        jclass ibaClass = env->FindClass("android/app/IBackupAgent");
        ElUtil::CheckErrorAndLog(env, "FindClass IBackupAgent: %d!", __LINE__);

        if (env->IsInstanceOf(jagent, ibaClass)) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jagent);
            CIBackupAgentNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&agent);
        } else {
            ALOGE("nativeAgentConnected() jagent NOT IBackupAgent!");
        }

        env->DeleteLocalRef(ibaClass);
    }

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    ECode ec = bm->AgentConnected(packageName, agent);
    if (FAILED(ec))
        ALOGE("nativeAgentConnected() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeAgentConnected()");
}

static void android_app_backup_ElBackupManagerProxy_nativeAgentDisconnected(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpackageName)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeAgentDisconnected()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    ECode ec = bm->AgentDisconnected(packageName);
    if (FAILED(ec))
        ALOGE("nativeAgentDisconnected() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeAgentDisconnected()");
}

static void android_app_backup_ElBackupManagerProxy_nativeRestoreAtInstall(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpackageName, jint jtoken)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeRestoreAtInstall()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    ECode ec = bm->RestoreAtInstall(packageName, (Int32)jtoken);
    if (FAILED(ec))
        ALOGE("nativeRestoreAtInstall() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeRestoreAtInstall()");
}

static void android_app_backup_ElBackupManagerProxy_nativeSetBackupEnabled(JNIEnv* env, jobject clazz, jlong jproxy,
        jboolean jisEnabled)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeSetBackupEnabled()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    bm->SetBackupEnabled((Boolean)jisEnabled);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeSetBackupEnabled()");
}

static void android_app_backup_ElBackupManagerProxy_nativeSetAutoRestore(JNIEnv* env, jobject clazz, jlong jproxy,
        jboolean jdoAutoRestore)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeSetAutoRestore()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    ECode ec = bm->SetAutoRestore((Boolean)jdoAutoRestore);
    if (FAILED(ec))
        ALOGE("nativeSetAutoRestore() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeSetAutoRestore()");
}

static void android_app_backup_ElBackupManagerProxy_nativeSetBackupProvisioned(JNIEnv* env, jobject clazz, jlong jproxy,
        jboolean jisProvisioned)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeSetBackupProvisioned()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    ECode ec = bm->SetBackupProvisioned((Boolean)jisProvisioned);
    if (FAILED(ec))
        ALOGE("nativeSetBackupProvisioned() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeSetBackupProvisioned()");
}

static jboolean android_app_backup_ElBackupManagerProxy_nativeIsBackupEnabled(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeIsBackupEnabled()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    Boolean isEnabled = FALSE;
    ECode ec = bm->IsBackupEnabled(&isEnabled);
    if (FAILED(ec))
        ALOGE("nativeIsBackupEnabled() ec = 0x%08x isEnabled = %d", ec, isEnabled);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeIsBackupEnabled()");
    return (jboolean)isEnabled;
}

static jboolean android_app_backup_ElBackupManagerProxy_nativeSetBackupPassword(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jcurrentPw, jstring jnewPw)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeSetBackupPassword()");

    String currentPw = ElUtil::ToElString(env, jcurrentPw);
    String newPw = ElUtil::ToElString(env, jnewPw);

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    Boolean result = FALSE;
    ECode ec = bm->SetBackupPassword(currentPw, newPw, &result);
    if (FAILED(ec))
        ALOGE("nativeSetBackupPassword() ec = 0x%08x result = %d", ec, result);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeSetBackupPassword()");
    return (jboolean)result;
}

static jboolean android_app_backup_ElBackupManagerProxy_nativeHasBackupPassword(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeHasBackupPassword()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    Boolean result = FALSE;
    ECode ec = bm->HasBackupPassword(&result);
    if (FAILED(ec))
        ALOGE("nativeHasBackupPassword() ec = 0x%08x result = %d", ec, result);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeHasBackupPassword()");
    return (jboolean)result;
}

static void android_app_backup_ElBackupManagerProxy_nativeBackupNow(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeBackupNow()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    bm->BackupNow();

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeBackupNow()");
}

static void android_app_backup_ElBackupManagerProxy_nativeFullBackup(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jfd, jboolean jincludeApks, jboolean jincludeObbs, jboolean jincludeShared, jboolean jdoWidgets,
    jboolean jallApps, jboolean jallIncludesSystem, jboolean jdoCompress, jobjectArray jpackageNames)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeFullBackup()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    AutoPtr<IParcelFileDescriptor> fd;
    if (jfd != NULL) {
        if (!ElUtil::ToElParcelFileDescriptor(env, jfd, (IParcelFileDescriptor**)&fd)) {
            ALOGE("nativeFullBackup() ToElParcelFileDescriptor fail!");
        }
    }

    AutoPtr<ArrayOf<String> > packageNames;
    if (jpackageNames != NULL) {
        if (!ElUtil::ToElStringArray(env, jpackageNames, (ArrayOf<String>**)&packageNames)) {
            ALOGE("nativeFullBackup() ToElStringArray fail!");
        }
    }

    ECode ec = bm->FullBackup(fd, jincludeApks, jincludeObbs, jincludeShared, jdoWidgets, jallApps,
            jallIncludesSystem, jdoCompress, packageNames);
    if (FAILED(ec))
        ALOGE("nativeFullBackup() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeFullBackup()");
}

static void android_app_backup_ElBackupManagerProxy_nativeFullTransportBackup(JNIEnv* env, jobject clazz, jlong jproxy,
    jobjectArray jpackageNames)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeFullTransportBackup()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    AutoPtr<ArrayOf<String> > packageNames;
    if (jpackageNames != NULL) {
        if (!ElUtil::ToElStringArray(env, jpackageNames, (ArrayOf<String>**)&packageNames)) {
            ALOGE("nativeFullTransportBackup() ToElStringArray fail!");
        }
    }

    ECode ec = bm->FullTransportBackup(packageNames);
    if (FAILED(ec))
        ALOGE("nativeFullTransportBackup() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeFullTransportBackup()");
}

static void android_app_backup_ElBackupManagerProxy_nativeFullBackupNoninteractive(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jfd, jobjectArray jdomainTokens, jstring jexcludeFilesRegex, jstring jpackageName,
    jboolean jshouldKillAfterBackup, jboolean jignoreEncryptionPasswordCheck, jobject jobserver)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeFullBackupNoninteractive()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    AutoPtr<IParcelFileDescriptor> fd;
    if (jfd != NULL) {
        if (!ElUtil::ToElParcelFileDescriptor(env, jfd, (IParcelFileDescriptor**)&fd)) {
            ALOGE("nativeFullBackupNoninteractive() ToElParcelFileDescriptor fail!");
        }
    }

    AutoPtr<ArrayOf<String> > domainTokens;
    if (jdomainTokens != NULL) {
        if (!ElUtil::ToElStringArray(env, jdomainTokens, (ArrayOf<String>**)&domainTokens)) {
            ALOGE("nativeFullBackupNoninteractive() ToElStringArray fail!");
        }
    }

    String excludeFilesRegex = ElUtil::ToElString(env, jexcludeFilesRegex);
    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIFullBackupRestoreObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        if(NOERROR != CIFullBackupRestoreObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIFullBackupRestoreObserver**)&observer)) {
            ALOGE("nativeFullBackupNoninteractive new CIFullBackupRestoreObserverNative fail!\n");
        }
    }

    ECode ec = bm->FullBackupNoninteractive(fd, domainTokens, excludeFilesRegex, packageName,
        jshouldKillAfterBackup, jignoreEncryptionPasswordCheck, observer);
    if (FAILED(ec))
        ALOGE("nativeFullBackupNoninteractive() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeFullBackupNoninteractive()");
}

static void android_app_backup_ElBackupManagerProxy_nativeFullRestore(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jfd)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeFullRestore()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    AutoPtr<IParcelFileDescriptor> fd;
    if (jfd != NULL) {
        if (!ElUtil::ToElParcelFileDescriptor(env, jfd, (IParcelFileDescriptor**)&fd)) {
            ALOGE("nativeFullRestore() ToElParcelFileDescriptor fail!");
        }
    }

    ECode ec = bm->FullRestore(fd);
    if (FAILED(ec))
        ALOGE("nativeFullRestore() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeFullRestore()");
}

static void android_app_backup_ElBackupManagerProxy_nativeFullRestoreNoninteractive(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jfd, jboolean jignoreEncryptionPasswordCheck, jobject jobserver)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeFullRestoreNoninteractive()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    AutoPtr<IParcelFileDescriptor> fd;
    if (jfd != NULL) {
        if (!ElUtil::ToElParcelFileDescriptor(env, jfd, (IParcelFileDescriptor**)&fd)) {
            ALOGE("nativeFullRestoreNoninteractive() ToElParcelFileDescriptor fail!");
        }
    }

    AutoPtr<IIFullBackupRestoreObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        if(NOERROR != CIFullBackupRestoreObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIFullBackupRestoreObserver**)&observer)) {
            ALOGE("nativeFullBackupNoninteractive new CIFullBackupRestoreObserverNative fail!\n");
        }
    }

    ECode ec = bm->FullRestoreNoninteractive(fd, jignoreEncryptionPasswordCheck, observer);
    if (FAILED(ec))
        ALOGE("nativeFullRestoreNoninteractive() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeFullRestoreNoninteractive()");
}

static void android_app_backup_ElBackupManagerProxy_nativeAcknowledgeFullBackupOrRestore(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtoken, jboolean jallow, jstring jcurPassword, jstring jencryptionPassword, jobject jobserver)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeAcknowledgeFullBackupOrRestore()");

    String curPassword = ElUtil::ToElString(env, jcurPassword);
    String encryptionPassword = ElUtil::ToElString(env, jencryptionPassword);

    AutoPtr<IIFullBackupRestoreObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        if(NOERROR != CIFullBackupRestoreObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIFullBackupRestoreObserver**)&observer)) {
            ALOGE("nativeAcknowledgeFullBackupOrRestore new CIFullBackupRestoreObserverNative fail!\n");
        }
    }

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    ECode ec = bm->AcknowledgeFullBackupOrRestore((Int32)jtoken, (Boolean)jallow, curPassword, encryptionPassword, observer);
    if (FAILED(ec))
        ALOGE("nativeAcknowledgeFullBackupOrRestore() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeAcknowledgeFullBackupOrRestore()");
}

static jstring android_app_backup_ElBackupManagerProxy_nativeGetCurrentTransport(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeGetCurrentTransport()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    String transport;
    bm->GetCurrentTransport(&transport);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeGetCurrentTransport()");
    return ElUtil::GetJavaString(env, transport);
}

static jobjectArray android_app_backup_ElBackupManagerProxy_nativeListAllTransports(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeListAllTransports()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    AutoPtr<ArrayOf<String> > transports;
    bm->ListAllTransports((ArrayOf<String>**)&transports);

    jobjectArray jtransports = NULL;
    if (transports != NULL) {
        jtransports = ElUtil::GetJavaStringArray(env, transports);
    }

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeListAllTransports()");
    return jtransports;
}

static jstring android_app_backup_ElBackupManagerProxy_nativeSelectBackupTransport(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtransport)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeSelectBackupTransport()");

    String transport = ElUtil::ToElString(env, jtransport);

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    String result;
    bm->SelectBackupTransport(transport, &result);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeSelectBackupTransport()");
    return ElUtil::GetJavaString(env, result);
}

static jobject android_app_backup_ElBackupManagerProxy_nativeGetConfigurationIntent(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtransport)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeGetConfigurationIntent()");

    String transport = ElUtil::ToElString(env, jtransport);

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    AutoPtr<IIntent> configuration;
    ECode ec = bm->GetConfigurationIntent(transport, (IIntent**)&configuration);
    if (FAILED(ec))
        ALOGE("nativeGetConfigurationIntent() ec = 0x%08x", ec);

    jobject jconfiguration = NULL;
    if (configuration != NULL) {
        jconfiguration = ElUtil::GetJavaIntent(env, configuration);
    }

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeGetConfigurationIntent()");
    return jconfiguration;
}

static jstring android_app_backup_ElBackupManagerProxy_nativeGetDestinationString(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtransport)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeGetDestinationString()");

    String transport = ElUtil::ToElString(env, jtransport);

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    String destination;
    ECode ec = bm->GetDestinationString(transport, &destination);
    if (FAILED(ec))
        ALOGE("nativeGetDestinationString() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeGetDestinationString()");
    return ElUtil::GetJavaString(env, destination);
}

static jobject android_app_backup_ElBackupManagerProxy_nativeGetDataManagementIntent(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtransport)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeGetDataManagementIntent()");

    String transport = ElUtil::ToElString(env, jtransport);

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    AutoPtr<IIntent> intent;
    ECode ec = bm->GetDataManagementIntent(transport, (IIntent**)&intent);
    if (FAILED(ec))
        ALOGE("nativeGetDataManagementIntent() ec = 0x%08x", ec);

    jobject jintent = NULL;
    if (intent != NULL) {
        jintent = ElUtil::GetJavaIntent(env, intent);
    }

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeGetDataManagementIntent()");
    return jintent;
}

static jstring android_app_backup_ElBackupManagerProxy_nativeGetDataManagementLabel(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtransport)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeGetDataManagementLabel()");

    String transport = ElUtil::ToElString(env, jtransport);

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    String label;
    ECode ec = bm->GetDataManagementLabel(transport, &label);
    if (FAILED(ec))
        ALOGE("nativeGetDataManagementLabel() ec = 0x%08x", ec);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeGetDataManagementLabel()");
    return ElUtil::GetJavaString(env, label);
}

static jobject android_app_backup_ElBackupManagerProxy_nativeBeginRestoreSession(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jstring jtransportID)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeBeginRestoreSession()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    String transportID = ElUtil::ToElString(env, jtransportID);

    IIBackupManager* bm = (IIBackupManager*)jproxy;

    AutoPtr<IIRestoreSession> session;
    bm->BeginRestoreSession(packageName, transportID, (IIRestoreSession**)&session);

    jobject jsession = NULL;
    if (session != NULL) {
        jclass c = env->FindClass("android/app/backup/ElIRestoreSessionProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElIRestoreSessionProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ElIRestoreSessionProxy : %d!\n", __LINE__);

        jsession = env->NewObject(c, m, (jlong)session.Get());
        ElUtil::CheckErrorAndLog(env, "NewObject: ElIRestoreSessionProxy : %d!\n", __LINE__);

        session->AddRef();
    }

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeBeginRestoreSession()");
    return jsession;
}

static void android_app_backup_ElBackupManagerProxy_nativeOpComplete(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtoken)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeOpComplete()");

    IIBackupManager* bm = (IIBackupManager*)jproxy;
    bm->OpComplete((Int32)jtoken);

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeOpComplete()");
}

static void android_app_backup_ElBackupManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_app_backup_ElBackupManagerProxy_nativeDestroy()");

    IIBackupManager* obj = (IIBackupManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_app_backup_ElBackupManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeFinalize },
    { "nativeDataChanged",    "(JLjava/lang/String;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeDataChanged },
    { "nativeClearBackupData",    "(JLjava/lang/String;Ljava/lang/String;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeClearBackupData },
    { "nativeAgentConnected",    "(JLjava/lang/String;Landroid/os/IBinder;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeAgentConnected },
    { "nativeAgentDisconnected",    "(JLjava/lang/String;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeAgentDisconnected },
    { "nativeRestoreAtInstall",    "(JLjava/lang/String;I)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeRestoreAtInstall },
    { "nativeSetBackupEnabled",    "(JZ)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeSetBackupEnabled },
    { "nativeSetAutoRestore",    "(JZ)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeSetAutoRestore },
    { "nativeSetBackupProvisioned",    "(JZ)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeSetBackupProvisioned },
    { "nativeIsBackupEnabled",    "(J)Z",
            (void*) android_app_backup_ElBackupManagerProxy_nativeIsBackupEnabled },
    { "nativeSetBackupPassword",    "(JLjava/lang/String;Ljava/lang/String;)Z",
            (void*) android_app_backup_ElBackupManagerProxy_nativeSetBackupPassword },
    { "nativeHasBackupPassword",    "(J)Z",
            (void*) android_app_backup_ElBackupManagerProxy_nativeHasBackupPassword },
    { "nativeBackupNow",    "(J)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeBackupNow },
    { "nativeFullBackup",    "(JLandroid/os/ParcelFileDescriptor;ZZZZZZZ[Ljava/lang/String;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeFullBackup },
    { "nativeFullTransportBackup",    "(J[Ljava/lang/String;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeFullTransportBackup },
    { "nativeFullBackupNoninteractive",    "(JLandroid/os/ParcelFileDescriptor;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/app/backup/IFullBackupRestoreObserver;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeFullBackupNoninteractive },
    { "nativeFullRestore",    "(JLandroid/os/ParcelFileDescriptor;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeFullRestore },
    { "nativeFullRestoreNoninteractive",    "(JLandroid/os/ParcelFileDescriptor;ZLandroid/app/backup/IFullBackupRestoreObserver;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeFullRestoreNoninteractive },
    { "nativeAcknowledgeFullBackupOrRestore",    "(JIZLjava/lang/String;Ljava/lang/String;Landroid/app/backup/IFullBackupRestoreObserver;)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeAcknowledgeFullBackupOrRestore },
    { "nativeGetCurrentTransport",    "(J)Ljava/lang/String;",
            (void*) android_app_backup_ElBackupManagerProxy_nativeGetCurrentTransport },
    { "nativeListAllTransports",    "(J)[Ljava/lang/String;",
            (void*) android_app_backup_ElBackupManagerProxy_nativeListAllTransports },
    { "nativeSelectBackupTransport",    "(JLjava/lang/String;)Ljava/lang/String;",
            (void*) android_app_backup_ElBackupManagerProxy_nativeSelectBackupTransport },
    { "nativeGetConfigurationIntent",    "(JLjava/lang/String;)Landroid/content/Intent;",
            (void*) android_app_backup_ElBackupManagerProxy_nativeGetConfigurationIntent },
    { "nativeGetDestinationString",    "(JLjava/lang/String;)Ljava/lang/String;",
            (void*) android_app_backup_ElBackupManagerProxy_nativeGetDestinationString },
    { "nativeGetDataManagementIntent",    "(JLjava/lang/String;)Landroid/content/Intent;",
            (void*) android_app_backup_ElBackupManagerProxy_nativeGetDataManagementIntent },
    { "nativeGetDataManagementLabel",    "(JLjava/lang/String;)Ljava/lang/String;",
            (void*) android_app_backup_ElBackupManagerProxy_nativeGetDataManagementLabel },
    { "nativeBeginRestoreSession",    "(JLjava/lang/String;Ljava/lang/String;)Landroid/app/backup/IRestoreSession;",
            (void*) android_app_backup_ElBackupManagerProxy_nativeBeginRestoreSession },
    { "nativeOpComplete",    "(JI)V",
            (void*) android_app_backup_ElBackupManagerProxy_nativeOpComplete },
};

int register_android_app_backup_ElBackupManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/app/backup/ElBackupManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

