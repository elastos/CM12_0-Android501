#define LOG_TAG "ElPackageManagerProxyJNI"

#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Utility.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Core.h>
#include <Elastos.CoreLibrary.Utility.h>

#include <utils/Log.h>
#include <elastos/utility/logging/Logger.h>

using Elastos::Utility::Logging::Logger;

using android::ElUtil;

using Elastos::Droid::Content::IIntentSender;
using Elastos::Droid::Content::Pm::ECLSID_CApplicationInfo;
using Elastos::Droid::Content::Pm::ECLSID_CPackageInfo;
using Elastos::Droid::Content::Pm::IActivityInfo;
using Elastos::Droid::Content::Pm::IIPackageManager;
using Elastos::Droid::Content::Pm::IParceledListSlice;
using Elastos::Droid::Content::Pm::IPackageInfo;
using Elastos::Droid::Content::Pm::IIPackageInstaller;
using Elastos::Droid::Content::Pm::IIPackageStatsObserver;
using Elastos::Droid::Content::Pm::IIPackageDataObserver;
using Elastos::Droid::Content::Pm::IIPackageDeleteObserver;
using Elastos::Droid::Content::Pm::IIPackageDeleteObserver2;
using Elastos::Droid::Content::Pm::IIPackageInstallObserver2;
using Elastos::Droid::Content::Pm::IIPackageMoveObserver;
using Elastos::Droid::Content::Pm::IPermissionInfo;
using Elastos::Droid::Content::Pm::IPermissionGroupInfo;
using Elastos::Droid::Content::Pm::IResolveInfo;
using Elastos::Droid::Content::Pm::IServiceInfo;
using Elastos::Droid::Content::Pm::IVerifierDeviceIdentity;
using Elastos::Droid::Content::Pm::IElSignatureParser;
using Elastos::Droid::JavaProxy::CPackageStatusObserverNative;
using Elastos::Droid::JavaProxy::CPackageInstallObserverNative;
using Elastos::Droid::JavaProxy::CPackageInstallObserverNative;
using Elastos::Droid::JavaProxy::CIPackageDataObserverNative;
using Elastos::Droid::JavaProxy::CIPackageDeleteObserverNative;
using Elastos::Droid::JavaProxy::CIPackageDeleteObserver2Native;
using Elastos::Droid::JavaProxy::CPackageInstallObserver2Native;
using Elastos::Droid::JavaProxy::CIPackageMoveObserverNative;
using Elastos::Droid::JavaProxy::CElSignatureParser;
using Elastos::Droid::Utility::CParcelableList;

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsPackageAvailable(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsPackageAvailable()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    pm->IsPackageAvailable(packageName, userId, &result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeIsPackageAvailable()");
    return result;
}

static jobjectArray android_content_pm_ElPackageManagerProxy_nativeGetPackagesForUid(JNIEnv* env, jobject clazz, jlong jproxy, jint juid)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPackagesForUid()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<ArrayOf<String> >packages;
    pm->GetPackagesForUid((Int32)juid, (ArrayOf<String>**)&packages);

    jobjectArray strArray = NULL;
    if (packages != NULL) {
        strArray = ElUtil::GetJavaStringArray(env, packages);
        if (strArray == NULL) {
            ALOGE("android_content_pm_ElPackageManagerProxy_nativeGetPackagesForUid() strArray is NULL!");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPackagesForUid()");
    return strArray;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetPackageInfo(JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPackageInfo()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IPackageInfo> packageInfo;

    String packageName = ElUtil::ToElString(env, jpackageName);
    pm->GetPackageInfo(packageName, (Int32)jflags, (Int32)juserId, (IPackageInfo**)&packageInfo);

    jobject jpackageInfo = NULL;
    if (packageInfo != NULL) {
        jpackageInfo = ElUtil::GetJavaPackageInfo(env, packageInfo);
        if (jpackageInfo == NULL) {
            ALOGE("android_content_pm_ElPackageManagerProxy_nativeGetPackageInfo() jpackageInfo is NULL!");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPackageInfo()");
    return jpackageInfo;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetActivityInfo(JNIEnv* env, jobject clazz, jlong jproxy, jobject jcomponentName, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetActivityInfo()");

    AutoPtr<IComponentName> componentName;

    if(!ElUtil::ToElComponentName(env, jcomponentName, (IComponentName**)&componentName)) {
        ALOGE("android_content_pm_ElPackageManagerProxy_nativeGetActivityInfo() ToElComponentName fail!");
    }

    String packages;
    componentName->GetPackageName(&packages);
    String className;
    componentName->GetClassName(&className);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IActivityInfo> activityInfo;
    ECode ec = pm->GetActivityInfo(componentName, (Int32)jflags, (Int32)juserId, (IActivityInfo**)&activityInfo);

    jobject jactivityInfo = NULL;
    if (activityInfo != NULL) {
        jactivityInfo = ElUtil::GetJavaActivityInfo(env, activityInfo);
        if(jactivityInfo == NULL) {
            ALOGE("android_content_pm_ElPackageManagerProxy_nativeGetActivityInfo() jactivityInfo is NULL");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetActivityInfo()");
    return jactivityInfo;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeActivitySupportsIntent(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcomponentName, jobject jintent, jstring jresolvedType)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeActivitySupportsIntent()");

    AutoPtr<IComponentName> componentName;

    if(!ElUtil::ToElComponentName(env, jcomponentName, (IComponentName**)&componentName)) {
        ALOGE("android_content_pm_ElPackageManagerProxy_nativeActivitySupportsIntent() ToElComponentName fail!");
    }

    AutoPtr<IIntent> intent;
    if(jintent != NULL) {
        if(!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeQueryIntentActivities() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    ECode ec = pm->ActivitySupportsIntent(componentName, intent, resolvedType, &result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeActivitySupportsIntent()");
    return result;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeQueryIntentActivities(JNIEnv* env, jobject clazz, jlong jproxy, jobject jintent,
        jstring jresolvedType, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeQueryIntentActivities()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IIntent> intent;
    if(jintent != NULL) {
        if(!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeQueryIntentActivities() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    AutoPtr<IList> container;
    ECode ec = pm->QueryIntentActivities(intent, resolvedType, (Int32)jflags, (Int32)juserId, (IList**)&container);
    // ALOGE("nativeQueryIntentActivities() QueryIntentActivities ec=0x%x", ec);

    jobject jlist = NULL;
    if (container != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetInstalledProviders Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetInstalledProviders Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jlist = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetInstalledProviders Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetJavaInputDevice Fail GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        container->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            container->Get(i, (IInterface**)&obj);
            AutoPtr<IResolveInfo> info = IResolveInfo::Probe(obj);
            jobject jInfo = ElUtil::GetJavaResolveInfo(env, info);
            if (jInfo != NULL) {
                env->CallBooleanMethod(jlist, mAdd, jInfo);
                ElUtil::CheckErrorAndLog(env, "nativeQueryIntentActivities Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jInfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeQueryIntentActivities()");
    return jlist;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsSafeMode(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsSafeMode()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    Boolean isSafeMode;
    pm->IsSafeMode(&isSafeMode);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeIsSafeMode()");
    return (jboolean)isSafeMode;
}

static jint android_content_pm_ElPackageManagerProxy_nativeGetFlagsForUid(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetFlagsForUid()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    Int32 flags;
    pm->GetFlagsForUid(uid, &flags);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetFlagsForUid()");
    return flags;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsUidPrivileged(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsUidPrivileged()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    Boolean isUidPrivileged;
    pm->IsUidPrivileged(uid, &isUidPrivileged);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeIsUidPrivileged()");
    return (jboolean)isUidPrivileged;
}

static jobjectArray android_content_pm_ElPackageManagerProxy_nativeGetAppOpPermissionPackages(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpermissionName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetAppOpPermissionPackages()");

    String permissionName = ElUtil::ToElString(env, jpermissionName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<ArrayOf<String> > packages;
    pm->GetAppOpPermissionPackages(permissionName, (ArrayOf<String>**)&packages);

    jobjectArray strArray = NULL;
    if (packages != NULL) {
        strArray = ElUtil::GetJavaStringArray(env, packages);
        if (strArray == NULL) {
            ALOGE("nativeGetAppOpPermissionPackages() strArray is NULL!");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetAppOpPermissionPackages()");
    return strArray;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeResolveIntent(JNIEnv* env, jobject clazz, jlong jproxy, jobject jintent,
    jstring jresolvedType, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeResolveIntent()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<IIntent> intent;
    if(jintent != NULL) {
        if(!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeQueryIntentActivities() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    AutoPtr<IResolveInfo> info;
    pm->ResolveIntent(intent, resolvedType, (Int32)jflags, (Int32)juserId, (IResolveInfo**)&info);

    jobject jInfo = NULL;
    if (info != NULL) {
        jInfo = ElUtil::GetJavaResolveInfo(env, info);
        if (jInfo == NULL) {
             ALOGE("nativeResolveIntent() jInfo is NULL ");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeResolveIntent()");
    return jInfo;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeCanForwardTo(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jintent, jstring jresolvedType, jint sourceUserId, jint targetUserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeCanForwardTo()");

    AutoPtr<IIntent> intent;
    if(jintent != NULL) {
        if(!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeCanForwardTo() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    Boolean result;
    pm->CanForwardTo(intent, resolvedType, sourceUserId, targetUserId, &result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeCanForwardTo()");
    return (jboolean)result;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetApplicationInfo(JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName,
        jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetApplicationInfo()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;


    String packageName = ElUtil::ToElString(env, jpackageName);
    if (packageName.Equals("com.android.defcontainer")) {
        packageName = "Elastos.Droid.DefContainer";
    }

    AutoPtr<IApplicationInfo> info;
    pm->GetApplicationInfo(packageName, (Int32)jflags, (Int32)juserId, (IApplicationInfo**)&info);

    jobject jInfo = NULL;
    if (info != NULL) {
        jInfo = ElUtil::GetJavaApplicationInfo(env, info);
        if (jInfo == NULL) {
             ALOGE("nativeGetApplicationInfo() jInfo is NULL ");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetApplicationInfo()");
    return jInfo;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetInstalledApplications(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetInstalledApplications()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<IParceledListSlice> slice;
    pm->GetInstalledApplications((Int32)jflags, (Int32)juserId, (IParceledListSlice**)&slice);

    jobject jslice = NULL;
    if (slice != NULL) {
        jslice = ElUtil::GetJavaParceledListSlice(env, ECLSID_CApplicationInfo, slice);
        if (jslice == NULL) {
             ALOGE("nativeGetInstalledApplications() jslice is NULL ");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetInstalledApplications()");
    return jslice;
}

static void android_content_pm_ElPackageManagerProxy_nativeGetPackageSizeInfo(JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName,
        jint juserHandle, jobject jobserver)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPackageSizeInfo()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    String packageName = ElUtil::ToElString(env, jpackageName);

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jobserver);

    AutoPtr<IIPackageStatsObserver> observer;
    ECode ec = CPackageStatusObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIPackageStatsObserver**)&observer);
    if (FAILED(ec)) {
        ALOGD("nativeGetPackageSizeInfo fail new CPackageStatusObserverNative ec: 0x%x =====\n", ec);
    }

    ec = pm->GetPackageSizeInfo(packageName, (Int32)juserHandle, observer);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPackageSizeInfo()");
}

static jobjectArray android_content_pm_ElPackageManagerProxy_nativeCurrentToCanonicalPackageNames(JNIEnv* env, jobject clazz, jlong jproxy, jobjectArray jnames){
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeCurrentToCanonicalPackageNames()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<ArrayOf<String> > names;
    AutoPtr<ArrayOf<String> > result;

    ElUtil::ToElStringArray(env, jnames, (ArrayOf<String>**)&names);

    if(names != NULL){
        ALOGD("android_content_pm_ElPackageManagerProxy_nativeCurrentToCanonicalPackageNames(), input array size:%d", names->GetLength());
        ALOGE("android_content_pm_ElPackageManagerProxy_nativeCurrentToCanonicalPackageNames(), call service not complete");
        //pm->CurrentToCanonicalPackageNames(*(names.Get()), (ArrayOf<String>**)&result);
    }

    jobjectArray strArray = NULL;
    if (result != NULL) {
        strArray = ElUtil::GetJavaStringArray(env, result);
        if (strArray == NULL) {
            ALOGE("android_content_pm_ElPackageManagerProxy_nativeCurrentToCanonicalPackageNames() strArray is NULL!");
        }
    }
    else{
        ALOGE("android_content_pm_ElPackageManagerProxy_nativeCurrentToCanonicalPackageNames(), CurrentToCanonicalPackageNames is NULL!");
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeCurrentToCanonicalPackageNames()");
    return strArray;
}

static jint android_content_pm_ElPackageManagerProxy_nativeCheckPermission(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpermName, jstring jpkgName) {
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeCheckPermission()");

    String permName = ElUtil::ToElString(env, jpermName);
    String pkgName = ElUtil::ToElString(env, jpkgName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Int32 result;
    pm->CheckPermission(permName, pkgName, &result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeCheckPermission()");
    return (jint)result;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeHasSystemFeature(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jname) {
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeHasSystemFeature()");

    String name = ElUtil::ToElString(env, jname);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    pm->HasSystemFeature(name, &result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeHasSystemFeature()");
    return (jboolean)result;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeResolveContentProvider(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jname, jint jflags, jint juserId) {
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeResolveContentProvider()");

    String name = ElUtil::ToElString(env, jname);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IProviderInfo> info;
    pm->ResolveContentProvider(name, (Int32)jflags, (Int32)juserId, (IProviderInfo**)&info);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaProviderInfo(env, info);
        if (jinfo == NULL) {
            ALOGE("nativeResolveContentProvider() jinfo is NULL!");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeResolveContentProvider()");
    return jinfo;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeQueryIntentServices(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jintent, jstring jresolvedType, jint jflags, jint juserId) {
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeQueryIntentServices()");

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGD("nativeQueryIntentServices() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> list;
    pm->QueryIntentServices(intent, resolvedType, (Int32)jflags, (Int32)juserId, (IList**)&list);

    jobject jservices = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentServices Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentServices Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jservices = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentServices Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentServices Fail GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 count = 0;
        list->GetSize(&count);
        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IResolveInfo> info = IResolveInfo::Probe(obj);
            jobject jInfo = ElUtil::GetJavaResolveInfo(env, info);
            if (jInfo != NULL) {
                env->CallBooleanMethod(jservices, mAdd, jInfo);
                ElUtil::CheckErrorAndLog(env, "nativeQueryIntentServices Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jInfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeQueryIntentServices()");
    return jservices;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeQueryIntentContentProviders(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jintent, jstring jresolvedType, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeQueryIntentContentProviders()");

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGD("nativeQueryIntentContentProviders() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> list;
    pm->QueryIntentContentProviders(intent, resolvedType, (Int32)jflags, (Int32)juserId, (IList**)&list);

    jobject jservices = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentContentProviders Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentContentProviders Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jservices = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentContentProviders Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentContentProviders Fail GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 count = 0;
        list->GetSize(&count);
        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IResolveInfo> info = IResolveInfo::Probe(obj);
            jobject jInfo = ElUtil::GetJavaResolveInfo(env, info);
            if (jInfo != NULL) {
                env->CallBooleanMethod(jservices, mAdd, jInfo);
                ElUtil::CheckErrorAndLog(env, "nativeQueryIntentContentProviders Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jInfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeQueryIntentContentProviders()");
    return jservices;
}

static jint android_content_pm_ElPackageManagerProxy_nativeGetComponentEnabledSetting(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jcomponentName, jint juserId) {
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetComponentEnabledSetting()");

    AutoPtr<IComponentName> componentName;
    if (jcomponentName != NULL) {
        if (!ElUtil::ToElComponentName(env, jcomponentName, (IComponentName**)&componentName)) {
            ALOGE("nativeGetComponentEnabledSetting() ToElComponentName fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Int32 setting;
    pm->GetComponentEnabledSetting(componentName, (Int32)juserId, &setting);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetComponentEnabledSetting()");
    return (jint)setting;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetServiceInfo(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclassName, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetServiceInfo()");
    AutoPtr<IComponentName> className;
    if (jclassName != NULL) {
        if (!ElUtil::ToElComponentName(env, jclassName, (IComponentName**)&className)) {
            ALOGE("nativeGetServiceInfo ToElComponentName fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IServiceInfo> info;
    pm->GetServiceInfo(className, (Int32)jflags, (Int32)juserId, (IServiceInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaServiceInfo(env, info);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetServiceInfo()");
    return jinfo;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetReceiverInfo(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclassName, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetReceiverInfo()");
    AutoPtr<IComponentName> className;
    if (jclassName != NULL) {
        if (!ElUtil::ToElComponentName(env, jclassName, (IComponentName**)&className)) {
            ALOGE("nativeGetReceiverInfo ToElComponentName fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IActivityInfo> info;
    pm->GetReceiverInfo(className, (Int32)jflags, (Int32)juserId, (IActivityInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaActivityInfo(env, info);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetReceiverInfo()");
    return jinfo;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeQueryIntentReceivers(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jintent, jstring jresolvedType, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeQueryIntentReceivers()");

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGD("nativeQueryIntentReceivers() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> container;
    pm->QueryIntentReceivers(intent, resolvedType, (Int32)jflags, (Int32)juserId, (IList**)&container);

    jobject jreceivers = NULL;
    if (container != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentReceivers Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentReceivers Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jreceivers  = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentReceivers Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeQueryIntentReceivers Fail GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        container->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            container->Get(i, (IInterface**)&obj);
            AutoPtr<IResolveInfo> info = IResolveInfo::Probe(obj);
            jobject jInfo = ElUtil::GetJavaResolveInfo(env, info);
            if (jInfo != NULL) {
                env->CallBooleanMethod(jreceivers, mAdd, jInfo);
                ElUtil::CheckErrorAndLog(env, "nativeQueryIntentReceivers Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jInfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeQueryIntentReceivers()");
    return jreceivers;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetPermissionInfo(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jname, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPermissionInfo()");

    String name = ElUtil::ToElString(env, jname);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IPermissionInfo> info;
    pm->GetPermissionInfo(name, (Int32)jflags, (IPermissionInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaPermissionInfo(env, info);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPermissionInfo()");
    return jinfo;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsPermissionEnforced(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpermission)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsPermissionEnforced()");

    String permission = ElUtil::ToElString(env, jpermission);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    pm->IsPermissionEnforced(permission, &result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeIsPermissionEnforced()");
    return (jboolean)result;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetPermissionGroupInfo(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jname, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPermissionGroupInfo()");

    String name = ElUtil::ToElString(env, jname);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IPermissionGroupInfo> info;
    pm->GetPermissionGroupInfo(name, (Int32)jflags, (IPermissionGroupInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaPermissionGroupInfo(env, info);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPermissionGroupInfo()");
    return jinfo;
}

static jint android_content_pm_ElPackageManagerProxy_nativeGetPreferredActivities(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject joutFilters, jobject joutActivities, jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPreferredActivities()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IList> outFilters;
    CParcelableList::New((IList**)&outFilters);

    AutoPtr<IList> outActivities;
    CParcelableList::New((IList**)&outActivities);

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    Int32 count = 0;
    ECode ec = pm->GetPreferredActivities(outFilters, outActivities, packageName, &count);
    if (FAILED(ec))
        ALOGE("nativeGetPreferredActivities() ec: 0x%8x", ec);

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetPreferredActivities() FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
    ElUtil::CheckErrorAndLog(env, "nativeGetPreferredActivities() GetMethodID: add : %d!\n", __LINE__);

    if (joutFilters != NULL) {
        Int32 size = 0;
        outFilters->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            outFilters->Get(i, (IInterface**)&obj);
            AutoPtr<IIntentFilter> filter = IIntentFilter::Probe(obj);
            if (filter != NULL) {
                jobject joutFilter= ElUtil::GetJavaIntentFilter(env, filter);
                if (joutFilter != NULL) {
                    env->CallBooleanMethod(joutFilters, mAdd, joutFilter);
                    ElUtil::CheckErrorAndLog(env, "nativeGetPreferredActivities() CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(joutFilter);
                } else {
                    ALOGE("nativeGetPreferredActivities() jfilter is NULL!");
                }
            }
        }
    }

    if (joutActivities != NULL) {
        Int32 size = 0;
        outActivities->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            outActivities->Get(i, (IInterface**)&obj);
            AutoPtr<IComponentName> outActivitiy = IComponentName::Probe(obj);
            if (outActivitiy != NULL) {
                jobject joutActivitiy= ElUtil::GetJavaComponentName(env, outActivitiy);
                if (joutActivitiy != NULL) {
                    env->CallBooleanMethod(joutActivities, mAdd, joutActivitiy);
                    ElUtil::CheckErrorAndLog(env, "nativeGetPreferredActivities() CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(joutActivitiy);
                } else {
                    ALOGE("nativeGetPreferredActivities() jfilter is NULL!");
                }
            }
        }
    }

    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPreferredActivities()");
    return (jint)count;
}

static void android_content_pm_ElPackageManagerProxy_nativeAddPersistentPreferredActivity(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jfilter, jobject jactivity, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeAddPersistentPreferredActivity()");

    AutoPtr<IIntentFilter> filter;
    if (!ElUtil::ToElIntentFilter(env, jfilter, (IIntentFilter**)&filter)) {
        ALOGE("nativeAddPreferredActivity ToElIntentFilter failed");
    }

    AutoPtr<IComponentName> activity;
    if(!ElUtil::ToElComponentName(env, jactivity, (IComponentName**)&activity)) {
        ALOGE("nativeAddPreferredActivity() ToElComponentName failed %d!", __LINE__);
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    pm->AddPersistentPreferredActivity(filter, activity, userId);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeAddPersistentPreferredActivity()");
}

static void android_content_pm_ElPackageManagerProxy_nativeClearPackagePersistentPreferredActivities(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeClearPackagePersistentPreferredActivities()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    pm->ClearPackagePersistentPreferredActivities(packageName, userId);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeClearPackagePersistentPreferredActivities()");
}

static void android_content_pm_ElPackageManagerProxy_nativeAddCrossProfileIntentFilter(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jfilter, jstring jownerPackage, jint ownerUserId, jint sourceUserId, jint targetUserId, jint flags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeAddCrossProfileIntentFilter()");

    AutoPtr<IIntentFilter> filter;
    if (!ElUtil::ToElIntentFilter(env, jfilter, (IIntentFilter**)&filter)) {
        ALOGE("nativeAddCrossProfileIntentFilter ToElIntentFilter failed");
    }

    String ownerPackage = ElUtil::ToElString(env, jownerPackage);

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    pm->AddCrossProfileIntentFilter(filter, ownerPackage, ownerUserId, sourceUserId, targetUserId, flags);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeAddCrossProfileIntentFilter()");
}

static void android_content_pm_ElPackageManagerProxy_nativeClearCrossProfileIntentFilters(JNIEnv* env, jobject clazz, jlong jproxy,
    jint sourceUserId, jstring jownerPackage, jint ownerUserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeClearCrossProfileIntentFilters()");

    String ownerPackage = ElUtil::ToElString(env, jownerPackage);

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    pm->ClearCrossProfileIntentFilters(sourceUserId, ownerPackage, ownerUserId);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeClearCrossProfileIntentFilters()");
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetHomeActivities(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject joutHomeCandidates)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetHomeActivities()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<IList> outHomeCandidates;
    CParcelableList::New((IList**)&outHomeCandidates);
    AutoPtr<IComponentName> cOutName;
    pm->GetHomeActivities(outHomeCandidates, (IComponentName**)&cOutName);

    if (outHomeCandidates != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetHomeActivities Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetHomeActivities Fail GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        outHomeCandidates->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            outHomeCandidates->Get(i, (IInterface**)&obj);
            AutoPtr<IResolveInfo> info = IResolveInfo::Probe(obj);
            jobject jInfo = ElUtil::GetJavaResolveInfo(env, info);
            if (jInfo != NULL) {
                env->CallBooleanMethod(joutHomeCandidates, mAdd, jInfo);
                ElUtil::CheckErrorAndLog(env, "nativeGetHomeActivities Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jInfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetHomeActivities()");
    return ElUtil::GetJavaComponentName(env, cOutName);
}

static void android_content_pm_ElPackageManagerProxy_nativeSetComponentEnabledSetting(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcomponentName, jint jnewState, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetComponentEnabledSetting()");

    AutoPtr<IComponentName> componentName;
    if (jcomponentName != NULL) {
        if (!ElUtil::ToElComponentName(env, jcomponentName, (IComponentName**)&componentName)) {
            ALOGE("nativeSetComponentEnabledSetting ToElComponentName fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    pm->SetComponentEnabledSetting(componentName, (Int32)jnewState, (Int32)jflags, (Int32)juserId);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeSetComponentEnabledSetting()");
}

static jint android_content_pm_ElPackageManagerProxy_nativeGetApplicationEnabledSetting(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetApplicationEnabledSetting()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Int32 result = 0;
    pm->GetApplicationEnabledSetting(packageName, (Int32)juserId, &result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetApplicationEnabledSetting()");
    return (jint)result;
}

static jstring android_content_pm_ElPackageManagerProxy_nativeGetInstallerPackageName(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetInstallerPackageName()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    String name;
    pm->GetInstallerPackageName(packageName, &name);
    jstring jname = ElUtil::GetJavaString(env, name);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetInstallerPackageName()");
    return jname;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeResolveService(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jintent, jstring jresolvedType, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeResolveService()");

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeResolveService() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<IResolveInfo> resolveInfo;
    pm->ResolveService(intent, resolvedType, (Int32)jflags, (Int32)juserId, (IResolveInfo**)&resolveInfo);
    jobject jresolveInfo = NULL;
    if (resolveInfo != NULL) {
        jresolveInfo = ElUtil::GetJavaResolveInfo(env, resolveInfo);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeResolveService()");
    return jresolveInfo;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsOnlyCoreApps(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsOnlyCoreApps()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    Boolean result = FALSE;
    pm->IsOnlyCoreApps(&result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeIsOnlyCoreApps()");
    return (jboolean)result;
}

static void android_content_pm_ElPackageManagerProxy_nativeDeletePackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jobject jobserver, jint userId, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeDeletePackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIPackageDeleteObserver2> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        if(NOERROR != CIPackageDeleteObserver2Native::New((Handle64)jvm, (Handle64)jInstance, (IIPackageDeleteObserver2**)&observer)) {
            ALOGE("nativeDeletePackage new CIPackageDeleteObserverNative fail!\n");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    pm->DeletePackage(packageName, observer, userId, (Int32)jflags);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeDeletePackage()");
}


static void android_content_pm_ElPackageManagerProxy_nativeDeletePackageAsUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jobject jobserver, jint userId, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeDeletePackageAsUser()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIPackageDeleteObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        if(NOERROR != CIPackageDeleteObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIPackageDeleteObserver**)&observer)) {
            ALOGE("nativeDeletePackageAsUser new CIPackageDeleteObserverNative fail!\n");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    pm->DeletePackageAsUser(packageName, observer, userId, (Int32)jflags);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeDeletePackageAsUser()");
}

static jobjectArray android_content_pm_ElPackageManagerProxy_nativeCanonicalToCurrentPackageNames(
    JNIEnv* env, jobject clazz, jlong jproxy, jobjectArray jnames)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeCanonicalToCurrentPackageNames()");

    AutoPtr<ArrayOf<String> > names;
    if (jnames != NULL) {
        if (!ElUtil::ToElStringArray(env, jnames, (ArrayOf<String>**)&names)) {
            ALOGE("nativeCanonicalToCurrentPackageNames() ToElStringArray fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<ArrayOf<String> > newNames;
    pm->CanonicalToCurrentPackageNames(*names, (ArrayOf<String>**)&newNames);

    jobjectArray strArray = NULL;
    if (newNames != NULL) {
        strArray = ElUtil::GetJavaStringArray(env, newNames);
        if (strArray == NULL) {
            ALOGE("nativeCanonicalToCurrentPackageNames() strArray is NULL!");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeCanonicalToCurrentPackageNames()");

    return strArray;
}

static jint android_content_pm_ElPackageManagerProxy_nativeCheckSignatures(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpkg1, jstring jpkg2)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeCheckSignatures()");

    String pkg1 = ElUtil::ToElString(env, jpkg1);
    String pkg2 = ElUtil::ToElString(env, jpkg2);

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    Int32 result = 0;
    pm->CheckSignatures(pkg1, pkg2, &result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeCheckSignatures()");
    return (jint)result;
}

static jint android_content_pm_ElPackageManagerProxy_nativeCheckUidSignatures(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juid1, jint juid2)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeCheckUidSignatures()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    Int32 result = 0;
    pm->CheckUidSignatures((Int32)juid1, (Int32)juid2, &result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeCheckUidSignatures()");
    return (jint)result;
}

static void android_content_pm_ElPackageManagerProxy_nativeDeleteApplicationCacheFiles(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jobject jobserver)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeDeleteApplicationCacheFiles()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIPackageDataObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        CIPackageDataObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIPackageDataObserver**)&observer);
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->DeleteApplicationCacheFiles(packageName, observer);
    if (FAILED(ec))
        ALOGE("nativeDeleteApplicationCacheFiles() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeDeleteApplicationCacheFiles()");
}

static jobjectArray android_content_pm_ElPackageManagerProxy_nativeGetSystemSharedLibraryNames(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetSystemSharedLibraryNames()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<ArrayOf<String> > names;
    ECode ec = pm->GetSystemSharedLibraryNames((ArrayOf<String>**)&names);
    if (FAILED(ec))
        ALOGE("nativeGetSystemSharedLibraryNames() ec: 0x%08x", ec);

    jobjectArray jnames = NULL;
    if (names != NULL) {
        jnames = ElUtil::GetJavaStringArray(env, names);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetSystemSharedLibraryNames()");
    return jnames;
}

static jobjectArray android_content_pm_ElPackageManagerProxy_nativeGetSystemAvailableFeatures(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetSystemAvailableFeatures()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<ArrayOf<IFeatureInfo*> > features;
    ECode ec = pm->GetSystemAvailableFeatures((ArrayOf<IFeatureInfo*>**)&features);
    if (FAILED(ec))
        ALOGE("nativeGetSystemAvailableFeatures() ec: 0x%08x", ec);

    jobjectArray jfeatures = NULL;
    if (features != NULL) {
        Int32 count = features->GetLength();

        jclass fiKlass = env->FindClass("android/content/pm/FeatureInfo");
        ElUtil::CheckErrorAndLog(env, "FindClass: FeatureInfo : %d!\n", __LINE__);

        jfeatures = env->NewObjectArray((jsize)count, fiKlass, 0);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: FeatureInfo : %d!\n", __LINE__);
        env->DeleteLocalRef(fiKlass);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jfeature = ElUtil::GetJavaFeatureInfo(env, (*features)[i]);
            if (jfeature != NULL) {
                env->SetObjectArrayElement(jfeatures, i, jfeature);
                env->DeleteLocalRef(jfeature);
            } else {
                ALOGE("nativeGetSystemSharedLibraryNames() GetJavaFeatureInfo fail!");
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetSystemAvailableFeatures()");
    return jfeatures;
}

static jint android_content_pm_ElPackageManagerProxy_nativeGetPackageUid(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPackageUid()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Int32 uid = 0;
    ECode ec = pm->GetPackageUid(packageName, (Int32)juserId, &uid);
    if (FAILED(ec))
        ALOGE("nativeGetPackageUid() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPackageUid()");
    return (jint)uid;
}

static jintArray android_content_pm_ElPackageManagerProxy_nativeGetPackageGids(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPackageGids()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<ArrayOf<Int32> > gids;
    ECode ec = pm->GetPackageGids(packageName, (ArrayOf<Int32>**)&gids);
    if (FAILED(ec))
        ALOGE("nativeGetPackageGids() ec: 0x%08x", ec);

    jintArray jgids = NULL;
    if (gids != NULL) {
        jgids = ElUtil::GetJavaIntArray(env, gids);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPackageGids()");
    return jgids;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeNextPackageToClean(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlastPackage)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeNextPackageToClean()");

    AutoPtr<IPackageCleanItem> lastPackage;
    if (jlastPackage != NULL) {
        if (!ElUtil::ToElPackageCleanItem(env, jlastPackage, (IPackageCleanItem**)&lastPackage)) {
            ALOGE("nativeNextPackageToClean() ToElPackageCleanItem fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IPackageCleanItem> nextPackage;
    pm->NextPackageToClean(lastPackage, (IPackageCleanItem**)&nextPackage);

    jobject jnextPackage = NULL;
    if (nextPackage != NULL) {
        jnextPackage = ElUtil::GetJavaPackageCleanItem(env, nextPackage);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeNextPackageToClean()");
    return jnextPackage;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeQueryPermissionsByGroup(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jgroup, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeQueryPermissionsByGroup()");

    String group = ElUtil::ToElString(env, jgroup);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> container;
    ECode ec = pm->QueryPermissionsByGroup(group, (Int32)jflags, (IList**)&container);
    if (FAILED(ec))
        ALOGE("nativeQueryPermissionsByGroup() ec: 0x%08x", ec);

    jobject jpermissions = NULL;
    if (container != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jpermissions = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        container->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            container->Get(i, (IInterface**)&obj);
            AutoPtr<IPermissionInfo> permission = IPermissionInfo::Probe(obj);
            jobject jpermission = ElUtil::GetJavaPermissionInfo(env, permission);
            if (jpermission != NULL) {
                env->CallBooleanMethod(jpermissions, mAdd, jpermission);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jpermission);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeQueryPermissionsByGroup()");
    return jpermissions;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetAllPermissionGroups(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetAllPermissionGroups()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> container;
    ECode ec = pm->GetAllPermissionGroups((Int32)jflags, (IList**)&container);
    if (FAILED(ec))
        ALOGE("nativeGetAllPermissionGroups() ec: 0x%08x", ec);

    jobject jperGroups = NULL;
    if (container != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jperGroups = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        container->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            container->Get(i, (IInterface**)&obj);
            AutoPtr<IPermissionGroupInfo> perGroup = IPermissionGroupInfo::Probe(obj);
            jobject jperGroup = ElUtil::GetJavaPermissionGroupInfo(env, perGroup);
            if (jperGroup != NULL) {
                env->CallBooleanMethod(jperGroups, mAdd, jperGroup);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jperGroup);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetAllPermissionGroups()");
    return jperGroups;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetProviderInfo(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jclassName, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetProviderInfo()");

    AutoPtr<IComponentName> className;
    if (jclassName != NULL) {
        if (!ElUtil::ToElComponentName(env, jclassName, (IComponentName**)&className)) {
            ALOGE("nativeGetProviderInfo() ToElComponentName fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IProviderInfo> info;
    ECode ec = pm->GetProviderInfo(className, (Int32)jflags, (Int32)juserId, (IProviderInfo**)&info);
    if (FAILED(ec))
        ALOGE("nativeGetProviderInfo() ec: 0x%08x", ec);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaProviderInfo(env, info);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetProviderInfo()");
    return jinfo;
}

static jint android_content_pm_ElPackageManagerProxy_nativeCheckUidPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpermName, jint juid)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeCheckUidPermission()");

    String permName = ElUtil::ToElString(env, jpermName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Int32 result = -1;
    ECode ec = pm->CheckUidPermission(permName, (Int32)juid, &result);
    if (FAILED(ec))
        ALOGE("nativeCheckUidPermission() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeCheckUidPermission()");
    return (jint)result;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeAddPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jinfo)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeAddPermission()");

    AutoPtr<IPermissionInfo> info;
    if (jinfo != NULL) {
        if (!ElUtil::ToElPermissionInfo(env, jinfo, (IPermissionInfo**)&info)) {
            ALOGE("nativeAddPermission() ToElPermissionInfo fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = pm->AddPermission(info, &result);
    if (FAILED(ec))
        ALOGE("nativeAddPermission() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeAddPermission()");
    return (jboolean)result;
}

static void android_content_pm_ElPackageManagerProxy_nativeRemovePermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeRemovePermission()");

    String name = ElUtil::ToElString(env, jname);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->RemovePermission(name);
    if (FAILED(ec))
        ALOGE("nativeRemovePermission() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeRemovePermission()");
}

static void android_content_pm_ElPackageManagerProxy_nativeGrantPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jstring jpermissionName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGrantPermission()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    String permissionName = ElUtil::ToElString(env, jpermissionName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->GrantPermission(packageName, permissionName);
    if (FAILED(ec))
        ALOGE("nativeGrantPermission() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGrantPermission()");
}

static void android_content_pm_ElPackageManagerProxy_nativeRevokePermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jstring jpermissionName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeRevokePermission()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    String permissionName = ElUtil::ToElString(env, jpermissionName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->RevokePermission(packageName, permissionName);
    if (FAILED(ec))
        ALOGE("nativeRevokePermission() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeRevokePermission()");
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsProtectedBroadcast(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jactionName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsProtectedBroadcast()");

    String actionName = ElUtil::ToElString(env, jactionName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = pm->IsProtectedBroadcast(actionName, &result);
    if (FAILED(ec))
        ALOGE("nativeIsProtectedBroadcast() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeIsProtectedBroadcast()");
    return (jboolean)result;
}

static jstring android_content_pm_ElPackageManagerProxy_nativeGetNameForUid(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juid)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetNameForUid()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    String name;
    ECode ec = pm->GetNameForUid((Int32)juid, &name);
    if (FAILED(ec))
        ALOGE("nativeGetNameForUid() ec: 0x%08x", ec);

    jstring jname = ElUtil::GetJavaString(env, name);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetNameForUid()");
    return jname;
}

static jint android_content_pm_ElPackageManagerProxy_nativeGetUidForSharedUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jsharedUserName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetUidForSharedUser()");

    String sharedUserName = ElUtil::ToElString(env, jsharedUserName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Int32 uid = -1;
    ECode ec = pm->GetUidForSharedUser(sharedUserName, &uid);
    if (FAILED(ec))
        ALOGE("nativeGetUidForSharedUser() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetUidForSharedUser()");
    return (jint)uid;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeQueryIntentActivityOptions(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcaller, jobjectArray jspecifics, jobjectArray jspecificTypes, jobject jintent, jstring jresolvedType, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeQueryIntentActivityOptions()");

    AutoPtr<IComponentName> caller;
    if (jcaller != NULL) {
        if (!ElUtil::ToElComponentName(env, jcaller, (IComponentName**)&caller)) {
            ALOGE("nativeQueryIntentActivityOptions() ToElComponentName fail!");
        }
    }

    AutoPtr<ArrayOf<IIntent*> > specifics;
    if (jspecifics != NULL) {
        int size = env->GetArrayLength(jspecifics);
        specifics = ArrayOf<IIntent*>::Alloc(size);

        for(int i = 0; i < size; i++){
            jobject jspecific = env->GetObjectArrayElement(jspecifics, i);
            ElUtil::CheckErrorAndLog(env, "GetObjectArrayelement failed : %d!\n", __LINE__);
            AutoPtr<IIntent> specific;
            if(!ElUtil::ToElIntent(env, jspecific, (IIntent**)&specific)) {
                ALOGE("nativeQueryIntentActivityOptions() ToElIntent fail!");
            }
            specifics->Set(i, specific);
            env->DeleteLocalRef(jspecific);
        }
    }

    AutoPtr<ArrayOf<String> > specificTypes;
    if (jspecificTypes != NULL) {
        if (!ElUtil::ToElStringArray(env, jspecificTypes, (ArrayOf<String>**)&specificTypes)) {
            ALOGE("nativeQueryIntentActivityOptions() ToElStringArray fail!");
        }
    }

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeQueryIntentActivityOptions() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> container;
    ECode ec = pm->QueryIntentActivityOptions(caller, specifics, specificTypes, intent, resolvedType,
        (Int32)jflags, (Int32)juserId, (IList**)&container);
    if (FAILED(ec))
        ALOGE("nativeQueryIntentActivityOptions() ec: 0x%08x", ec);

    jobject jresvInfos = NULL;
    if (container != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jresvInfos = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        container->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            container->Get(i, (IInterface**)&obj);
            AutoPtr<IResolveInfo> resvInfo = IResolveInfo::Probe(obj);
            jobject jresvInfo = ElUtil::GetJavaResolveInfo(env, resvInfo);
            if (jresvInfo != NULL) {
                env->CallBooleanMethod(jresvInfos, mAdd, jresvInfo);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jresvInfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeQueryIntentActivityOptions()");
    return jresvInfos;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetInstalledPackages(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetInstalledPackages()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<IParceledListSlice> slice;
    pm->GetInstalledPackages((Int32)jflags, (Int32)juserId, (IParceledListSlice**)&slice);

    jobject jslice = NULL;
    if (slice != NULL) {
        jslice = ElUtil::GetJavaParceledListSlice(env, ECLSID_CPackageInfo, slice);
        if (jslice == NULL) {
             ALOGE("nativeGetInstalledPackages() jslice is NULL ");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetInstalledPackages()");
    return jslice;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetPackagesHoldingPermissions(
    JNIEnv* env, jobject clazz, jlong jproxy, jobjectArray jpermissions, jint jflags, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPackagesHoldingPermissions()");

    AutoPtr<ArrayOf<String> > permissions;
    if (jpermissions != NULL) {
        if (!ElUtil::ToElStringArray(env, jpermissions, (ArrayOf<String>**)&permissions)) {
            ALOGE("nativeGetPackagesHoldingPermissions() ToElStringArray fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<IParceledListSlice> slice;
    pm->GetPackagesHoldingPermissions(permissions, (Int32)jflags, (Int32)juserId, (IParceledListSlice**)&slice);

    jobject jslice = NULL;
    if (slice != NULL) {
        jslice = ElUtil::GetJavaParceledListSlice(env, ECLSID_CPackageInfo, slice);
        if (jslice == NULL) {
             ALOGE("nativeGetPackagesHoldingPermissions() jslice is NULL ");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPackagesHoldingPermissions()");
    return jslice;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetPersistentApplications(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPersistentApplications()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> container;
    ECode ec = pm->GetPersistentApplications((Int32)jflags, (IList**)&container);
    if (FAILED(ec))
        ALOGE("nativeGetPersistentApplications() ec: 0x%08x", ec);

    jobject jpstInfos = NULL;
    if (container != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jpstInfos = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

       Int32 size = 0;
        container->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            container->Get(i, (IInterface**)&obj);
            AutoPtr<IApplicationInfo> pstInfo = IApplicationInfo::Probe(obj);
            jobject jpstInfo = ElUtil::GetJavaApplicationInfo(env, pstInfo);
            if (jpstInfo != NULL) {
                env->CallBooleanMethod(jpstInfos, mAdd, jpstInfo);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jpstInfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPersistentApplications()");
    return jpstInfos;
}

static void android_content_pm_ElPackageManagerProxy_nativeQuerySyncProviders(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject joutNames, jobject joutInfos)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeQuerySyncProviders()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> outNames;
    AutoPtr<IList> outInfo;
    CParcelableList::New((IList**)&outNames);
    CParcelableList::New((IList**)&outInfo);

    ECode ec = pm->QuerySyncProviders(outNames, outInfo);
    if (FAILED(ec))
        ALOGE("nativeQuerySyncProviders() ec: 0x%08x", ec);

    if (outNames != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        outNames->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            outNames->Get(i, (IInterface**)&obj);
            AutoPtr<ICharSequence> cOutName = ICharSequence::Probe(obj);
            String outName;
            cOutName->ToString(&outName);

            jobject joutName = ElUtil::GetJavaString(env, outName);
            if (joutName != NULL) {
                env->CallBooleanMethod(joutNames, mAdd, joutName);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(joutName);
            }
        }
    }

    if (outInfo != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        outInfo->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            outInfo->Get(i, (IInterface**)&obj);
            AutoPtr<IProviderInfo> outInfo = IProviderInfo::Probe(obj);
            jobject joutInfo = ElUtil::GetJavaProviderInfo(env, outInfo);
            if (joutInfo != NULL) {
                env->CallBooleanMethod(joutInfos, mAdd, joutInfo);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(joutInfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeQuerySyncProviders()");
}

static jobject android_content_pm_ElPackageManagerProxy_nativeQueryContentProviders(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jprocessName, jint juid, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeQueryContentProviders()");

    String processName = ElUtil::ToElString(env, jprocessName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> container;
    ECode ec = pm->QueryContentProviders(processName, (Int32)juid, (Int32)jflags, (IList**)&container);
    if (FAILED(ec))
        ALOGE("nativeQueryContentProviders() ec: 0x%08x", ec);

    jobject jproviders = NULL;
    if (container != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jproviders = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        container->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            container->Get(i, (IInterface**)&obj);
            AutoPtr<IProviderInfo> provider = IProviderInfo::Probe(obj);
            jobject jprovider = ElUtil::GetJavaProviderInfo(env, provider);
            if (jprovider != NULL) {
                env->CallBooleanMethod(jproviders, mAdd, jprovider);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jprovider);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeQueryContentProviders()");
    return jproviders;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetInstrumentationInfo(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jclassName, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetInstrumentationInfo()");

    AutoPtr<IComponentName> className;
    if (jclassName != NULL) {
        if (!ElUtil::ToElComponentName(env, jclassName, (IComponentName**)&className)) {
            ALOGE("nativeGetInstrumentationInfo() ToElComponentName fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IInstrumentationInfo> info;
    ECode ec = pm->GetInstrumentationInfo(className, (Int32)jflags, (IInstrumentationInfo**)&info);
    if (FAILED(ec))
        ALOGE("nativeGetInstrumentationInfo() ec: 0x%08x", ec);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaInstrumentationInfo(env, info);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetInstrumentationInfo()");
    return jinfo;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeQueryInstrumentation(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtargetPackage, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeQueryInstrumentation()");

    String targetPackage = ElUtil::ToElString(env, jtargetPackage);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> container;
    ECode ec = pm->QueryInstrumentation(targetPackage, (Int32)jflags, (IList**)&container);
    if (FAILED(ec))
        ALOGE("nativeQueryInstrumentation() ec: 0x%08x", ec);

    jobject jinfos = NULL;
    if (container != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jinfos = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        container->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            container->Get(i, (IInterface**)&obj);
            AutoPtr<IInstrumentationInfo> info = IInstrumentationInfo::Probe(obj);
            jobject jinfo = ElUtil::GetJavaInstrumentationInfo(env, info);
            if (jinfo != NULL) {
                env->CallBooleanMethod(jinfos, mAdd, jinfo);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jinfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeQueryInstrumentation()");
    return jinfos;
}

static void android_content_pm_ElPackageManagerProxy_nativeInstallPackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring joriginPath, jobject jobserver, jint jflags, jstring jinstallerPackageName, jobject jverificationParams, jstring jpackageAbiOverride)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeInstallPackage()");

    String originPath = ElUtil::ToElString(env, joriginPath);

    AutoPtr<IIPackageInstallObserver2> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        ECode ec = CPackageInstallObserver2Native::New((Handle64)jvm, (Handle64)jInstance, (IIPackageInstallObserver2**)&observer);
        if (FAILED(ec)) {
            ALOGD("nativeInstallPackage() fail new CPackageInstallObserverNative ec: 0x%x", ec);
        }
    }

    String installerPackageName = ElUtil::ToElString(env, jinstallerPackageName);

    AutoPtr<IVerificationParams> verificationParams;
    if (jverificationParams != NULL) {
        if (!ElUtil::ToElVerificationParams(env, jverificationParams, (IVerificationParams**)&verificationParams)) {
            ALOGE("nativeInstallPackage: ToElVerificationParams fail");
        }
    }

    String packageAbiOverride = ElUtil::ToElString(env, jpackageAbiOverride);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->InstallPackage(originPath, observer, (Int32)jflags, installerPackageName, verificationParams, packageAbiOverride);
    if (FAILED(ec))
        ALOGE("nativeInstallPackage() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeInstallPackage()");
}

static void android_content_pm_ElPackageManagerProxy_nativeInstallPackageAsUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring joriginPath, jobject jobserver, jint jflags, jstring jinstallerPackageName,
    jobject jverificationParams, jstring jpackageAbiOverride, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeInstallPackageAsUser()");

    String originPath = ElUtil::ToElString(env, joriginPath);

    AutoPtr<IIPackageInstallObserver2> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        ECode ec = CPackageInstallObserver2Native::New((Handle64)jvm, (Handle64)jInstance, (IIPackageInstallObserver2**)&observer);
        if (FAILED(ec)) {
            ALOGD("nativeInstallPackageAsUser() fail new CPackageInstallObserverNative ec: 0x%x", ec);
        }
    }

    String installerPackageName = ElUtil::ToElString(env, jinstallerPackageName);

    AutoPtr<IVerificationParams> verificationParams;
    if (jverificationParams != NULL) {
        if (!ElUtil::ToElVerificationParams(env, jverificationParams, (IVerificationParams**)&verificationParams)) {
            ALOGE("nativeInstallPackageAsUser: ToElVerificationParams fail");
        }
    }

    String packageAbiOverride = ElUtil::ToElString(env, jpackageAbiOverride);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->InstallPackageAsUser(originPath, observer, (Int32)jflags, installerPackageName, verificationParams, packageAbiOverride, userId);
    if (FAILED(ec))
        ALOGE("nativeInstallPackageAsUser() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeInstallPackageAsUser()");
}

static void android_content_pm_ElPackageManagerProxy_nativeFinishPackageInstall(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtoken)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeFinishPackageInstall()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->FinishPackageInstall((Int32)jtoken);
    if (FAILED(ec))
        ALOGE("nativeFinishPackageInstall() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeFinishPackageInstall()");
}

static void android_content_pm_ElPackageManagerProxy_nativeSetInstallerPackageName(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtargetPackage, jstring jinstallerPackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetInstallerPackageName()");

    String targetPackage = ElUtil::ToElString(env, jtargetPackage);
    String installerPackageName = ElUtil::ToElString(env, jinstallerPackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->SetInstallerPackageName(targetPackage, installerPackageName);
    if (FAILED(ec))
        ALOGE("nativeSetInstallerPackageName() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeSetInstallerPackageName()");
}

static void android_content_pm_ElPackageManagerProxy_nativeAddPackageToPreferred(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeAddPackageToPreferred()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->AddPackageToPreferred(packageName);
    if (FAILED(ec))
        ALOGE("nativeAddPackageToPreferred() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeAddPackageToPreferred()");
}

static void android_content_pm_ElPackageManagerProxy_nativeRemovePackageFromPreferred(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeRemovePackageFromPreferred()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->RemovePackageFromPreferred(packageName);
    if (FAILED(ec))
        ALOGE("nativeremovePackageFromPreferred() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeRemovePackageFromPreferred()");
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetPreferredPackages(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPreferredPackages()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IList> container;
    ECode ec = pm->GetPreferredPackages((Int32)jflags, (IList**)&container);
    if (FAILED(ec))
        ALOGE("nativeGetPreferredPackages() ec: 0x%08x", ec);

    jobject jinfos = NULL;
    if (container != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jinfos = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);
        env->DeleteLocalRef(listKlass);

        Int32 size = 0;
        container->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            container->Get(i, (IInterface**)&obj);
            AutoPtr<IPackageInfo> info = IPackageInfo::Probe(obj);
            jobject jinfo = ElUtil::GetJavaPackageInfo(env, info);
            if (jinfo != NULL) {
                env->CallBooleanMethod(jinfos, mAdd, jinfo);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jinfo);
            }
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetPreferredPackages()");
    return jinfos;
}

static void android_content_pm_ElPackageManagerProxy_nativeResetPreferredActivities(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeResetPreferredActivities()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->ResetPreferredActivities(juserId);
    if (FAILED(ec))
        ALOGE("nativeResetPreferredActivities() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeResetPreferredActivities()");
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetLastChosenActivity(JNIEnv* env, jobject clazz, jlong jproxy, jobject jintent,
    jstring jresolvedType, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetLastChosenActivity()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<IIntent> intent;
    if(jintent != NULL) {
        if(!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeGetLastChosenActivity() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    AutoPtr<IResolveInfo> info;
    pm->GetLastChosenActivity(intent, resolvedType, (Int32)jflags, (IResolveInfo**)&info);

    jobject jInfo = NULL;
    if (info != NULL) {
        jInfo = ElUtil::GetJavaResolveInfo(env, info);
        if (jInfo == NULL) {
             ALOGE("nativeGetLastChosenActivity() jInfo is NULL ");
        }
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetLastChosenActivity()");
    return jInfo;
}

static void android_content_pm_ElPackageManagerProxy_nativeSetLastChosenActivity(JNIEnv* env, jobject clazz, jlong jproxy, jobject jintent,
    jstring jresolvedType, jint jflags, jobject jfilter, jint match, jobject jactivity)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetLastChosenActivity()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<IIntent> intent;
    if(jintent != NULL) {
        if(!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeSetLastChosenActivity() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    AutoPtr<IIntentFilter> filter;
    if (!ElUtil::ToElIntentFilter(env, jfilter, (IIntentFilter**)&filter)) {
        ALOGE("nativeSetLastChosenActivity ToElIntentFilter failed");
    }

    AutoPtr<IComponentName> activity;
    if(!ElUtil::ToElComponentName(env, jactivity, (IComponentName**)&activity)) {
        ALOGE("nativeSetLastChosenActivity() ToElComponentName failed %d!", __LINE__);
    }

    pm->SetLastChosenActivity(intent, resolvedType, (Int32)jflags, filter, match, activity);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeSetLastChosenActivity()");
}

static void android_content_pm_ElPackageManagerProxy_nativeAddPreferredActivity(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jfilter, jint jmatch, jobjectArray jset, jobject jactivity, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeAddPreferredActivity()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;

    AutoPtr<IIntentFilter> filter;
    if (!ElUtil::ToElIntentFilter(env, jfilter, (IIntentFilter**)&filter)) {
        ALOGE("nativeAddPreferredActivity ToElIntentFilter failed");
    }

    AutoPtr<ArrayOf<IComponentName*> > set;
    if (jset != NULL) {
        int size = env->GetArrayLength(jset);
        set = ArrayOf<IComponentName*>::Alloc(size);

        for(int i = 0; i < size; i++){
            jobject jcomponentName = env->GetObjectArrayElement(jset, i);
            ElUtil::CheckErrorAndLog(env, "nativeAddPreferredActivity(); GetObjectArrayelement failed : %d!\n", __LINE__);
            AutoPtr<IComponentName> componentName;
            if(!ElUtil::ToElComponentName(env, jcomponentName, (IComponentName**)&componentName)) {
                ALOGE("nativeAddPreferredActivity() ToElComponentName fail!");
            }
            set->Set(i, componentName);
            env->DeleteLocalRef(jcomponentName);
        }
    }

    AutoPtr<IComponentName> activity;
    if(!ElUtil::ToElComponentName(env, jactivity, (IComponentName**)&activity)) {
        ALOGE("nativeAddPreferredActivity() ToElComponentName failed %d!", __LINE__);
    }

    pm->AddPreferredActivity(filter, (Int32)jmatch, set, activity, (Int32)juserId);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeAddPreferredActivity()");
}

static void android_content_pm_ElPackageManagerProxy_naitveReplacePreferredActivity(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jfilter, jint jmatch, jobjectArray jset, jobject jactivity, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_naitveReplacePreferredActivity()");

    AutoPtr<IIntentFilter> filter;
    if (!ElUtil::ToElIntentFilter(env, jfilter, (IIntentFilter**)&filter)) {
        ALOGE("nativeAddPreferredActivity ToElIntentFilter failed");
    }

    AutoPtr<ArrayOf<IComponentName*> > set;
    if (jset != NULL) {
        int size = env->GetArrayLength(jset);
        set = ArrayOf<IComponentName*>::Alloc(size);

        for(int i = 0; i < size; i++){
            jobject jcomponentName = env->GetObjectArrayElement(jset, i);
            ElUtil::CheckErrorAndLog(env, "nativeAddPreferredActivity(); GetObjectArrayelement failed : %d!\n", __LINE__);
            AutoPtr<IComponentName> componentName;
            if(!ElUtil::ToElComponentName(env, jcomponentName, (IComponentName**)&componentName)) {
                ALOGE("nativeAddPreferredActivity() ToElComponentName fail!");
            }
            set->Set(i, componentName);
            env->DeleteLocalRef(jcomponentName);
        }
    }

    AutoPtr<IComponentName> activity;
    if(!ElUtil::ToElComponentName(env, jactivity, (IComponentName**)&activity)) {
        ALOGE("nativeAddPreferredActivity() ToElComponentName failed %d!", __LINE__);
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->ReplacePreferredActivity(filter, (Int32)jmatch, set, activity, userId);
    if (FAILED(ec))
        ALOGE("naitveReplacePreferredActivity() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_naitveReplacePreferredActivity()");
}

static void android_content_pm_ElPackageManagerProxy_nativeClearPackagePreferredActivities(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeClearPackagePreferredActivities()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->ClearPackagePreferredActivities(packageName);
    if (FAILED(ec))
        ALOGE("nativeClearPackagePreferredActivities() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeClearPackagePreferredActivities()");
}

static void android_content_pm_ElPackageManagerProxy_nativeSetApplicationEnabledSetting(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint jnewState, jint jflags, jint juserId, jstring jcallingPackage)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetApplicationEnabledSetting()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->SetApplicationEnabledSetting(packageName, (Int32)jnewState, (Int32)jflags, (Int32)juserId, callingPackage);
    if (FAILED(ec))
        ALOGE("nativeSetApplicationEnabledSetting() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeSetApplicationEnabledSetting()");
}

static void android_content_pm_ElPackageManagerProxy_nativeSetPackageStoppedState(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jboolean jstopped, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetPackageStoppedState()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->SetPackageStoppedState(packageName, (Boolean)jstopped, (Int32)juserId);
    if (FAILED(ec))
        ALOGE("nativeSetPackageStoppedState() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeSetPackageStoppedState()");
}

static void android_content_pm_ElPackageManagerProxy_nativeFreeStorageAndNotify(JNIEnv* env, jobject clazz, jlong jproxy,
    jlong jfreeStorageSize, jobject jobserver)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeFreeStorageAndNotify()");

    AutoPtr<IIPackageDataObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        CIPackageDataObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIPackageDataObserver**)&observer);
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->FreeStorageAndNotify((Int64)jfreeStorageSize, observer);
    if (FAILED(ec))
        ALOGE("nativeFreeStorageAndNotify() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeFreeStorageAndNotify()");
}

static void android_content_pm_ElPackageManagerProxy_nativeFreeStorage(JNIEnv* env, jobject clazz, jlong jproxy,
    jlong jfreeStorageSize, jobject jpi)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeFreeStorageAndNotify()");

    AutoPtr<IIntentSender> pi;
    if (jpi != NULL) {
        ALOGE("nativeFreeStorage() IIntentSender jpi not NULL!");
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->FreeStorage((Int64)jfreeStorageSize, pi);
    if (FAILED(ec))
        ALOGE("nativeFreeStorage() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeFreeStorageAndNotify()");
}

static void android_content_pm_ElPackageManagerProxy_nativeClearApplicationUserData(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jobject jobserver, jint juserId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeClearApplicationUserData()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIPackageDataObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        CIPackageDataObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIPackageDataObserver**)&observer);
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->ClearApplicationUserData(packageName, observer, (Int32)juserId);
    if (FAILED(ec))
        ALOGE("nativeClearApplicationUserData() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeClearApplicationUserData()");
}

static void android_content_pm_ElPackageManagerProxy_nativeEnterSafeMode(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeEnterSafeMode()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->EnterSafeMode();
    if (FAILED(ec))
        ALOGE("nativeEnterSafeMode() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeEnterSafeMode()");
}

static void android_content_pm_ElPackageManagerProxy_nativeSystemReady(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSystemReady()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->SystemReady();
    if (FAILED(ec))
        ALOGE("nativeSystemReady() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeSystemReady()");
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeHasSystemUidErrors(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeHasSystemUidErrors()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = pm->HasSystemUidErrors(&result);
    if (FAILED(ec))
        ALOGE("nativeHasSystemUidErrors() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeHasSystemUidErrors()");
    return (jboolean)result;
}

static void android_content_pm_ElPackageManagerProxy_nativePerformBootDexOpt(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativePerformBootDexOpt()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->PerformBootDexOpt();
    if (FAILED(ec))
        ALOGE("nativePerformBootDexOpt() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativePerformBootDexOpt()");
}

static jboolean android_content_pm_ElPackageManagerProxy_nativePerformDexOptIfNeeded(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jstring jinstructionSet)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativePerformDexOptIfNeeded()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    String instructionSet = ElUtil::ToElString(env, jinstructionSet);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = pm->PerformDexOptIfNeeded(packageName, instructionSet, &result);
    if (FAILED(ec))
        ALOGE("nativePerformDexOptIfNeeded() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativePerformDexOptIfNeeded()");
    return (jboolean)result;
}


static void android_content_pm_ElPackageManagerProxy_nativeForceDexOpt(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeForceDexOpt()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->ForceDexOpt(packageName);
    if (FAILED(ec))
        ALOGE("nativeForceDexOpt() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeForceDexOpt()");
}

static void android_content_pm_ElPackageManagerProxy_nativeUpdateExternalMediaStatus(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jmounted, jboolean jreportStatus)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeExternalMediaStatus()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->UpdateExternalMediaStatus((Boolean)jmounted, (Boolean)jreportStatus);
    if (FAILED(ec))
        ALOGE("nativeUpdateExternalMediaStatus() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeExternalMediaStatus()");
}

static void android_content_pm_ElPackageManagerProxy_nativeMovePackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jobject jobserver, jint jflags)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeMovePackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIPackageMoveObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        CIPackageMoveObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIPackageMoveObserver**)&observer);
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->MovePackage(packageName, observer, (Int32)jflags);
    if (FAILED(ec))
        ALOGE("nativeMovePackage() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeMovePackage()");
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeAddPermissionAsync(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jinfo)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeAddPermissionAsync()");

    AutoPtr<IPermissionInfo> info;
    if (jinfo != NULL) {
        if (!ElUtil::ToElPermissionInfo(env, jinfo, (IPermissionInfo**)&info)) {
            ALOGE("nativeAddPermissionAsync() ToElPermissionInfo fail!");
        }
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = pm->AddPermissionAsync(info, &result);
    if (FAILED(ec))
        ALOGE("nativeAddPermissionAsync() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeAddPermissionAsync()");
    return (jboolean)result;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeSetInstallLocation(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jloc)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetInstallLocation()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = pm->SetInstallLocation((Int32)jloc, &result);
    if (FAILED(ec))
        ALOGE("nativeSetInstallLocation() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeSetInstallLocation()");
    return (jboolean)result;
}

static jint android_content_pm_ElPackageManagerProxy_nativeGetInstallLocation(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetInstallLocation()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Int32 location = 0;
    ECode ec = pm->GetInstallLocation(&location);
    if (FAILED(ec))
        ALOGE("nativeGetInstallLocation() ec: 0x%08x location: %d", ec, location);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetInstallLocation()");
    return (jint)location;
}

static jint android_content_pm_ElPackageManagerProxy_nativeInstallExistingPackageAsUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeInstallExistingPackageAsUser()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Int32 result = 0;
    ECode ec = pm->InstallExistingPackageAsUser(packageName, userId, &result);
    if (FAILED(ec))
        ALOGE("nativeInstallExistingPackageAsUser() ec: 0x%08x result: %d", ec, result);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeInstallExistingPackageAsUser()");
    return (jint)result;
}

static void android_content_pm_ElPackageManagerProxy_nativeVerifyPendingInstall(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jid, jint jverificationCode)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeVerifyPendingInstall()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->VerifyPendingInstall((Int32)jid, (Int32)jverificationCode);
    if (FAILED(ec))
        ALOGE("nativeVerifyPendingInstall() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeVerifyPendingInstall()");
}

static void android_content_pm_ElPackageManagerProxy_nativeExtendVerificationTimeout(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jid, jint jverificationCodeAtTimeout, jlong jmillisecondsToDelay)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeExtendVerificationTimeout()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->ExtendVerificationTimeout((Int32)jid, (Int32)jverificationCodeAtTimeout, (Int64)jmillisecondsToDelay);
    if (FAILED(ec))
        ALOGE("nativeExtendVerificationTimeout() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeExtendVerificationTimeout()");
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetVerifierDeviceIdentity(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetVerifierDeviceIdentity()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IVerifierDeviceIdentity> identity;
    ECode ec = pm->GetVerifierDeviceIdentity((IVerifierDeviceIdentity**)&identity);
    if (FAILED(ec))
        ALOGE("nativeGetVerifierDeviceIdentity() ec: 0x%08x", ec);

    jobject jidentity = NULL;
    if (identity != NULL) {
        jidentity = ElUtil::GetJavaVerifierDeviceIdentity(env, identity);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeGetVerifierDeviceIdentity()");
    return jidentity;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsFirstBoot(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsFirstBoot()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean isFirstBoot = FALSE;
    ECode ec = pm->IsFirstBoot(&isFirstBoot);
    if (FAILED(ec))
        ALOGE("nativeIsFirstBoot() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeIsFirstBoot()");
    return (jboolean)isFirstBoot;
}

static void android_content_pm_ElPackageManagerProxy_nativeSetPermissionEnforced(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpermission, jboolean jenforced)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetPermissionEnforced()");

    String permission = ElUtil::ToElString(env, jpermission);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->SetPermissionEnforced(permission, (Boolean)jenforced);
    if (FAILED(ec))
        ALOGE("nativeSetPermissionEnforced() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeSetPermissionEnforced()");
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsStorageLow(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsStorageLow()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean isStorageLow = FALSE;
    ECode ec = pm->IsStorageLow(&isStorageLow);
    if (FAILED(ec))
        ALOGE("nativeIsStorageLow() ec: 0x%08x", ec);

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeIsStorageLow()");
    return (jboolean)isStorageLow;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeSetApplicationHiddenSettingAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jboolean hidden, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetApplicationHiddenSettingAsUser()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    ECode ec = pm->SetApplicationHiddenSettingAsUser(packageName, hidden, userId, &result);
    if (FAILED(ec))
        ALOGE("nativeSetApplicationHiddenSettingAsUser() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeSetApplicationHiddenSettingAsUser()");
    return result;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeGetApplicationHiddenSettingAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetApplicationHiddenSettingAsUser()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    ECode ec = pm->GetApplicationHiddenSettingAsUser(packageName, userId, &result);
    if (FAILED(ec))
        ALOGE("nativeGetApplicationHiddenSettingAsUser() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeGetApplicationHiddenSettingAsUser()");
    return result;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetPackageInstaller(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetPackageInstaller()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IIPackageInstaller> installer;
    ECode ec = pm->GetPackageInstaller((IIPackageInstaller**)&installer);
    if (FAILED(ec))
        ALOGE("nativeGetPackageInstaller() ec: ox%08x", ec);

    jobject jinstaller = NULL;
    if (installer != NULL) {
        jclass piKlass = env->FindClass("android/content/pm/ElPackageInstallerProxy");
        ElUtil::CheckErrorAndLog(env, "nativeGetPackageInstaller Fail FindClass: ElPackageInstallerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(piKlass, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "nativeGetPackageInstaller Fail GetMethodID: ElPackageInstallerProxy : %d!\n", __LINE__);

        jinstaller = env->NewObject(piKlass, m, (jlong)installer.Get());
        ElUtil::CheckErrorAndLog(env, "nativeGetPackageInstaller Fail NewObject: ElPackageInstallerProxy : %d!\n", __LINE__);
        installer->AddRef();

        env->DeleteLocalRef(piKlass);
    }

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeGetPackageInstaller()");
    return jinstaller;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeSetBlockUninstallForUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jboolean blockUninstall, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetBlockUninstallForUser()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    ECode ec = pm->SetBlockUninstallForUser(packageName, blockUninstall, userId, &result);
    if (FAILED(ec))
        ALOGE("nativeSetBlockUninstallForUser() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeSetBlockUninstallForUser()");
    return result;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeGetBlockUninstallForUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetBlockUninstallForUser()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    ECode ec = pm->GetBlockUninstallForUser(packageName, userId, &result);
    if (FAILED(ec))
        ALOGE("nativeGetBlockUninstallForUser() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeGetBlockUninstallForUser()");
    return result;
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetKeySetByAlias(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jstring jalias)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetKeySetByAlias()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    String alias = ElUtil::ToElString(env, jalias);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IKeySet> keySet;
    ECode ec = pm->GetKeySetByAlias(packageName, alias, (IKeySet**)&keySet);
    if (FAILED(ec))
        ALOGE("nativeGetKeySetByAlias() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeGetKeySetByAlias()");
    return ElUtil::GetJavaKeySet(env, keySet);
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetSigningKeySet(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetSigningKeySet()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
     AutoPtr<IKeySet> keySet;
    ECode ec = pm->GetSigningKeySet(packageName, (IKeySet**)&keySet);
    if (FAILED(ec))
        ALOGE("nativeGetSigningKeySet() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeGetSigningKeySet()");
    return ElUtil::GetJavaKeySet(env, keySet);
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsPackageSignedByKeySet(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jobject jks)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsPackageSignedByKeySet()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IKeySet> ks;
    if (jks != NULL) {
        if (!ElUtil::ToElKeySet(env, jks, (IKeySet**)&ks))
            ALOGE("nativeIsPackageSignedByKeySet ToElKeySet fail");
    }
    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    ECode ec = pm->IsPackageSignedByKeySet(packageName, ks, &result);
    if (FAILED(ec))
        ALOGE("nativeIsPackageSignedByKeySet() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeIsPackageSignedByKeySet()");
    return result;
}

static jboolean android_content_pm_ElPackageManagerProxy_nativeIsPackageSignedByKeySetExactly(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jobject jks)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeIsPackageSignedByKeySetExactly()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IKeySet> ks;
    if (jks != NULL) {
        if (!ElUtil::ToElKeySet(env, jks, (IKeySet**)&ks))
            ALOGE("nativeIsPackageSignedByKeySetExactly ToElKeySet fail");
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Boolean result;
    ECode ec = pm->IsPackageSignedByKeySetExactly(packageName, ks, &result);
    if (FAILED(ec))
        ALOGE("nativeIsPackageSignedByKeySetExactly() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeIsPackageSignedByKeySetExactly()");
    return result;
}

static void android_content_pm_ElPackageManagerProxy_nativeSetComponentProtectedSetting(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcomponentName, jboolean newState, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetComponentProtectedSetting()");

    AutoPtr<IComponentName> componentName;
    if (jcomponentName != NULL) {
        if (!ElUtil::ToElComponentName(env, jcomponentName, (IComponentName**)&componentName))
            ALOGE("nativeIsPackageSignedByComponentNameExactly ToElComponentName fail");
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->SetComponentProtectedSetting(componentName, newState, userId);
    if (FAILED(ec))
        ALOGE("nativeSetComponentProtectedSetting() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeSetComponentProtectedSetting()");
}

static void android_content_pm_ElPackageManagerProxy_nativeUpdateIconMapping(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeUpdateIconMapping()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->UpdateIconMapping(packageName);
    if (FAILED(ec))
        ALOGE("nativeUpdateIconMapping() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeUpdateIconMapping()");
}

static jobject android_content_pm_ElPackageManagerProxy_nativeGetComposedIconInfo(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeGetComposedIconInfo()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    AutoPtr<IComposedIconInfo> info;
    ECode ec = pm->GetComposedIconInfo((IComposedIconInfo**)&info);
    if (FAILED(ec))
        ALOGE("nativeGetComposedIconInfo() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeGetComposedIconInfo()");
    return ElUtil::GetJavaComposedIconInfo(env, info);
}

static jint android_content_pm_ElPackageManagerProxy_nativeProcessThemeResources(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jthemePkgName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeProcessThemeResources()");

    String themePkgName = ElUtil::ToElString(env, jthemePkgName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    Int32 result;
    ECode ec = pm->ProcessThemeResources(themePkgName, &result);
    if (FAILED(ec))
        ALOGE("nativeProcessThemeResources() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeProcessThemeResources()");
    return result;
}

static void android_content_pm_ElPackageManagerProxy_nativeSetPreLaunchCheckActivity(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcomponentName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeSetPreLaunchCheckActivity()");

    AutoPtr<IComponentName> componentName;
    if (jcomponentName != NULL) {
        if (!ElUtil::ToElComponentName(env, jcomponentName, (IComponentName**)&componentName))
            ALOGE("nativeSetPreLaunchCheckActivity ToElComponentName fail");
    }

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->SetPreLaunchCheckActivity(componentName);
    if (FAILED(ec))
        ALOGE("nativeSetPreLaunchCheckActivity() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeSetPreLaunchCheckActivity()");
}

static void android_content_pm_ElPackageManagerProxy_nativeAddPreLaunchCheckPackage(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeAddPreLaunchCheckPackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->AddPreLaunchCheckPackage(packageName);
    if (FAILED(ec))
        ALOGE("nativeAddPreLaunchCheckPackage() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeAddPreLaunchCheckPackage()");
}

static void android_content_pm_ElPackageManagerProxy_nativeRemovePreLaunchCheckPackage(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeRemovePreLaunchCheckPackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->RemovePreLaunchCheckPackage(packageName);
    if (FAILED(ec))
        ALOGE("nativeRemovePreLaunchCheckPackage() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeRemovePreLaunchCheckPackage()");
}

static void android_content_pm_ElPackageManagerProxy_nativeClearPreLaunchCheckPackages(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeClearPreLaunchCheckPackages()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    ECode ec = pm->ClearPreLaunchCheckPackages();
    if (FAILED(ec))
        ALOGE("nativeClearPreLaunchCheckPackages() ec: ox%08x", ec);

    // ALOGD("-android_content_pm_ElPackageManagerProxy_nativeClearPreLaunchCheckPackages()");
}

static void android_content_pm_ElPackageManagerProxy_nativeParseSignatureByJava(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageManagerProxy_nativeParseSignatureByJava()");

    IIPackageManager* pm = (IIPackageManager*)jproxy;
    if (pm != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        AutoPtr<IElSignatureParser> parser;
        CElSignatureParser::New((Handle64)jvm, (IElSignatureParser**)&parser);
        pm->ParseSignatureByJava(parser);
    }

    // ALOGD("- android_content_pm_ElPackageManagerProxy_nativeParseSignatureByJava()");
}

static void android_content_ElPackageManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_content_ElPackageManagerProxy_nativeFinalize()");

    IIPackageManager* obj = (IIPackageManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_content_ElPackageManagerProxy_nativeFinalize()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_content_ElPackageManagerProxy_nativeFinalize },
    { "nativeIsPackageAvailable",    "(JLjava/lang/String;I)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsPackageAvailable },
    { "nativeGetPackagesForUid",    "(JI)[Ljava/lang/String;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPackagesForUid },
    { "nativeGetPackageInfo",    "(JLjava/lang/String;II)Landroid/content/pm/PackageInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPackageInfo },
    { "nativeGetActivityInfo",    "(JLandroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetActivityInfo },
    { "nativeActivitySupportsIntent",    "(JLandroid/content/ComponentName;Landroid/content/Intent;Ljava/lang/String;)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeActivitySupportsIntent },
    { "nativeQueryIntentActivities",    "(JLandroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeQueryIntentActivities },
    { "nativeIsSafeMode",    "(J)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsSafeMode },
    { "nativeGetFlagsForUid",    "(JI)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetFlagsForUid },
    { "nativeIsUidPrivileged",    "(JI)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsUidPrivileged },
    { "nativeGetAppOpPermissionPackages",    "(JLjava/lang/String;)[Ljava/lang/String;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetAppOpPermissionPackages },
    { "nativeResolveIntent",    "(JLandroid/content/Intent;Ljava/lang/String;II)Landroid/content/pm/ResolveInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeResolveIntent },
    { "nativeCanForwardTo",    "(JLandroid/content/Intent;Ljava/lang/String;II)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeCanForwardTo },
    { "nativeGetApplicationInfo",    "(JLjava/lang/String;II)Landroid/content/pm/ApplicationInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetApplicationInfo },
    { "nativeGetInstalledApplications",    "(JII)Landroid/content/pm/ParceledListSlice;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetInstalledApplications },
    { "nativeGetPackageSizeInfo",    "(JLjava/lang/String;ILandroid/content/pm/IPackageStatsObserver;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPackageSizeInfo },
    { "nativeCurrentToCanonicalPackageNames",    "(J[Ljava/lang/String;)[Ljava/lang/String;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeCurrentToCanonicalPackageNames },
    { "nativeCheckPermission",    "(JLjava/lang/String;Ljava/lang/String;)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeCheckPermission },
    { "nativeHasSystemFeature",    "(JLjava/lang/String;)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeHasSystemFeature },
    { "nativeResolveContentProvider",    "(JLjava/lang/String;II)Landroid/content/pm/ProviderInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeResolveContentProvider },
    { "nativeQueryIntentServices",    "(JLandroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeQueryIntentServices },
    { "nativeQueryIntentContentProviders",    "(JLandroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeQueryIntentContentProviders },
    { "nativeGetComponentEnabledSetting",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetComponentEnabledSetting },
    { "nativeGetServiceInfo",    "(JLandroid/content/ComponentName;II)Landroid/content/pm/ServiceInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetServiceInfo },
    { "nativeGetReceiverInfo",    "(JLandroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetReceiverInfo },
    { "nativeQueryIntentReceivers",    "(JLandroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeQueryIntentReceivers },
    { "nativeGetPermissionInfo",    "(JLjava/lang/String;I)Landroid/content/pm/PermissionInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPermissionInfo },
    { "nativeIsPermissionEnforced",    "(JLjava/lang/String;)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsPermissionEnforced },
    { "nativeGetPermissionGroupInfo",    "(JLjava/lang/String;I)Landroid/content/pm/PermissionGroupInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPermissionGroupInfo },
    { "nativeGetPreferredActivities",    "(JLjava/util/List;Ljava/util/List;Ljava/lang/String;)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPreferredActivities },
    { "nativeAddPersistentPreferredActivity",    "(JLandroid/content/IntentFilter;Landroid/content/ComponentName;I)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeAddPersistentPreferredActivity },
    { "nativeClearPackagePersistentPreferredActivities",    "(JLjava/lang/String;I)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeClearPackagePersistentPreferredActivities },
    { "nativeAddCrossProfileIntentFilter",    "(JLandroid/content/IntentFilter;Ljava/lang/String;IIII)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeAddCrossProfileIntentFilter },
    { "nativeClearCrossProfileIntentFilters",    "(JILjava/lang/String;I)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeClearCrossProfileIntentFilters },
    { "nativeGetHomeActivities",    "(JLjava/util/List;)Landroid/content/ComponentName;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetHomeActivities },
    { "nativeSetComponentEnabledSetting",    "(JLandroid/content/ComponentName;III)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetComponentEnabledSetting },
    { "nativeGetApplicationEnabledSetting",    "(JLjava/lang/String;I)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetApplicationEnabledSetting },
    { "nativeGetInstallerPackageName",    "(JLjava/lang/String;)Ljava/lang/String;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetInstallerPackageName },
    { "nativeResolveService",    "(JLandroid/content/Intent;Ljava/lang/String;II)Landroid/content/pm/ResolveInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeResolveService },
    { "nativeIsOnlyCoreApps",    "(J)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsOnlyCoreApps },
    { "nativeDeletePackageAsUser",    "(JLjava/lang/String;Landroid/content/pm/IPackageDeleteObserver;II)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeDeletePackageAsUser },
    { "nativeDeletePackage",    "(JLjava/lang/String;Landroid/content/pm/IPackageDeleteObserver2;II)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeDeletePackage },
    { "nativeCanonicalToCurrentPackageNames",    "(J[Ljava/lang/String;)[Ljava/lang/String;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeCanonicalToCurrentPackageNames },
    { "nativeCheckSignatures",    "(JLjava/lang/String;Ljava/lang/String;)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeCheckSignatures },
    { "nativeCheckUidSignatures",    "(JII)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeCheckUidSignatures },
    { "nativeDeleteApplicationCacheFiles",    "(JLjava/lang/String;Landroid/content/pm/IPackageDataObserver;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeDeleteApplicationCacheFiles },
    { "nativeGetSystemSharedLibraryNames",    "(J)[Ljava/lang/String;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetSystemSharedLibraryNames },
    { "nativeGetSystemAvailableFeatures",    "(J)[Landroid/content/pm/FeatureInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetSystemAvailableFeatures },
    { "nativeGetPackageUid",    "(JLjava/lang/String;I)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPackageUid },
    { "nativeGetPackageGids",    "(JLjava/lang/String;)[I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPackageGids },
    { "nativeNextPackageToClean",    "(JLandroid/content/pm/PackageCleanItem;)Landroid/content/pm/PackageCleanItem;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeNextPackageToClean },
    { "nativeQueryPermissionsByGroup",    "(JLjava/lang/String;I)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeQueryPermissionsByGroup },
    { "nativeGetAllPermissionGroups",    "(JI)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetAllPermissionGroups },
    { "nativeGetProviderInfo",    "(JLandroid/content/ComponentName;II)Landroid/content/pm/ProviderInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetProviderInfo },
    { "nativeCheckUidPermission",    "(JLjava/lang/String;I)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeCheckUidPermission },
    { "nativeAddPermission",    "(JLandroid/content/pm/PermissionInfo;)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeAddPermission },
    { "nativeRemovePermission",    "(JLjava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeRemovePermission },
    { "nativeGrantPermission",    "(JLjava/lang/String;Ljava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGrantPermission },
    { "nativeRevokePermission",    "(JLjava/lang/String;Ljava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeRevokePermission },
    { "nativeIsProtectedBroadcast",    "(JLjava/lang/String;)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsProtectedBroadcast },
    { "nativeGetNameForUid",    "(JI)Ljava/lang/String;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetNameForUid },
    { "nativeGetUidForSharedUser",    "(JLjava/lang/String;)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetUidForSharedUser },
    { "nativeQueryIntentActivityOptions",    "(JLandroid/content/ComponentName;[Landroid/content/Intent;[Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeQueryIntentActivityOptions },
    { "nativeGetInstalledPackages",    "(JII)Landroid/content/pm/ParceledListSlice;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetInstalledPackages },
    { "nativeGetPackagesHoldingPermissions",    "(J[Ljava/lang/String;II)Landroid/content/pm/ParceledListSlice;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPackagesHoldingPermissions },
    { "nativeGetPersistentApplications",    "(JI)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPersistentApplications },
    { "nativeQuerySyncProviders",    "(JLjava/util/List;Ljava/util/List;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeQuerySyncProviders },
    { "nativeQueryContentProviders",    "(JLjava/lang/String;II)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeQueryContentProviders },
    { "nativeGetInstrumentationInfo",    "(JLandroid/content/ComponentName;I)Landroid/content/pm/InstrumentationInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetInstrumentationInfo },
    { "nativeQueryInstrumentation",    "(JLjava/lang/String;I)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeQueryInstrumentation },
    { "nativeInstallPackage",    "(JLjava/lang/String;Landroid/content/pm/IPackageInstallObserver2;ILjava/lang/String;Landroid/content/pm/VerificationParams;Ljava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeInstallPackage },
    { "nativeInstallPackageAsUser",    "(JLjava/lang/String;Landroid/content/pm/IPackageInstallObserver2;ILjava/lang/String;Landroid/content/pm/VerificationParams;Ljava/lang/String;I)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeInstallPackageAsUser },
    { "nativeFinishPackageInstall",    "(JI)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeFinishPackageInstall },
    { "nativeSetInstallerPackageName",    "(JLjava/lang/String;Ljava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetInstallerPackageName },
    { "nativeAddPackageToPreferred",    "(JLjava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeAddPackageToPreferred },
    { "nativeRemovePackageFromPreferred",    "(JLjava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeRemovePackageFromPreferred },
    { "nativeGetPreferredPackages",    "(JI)Ljava/util/List;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPreferredPackages },
    { "nativeResetPreferredActivities",    "(JI)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeResetPreferredActivities },
    { "nativeGetLastChosenActivity",    "(JLandroid/content/Intent;Ljava/lang/String;I)Landroid/content/pm/ResolveInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetLastChosenActivity },
    { "nativeSetLastChosenActivity",    "(JLandroid/content/Intent;Ljava/lang/String;ILandroid/content/IntentFilter;ILandroid/content/ComponentName;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetLastChosenActivity },
    { "nativeAddPreferredActivity",    "(JLandroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;I)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeAddPreferredActivity },
    { "naitveReplacePreferredActivity",    "(JLandroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;I)V",
            (void*) android_content_pm_ElPackageManagerProxy_naitveReplacePreferredActivity },
    { "nativeClearPackagePreferredActivities",    "(JLjava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeClearPackagePreferredActivities },
    { "nativeSetApplicationEnabledSetting",    "(JLjava/lang/String;IIILjava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetApplicationEnabledSetting },
    { "nativeSetPackageStoppedState",    "(JLjava/lang/String;ZI)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetPackageStoppedState },
    { "nativeFreeStorageAndNotify",    "(JJLandroid/content/pm/IPackageDataObserver;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeFreeStorageAndNotify },
    { "nativeFreeStorage",    "(JJLandroid/content/IntentSender;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeFreeStorage },
    { "nativeClearApplicationUserData",    "(JLjava/lang/String;Landroid/content/pm/IPackageDataObserver;I)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeClearApplicationUserData },
    { "nativeEnterSafeMode",    "(J)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeEnterSafeMode },
    { "nativeSystemReady",    "(J)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSystemReady },
    { "nativeHasSystemUidErrors",    "(J)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeHasSystemUidErrors },
    { "nativePerformBootDexOpt",    "(J)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativePerformBootDexOpt },
    { "nativePerformDexOptIfNeeded",    "(JLjava/lang/String;Ljava/lang/String;)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativePerformDexOptIfNeeded },
    { "nativeForceDexOpt",    "(JLjava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeForceDexOpt },
    { "nativeUpdateExternalMediaStatus",    "(JZZ)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeUpdateExternalMediaStatus },
    { "nativeMovePackage",    "(JLjava/lang/String;Landroid/content/pm/IPackageMoveObserver;I)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeMovePackage },
    { "nativeAddPermissionAsync",    "(JLandroid/content/pm/PermissionInfo;)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeAddPermissionAsync },
    { "nativeSetInstallLocation",    "(JI)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetInstallLocation },
    { "nativeGetInstallLocation",    "(J)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetInstallLocation },
    { "nativeInstallExistingPackageAsUser",    "(JLjava/lang/String;I)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeInstallExistingPackageAsUser },
    { "nativeVerifyPendingInstall",    "(JII)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeVerifyPendingInstall },
    { "nativeExtendVerificationTimeout",    "(JIIJ)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeExtendVerificationTimeout },
    { "nativeGetVerifierDeviceIdentity",    "(J)Landroid/content/pm/VerifierDeviceIdentity;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetVerifierDeviceIdentity },
    { "nativeIsFirstBoot",    "(J)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsFirstBoot },
    { "nativeSetPermissionEnforced",    "(JLjava/lang/String;Z)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetPermissionEnforced },
    { "nativeIsStorageLow",    "(J)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsStorageLow },
    { "nativeSetApplicationHiddenSettingAsUser",    "(JLjava/lang/String;ZI)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetApplicationHiddenSettingAsUser },
    { "nativeGetApplicationHiddenSettingAsUser",    "(JLjava/lang/String;I)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetApplicationHiddenSettingAsUser },
    { "nativeGetPackageInstaller",    "(J)Landroid/content/pm/IPackageInstaller;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetPackageInstaller },
    { "nativeSetBlockUninstallForUser",    "(JLjava/lang/String;ZI)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetBlockUninstallForUser },
    { "nativeGetBlockUninstallForUser",    "(JLjava/lang/String;I)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetBlockUninstallForUser },
    { "nativeGetKeySetByAlias",    "(JLjava/lang/String;Ljava/lang/String;)Landroid/content/pm/KeySet;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetKeySetByAlias },
    { "nativeGetSigningKeySet",    "(JLjava/lang/String;)Landroid/content/pm/KeySet;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetSigningKeySet },
    { "nativeIsPackageSignedByKeySet",    "(JLjava/lang/String;Landroid/content/pm/KeySet;)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsPackageSignedByKeySet },
    { "nativeIsPackageSignedByKeySetExactly",    "(JLjava/lang/String;Landroid/content/pm/KeySet;)Z",
            (void*) android_content_pm_ElPackageManagerProxy_nativeIsPackageSignedByKeySetExactly },
    { "nativeSetComponentProtectedSetting",    "(JLandroid/content/ComponentName;ZI)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetComponentProtectedSetting },
    { "nativeUpdateIconMapping",    "(JLjava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeUpdateIconMapping },
    { "nativeGetComposedIconInfo",    "(J)Landroid/app/ComposedIconInfo;",
            (void*) android_content_pm_ElPackageManagerProxy_nativeGetComposedIconInfo },
    { "nativeProcessThemeResources",    "(JLjava/lang/String;)I",
            (void*) android_content_pm_ElPackageManagerProxy_nativeProcessThemeResources },
    { "nativeSetPreLaunchCheckActivity",    "(JLandroid/content/ComponentName;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeSetPreLaunchCheckActivity },
    { "nativeAddPreLaunchCheckPackage",    "(JLjava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeAddPreLaunchCheckPackage },
    { "nativeRemovePreLaunchCheckPackage",    "(JLjava/lang/String;)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeRemovePreLaunchCheckPackage },
    { "nativeClearPreLaunchCheckPackages",    "(J)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeClearPreLaunchCheckPackages },
    { "nativeParseSignatureByJava",    "(J)V",
            (void*) android_content_pm_ElPackageManagerProxy_nativeParseSignatureByJava },
};

int register_android_content_pm_ElPackageManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/content/pm/ElPackageManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

