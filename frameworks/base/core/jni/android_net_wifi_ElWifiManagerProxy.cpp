
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <elastos/utility/etl/HashMap.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Wifi.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Utility.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Wifi::IIWifiManager;
using Elastos::Droid::Wifi::IWifiInfo;
using Elastos::Droid::Wifi::IWifiConfiguration;
using Elastos::Droid::Wifi::IScanResult;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Os::IMessenger;
using Elastos::Droid::JavaProxy::CBinderNative;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, AutoPtr<IBinder> > sLock;
static Mutex sLockLock;

static jint android_net_wifi_ElWifiManagerProxy_nativeGetSupportedFeatures(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetSupportedFeatures()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    Int32 result;
    wifi->GetSupportedFeatures(&result);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetSupportedFeatures()");
    return (jint)result;
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeReportActivityInfo(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeReportActivityInfo()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    AutoPtr<IWifiActivityEnergyInfo> info;
    wifi->ReportActivityInfo((IWifiActivityEnergyInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaWifiActivityEnergyInfo(env, info);
    }

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeReportActivityInfo()");
    return jinfo;
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetWifiServiceMessenger(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetWifiServiceMessenger()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    AutoPtr<IMessenger> messenger;
    wifi->GetWifiServiceMessenger((IMessenger**)&messenger);
    jobject jmessenger = NULL;
    if (messenger != NULL) {
        jmessenger = ElUtil::GetJavaMessenger(env,messenger);
    }

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetWifiServiceMessenger()");
    return jmessenger;
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetConnectionInfo(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetConnectionInfo()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    AutoPtr<IWifiInfo> info;
    wifi->GetConnectionInfo((IWifiInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaWifiInfo(env,info);
    }

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetConnectionInfo()");
    return jinfo;
}

static jint android_net_wifi_ElWifiManagerProxy_nativeGetWifiEnabledState(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetWifiEnabledState()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    Int32 state;
    wifi->GetWifiEnabledState(&state);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetWifiEnabledState()");
    return (jint)state;
}

static jint android_net_wifi_ElWifiManagerProxy_nativeGetWifiApEnabledState(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetWifiApEnabledState()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    Int32 state;
    wifi->GetWifiApEnabledState(&state);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetWifiApEnabledState()");
    return (jint)state;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeSetWifiEnabled(JNIEnv* env, jobject clazz, jlong jproxy, jboolean jenable)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeSetWifiEnabled()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    Boolean result;
    wifi->SetWifiEnabled((Boolean)jenable, &result);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeSetWifiEnabled()");
    return (jboolean)result;
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetChannelList(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetChannelList()");

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetChannelList Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetChannelList Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jlist = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetChannelList Fail NewObject: ArrayList : %d!\n", __LINE__);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    AutoPtr<IList> list;
    wifi->GetChannelList((IList**)&list);
    if (list != NULL) {
        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetChannelList GetMethodID: add : %d!\n", __LINE__);

        Int32 size;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IWifiChannel> channel = IWifiChannel::Probe(obj);
            if (channel != NULL) {
                jobject jchannel = ElUtil::GetJavaWifiChannel(env, channel);
                if (jchannel != NULL) {
                    env->CallBooleanMethod(jlist, mAdd, jchannel);
                    ElUtil::CheckErrorAndLog(env, "nativeGetChannelList Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(jchannel);
                }
                else {
                    ALOGE("nativeGetChannelList() jchannel is NULL!");
                }
            }
        }
    }
    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetChannelList()");
    return jlist;
}

static void android_net_wifi_ElWifiManagerProxy_nativeStartScan(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jrequested, jobject jws)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeStartScan()");

    AutoPtr<IScanSettings> requested;
    if (jrequested != NULL) {
        if (!ElUtil::ToElScanSettings(env, jrequested, (IScanSettings**)&requested))
            ALOGE("nativeStartScan() ToElScanSettings fail!");
    }

    AutoPtr<IWorkSource> ws;
    if (jws != NULL) {
        if (!ElUtil::ToElWorkSource(env, jws, (IWorkSource**)&ws)) {
            ALOGE("nativeStartScan() ToElWorkSource fail!");
        }
    }

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    wifi->StartScan(requested, ws);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeStartScan()");
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetScanResults(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jcallingPackage)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetScanResults()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetScanResults Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetScanResults Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jresults = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetScanResults Fail NewObject: ArrayList : %d!\n", __LINE__);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    AutoPtr<IList> list;
    wifi->GetScanResults(callingPackage, (IList**)&list);
    if (list != NULL) {
        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetScanResults Fail GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IScanResult> result = IScanResult::Probe(obj);
            jobject jresult = ElUtil::GetJavaScanResult(env, result);
            if (jresult != NULL) {
                env->CallBooleanMethod(jresults, mAdd, jresult);
                ElUtil::CheckErrorAndLog(env, "nativeGetScanResults Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jresult);
            }
        }
    }

    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetScanResults()");
    return jresults;
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetConfiguredNetworks(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetConfiguredNetworks()");

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetConfiguredNetworks Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetConfiguredNetworks Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jlist = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetConfiguredNetworks Fail NewObject: ArrayList : %d!\n", __LINE__);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    AutoPtr<IList> list;
    wifi->GetConfiguredNetworks((IList**)&list);
    if (list != NULL) {
        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetConfiguredNetworks GetMethodID: add : %d!\n", __LINE__);

        Int32 size;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IWifiConfiguration> config = IWifiConfiguration::Probe(obj);
            if (config != NULL) {
                jobject jconfig = ElUtil::GetJavaWifiConfiguration(env, config);
                if (jconfig != NULL) {
                    env->CallBooleanMethod(jlist, mAdd, jconfig);
                    ElUtil::CheckErrorAndLog(env, "nativeGetConfiguredNetworks Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(jconfig);
                }
                else {
                    ALOGE("nativeGetConfiguredNetworks() jconfig is NULL!");
                }
            }
        }
    }
    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetConfiguredNetworks()");
    return jlist;
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetPrivilegedConfiguredNetworks(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetPrivilegedConfiguredNetworks()");

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetPrivilegedConfiguredNetworks Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetPrivilegedConfiguredNetworks Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jlist = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetPrivilegedConfiguredNetworks Fail NewObject: ArrayList : %d!\n", __LINE__);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    AutoPtr<IList> list;
    wifi->GetPrivilegedConfiguredNetworks((IList**)&list);
    if (list != NULL) {
        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetPrivilegedConfiguredNetworks GetMethodID: add : %d!\n", __LINE__);

        Int32 size;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IWifiConfiguration> config = IWifiConfiguration::Probe(obj);
            if (config != NULL) {
                jobject jconfig = ElUtil::GetJavaWifiConfiguration(env, config);
                if (jconfig != NULL) {
                    env->CallBooleanMethod(jlist, mAdd, jconfig);
                    ElUtil::CheckErrorAndLog(env, "nativeGetPrivilegedConfiguredNetworks Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(jconfig);
                }
                else {
                    ALOGE("nativeGetPrivilegedConfiguredNetworks() jconfig is NULL!");
                }
            }
        }
    }
    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetPrivilegedConfiguredNetworks()");
    return jlist;
}

static void android_net_wifi_ElWifiManagerProxy_nativeStartWifi(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeStartWifi()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    wifi->StartWifi();

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeStartWifi()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeStopWifi(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeStopWifi()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    wifi->StopWifi();

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeStopWifi()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeDisconnect(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeDisconnect()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    wifi->Disconnect();

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeDisconnect()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeReconnect(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeReconnect()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    wifi->Reconnect();

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeReconnect()");
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeEnableNetwork(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jnetId, jboolean jdisableOthers)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeEnableNetwork()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    Boolean result;
    wifi->EnableNetwork((Int32)jnetId, (Boolean)jdisableOthers, &result);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeEnableNetwork()");
    return (jboolean)result;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeDisableNetwork(JNIEnv* env, jobject clazz, jlong jproxy, jint jnetId)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeDisableNetwork()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    Boolean result;
    wifi->DisableNetwork((Int32)jnetId, &result);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeDisableNetwork()");
    return (jboolean)result;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeRemoveNetwork(JNIEnv* env, jobject clazz, jlong jproxy, jint jnetId)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeRemoveNetwork()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    Boolean result;
    wifi->RemoveNetwork((Int32)jnetId, &result);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeRemoveNetwork()");
    return (jboolean)result;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeAcquireWifiLock(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlock, jint jlockType, jstring jtag, jobject jws)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeAcquireWifiLock()");

    AutoPtr<IBinder> lock;
    if (jlock != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlock);

        Mutex::Autolock alock(sLockLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sLock.Find(hashCode);
        if (it != sLock.End()) {
            lock = it->mSecond;
        } else {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);

            jobject jInstance = env->NewGlobalRef(jlock);
            CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&lock);
            lock->AddRef();

            sLock[hashCode] = lock;
        }
    }

    String tag = ElUtil::ToElString(env, jtag);

    AutoPtr<IWorkSource> ws;
    if (jws != NULL) {
        if (!ElUtil::ToElWorkSource(env, jws, (IWorkSource**)&ws)) {
            ALOGE("nativeAcquireWakeLock() ToElWorkSource fail!");
        }
    }

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    Boolean result;
    wifi->AcquireWifiLock(lock, (Int32)jlockType, tag, ws, &result);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeAcquireWifiLock()");
    return (jboolean)result;
}

static void android_net_wifi_ElWifiManagerProxy_nativeUpdateWifiLockWorkSource(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlock, jobject jws)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeUpdateWifiLockWorkSource()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jlock);

    AutoPtr<IBinder> lock;
    {
        Mutex::Autolock alock(sLockLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sLock.Find(hashCode);
        if (it != sLock.End()) {
            lock = it->mSecond;
        }
    }

    if (NULL == lock) {
        ALOGE("nativeUpdateWakeLockWorkSource() Invalid jlock!\n");
        env->ExceptionDescribe();
        return;
    }

    AutoPtr<IWorkSource> ws;
    if (jws != NULL) {
        if (!ElUtil::ToElWorkSource(env, jws, (IWorkSource**)&ws)) {
            ALOGE("nativeUpdateWakeLockWorkSource() ToElWorkSource fail!");
        }
    }

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    ECode ec = wifi->UpdateWifiLockWorkSource(lock, ws);
    if (FAILED(ec))
        ALOGE("nativeUpdateWifiLockWorkSource() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeUpdateWifiLockWorkSource()");
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeReleaseWifiLock(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlock)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeReleaseWifiLock()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jlock);

    AutoPtr<IBinder> lock;
    {
        Mutex::Autolock alock(sLockLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sLock.Find(hashCode);
        if (it != sLock.End()) {
            lock = it->mSecond;
            sLock.Erase(it);
        }
    }

    if (NULL == lock) {
        ALOGE("nativeReleaseWifiLock() Invalid jlock!\n");
        env->ExceptionDescribe();
        return JNI_FALSE;
    }

    IIWifiManager* wifi = (IIWifiManager*)jproxy;

    Boolean result = FALSE;
    wifi->ReleaseWifiLock(lock, &result);


    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeReleaseWifiLock()");
    return (jboolean)result;
}

static jint android_net_wifi_ElWifiManagerProxy_nativeAddOrUpdateNetwork(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jconfig)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeAddOrUpdateNetwork()");

    AutoPtr<IWifiConfiguration> config;
    if (jconfig != NULL) {
        if (!ElUtil::ToElWifiConfiguration(env, jconfig, (IWifiConfiguration**)&config)) {
            ALOGE("nativeAddOrUpdateNetwork() ToElWifiConfiguration fail!");
        }
    }

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Int32 result = 0;
    ECode ec = wifi->AddOrUpdateNetwork(config, &result);
    if (FAILED(ec))
        ALOGE("nativeAddOrUpdateNetwork() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeAddOrUpdateNetwork()");
    return (jint)result;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativePingSupplicant(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativePingSupplicant()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wifi->PingSupplicant(&result);
    if (FAILED(ec))
        ALOGE("nativePingSupplicant() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativePingSupplicant()");
    return (jboolean)result;
}

static void android_net_wifi_ElWifiManagerProxy_nativeReassociate(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeReassociate()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->Reassociate();
    if (FAILED(ec))
        ALOGE("nativeReassociate() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeReassociate()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeSetCountryCode(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jcountry, jboolean jpersist)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeSetCountryCode()");

    String country= ElUtil::ToElString(env, jcountry);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->SetCountryCode(country, (Boolean)jpersist);
    if (FAILED(ec))
        ALOGE("nativeSetCountryCode() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeSetCountryCode()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeSetFrequencyBand(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jband, jboolean jpersist)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeSetFrequencyBand()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->SetFrequencyBand((Int32)jband, (Boolean)jpersist);
    if (FAILED(ec))
        ALOGE("nativeSetFrequencyBand() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeSetFrequencyBand()");
}

static jint android_net_wifi_ElWifiManagerProxy_nativeGetFrequencyBand(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetFrequencyBand()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Int32 band = 0;
    ECode ec = wifi->GetFrequencyBand(&band);
    if (FAILED(ec))
        ALOGE("nativeGetFrequencyBand() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetFrequencyBand()");
    return (jint)band;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeIsDualBandSupported(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeIsDualBandSupported()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wifi->IsDualBandSupported(&result);
    if (FAILED(ec))
        ALOGE("nativeIsDualBandSupported() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeIsDualBandSupported()");
    return (jboolean)result;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeIsIbssSupported(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeIsIbssSupported()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wifi->IsIbssSupported(&result);
    if (FAILED(ec))
        ALOGE("nativeIsIbssSupported() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeIsIbssSupported()");
    return (jboolean)result;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeSaveConfiguration(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeSaveConfiguration()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wifi->SaveConfiguration(&result);
    if (FAILED(ec))
        ALOGE("nativeSaveConfiguration() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeSaveConfiguration()");
    return (jboolean)result;
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetDhcpInfo(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetDhcpInfo()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    AutoPtr<IDhcpInfo> info;
    wifi->GetDhcpInfo((IDhcpInfo**)&info);

    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaDhcpInfo(env, info);
    }

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetDhcpInfo()");
    return jinfo;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeIsScanAlwaysAvailable(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeIsScanAlwaysAvailable()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wifi->IsScanAlwaysAvailable(&result);
    if (FAILED(ec))
        ALOGE("nativeIsScanAlwaysAvailable() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeIsScanAlwaysAvailable()");
    return (jboolean)result;
}

static void android_net_wifi_ElWifiManagerProxy_nativeInitializeMulticastFiltering(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeInitializeMulticastFiltering()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->InitializeMulticastFiltering();
    if (FAILED(ec))
        ALOGE("nativeInitializeMulticastFiltering() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeInitializeMulticastFiltering()");
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeIsMulticastEnabled(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeIsMulticastEnabled()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wifi->IsMulticastEnabled(&result);
    if (FAILED(ec))
        ALOGE("nativeIsMulticastEnabled() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeIsMulticastEnabled()");
    return (jboolean)result;
}

static void android_net_wifi_ElWifiManagerProxy_nativeAcquireMulticastLock(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jbinder, jstring jtag)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeAcquireMulticastLock()");

    AutoPtr<IBinder> binder;
    if (jbinder != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jbinder);

        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jbinder);
        CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&binder);
    }

    String tag = ElUtil::ToElString(env, jtag);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->AcquireMulticastLock(binder, tag);
    if (FAILED(ec))
        ALOGE("nativeAcquireMulticastLock() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeAcquireMulticastLock()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeReleaseMulticastLock(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeReleaseMulticastLock()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->ReleaseMulticastLock();
    if (FAILED(ec))
        ALOGE("nativeReleaseMulticastLock() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeReleaseMulticastLock()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeSetWifiApEnabled(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwifiConfig, jboolean jenable)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeSetWifiApEnabled()");

    AutoPtr<IWifiConfiguration> wifiConfig;
    if (jwifiConfig != NULL) {
        if (!ElUtil::ToElWifiConfiguration(env, jwifiConfig, (IWifiConfiguration**)&wifiConfig)) {
            ALOGE("nativeSetWifiApEnabled() ToElWifiConfiguration fail!");
        }
    }

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->SetWifiApEnabled(wifiConfig, (Boolean)jenable);
    if (FAILED(ec))
        ALOGE("nativeSetWifiApEnabled() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeSetWifiApEnabled()");
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetWifiApConfiguration(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetWifiApConfiguration()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    AutoPtr<IWifiConfiguration> wifiConfig;
    ECode ec = wifi->GetWifiApConfiguration((IWifiConfiguration**)&wifiConfig);
    if (FAILED(ec))
        ALOGE("nativeGetWifiApConfiguration() ec: 0x%08x", ec);

    jobject jwifiConfig = NULL;
    if (wifiConfig != NULL) {
        jwifiConfig = ElUtil::GetJavaWifiConfiguration(env, wifiConfig);
    }

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetWifiApConfiguration()");
    return jwifiConfig;
}

static void android_net_wifi_ElWifiManagerProxy_nativeSetWifiApConfiguration(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwifiConfig)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeSetWifiApConfiguration()");

    AutoPtr<IWifiConfiguration> wifiConfig;
    if (jwifiConfig != NULL) {
        if (!ElUtil::ToElWifiConfiguration(env, jwifiConfig, (IWifiConfiguration**)&wifiConfig)) {
            ALOGE("nativeSetWifiApConfiguration() ToElWifiConfiguration fail!");
        }
    }

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->SetWifiApConfiguration(wifiConfig);
    if (FAILED(ec))
        ALOGE("nativeSetWifiApConfiguration() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeSetWifiApConfiguration()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeAddToBlacklist(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jbssid)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeAddToBlacklist()");

    String bssid = ElUtil::ToElString(env, jbssid);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->AddToBlacklist(bssid);
    if (FAILED(ec))
        ALOGE("nativeAddToBlacklist() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeAddToBlacklist()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeClearBlacklist(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeClearBlacklist()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->ClearBlacklist();
    if (FAILED(ec))
        ALOGE("nativeClearBlacklist() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeClearBlacklist()");
}

static jstring android_net_wifi_ElWifiManagerProxy_nativeGetConfigFile(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetConfigFile()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    String file;
    ECode ec = wifi->GetConfigFile(&file);
    if (FAILED(ec))
        ALOGE("nativeGetConfigFile() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetConfigFile()");
    return ElUtil::GetJavaString(env, file);
}

static void android_net_wifi_ElWifiManagerProxy_nativeEnableTdls(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jremoteIPAddress, jboolean enable)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeEnableTdls()");

    String remoteIPAddress = ElUtil::ToElString(env, jremoteIPAddress);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->EnableTdls(remoteIPAddress, enable);
    if (FAILED(ec))
        ALOGE("nativeEnableTdls() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeEnableTdls()");
}

static void android_net_wifi_ElWifiManagerProxy_nativeEnableTdlsWithMacAddress(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jremoteMacAddress, jboolean enable)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeEnableTdlsWithMacAddress()");

    String remoteMacAddress = ElUtil::ToElString(env, jremoteMacAddress);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->EnableTdlsWithMacAddress(remoteMacAddress, enable);
    if (FAILED(ec))
        ALOGE("nativeEnableTdlsWithMacAddress() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeEnableTdlsWithMacAddress()");
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeRequestBatchedScan(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrequested, jobject jbinder, jobject jws)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeRequestBatchedScan()");

    AutoPtr<IBatchedScanSettings> requested;
    if (jrequested != NULL) {
        if (!ElUtil::ToElBatchedScanSettings(env, jrequested, (IBatchedScanSettings**)&requested)) {
            ALOGE("nativeRequestBatchedScan() ToElBatchedScanSettings fail!");
        }
    }

    AutoPtr<IBinder> binder;
    if (jbinder != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jbinder);

        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jbinder);
        CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&binder);
    }

    AutoPtr<IWorkSource> ws;
    if (jws != NULL) {
        if (!ElUtil::ToElWorkSource(env, jws, (IWorkSource**)&ws)) {
            ALOGE("nativeRequestBatchedScan() ToElWorkSource fail!");
        }
    }

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Boolean result;
    ECode ec = wifi->RequestBatchedScan(requested, binder, ws, &result);
    if (FAILED(ec))
        ALOGE("nativeRequestBatchedScan() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeRequestBatchedScan()");
    return result;
}

static void android_net_wifi_ElWifiManagerProxy_nativeStopBatchedScan(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrequested)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeStopBatchedScan()");

    AutoPtr<IBatchedScanSettings> requested;
    if (jrequested != NULL) {
        if (!ElUtil::ToElBatchedScanSettings(env, jrequested, (IBatchedScanSettings**)&requested)) {
            ALOGE("nativeRequestBatchedScan() ToElBatchedScanSettings fail!");
        }
    }

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->StopBatchedScan(requested);
    if (FAILED(ec))
        ALOGE("nativeStopBatchedScan() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeStopBatchedScan()");
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetBatchedScanResults(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetBatchedScanResults()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = wifi->GetBatchedScanResults(callingPackage, (IList**)&list);
    if (FAILED(ec))
        ALOGE("nativeGetBatchedScanResults() ec: 0x%08x", ec);

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetBatchedScanResults Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetBatchedScanResults Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jlist = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetBatchedScanResults Fail NewObject: ArrayList : %d!\n", __LINE__);

    if (list != NULL) {
        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetBatchedScanResults GetMethodID: add : %d!\n", __LINE__);

        Int32 size;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IBatchedScanSettings> settings = IBatchedScanSettings::Probe(obj);
            if (settings != NULL) {
                jobject jsettings = ElUtil::GetJavaBatchedScanSettings(env, settings);
                if (jsettings != NULL) {
                    env->CallBooleanMethod(jlist, mAdd, jsettings);
                    ElUtil::CheckErrorAndLog(env, "nativeGetBatchedScanResults Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(jsettings);
                }
                else {
                    ALOGE("nativeGetBatchedScanResults() jsettings is NULL!");
                }
            }
        }
    }
    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetBatchedScanResults()");
    return jlist;
}

static jboolean android_net_wifi_ElWifiManagerProxy_nativeIsBatchedScanSupported(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeIsBatchedScanSupported()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Boolean result;
    ECode ec = wifi->IsBatchedScanSupported(&result);
    if (FAILED(ec))
        ALOGE("nativeIsBatchedScanSupported() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeIsBatchedScanSupported()");
    return result;
}

static void android_net_wifi_ElWifiManagerProxy_nativePollBatchedScan(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativePollBatchedScan()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->PollBatchedScan();
    if (FAILED(ec))
        ALOGE("nativePollBatchedScan() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativePollBatchedScan()");
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetWpsNfcConfigurationToken(
    JNIEnv* env, jobject clazz, jlong jproxy, jint netId)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetWpsNfcConfigurationToken()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    String str;
    ECode ec = wifi->GetWpsNfcConfigurationToken(netId, &str);
    if (FAILED(ec))
        ALOGE("nativeGetWpsNfcConfigurationToken() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetWpsNfcConfigurationToken()");
    return ElUtil::GetJavaString(env, str);
}

static void android_net_wifi_ElWifiManagerProxy_nativeEnableVerboseLogging(
    JNIEnv* env, jobject clazz, jlong jproxy, jint verbose)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeEnableVerboseLogging()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->EnableVerboseLogging(verbose);
    if (FAILED(ec))
        ALOGE("nativeEnableVerboseLogging() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeEnableVerboseLogging()");
}

static jint android_net_wifi_ElWifiManagerProxy_nativeGetVerboseLoggingLevel(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetVerboseLoggingLevel()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Int32 result;
    ECode ec = wifi->GetVerboseLoggingLevel(&result);
    if (FAILED(ec))
        ALOGE("nativeGetVerboseLoggingLevel() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetVerboseLoggingLevel()");
    return result;
}

static jint android_net_wifi_ElWifiManagerProxy_nativeGetAggressiveHandover(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetAggressiveHandover()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Int32 result;
    ECode ec = wifi->GetAggressiveHandover(&result);
    if (FAILED(ec))
        ALOGE("nativeGetAggressiveHandover() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetAggressiveHandover()");
    return result;
}

static void android_net_wifi_ElWifiManagerProxy_nativeEnableAggressiveHandover(
    JNIEnv* env, jobject clazz, jlong jproxy, jint enabled)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeEnableAggressiveHandover()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->EnableAggressiveHandover(enabled);
    if (FAILED(ec))
        ALOGE("nativeEnableAggressiveHandover() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeEnableAggressiveHandover()");
}

static jint android_net_wifi_ElWifiManagerProxy_nativeGetAllowScansWithTraffic(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetAllowScansWithTraffic()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    Int32 result;
    ECode ec = wifi->GetAllowScansWithTraffic(&result);
    if (FAILED(ec))
        ALOGE("nativeGetAllowScansWithTraffic() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetAllowScansWithTraffic()");
    return result;
}

static void android_net_wifi_ElWifiManagerProxy_nativeSetAllowScansWithTraffic(
    JNIEnv* env, jobject clazz, jlong jproxy, jint enabled)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeSetAllowScansWithTraffic()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    ECode ec = wifi->SetAllowScansWithTraffic(enabled);
    if (FAILED(ec))
        ALOGE("nativeSetAllowScansWithTraffic() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeSetAllowScansWithTraffic()");
}

static jobject android_net_wifi_ElWifiManagerProxy_nativeGetConnectionStatistics(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeGetConnectionStatistics()");

    IIWifiManager* wifi = (IIWifiManager*)jproxy;
    AutoPtr<IWifiConnectionStatistics> wcs;
    ECode ec = wifi->GetConnectionStatistics((IWifiConnectionStatistics**)&wcs);
    if (FAILED(ec))
        ALOGE("nativeGetConnectionStatistics() ec: 0x%08x", ec);

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeGetConnectionStatistics()");
    return ElUtil::GetJavaWifiConnectionStatistics(env, wcs);
}

static void android_net_wifi_ElWifiManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_net_wifi_ElWifiManagerProxy_nativeDestroy()");

    IIWifiManager* obj = (IIWifiManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_net_wifi_ElWifiManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeFinalize },
    { "nativeGetSupportedFeatures",    "(J)I",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetSupportedFeatures },
    { "nativeReportActivityInfo",    "(J)Landroid/net/wifi/WifiActivityEnergyInfo;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeReportActivityInfo },
    { "nativeGetWifiServiceMessenger",    "(J)Landroid/os/Messenger;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetWifiServiceMessenger },
    { "nativeGetConnectionInfo",    "(J)Landroid/net/wifi/WifiInfo;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetConnectionInfo },
    { "nativeGetWifiEnabledState",    "(J)I",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetWifiEnabledState },
    { "nativeGetWifiApEnabledState",    "(J)I",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetWifiApEnabledState },
    { "nativeSetWifiEnabled",    "(JZ)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeSetWifiEnabled },
    { "nativeGetChannelList",    "(J)Ljava/util/List;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetChannelList },
    { "nativeStartScan",    "(JLandroid/net/wifi/ScanSettings;Landroid/os/WorkSource;)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeStartScan },
    { "nativeGetScanResults",    "(JLjava/lang/String;)Ljava/util/List;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetScanResults },
    { "nativeGetConfiguredNetworks",    "(J)Ljava/util/List;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetConfiguredNetworks },
    { "nativeGetPrivilegedConfiguredNetworks",    "(J)Ljava/util/List;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetPrivilegedConfiguredNetworks },
    { "nativeStartWifi",    "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeStartWifi },
    { "nativeStopWifi",    "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeStopWifi },
    { "nativeDisconnect",    "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeDisconnect },
    { "nativeReconnect",    "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeReconnect },
    { "nativeEnableNetwork",    "(JIZ)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeEnableNetwork },
    { "nativeDisableNetwork",    "(JI)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeDisableNetwork },
    { "nativeRemoveNetwork",    "(JI)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeRemoveNetwork },
    { "nativeAcquireWifiLock",    "(JLandroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeAcquireWifiLock },
    { "nativeUpdateWifiLockWorkSource",    "(JLandroid/os/IBinder;Landroid/os/WorkSource;)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeUpdateWifiLockWorkSource },
    { "nativeReleaseWifiLock",    "(JLandroid/os/IBinder;)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeReleaseWifiLock },
    { "nativeAddOrUpdateNetwork",    "(JLandroid/net/wifi/WifiConfiguration;)I",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeAddOrUpdateNetwork },
    { "nativePingSupplicant",    "(J)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativePingSupplicant },
    { "nativeReassociate",    "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeReassociate },
    { "nativeSetCountryCode",    "(JLjava/lang/String;Z)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeSetCountryCode },
    { "nativeSetFrequencyBand",    "(JIZ)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeSetFrequencyBand },
    { "nativeGetFrequencyBand",    "(J)I",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetFrequencyBand },
    { "nativeIsDualBandSupported",    "(J)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeIsDualBandSupported },
    { "nativeIsIbssSupported",    "(J)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeIsIbssSupported },
    { "nativeSaveConfiguration",    "(J)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeSaveConfiguration },
    { "nativeGetDhcpInfo",    "(J)Landroid/net/DhcpInfo;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetDhcpInfo },
    { "nativeIsScanAlwaysAvailable",    "(J)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeIsScanAlwaysAvailable },
    { "nativeInitializeMulticastFiltering",    "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetDhcpInfo },
    { "nativeIsMulticastEnabled",    "(J)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeIsMulticastEnabled },
    { "nativeAcquireMulticastLock",    "(JLandroid/os/IBinder;Ljava/lang/String;)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeAcquireMulticastLock },
    { "nativeReleaseMulticastLock",    "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeReleaseMulticastLock },
    { "nativeSetWifiApEnabled",    "(JLandroid/net/wifi/WifiConfiguration;Z)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeSetWifiApEnabled },
    { "nativeGetWifiApConfiguration",    "(J)Landroid/net/wifi/WifiConfiguration;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetWifiApConfiguration },
    { "nativeSetWifiApConfiguration",    "(JLandroid/net/wifi/WifiConfiguration;)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeSetWifiApConfiguration },
    { "nativeAddToBlacklist",    "(JLjava/lang/String;)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeAddToBlacklist },
    { "nativeClearBlacklist",    "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeClearBlacklist },
    { "nativeGetConfigFile",    "(J)Ljava/lang/String;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetConfigFile },
    { "nativeEnableTdls",    "(JLjava/lang/String;Z)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeEnableTdls },
    { "nativeEnableTdlsWithMacAddress",    "(JLjava/lang/String;Z)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeEnableTdlsWithMacAddress },
    { "nativeRequestBatchedScan",    "(JLandroid/net/wifi/BatchedScanSettings;Landroid/os/IBinder;Landroid/os/WorkSource;)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeRequestBatchedScan },
    { "nativeStopBatchedScan",    "(JLandroid/net/wifi/BatchedScanSettings;)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeStopBatchedScan },
    { "nativeGetBatchedScanResults",    "(JLjava/lang/String;)Ljava/util/List;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetBatchedScanResults },
    { "nativeIsBatchedScanSupported",    "(J)Z",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeIsBatchedScanSupported },
    { "nativePollBatchedScan",    "(J)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativePollBatchedScan },
    { "nativeGetWpsNfcConfigurationToken",    "(JI)Ljava/lang/String;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetWpsNfcConfigurationToken },
    { "nativeEnableVerboseLogging",    "(JI)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeEnableVerboseLogging },
    { "nativeGetVerboseLoggingLevel",    "(J)I",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetVerboseLoggingLevel },
    { "nativeGetAggressiveHandover",    "(J)I",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetAggressiveHandover },
    { "nativeEnableAggressiveHandover",    "(JI)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeEnableAggressiveHandover },
    { "nativeGetAllowScansWithTraffic",    "(J)I",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetAllowScansWithTraffic },
    { "nativeSetAllowScansWithTraffic",    "(JI)V",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeSetAllowScansWithTraffic },
    { "nativeGetConnectionStatistics",    "(J)Landroid/net/wifi/WifiConnectionStatistics;",
            (void*) android_net_wifi_ElWifiManagerProxy_nativeGetConnectionStatistics },
};

int register_android_net_wifi_ElWifiManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/net/wifi/ElWifiManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

