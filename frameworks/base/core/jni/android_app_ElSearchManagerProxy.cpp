
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.CoreLibrary.Utility.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::App::IISearchManager;
using Elastos::Droid::App::ISearchableInfo;

static jobject android_app_ElSearchManagerProxy_nativeGetGlobalSearchActivity(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElSearchManagerProxy_nativeGetGlobalSearchActivity()");

    IISearchManager* serm = (IISearchManager*)jproxy;

    AutoPtr<IComponentName> name;
    serm->GetGlobalSearchActivity((IComponentName**)&name);
    jobject jname = NULL;
    if (name != NULL) {
        jname = ElUtil::GetJavaComponentName(env, name);
    }

    // ALOGD("- android_app_ElSearchManagerProxy_nativeGetGlobalSearchActivity()");
    return jname;
}

static jobject android_app_ElSearchManagerProxy_nativeGetWebSearchActivity(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElSearchManagerProxy_nativeGetWebSearchActivity()");

    IISearchManager* serm = (IISearchManager*)jproxy;

    AutoPtr<IComponentName> name;
    serm->GetWebSearchActivity((IComponentName**)&name);
    jobject jname = NULL;
    if (name != NULL) {
        jname = ElUtil::GetJavaComponentName(env, name);
    }

    // ALOGD("- android_app_ElSearchManagerProxy_nativeGetWebSearchActivity()");
    return jname;
}

static jobject android_app_ElSearchManagerProxy_nativeGetAssistIntent(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserHandle)
{
    // ALOGD("+ android_app_ElSearchManagerProxy_nativeGetAssistIntent()");

    IISearchManager* serm = (IISearchManager*)jproxy;

    AutoPtr<IComponentName> name;
    serm->GetAssistIntent((Int32)juserHandle, (IComponentName**)&name);
    jobject jname = NULL;
    if (name != NULL) {
        jname = ElUtil::GetJavaComponentName(env, name);
    }

    // ALOGD("- android_app_ElSearchManagerProxy_nativeGetAssistIntent()");
    return jname;
}

static jobject android_app_ElSearchManagerProxy_nativeGetSearchableInfo(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlaunchActivity)
{
    // ALOGD("+ android_app_ElSearchManagerProxy_nativeGetSearchableInfo()");

    AutoPtr<IComponentName> launchActivity;
    if (jlaunchActivity != NULL) {
        if (!ElUtil::ToElComponentName(env, jlaunchActivity, (IComponentName**)&launchActivity)) {
            ALOGE("nativeGetSearchableInfo() ToElComponentName fail!");
        }
    }

    IISearchManager* serm = (IISearchManager*)jproxy;

    AutoPtr<ISearchableInfo> info;
    serm->GetSearchableInfo(launchActivity, (ISearchableInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaSearchableInfo(env, info);
    }

    // ALOGD("- android_app_ElSearchManagerProxy_nativeGetSearchableInfo()");
    return jinfo;
}

static jobject android_app_ElSearchManagerProxy_nativeGetSearchablesInGlobalSearch(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElSearchManagerProxy_nativeGetSearchablesInGlobalSearch()");

    IISearchManager* serm = (IISearchManager*)jproxy;

    AutoPtr<IList> list;
    serm->GetSearchablesInGlobalSearch((IList**)&list);

    jobject jlist = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetSearchablesInGlobalSearch FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetSearchablesInGlobalSearch GetMethodID: ArrayList : %d!\n", __LINE__);

        jlist = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetSearchablesInGlobalSearch NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetSearchablesInGlobalSearch GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<ISearchableInfo> info = ISearchableInfo::Probe(obj);
            jobject jInfo = ElUtil::GetJavaSearchableInfo(env, info);
            if (jInfo != NULL) {
                env->CallBooleanMethod(jlist, mAdd, jInfo);
                ElUtil::CheckErrorAndLog(env, "nativeGetSearchablesInGlobalSearch CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jInfo);
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_app_ElSearchManagerProxy_nativeGetSearchablesInGlobalSearch()");
    return jlist;
}

static jobject android_app_ElSearchManagerProxy_nativeGetGlobalSearchActivities(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElSearchManagerProxy_nativeGetGlobalSearchActivities()");

    IISearchManager* serm = (IISearchManager*)jproxy;

    AutoPtr<IList> list;
    serm->GetGlobalSearchActivities((IList**)&list);

    jobject jlist = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetGlobalSearchActivities FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetGlobalSearchActivities GetMethodID: ArrayList : %d!\n", __LINE__);

        jlist = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetGlobalSearchActivities NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetGlobalSearchActivities GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IResolveInfo> info = IResolveInfo::Probe(obj);
            jobject jInfo = ElUtil::GetJavaResolveInfo(env, info);
            if (jInfo != NULL) {
                env->CallBooleanMethod(jlist, mAdd, jInfo);
                ElUtil::CheckErrorAndLog(env, "nativeGetGlobalSearchActivities CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jInfo);
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_app_ElSearchManagerProxy_nativeGetGlobalSearchActivities()");
    return jlist;
}

static jboolean android_app_ElSearchManagerProxy_nativeLaunchAssistAction(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jrequestType, jstring jhint, jint juserHandle)
{
    // ALOGD("+ android_app_ElSearchManagerProxy_nativeLaunchAssistAction()");

    String hint = ElUtil::ToElString(env, jhint);

    IISearchManager* serm = (IISearchManager*)jproxy;

    Boolean result;
    serm->LaunchAssistAction(jrequestType, hint, juserHandle, &result);

    // ALOGD("- android_app_ElSearchManagerProxy_nativeLaunchAssistAction()");
    return result;
}

static void android_app_ElSearchManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_app_ElSearchManagerProxy_nativeDestroy()");

    IISearchManager* obj = (IISearchManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_app_ElSearchManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_app_ElSearchManagerProxy_nativeFinalize },
    { "nativeGetGlobalSearchActivity",    "(J)Landroid/content/ComponentName;",
            (void*) android_app_ElSearchManagerProxy_nativeGetGlobalSearchActivity },
    { "nativeGetWebSearchActivity",    "(J)Landroid/content/ComponentName;",
            (void*) android_app_ElSearchManagerProxy_nativeGetWebSearchActivity },
    { "nativeGetAssistIntent",    "(JI)Landroid/content/ComponentName;",
            (void*) android_app_ElSearchManagerProxy_nativeGetAssistIntent },
    { "nativeGetSearchableInfo",    "(JLandroid/content/ComponentName;)Landroid/app/SearchableInfo;",
            (void*) android_app_ElSearchManagerProxy_nativeGetSearchableInfo },
    { "nativeGetSearchablesInGlobalSearch",    "(J)Ljava/util/List;",
            (void*) android_app_ElSearchManagerProxy_nativeGetSearchablesInGlobalSearch },
    { "nativeGetGlobalSearchActivities",    "(J)Ljava/util/List;",
            (void*) android_app_ElSearchManagerProxy_nativeGetGlobalSearchActivities },
    { "nativeLaunchAssistAction",    "(JILjava/lang/String;I)Z",
            (void*) android_app_ElSearchManagerProxy_nativeLaunchAssistAction },
};

int register_android_app_ElSearchManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/app/ElSearchManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

