#define LOG_TAG "ElAlarmManagerProxyJNI"

#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>
#include <utils/Mutex.h>
#include <elastos/utility/etl/HashMap.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::App::IIAlarmManager;
using Elastos::Utility::Etl::HashMap;

static Mutex sOperationMapLock;
static HashMap<Int32, AutoPtr<IPendingIntent> > sOperationMap;

static void android_app_ElAlarmManagerProxy_nativeSetTimeZone(JNIEnv* env, jobject clazz, jlong jproxy, jstring jzone)
{
    // ALOGD("+ android_app_ElAlarmManagerProxy_nativeSetTimeZone()");
    IIAlarmManager* alarm = (IIAlarmManager*)jproxy;
    alarm->SetTimeZone(ElUtil::ToElString(env, jzone));
    // ALOGD("- android_app_ElAlarmManagerProxy_nativeSetTimeZone()");
}

static void android_app_ElAlarmManagerProxy_nativeRemove(JNIEnv* env, jobject clazz, jlong jproxy, jobject joperation)
{
    // ALOGD("+ android_app_ElAlarmManagerProxy_nativeRemove()");
    if (joperation == NULL)
        return;

    Int32 hashCode = ElUtil::GetJavaHashCode(env, joperation);

    AutoPtr<IPendingIntent> intent;
    {
        Mutex::Autolock lock(sOperationMapLock);
        HashMap<Int32, AutoPtr<IPendingIntent> >::Iterator it = sOperationMap.Find(hashCode);

        if (it != sOperationMap.End()) {
            intent = it->mSecond;
            sOperationMap.Erase(it);
        }
        else {
            ALOGE("android_app_ElAlarmManagerProxy_nativeRemove cann't find IPendingIntent");
            return;
        }
    }

    IIAlarmManager* alarm = (IIAlarmManager*)jproxy;
    alarm->Remove(intent);
    // ALOGD("- android_app_ElAlarmManagerProxy_nativeRemove()");
}

static void android_app_ElAlarmManagerProxy_nativeSet(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtype, jlong jtriggerAtTime, jlong jwindowLength, jlong jinterval, jobject joperation,
    jobject jworkSource, jobject jalarmClock)
{
    // ALOGD("+ android_app_ElAlarmManagerProxy_nativeSet()");
    IIAlarmManager* alarm = (IIAlarmManager*)jproxy;

    AutoPtr<IPendingIntent> intent;
    if (joperation != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, joperation);

        Mutex::Autolock lock(sOperationMapLock);
        HashMap<Int32, AutoPtr<IPendingIntent> >::Iterator it = sOperationMap.Find(hashCode);

        if (it != sOperationMap.End()) {
            intent = it->mSecond;
        }

        if (intent == NULL) {
            if (ElUtil::ToElPendingIntent(env, joperation, (IPendingIntent**)&intent))
                sOperationMap[hashCode] = intent;
            else
                ALOGE("android_app_ElAlarmManagerProxy_nativeSet ToElPendingIntent fail!");
        }

    }

    AutoPtr<IWorkSource> workSource;
    if (jworkSource != NULL) {
        if (!ElUtil::ToElWorkSource(env, jworkSource, (IWorkSource**)&workSource))
            ALOGE("android_app_ElAlarmManagerProxy_nativeSet ToElWorkSource fail!");
    }

    AutoPtr<IAlarmClockInfo> alarmClock;
    if (jalarmClock != NULL) {
        if (!ElUtil::ToElAlarmClockInfo(env, jalarmClock, (IAlarmClockInfo**)&alarmClock))
            ALOGE("android_app_ElAlarmManagerProxy_nativeSet ToElAlarmClockInfo fail!");
    }

    alarm->Set((Int32)jtype, (Int64)jtriggerAtTime, jwindowLength, jinterval, intent,
        workSource, alarmClock);
    // ALOGD("- android_app_ElAlarmManagerProxy_nativeSet()");
}

static jboolean android_app_ElAlarmManagerProxy_nativeSetTime(JNIEnv* env, jobject clazz, jlong jproxy, jlong jmillis)
{
    // ALOGD("+ android_app_ElAlarmManagerProxy_nativeSetTime()");
    IIAlarmManager* alarm = (IIAlarmManager*)jproxy;

    Boolean res;
    alarm->SetTime((Int64)jmillis, &res);
    // ALOGD("- android_app_ElAlarmManagerProxy_nativeSetTime()");
    return res;
}

static jobject android_app_ElAlarmManagerProxy_nativeGetNextAlarmClock(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juserId)
{
    // ALOGD("+ android_app_ElAlarmManagerProxy_nativeGetNextAlarmClock()");
    IIAlarmManager* alarm = (IIAlarmManager*)jproxy;

    AutoPtr<IAlarmClockInfo> info;
    alarm->GetNextAlarmClock(juserId, (IAlarmClockInfo**)&info);
    // ALOGD("- android_app_ElAlarmManagerProxy_nativeGetNextAlarmClock()");
    return ElUtil::GetJavaAlarmClockInfo(env, info);
}

static void android_app_ElAlarmManagerProxy_nativeUpdateBlockedUids(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juid, jboolean jisBlocked)
{
    // ALOGD("+ android_app_ElAlarmManagerProxy_nativeUpdateBlockedUids()");

    IIAlarmManager* alarm = (IIAlarmManager*)jproxy;
    alarm->UpdateBlockedUids(juid, jisBlocked);

    // ALOGD("- android_app_ElAlarmManagerProxy_nativeUpdateBlockedUids()");
}

static void android_app_ElAlarmManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_app_ElAlarmManagerProxy_nativeDestroy()");

    IIAlarmManager* obj = (IIAlarmManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_app_ElAlarmManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_app_ElAlarmManagerProxy_nativeFinalize },
    { "nativeSetTimeZone",    "(JLjava/lang/String;)V",
            (void*) android_app_ElAlarmManagerProxy_nativeSetTimeZone },
    { "nativeRemove",    "(JLandroid/app/PendingIntent;)V",
            (void*) android_app_ElAlarmManagerProxy_nativeRemove },
    { "nativeSet",    "(JIJJJLandroid/app/PendingIntent;Landroid/os/WorkSource;Landroid/app/AlarmManager$AlarmClockInfo;)V",
            (void*) android_app_ElAlarmManagerProxy_nativeSet },
    { "nativeSetTime",    "(JJ)Z",
            (void*) android_app_ElAlarmManagerProxy_nativeSetTime },
    { "nativeGetNextAlarmClock",    "(JI)Landroid/app/AlarmManager$AlarmClockInfo;",
            (void*) android_app_ElAlarmManagerProxy_nativeGetNextAlarmClock },
    { "nativeUpdateBlockedUids",    "(JIZ)V",
            (void*) android_app_ElAlarmManagerProxy_nativeUpdateBlockedUids },
};

int register_android_app_ElAlarmManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/app/ElAlarmManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

