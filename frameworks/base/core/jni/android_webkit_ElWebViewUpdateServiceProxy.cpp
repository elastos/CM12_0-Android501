
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Webkit.h>

#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::Webkit::IIWebViewUpdateService;

static void android_webkit_ElWebViewUpdateServiceProxy_nativeNotifyRelroCreationCompleted(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean is64Bit, jboolean success)
{
    // ALOGD("+ android_webkit_ElWebViewUpdateServiceProxy_nativeNotifyRelroCreationCompleted()");

    IIWebViewUpdateService* wvus = (IIWebViewUpdateService*)jproxy;
    wvus->NotifyRelroCreationCompleted(is64Bit, success);

    // ALOGD("- android_webkit_ElWebViewUpdateServiceProxy_nativeNotifyRelroCreationCompleted()");
}

static void android_webkit_ElWebViewUpdateServiceProxy_nativeWaitForRelroCreationCompleted(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean is64Bit)
{
    // ALOGD("+ android_webkit_ElWebViewUpdateServiceProxy_nativeWaitForRelroCreationCompleted()");

    IIWebViewUpdateService* wvus = (IIWebViewUpdateService*)jproxy;
    wvus->WaitForRelroCreationCompleted(is64Bit);

    // ALOGD("- android_webkit_ElWebViewUpdateServiceProxy_nativeWaitForRelroCreationCompleted()");
}

static void android_webkit_ElWebViewUpdateServiceProxy_nativeFinalize(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_webkit_ElWebViewUpdateServiceProxy_nativeFinalize()");

    IIWebViewUpdateService* wvus = (IIWebViewUpdateService*)jproxy;
    wvus->Release();

    // ALOGD("- android_webkit_ElWebViewUpdateServiceProxy_nativeFinalize()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeNotifyRelroCreationCompleted",    "(JZZ)V",
            (void*) android_webkit_ElWebViewUpdateServiceProxy_nativeNotifyRelroCreationCompleted },
    { "nativeWaitForRelroCreationCompleted",    "(JZ)V",
            (void*) android_webkit_ElWebViewUpdateServiceProxy_nativeWaitForRelroCreationCompleted },
    { "nativeFinalize",    "(J)V",
            (void*) android_webkit_ElWebViewUpdateServiceProxy_nativeFinalize },
};

int register_android_webkit_ElWebViewUpdateServiceProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/webkit/ElWebViewUpdateServiceProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android
