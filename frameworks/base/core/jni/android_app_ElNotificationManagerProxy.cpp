
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <elastos/utility/etl/HashMap.h>
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.Droid.Service.h>

#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::App::INotification;
using Elastos::Droid::App::IINotificationManager;
using Elastos::Droid::App::IITransientNotification;
using Elastos::Droid::JavaProxy::CIConditionListenerNative;
using Elastos::Droid::JavaProxy::CIConditionProviderNative;
using Elastos::Droid::JavaProxy::CINotificationListenerNative;
using Elastos::Droid::JavaProxy::CITransientNotificationNative;
using Elastos::Droid::Service::Notification::ECLSID_CStatusBarNotification;
using Elastos::Droid::Service::Notification::IIConditionListener;
using Elastos::Droid::Service::Notification::IIConditionProvider;
using Elastos::Droid::Service::Notification::IINotificationListener;
using Elastos::Utility::Etl::HashMap;

static Mutex sNotificationListenersLock;
static HashMap<Int32, AutoPtr<IINotificationListener> > sNotificationListeners;

static void android_app_ElNotificationManagerProxy_nativeEnqueueToast(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpkg, jobject jcallback, jint jduration)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeEnqueueToast()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;

    String pkg = ElUtil::ToElString(env, jpkg);

    AutoPtr<IITransientNotification> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);
        CITransientNotificationNative::New((Handle64)jvm, (Handle64)jInstance, (IITransientNotification**)&callback);
    }

    nitm->EnqueueToast(pkg, callback, (Int32)jduration);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeEnqueueToast()");
}

static void android_app_ElNotificationManagerProxy_nativeCancelAllNotifications(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpkg, jint juserId)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeCancelAllNotifications()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;

    String pkg = ElUtil::ToElString(env, jpkg);

    nitm->CancelAllNotifications(pkg, (Int32)juserId);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeCancelAllNotifications()");
}

static void android_app_ElNotificationManagerProxy_nativeCancelToast(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpkg, jobject jcallback)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeCancelToast()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;

    String pkg = ElUtil::ToElString(env, jpkg);

    AutoPtr<IITransientNotification> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);
        CITransientNotificationNative::New((Handle64)jvm, (Handle64)jInstance, (IITransientNotification**)&callback);
    }

    nitm->CancelToast(pkg, callback);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeCancelToast()");
}

static void android_app_ElNotificationManagerProxy_nativeEnqueueNotificationWithTag(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpkg, jstring jopPkg, jstring jtag, jint jid, jobject jnotification, jintArray jidReceived, jint juserId)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeEnqueueNotificationWithTag()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;

    String pkg = ElUtil::ToElString(env, jpkg);

    String opPkg = ElUtil::ToElString(env, jopPkg);
    String tag = ElUtil::ToElString(env, jtag);

    AutoPtr<INotification> notification;
    if (jnotification != NULL) {
        if (!ElUtil::ToElNotification(env, jnotification, (INotification**)&notification)) {
            ALOGE("nativeEnqueueNotificationWithTag() ToElNotification fail!");
        }
    }

    AutoPtr<ArrayOf<Int32> > idReceived;
    nitm->EnqueueNotificationWithTag(pkg, opPkg, tag, (Int32)jid, notification, NULL, (Int32)juserId, (ArrayOf<Int32>**)&idReceived);
    if (idReceived != NULL) {
        Int32 count = idReceived->GetLength();
        jint* payload = (jint*)idReceived->GetPayload();

        env->SetIntArrayRegion(jidReceived, 0, count, payload);
        ElUtil::CheckErrorAndLog(env, "nativeEnqueueNotificationWithTag() SetIntArrayRegion failed %d\n", __LINE__);
    }

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeEnqueueNotificationWithTag()");
}

static void android_app_ElNotificationManagerProxy_nativeCancelNotificationWithTag(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpkg, jstring jtag, jint jid, jint juserId)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeCancelNotificationWithTag()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;

    String pkg = ElUtil::ToElString(env, jpkg);

    String tag = ElUtil::ToElString(env, jtag);

    nitm->CancelNotificationWithTag(pkg, tag, (Int32)jid, (Int32)juserId);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeCancelNotificationWithTag()");
}

static void android_app_ElNotificationManagerProxy_nativeSetNotificationsEnabledForPackage(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpkg, jint juid, jboolean jenabled)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeSetNotificationsEnabledForPackage()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;

    String pkg = ElUtil::ToElString(env, jpkg);

    nitm->SetNotificationsEnabledForPackage(pkg, juid, (Boolean)jenabled);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeSetNotificationsEnabledForPackage()");
}

static jboolean android_app_ElNotificationManagerProxy_nativeAreNotificationsEnabledForPackage(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpkg, jint juid)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeAreNotificationsEnabledForPackage()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;

    String pkg = ElUtil::ToElString(env, jpkg);

    Boolean result = FALSE;
    nitm->AreNotificationsEnabledForPackage(pkg, juid, &result);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeAreNotificationsEnabledForPackage()");
    return (jboolean)result;
}

static void android_app_ElNotificationManagerProxy_nativeSetPackagePriority(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpkg, jint juid, jint jpriority)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeSetPackagePriority()");

    String pkg = ElUtil::ToElString(env, jpkg);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->SetPackagePriority(pkg, juid, jpriority);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeSetPackagePriority()");
}

static jint android_app_ElNotificationManagerProxy_nativeGetPackagePriority(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpkg, jint juid)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetPackagePriority()");

    String pkg = ElUtil::ToElString(env, jpkg);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    Int32 result;
    nitm->GetPackagePriority(pkg, juid, &result);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetPackagePriority()");
    return result;
}

static void android_app_ElNotificationManagerProxy_nativeSetPackageVisibilityOverride(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpkg, jint juid, jint jvisibility)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeSetPackageVisibilityOverride()");

    String pkg = ElUtil::ToElString(env, jpkg);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->SetPackageVisibilityOverride(pkg, juid, jvisibility);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeSetPackageVisibilityOverride()");
}

static jint android_app_ElNotificationManagerProxy_nativeGetPackageVisibilityOverride(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpkg, jint juid)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetPackageVisibilityOverride()");

    String pkg = ElUtil::ToElString(env, jpkg);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    Int32 result;
    nitm->GetPackageVisibilityOverride(pkg, juid, &result);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetPackageVisibilityOverride()");
    return result;
}

static void android_app_ElNotificationManagerProxy_nativeSetShowNotificationForPackageOnKeyguard(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpkg, jint juid, jint jstatus)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeSetShowNotificationForPackageOnKeyguard()");

    String pkg = ElUtil::ToElString(env, jpkg);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->SetShowNotificationForPackageOnKeyguard(pkg, juid, jstatus);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeSetShowNotificationForPackageOnKeyguard()");
}

static jint android_app_ElNotificationManagerProxy_nativeGetShowNotificationForPackageOnKeyguard(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpkg, jint juid)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetShowNotificationForPackageOnKeyguard()");

    String pkg = ElUtil::ToElString(env, jpkg);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    Int32 result;
    nitm->GetShowNotificationForPackageOnKeyguard(pkg, juid, &result);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetShowNotificationForPackageOnKeyguard()");
    return result;
}

static jobjectArray android_app_ElNotificationManagerProxy_nativeGetActiveNotifications(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetActiveNotifications()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    AutoPtr<ArrayOf<IStatusBarNotification*> > notifications;
    nitm->GetActiveNotifications(callingPkg, (ArrayOf<IStatusBarNotification*>**)&notifications);

    jobjectArray jnotifications = NULL;
    if (notifications != NULL) {
        Int32 count = notifications->GetLength();

        jclass sbnKlass = env->FindClass("android/service/notification/StatusBarNotification");
        ElUtil::CheckErrorAndLog(env, "FindClass: StatusBarNotification  %d\n", __LINE__);

        jnotifications = env->NewObjectArray((jsize)count, sbnKlass, 0);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: StatusBarNotification  %d\n", __LINE__);
        env->DeleteLocalRef(sbnKlass);

        for (Int32 i = 0; i < count; i++ ) {
            jobject notification = ElUtil::GetJavaStatusBarNotification(env, (*notifications)[i]);
            if (notification != NULL) {
                env->SetObjectArrayElement(jnotifications, i, notification);
                env->DeleteLocalRef(notification);
            }
            else {
                ALOGE("nativeGetActiveNotifications notification is NULL");
            }
        }
    }
    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetActiveNotifications()");

    return jnotifications;
}

static jobjectArray android_app_ElNotificationManagerProxy_nativeGetHistoricalNotifications(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jint jcount)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetHistoricalNotifications()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    AutoPtr<ArrayOf<IStatusBarNotification*> > notifications;
    nitm->GetHistoricalNotifications(callingPkg, jcount, (ArrayOf<IStatusBarNotification*>**)&notifications);

    jobjectArray jnotifications = NULL;
    if (notifications != NULL) {
        Int32 count = notifications->GetLength();

        jclass sbnKlass = env->FindClass("android/service/notification/StatusBarNotification");
        ElUtil::CheckErrorAndLog(env, "FindClass: StatusBarNotification  %d\n", __LINE__);

        jnotifications = env->NewObjectArray((jsize)count, sbnKlass, 0);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: StatusBarNotification  %d\n", __LINE__);
        env->DeleteLocalRef(sbnKlass);

        for (Int32 i = 0; i < count; i++ ) {
            jobject notification = ElUtil::GetJavaStatusBarNotification(env, (*notifications)[i]);
            if (notification != NULL) {
                env->SetObjectArrayElement(jnotifications, i, notification);
                env->DeleteLocalRef(notification);
            }
            else {
                ALOGE("nativeGetHistoricalNotifications notification is NULL");
            }
        }
    }

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetHistoricalNotifications()");

    return jnotifications;
}

static void android_app_ElNotificationManagerProxy_nativeRegisterListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jlistener, jobject jcomponent, jint juserid)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeRegisterListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);

    AutoPtr<IINotificationListener> listener;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            listener = it->mSecond;
        }

        if (listener == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jlistener);

            CINotificationListenerNative::New((Handle64)jvm, (Handle64)jInstance, (IINotificationListener**)&listener);
            sNotificationListeners[hashCode] = listener;
        }
    }

    AutoPtr<IComponentName> component;
    if (jcomponent != NULL) {
        ElUtil::ToElComponentName(env, jcomponent, (IComponentName**)&component);
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->RegisterListener(listener, component, juserid);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeRegisterListener()");
}

static void android_app_ElNotificationManagerProxy_nativeUnregisterListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobjectArray jlistener, jint juserid)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeUnregisterListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);

    AutoPtr<IINotificationListener> listener;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            listener = it->mSecond;
            sNotificationListeners.Erase(it);
        }
        else {
            ALOGE("nativeUnregisterListener cann't find IINotificationListener");
            return;
        }
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->UnregisterListener(listener, juserid);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeUnregisterListener()");
}

static void android_app_ElNotificationManagerProxy_nativeCancelNotificationFromListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jstring jpkg, jstring jtag, jint jid)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeCancelNotificationFromListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IINotificationListener> token;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            token = it->mSecond;
            sNotificationListeners.Erase(it);
        }
        else {
            ALOGE("nativeCancelNotificationFromListener cann't find token");
            return;
        }
    }

    String pkg = ElUtil::ToElString(env, jpkg);
    String tag = ElUtil::ToElString(env, jtag);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->CancelNotificationFromListener(token, pkg, tag, jid);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeCancelNotificationFromListener()");
}

static void android_app_ElNotificationManagerProxy_nativeCancelNotificationsFromListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jobjectArray jkeys)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeCancelNotificationsFromListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IINotificationListener> token;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            token = it->mSecond;
            sNotificationListeners.Erase(it);
        }
        else {
            ALOGE("nativeCancelNotificationsFromListener cann't find token");
            return;
        }
    }

    AutoPtr<ArrayOf<String> > keys;
    if (jkeys != NULL) {
        ElUtil::ToElStringArray(env, jkeys, (ArrayOf<String>**)&keys);
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->CancelNotificationsFromListener(token, keys);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeCancelNotificationsFromListener()");
}

static jobject android_app_ElNotificationManagerProxy_nativeGetActiveNotificationsFromListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jobjectArray jkeys, jint jtrim)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetActiveNotificationsFromListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IINotificationListener> token;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            token = it->mSecond;
            sNotificationListeners.Erase(it);
        }
        else {
            ALOGE("nativeGetActiveNotificationsFromListener cann't find token");
            return NULL;
        }
    }

    AutoPtr<ArrayOf<String> > keys;
    if (jkeys != NULL) {
        ElUtil::ToElStringArray(env, jkeys, (ArrayOf<String>**)&keys);
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    AutoPtr<IParceledListSlice> result;
    nitm->GetActiveNotificationsFromListener(token, keys, jtrim, (IParceledListSlice**)&result);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetActiveNotificationsFromListener()");
    return ElUtil::GetJavaParceledListSlice(env, ECLSID_CStatusBarNotification, result);
}

static void android_app_ElNotificationManagerProxy_nativeRequestHintsFromListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jint jhints)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeRequestHintsFromListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IINotificationListener> token;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            token = it->mSecond;
            sNotificationListeners.Erase(it);
        }
        else {
            ALOGE("nativeRequestHintsFromListener cann't find token");
            return;
        }
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->RequestHintsFromListener(token, jhints);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeRequestHintsFromListener()");
}

static jint android_app_ElNotificationManagerProxy_nativeGetHintsFromListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetHintsFromListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IINotificationListener> token;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            token = it->mSecond;
            sNotificationListeners.Erase(it);
        }
        else {
            ALOGE("nativeGetHintsFromListener cann't find token");
            return 0;
        }
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    Int32 result;
    nitm->GetHintsFromListener(token, &result);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetHintsFromListener()");
    return result;
}

static void android_app_ElNotificationManagerProxy_nativeRequestInterruptionFilterFromListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jint jinterruptionFilter)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeRequestInterruptionFilterFromListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IINotificationListener> token;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            token = it->mSecond;
            sNotificationListeners.Erase(it);
        }
        else {
            ALOGE("nativeRequestInterruptionFilterFromListener cann't find token");
            return;
        }
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->RequestInterruptionFilterFromListener(token, jinterruptionFilter);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeRequestInterruptionFilterFromListener()");
}

static jint android_app_ElNotificationManagerProxy_nativeGetInterruptionFilterFromListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetInterruptionFilterFromListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IINotificationListener> token;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            token = it->mSecond;
            sNotificationListeners.Erase(it);
        }
        else {
            ALOGE("nativeGetInterruptionFilterFromListener cann't find token");
            return 0;
        }
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    Int32 result;
    nitm->GetInterruptionFilterFromListener(token, &result);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetInterruptionFilterFromListener()");
    return result;
}

static void android_app_ElNotificationManagerProxy_nativeSetOnNotificationPostedTrimFromListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jint jtrim)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeSetOnNotificationPostedTrimFromListener()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IINotificationListener> token;
    {
        Mutex::Autolock lock(sNotificationListenersLock);
        HashMap<Int32, AutoPtr<IINotificationListener> >::Iterator it = sNotificationListeners.Find(hashCode);

        if (it != sNotificationListeners.End()) {
            token = it->mSecond;
            sNotificationListeners.Erase(it);
        }
        else {
            ALOGE("nativeSetOnNotificationPostedTrimFromListener cann't find token");
            return;
        }
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->SetOnNotificationPostedTrimFromListener(token, jtrim);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeSetOnNotificationPostedTrimFromListener()");
}

static jobject android_app_ElNotificationManagerProxy_nativeGetEffectsSuppressor(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetEffectsSuppressor()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    AutoPtr<IComponentName> component;
    nitm->GetEffectsSuppressor((IComponentName**)&component);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetEffectsSuppressor()");
    return ElUtil::GetJavaComponentName(env, component);
}

static jboolean android_app_ElNotificationManagerProxy_nativeMatchesCallFilter(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jextras)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeMatchesCallFilter()");

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        ElUtil::ToElBundle(env, jextras, (IBundle**)&extras);
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    Boolean result;
    nitm->MatchesCallFilter(extras, &result);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeMatchesCallFilter()");
    return result;
}

static jobject android_app_ElNotificationManagerProxy_nativeGetZenModeConfig(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetZenModeConfig()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    AutoPtr<IZenModeConfig> config;
    nitm->GetZenModeConfig((IZenModeConfig**)&config);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetZenModeConfig()");
    return ElUtil::GetJavaZenModeConfig(env, config);
}

static jboolean android_app_ElNotificationManagerProxy_nativeSetZenModeConfig(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jconfig)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeSetZenModeConfig()");

    AutoPtr<IZenModeConfig> config;
    if (jconfig != NULL) {
        ElUtil::ToElZenModeConfig(env, jconfig, (IZenModeConfig**)&config);
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    Boolean result;
    nitm->SetZenModeConfig(config, &result);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeSetZenModeConfig()");
    return result;
}

static void android_app_ElNotificationManagerProxy_nativeNotifyConditions(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpkg, jobject jprovider, jobjectArray jconditions)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeNotifyConditions()");

    String pkg = ElUtil::ToElString(env, jpkg);

    AutoPtr<IIConditionProvider> provider;
    if (jprovider != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jprovider);

        CIConditionProviderNative::New((Handle64)jvm, (Handle64)jInstance, (IIConditionProvider**)&provider);
    }

    AutoPtr<ArrayOf<ICondition*> > conditions;
    if (jconditions != NULL) {
        int size = env->GetArrayLength(jconditions);
        ElUtil::CheckErrorAndLog(env, "nativeNotifyConditions(); GetArrayLength failed %d\n", __LINE__);

        conditions = ArrayOf<ICondition*>::Alloc(size);

        for(int i = 0; i < size; i++) {
            jobject jcondition = env->GetObjectArrayElement(jconditions, i);
            ElUtil::CheckErrorAndLog(env, "nativeNotifyConditions(); GetObjectArrayelement failed  %d\n", __LINE__);
            AutoPtr<ICondition> condition;
            ElUtil::ToElCondition(env, jcondition, (ICondition**)&condition);
            conditions->Set(i, condition);
            env->DeleteLocalRef(jcondition);
        }
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->NotifyConditions(pkg, provider, conditions);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeNotifyConditions()");
}

static void android_app_ElNotificationManagerProxy_nativeRequestZenModeConditions(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcallback, jint jrelevance)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeRequestZenModeConditions()");

    AutoPtr<IIConditionListener> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);

        CIConditionListenerNative::New((Handle64)jvm, (Handle64)jInstance, (IIConditionListener**)&callback);
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->RequestZenModeConditions(callback, jrelevance);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeRequestZenModeConditions()");
}

static void android_app_ElNotificationManagerProxy_nativeSetZenModeCondition(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcondition)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeSetZenModeCondition()");

    AutoPtr<ICondition> condition;
    if (jcondition != NULL)
        ElUtil::ToElCondition(env, jcondition, (ICondition**)&condition);

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->SetZenModeCondition(condition);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeSetZenModeCondition()");
}

static void android_app_ElNotificationManagerProxy_nativeSetAutomaticZenModeConditions(
    JNIEnv* env, jobject clazz, jlong jproxy, jobjectArray jconditionIds)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeSetAutomaticZenModeConditions()");

    AutoPtr<ArrayOf<IUri*> > conditionIds;
    if (jconditionIds != NULL) {
        int size = env->GetArrayLength(jconditionIds);
        ElUtil::CheckErrorAndLog(env, "nativeSetAutomaticZenModeConditions(); GetArrayLength failed %d\n", __LINE__);

        conditionIds = ArrayOf<IUri*>::Alloc(size);

        for(int i = 0; i < size; i++) {
            jobject jconditionId = env->GetObjectArrayElement(jconditionIds, i);
            ElUtil::CheckErrorAndLog(env, "nativeSetAutomaticZenModeConditions(); GetObjectArrayelement failed  %d\n", __LINE__);
            AutoPtr<IUri> conditionId;
            ElUtil::ToElUri(env, jconditionId, (IUri**)&conditionId);
            conditionIds->Set(i, conditionId);
            env->DeleteLocalRef(jconditionId);
        }
    }

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    nitm->SetAutomaticZenModeConditions(conditionIds);

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeSetAutomaticZenModeConditions()");
}

static jobjectArray android_app_ElNotificationManagerProxy_nativeGetAutomaticZenModeConditions(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeGetAutomaticZenModeConditions()");

    IINotificationManager* nitm = (IINotificationManager*)jproxy;
    AutoPtr<ArrayOf<ICondition*> > conditions;
    nitm->GetAutomaticZenModeConditions((ArrayOf<ICondition*>**)&conditions);

    jobjectArray jconditions = NULL;
    if (conditions != NULL) {
        Int32 count = conditions->GetLength();

        jclass conditionKlass = env->FindClass("android/service/notification/Condition");
        ElUtil::CheckErrorAndLog(env, "FindClass: Condition  %d\n", __LINE__);

        jconditions = env->NewObjectArray((jsize)count, conditionKlass, 0);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: Condition  %d\n", __LINE__);
        env->DeleteLocalRef(conditionKlass);

        for (Int32 i = 0; i < count; i++ ) {
            jobject condition = ElUtil::GetJavaCondition(env, (*conditions)[i]);
            if (condition != NULL) {
                env->SetObjectArrayElement(jconditions, i, condition);
                env->DeleteLocalRef(condition);
            }
            else {
                ALOGE("nativeGetAutomaticZenModeConditions condition is NULL");
            }
        }
    }

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeGetAutomaticZenModeConditions()");
    return jconditions;
}

static void android_app_ElNotificationManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_app_ElNotificationManagerProxy_nativeDestroy()");

    IINotificationManager* obj = (IINotificationManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_app_ElNotificationManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_app_ElNotificationManagerProxy_nativeFinalize },
    { "nativeEnqueueToast",    "(JLjava/lang/String;Landroid/app/ITransientNotification;I)V",
            (void*) android_app_ElNotificationManagerProxy_nativeEnqueueToast },
    { "nativeCancelAllNotifications",    "(JLjava/lang/String;I)V",
            (void*) android_app_ElNotificationManagerProxy_nativeCancelAllNotifications },
    { "nativeCancelToast",    "(JLjava/lang/String;Landroid/app/ITransientNotification;)V",
            (void*) android_app_ElNotificationManagerProxy_nativeCancelToast },
    { "nativeEnqueueNotificationWithTag",    "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/app/Notification;[II)V",
            (void*) android_app_ElNotificationManagerProxy_nativeEnqueueNotificationWithTag },
    { "nativeCancelNotificationWithTag",    "(JLjava/lang/String;Ljava/lang/String;II)V",
            (void*) android_app_ElNotificationManagerProxy_nativeCancelNotificationWithTag },
    { "nativeSetNotificationsEnabledForPackage",    "(JLjava/lang/String;IZ)V",
            (void*) android_app_ElNotificationManagerProxy_nativeSetNotificationsEnabledForPackage },
    { "nativeAreNotificationsEnabledForPackage",    "(JLjava/lang/String;I)Z",
            (void*) android_app_ElNotificationManagerProxy_nativeAreNotificationsEnabledForPackage },
    { "nativeSetPackagePriority",      "(JLjava/lang/String;II)V",
            (void*) android_app_ElNotificationManagerProxy_nativeSetPackagePriority },
    { "nativeGetPackagePriority",      "(JLjava/lang/String;I)I",
            (void*) android_app_ElNotificationManagerProxy_nativeGetPackagePriority },
    { "nativeSetPackageVisibilityOverride",      "(JLjava/lang/String;II)V",
            (void*) android_app_ElNotificationManagerProxy_nativeSetPackageVisibilityOverride },
    { "nativeGetPackageVisibilityOverride",      "(JLjava/lang/String;I)I",
            (void*) android_app_ElNotificationManagerProxy_nativeGetPackageVisibilityOverride },
    { "nativeSetShowNotificationForPackageOnKeyguard",      "(JLjava/lang/String;II)V",
            (void*) android_app_ElNotificationManagerProxy_nativeSetShowNotificationForPackageOnKeyguard },
    { "nativeGetShowNotificationForPackageOnKeyguard",      "(JLjava/lang/String;I)I",
            (void*) android_app_ElNotificationManagerProxy_nativeGetShowNotificationForPackageOnKeyguard },
    { "nativeGetActiveNotifications",      "(JLjava/lang/String;)[Landroid/service/notification/StatusBarNotification;",
            (void*) android_app_ElNotificationManagerProxy_nativeGetActiveNotifications },
    { "nativeGetHistoricalNotifications",      "(JLjava/lang/String;I)[Landroid/service/notification/StatusBarNotification;",
            (void*) android_app_ElNotificationManagerProxy_nativeGetHistoricalNotifications },
    { "nativeRegisterListener",      "(JLandroid/service/notification/INotificationListener;Landroid/content/ComponentName;I)V",
            (void*) android_app_ElNotificationManagerProxy_nativeRegisterListener },
    { "nativeUnregisterListener",      "(JLandroid/service/notification/INotificationListener;I)V",
            (void*) android_app_ElNotificationManagerProxy_nativeUnregisterListener },
    { "nativeCancelNotificationFromListener",      "(JLandroid/service/notification/INotificationListener;Ljava/lang/String;Ljava/lang/String;I)V",
            (void*) android_app_ElNotificationManagerProxy_nativeCancelNotificationFromListener },
    { "nativeCancelNotificationsFromListener",      "(JLandroid/service/notification/INotificationListener;[Ljava/lang/String;)V",
            (void*) android_app_ElNotificationManagerProxy_nativeCancelNotificationsFromListener },
    { "nativeGetActiveNotificationsFromListener",      "(JLandroid/service/notification/INotificationListener;[Ljava/lang/String;I)Landroid/content/pm/ParceledListSlice;",
            (void*) android_app_ElNotificationManagerProxy_nativeGetActiveNotificationsFromListener },
    { "nativeRequestHintsFromListener",      "(JLandroid/service/notification/INotificationListener;I)V",
            (void*) android_app_ElNotificationManagerProxy_nativeRequestHintsFromListener },
    { "nativeGetHintsFromListener",      "(JLandroid/service/notification/INotificationListener;)I",
            (void*) android_app_ElNotificationManagerProxy_nativeGetHintsFromListener },
    { "nativeRequestInterruptionFilterFromListener",      "(JLandroid/service/notification/INotificationListener;I)V",
            (void*) android_app_ElNotificationManagerProxy_nativeRequestInterruptionFilterFromListener },
    { "nativeGetInterruptionFilterFromListener",      "(JLandroid/service/notification/INotificationListener;)I",
            (void*) android_app_ElNotificationManagerProxy_nativeGetInterruptionFilterFromListener },
    { "nativeSetOnNotificationPostedTrimFromListener",      "(JLandroid/service/notification/INotificationListener;I)V",
            (void*) android_app_ElNotificationManagerProxy_nativeSetOnNotificationPostedTrimFromListener },
    { "nativeGetEffectsSuppressor",      "(J)Landroid/content/ComponentName;",
            (void*) android_app_ElNotificationManagerProxy_nativeGetEffectsSuppressor },
    { "nativeMatchesCallFilter",      "(JLandroid/os/Bundle;)Z",
            (void*) android_app_ElNotificationManagerProxy_nativeMatchesCallFilter },
    { "nativeGetZenModeConfig",      "(J)Landroid/service/notification/ZenModeConfig;",
            (void*) android_app_ElNotificationManagerProxy_nativeGetZenModeConfig },
    { "nativeSetZenModeConfig",      "(JLandroid/service/notification/ZenModeConfig;)Z",
            (void*) android_app_ElNotificationManagerProxy_nativeSetZenModeConfig },
    { "nativeNotifyConditions",      "(JLjava/lang/String;Landroid/service/notification/IConditionProvider;[Landroid/service/notification/Condition;)V",
            (void*) android_app_ElNotificationManagerProxy_nativeNotifyConditions },
    { "nativeRequestZenModeConditions",      "(JLandroid/service/notification/IConditionListener;I)V",
            (void*) android_app_ElNotificationManagerProxy_nativeRequestZenModeConditions },
    { "nativeSetZenModeCondition",      "(JLandroid/service/notification/Condition;)V",
            (void*) android_app_ElNotificationManagerProxy_nativeSetZenModeCondition },
    { "nativeSetAutomaticZenModeConditions",      "(J[Landroid/net/Uri;)V",
            (void*) android_app_ElNotificationManagerProxy_nativeSetAutomaticZenModeConditions },
    { "nativeGetAutomaticZenModeConditions",      "(J)[Landroid/service/notification/Condition;",
            (void*) android_app_ElNotificationManagerProxy_nativeGetAutomaticZenModeConditions },
};

int register_android_app_ElNotificationManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/app/ElNotificationManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

