
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Database.h>
#include <Elastos.Droid.Net.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Database::IContentObserver;

static void android_database_ElContentObserverProxy_naitveOnChange(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jselfChange)
{
    ALOGD("+ android_database_ElContentObserverProxy_naitveOnChange()");

    IContentObserver* observer = (IContentObserver*)jproxy;
    ECode ec = observer->OnChange((Boolean)jselfChange);
    ALOGE("naitveOnChange() ec: 0x%08x", ec);

    ALOGD("- android_database_ElContentObserverProxy_naitveOnChange()");
}

static void android_database_ElContentObserverProxy_naitveOnChangeEx(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jselfChange, jobject juri)
{
    // ALOGD("+ android_database_ElContentObserverProxy_naitveOnChangeEx()");

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("naitveOnChange() ToElUri fail!");
        }
    }

    IContentObserver* observer = (IContentObserver*)jproxy;
    observer->OnChange((Boolean)jselfChange, uri);

    // ALOGD("- android_database_ElContentObserverProxy_naitveOnChangeEx()");
}

static void android_content_ElIIntentSenderProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_content_ElIIntentSenderProxy_nativeDestroy()");

    IContentObserver* obj = (IContentObserver*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_content_ElIIntentSenderProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_content_ElIIntentSenderProxy_nativeFinalize },
    { "naitveOnChange",    "(JZ)V",
            (void*) android_database_ElContentObserverProxy_naitveOnChange },
    { "nativeOnChangeEx",    "(JZLandroid/net/Uri;)V",
            (void*) android_database_ElContentObserverProxy_naitveOnChangeEx },
};

int register_android_database_ElContentObserverProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/database/ElContentObserverProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

