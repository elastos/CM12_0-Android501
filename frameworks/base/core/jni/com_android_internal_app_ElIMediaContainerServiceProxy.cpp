
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Internal.h>
#include <_Elastos.Droid.JavaProxy.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Net::IUri;
using Elastos::Droid::Content::Pm::IPackageInfoLite;
using Elastos::Droid::Content::Res::IObbInfo;
using Elastos::Droid::Os::IParcelFileDescriptor;
using Elastos::Droid::Internal::App::IIMediaContainerService;
using Elastos::Droid::Internal::Os::IIParcelFileDescriptorFactory;
using Elastos::Droid::JavaProxy::CIParcelFileDescriptorFactoryNative;

static jstring com_android_internal_app_ElIMediaContainerServiceProxy_nativeCopyPackageToContainer(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackagePath, jstring jcontainerId, jstring jkey, jboolean jisExternal, jboolean jisForwardLocked,
    jstring jabiOverride)
{
    // ALOGD("+ com_android_internal_app_ElIMediaContainerServiceProxy_nativeCopyPackageToContainer()");

    String packagePath = ElUtil::ToElString(env, jpackagePath);
    String containerId = ElUtil::ToElString(env, jcontainerId);
    String key = ElUtil::ToElString(env, jkey);
    String abiOverride = ElUtil::ToElString(env, jabiOverride);

    IIMediaContainerService* service = (IIMediaContainerService*)jproxy;
    String path;
    service->CopyPackageToContainer(packagePath, containerId, key,
        (Boolean)jisExternal, (Boolean)jisForwardLocked, abiOverride, &path);

    jstring jpath = ElUtil::GetJavaString(env, path);

    // ALOGD("- com_android_internal_app_ElIMediaContainerServiceProxy_nativeCopyPackageToContainer()");
    return jpath;
}

static jint com_android_internal_app_ElIMediaContainerServiceProxy_nativeCopyPackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackagePath, jobject jtarget)
{
    // ALOGD("+ com_android_internal_app_ElIMediaContainerServiceProxy_nativeCopyPackage()");

    String packagePath = ElUtil::ToElString(env, jpackagePath);

    AutoPtr<IIParcelFileDescriptorFactory> target;
    if (jtarget != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jtarget);

        if (NOERROR != CIParcelFileDescriptorFactoryNative::New((Handle64)jvm, (Handle64)jInstance, (IIParcelFileDescriptorFactory**)&target)) {
            ALOGE("nativeCopyPackage new CIParcelFileDescriptorFactoryNative fail!\n");
        }
    }

    IIMediaContainerService* service = (IIMediaContainerService*)jproxy;
    Int32 res = 0;
    service->CopyPackage(packagePath, target, &res);

    // ALOGD("- com_android_internal_app_ElIMediaContainerServiceProxy_nativeCopyPackage()");
    return (jint)res;
}

static jobject com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetMinimalPackageInfo(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpackagePath, jint jflags, jstring jabiOverride)
{
    // ALOGD("+ com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetMinimalPackageInfo()");

    String packagePath = ElUtil::ToElString(env, jpackagePath);
    String abiOverride = ElUtil::ToElString(env, jabiOverride);

    IIMediaContainerService* service = (IIMediaContainerService*)jproxy;
    AutoPtr<IPackageInfoLite> pkgLite;
    service->GetMinimalPackageInfo(packagePath, (Int32)jflags, abiOverride, (IPackageInfoLite**)&pkgLite);

    jobject jpkgLite = NULL;
    if (pkgLite != NULL) {
        jpkgLite = ElUtil::GetJavaPackageInfoLite(env, pkgLite);
    }

    // ALOGD("- com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetMinimalPackageInfo()");
    return jpkgLite;
}

static jobject com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetObbInfo(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jfilename)
{
    // ALOGD("+ com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetObbInfo()");

    String filename = ElUtil::ToElString(env, jfilename);

    IIMediaContainerService* service = (IIMediaContainerService*)jproxy;
    AutoPtr<IObbInfo> info;
    service->GetObbInfo(filename, (IObbInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaObbInfo(env, info);
    }

    // ALOGD("- com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetObbInfo()");
    return jinfo;
}

static jlong com_android_internal_app_ElIMediaContainerServiceProxy_nativeCalculateDirectorySize(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jdirectory)
{
    // ALOGD("+ com_android_internal_app_ElIMediaContainerServiceProxy_nativeCalculateDirectorySize()");

    String directory = ElUtil::ToElString(env, jdirectory);

    IIMediaContainerService* service = (IIMediaContainerService*)jproxy;
    Int64 size = 0;
    service->CalculateDirectorySize(directory, &size);

    // ALOGD("- com_android_internal_app_ElIMediaContainerServiceProxy_nativeCalculateDirectorySize()");
    return (jlong)size;
}

static jlongArray com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetFileSystemStats(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpath)
{
    // ALOGD("+ com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetFileSystemStats()");

    String path = ElUtil::ToElString(env, jpath);

    IIMediaContainerService* service = (IIMediaContainerService*)jproxy;
    AutoPtr<ArrayOf<Int64> > stats;
    service->GetFileSystemStats(path, (ArrayOf<Int64>**)&stats);
    jlongArray jstats = NULL;
    if (stats != NULL) {
        jstats = ElUtil::GetJavaLongArray(env, stats);
    }

    // ALOGD("- com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetFileSystemStats()");
    return jstats;
}

static void com_android_internal_app_ElIMediaContainerServiceProxy_nativeClearDirectory(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jdirectory)
{
    // ALOGD("+ com_android_internal_app_ElIMediaContainerServiceProxy_nativeClearDirectory()");

    String directory = ElUtil::ToElString(env, jdirectory);

    IIMediaContainerService* service = (IIMediaContainerService*)jproxy;
    service->ClearDirectory(directory);

    // ALOGD("- com_android_internal_app_ElIMediaContainerServiceProxy_nativeClearDirectory()");
}

static jlong com_android_internal_app_ElIMediaContainerServiceProxy_nativeCalculateInstalledSize(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jpackagePath, jboolean jisForwardLocked, jstring jabiOverride)
{
    // ALOGD("+ com_android_internal_app_ElIMediaContainerServiceProxy_nativeCalculateInstalledSize()");

    String packagePath = ElUtil::ToElString(env, jpackagePath);
    String abiOverride = ElUtil::ToElString(env, jabiOverride);

    IIMediaContainerService* service = (IIMediaContainerService*)jproxy;
    Int64 size = 0;
    service->CalculateInstalledSize(packagePath, (Boolean)jisForwardLocked, abiOverride, &size);

    // ALOGD("- com_android_internal_app_ElIMediaContainerServiceProxy_nativeCalculateInstalledSize()");
    return (jlong)size;
}

static void com_android_internal_app_ElIMediaContainerServiceProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ com_android_internal_app_ElIMediaContainerServiceProxy_nativeDestroy()");

    IIMediaContainerService* obj = (IIMediaContainerService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- com_android_internal_app_ElIMediaContainerServiceProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) com_android_internal_app_ElIMediaContainerServiceProxy_nativeFinalize },
    { "nativeCopyPackageToContainer",    "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Ljava/lang/String;",
            (void*) com_android_internal_app_ElIMediaContainerServiceProxy_nativeCopyPackageToContainer },
    { "nativeCopyPackage",    "(JLjava/lang/String;Lcom/android/internal/os/IParcelFileDescriptorFactory;)I",
            (void*) com_android_internal_app_ElIMediaContainerServiceProxy_nativeCopyPackage },
    { "nativeGetMinimalPackageInfo",    "(JLjava/lang/String;ILjava/lang/String;)Landroid/content/pm/PackageInfoLite;",
            (void*) com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetMinimalPackageInfo },
    { "nativeGetObbInfo",    "(JLjava/lang/String;)Landroid/content/res/ObbInfo;",
            (void*) com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetObbInfo },
    { "nativeCalculateDirectorySize",    "(JLjava/lang/String;)J",
            (void*) com_android_internal_app_ElIMediaContainerServiceProxy_nativeCalculateDirectorySize },
    { "nativeGetFileSystemStats",    "(JLjava/lang/String;)[J",
            (void*) com_android_internal_app_ElIMediaContainerServiceProxy_nativeGetFileSystemStats },
    { "nativeClearDirectory",    "(JLjava/lang/String;)V",
            (void*) com_android_internal_app_ElIMediaContainerServiceProxy_nativeClearDirectory },
    { "nativeCalculateInstalledSize",    "(JLjava/lang/String;ZLjava/lang/String;)J",
            (void*) com_android_internal_app_ElIMediaContainerServiceProxy_nativeCalculateInstalledSize },
};

int register_com_android_internal_app_ElIMediaContainerServiceProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/internal/app/ElIMediaContainerServiceProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

