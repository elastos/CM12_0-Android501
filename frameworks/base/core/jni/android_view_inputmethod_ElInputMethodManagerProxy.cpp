
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Text.h>
#include <Elastos.Droid.View.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Utility.h>
#include <elastos/utility/etl/HashMap.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Text::Style::ISuggestionSpan;
using Elastos::Droid::View::IIWindow;
using Elastos::Droid::View::IWindowSession;
using Elastos::Droid::View::IIWindowManager;
using Elastos::Droid::View::InputMethod::IInputMethodSession;
using Elastos::Droid::View::InputMethod::IInputMethodInfo;
using Elastos::Droid::Internal::View::IIInputMethodManager;
using Elastos::Droid::Internal::View::IInputMethodClient;
using Elastos::Droid::Internal::View::IIInputContext;
using Elastos::Droid::Internal::View::IInputBindResult;
using Elastos::Droid::JavaProxy::CInputMethodClientNative;
using Elastos::Droid::JavaProxy::CInputContextNative;
using Elastos::Droid::JavaProxy::ECLSID_CInputContextNative;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Os::IResultReceiver;
using Elastos::Utility::Etl::HashMap;

extern HashMap<Int32, AutoPtr<IInputMethodClient> > sIMClientMap;
extern Mutex sIMClientMapLock;
extern HashMap<Int32, AutoPtr<IIInputContext> > sInContextMap;
extern Mutex sInContextMapLock;
extern HashMap<Int32, AutoPtr<IIWindow> > sWindowMap;
extern Mutex sWindowMapLock;

static jobject android_view_inputmethod_ElInputMethodManagerProxy_nativeWindowGainedFocus(JNIEnv* env, jobject clazz, jlong jproxy, jint jclient,
     jobject jwindowToken, jint jcontrolFlags, jint jsoftInputMode, jint jwindowFlags, jobject jattribute, jobject jinputContext)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeWindowGainedFocus()");

    AutoPtr<IInputMethodClient> client;
    if (jclient > 0) {
        Mutex::Autolock lock(sIMClientMapLock);
        HashMap<Int32, AutoPtr<IInputMethodClient> >::Iterator it = sIMClientMap.Find(jclient);
        if (it != sIMClientMap.End()) {
            client = it->mSecond;
        }

        if (NULL == client) {
            ALOGE("nativeWindowGainedFocus() Invalid jclient\n");
            env->ExceptionDescribe();
        }
    }

    AutoPtr<IBinder> token;
    if (jwindowToken != NULL) {
        jclass viewRootWClass = env->FindClass("android/view/ViewRootImpl$W");
        ElUtil::CheckErrorAndLog(env, "FindClass ViewRootImpl$W fail : %d!\n", __LINE__);

        Int32 hashCode = ElUtil::GetJavaHashCode(env, jwindowToken);

        if (env->IsInstanceOf(jwindowToken, viewRootWClass)) {
            Mutex::Autolock lock(sWindowMapLock);
            HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(hashCode);
            if (it != sWindowMap.End()) {
                token = IBinder::Probe(it->mSecond);
            } else {
                ALOGE("nativeWindowGainedFocus() can't find token in sWindowMap");
                assert(0);
            }
        } else {
            String objClassName = ElUtil::GetClassName(env, jwindowToken);
            ALOGE("nativeWindowGainedFocus() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(viewRootWClass);
    } else {
        ALOGE("nativeWindowGainedFocus() what is this token ?");
        assert(0);
    }

    AutoPtr<IEditorInfo> attribute;
    if (jattribute != NULL) {
        if(!ElUtil::ToElEditorInfo(env, jattribute, (IEditorInfo**)&attribute)) {
            ALOGE("nativeWindowGainedFocus ToElEditorInfo fail!");
        }
    }

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    AutoPtr<IIInputContext> inputContext;
    if(jinputContext != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jinputContext);
        Mutex::Autolock lock(sInContextMapLock);
        HashMap<Int32, AutoPtr<IIInputContext> >::Iterator it = sInContextMap.Find(hashCode);
        if (it != sInContextMap.End()) {
            inputContext = it->mSecond;
        } else {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jContextgInstance = env->NewGlobalRef(jinputContext);
            ECode ec = CInputContextNative::New((Handle64)jvm, (Handle64)jContextgInstance, (IIInputContext**)&inputContext);
            if (FAILED(ec)) {
                ALOGE("nativeWindowGainedFocus() new CInputContextNative fail ec: 0x%x\n", ec);
            }
            sInContextMap[hashCode] = inputContext;
        }
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    AutoPtr<IInputBindResult> ibResult;
    im->WindowGainedFocus(client, token, (Int32)jcontrolFlags, (Int32)jsoftInputMode, (Int32)jwindowFlags, attribute, inputContext, (IInputBindResult**)&ibResult);
    jobject jibResult = NULL;
    if (ibResult != NULL) {
        jibResult = ElUtil::GetJavaInputBindResult(env, ibResult);
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeWindowGainedFocus()");
    return jibResult;
}

static jobject android_view_inputmethod_ElInputMethodManagerProxy_nativeStartInput(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jclient, jobject jinputContext, jobject jattribute, jint jcontrolFlags)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeStartInput()");

    AutoPtr<IInputMethodClient> client;
    {
        Mutex::Autolock lock(sIMClientMapLock);
        HashMap<Int32, AutoPtr<IInputMethodClient> >::Iterator it = sIMClientMap.Find(jclient);
        if (it != sIMClientMap.End()) {
            client = it->mSecond;
        }
    }
    if (NULL == client) {
        ALOGE("nativeHideSoftInput() Invalid IInputMethodClient!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IIInputContext> inputContext;
    if(jinputContext != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jinputContext);
        Mutex::Autolock lock(sInContextMapLock);
        HashMap<Int32, AutoPtr<IIInputContext> >::Iterator it = sInContextMap.Find(hashCode);
        if (it != sInContextMap.End()) {
            inputContext = it->mSecond;
        } else {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jContextgInstance = env->NewGlobalRef(jinputContext);
            ECode ec = CInputContextNative::New((Handle64)jvm, (Handle64)jContextgInstance, (IIInputContext**)&inputContext);
            if (FAILED(ec)) {
                ALOGE("nativeOpenSession() new CInputContextNative fail ec: 0x%x\n", ec);
            }
            sInContextMap[hashCode] = inputContext;
        }
    }

    AutoPtr<IEditorInfo> attribute;
    if (jattribute != NULL) {
        if(!ElUtil::ToElEditorInfo(env, jattribute, (IEditorInfo**)&attribute)) {
            ALOGE("nativeWindowGainedFocus ToElEditorInfo fail!");
        }
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    AutoPtr<IInputBindResult> ibResult;
    im->StartInput(client, inputContext, attribute, (Int32)jcontrolFlags, (IInputBindResult**)&ibResult);

    jobject jibResult = NULL;
    if (ibResult != NULL) {
        jibResult = ElUtil::GetJavaInputBindResult(env, ibResult);
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeStartInput()");
    return jibResult;
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeFinishInput(JNIEnv* env, jobject clazz, jlong jproxy, jint jclient)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeFinishInput()");

    AutoPtr<IInputMethodClient> client;
    {
        Mutex::Autolock lock(sIMClientMapLock);
        HashMap<Int32, AutoPtr<IInputMethodClient> >::Iterator it = sIMClientMap.Find(jclient);
        if (it != sIMClientMap.End()) {
            client = it->mSecond;
        }
    }
    if (NULL == client) {
        ALOGE("nativeFinishInput() Invalid IInputMethodClient!\n");
        env->ExceptionDescribe();
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    im->FinishInput(client);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeFinishInput()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeHideMySoftInput(JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jint jflags)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeHideMySoftInput()");

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass viewRootWClass = env->FindClass("android/view/ViewRootImpl$W");
        ElUtil::CheckErrorAndLog(env, "FindClass ViewRootImpl$W fail : %d!\n", __LINE__);
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, viewRootWClass)) {
            Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
            Mutex::Autolock lock(sWindowMapLock);
            HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(hashCode);
            if (it != sWindowMap.End()) {
                token = IBinder::Probe(it->mSecond);
            } else {
                ALOGE("nativeWindowGainedFocus() can't find token in sWindowMap");
                assert(0);
            }
        } else if (env->IsInstanceOf(jtoken, binderClass)) {
            jfieldID f = env->GetFieldID(binderClass, "mNativeProxy", "I");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int32)env->GetIntField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        } else {
            String objClassName = ElUtil::GetClassName(env, jtoken);
            ALOGE("nativeHideMySoftInput() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(viewRootWClass);
        env->DeleteLocalRef(binderClass);
    } else {
        ALOGE("nativeWindowGainedFocus() what is this token ?");
        assert(0);
    }

    im->HideMySoftInput(token, (Int32)jflags);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeHideMySoftInput()");
}

static jobject android_view_inputmethod_ElInputMethodManagerProxy_nativeGetInputMethodList(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeGetInputMethodList()");

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    AutoPtr<IList> list;
    im->GetInputMethodList((IList**)&list);
    jobject jinputInfo = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetInputMethodList Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetInputMethodList Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jinputInfo = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetInputMethodList Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetInputMethodList Fail GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IInputMethodInfo> info = IInputMethodInfo::Probe(obj);
            if (info != NULL) {
                jobject jinfo = ElUtil::GetJavaInputMethodInfo(env, info);

                env->CallBooleanMethod(jinputInfo, mAdd, jinfo);
                ElUtil::CheckErrorAndLog(env, "nativeGetInputMethodList Fail CallObjectMethod: mAdd : %d!\n", __LINE__);

                env->DeleteLocalRef(jinfo);
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeGetInputMethodList()");
    return jinputInfo;
}

static jboolean android_view_inputmethod_ElInputMethodManagerProxy_nativeShowSoftInput(JNIEnv* env, jobject clazz, jlong jproxy,
            jint jclient, jint jflags, jobject jresultReceiver)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeShowSoftInput()");

    AutoPtr<IInputMethodClient> client;
    {
        Mutex::Autolock lock(sIMClientMapLock);
        HashMap<Int32, AutoPtr<IInputMethodClient> >::Iterator it = sIMClientMap.Find(jclient);
        if (it != sIMClientMap.End()) {
            client = it->mSecond;
        }
    }
    if (NULL == client) {
        ALOGE("nativeHideSoftInput() Invalid IInputMethodClient!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IResultReceiver> resultReceiver;
    if (jresultReceiver != NULL) {
        ALOGE("nativeShowSoftInput() jresultReceiver not NULL!\n");
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    Boolean result;
    im->ShowSoftInput(client, (Int32)jflags, resultReceiver, &result);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeShowSoftInput()");
    return (jboolean)result;
}

static jboolean android_view_inputmethod_ElInputMethodManagerProxy_nativeHideSoftInput(JNIEnv* env, jobject clazz, jlong jproxy,
            jint jclient, jint jflags, jobject jresultReceiver)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeHideSoftInput()");

    AutoPtr<IInputMethodClient> client;
    {
        Mutex::Autolock lock(sIMClientMapLock);
        HashMap<Int32, AutoPtr<IInputMethodClient> >::Iterator it = sIMClientMap.Find(jclient);
        if (it != sIMClientMap.End()) {
            client = it->mSecond;
        }
    }

    if (NULL == client) {
        ALOGE("nativeHideSoftInput() Invalid IInputMethodClient!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IResultReceiver> resultReceiver;
    if (jresultReceiver != NULL) {
        ALOGE("nativeHideSoftInput() jresultReceiver not NULL!\n");
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    Boolean result;
    im->HideSoftInput(client, (Int32)jflags, resultReceiver, &result);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeHideSoftInput()");
    return (jboolean)result;
}

static jobject android_view_inputmethod_ElInputMethodManagerProxy_nativeGetEnabledInputMethodList(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeGetEnabledInputMethodList()");

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    AutoPtr<IList> list;
    im->GetEnabledInputMethodList((IList**)&list);
    jobject jinputInfo = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetEnabledInputMethodList FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetEnabledInputMethodList GetMethodID: ArrayList : %d!\n", __LINE__);

        jinputInfo = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetEnabledInputMethodList NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetEnabledInputMethodList GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IInputMethodInfo> info = IInputMethodInfo::Probe(obj);
            if (info != NULL) {
                jobject jinfo = ElUtil::GetJavaInputMethodInfo(env, info);

                env->CallBooleanMethod(jinputInfo, mAdd, jinfo);
                ElUtil::CheckErrorAndLog(env, "nativeGetEnabledInputMethodList CallObjectMethod: mAdd : %d!\n", __LINE__);

                env->DeleteLocalRef(jinfo);
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeGetEnabledInputMethodList()");
    return jinputInfo;
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeShowInputMethodPickerFromClient(JNIEnv* env, jobject clazz, jlong jproxy,
            jint jclient)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeShowInputMethodPickerFromClient()");

    AutoPtr<IInputMethodClient> client;
    {
        Mutex::Autolock lock(sIMClientMapLock);
        HashMap<Int32, AutoPtr<IInputMethodClient> >::Iterator it = sIMClientMap.Find(jclient);
        if (it != sIMClientMap.End()) {
            client = it->mSecond;
        }
    }

    if (NULL == client) {
        ALOGE("nativeHideSoftInput() Invalid IInputMethodClient!\n");
        env->ExceptionDescribe();
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    im->ShowInputMethodPickerFromClient(client);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeShowInputMethodPickerFromClient()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeSetImeWindowStatus(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jint jvis, jint jbackDisposition)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeSetImeWindowStatus()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass viewRootWClass = env->FindClass("android/view/ViewRootImpl$W");
        ElUtil::CheckErrorAndLog(env, "FindClass ViewRootImpl$W fail : %d!\n", __LINE__);
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, viewRootWClass)) {
            Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
            Mutex::Autolock lock(sWindowMapLock);
            HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(hashCode);
            if (it != sWindowMap.End()) {
                token = IBinder::Probe(it->mSecond);

            } else {
                ALOGE("nativeWindowGainedFocus() can't find token in sWindowMap");
                assert(0);
            }
        } else if (env->IsInstanceOf(jtoken, binderClass)) {
            jfieldID f = env->GetFieldID(binderClass, "mNativeProxy", "I");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int32)env->GetIntField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        } else {
            String objClassName = ElUtil::GetClassName(env, jtoken);
            ALOGE("nativeSetImeWindowStatus() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(viewRootWClass);
        env->DeleteLocalRef(binderClass);
    } else {
        ALOGE("nativeWindowGainedFocus() what is this token ?");
        assert(0);
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    im->SetImeWindowStatus(token, (Int32)jvis, (Int32)jbackDisposition);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeSetImeWindowStatus()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeUpdateStatusIcon(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jstring jpackageName, jint jiconId)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeUpdateStatusIcon()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, binderClass)) {
            jfieldID f = env->GetFieldID(binderClass, "mNativeProxy", "I");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int32)env->GetIntField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        } else {
            String objClassName = ElUtil::GetClassName(env, jtoken);
            ALOGE("nativeSetImeWindowStatus() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(binderClass);
    } else {
        ALOGE("nativeWindowGainedFocus() what is this token ?");
        assert(0);
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;

    im->UpdateStatusIcon(token, packageName, (Int32)jiconId);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeUpdateStatusIcon()");
}

static jobject android_view_inputmethod_ElInputMethodManagerProxy_nativeGetEnabledInputMethodSubtypeList(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jimiId, jboolean jallowsImplicitlySelectedSubtypes)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeGetEnabledInputMethodSubtypeList()");

    String imiId = ElUtil::ToElString(env, jimiId);

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = im->GetEnabledInputMethodSubtypeList(imiId, (Boolean)jallowsImplicitlySelectedSubtypes, (IList**)&list);
    if (FAILED(ec))
        ALOGE("nativeGetEnabledInputMethodSubtypeList() ec: 0x%08x", ec);

    jobject jlist = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jlist = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IInputMethodSubtype> type = IInputMethodSubtype::Probe(obj);
            if (type != NULL) {
                jobject jtype = ElUtil::GetJavaInputMethodSubtype(env, type);

                env->CallBooleanMethod(jlist, mAdd, jtype);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);

                env->DeleteLocalRef(jtype);
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeGetEnabledInputMethodSubtypeList()");
    return jlist;
}

static jobject android_view_inputmethod_ElInputMethodManagerProxy_nativeGetLastInputMethodSubtype(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeGetLastInputMethodSubtype()");

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    AutoPtr<IInputMethodSubtype> type;
    ECode ec = im->GetLastInputMethodSubtype((IInputMethodSubtype**)&type);
    if (FAILED(ec))
        ALOGE("nativeGetLastInputMethodSubtype() ec: 0x%08x", ec);

    jobject jtype = NULL;
    if (type != NULL) {
        jtype = ElUtil::GetJavaInputMethodSubtype(env, type);
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeGetLastInputMethodSubtype()");
    return jtype;
}

static jobject android_view_inputmethod_ElInputMethodManagerProxy_nativeGetShortcutInputMethodsAndSubtypes(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeGetShortcutInputMethodsAndSubtypes()");

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = im->GetShortcutInputMethodsAndSubtypes((IList**)&list);
    if (FAILED(ec))
        ALOGE("nativeGetShortcutInputMethodsAndSubtypes() ec: 0x%08x", ec);

    jobject jlist = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jlist = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IInputMethodInfo> info = IInputMethodInfo::Probe(obj);
            if (info != NULL) {
                jobject jinfo = ElUtil::GetJavaInputMethodInfo(env, info);

                env->CallBooleanMethod(jlist, mAdd, jinfo);
                ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);

                env->DeleteLocalRef(jinfo);
            } else {
                AutoPtr<IInputMethodSubtype> type = IInputMethodSubtype::Probe(obj);
                if (type != NULL) {
                    jobject jtype = ElUtil::GetJavaInputMethodSubtype(env, type);

                    env->CallBooleanMethod(jlist, mAdd, jtype);
                    ElUtil::CheckErrorAndLog(env, "CallObjectMethod: mAdd : %d!\n", __LINE__);

                    env->DeleteLocalRef(jtype);
                }
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeGetShortcutInputMethodsAndSubtypes()");
    return jlist;
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeAddClient(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jclient, jobject jinputContext, jint juid, jint jpid)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeAddClient()");

    AutoPtr<IInputMethodClient> client;
    if(jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);

        Mutex::Autolock lock(sIMClientMapLock);
        if (sIMClientMap.Find(hashCode) != sIMClientMap.End()) {
            client = sIMClientMap[hashCode];
        } else {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jclient);

            ECode ec = CInputMethodClientNative::New((Handle64)jvm, (Handle64)jInstance, (IInputMethodClient**)&client);
            if (FAILED(ec)) {
                ALOGE("nativeAddClient() new CInputMethodClientNative fail ec: 0x%x\n", ec);
            }
            sIMClientMap[hashCode] = client;
        }
    }

    AutoPtr<IIInputContext> inputContext;
    if(jinputContext != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jinputContext);
        Mutex::Autolock lock(sInContextMapLock);
        if (sInContextMap.Find(hashCode) != sInContextMap.End()) {
            inputContext = sInContextMap[hashCode];
        } else {
            ALOGE("nativeAddClient() Unknown InputContext!");
        }
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    ECode ec = im->AddClient(client, inputContext, (Int32)juid, (Int32)jpid);
    if (FAILED(ec))
        ALOGE("nativeAddClient() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeAddClient()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeRemoveClient(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jclient)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeRemoveClient()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);

    AutoPtr<IInputMethodClient> client;
    {
        Mutex::Autolock lock(sIMClientMapLock);
        HashMap<Int32, AutoPtr<IInputMethodClient> >::Iterator it = sIMClientMap.Find(hashCode);
        if (it != sIMClientMap.End()) {
            client = it->mSecond;
            sIMClientMap.Erase(it);
        } else {
            ALOGE("nativeRemoveClient() Unknown IInputMethodClient !");
        }
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    ECode ec = im->RemoveClient(client);
    if (FAILED(ec))
        ALOGE("nativeRemoveClient() ec: 0x%08x", ec);
    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeRemoveClient()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeShowInputMethodAndSubtypeEnablerFromClient(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jclient, jstring jtopId)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeShowInputMethodAndSubtypeEnablerFromClient()");

    AutoPtr<IInputMethodClient> client;
    if (jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);
        Mutex::Autolock lock(sIMClientMapLock);
        HashMap<Int32, AutoPtr<IInputMethodClient> >::Iterator it = sIMClientMap.Find(hashCode);
        if (it != sIMClientMap.End()) {
            client = it->mSecond;
        } else {
            ALOGE("nativeShowInputMethodAndSubtypeEnablerFromClient() Unknown IInputMethodClient !");
        }
    }

    String topId = ElUtil::ToElString(env, jtopId);

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    ECode ec = im->ShowInputMethodAndSubtypeEnablerFromClient(client, topId);
    if (FAILED(ec))
        ALOGE("nativeShowInputMethodAndSubtypeEnablerFromClient() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeShowInputMethodAndSubtypeEnablerFromClient()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethod(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jstring jid)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethod()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, binderClass)) {
            jfieldID f = env->GetFieldID(binderClass, "mNativeProxy", "I");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int32)env->GetIntField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        } else {
            String objClassName = ElUtil::GetClassName(env, jtoken);
            ALOGE("nativeSetInputMethod() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(binderClass);
    } else {
        ALOGE("nativeSetInputMethod() what is this token ?");
        assert(0);
    }

    String id = ElUtil::ToElString(env, jid);

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    ECode ec = im->SetInputMethod(token, id);
    if (FAILED(ec))
        ALOGE("nativeSetInputMethod() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethod()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethodAndSubtype(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jstring jid, jobject jsubtype)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethodAndSubtype()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, binderClass)) {
            jfieldID f = env->GetFieldID(binderClass, "mNativeProxy", "I");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int32)env->GetIntField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        } else {
            String objClassName = ElUtil::GetClassName(env, jtoken);
            ALOGE("nativeSetInputMethodAndSubtype() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(binderClass);
    } else {
        ALOGE("nativeSetInputMethodAndSubtype() what is this token ?");
        assert(0);
    }

    String id = ElUtil::ToElString(env, jid);

    AutoPtr<IInputMethodSubtype> subtype;
    if (jsubtype != NULL) {
        if (!ElUtil::ToElInputMethodSubtype(env, jsubtype, (IInputMethodSubtype**)&subtype)) {
            ALOGE("nativeSetInputMethodAndSubtype() ToElInputMethodSubtype fail!");
        }
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    ECode ec = im->SetInputMethodAndSubtype(token, id, subtype);
    if (FAILED(ec))
        ALOGE("nativeSetInputMethodAndSubtype() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethodAndSubtype()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeShowMySoftInput(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jint jflags)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeShowMySoftInput()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, binderClass)) {
            jfieldID f = env->GetFieldID(binderClass, "mNativeProxy", "I");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int32)env->GetIntField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        } else {
            String objClassName = ElUtil::GetClassName(env, jtoken);
            ALOGE("nativeShowMySoftInput() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(binderClass);
    } else {
        ALOGE("nativeShowMySoftInput() what is this token ?");
        assert(0);
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    ECode ec = im->ShowMySoftInput(token, (Int32)jflags);
    if (FAILED(ec))
        ALOGE("nativeShowMySoftInput() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeShowMySoftInput()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeRegisterSuggestionSpansForNotification(JNIEnv* env, jobject clazz, jlong jproxy,
    jobjectArray jspans)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeRegisterSuggestionSpansForNotification()");

    AutoPtr<ArrayOf<ISuggestionSpan*> > spans;
    if (jspans != NULL) {
        int size = env->GetArrayLength(jspans);
        spans = ArrayOf<ISuggestionSpan*>::Alloc(size);

        for(int i = 0; i < size; i++){
            jobject jspan = env->GetObjectArrayElement(jspans, i);
            ElUtil::CheckErrorAndLog(env, "GetObjectArrayelement failed : %d!\n", __LINE__);
            AutoPtr<ISuggestionSpan> span;
            if(!ElUtil::ToElSuggestionSpan(env, jspan, (ISuggestionSpan**)&span)) {
                ALOGE("nativeRegisterSuggestionSpansForNotification() ToElSuggestionSpan fail!");
            }
            spans->Set(i, span);
            env->DeleteLocalRef(jspan);
        }
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    ECode ec = im->RegisterSuggestionSpansForNotification(spans);
    if (FAILED(ec))
        ALOGE("nativeRegisterSuggestionSpansForNotification() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeRegisterSuggestionSpansForNotification()");
}

static jboolean android_view_inputmethod_ElInputMethodManagerProxy_nativeNotifySuggestionPicked(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jspan, jstring joriginalString, jint jindex)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeNotifySuggestionPicked()");

    AutoPtr<ISuggestionSpan> span;
    if (jspan != NULL) {
        if(!ElUtil::ToElSuggestionSpan(env, jspan, (ISuggestionSpan**)&span)) {
            ALOGE("nativeGetIntentSender() ToElSuggestionSpan fail!");
        }
    }

    String originalString = ElUtil::ToElString(env, joriginalString);

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = im->NotifySuggestionPicked(span, originalString, (Int32)jindex, &result);
    if (FAILED(ec))
        ALOGE("nativeNotifySuggestionPicked() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeNotifySuggestionPicked()");
    return (jboolean)result;
}

static jobject android_view_inputmethod_ElInputMethodManagerProxy_nativeGetCurrentInputMethodSubtype(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeGetCurrentInputMethodSubtype()");

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    AutoPtr<IInputMethodSubtype> subtype;
    ECode ec = im->GetCurrentInputMethodSubtype((IInputMethodSubtype**)&subtype);
    if (FAILED(ec))
        ALOGE("nativeGetCurrentInputMethodSubtype() ec: 0x%08x", ec);

    jobject jsubtype = NULL;
    if (subtype != NULL) {
        jsubtype = ElUtil::GetJavaInputMethodSubtype(env, subtype);
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeGetCurrentInputMethodSubtype()");
    return jsubtype;
}

static jboolean android_view_inputmethod_ElInputMethodManagerProxy_nativeSetCurrentInputMethodSubtype(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jsubtype)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeSetCurrentInputMethodSubtype()");

    AutoPtr<IInputMethodSubtype> subtype;
    if (jsubtype != NULL) {
        if(!ElUtil::ToElInputMethodSubtype(env, jsubtype, (IInputMethodSubtype**)&subtype)) {
            ALOGE("nativeSetCurrentInputMethodSubtype() ToElInputMethodSubtype fail!");
        }
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = im->SetCurrentInputMethodSubtype(subtype, &result);
    if (FAILED(ec))
        ALOGE("nativeSetCurrentInputMethodSubtype() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeSetCurrentInputMethodSubtype()");
    return (jboolean)result;
}

static jboolean android_view_inputmethod_ElInputMethodManagerProxy_nativeSwitchToLastInputMethod(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeSwitchToLastInputMethod()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, binderClass)) {
            jfieldID f = env->GetFieldID(binderClass, "mNativeProxy", "I");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int32)env->GetIntField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        } else {
            String objClassName = ElUtil::GetClassName(env, jtoken);
            ALOGE("nativeSwitchToLastInputMethod() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(binderClass);
    } else {
        ALOGE("nativeSwitchToLastInputMethod() what is this token ?");
        assert(0);
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = im->SwitchToLastInputMethod(token, &result);
    if (FAILED(ec))
        ALOGE("nativeSwitchToLastInputMethod() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeSwitchToLastInputMethod()");
    return (jboolean)result;
}

static jboolean android_view_inputmethod_ElInputMethodManagerProxy_nativeSwitchToNextInputMethod(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jboolean jonlyCurrentIme)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeSwitchToNextInputMethod()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, binderClass)) {
            jfieldID f = env->GetFieldID(binderClass, "mNativeProxy", "I");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int32)env->GetIntField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        } else {
            String objClassName = ElUtil::GetClassName(env, jtoken);
            ALOGE("nativeSwitchToNextInputMethod() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(binderClass);
    } else {
        ALOGE("nativeSwitchToNextInputMethod() what is this token ?");
        assert(0);
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = im->SwitchToNextInputMethod(token, (Boolean)jonlyCurrentIme, &result);
    if (FAILED(ec))
        ALOGE("nativeSwitchToNextInputMethod() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeSwitchToNextInputMethod()");
    return (jboolean)result;
}

static jboolean android_view_inputmethod_ElInputMethodManagerProxy_nativeShouldOfferSwitchingToNextInputMethod(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeShouldOfferSwitchingToNextInputMethod()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        jclass binderClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, binderClass)) {
            jfieldID f = env->GetFieldID(binderClass, "mNativeProxy", "I");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int32)env->GetIntField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetIntField mNativeProxy : %d!\n", __LINE__);
        } else {
            String objClassName = ElUtil::GetClassName(env, jtoken);
            ALOGE("nativeShouldOfferSwitchingToNextInputMethod() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(binderClass);
    } else {
        ALOGE("nativeShouldOfferSwitchingToNextInputMethod() what is this token ?");
        assert(0);
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = im->ShouldOfferSwitchingToNextInputMethod(token, &result);
    if (FAILED(ec))
        ALOGE("nativeShouldOfferSwitchingToNextInputMethod() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeShouldOfferSwitchingToNextInputMethod()");
    return (jboolean)result;
}

static jboolean android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethodEnabled(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid, jboolean jenabled)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethodEnabled()");

    String id = ElUtil::ToElString(env, jid);

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = im->SetInputMethodEnabled(id, (Boolean)jenabled, &result);
    if (FAILED(ec))
        ALOGE("nativeSetInputMethodEnabled() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethodEnabled()");
    return (jboolean)result;
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeSetAdditionalInputMethodSubtypes(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid, jobjectArray jsubtypes)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeSetAdditionalInputMethodSubtypes()");

    String id = ElUtil::ToElString(env, jid);

    AutoPtr<ArrayOf<IInputMethodSubtype*> > subtypes;
    if (jsubtypes != NULL) {
        int size = env->GetArrayLength(jsubtypes);
        subtypes = ArrayOf<IInputMethodSubtype*>::Alloc(size);

        for(int i = 0; i < size; i++){
            jobject jsubtype = env->GetObjectArrayElement(jsubtypes, i);
            ElUtil::CheckErrorAndLog(env, "GetObjectArrayelement failed : %d!\n", __LINE__);
            AutoPtr<IInputMethodSubtype> subtype;
            if(!ElUtil::ToElInputMethodSubtype(env, jsubtype, (IInputMethodSubtype**)&subtype)) {
                ALOGE("nativeSetAdditionalInputMethodSubtypes() ToElInputMethodSubtype fail!");
            }
            subtypes->Set(i, subtype);
            env->DeleteLocalRef(jsubtype);
        }
    }

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    ECode ec = im->SetAdditionalInputMethodSubtypes(id, subtypes);
    if (FAILED(ec))
        ALOGE("nativeSetAdditionalInputMethodSubtypes() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeSetAdditionalInputMethodSubtypes()");
}

static jint android_view_inputmethod_ElInputMethodManagerProxy_nativeGetInputMethodWindowVisibleHeight(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeGetInputMethodWindowVisibleHeight()");

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    Int32 result;
    ECode ec = im->GetInputMethodWindowVisibleHeight(&result);
    if (FAILED(ec))
        ALOGE("nativeGetInputMethodWindowVisibleHeight() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeGetInputMethodWindowVisibleHeight()");
    return result;
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeNotifyUserAction(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jsequenceNumber)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeNotifyUserAction()");

    IIInputMethodManager* im = (IIInputMethodManager*)jproxy;
    ECode ec = im->NotifyUserAction(jsequenceNumber);
    if (FAILED(ec))
        ALOGE("nativeNotifyUserAction() ec: 0x%08x", ec);

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeNotifyUserAction()");
}

static void android_view_inputmethod_ElInputMethodManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_view_inputmethod_ElInputMethodManagerProxy_nativeFinalize()");

    IIInputMethodManager* obj = (IIInputMethodManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_view_inputmethod_ElInputMethodManagerProxy_nativeFinalize()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeFinalize },
    { "nativeWindowGainedFocus",    "(JILandroid/os/IBinder;IIILandroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;)Lcom/android/internal/view/InputBindResult;",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeWindowGainedFocus },
    { "nativeStartInput",    "(JILcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeStartInput },
    { "nativeFinishInput",    "(JI)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeFinishInput },
    { "nativeHideMySoftInput",    "(JLandroid/os/IBinder;I)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeHideMySoftInput },
    { "nativeGetInputMethodList",    "(J)Ljava/util/List;",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeGetInputMethodList },
    { "nativeShowSoftInput",    "(JIILandroid/os/ResultReceiver;)Z",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeShowSoftInput },
    { "nativeHideSoftInput",    "(JIILandroid/os/ResultReceiver;)Z",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeHideSoftInput },
    { "nativeGetEnabledInputMethodList",    "(J)Ljava/util/List;",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeGetEnabledInputMethodList },
    { "nativeShowInputMethodPickerFromClient",    "(JI)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeShowInputMethodPickerFromClient },
    { "nativeSetImeWindowStatus",    "(JLandroid/os/IBinder;II)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeSetImeWindowStatus },
    { "nativeUpdateStatusIcon",    "(JLandroid/os/IBinder;Ljava/lang/String;I)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeUpdateStatusIcon },
    { "nativeGetEnabledInputMethodSubtypeList",    "(JLjava/lang/String;Z)Ljava/util/List;",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeGetEnabledInputMethodSubtypeList },
    { "nativeGetLastInputMethodSubtype",    "(J)Landroid/view/inputmethod/InputMethodSubtype;",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeGetLastInputMethodSubtype },
    { "nativeGetShortcutInputMethodsAndSubtypes",    "(J)Ljava/util/List;",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeGetShortcutInputMethodsAndSubtypes },
    { "nativeAddClient",    "(JLcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;II)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeAddClient },
    { "nativeRemoveClient",    "(JLcom/android/internal/view/IInputMethodClient;)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeRemoveClient },
    { "nativeShowInputMethodAndSubtypeEnablerFromClient",    "(JLcom/android/internal/view/IInputMethodClient;Ljava/lang/String;)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeShowInputMethodAndSubtypeEnablerFromClient },
    { "nativeSetInputMethod",    "(JLandroid/os/IBinder;Ljava/lang/String;)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethod },
    { "nativeSetInputMethodAndSubtype",    "(JLandroid/os/IBinder;Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethodAndSubtype },
    { "nativeShowMySoftInput",    "(JLandroid/os/IBinder;I)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeShowMySoftInput },
    { "nativeRegisterSuggestionSpansForNotification",    "(J[Landroid/text/style/SuggestionSpan;)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeRegisterSuggestionSpansForNotification },
    { "nativeNotifySuggestionPicked",    "(JLandroid/text/style/SuggestionSpan;Ljava/lang/String;I)Z",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeNotifySuggestionPicked },
    { "nativeGetCurrentInputMethodSubtype",    "(J)Landroid/view/inputmethod/InputMethodSubtype;",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeGetCurrentInputMethodSubtype },
    { "nativeSetCurrentInputMethodSubtype",    "(JLandroid/view/inputmethod/InputMethodSubtype;)Z",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeSetCurrentInputMethodSubtype },
    { "nativeSwitchToLastInputMethod",    "(JLandroid/os/IBinder;)Z",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeSwitchToLastInputMethod },
    { "nativeSwitchToNextInputMethod",    "(JLandroid/os/IBinder;Z)Z",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeSwitchToNextInputMethod },
    { "nativeShouldOfferSwitchingToNextInputMethod",    "(JLandroid/os/IBinder;)Z",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeShouldOfferSwitchingToNextInputMethod },
    { "nativeSetInputMethodEnabled",    "(JLjava/lang/String;Z)Z",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeSetInputMethodEnabled },
    { "nativeSetAdditionalInputMethodSubtypes",    "(JLjava/lang/String;[Landroid/view/inputmethod/InputMethodSubtype;)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeSetAdditionalInputMethodSubtypes },
    { "nativeGetInputMethodWindowVisibleHeight",    "(J)I",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeGetInputMethodWindowVisibleHeight },
    { "nativeNotifyUserAction",    "(JI)V",
            (void*) android_view_inputmethod_ElInputMethodManagerProxy_nativeNotifyUserAction },
};

int register_android_view_inputmethod_ElInputMethodManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/view/inputmethod/ElInputMethodManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

