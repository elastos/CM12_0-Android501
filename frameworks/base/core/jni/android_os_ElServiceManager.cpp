#define LOG_TAG "ElServiceManagerJNI"
#include "JNIHelp.h"
#include "jni.h"
#include <utils/Log.h>
#include <Elastos.Droid.Accounts.h>
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Database.h>
#include <Elastos.Droid.Graphics.h>
#include <Elastos.Droid.Hardware.h>
#include <Elastos.Droid.Location.h>
#include <Elastos.Droid.Media.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
// #include <Elastos.Droid.Privacy.h>
#include <Elastos.Droid.View.h>
#include <Elastos.Droid.Service.h>
#include <Elastos.Droid.Media.h>
#include <Elastos.Droid.Text.h>
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Webkit.h>
#include <Elastos.Droid.Widget.h>
#include <Elastos.Droid.Wifi.h>
#include <Elastos.Droid.Utility.h>

#include "ElUtil.h"

using namespace Elastos;

using Elastos::Droid::Accounts::IIAccountManager;
using Elastos::Droid::App::Admin::IIDevicePolicyManager;
using Elastos::Droid::App::Backup::IIBackupManager;
using Elastos::Droid::App::IIActivityManager;
using Elastos::Droid::App::IIAlarmManager;
using Elastos::Droid::App::IINotificationManager;
using Elastos::Droid::App::IISearchManager;
using Elastos::Droid::App::IIWallpaperManager;
using Elastos::Droid::Content::IIContentService;
using Elastos::Droid::Content::IIClipboard;
using Elastos::Droid::Content::Pm::IIPackageManager;
using Elastos::Droid::Hardware::Display::IIDisplayManager;
using Elastos::Droid::Hardware::Usb::IIUsbManager;
using Elastos::Droid::Internal::AppWidget::IIAppWidgetService;
using Elastos::Droid::Internal::Os::IIDropBoxManagerService;
using Elastos::Droid::Internal::TextService::IITextServicesManager;
using Elastos::Droid::Internal::Telephony::IISms;
using Elastos::Droid::Internal::Telephony::IISub;
using Elastos::Droid::Location::IICountryDetector;
using Elastos::Droid::Location::IILocationManager;
using Elastos::Droid::Media::IIAudioService;
using Elastos::Droid::Net::IIConnectivityManager;
using Elastos::Droid::Net::IIEthernetManager;
using Elastos::Droid::Net::IINetworkPolicyManager;
using Elastos::Droid::Net::IINetworkStatsService;
using Elastos::Droid::Net::Nsd::IINsdManager;
using Elastos::Droid::Wifi::IIWifiManager;
using Elastos::Droid::Wifi::P2p::IIWifiP2pManager;
using Elastos::Droid::Os::IIUserManager;
using Elastos::Droid::Os::IServiceManager;
using Elastos::Droid::Os::IIPowerManager;
using Elastos::Droid::Os::CServiceManager;
using Elastos::Droid::Os::Storage::IIMountService;
// using Elastos::Droid::Privacy::IIPrivacySettingsManager;
using Elastos::Droid::View::IIWindowManager;
using Elastos::Droid::View::Accessibility::IIAccessibilityManager;
using Elastos::Droid::Hardware::Input::IIInputManager;
using Elastos::Droid::Internal::App::IIAppOpsService;
using Elastos::Droid::Internal::View::IIInputMethodManager;
using Elastos::Droid::Internal::Widget::IILockSettings;
using Elastos::Droid::Webkit::IIWebViewUpdateService;
using Elastos::Droid::Media::IIMediaRouterService;
using Elastos::Droid::Media::Session::IISessionManager;

static AutoPtr<IServiceManager> sServiceManager;

namespace android {

static jobject android_os_ElServiceManagerProxy_nativeGetService(JNIEnv* env, jobject clazz, jstring jname) {
    const char* name = env->GetStringUTFChars(jname, NULL);
    // ALOGD("android_os_ElServiceManagerProxy_nativeGetService() name: %s", name);

    IServiceManager* serviceManager = sServiceManager;
    AutoPtr<IInterface> service;
    if (!strcmp(name, "activity")) {
        ECode ec = serviceManager->GetService(String("activity"), (IInterface**)&service);
        // ALOGD("==== Call android.os.ElServiceManager.getNativeServiceManager() 3 ec: 0x%08x====", ec);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Activity Manager FAILED!");
            return NULL;
        }
        AutoPtr<IIActivityManager> activityManager = IIActivityManager::Probe(service);

        env->ReleaseStringUTFChars(jname, name);

        jclass c = env->FindClass("android/app/ElActivityManagerProxy");
        if (env->ExceptionCheck() != 0) {
            ALOGE("*** Uncaught exception returned from Java call 1111!\n");
            env->ExceptionDescribe();
        }

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        if (env->ExceptionCheck() != 0) {
            ALOGE("*** Uncaught exception returned from Java call 2222!\n");
            env->ExceptionDescribe();
        }

        jobject obj = env->NewObject(c, m, (jlong)activityManager.Get());
        if (env->ExceptionCheck() != 0) {
            ALOGE("*** Uncaught exception returned from Java call 3333!\n");
            env->ExceptionDescribe();
        }
        activityManager->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "package")) {
        ECode ec = serviceManager->GetService(String("package"), (IInterface**)&service);
        // ALOGD("android_os_ElServiceManagerProxy_nativeGetService() ec: 0x%08x pm:0x%08x====", ec, pm);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Package Manager FAILED!");
            return NULL;
        }
        AutoPtr<IIPackageManager> pm = IIPackageManager::Probe(service);

        jclass c = env->FindClass("android/content/pm/ElPackageManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElPackageManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ElPackageManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)pm.Get());
        ElUtil::CheckErrorAndLog(env, "NewObject: ElPackageManagerProxy : %d!\n", __LINE__);
        pm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "window")) {
        ECode ec = serviceManager->GetService(String("window"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Window Manager FAILED!");
            return NULL;
        }

        AutoPtr<IIWindowManager> wm = IIWindowManager::Probe(service);
        jclass c = env->FindClass("android/view/ElWindowManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWindowManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ElWindowManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)wm.Get());
        ElUtil::CheckErrorAndLog(env, "NewObject: ElWindowManagerProxy : %d!\n", __LINE__);
        wm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "display")) {
        ECode ec = serviceManager->GetService(String("display"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Display Manager FAILED!");
            return NULL;
        }

        AutoPtr<IIDisplayManager> wm = IIDisplayManager::Probe(service);
        jclass c = env->FindClass("android/hardware/display/ElDisplayManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWindowManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ElWindowManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)wm.Get());
        ElUtil::CheckErrorAndLog(env, "NewObject: ElWindowManagerProxy : %d!\n", __LINE__);
        wm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "accessibility")) {
        ECode ec = serviceManager->GetService(String("accessibility"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Accessibility Manager FAILED!");
            return NULL;
        }

        // AutoPtr<IIAccessibilityManager> accsm = IIAccessibilityManager::Probe(service);
        // jclass c = env->FindClass("android/view/ElAccessibilityManagerProxy");
        // ElUtil::CheckErrorAndLog(env, "FindClass: ElAccessibilityManagerProxy : %d!\n", __LINE__);

        // jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        // ElUtil::CheckErrorAndLog(env, "GetMethodID: ElAccessibilityManagerProxy : %d!\n", __LINE__);

        // jobject obj = env->NewObject(c, m, (jlong)accsm.Get());
        // ElUtil::CheckErrorAndLog(env, "NewObject: ElAccessibilityManagerProxy : %d!\n", __LINE__);
        // accsm->AddRef();
        // env->DeleteLocalRef(c);
        // return obj;
        return NULL;
    } else if (!strcmp(name, "power")) {
        ECode ec = serviceManager->GetService(String("power"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Power Manager FAILED!");
            return NULL;
        }

        AutoPtr<IIPowerManager> pm = IIPowerManager::Probe(service);
        jclass c = env->FindClass("android/os/ElPowerManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElPowerManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ElPowerManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)pm.Get());
        ElUtil::CheckErrorAndLog(env, "NewObject: ElPowerManagerProxy : %d!\n", __LINE__);
        pm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "input_method")) {
        ECode ec = serviceManager->GetService(String("input_method"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Input Method Manager FAILED!");
            return NULL;
        }

        AutoPtr<IIInputMethodManager> im = IIInputMethodManager::Probe(service);
        jclass c = env->FindClass("android/view/inputmethod/ElInputMethodManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElInputMethodManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElInputMethodManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)im.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElInputMethodManagerProxy : %d!\n", __LINE__);
        im->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "input")) {
        ECode ec = serviceManager->GetService(String("input"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Input Manager FAILED!");
            return NULL;
        }

        AutoPtr<IIInputManager> im = IIInputManager::Probe(service);
        jclass c = env->FindClass("android/hardware/input/ElInputManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElInputManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElInputManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)im.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElInputManagerProxy : %d!\n", __LINE__);
        im->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "mount")) {
        ECode ec = serviceManager->GetService(String("mount"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Mount Seervice FAILED!");
            return NULL;
        }

        AutoPtr<IIMountService> ms = IIMountService::Probe(service);
        jclass c = env->FindClass("android/os/storage/ElMountServiceProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElMountServiceProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElMountServiceProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)ms.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElMountServiceProxy : %d!\n", __LINE__);
        ms->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "content")) {
        ECode ec = serviceManager->GetService(String("content"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Content Seervice FAILED!");
            return NULL;
        }

        AutoPtr<IIContentService> cs = IIContentService::Probe(service);
        jclass c = env->FindClass("android/content/ElContentServiceProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElContentServiceProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElContentServiceProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)cs.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElContentServiceProxy : %d!\n", __LINE__);
        cs->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "appwidget")) {
        ECode ec = serviceManager->GetService(String("appwidget"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get appwidget Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IIAppWidgetService> aws = IIAppWidgetService::Probe(service);
        jclass c = env->FindClass("android/appwidget/ElAppWidgetManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAppWidgetManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAppWidgetManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)aws.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAppWidgetManagerProxy : %d!\n", __LINE__);
        aws->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "search")) {
        ECode ec = serviceManager->GetService(String("search"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() get Search Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IISearchManager> sm = IISearchManager::Probe(service);
        jclass c = env->FindClass("android/app/ElSearchManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSearchManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSearchManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)sm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSearchManagerProxy : %d!\n", __LINE__);
        sm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "textservices")) {
        ECode ec = serviceManager->GetService(String("textservices"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Text Service Manager FAILED! ec: 0x%08x", ec);
            return NULL;
        }

        AutoPtr<IITextServicesManager> tsm = IITextServicesManager::Probe(service);
        jclass c = env->FindClass("android/view/textservice/ElTextServicesManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElTextServicesManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElTextServicesManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)tsm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElTextServicesManagerProxy : %d!\n", __LINE__);
        tsm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "audio")) {
        ECode ec = serviceManager->GetService(String("audio"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() audio Service Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IIAudioService> as = IIAudioService::Probe(service);
        jclass c = env->FindClass("android/media/ElAudioServiceProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAudioServiceProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAudioServiceProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)as.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAudioServiceProxy : %d!\n", __LINE__);
        as->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "wifi")) {
        ECode ec = serviceManager->GetService(String("wifi"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Wifi Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IIWifiManager> wim  = IIWifiManager::Probe(service);
        jclass c = env->FindClass("android/net/wifi/ElWifiManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWifiManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWifiManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)wim.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWifiManagerProxy : %d!\n", __LINE__);
        wim->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    // } else if (!strcmp(name, "privacy")) {
    //     ECode ec = serviceManager->GetService(String("privacy"), (IInterface**)&service);
    //     if (FAILED(ec)) {
    //         ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Privacy Service FAILED! ====");
    //         return NULL;
    //     }

    //     AutoPtr<IIPrivacySettingsManager> psm = IIPrivacySettingsManager::Probe(service);
    //     jclass c = env->FindClass("android/privacy/ElPrivacySettingsManagerProxy");
    //     ElUtil::CheckErrorAndLog(env, "FindClass: ElPrivacySettingsManagerProxy : %d!\n", __LINE__);

    //     jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
    //     ElUtil::CheckErrorAndLog(env, "FindClass: ElPrivacySettingsManagerProxy : %d!\n", __LINE__);

    //     jobject obj = env->NewObject(c, m, (jlong)psm.Get());
    //     ElUtil::CheckErrorAndLog(env, "FindClass: ElPrivacySettingsManagerProxy : %d!\n", __LINE__);
    //     psm->AddRef();
    //     env->DeleteLocalRef(c);
    //     return obj;
    } else if (!strcmp(name, "alarm")) {
        ECode ec = serviceManager->GetService(String("alarm"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Alarm Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IIAlarmManager> alarm = IIAlarmManager::Probe(service);
        jclass c = env->FindClass("android/app/ElAlarmManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAlarmManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAlarmManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)alarm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAlarmManagerProxy : %d!\n", __LINE__);
        alarm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "user")) {
        ECode ec = serviceManager->GetService(String("user"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() User Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IIUserManager> um = IIUserManager::Probe(service);
        jclass c = env->FindClass("android/os/ElUserManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElUserManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElUserManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)um.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElUserManagerProxy : %d!\n", __LINE__);
        um->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "ethernet")) {
        ECode ec = serviceManager->GetService(String("ethernet"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Ethernet Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IIEthernetManager> ethm = IIEthernetManager::Probe(service);
        jclass c = env->FindClass("android/net/ethernet/ElEthernetManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElEthernetManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElEthernetManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)ethm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElEthernetManagerProxy : %d!\n", __LINE__);
        ethm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "connectivity")) {
        ECode ec = serviceManager->GetService(String("connectivity"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Connectivity Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IIConnectivityManager> connm = IIConnectivityManager::Probe(service);
        jclass c = env->FindClass("android/net/ElConnectivityManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElConnectivityManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElConnectivityManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)connm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElConnectivityManagerProxy : %d!\n", __LINE__);
        connm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "account")) {
        ECode ec = serviceManager->GetService(String("account"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Account Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IIAccountManager> actm = IIAccountManager::Probe(service);
        jclass c = env->FindClass("android/accounts/ElAccountManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAccountManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAccountManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)actm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAccountManagerProxy : %d!\n", __LINE__);
        actm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "notification")) {
        ECode ec = serviceManager->GetService(String("notification"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Notification Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IINotificationManager> notim = IINotificationManager::Probe(service);
        jclass c = env->FindClass("android/app/ElNotificationManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNotificationManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNotificationManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)notim.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNotificationManagerProxy : %d!\n", __LINE__);
        notim->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "location")) {
        ECode ec = serviceManager->GetService(String("location"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Location Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IILocationManager> lm = IILocationManager::Probe(service);
        jclass c = env->FindClass("android/location/ElLocationManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElLocationManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElLocationManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)lm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElLocationManagerProxy : %d!\n", __LINE__);
        lm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "usb")) {
        ECode ec = serviceManager->GetService(String("usb"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Usb Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IIUsbManager> um = IIUsbManager::Probe(service);
        jclass c = env->FindClass("android/hardware/usb/ElUsbManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElUsbManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElUsbManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)um.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElUsbManagerProxy : %d!\n", __LINE__);
        um->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "clipboard")) {
        ECode ec = serviceManager->GetService(String("clipboard"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Clipboard Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IIClipboard> cbm = IIClipboard::Probe(service);
        jclass c = env->FindClass("android/content/ElClipboardProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElClipboardProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElClipboardProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)cbm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElClipboardProxy : %d!\n", __LINE__);
        cbm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "device_policy")) {
        ECode ec = serviceManager->GetService(String("device_policy"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Device Policy Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IIDevicePolicyManager> dpm = IIDevicePolicyManager::Probe(service);
        jclass c = env->FindClass("android/app/admin/ElDevicePolicyManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElDevicePolicyManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElDevicePolicyManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)dpm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElDevicePolicyManagerProxy : %d!\n", __LINE__);
        dpm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "wallpaper")) {
        ECode ec = serviceManager->GetService(String("wallpaper"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Wallpaper Manager FAILED! ====");
            return NULL;
        }

        AutoPtr<IIWallpaperManager> wallm = IIWallpaperManager::Probe(service);
        jclass c = env->FindClass("android/app/ElWallpaperManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWallpaperManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWallpaperManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)wallm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWallpaperManagerProxy : %d!\n", __LINE__);
        wallm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "dropbox")) {
        ECode ec = serviceManager->GetService(String("dropbox"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() DropBox Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IIDropBoxManagerService> dboxm = IIDropBoxManagerService::Probe(service);
        jclass c = env->FindClass("com/android/internal/os/ElDropBoxManagerServiceProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElDropBoxManagerServiceProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElDropBoxManagerServiceProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)dboxm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElDropBoxManagerServiceProxy : %d!\n", __LINE__);
        dboxm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "wifip2p")) {
        ECode ec = serviceManager->GetService(String("wifip2p"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Wifi P2p Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IIWifiP2pManager> wp = IIWifiP2pManager::Probe(service);
        jclass c = env->FindClass("android/net/wifi/p2p/ElWifiP2pServiceProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWifiP2pServiceProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWifiP2pServiceProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)wp.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWifiP2pServiceProxy : %d!\n", __LINE__);
        wp->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "backup")) {
        ECode ec = serviceManager->GetService(String("backup"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() backup  Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IIBackupManager> bm = IIBackupManager::Probe(service);
        jclass c = env->FindClass("android/app/backup/ElBackupManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElBackupManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElBackupManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)bm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElBackupManagerProxy : %d!\n", __LINE__);
        bm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "country_detector")) {
        ECode ec = serviceManager->GetService(String("country_detector"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() Country Detector FAILED! ====");
            return NULL;
        }

        AutoPtr<IICountryDetector> icd = IICountryDetector::Probe(service);
        jclass c = env->FindClass("android/location/ElCountryDetectorProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElCountryDetectorProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElCountryDetectorProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)icd.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElCountryDetectorProxy : %d!\n", __LINE__);
        icd->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "lock_settings")) {
        ECode ec = serviceManager->GetService(String("lock_settings"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() lock_settings Service FAILED! ====");
            return NULL;
        }

        IILockSettings* ls = IILockSettings::Probe(service);
        jclass c = env->FindClass("com/android/internal/widget/ElLockSettingsProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElLockSettingsProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElLockSettingsProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)ls);
        ElUtil::CheckErrorAndLog(env, "FindClass: ElLockSettingsProxy : %d!\n", __LINE__);
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "servicediscovery")) {
        ECode ec = serviceManager->GetService(String("servicediscovery"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() servicediscovery Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IINsdManager> nm = IINsdManager::Probe(service);
        jclass c = env->FindClass("android/net/nsd/ElNsdManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNsdManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNsdManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)nm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNsdManagerProxy : %d!\n", __LINE__);
        nm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "webviewupdate")) {
        ECode ec = serviceManager->GetService(String("webviewupdate"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() webviewupdate Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IIWebViewUpdateService> wvus = IIWebViewUpdateService::Probe(service);
        jclass c = env->FindClass("android/webkit/ElWebViewUpdateServiceProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWebViewUpdateServiceProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWebViewUpdateServiceProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)wvus.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElWebViewUpdateServiceProxy : %d!\n", __LINE__);
        wvus->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "netpolicy")) {
        ECode ec = serviceManager->GetService(String("netpolicy"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() netpolicy Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IINetworkPolicyManager> npm = IINetworkPolicyManager::Probe(service);
        jclass c = env->FindClass("android/net/ElNetworkPolicyManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNetworkPolicyManagerProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNetworkPolicyManagerProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)npm.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNetworkPolicyManagerProxy : %d!\n", __LINE__);
        npm->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "appops")) {
        ECode ec = serviceManager->GetService(String("appops"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() appops Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IIAppOpsService> aos = IIAppOpsService::Probe(service);
        jclass c = env->FindClass("com/android/internal/app/ElAppOpsServiceProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAppOpsServiceProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAppOpsServiceProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)aos.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElAppOpsServiceProxy : %d!\n", __LINE__);
        aos->AddRef();
        env->DeleteLocalRef(c);
        return obj;
     } else if (!strcmp(name, "netstats")) {
        ECode ec = serviceManager->GetService(String("netstats"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() netstats Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IINetworkStatsService> nss = IINetworkStatsService::Probe(service);
        jclass c = env->FindClass("android/net/ElNetworkStatsServiceProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNetworkStatsServiceProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNetworkStatsServiceProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)nss.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNetworkStatsServiceProxy : %d!\n", __LINE__);
        nss->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "isub")) {
        ECode ec = serviceManager->GetService(String("isub"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() isub Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IISub> isub = IISub::Probe(service);
        jclass c = env->FindClass("com/android/internal/telephony/ElSubscriptionController");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSubscriptionController : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSubscriptionController : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)isub.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSubscriptionController : %d!\n", __LINE__);
        isub->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    } else if (!strcmp(name, "isms")) {
        ECode ec = serviceManager->GetService(String("isms"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() isms Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IISms> isms = IISms::Probe(service);
        jclass c = env->FindClass("com/android/internal/telephony/ElUiccSmsController");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElUiccSmsController : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElUiccSmsController : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)isms.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElUiccSmsController : %d!\n", __LINE__);
        isms->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    }
    else if (!strcmp(name, "media_session")) {
        ECode ec = serviceManager->GetService(String("media_session"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() media_session Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IISessionManager> ism = IISessionManager::Probe(service);
        jclass c = env->FindClass("android/media/session/ElSessionManagerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSessionManagerProxy: %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSessionManagerProxy: %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)ism.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSessionManagerProxy: %d!\n", __LINE__);
        ism->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    }
    else if (!strcmp(name, "media_router")) {
        ECode ec = serviceManager->GetService(String("media_router"), (IInterface**)&service);
        if (FAILED(ec)) {
            ALOGE("android_os_ElServiceManagerProxy_nativeGetService() media_router Service FAILED! ====");
            return NULL;
        }

        AutoPtr<IIMediaRouterService> mrs = IIMediaRouterService::Probe(service);
        jclass c = env->FindClass("android/media/ElMediaRouterServiceProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElMediaRouterServiceProxy: %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElMediaRouterServiceProxy: %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)mrs.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElMediaRouterServiceProxy: %d!\n", __LINE__);
        mrs->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    }
    else {
        ALOGE("nativeGetService() unsupport service %s!", name);
    }

    return NULL;
}

static JNINativeMethod gMethods1[] = {
    { "nativeGetService",    "(Ljava/lang/String;)Landroid/os/IBinder;",
            (void*) android_os_ElServiceManagerProxy_nativeGetService },
};

int register_android_os_ElServiceManagerProxy(JNIEnv *env) {
    CServiceManager::AcquireSingleton((IServiceManager**)&sServiceManager);

    return jniRegisterNativeMethods(env, "android/os/ElServiceManagerProxy", gMethods1, NELEM(gMethods1));
}

} // namespace android
