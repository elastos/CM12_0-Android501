
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Internal::Os::IIDropBoxManagerService;
using Elastos::Droid::Os::IDropBoxManagerEntry;

static void com_android_internal_os_ElDropBoxManagerServiceProxy_nativeAdd(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jentry)
{
    // ALOGD("+ com_android_internal_os_ElDropBoxManagerServiceProxy_nativeAdd()");

    IIDropBoxManagerService* dboxs = (IIDropBoxManagerService*)jproxy;

    AutoPtr<IDropBoxManagerEntry> entry;
    if (jentry != NULL) {
        if (!ElUtil::ToElDropBoxManagerEntry(env, jentry, (IDropBoxManagerEntry**)&entry)) {
            ALOGE("ElDropBoxManagerServiceProxy::nativeAdd() ToElDropBoxManagerEntry fail!");
        }
    }

    dboxs->Add(entry);

    // ALOGD("- com_android_internal_os_ElDropBoxManagerServiceProxy_nativeAdd()");
}

static jboolean com_android_internal_os_ElDropBoxManagerServiceProxy_nativeIsTagEnabled(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtag)
{
    ALOGD("+ com_android_internal_os_ElDropBoxManagerServiceProxy_nativeIsTagEnabled()");

    IIDropBoxManagerService* dboxs = (IIDropBoxManagerService*)jproxy;

    String tag = ElUtil::ToElString(env, jtag);

    Boolean result = FALSE;
    ECode ec = dboxs->IsTagEnabled(tag, &result);
    ALOGE("ElDropBoxManagerServiceProxy::nativeIsTagEnabled() ec: 0x%08x result: %d", ec, result);

    ALOGD("- com_android_internal_os_ElDropBoxManagerServiceProxy_nativeIsTagEnabled()");
    return (jboolean)result;
}

static jobject com_android_internal_os_ElDropBoxManagerServiceProxy_nativeGetNextEntry(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtag, jlong jmillis)
{
    ALOGD("+ com_android_internal_os_ElDropBoxManagerServiceProxy_nativeGetNextEntry()");

    IIDropBoxManagerService* dboxs = (IIDropBoxManagerService*)jproxy;

    String tag = ElUtil::ToElString(env, jtag);

    AutoPtr<IDropBoxManagerEntry> entry;
    ECode ec = dboxs->GetNextEntry(tag, (Int64)jmillis, (IDropBoxManagerEntry**)&entry);
    ALOGE("ElDropBoxManagerServiceProxy::nativeGetNextEntry() ec: 0x%08x ", ec);

    jobject jentry = NULL;
    if (entry != NULL) {
        jentry = ElUtil::GetJavaDropBoxManagerEntry(env, entry);
    }

    ALOGD("- com_android_internal_os_ElDropBoxManagerServiceProxy_nativeGetNextEntry()");
    return jentry;
}

static void com_android_internal_os_ElDropBoxManagerServiceProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ com_android_internal_os_ElDropBoxManagerServiceProxy_nativeDestroy()");

    IIDropBoxManagerService* obj = (IIDropBoxManagerService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- com_android_internal_os_ElDropBoxManagerServiceProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) com_android_internal_os_ElDropBoxManagerServiceProxy_nativeFinalize },
    { "nativeAdd",    "(JLandroid/os/DropBoxManager$Entry;)V",
            (void*) com_android_internal_os_ElDropBoxManagerServiceProxy_nativeAdd },
    { "nativeIsTagEnabled",    "(JLjava/lang/String;)Z",
            (void*) com_android_internal_os_ElDropBoxManagerServiceProxy_nativeIsTagEnabled },
    { "nativeGetNextEntry",    "(JLjava/lang/String;J)Landroid/os/DropBoxManager$Entry;",
            (void*) com_android_internal_os_ElDropBoxManagerServiceProxy_nativeGetNextEntry },
};

int register_com_android_internal_os_ElDropBoxManagerServiceProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/internal/os/ElDropBoxManagerServiceProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

