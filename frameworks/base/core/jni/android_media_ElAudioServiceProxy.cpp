#define LOG_TAG "ElAudioServiceProxy"

#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include "android_runtime/AndroidRuntime.h"

#include <utils/Log.h>
#include <utils/Mutex.h>
#include <elastos/utility/etl/HashMap.h>
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Bluetooth.h>
#include <Elastos.Droid.Content.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.Droid.Media.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.View.h>
#include <Elastos.CoreLibrary.Core.h>

using android::ElUtil;
using android::AndroidRuntime;
using android::Mutex;
using Elastos::Droid::Bluetooth::CBluetoothDevice;
using Elastos::Droid::Bluetooth::IBluetoothDevice;
using Elastos::Droid::Content::IComponentName;
using Elastos::Droid::Media::IIAudioFocusDispatcher;
using Elastos::Droid::Media::IIAudioRoutesObserver;
using Elastos::Droid::Media::IIAudioService;
using Elastos::Droid::Media::IIVolumeController;
using Elastos::Droid::Media::IIRemoteControlClient;
using Elastos::Droid::Media::IIRemoteControlDisplay;
using Elastos::Droid::Media::IIRemoteVolumeObserver;
using Elastos::Droid::Media::IIRingtonePlayer;
using Elastos::Droid::JavaProxy::CBinderNative;
using Elastos::Droid::JavaProxy::CIAudioFocusDispatcher;
using Elastos::Droid::JavaProxy::CIAudioRoutesObserver;
using Elastos::Droid::JavaProxy::CIRemoteControlClient;
using Elastos::Droid::JavaProxy::CIRemoteControlDisplay;
using Elastos::Droid::JavaProxy::CIRemoteVolumeObserver;
using Elastos::Droid::JavaProxy::CIRingtonePlayer;
using Elastos::Droid::Os::IBinder;
using Elastos::Utility::Etl::HashMap;

static Mutex sICallBackMapLock;
static HashMap<Int32, AutoPtr<IBinder> > sICallBackMap;
static Mutex sIAudioFocusDispatcherLock;
static HashMap<Int32, AutoPtr<IIAudioFocusDispatcher> > sIAudioFocusDispatcher;
static Mutex sIRemoteControlClientLock;
static HashMap<Int32, AutoPtr<IIRemoteControlClient> > sIRemoteControlClient;
static Mutex sIRemoteControlDisplayLock;
static HashMap<Int32, AutoPtr<IIRemoteControlDisplay> > sIRemoteControlDisplay;

static void android_media_ElAudioServiceProxy_nativePlaySoundEffectVolume(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jeffectType, jfloat jvolume)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativePlaySoundEffectVolume()");

    IIAudioService* as = (IIAudioService*)jproxy;
    as->PlaySoundEffectVolume((Int32)jeffectType, (Float)jvolume);

    // ALOGD("- android_media_ElAudioServiceProxy_nativePlaySoundEffectVolume()");
}

static jint android_media_ElAudioServiceProxy_nativeGetMasterVolume(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetMasterVolume()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Int32 volume = 0;
    as->GetMasterVolume(&volume);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetMasterVolume()");
    return (jint)volume;
}

static jint android_media_ElAudioServiceProxy_nativeGetMasterMaxVolume(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetMasterMaxVolume()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Int32 maxVolume = 0;
    as->GetMasterMaxVolume(&maxVolume);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetMasterMaxVolume()");
    return (jint)maxVolume;
}

static jobject android_media_ElAudioServiceProxy_nativeGetRingtonePlayer(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetRingtonePlayer()");

    IIAudioService* as = (IIAudioService*)jproxy;
    AutoPtr<IIRingtonePlayer> player;
    as->GetRingtonePlayer((IIRingtonePlayer**)&player);
    jobject jplayer = NULL;
    if (player != NULL) {
        ALOGE("+ android_media_ElAudioServiceProxy_nativeGetRingtonePlayer() player not NULL!");
    }

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetRingtonePlayer()");
    return jplayer;
}

static jint android_media_ElAudioServiceProxy_nativeGetStreamVolume(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstreamType)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetStreamVolume()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Int32 volume = 0;;
    as->GetStreamVolume((Int32)jstreamType, &volume);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetStreamVolume()");
    return (jint)volume;
}

static jint android_media_ElAudioServiceProxy_nativeGetStreamMaxVolume(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstreamType)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetStreamMaxVolume()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Int32 maxVolume = 0;
    as->GetStreamMaxVolume((Int32)jstreamType, &maxVolume);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetStreamMaxVolume()");
    return (jint)maxVolume;
}

static jint android_media_ElAudioServiceProxy_nativeGetMode(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetMode()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Int32 mode = 0;
    as->GetMode(&mode);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetMode()");
    return (jint)mode;
}

static void android_media_ElAudioServiceProxy_nativeSetMode(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jmode, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetMode()");

    if (jcb == NULL) {
        ALOGE("ElAudioServiceProxy_nativeSetMode, jcb is NULL!");
    }

    AutoPtr<IBinder> cb;
    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jcb);
    CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);

    IIAudioService* as = (IIAudioService*)jproxy;
    as->SetMode((Int32)jmode, cb);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetMode()");
}

static void android_media_ElAudioServiceProxy_nativeSetStreamVolume(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jstreamType, jint jindex, jint jflags, jstring jcallingPackage)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetStreamVolume()");
    String callingPackage = ElUtil::ToElString(env, jcallingPackage);
    IIAudioService* as = (IIAudioService*)jproxy;
    as->SetStreamVolume((Int32)jstreamType, (Int32)jindex, (Int32)jflags, callingPackage);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetStreamVolume()");
}

static jboolean android_media_ElAudioServiceProxy_nativeIsSpeakerphoneOn(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeIsSpeakerphoneOn()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Boolean isOn = FALSE;
    as->IsSpeakerphoneOn(&isOn);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeIsSpeakerphoneOn()");
    return (jboolean)isOn;
}

static jint android_media_ElAudioServiceProxy_nativeGetRingerMode(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetRingerMode()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Int32 mode = 2 /*RINGER_MODE_NORMAL*/;
    as->GetRingerMode(&mode);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetRingerMode()");
    return (jint)mode;
}

static jboolean android_media_ElAudioServiceProxy_nativeLoadSoundEffects(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeLoadSoundEffects()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Boolean result = FALSE;
    as->LoadSoundEffects(&result);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeLoadSoundEffects()");
    return (jboolean)result;
}

static void android_media_ElAudioServiceProxy_nativeAdjustSuggestedStreamVolume(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdirection, jint jsuggestedStreamType, jint jflags, jstring jcallingPackage)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeAdjustSuggestedStreamVolume()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);
    IIAudioService* as = (IIAudioService*)jproxy;
    as->AdjustSuggestedStreamVolume((Int32)jdirection, (Int32)jsuggestedStreamType, (Int32)jflags, callingPackage);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeAdjustSuggestedStreamVolume()");
}

static void android_media_ElAudioServiceProxy_nativeAdjustStreamVolume(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstreamType, jint jdirection, jint jflags, jstring jcallingPackage)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeAdjustStreamVolume()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);
    IIAudioService* as = (IIAudioService*)jproxy;
    as->AdjustStreamVolume((Int32)jstreamType, (Int32)jdirection, (Int32)jflags, callingPackage);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeAdjustStreamVolume()");
}

static void android_media_ElAudioServiceProxy_nativeAdjustMasterVolume(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdirection, jint jflags, jstring jcallingPackage)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeAdjustMasterVolume()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);
    IIAudioService* as = (IIAudioService*)jproxy;
    as->AdjustMasterVolume((Int32)jdirection, (Int32)jflags, callingPackage);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeAdjustMasterVolume()");
}

static jint android_media_ElAudioServiceProxy_nativeRequestAudioFocus(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jmainStreamType, jint jdurationHint, jobject jcb, jobject jdispatcher, jstring jclientId, jstring jcallingPackageName)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeRequestAudioFocus() not finish");

    // Process IBinder
    AutoPtr<IBinder> cb;
    if (jcb == NULL) {
        ALOGE("ElAudioServiceProxy_nativeSetMode, jcb is NULL!");
        return 0; // AudioManager.AUDIOFOCUS_REQUEST_FAILED
    }

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jcb);
    CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);

    //Process IAudioFocusDispatcher
    AutoPtr<IIAudioFocusDispatcher> dispatcher;
    if(jdispatcher == NULL){
        ALOGE("ElAudioServiceProxy_nativeSetMode, jdispatcher is NULL!");
        return 0; // // AudioManager.AUDIOFOCUS_REQUEST_FAILED
    }

    {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jdispatcher);
        Mutex::Autolock lock(sIAudioFocusDispatcherLock);
        HashMap<Int32, AutoPtr<IIAudioFocusDispatcher> >::Iterator it = sIAudioFocusDispatcher.Find(hashCode);

        if(it != sIAudioFocusDispatcher.End()) {
            dispatcher = it->mSecond;
        } else{
            jobject jInstance = env->NewGlobalRef(jdispatcher);
            CIAudioFocusDispatcher::New((Handle64)jvm, (Handle64)jInstance, (IIAudioFocusDispatcher**)&dispatcher);
            sIAudioFocusDispatcher[hashCode] = dispatcher;
        }
    }

    String clientId = ElUtil::ToElString(env, jclientId);
    String callingPkgName = ElUtil::ToElString(env, jcallingPackageName);

    Int32 result;
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->RequestAudioFocus((Int32)jmainStreamType, jdurationHint, cb, dispatcher, clientId, callingPkgName, &result);
    // ALOGD("RequestAudioFocus ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeRequestAudioFocus()");
    return result;
}

static jint android_media_ElAudioServiceProxy_nativeAbandonAudioFocus(JNIEnv* env, jobject clazz, jlong jproxy, jobject jdispatcher, jstring jclientId)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeAbandonAudioFocus()");
    AutoPtr<IIAudioFocusDispatcher> dispatcher;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jdispatcher);

    {
        Mutex::Autolock lock(sIAudioFocusDispatcherLock);
        HashMap<Int32, AutoPtr<IIAudioFocusDispatcher> >::Iterator it = sIAudioFocusDispatcher.Find(hashCode);

        if(it != sIAudioFocusDispatcher.End()) {
            dispatcher = it->mSecond;
            sIAudioFocusDispatcher.Erase(it);
        }
    }

    Int32 result(0);
    if(dispatcher != NULL){
        String clientId = ElUtil::ToElString(env, jclientId);

        IIAudioService* as = (IIAudioService*)jproxy;
        ECode ec = as->AbandonAudioFocus(dispatcher, clientId, &result);
        // ALOGD("AbandonAudioFocus ec = %08x", ec);

    } else{
        ALOGE("android_media_ElAudioServiceProxy_nativeAbandonAudioFocus, find jdispatcher failed!");
    }

    // ALOGD("- android_media_ElAudioServiceProxy_nativeAbandonAudioFocus()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeSetRemoteStreamVolume(JNIEnv* env, jobject clazz, jlong jproxy, jint index)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetRemoteStreamVolume()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetRemoteStreamVolume((Int32)index);
    if (FAILED(ec))
        ALOGE("SetRemoteStreamVolume(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetRemoteStreamVolume()");
}

static void android_media_ElAudioServiceProxy_nativeSetMasterVolume(
    JNIEnv* env, jobject clazz, jlong jproxy, jint index, jint flags, jstring jcallingPackage)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetMasterVolume()");
    String callingPackage = ElUtil::ToElString(env, jcallingPackage);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetMasterVolume((Int32)index, (Int32)flags, callingPackage);
    if (FAILED(ec))
        ALOGE("SetMasterVolume(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetMasterVolume()");
}

static void android_media_ElAudioServiceProxy_nativeSetStreamSolo(JNIEnv* env, jobject clazz, jlong jproxy, jint streamType, jboolean state, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetStreamSolo()");

    AutoPtr<IBinder> cb = NULL;
    // Process IBinder
    if (jcb == NULL) {
        ALOGE("-android_media_ElAudioServiceProxy_nativeSetStreamSolo(), jcb is NULL!");
        return;
    }

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jcb);
    CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetStreamSolo((Int32)streamType, (Boolean)state, cb);
    if (FAILED(ec))
        ALOGE("SetStreamSolo(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetStreamSolo()");
}

static void android_media_ElAudioServiceProxy_nativeSetStreamMute(JNIEnv* env, jobject clazz, jlong jproxy, jint streamType, jboolean state, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetStreamMute()");

    AutoPtr<IBinder> cb = NULL;
    // Process IBinder
    if (jcb != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jcb);

        Mutex::Autolock lock(sICallBackMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sICallBackMap.Find(hashCode);

        if (it != sICallBackMap.End()) {
            cb = it->mSecond;
        }

        if (cb == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jcb);
            CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);
            // Warn: Here we stored the obj, but did not remove
            sICallBackMap[hashCode] = cb;
        }
    }

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetStreamMute((Int32)streamType, (Boolean)state, cb);
    if (FAILED(ec))
        ALOGE("SetStreamMute(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetStreamMute()");
}

static jboolean android_media_ElAudioServiceProxy_nativeIsStreamMute(JNIEnv* env, jobject clazz, jlong jproxy, jint streamType)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeIsStreamMute()");

    Boolean result(FALSE);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->IsStreamMute((Int32)streamType, &result);
    if (FAILED(ec))
        ALOGE("IsStreamMute(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeIsStreamMute()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeForceRemoteSubmixFullVolume(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean startForcing, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeForceRemoteSubmixFullVolume()");

    AutoPtr<IBinder> cb = NULL;
    // Process IBinder
    if (jcb == NULL) {
        ALOGE("- android_media_ElAudioServiceProxy_nativeForceRemoteSubmixFullVolume(), jcb is NULL!");
        return;
    }

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jcb);
    CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->ForceRemoteSubmixFullVolume((Boolean)startForcing, cb);
    if (FAILED(ec))
        ALOGE("ForceRemoteSubmixFullVolume(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeForceRemoteSubmixFullVolume()");
}

static void android_media_ElAudioServiceProxy_nativeSetMasterMute(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean state, jint flags, jstring jcallingPackage, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetMasterMute()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);
    AutoPtr<IBinder> cb = NULL;
    // Process IBinder
    if (jcb == NULL) {
        ALOGE("- android_media_ElAudioServiceProxy_nativeSetMasterMute(), jcb is NULL!");
        return;
    }

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jcb);
    CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetMasterMute((Boolean)state, (Int32)flags, callingPackage, cb);
    if (FAILED(ec))
        ALOGE("SetMasterMute(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetMasterMute()");
}

static jboolean android_media_ElAudioServiceProxy_nativeIsMasterMute(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeIsMasterMute()");

    Boolean result(FALSE);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->IsMasterMute(&result);
    if (FAILED(ec))
        ALOGE("IsMasterMute(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeIsMasterMute()");
    return result;
}

static jint android_media_ElAudioServiceProxy_nativeGetLastAudibleStreamVolume(JNIEnv* env, jobject clazz, jlong jproxy, jint streamType)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetLastAudibleStreamVolume()");

    Int32 result;
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->GetLastAudibleStreamVolume((Int32)streamType, &result);
    if (FAILED(ec))
        ALOGE("GetLastAudibleStreamVolume(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetLastAudibleStreamVolume()");
    return result;
}

static jint android_media_ElAudioServiceProxy_nativeGetLastAudibleMasterVolume(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetLastAudibleMasterVolume()");

    Int32 result;
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->GetLastAudibleMasterVolume(&result);
    if (FAILED(ec))
        ALOGE("GetLastAudibleMasterVolume(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetLastAudibleMasterVolume()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeSetMicrophoneMute(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean jon, jstring jcallingPackage)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetMicrophoneMute()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetMicrophoneMute(jon, callingPackage);
    if (FAILED(ec))
        ALOGE("SetMicrophoneMute(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetMicrophoneMute()");
}


static void android_media_ElAudioServiceProxy_nativeSetRingerMode(
    JNIEnv* env, jobject clazz, jlong jproxy, jint ringerMode, jboolean jcheckZen)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetRingerMode()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetRingerMode((Int32)ringerMode, jcheckZen);
    if (FAILED(ec))
        ALOGE("SetRingerMode(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetRingerMode()");
}

static void android_media_ElAudioServiceProxy_nativeSetVibrateSetting(JNIEnv* env, jobject clazz, jlong jproxy, jint vibrateType, jint vibrateSetting)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetVibrateSetting()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetVibrateSetting((Int32)vibrateType, (Int32)vibrateSetting);
    if (FAILED(ec))
        ALOGE("SetVibrateSetting(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetVibrateSetting()");
}

static jint android_media_ElAudioServiceProxy_nativeGetVibrateSetting(JNIEnv* env, jobject clazz, jlong jproxy, jint vibrateType)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetVibrateSetting()");

    Int32 result;
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->GetVibrateSetting((Int32)vibrateType, &result);
    if (FAILED(ec))
        ALOGE("GetVibrateSetting(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetVibrateSetting()");
    return result;
}

static jboolean android_media_ElAudioServiceProxy_nativeShouldVibrate(JNIEnv* env, jobject clazz, jlong jproxy, jint vibrateType)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeShouldVibrate()");

    Boolean result(FALSE);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->ShouldVibrate((Int32)vibrateType, &result);
    if (FAILED(ec))
        ALOGE("ShouldVibrate(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeShouldVibrate()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativePlaySoundEffect(JNIEnv* env, jobject clazz, jlong jproxy, jint effectType)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativePlaySoundEffect()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->PlaySoundEffect((Int32)effectType);
    if (FAILED(ec))
        ALOGE("PlaySoundEffect(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativePlaySoundEffect()");
}

static void android_media_ElAudioServiceProxy_nativeUnloadSoundEffects(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeUnloadSoundEffects()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->UnloadSoundEffects();
    if (FAILED(ec))
        ALOGE("UnloadSoundEffects(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeUnloadSoundEffects()");
}

static void android_media_ElAudioServiceProxy_nativeReloadAudioSettings(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeReloadAudioSettings()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->ReloadAudioSettings();
    if (FAILED(ec))
        ALOGE("ReloadAudioSettings(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeReloadAudioSettings()");
}

static void android_media_ElAudioServiceProxy_nativeAvrcpSupportsAbsoluteVolume(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jaddress, jboolean support)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeAvrcpSupportsAbsoluteVolume()");

    String address = ElUtil::ToElString(env, jaddress);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->AvrcpSupportsAbsoluteVolume(address, support);
    if (FAILED(ec))
        ALOGE("AvrcpSupportsAbsoluteVolume(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeAvrcpSupportsAbsoluteVolume()");
}

static void android_media_ElAudioServiceProxy_nativeSetSpeakerphoneOn(JNIEnv* env, jobject clazz, jlong jproxy, jboolean on)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetSpeakerphoneOn()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetSpeakerphoneOn((Boolean)on);
    if (FAILED(ec))
        ALOGE("SetSpeakerphoneOn(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetSpeakerphoneOn()");
}

static void android_media_ElAudioServiceProxy_nativeSetBluetoothScoOn(JNIEnv* env, jobject clazz, jlong jproxy, jboolean on)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetBluetoothScoOn()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetBluetoothScoOn((Boolean)on);
    if (FAILED(ec))
        ALOGE("SetBluetoothScoOn(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetBluetoothScoOn()");
}

static jboolean android_media_ElAudioServiceProxy_nativeIsBluetoothScoOn(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeIsBluetoothScoOn()");

    Boolean result(FALSE);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->IsBluetoothScoOn(&result);
    if (FAILED(ec))
        ALOGE("IsBluetoothScoOn(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeIsBluetoothScoOn()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeSetBluetoothA2dpOn(JNIEnv* env, jobject clazz, jlong jproxy, jboolean on)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetBluetoothA2dpOn()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetBluetoothA2dpOn((Boolean)on);
    if (FAILED(ec))
        ALOGE("SetBluetoothA2dpOn(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetBluetoothA2dpOn()");
}

static jboolean android_media_ElAudioServiceProxy_nativeIsBluetoothA2dpOn(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeIsBluetoothA2dpOn()");

    Boolean result(FALSE);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->IsBluetoothA2dpOn(&result);
    if (FAILED(ec))
        ALOGE("IsBluetoothA2dpOn(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeIsBluetoothA2dpOn()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeUnregisterAudioFocusClient(JNIEnv* env, jobject clazz, jlong jproxy, jstring jclientId)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeUnregisterAudioFocusClient()");

    String clientId = ElUtil::ToElString(env, jclientId);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->UnregisterAudioFocusClient(clientId);
    if (FAILED(ec))
        ALOGE("UnregisterAudioFocusClient(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeUnregisterAudioFocusClient()");
}

static jint android_media_ElAudioServiceProxy_nativeGetCurrentAudioFocus(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetCurrentAudioFocus()");
    IIAudioService* as = (IIAudioService*)jproxy;
    Int32 result;
    ECode ec = as->GetCurrentAudioFocus(&result);
    if (FAILED(ec))
        ALOGE("GetCurrentAudioFocus(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetCurrentAudioFocus()");
    return result;
}

static jboolean android_media_ElAudioServiceProxy_nativeRegisterRemoteControlDisplay(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrcd, jint w, jint h)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeRegisterRemoteControlDisplay()");

    AutoPtr<IIRemoteControlDisplay> rcd;

    if (jrcd != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jrcd);
        Mutex::Autolock lock(sIRemoteControlDisplayLock);
        HashMap<Int32, AutoPtr<IIRemoteControlDisplay> >::Iterator it = sIRemoteControlDisplay.Find(hashCode);

        if (it != sIRemoteControlDisplay.End()) {
            rcd = it->mSecond;
        }

        if (rcd == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jrcd);
            CIRemoteControlDisplay::New((Handle64)jvm, (Handle64)jInstance, (IIRemoteControlDisplay**)&rcd);
            sIRemoteControlDisplay[hashCode] = rcd;
        }
    } else {
        ALOGE("nativeRegisterRemoteControlDisplay(), jrcd is NULL!");
    }

    IIAudioService* as = (IIAudioService*)jproxy;
    Boolean result;
    ECode ec = as->RegisterRemoteControlDisplay(rcd, w, h, &result);
    if (FAILED(ec))
        ALOGE("RegisterRemoteControlDisplay(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeRegisterRemoteControlDisplay()");r
    return result;
}


static jboolean android_media_ElAudioServiceProxy_nativeRegisterRemoteController(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrcd, jint w, jint h, jobject jlistenerComp)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeRegisterRemoteController()");

    AutoPtr<IIRemoteControlDisplay> rcd;

    if (jrcd != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jrcd);

        Mutex::Autolock lock(sIRemoteControlDisplayLock);
        HashMap<Int32, AutoPtr<IIRemoteControlDisplay> >::Iterator it = sIRemoteControlDisplay.Find(hashCode);

        if (it != sIRemoteControlDisplay.End()) {
            rcd = it->mSecond;
        }

        if (rcd == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jrcd);
            CIRemoteControlDisplay::New((Handle64)jvm, (Handle64)jInstance, (IIRemoteControlDisplay**)&rcd);
            sIRemoteControlDisplay[hashCode] = rcd;
        }
    } else {
        ALOGE("nativeRegisterRemoteController(), jrcd is NULL!");
    }
    AutoPtr<IComponentName> listenerComp;
    if (jlistenerComp != NULL) {
        ElUtil::ToElComponentName(env, jlistenerComp, (IComponentName**)&listenerComp);
    }
    IIAudioService* as = (IIAudioService*)jproxy;
    Boolean result;
    ECode ec = as->RegisterRemoteController(rcd, w, h, listenerComp, &result);
    if (FAILED(ec))
        ALOGE("RegisterRemoteController(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeRegisterRemoteController()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeUnregisterRemoteControlDisplay(JNIEnv* env, jobject clazz, jlong jproxy, jobject jrcd)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeUnregisterRemoteControlDisplay()");

    if (jrcd == NULL) {
        ALOGE("nativeUnregisterRemoteControlDisplay(), jrcd is NULL!");
        return;
    }

    AutoPtr<IIRemoteControlDisplay> rcd;
    {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jrcd);
        Mutex::Autolock lock(sIRemoteControlDisplayLock);
        HashMap<Int32, AutoPtr<IIRemoteControlDisplay> >::Iterator it = sIRemoteControlDisplay.Find(hashCode);

        if (it != sIRemoteControlDisplay.End()) {
            rcd = it->mSecond;
            sIRemoteControlDisplay.Erase(it);
        }
    }

    if (rcd != NULL) {
        IIAudioService* as = (IIAudioService*)jproxy;
        ECode ec = as->UnregisterRemoteControlDisplay(rcd);
        ALOGE("UnregisterRemoteControlDisplay(), ec = %08x", ec);
    } else {
        ALOGE("nativeUnregisterRemoteControlDisplay, Did not find IIRemoteControlDisplay obj in cache!");
    }

    // ALOGD("- android_media_ElAudioServiceProxy_nativeUnregisterRemoteControlDisplay()");
}

static void android_media_ElAudioServiceProxy_nativeRemoteControlDisplayUsesBitmapSize(JNIEnv* env, jobject clazz, jlong jproxy, jobject jrcd, jint w, jint h)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeRemoteControlDisplayUsesBitmapSize()");

    AutoPtr<IIRemoteControlDisplay> rcd;

    if (jrcd != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jrcd);
        Mutex::Autolock lock(sIRemoteControlDisplayLock);
        HashMap<Int32, AutoPtr<IIRemoteControlDisplay> >::Iterator it = sIRemoteControlDisplay.Find(hashCode);

        if (it != sIRemoteControlDisplay.End()) {
            rcd = it->mSecond;
        }

        if (rcd == NULL) {
            ALOGE("nativeRemoteControlDisplayUsesBitmapSize, did not find IIRemoteControlDisplay obj in cache!");
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jrcd);
            CIRemoteControlDisplay::New((Handle64)jvm, (Handle64)jInstance, (IIRemoteControlDisplay**)&rcd);
            sIRemoteControlDisplay[hashCode] = rcd;
        }
    } else {
        ALOGE("nativeRemoteControlDisplayUsesBitmapSize(), jrcd is NULL!");
    }

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->RemoteControlDisplayUsesBitmapSize(rcd, (Int32)w, (Int32)h);
    if (FAILED(ec))
        ALOGE("RemoteControlDisplayUsesBitmapSize(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeRemoteControlDisplayUsesBitmapSize()");
}


static void android_media_ElAudioServiceProxy_nativeRemoteControlDisplayWantsPlaybackPositionSync(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrcd, jboolean jwantsSync)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeRemoteControlDisplayWantsPlaybackPositionSync()");

    AutoPtr<IIRemoteControlDisplay> rcd;

    if (jrcd != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jrcd);
        Mutex::Autolock lock(sIRemoteControlDisplayLock);
        HashMap<Int32, AutoPtr<IIRemoteControlDisplay> >::Iterator it = sIRemoteControlDisplay.Find(hashCode);

        if (it != sIRemoteControlDisplay.End()) {
            rcd = it->mSecond;
        }

        if (rcd == NULL) {
            ALOGE("nativeRemoteControlDisplayWantsPlaybackPositionSync, did not find IIRemoteControlDisplay obj in cache!");
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jrcd);
            CIRemoteControlDisplay::New((Handle64)jvm, (Handle64)jInstance, (IIRemoteControlDisplay**)&rcd);
            sIRemoteControlDisplay[hashCode] = rcd;
        }
    } else {
        ALOGE("nativeRemoteControlDisplayWantsPlaybackPositionSync(), jrcd is NULL!");
    }

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->RemoteControlDisplayWantsPlaybackPositionSync(rcd, jwantsSync);
    if (FAILED(ec))
        ALOGE("RemoteControlDisplayWantsPlaybackPositionSync(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeRemoteControlDisplayWantsPlaybackPositionSync()");
}

static void android_media_ElAudioServiceProxy_nativeStartBluetoothSco(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcb, jint jtargetSdkVersion)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeStartBluetoothSco()");

    AutoPtr<IBinder> cb;
    if (jcb != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jcb);

        Mutex::Autolock lock(sICallBackMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sICallBackMap.Find(hashCode);

        if (it != sICallBackMap.End()) {
            cb = it->mSecond;
        }

        if (cb == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);

            jobject jInstance = env->NewGlobalRef(jcb);
            CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);

            sICallBackMap[hashCode] = cb;
        }
    }

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->StartBluetoothSco(cb, jtargetSdkVersion);
    // ALOGD("StartBluetoothSco(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeStartBluetoothSco()");
}


static void android_media_ElAudioServiceProxy_nativeStartBluetoothScoVirtualCall(JNIEnv* env, jobject clazz, jlong jproxy, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeStartBluetoothScoVirtualCall()");

    AutoPtr<IBinder> cb;
    if (jcb != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jcb);

        Mutex::Autolock lock(sICallBackMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sICallBackMap.Find(hashCode);

        if (it != sICallBackMap.End()) {
            cb = it->mSecond;
        }

        if (cb == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);

            jobject jInstance = env->NewGlobalRef(jcb);
            CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);

            sICallBackMap[hashCode] = cb;
        }
    }

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->StartBluetoothScoVirtualCall(cb);
    // ALOGD("StartBluetoothScoVirtualCall(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeStartBluetoothScoVirtualCall()");
}

static void android_media_ElAudioServiceProxy_nativeStopBluetoothSco(JNIEnv* env, jobject clazz, jlong jproxy, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeStopBluetoothSco()");

    AutoPtr<IBinder> cb;
    if (jcb == NULL) {
        ALOGE("-android_media_ElAudioServiceProxy_nativeStopBluetoothSco() jcb is NULL!");
        return;
    }

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcb);

    {
        Mutex::Autolock lock(sICallBackMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sICallBackMap.Find(hashCode);
        if (it != sICallBackMap.End()) {
            cb = it->mSecond;
            sICallBackMap.Erase(it);
        }
    }

    if (cb == NULL) {
        ALOGE("nativeStopBluetoothSco(), did not find IBinder callback!");
        return;
    }

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->StopBluetoothSco(cb);
    // ALOGD("StopBluetoothSco(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeStopBluetoothSco()");
}

static void android_media_ElAudioServiceProxy_nativeForceVolumeControlStream(JNIEnv* env, jobject clazz, jlong jproxy, jint streamType, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeForceVolumeControlStream()");

    AutoPtr<IBinder> cb;
    if (jcb == NULL) {
        ALOGE("- android_media_ElAudioServiceProxy_nativeForceVolumeControlStream() jcb is NULL!");
        return;
    }

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jcb);
    CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->ForceVolumeControlStream((Int32)streamType, cb);
    // ALOGD("ForceVolumeControlStream(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeForceVolumeControlStream()");
}

static void android_media_ElAudioServiceProxy_nativeSetRingtonePlayer(JNIEnv* env, jobject clazz, jlong jproxy, jobject jplayer)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetRingtonePlayer()");

    AutoPtr<IIRingtonePlayer> player;
    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jplayer);
    CIRingtonePlayer::New((Handle64)jvm, (Handle64)jInstance, (IIRingtonePlayer**)&player);

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetRingtonePlayer(player);
    // ALOGD("SetRingtonePlayer(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetRingtonePlayer()");
}

static jint android_media_ElAudioServiceProxy_nativeGetMasterStreamType(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetMasterStreamType()");

    Int32 result;
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->GetMasterStreamType(&result);
    if (FAILED(ec))
        ALOGE("GetMasterStreamType(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetMasterStreamType()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeSetWiredDeviceConnectionState(JNIEnv* env, jobject clazz, jlong jproxy, jint device, jint state, jstring jname)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetWiredDeviceConnectionState()");

    String name = ElUtil::ToElString(env, jname);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetWiredDeviceConnectionState((Int32)device, (Int32)state, name);
    if (FAILED(ec))
        ALOGE("SetWiredDeviceConnectionState(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetWiredDeviceConnectionState()");
}

static jint android_media_ElAudioServiceProxy_nativeSetBluetoothA2dpDeviceConnectionState(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jdevice, jint state, jint jprofile)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetBluetoothA2dpDeviceConnectionState()");

    jclass klass = env->FindClass("android/bluetooth/BluetoothDevice");
    ElUtil::CheckErrorAndLog(env, "nativeSetBluetoothA2dpDeviceConnectionState Fail FindClass: BluetoothDevice : %d!\n", __LINE__);

    jfieldID stringField = env->GetFieldID(klass, "mAddress", "Ljava/lang/String;");
    ElUtil::CheckErrorAndLog(env, "nativeSetBluetoothA2dpDeviceConnectionState: Fail get string field id: mAddress. %d!\n", __LINE__);
    jstring stringValue = (jstring)env->GetObjectField(jdevice, stringField);
    ElUtil::CheckErrorAndLog(env, "nativeSetBluetoothA2dpDeviceConnectionState: Fail get string field: mAddress : %d!\n", __LINE__);
    String address = ElUtil::ToElString(env, stringValue);
    env->DeleteLocalRef(stringValue);
    env->DeleteLocalRef(klass);

    AutoPtr<IBluetoothDevice> device;
    CBluetoothDevice::New(address, (IBluetoothDevice**)&device);

    Int32 result;
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetBluetoothA2dpDeviceConnectionState(device, (Int32)state, jprofile, &result);
    if (FAILED(ec))
        ALOGE("SetBluetoothA2dpDeviceConnectionState(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetBluetoothA2dpDeviceConnectionState()");
    return result;
}

static jobject android_media_ElAudioServiceProxy_nativeStartWatchingRoutes(JNIEnv* env, jobject clazz, jlong jproxy, jobject jobserver)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeStartWatchingRoutes()");

    AutoPtr<IIAudioRoutesObserver> observer;
    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jobserver);
    CIAudioRoutesObserver::New((Handle64)jvm, (Handle64)jInstance, (IIAudioRoutesObserver**)&observer);

    AutoPtr<IAudioRoutesInfo> audioRoutesInfo;
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->StartWatchingRoutes(observer, (IAudioRoutesInfo**)&audioRoutesInfo);
    // ALOGD("StartWatchingRoutes(), ec = %08x", ec);

    jobject jaudioRoutesInfo = ElUtil::GetJavaAudioRoutesInfo(env, audioRoutesInfo);
    // ALOGD("- android_media_ElAudioServiceProxy_nativeStartWatchingRoutes()");
    return jaudioRoutesInfo;
}

static jboolean android_media_ElAudioServiceProxy_nativeIsCameraSoundForced(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeIsCameraSoundForced()");

    Boolean result(FALSE);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->IsCameraSoundForced(&result);
    if (FAILED(ec))
        ALOGE("IsCameraSoundForced(), ec = %08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeIsCameraSoundForced()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeSetVolumeController(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcontroller)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetVolumeController()");

    AutoPtr<IIVolumeController> controller;
    if (jcontroller != NULL) {
        ALOGE("TODO: CIVolumeControllerNative is not implemented!");
        assert(0);
        // JavaVM* jvm = NULL;
        // env->GetJavaVM(&jvm);
        // jobject jInstance = env->NewGlobalRef(jcontroller);
        // CIVolumeControllerNative::New((Handle64)jvm, (Handle64)jInstance, (IIVolumeController**)&controller);
    }
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetVolumeController(controller);
    if (FAILED(ec))
        ALOGE("SetVolumeController, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetVolumeController()");
}

static void android_media_ElAudioServiceProxy_nativeNotifyVolumeControllerVisible(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcontroller, jboolean visible)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeNotifyVolumeControllerVisible()");

    AutoPtr<IIVolumeController> controller;
    if (jcontroller != NULL) {
        ALOGE("TODO: CIVolumeControllerNative is not implemented!");
        assert(0);
        // JavaVM* jvm = NULL;
        // env->GetJavaVM(&jvm);
        // jobject jInstance = env->NewGlobalRef(jcontroller);
        // CIVolumeControllerNative::New((Handle64)jvm, (Handle64)jInstance, (IIVolumeController**)&controller);
    }
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->NotifyVolumeControllerVisible(controller, visible);
    if (FAILED(ec))
        ALOGE("NotifyVolumeControllerVisible, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeNotifyVolumeControllerVisible()");
}

static jboolean android_media_ElAudioServiceProxy_nativeIsStreamAffectedByRingerMode(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jstreamType)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeIsStreamAffectedByRingerMode()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Boolean result;
    ECode ec = as->IsStreamAffectedByRingerMode(jstreamType, &result);
    if (FAILED(ec))
        ALOGE("IsStreamAffectedByRingerMode, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeIsStreamAffectedByRingerMode()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeDisableSafeMediaVolume(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeDisableSafeMediaVolume()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->DisableSafeMediaVolume();
    if (FAILED(ec))
        ALOGE("DisableSafeMediaVolume, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeDisableSafeMediaVolume()");
}

static jint android_media_ElAudioServiceProxy_nativeSetHdmiSystemAudioSupported(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean jon)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetHdmiSystemAudioSupported()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Int32 result;
    ECode ec = as->SetHdmiSystemAudioSupported(jon, &result);
    if (FAILED(ec))
        ALOGE("SetHdmiSystemAudioSupported, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetHdmiSystemAudioSupported()");
    return result;
}

static jboolean android_media_ElAudioServiceProxy_nativeIsHdmiSystemAudioSupported(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeIsHdmiSystemAudioSupported()");

    IIAudioService* as = (IIAudioService*)jproxy;
    Boolean result;
    ECode ec = as->IsHdmiSystemAudioSupported(&result);
    if (FAILED(ec))
        ALOGE("IsHdmiSystemAudioSupported, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeIsHdmiSystemAudioSupported()");
    return result;
}

static jboolean android_media_ElAudioServiceProxy_nativeRegisterAudioPolicy(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jpolicyConfig, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeRegisterAudioPolicy()");

    AutoPtr<IAudioPolicyConfig> policyConfig;
    if (jpolicyConfig != NULL) {
        ElUtil::ToElAudioPolicyConfig(env, jpolicyConfig, (IAudioPolicyConfig**)&policyConfig);
    }
    AutoPtr<IBinder> cb;
    if (jcb != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jcb);

        Mutex::Autolock lock(sICallBackMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sICallBackMap.Find(hashCode);
        if (it != sICallBackMap.End()) {
            cb = it->mSecond;
        }
        else {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jcb);
            CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&cb);
            // Warn: Here we stored the obj, but did not remove
            sICallBackMap[hashCode] = cb;
        }
    }
    IIAudioService* as = (IIAudioService*)jproxy;
    Boolean result;
    ECode ec = as->RegisterAudioPolicy(policyConfig, cb, &result);
    if (FAILED(ec))
        ALOGE("RegisterAudioPolicy, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeRegisterAudioPolicy()");
    return result;
}

static void android_media_ElAudioServiceProxy_nativeUnregisterAudioPolicyAsync(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcb)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeUnregisterAudioPolicyAsync()");

    AutoPtr<IBinder> cb;
    if (jcb != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jcb);

        Mutex::Autolock lock(sICallBackMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sICallBackMap.Find(hashCode);
        if (it != sICallBackMap.End()) {
            cb = it->mSecond;
            sICallBackMap.Erase(it);
        }
    }
    assert(cb != NULL);

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->UnregisterAudioPolicyAsync(cb);
    if (FAILED(ec))
        ALOGE("UnregisterAudioPolicyAsync, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeUnregisterAudioPolicyAsync()");
}

static void android_media_ElAudioServiceProxy_nativeSetRemoteControlClientBrowsedPlayer(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetRemoteControlClientBrowsedPlayer()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetRemoteControlClientBrowsedPlayer();
    if (FAILED(ec))
        ALOGE("SetRemoteControlClientBrowsedPlayer, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetRemoteControlClientBrowsedPlayer()");
}

static void android_media_ElAudioServiceProxy_nativeGetRemoteControlClientNowPlayingEntries(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeGetRemoteControlClientNowPlayingEntries()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->GetRemoteControlClientNowPlayingEntries();
    if (FAILED(ec))
        ALOGE("GetRemoteControlClientNowPlayingEntries, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeGetRemoteControlClientNowPlayingEntries()");
}

static void android_media_ElAudioServiceProxy_nativeSetRemoteControlClientPlayItem(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong juid, jint jscope)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeSetRemoteControlClientPlayItem()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->SetRemoteControlClientPlayItem(juid, jscope);
    if (FAILED(ec))
        ALOGE("SetRemoteControlClientPlayItem, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeSetRemoteControlClientPlayItem()");
}

static void android_media_ElAudioServiceProxy_nativeUpdateRemoteControllerOnExistingMediaPlayers(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeUpdateRemoteControllerOnExistingMediaPlayers()");

    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->UpdateRemoteControllerOnExistingMediaPlayers();
    if (FAILED(ec))
        ALOGE("UpdateRemoteControllerOnExistingMediaPlayers, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeUpdateRemoteControllerOnExistingMediaPlayers()");
}

static void android_media_ElAudioServiceProxy_nativeAddMediaPlayerAndUpdateRemoteController(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeAddMediaPlayerAndUpdateRemoteController()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->AddMediaPlayerAndUpdateRemoteController(packageName);
    if (FAILED(ec))
        ALOGE("AddMediaPlayerAndUpdateRemoteController, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeAddMediaPlayerAndUpdateRemoteController()");
}

static void android_media_ElAudioServiceProxy_nativeRemoveMediaPlayerAndUpdateRemoteController(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeRemoveMediaPlayerAndUpdateRemoteController()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    IIAudioService* as = (IIAudioService*)jproxy;
    ECode ec = as->RemoveMediaPlayerAndUpdateRemoteController(packageName);
    if (FAILED(ec))
        ALOGE("RemoveMediaPlayerAndUpdateRemoteController, ec = 0x%08x", ec);

    // ALOGD("- android_media_ElAudioServiceProxy_nativeRemoveMediaPlayerAndUpdateRemoteController()");
}

static void android_media_ElAudioServiceProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_media_ElAudioServiceProxy_nativeDestroy()");

    IIAudioService* obj = (IIAudioService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_media_ElAudioServiceProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_media_ElAudioServiceProxy_nativeFinalize },
    { "nativePlaySoundEffectVolume",    "(JIF)V",
            (void*) android_media_ElAudioServiceProxy_nativePlaySoundEffectVolume },
    { "nativeGetMasterVolume",    "(J)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetMasterVolume },
    { "nativeGetMasterMaxVolume",    "(J)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetMasterMaxVolume },
    { "nativeGetRingtonePlayer",    "(J)Landroid/media/IRingtonePlayer;",
            (void*) android_media_ElAudioServiceProxy_nativeGetRingtonePlayer },
    { "nativeGetStreamVolume",    "(JI)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetStreamVolume },
    { "nativeGetStreamMaxVolume",    "(JI)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetStreamMaxVolume },
    { "nativeGetMode",    "(J)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetMode },
    { "nativeSetMode",    "(JILandroid/os/IBinder;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetMode },
    { "nativeSetStreamVolume",    "(JIIILjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetStreamVolume },
    { "nativeIsSpeakerphoneOn",    "(J)Z",
            (void*) android_media_ElAudioServiceProxy_nativeIsSpeakerphoneOn },
    { "nativeGetRingerMode",    "(J)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetRingerMode },
    { "nativeLoadSoundEffects",    "(J)Z",
            (void*) android_media_ElAudioServiceProxy_nativeLoadSoundEffects },
    { "nativeAdjustSuggestedStreamVolume",    "(JIIILjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeAdjustSuggestedStreamVolume },
    { "nativeAdjustStreamVolume",    "(JIIILjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeAdjustStreamVolume },
    { "nativeAdjustMasterVolume",    "(JIILjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeAdjustMasterVolume },
    { "nativeRequestAudioFocus",    "(JIILandroid/os/IBinder;Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;Ljava/lang/String;)I",
            (void*) android_media_ElAudioServiceProxy_nativeRequestAudioFocus },
    { "nativeAbandonAudioFocus",    "(JLandroid/media/IAudioFocusDispatcher;Ljava/lang/String;)I",
            (void*) android_media_ElAudioServiceProxy_nativeAbandonAudioFocus },
    { "nativeSetRemoteStreamVolume",    "(JI)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetRemoteStreamVolume },
    { "nativeSetMasterVolume",    "(JIILjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetMasterVolume },
    { "nativeSetStreamSolo",    "(JIZLandroid/os/IBinder;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetStreamSolo },
    { "nativeSetStreamMute",    "(JIZLandroid/os/IBinder;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetStreamMute },
    { "nativeIsStreamMute",    "(JI)Z",
            (void*) android_media_ElAudioServiceProxy_nativeIsStreamMute },
    { "nativeForceRemoteSubmixFullVolume",    "(JZLandroid/os/IBinder;)V",
            (void*) android_media_ElAudioServiceProxy_nativeForceRemoteSubmixFullVolume },
    { "nativeSetMasterMute",    "(JZILjava/lang/String;Landroid/os/IBinder;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetMasterMute },
    { "nativeIsMasterMute",    "(J)Z",
            (void*) android_media_ElAudioServiceProxy_nativeIsMasterMute },
    { "nativeGetLastAudibleStreamVolume",    "(JI)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetLastAudibleStreamVolume },
    { "nativeGetLastAudibleMasterVolume",    "(J)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetLastAudibleMasterVolume },
    { "nativeSetMicrophoneMute",    "(JZLjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetMicrophoneMute },
    { "nativeSetRingerMode",    "(JIZ)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetRingerMode },
    { "nativeSetVibrateSetting",    "(JII)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetVibrateSetting },
    { "nativeGetVibrateSetting",    "(JI)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetVibrateSetting },
    { "nativeShouldVibrate",    "(JI)Z",
            (void*) android_media_ElAudioServiceProxy_nativeShouldVibrate },
    { "nativePlaySoundEffect",    "(JI)V",
            (void*) android_media_ElAudioServiceProxy_nativePlaySoundEffect },
    { "nativeUnloadSoundEffects",    "(J)V",
            (void*) android_media_ElAudioServiceProxy_nativeUnloadSoundEffects },
    { "nativeReloadAudioSettings",    "(J)V",
            (void*) android_media_ElAudioServiceProxy_nativeReloadAudioSettings },
    { "nativeAvrcpSupportsAbsoluteVolume",    "(JLjava/lang/String;Z)V",
            (void*) android_media_ElAudioServiceProxy_nativeAvrcpSupportsAbsoluteVolume },
    { "nativeSetSpeakerphoneOn",    "(JZ)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetSpeakerphoneOn },
    { "nativeSetBluetoothScoOn",    "(JZ)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetBluetoothScoOn },
    { "nativeIsBluetoothScoOn",    "(J)Z",
            (void*) android_media_ElAudioServiceProxy_nativeIsBluetoothScoOn },
    { "nativeSetBluetoothA2dpOn",    "(JZ)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetBluetoothA2dpOn },
    { "nativeIsBluetoothA2dpOn",    "(J)Z",
            (void*) android_media_ElAudioServiceProxy_nativeIsBluetoothA2dpOn },
    { "nativeUnregisterAudioFocusClient",    "(JLjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeUnregisterAudioFocusClient },
    { "nativeGetCurrentAudioFocus",    "(J)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetCurrentAudioFocus },
    { "nativeRegisterRemoteControlDisplay",    "(JLandroid/media/IRemoteControlDisplay;II)Z",
            (void*) android_media_ElAudioServiceProxy_nativeRegisterRemoteControlDisplay },
    { "nativeRegisterRemoteController",    "(JLandroid/media/IRemoteControlDisplay;IILandroid/content/ComponentName;)Z",
            (void*) android_media_ElAudioServiceProxy_nativeRegisterRemoteControlDisplay },
    { "nativeUnregisterRemoteControlDisplay",    "(JLandroid/media/IRemoteControlDisplay;)V",
            (void*) android_media_ElAudioServiceProxy_nativeUnregisterRemoteControlDisplay },
    { "nativeRemoteControlDisplayUsesBitmapSize",    "(JLandroid/media/IRemoteControlDisplay;II)V",
            (void*) android_media_ElAudioServiceProxy_nativeRemoteControlDisplayUsesBitmapSize },
    { "nativeRemoteControlDisplayWantsPlaybackPositionSync",    "(JLandroid/media/IRemoteControlDisplay;Z)V",
            (void*) android_media_ElAudioServiceProxy_nativeRemoteControlDisplayWantsPlaybackPositionSync },
    { "nativeStartBluetoothSco",    "(JLandroid/os/IBinder;I)V",
            (void*) android_media_ElAudioServiceProxy_nativeStartBluetoothSco },
    { "nativeStartBluetoothScoVirtualCall",    "(JLandroid/os/IBinder;)V",
            (void*) android_media_ElAudioServiceProxy_nativeStartBluetoothScoVirtualCall },
    { "nativeStopBluetoothSco",    "(JLandroid/os/IBinder;)V",
            (void*) android_media_ElAudioServiceProxy_nativeStopBluetoothSco },
    { "nativeForceVolumeControlStream",    "(JILandroid/os/IBinder;)V",
            (void*) android_media_ElAudioServiceProxy_nativeForceVolumeControlStream },
    { "nativeSetRingtonePlayer",    "(JLandroid/media/IRingtonePlayer;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetRingtonePlayer },
    { "nativeGetMasterStreamType",    "(J)I",
            (void*) android_media_ElAudioServiceProxy_nativeGetMasterStreamType },
    { "nativeSetWiredDeviceConnectionState",    "(JIILjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetWiredDeviceConnectionState },
    { "nativeSetBluetoothA2dpDeviceConnectionState",    "(JLandroid/bluetooth/BluetoothDevice;II)I",
            (void*) android_media_ElAudioServiceProxy_nativeSetBluetoothA2dpDeviceConnectionState },
    { "nativeStartWatchingRoutes",    "(JLandroid/media/IAudioRoutesObserver;)Landroid/media/AudioRoutesInfo;",
            (void*) android_media_ElAudioServiceProxy_nativeStartWatchingRoutes },
    { "nativeIsCameraSoundForced",    "(J)Z",
            (void*) android_media_ElAudioServiceProxy_nativeIsCameraSoundForced },
    { "nativeSetVolumeController",    "(JLandroid/media/IVolumeController;)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetVolumeController },
    { "nativeNotifyVolumeControllerVisible",    "(JLandroid/media/IVolumeController;Z)V",
            (void*) android_media_ElAudioServiceProxy_nativeNotifyVolumeControllerVisible },
    { "nativeIsStreamAffectedByRingerMode",    "(JI)Z",
            (void*) android_media_ElAudioServiceProxy_nativeIsStreamAffectedByRingerMode },
    { "nativeDisableSafeMediaVolume",    "(J)V",
            (void*) android_media_ElAudioServiceProxy_nativeDisableSafeMediaVolume },
    { "nativeSetHdmiSystemAudioSupported",    "(JZ)I",
            (void*) android_media_ElAudioServiceProxy_nativeSetHdmiSystemAudioSupported },
    { "nativeIsHdmiSystemAudioSupported",    "(J)Z",
            (void*) android_media_ElAudioServiceProxy_nativeIsHdmiSystemAudioSupported },
    { "nativeRegisterAudioPolicy",    "(JLandroid/media/audiopolicy/AudioPolicyConfig;Landroid/os/IBinder;)Z",
            (void*) android_media_ElAudioServiceProxy_nativeRegisterAudioPolicy },
    { "nativeUnregisterAudioPolicyAsync",    "(JLandroid/os/IBinder;)V",
            (void*) android_media_ElAudioServiceProxy_nativeUnregisterAudioPolicyAsync },
    { "nativeSetRemoteControlClientBrowsedPlayer",    "(J)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetRemoteControlClientBrowsedPlayer },
    { "nativeGetRemoteControlClientNowPlayingEntries",    "(J)V",
            (void*) android_media_ElAudioServiceProxy_nativeGetRemoteControlClientNowPlayingEntries },
    { "nativeSetRemoteControlClientPlayItem",    "(JJI)V",
            (void*) android_media_ElAudioServiceProxy_nativeSetRemoteControlClientPlayItem },
    { "nativeUpdateRemoteControllerOnExistingMediaPlayers",    "(J)V",
            (void*) android_media_ElAudioServiceProxy_nativeUpdateRemoteControllerOnExistingMediaPlayers },
    { "nativeAddMediaPlayerAndUpdateRemoteController",    "(JLjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeAddMediaPlayerAndUpdateRemoteController },
    { "nativeRemoveMediaPlayerAndUpdateRemoteController",    "(JLjava/lang/String;)V",
            (void*) android_media_ElAudioServiceProxy_nativeRemoveMediaPlayerAndUpdateRemoteController }
};

int register_android_media_ElAudioServiceProxy(JNIEnv *env) {
    return AndroidRuntime::registerNativeMethods(env, "android/media/ElAudioServiceProxy",
        		gMethods, NELEM(gMethods));
}

}; // namespace android
