
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <elastos/utility/etl/HashMap.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Content::IIClipboard;
using Elastos::Droid::Content::IClipData;
using Elastos::Droid::Content::IClipDescription;
using Elastos::Droid::Content::IOnPrimaryClipChangedListener;
using Elastos::Droid::JavaProxy::CIOnPrimaryClipChangedListenerNative;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, IOnPrimaryClipChangedListener*> sCCListener;
static Mutex sCCListenerLock;

static jboolean android_content_ElClipboardProxy_nativeHasPrimaryClip(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage)
{
    // ALOGD("+ android_content_ElClipboardProxy_nativeHasPrimaryClip()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIClipboard* cb = (IIClipboard*)jproxy;
    Boolean result = FALSE;
    cb->HasPrimaryClip(callingPackage, &result);

    // ALOGD("- android_content_ElClipboardProxy_nativeHasPrimaryClip()");
    return (jboolean)result;
}

static jboolean android_content_ElClipboardProxy_nativeHasClipboardText(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage)
{
    // ALOGD("+ android_content_ElClipboardProxy_nativeHasClipboardText()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIClipboard* cb = (IIClipboard*)jproxy;
    Boolean result = FALSE;
    ECode ec = cb->HasClipboardText(callingPackage, &result);
    ALOGE("nativeHasClipboardText ec: 0x%08x result: %d", ec, result);

    // ALOGD("- android_content_ElClipboardProxy_nativeHasClipboardText()");
    return (jboolean)result;
}

static void android_content_ElClipboardProxy_nativeSetPrimaryClip(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclip, jstring jcallingPackage)
{
    // ALOGD("+ android_content_ElClipboardProxy_nativeSetPrimaryClip()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<IClipData> clip;
    if (jclip != NULL) {
        if (!ElUtil::ToElClipData(env, jclip, (IClipData**)&clip)) {
            ALOGE("nativeSetPrimaryClip() ToElClipData fail!");
        }
    }

    IIClipboard* cb = (IIClipboard*)jproxy;
    cb->SetPrimaryClip(clip, callingPackage);

    // ALOGD("- android_content_ElClipboardProxy_nativeSetPrimaryClip()");
}

static jobject android_content_ElClipboardProxy_nativeGetPrimaryClip(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpkg)
{
    // ALOGD("+ android_content_ElClipboardProxy_nativeGetPrimaryClip()");

    String pkg = ElUtil::ToElString(env, jpkg);

    IIClipboard* cb = (IIClipboard*)jproxy;
    AutoPtr<IClipData> clip;
    cb->GetPrimaryClip(pkg, (IClipData**)&clip);
    jobject jclip = NULL;
    if (clip != NULL) {
        jclip = ElUtil::GetJavaClipData(env, clip);
    }

    // ALOGD("- android_content_ElClipboardProxy_nativeGetPrimaryClip()");
    return jclip;
}

static jobject android_content_ElClipboardProxy_nativeGetPrimaryClipDescription(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage)
{
    // ALOGD("+ android_content_ElClipboardProxy_nativeGetPrimaryClipDescription()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIClipboard* cb = (IIClipboard*)jproxy;
    AutoPtr<IClipDescription> clipDesc;
    cb->GetPrimaryClipDescription(callingPackage, (IClipDescription**)&clipDesc);
    jobject jclipDesc = NULL;
    if (clipDesc != NULL) {
        jclipDesc = ElUtil::GetJavaClipDescription(env, clipDesc);
    }

    // ALOGD("- android_content_ElClipboardProxy_nativeGetPrimaryClipDescription()");
    return jclipDesc;
}

static void android_content_ElClipboardProxy_nativeAddPrimaryClipChangedListener(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlistener, jstring jcallingPackage)
{
    // ALOGD("+ android_content_ElClipboardProxy_nativeAddPrimaryClipChangedListener()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);

    AutoPtr<IOnPrimaryClipChangedListener> listener;
    if (jlistener != NULL) {
        jobject jInstance = env->NewGlobalRef(jlistener);
        CIOnPrimaryClipChangedListenerNative::New((Handle64)jvm, (Handle64)jInstance, (IOnPrimaryClipChangedListener**)&listener);
        listener->AddRef();

        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
        Mutex::Autolock lock(sCCListenerLock);
        sCCListener[hashCode] = listener;
    }

    IIClipboard* cb = (IIClipboard*)jproxy;
    cb->AddPrimaryClipChangedListener(listener, callingPackage);

    // ALOGD("- android_content_ElClipboardProxy_nativeAddPrimaryClipChangedListener()");
}

static void android_content_ElClipboardProxy_nativeRemovePrimaryClipChangedListener(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlistener)
{
    // ALOGD("+ android_content_ElClipboardProxy_nativeRemovePrimaryClipChangedListener()");

    AutoPtr<IOnPrimaryClipChangedListener> listener;

    {
        Mutex::Autolock lock(sCCListenerLock);
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
        if (sCCListener.Find(hashCode) != sCCListener.End()) {
            listener = sCCListener[hashCode];
            sCCListener.Erase(hashCode);
        }
    }

    if (NULL == listener) {
        ALOGE("nativeRemovePrimaryClipChangedListener() Invalid IOnPrimaryClipChangedListener!\n");
        env->ExceptionDescribe();
    }

    IIClipboard* cb = (IIClipboard*)jproxy;
    cb->RemovePrimaryClipChangedListener(listener);

    listener->Release();

    // ALOGD("- android_content_ElClipboardProxy_nativeRemovePrimaryClipChangedListener()");
}

static void android_content_ElClipboardProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_content_ElClipboardProxy_nativeDestroy()");

    IIClipboard* obj = (IIClipboard*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_content_ElClipboardProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_content_ElClipboardProxy_nativeFinalize },
    { "nativeHasPrimaryClip",    "(JLjava/lang/String;)Z",
            (void*) android_content_ElClipboardProxy_nativeHasPrimaryClip },
    { "nativeHasClipboardText",    "(JLjava/lang/String;)Z",
            (void*) android_content_ElClipboardProxy_nativeHasClipboardText },
    { "nativeSetPrimaryClip",    "(JLandroid/content/ClipData;Ljava/lang/String;)V",
            (void*) android_content_ElClipboardProxy_nativeSetPrimaryClip },
    { "nativeGetPrimaryClip",    "(JLjava/lang/String;)Landroid/content/ClipData;",
            (void*) android_content_ElClipboardProxy_nativeGetPrimaryClip },
    { "nativeGetPrimaryClipDescription",    "(JLjava/lang/String;)Landroid/content/ClipDescription;",
            (void*) android_content_ElClipboardProxy_nativeGetPrimaryClipDescription },
    { "nativeAddPrimaryClipChangedListener",    "(JLandroid/content/IOnPrimaryClipChangedListener;Ljava/lang/String;)V",
            (void*) android_content_ElClipboardProxy_nativeAddPrimaryClipChangedListener },
    { "nativeRemovePrimaryClipChangedListener",    "(JLandroid/content/IOnPrimaryClipChangedListener;)V",
            (void*) android_content_ElClipboardProxy_nativeRemovePrimaryClipChangedListener },
};

int register_android_content_ElClipboardProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/content/ElClipboardProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

