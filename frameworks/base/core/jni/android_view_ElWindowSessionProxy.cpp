
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include "android_view_InputChannel.h"
#include <elastos/droid/view/NativeInputChannel.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Graphics.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.View.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <elastos/utility/etl/HashMap.h>
#include <utils/Log.h>
#include <utils/Mutex.h>
#include <gui/Surface.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::JavaProxy::CWindowNative;
using Elastos::Droid::View::CInputChannel;
using Elastos::Droid::View::IIWindow;
using Elastos::Droid::View::IWindowSession;
using Elastos::Droid::View::IInputChannel;
using Elastos::Droid::View::IWindowManagerLayoutParams;
using Elastos::Droid::View::CSurface;;
using Elastos::Droid::Graphics::IRect;
using Elastos::Droid::Graphics::IRegion;
using Elastos::Droid::Graphics::CRect;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Os::EIID_IBinder;
using Elastos::Utility::Etl::HashMap;

namespace android {

class NativeInputChannel {
public:
    NativeInputChannel(const sp<InputChannel>& inputChannel);
    ~NativeInputChannel();

    inline sp<InputChannel> getInputChannel() { return mInputChannel; }

    void setDisposeCallback(InputChannelObjDisposeCallback callback, void* data);
    void invokeAndRemoveDisposeCallback(JNIEnv* env, jobject obj);

private:
    sp<InputChannel> mInputChannel;
    InputChannelObjDisposeCallback mDisposeCallback;
    void* mDisposeData;
};

} // namespace android


static void setJavaRect(JNIEnv* env, IRect* elRect, jobject jrect)
{
    if (elRect == NULL || jrect == NULL) {
        ALOGE("setJavaRect Invalid arguments!");
        return;
    }

    jclass rectKlass = env->FindClass("android/graphics/Rect");
    ElUtil::CheckErrorAndLog(env, "nativeRelayout Fail FindClass: Rect : %d!\n", __LINE__);

    jfieldID f = env->GetFieldID(rectKlass, "left", "I");
    ElUtil::CheckErrorAndLog(env, "nativeRelayout Fail GetFieldID: left : %d!\n", __LINE__);

    Int32 tmpInt;
    elRect->GetLeft(&tmpInt);
    env->SetIntField(jrect, f, tmpInt);
    ElUtil::CheckErrorAndLog(env, "nativeRelayout Fail SetIntField: left : %d!\n", __LINE__);

    f = env->GetFieldID(rectKlass, "top", "I");
    ElUtil::CheckErrorAndLog(env, "nativeRelayout Fail GetFieldID: top : %d!\n", __LINE__);

    elRect->GetTop(&tmpInt);
    env->SetIntField(jrect, f, tmpInt);
    ElUtil::CheckErrorAndLog(env, "nativeRelayout Fail SetIntField: top : %d!\n", __LINE__);

    f = env->GetFieldID(rectKlass, "right", "I");
    ElUtil::CheckErrorAndLog(env, "nativeRelayout Fail GetFieldID: right : %d!\n", __LINE__);

    elRect->GetRight(&tmpInt);
    env->SetIntField(jrect, f, tmpInt);
    ElUtil::CheckErrorAndLog(env, "nativeRelayout Fail SetIntField: right : %d!\n", __LINE__);

    f = env->GetFieldID(rectKlass, "bottom", "I");
    ElUtil::CheckErrorAndLog(env, "nativeRelayout Fail GetFieldID: bottom : %d!\n", __LINE__);

    elRect->GetBottom(&tmpInt);
    env->SetIntField(jrect, f, tmpInt);
    ElUtil::CheckErrorAndLog(env, "nativeRelayout Fail SetIntField: bottom : %d!\n", __LINE__);

    env->DeleteLocalRef(rectKlass);
}

HashMap<Int32, AutoPtr<IIWindow> > sWindowMap;
Mutex sWindowMapLock;

static jint android_view_ElWindowSessionProxy_nativeAddToDisplay(JNIEnv* env, jobject clazz, jlong jproxy, jobject jwindow, jint jwindowHashCode, jint jseq,
                jobject jattrs, jint jviewVisibility, jint jdisplayId, jobject joutContentInsets, jobject joutInputChannel)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeAddToDisplay()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jwindow);

    AutoPtr<IIWindow> window;
    ECode ec = CWindowNative::New((Handle64)jvm, (Handle64)jInstance, (IIWindow**)&window);
    if (FAILED(ec)) {
        ALOGE("android_view_ElWindowSessionProxy_nativeAddToDisplay() new CWindowNative fail 0x%x ", ec);
    }
    {
        Mutex::Autolock lock(sWindowMapLock);
        sWindowMap[jwindowHashCode] = window;
    }

    AutoPtr<IWindowManagerLayoutParams> attrs;
    if(jattrs != NULL) {
        if(!ElUtil::ToElWindowManagerLayoutParams(env, jattrs, (IWindowManagerLayoutParams**)&attrs)) {
            ALOGE("nativeAddToDisplay: ToElWindowManagerLayoutParams fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IRect> contentInsets;
    if(!ElUtil::ToElRect(env, joutContentInsets, (IRect**)&contentInsets)) {
        ALOGE("nativeAddToDisplay: ToElRect fail : %d!\n", __LINE__);
    }
    AutoPtr<IInputChannel> inputChannel;
    CInputChannel::New((IInputChannel**)&inputChannel);

    AutoPtr<IRect> outContentInsets;
    AutoPtr<IInputChannel> outInputChannel;
    Int32 result;

    IWindowSession* session = (IWindowSession*)jproxy;
    ec = session->AddToDisplay(window, (Int32)jseq, attrs, (Int32)jviewVisibility, (Int32)jdisplayId,
                contentInsets, inputChannel, (IRect**)&outContentInsets, (IInputChannel**)&outInputChannel, &result);
    // ALOGD("android_view_ElWindowSessionProxy_nativeAddToDisplay() AddToDisplay ec:%d result=%d ", ec, result);

    if(outContentInsets != NULL) {
        jobject jcontentInsets = ElUtil::GetJavaRect(env, outContentInsets);
        if (jcontentInsets != NULL) {
            jclass rectKlass = env->FindClass("android/graphics/Rect");
            ElUtil::CheckErrorAndLog(env, "nativeAddToDisplay Fail FindClass: Rect : %d!\n", __LINE__);

            Int32 tmpInt;
            outContentInsets->GetLeft(&tmpInt);
            jfieldID f = env->GetFieldID(rectKlass, "left", "I");
            ElUtil::CheckErrorAndLog(env, "nativeAddToDisplay Fail GetFieldID: mPtr : %d!\n", __LINE__);

            env->SetIntField(joutContentInsets, f, (jint)tmpInt);
            ElUtil::CheckErrorAndLog(env, "nativeAddToDisplay Fail SetIntField: left : %d!\n", __LINE__);

            outContentInsets->GetTop(&tmpInt);
            f = env->GetFieldID(rectKlass, "top", "I");
            ElUtil::CheckErrorAndLog(env, "nativeAddToDisplay Fail GetFieldID: top : %d!\n", __LINE__);

            env->SetIntField(joutContentInsets, f, (jint)tmpInt);
            ElUtil::CheckErrorAndLog(env, "nativeAddToDisplay Fail SetIntField: top : %d!\n", __LINE__);

            outContentInsets->GetRight(&tmpInt);
            f = env->GetFieldID(rectKlass, "right", "I");
            ElUtil::CheckErrorAndLog(env, "nativeAddToDisplay Fail GetFieldID: right : %d!\n", __LINE__);

            env->SetIntField(joutContentInsets, f, (jint)tmpInt);
            ElUtil::CheckErrorAndLog(env, "nativeAddToDisplay Fail SetIntField: right : %d!\n", __LINE__);

            outContentInsets->GetBottom(&tmpInt);
            f = env->GetFieldID(rectKlass, "bottom", "I");
            ElUtil::CheckErrorAndLog(env, "nativeAddToDisplay Fail GetFieldID: bottom : %d!\n", __LINE__);

            env->SetIntField(joutContentInsets, f, (jint)tmpInt);
            ElUtil::CheckErrorAndLog(env, "nativeAddToDisplay Fail SetIntField: bottom : %d!\n", __LINE__);

            env->DeleteLocalRef(rectKlass);
            env->DeleteLocalRef(jcontentInsets);
        } else {
            ALOGE("android_view_ElWindowSessionProxy_nativeAddToDisplay() jcontentInsets is NULL ");
        }
    }

    if(outInputChannel != NULL) {
        android::NativeInputChannel* nativeInputChannel = NULL;
        Handle64 elInputChannel;
        outInputChannel->GetNativeInputChannel(&elInputChannel);
        android::sp<android::InputChannel> inputChannel =
            reinterpret_cast<Elastos::Droid::View::NativeInputChannel*>(elInputChannel)->getInputChannel();
        nativeInputChannel = new android::NativeInputChannel(inputChannel);

        jclass inChannelKlass = env->FindClass("android/view/InputChannel");
        ElUtil::CheckErrorAndLog(env, "GetJavaInputChannel Fail FindClass: InputChannel : %d!\n", __LINE__);

        jfieldID f = env->GetFieldID(inChannelKlass, "mPtr", "J");
        ElUtil::CheckErrorAndLog(env, "GetJavaInputChannel Fail GetFieldID: mPtr : %d!\n", __LINE__);
        env->DeleteLocalRef(inChannelKlass);

        env->SetIntField(joutInputChannel, f, (jlong)nativeInputChannel);
        ElUtil::CheckErrorAndLog(env, "GetJavaInputChannel Fail SetIntField: mPtr : %d!\n", __LINE__);
    }

    // ALOGD("- android_view_ElWindowSessionProxy_nativeAddToDisplay()");
    return result;
}

static jint android_view_ElWindowSessionProxy_nativeRelayout(JNIEnv* env, jobject clazz, jlong jproxy, jint jwindow,
        jint jseq, jobject jattrs, jint jrequestedWidth, jint jrequestedHeight, jint jviewFlags, jint jflags, jobject joutFrame,
        jobject joutOverscanInsets, jobject joutContentInsets, jobject joutVisibleInsets, jobject joutStableInsets,
        jobject joutConfig, jobject joutSurface)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeRelayout()");

    // ALOGD("==== jseq: %d, jrequestedWidth: %d, jrequestedHeight: %d, jviewFlags: %d, jflags: %d, jattrs = 0x%08x ====",
        // jseq, jrequestedWidth, jrequestedHeight, jviewFlags, jflags, jattrs);

    IWindowSession* session = (IWindowSession*)jproxy;

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
        }
    }
    if (NULL == window) {
        ALOGE("nativeRelayout() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IRect> inFrame;
    if (joutFrame != NULL) {
        if(!ElUtil::ToElRect(env, joutFrame, (IRect**)&inFrame)) {
            ALOGE("nativeRelayout: ToElRect fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IRect> inOverscanInsets;
    if (joutOverscanInsets != NULL) {
        if(!ElUtil::ToElRect(env, joutOverscanInsets, (IRect**)&inOverscanInsets)) {
            ALOGE("nativeRelayout: ToElRect fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IRect> inContentInsets;
    if (joutFrame != NULL) {
        if(!ElUtil::ToElRect(env, joutFrame, (IRect**)&inContentInsets)) {
            ALOGE("nativeRelayout: ToElRect fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IRect> inVisibleInsets;
    if (joutFrame != NULL) {
        if(!ElUtil::ToElRect(env, joutFrame, (IRect**)&inVisibleInsets)) {
            ALOGE("nativeRelayout: ToElRect fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IRect> inStableInsets;
    if (joutStableInsets != NULL) {
        if(!ElUtil::ToElRect(env, joutStableInsets, (IRect**)&inStableInsets)) {
            ALOGE("nativeRelayout: ToElRect fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IWindowManagerLayoutParams> attrs;
    if (jattrs != NULL) {
        if(!ElUtil::ToElWindowManagerLayoutParams(env, jattrs, (IWindowManagerLayoutParams**)&attrs)) {
            ALOGE("nativeRelayout: ToElWindowManagerLayoutParams fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IConfiguration> inConfig;
    if (joutConfig != NULL) {
        if(!ElUtil::ToElConfiguration(env, joutConfig, (IConfiguration**)&inConfig)) {
            ALOGE("nativeRelayout: ToElConfiguration fail : %d!\n", __LINE__);
        }
    }

    jclass surfaceKlass = env->FindClass("android/view/Surface");
    ElUtil::CheckErrorAndLog(env, "FindClass: android/os/Bundle : %d!\n", __LINE__);

    jfieldID fNativeSurface = env->GetFieldID(surfaceKlass, "mNativeObject", "J");
    ElUtil::CheckErrorAndLog(env, "GetFieldID: mNativeObject : %d!\n", __LINE__);

    jlong jNativeSurface = env->GetLongField(joutSurface, fNativeSurface);
    ElUtil::CheckErrorAndLog(env, "GetLongField: mNativeObject : %d!\n", __LINE__);

    AutoPtr<ISurface> inSurface;
    if(NOERROR != CSurface::New(jNativeSurface, (ISurface**)&inSurface)) {
        ALOGE("nativeRelayout() new CSurface fail!");
    }

    AutoPtr<IRect> outFrame;
    AutoPtr<IRect> outOverscanInsets;
    AutoPtr<IRect> outContentInsets;
    AutoPtr<IRect> outVisibleInsets;
    AutoPtr<IRect> outStableInsets;
    AutoPtr<IConfiguration> outConfig;
    AutoPtr<ISurface> outSurface;

    Int32 result = 0;
    session->Relayout(window, (Int32)jseq, attrs, (Int32)jrequestedWidth, (Int32)jrequestedHeight, (Int32)jviewFlags, (Int32)jflags,
        inFrame, inOverscanInsets, inContentInsets, inVisibleInsets, inStableInsets, inConfig, inSurface, (IRect**)&outFrame, (IRect**)&outOverscanInsets,
        (IRect**)&outContentInsets, (IRect**)&outVisibleInsets, (IRect**)&outStableInsets, (IConfiguration**)&outConfig, &result, (ISurface**)&outSurface);

    ElUtil::SetJavaRect(env, joutFrame, outFrame);
    ElUtil::SetJavaRect(env, joutOverscanInsets, outOverscanInsets);
    ElUtil::SetJavaRect(env, joutContentInsets, outContentInsets);
    ElUtil::SetJavaRect(env, joutVisibleInsets, outVisibleInsets);
    ElUtil::SetJavaRect(env, joutStableInsets, outStableInsets);

    if (outConfig != NULL) {
        ElUtil::SetJavaConfiguration(env, outConfig, joutConfig);
    }

    if (outSurface != NULL) {
        Int64 nativeSurface;
        outSurface->GetNativeSurface(&nativeSurface);
        android::Surface* surface = reinterpret_cast<android::Surface*>(nativeSurface);
        if (surface != NULL) {
            surface->incStrong(joutSurface);
        }

        outSurface->ReleaseResources();

        if (jNativeSurface != 0) {
            reinterpret_cast<android::Surface*>(jNativeSurface)->decStrong(joutSurface);
        }

        jmethodID mid = env->GetMethodID(surfaceKlass, "setNativeObjectLocked", "(J)V");
        ElUtil::CheckErrorAndLog(env, "nativeRelayout: FindMethod: setNativeObjectLocked: %d!\n", __LINE__);

        env->CallVoidMethod(joutSurface, mid, nativeSurface);
        ElUtil::CheckErrorAndLog(env, "nativeRelayout: call method setNativeObjectLocked: %d!\n", __LINE__);
    }

    env->DeleteLocalRef(surfaceKlass);

    // ALOGD("- android_view_ElWindowSessionProxy_nativeRelayout()");
    return result;
}

static void android_view_ElWindowSessionProxy_nativeFinishDrawing(JNIEnv* env, jobject clazz, jlong jproxy, jint jwindow)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeFinishDrawing()");
    IWindowSession* session = (IWindowSession*)jproxy;

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
        }
    }
    if (NULL == window) {
        ALOGE("Invalid jwindow!\n");
        env->ExceptionDescribe();
    }
    session->FinishDrawing(window);
    // ALOGD("- android_view_ElWindowSessionProxy_nativeFinishDrawing()");
}

static void android_view_ElWindowSessionProxy_nativeOnRectangleOnScreenRequested(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jwindow, jobject jrectangle)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeOnRectangleOnScreenRequested()");
    IWindowSession* session = (IWindowSession*)jproxy;

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
        }
    }
    if (NULL == window) {
        ALOGE("Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IRect> rectangle;
    if(jrectangle != NULL) {
        if(!ElUtil::ToElRect(env, jrectangle, (IRect**)&rectangle)) {
            ALOGE("nativeOnRectangleOnScreenRequested() ToElRect fail!");
        }
    }
    IBinder* bw = (IBinder*)window->Probe(EIID_IBinder);
    if(bw == NULL) {
        ALOGE("nativeOnRectangleOnScreenRequested() bw is NULL!");
    }
    session->OnRectangleOnScreenRequested(bw, rectangle);
    // ALOGD("- android_view_ElWindowSessionProxy_nativeOnRectangleOnScreenRequested()");
}

static void android_view_ElWindowSessionProxy_nativeRemove(JNIEnv* env, jobject clazz, jlong jproxy, jint jwindow)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeRemove()");
    IWindowSession* session = (IWindowSession*)jproxy;

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
            sWindowMap.Erase(it);
        }
    }
    if (NULL == window) {
        ALOGE("nativeRemove() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }
    session->Remove(window);

    // ALOGD("- android_view_ElWindowSessionProxy_nativeRemove()");
}

static jboolean android_view_ElWindowSessionProxy_nativePerformHapticFeedback(JNIEnv* env, jobject clazz, jlong jproxy, jint jwindow, jint jeffectId, jboolean jalways)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativePerformHapticFeedback()");

    IWindowSession* session = (IWindowSession*)jproxy;

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
        }
    }

    if (NULL == window) {
        ALOGE("nativeRemove() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    Boolean result = FALSE;
    session->PerformHapticFeedback(window, (Int32)jeffectId, (Int32)jalways, &result);

    // ALOGD("- android_view_ElWindowSessionProxy_nativePerformHapticFeedback()");
    return (jboolean)result;
}

static void android_view_ElWindowSessionProxy_nativeSetWallpaperPosition(JNIEnv* env, jobject clazz, jlong jproxy, jint jwindow,
        jfloat jx, jfloat jy, jfloat jxStep, jfloat jyStep)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeSetWallpaperPosition()");
    IWindowSession* session = (IWindowSession*)jproxy;

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
        }
    }
    if (NULL == window) {
        ALOGE("nativeRemove() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    IBinder* binder = (IBinder*)window->Probe(EIID_IBinder);
    session->SetWallpaperPosition(binder, (Float)jx, (Float)jy, (Float)jxStep, (Float)jyStep);
    // ALOGD("- android_view_ElWindowSessionProxy_nativeSetWallpaperPosition()");
}

static jboolean android_view_ElWindowSessionProxy_nativeGetInTouchMode(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeGetInTouchMode()");
    IWindowSession* session = (IWindowSession*)jproxy;

    Boolean result;
    session->GetInTouchMode(&result);
    // ALOGD("- android_view_ElWindowSessionProxy_nativeGetInTouchMode()");
    return (jboolean)result;
}

static void android_view_ElWindowSessionProxy_nativeSetInTouchMode(JNIEnv* env, jobject clazz, jlong jproxy, jboolean jshowFocus)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeSetInTouchMode()");
    IWindowSession* session = (IWindowSession*)jproxy;
    session->SetInTouchMode((Boolean)jshowFocus);
    // ALOGD("- android_view_ElWindowSessionProxy_nativeSetInTouchMode()");
}

static jint android_view_ElWindowSessionProxy_nativeAddToDisplayWithoutInputChannel(JNIEnv* env, jobject clazz, jlong jproxy, jobject jwindow,
    jint jwindowHashCode, jint jseq, jobject jattrs, jint jviewVisibility, jint jdisplayId, jobject jinContentInsets)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeAddToDisplayWithoutInputChannel()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jwindow);

    AutoPtr<IIWindow> window;
    ECode ec = CWindowNative::New((Handle64)jvm, (Handle64)jInstance, (IIWindow**)&window);
    if (FAILED(ec)) {
        ALOGE("nativeAddToDisplayWithoutInputChannel() new CWindowNative fail 0x%x ", ec);
    }
    {
        Mutex::Autolock lock(sWindowMapLock);
        sWindowMap[jwindowHashCode] = window;
    }

    AutoPtr<IWindowManagerLayoutParams> attrs;
    if(jattrs != NULL) {
        if(!ElUtil::ToElWindowManagerLayoutParams(env, jattrs, (IWindowManagerLayoutParams**)&attrs)) {
            ALOGE("nativeAddToDisplayWithoutInputChannel: ToElWindowManagerLayoutParams fail : %d!\n", __LINE__);
        }
    }

    AutoPtr<IRect> contentInsets;
    if(!ElUtil::ToElRect(env, jinContentInsets, (IRect**)&contentInsets)) {
        ALOGE("nativeAddToDisplayWithoutInputChannel: ToElRect fail : %d!\n", __LINE__);
    }

    AutoPtr<IRect> outContentInsets;
    Int32 result;

    IWindowSession* session = (IWindowSession*)jproxy;
    ec = session->AddToDisplayWithoutInputChannel(window, (Int32)jseq, attrs, (Int32)jviewVisibility, (Int32)jdisplayId,
                contentInsets, (IRect**)&outContentInsets, &result);

    if(outContentInsets != NULL) {
        jclass rectKlass = env->FindClass("android/graphics/Rect");
        ElUtil::CheckErrorAndLog(env, "nativeAddToDisplayWithoutInputChannel Fail FindClass: Rect : %d!\n", __LINE__);

        Int32 tmpInt;
        outContentInsets->GetLeft(&tmpInt);
        jfieldID f = env->GetFieldID(rectKlass, "left", "I");
        ElUtil::CheckErrorAndLog(env, "nativeAddToDisplayWithoutInputChannel Fail GetFieldID: mPtr : %d!\n", __LINE__);

        env->SetIntField(jinContentInsets, f, (jint)tmpInt);
        ElUtil::CheckErrorAndLog(env, "nativeAddToDisplayWithoutInputChannel Fail SetIntField: left : %d!\n", __LINE__);

        outContentInsets->GetTop(&tmpInt);
        f = env->GetFieldID(rectKlass, "top", "I");
        ElUtil::CheckErrorAndLog(env, "nativeAddToDisplayWithoutInputChannel Fail GetFieldID: top : %d!\n", __LINE__);

        env->SetIntField(jinContentInsets, f, (jint)tmpInt);
        ElUtil::CheckErrorAndLog(env, "nativeAddToDisplayWithoutInputChannel Fail SetIntField: top : %d!\n", __LINE__);

        outContentInsets->GetRight(&tmpInt);
        f = env->GetFieldID(rectKlass, "right", "I");
        ElUtil::CheckErrorAndLog(env, "nativeAddToDisplayWithoutInputChannel Fail GetFieldID: right : %d!\n", __LINE__);

        env->SetIntField(jinContentInsets, f, (jint)tmpInt);
        ElUtil::CheckErrorAndLog(env, "nativeAddToDisplayWithoutInputChannel Fail SetIntField: right : %d!\n", __LINE__);

        outContentInsets->GetBottom(&tmpInt);
        f = env->GetFieldID(rectKlass, "bottom", "I");
        ElUtil::CheckErrorAndLog(env, "nativeAddToDisplayWithoutInputChannel Fail GetFieldID: bottom : %d!\n", __LINE__);

        env->SetIntField(jinContentInsets, f, (jint)tmpInt);
        ElUtil::CheckErrorAndLog(env, "nativeAddToDisplayWithoutInputChannel Fail SetIntField: bottom : %d!\n", __LINE__);

        env->DeleteLocalRef(rectKlass);
    }

    // ALOGD("- android_view_ElWindowSessionProxy_nativeAddToDisplayWithoutInputChannel()");
    return result;
}

static void android_view_ElWindowSessionProxy_nativeSetTransparentRegion(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jwindow, jobject jregion)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeSetTransparentRegion()");

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
        }
    }
    if (NULL == window) {
        ALOGE("nativeSetTransparentRegion() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IRegion> region;
    if (jregion != NULL) {
        if (!ElUtil::ToElRegion(env, jregion, (IRegion**)&region)) {
            ALOGE("nativeSetTransparentRegion() ToElRegion fail!\n");
        }
    }

    IWindowSession* session = (IWindowSession*)jproxy;
    session->SetTransparentRegion(window, region);

    // ALOGD("- android_view_ElWindowSessionProxy_nativeSetTransparentRegion()");
}

static void android_view_ElWindowSessionProxy_nativeGetDisplayFrame(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jwindow, jobject joutDisplayFrame)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeGetDisplayFrame()");

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
        }
    }
    if (NULL == window) {
        ALOGE("nativeGetDisplayFrame() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }


    IWindowSession* session = (IWindowSession*)jproxy;
    AutoPtr<IRect> outDisplayFrame;
    session->GetDisplayFrame(window, (IRect**)&outDisplayFrame);
    if (outDisplayFrame != NULL) {
        ElUtil::SetJavaRect(env, joutDisplayFrame, outDisplayFrame);
    }

    // ALOGD("- android_view_ElWindowSessionProxy_nativeGetDisplayFrame()");
}

static void android_view_ElWindowSessionProxy_nativePerformDeferredDestroy(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jwindow)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativePerformDeferredDestroy()");

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
        }
    }
    if (NULL == window) {
        ALOGE("nativeGetDisplayFrame() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }


    IWindowSession* session = (IWindowSession*)jproxy;
    session->PerformDeferredDestroy(window);

    // ALOGD("- android_view_ElWindowSessionProxy_nativePerformDeferredDestroy()");
}

static void android_view_ElWindowSessionProxy_nativeSetInsets(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jwindow, jint jtouchable, jobject jcontentInsets, jobject jvisibleInsets, jobject jtouchableRegion)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeSetInsets()");

    AutoPtr<IIWindow> window;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            window = it->mSecond;
        }
    }

    if (NULL == window) {
        ALOGE("nativeGetDisplayFrame() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IRect> contentInsets;
    if (jcontentInsets != NULL) {
        if (!ElUtil::ToElRect(env, jcontentInsets, (IRect**)&contentInsets)) {
            ALOGE("nativeSetInsets ToElRect jcontentInsets fail!");
        }
    }

    AutoPtr<IRect> visibleInsets;
    if (jvisibleInsets != NULL) {
        if (!ElUtil::ToElRect(env, jvisibleInsets, (IRect**)&visibleInsets)) {
            ALOGE("nativeSetInsets ToElRect jvisibleInsets fail!");
        }
    }

    AutoPtr<IRegion> touchableRegion;
    if (jtouchableRegion != NULL) {
        if (!ElUtil::ToElRegion(env, jtouchableRegion, (IRegion**)&touchableRegion)) {
            ALOGE("nativeSetInsets ToElRegion jtouchableRegion fail!");
        }
    }

    IWindowSession* session = (IWindowSession*)jproxy;
    session->SetInsets(window, (Int32)jtouchable, contentInsets, visibleInsets, touchableRegion);

    // ALOGD("- android_view_ElWindowSessionProxy_nativeSetInsets()");
}

static jobject android_view_ElWindowSessionProxy_nativeSendWallpaperCommand(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jwindow, jstring jaction, jint jx, jint jy, jint jz, jobject jextras, jboolean jsync)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeSendWallpaperCommand()");

    AutoPtr<IIWindow> iwindow;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            iwindow = it->mSecond;
        }
    }
    if (NULL == iwindow) {
        ALOGE("nativeRemove() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    IBinder* window = (IBinder*)iwindow->Probe(EIID_IBinder);

    String action = ElUtil::ToElString(env, jaction);

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("nativeSendWallpaperCommand() ToElBundle fail!");
        }
    }

    IWindowSession* session = (IWindowSession*)jproxy;
    AutoPtr<IBundle> result;
    session->SendWallpaperCommand(window, action, (Int32)jx, (Int32)jy, (Int32)jz,
        extras, (Boolean)jsync, (IBundle**)&result);

    jobject jresult = NULL;
    if (result != NULL) {
        jresult = ElUtil::GetJavaBundle(env, result);
    }

    // ALOGD("- android_view_ElWindowSessionProxy_nativeSendWallpaperCommand()");
    return jresult;
}

static void android_view_ElWindowSessionProxy_nativeWallpaperOffsetsComplete(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jwindow)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeWallpaperOffsetsComplete()");

    AutoPtr<IIWindow> iwindow;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            iwindow = it->mSecond;
        }
    }
    if (NULL == iwindow) {
        ALOGE("nativeRemove() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    IBinder* window = (IBinder*)iwindow->Probe(EIID_IBinder);

    IWindowSession* session = (IWindowSession*)jproxy;
    session->WallpaperOffsetsComplete(window);

    // ALOGD("- android_view_ElWindowSessionProxy_nativeWallpaperOffsetsComplete()");
}

static void android_view_ElWindowSessionProxy_nativeSetWallpaperDisplayOffset(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jwindow, jint x, jint y)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeSetWallpaperDisplayOffset()");

    AutoPtr<IIWindow> iwindow;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            iwindow = it->mSecond;
        }
    }
    if (NULL == iwindow) {
        ALOGE("nativeRemove() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    IBinder* window = (IBinder*)iwindow->Probe(EIID_IBinder);

    IWindowSession* session = (IWindowSession*)jproxy;
    session->SetWallpaperDisplayOffset(window, x, y);

    // ALOGD("- android_view_ElWindowSessionProxy_nativeSetWallpaperDisplayOffset()");
}

static jint android_view_ElWindowSessionProxy_nativeGetLastWallpaperX(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeGetLastWallpaperX()");
    IWindowSession* session = (IWindowSession*)jproxy;

    Int32 result;
    session->GetLastWallpaperX(&result);
    // ALOGD("- android_view_ElWindowSessionProxy_nativeGetLastWallpaperX()");
    return (jint)result;
}

static jint android_view_ElWindowSessionProxy_nativeGetLastWallpaperY(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeGetLastWallpaperY()");
    IWindowSession* session = (IWindowSession*)jproxy;

    Int32 result;
    session->GetLastWallpaperY(&result);
    // ALOGD("- android_view_ElWindowSessionProxy_nativeGetLastWallpaperY()");
    return (jint)result;
}

static void android_view_ElWindowSessionProxy_nativeWallpaperCommandComplete(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jwindow, jobject jresult)
{
    ALOGD("+ android_view_ElWindowSessionProxy_nativeWallpaperCommandComplete()");

    AutoPtr<IIWindow> iwindow;
    {
        Mutex::Autolock lock(sWindowMapLock);
        HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jwindow);
        if (it != sWindowMap.End()) {
            iwindow = it->mSecond;
        }
    }
    if (NULL == iwindow) {
        ALOGE("nativeRemove() Invalid jwindow!\n");
        env->ExceptionDescribe();
    }

    IBinder* window = (IBinder*)iwindow->Probe(EIID_IBinder);

    AutoPtr<IBundle> result;
    if (jresult != NULL) {
        if (!ElUtil::ToElBundle(env, jresult, (IBundle**)&result)) {
            ALOGE("nativeSendWallpaperCommand() ToElBundle fail!");
        }
    }

    IWindowSession* session = (IWindowSession*)jproxy;
    session->WallpaperCommandComplete(window, result);

    ALOGD("- android_view_ElWindowSessionProxy_nativeWallpaperCommandComplete()");
}

static void android_view_ElWindowSessionProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowSessionProxy_nativeDestroy()");

    IWindowSession* obj = (IWindowSession*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_view_ElWindowSessionProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_view_ElWindowSessionProxy_nativeFinalize },
    { "nativeAddToDisplay",    "(JLandroid/view/IWindow;IILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/view/InputChannel;)I",
            (void*) android_view_ElWindowSessionProxy_nativeAddToDisplay },
    { "nativeRelayout",    "(JIILandroid/view/WindowManager$LayoutParams;IIIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I",
            (void*) android_view_ElWindowSessionProxy_nativeRelayout },
    { "nativeFinishDrawing",    "(JI)V",
            (void*) android_view_ElWindowSessionProxy_nativeFinishDrawing },
    { "nativeOnRectangleOnScreenRequested",    "(JILandroid/graphics/Rect;)V",
            (void*) android_view_ElWindowSessionProxy_nativeOnRectangleOnScreenRequested },
    { "nativeRemove",    "(JI)V",
            (void*) android_view_ElWindowSessionProxy_nativeRemove },
    { "nativePerformHapticFeedback",    "(JIIZ)Z",
            (void*) android_view_ElWindowSessionProxy_nativePerformHapticFeedback },
    { "nativeSetWallpaperPosition",    "(JIFFFF)V",
            (void*) android_view_ElWindowSessionProxy_nativeSetWallpaperPosition },
    { "nativeGetInTouchMode",    "(J)Z",
            (void*) android_view_ElWindowSessionProxy_nativeGetInTouchMode },
    { "nativeSetInTouchMode",    "(JZ)V",
            (void*) android_view_ElWindowSessionProxy_nativeSetInTouchMode },
    { "nativeAddToDisplayWithoutInputChannel",    "(JLandroid/view/IWindow;IILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;)I",
            (void*) android_view_ElWindowSessionProxy_nativeAddToDisplayWithoutInputChannel },
    { "nativeSetTransparentRegion",    "(JILandroid/graphics/Region;)V",
            (void*) android_view_ElWindowSessionProxy_nativeSetTransparentRegion },
    { "nativeGetDisplayFrame",    "(JILandroid/graphics/Rect;)V",
            (void*) android_view_ElWindowSessionProxy_nativeGetDisplayFrame },
    { "nativePerformDeferredDestroy",    "(JI)V",
            (void*) android_view_ElWindowSessionProxy_nativePerformDeferredDestroy },
    { "nativeSetInsets",    "(JIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Region;)V",
            (void*) android_view_ElWindowSessionProxy_nativeSetInsets },
    { "nativeSendWallpaperCommand",    "(JILjava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;",
            (void*) android_view_ElWindowSessionProxy_nativeSendWallpaperCommand },
    { "nativeWallpaperOffsetsComplete",    "(JI)V",
            (void*) android_view_ElWindowSessionProxy_nativeWallpaperOffsetsComplete },
    { "nativeSetWallpaperDisplayOffset",    "(JIII)V",
            (void*) android_view_ElWindowSessionProxy_nativeSetWallpaperDisplayOffset },
    { "nativeGetLastWallpaperX",    "(J)I",
            (void*) android_view_ElWindowSessionProxy_nativeGetLastWallpaperX },
    { "nativeGetLastWallpaperY",    "(J)I",
            (void*) android_view_ElWindowSessionProxy_nativeGetLastWallpaperY },
    { "nativeWallpaperCommandComplete",    "(JILandroid/os/Bundle;)V",
            (void*) android_view_ElWindowSessionProxy_nativeWallpaperCommandComplete },
};

int register_android_view_ElWindowSessionProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/view/ElWindowSessionProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

