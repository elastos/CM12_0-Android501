
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Location.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <elastos/utility/etl/HashMap.h>

#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Location::CCountry;
using Elastos::Droid::Location::ICountry;
using Elastos::Droid::Location::IICountryDetector;
using Elastos::Droid::Location::IICountryListener;
using Elastos::Droid::JavaProxy::CICountryListenerNative;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, IICountryListener*> sCListener;
static Mutex sCListenerLock;

static jobject android_location_ElCountryDetectorProxy_nativeDetectCountry(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_location_ElCountryDetectorProxy_nativeDetectCountry()");

    IICountryDetector* icd = (IICountryDetector*)jproxy;

    AutoPtr<ICountry> country;
    ECode ec = icd->DetectCountry((ICountry**)&country);
    ALOGE("nativeDetectCountry() ec = 0x%08x", ec);

    // TODO: need delete
    if (country == NULL) {
        ALOGE("nativeDetectCountry() Forged Country!");
        CCountry::New(String("CN"), 0/*COUNTRY_SOURCE_NETWORK*/, (ICountry**)&country);
    }
    // TODO: end

    jobject jcountry = NULL;
    if (country != NULL) {
        jcountry = ElUtil::GetJavaCountry(env, country);
    }

    // ALOGD("- android_location_ElCountryDetectorProxy_nativeDetectCountry()");
    return jcountry;
}

static void android_location_ElCountryDetectorProxy_nativeAddCountryListener(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlistener)
{
    // ALOGD("+ android_location_ElCountryDetectorProxy_nativeAddCountryListener()");

    AutoPtr<IICountryListener> listener;
    if (jlistener != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jlistener);

        CICountryListenerNative::New((Handle64)jvm, (Handle64)jInstance, (IICountryListener**)&listener);
        listener->AddRef();

        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
        Mutex::Autolock lock(sCListenerLock);
        sCListener[hashCode] = listener;
    }

    IICountryDetector* icd = (IICountryDetector*)jproxy;
    icd->AddCountryListener(listener);

    // ALOGD("- android_location_ElCountryDetectorProxy_nativeAddCountryListener()");
}

static void android_location_ElCountryDetectorProxy_nativeRemoveCountryListener(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlistener)
{
    ALOGD("+ android_location_ElCountryDetectorProxy_nativeRemoveCountryListener()");

    AutoPtr<IICountryListener> listener;

    {
        Mutex::Autolock lock(sCListenerLock);
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
        if (sCListener.Find(hashCode) != sCListener.End()) {
            listener = sCListener[hashCode];
            sCListener.Erase(hashCode);
        }
    }

    IICountryDetector* icd = (IICountryDetector*)jproxy;
    ECode ec = icd->RemoveCountryListener(listener);
    ALOGE("nativeRemoveCountryListener() ec = 0x%08x", ec);

    listener->Release();
    ALOGD("- android_location_ElCountryDetectorProxy_nativeRemoveCountryListener()");
}

static void android_location_ElCountryDetectorProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_location_ElCountryDetectorProxy_nativeDestroy()");

    IICountryDetector* obj = (IICountryDetector*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_location_ElCountryDetectorProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_location_ElCountryDetectorProxy_nativeFinalize },
    { "nativeDetectCountry",    "(J)Landroid/location/Country;",
            (void*) android_location_ElCountryDetectorProxy_nativeDetectCountry },
    { "nativeAddCountryListener",    "(JLandroid/location/ICountryListener;)V",
            (void*) android_location_ElCountryDetectorProxy_nativeAddCountryListener },
    { "nativeRemoveCountryListener",    "(JLandroid/location/ICountryListener;)V",
            (void*) android_location_ElCountryDetectorProxy_nativeRemoveCountryListener },
};

int register_android_location_ElCountryDetectorProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/location/ElCountryDetectorProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

