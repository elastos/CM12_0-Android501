
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Hardware.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.View.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <elastos/utility/etl/HashMap.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::View::IInputDevice;
using Elastos::Droid::Hardware::Input::IIInputManager;
using Elastos::Droid::Hardware::Input::IInputDevicesChangedListener;
using Elastos::Droid::JavaProxy::CInputDevicesChangedListenerNative;
using Elastos::Droid::JavaProxy::CBinderNative;
using Elastos::Droid::Os::IBinder;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, AutoPtr<IBinder> > sTokenMap;
static Mutex sTokenMapLock;

static void android_hardware_input_ElInputManagerProxy_nativeRegisterInputDevicesChangedListener(JNIEnv* env, jobject clazz, jlong jproxy, jobject jlistener)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeRegisterInputDevicesChangedListener()");

    JavaVM* jvm;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jlistener);
    AutoPtr<IInputDevicesChangedListener> listener;
    ECode ec = CInputDevicesChangedListenerNative::New((Handle64)jvm, (Handle64)jInstance, (IInputDevicesChangedListener**)&listener);
    if (FAILED(ec)) {
        ALOGE("====== CInputDevicesChangedListenerNative::New ec: 0x%x =====\n", ec);
    }
    listener->AddRef();

    IIInputManager* im = (IIInputManager*)jproxy;
    im->RegisterInputDevicesChangedListener(listener);
    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeRegisterInputDevicesChangedListener()");
}

static jintArray android_hardware_input_ElInputManagerProxy_nativeGetInputDeviceIds(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeGetInputDeviceIds()");

    IIInputManager* im = (IIInputManager*)jproxy;
    AutoPtr<ArrayOf<Int32> > ids;
    im->GetInputDeviceIds((ArrayOf<Int32>**)&ids);

    jintArray jids = NULL;
    if(ids != NULL) {
        Int32 count = ids->GetLength();
        jids= env->NewIntArray(count);
        ElUtil::CheckErrorAndLog(env, "nativeGetInputDeviceIds() NewIntArray fail : %d!\n", __LINE__);
        if (count > 0) {
            jint* idsPlayLoad = (jint*)ids->GetPayload();

            env->SetIntArrayRegion(jids, 0, count, idsPlayLoad);
            ElUtil::CheckErrorAndLog(env, "nativeGetInputDeviceIds() SetIntArrayRegion fail : %d!\n", __LINE__);
        }
    }
    else {
        jids= env->NewIntArray(0);
        ElUtil::CheckErrorAndLog(env, "nativeGetInputDeviceIds() NewIntArray[0] fail : %d!\n", __LINE__);
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeGetInputDeviceIds()");
    return jids;
}

static jobject android_hardware_input_ElInputManagerProxy_nativeGetInputDevice(JNIEnv* env, jobject clazz, jlong jproxy, jint jdeviceId)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeGetInputDevice()");

    IIInputManager* im = (IIInputManager*)jproxy;

    jobject jinpDevice = NULL;
    AutoPtr<IInputDevice> inpDevice;
    im->GetInputDevice((Int32)jdeviceId, (IInputDevice**)&inpDevice);
    if(inpDevice != NULL) {
        jinpDevice = ElUtil::GetJavaInputDevice(env, inpDevice);
        if(jinpDevice == NULL) {
            ALOGE("android_hardware_input_ElInputManagerProxy_nativeGetInputDevice() jinpDevice is NULL!");
        }
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeGetInputDevice()");
    return jinpDevice;
}

static jboolean android_hardware_input_ElInputManagerProxy_nativeHasKeys(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdeviceId, jint jsourceMask, jintArray jkeyCodes, jbooleanArray jkeyExists)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeHasKeys()");

    AutoPtr<ArrayOf<Int32> > keyCodes;
    jint* jipayload = NULL;
    if (jkeyCodes != NULL) {
        jint size = env->GetArrayLength(jkeyCodes);
        ElUtil::CheckErrorAndLog(env, "nativeHasKeys() GetArrayLength:(): %d!\n", __LINE__);

        jipayload = (jint*)malloc(size * sizeof(Int32));

        env->GetIntArrayRegion(jkeyCodes, 0, size, jipayload);
        ElUtil::CheckErrorAndLog(env, "nativeHasKeys() GetIntArrayRegion:(): %d!\n", __LINE__);
        keyCodes = ArrayOf<Int32>::Alloc((Int32*)jipayload, size);
    }

    AutoPtr<ArrayOf<Boolean> > keyExists;
    jboolean* jbpayload = NULL;
    if (jkeyExists != NULL) {
        jint size = env->GetArrayLength(jkeyExists);
        ElUtil::CheckErrorAndLog(env, "nativeHasKeys() GetArrayLength:(): %d!\n", __LINE__);

        jbpayload = (jboolean*)malloc(size * sizeof(Boolean));

        env->GetBooleanArrayRegion(jkeyExists, 0, size, jbpayload);
        ElUtil::CheckErrorAndLog(env, "nativeHasKeys() GetBooleanArrayRegion:(): %d!\n", __LINE__);
        keyExists = ArrayOf<Boolean>::Alloc((Boolean*)jbpayload, size);
    }

    IIInputManager* im = (IIInputManager*)jproxy;

    jobject jinpDevice = NULL;
    Boolean result = FALSE;
    im->HasKeys((Int32)jdeviceId, (Int32)jsourceMask, *keyCodes, keyExists, &result);

    free(jipayload);
    free(jbpayload);

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeHasKeys()");
    return (jboolean)result;
}

static jboolean android_hardware_input_ElInputManagerProxy_nativeInjectInputEvent(JNIEnv* env, jobject clazz, jlong jproxy, jobject jinputEvent, jint jmode)
{
    IIInputManager* im = (IIInputManager*)jproxy;
    AutoPtr<IInputEvent> ev;

    ElUtil::ToElInputEvent(env, jinputEvent, (IInputEvent**)&ev);

    Boolean result;
    im->InjectInputEvent(ev, (Int32)jmode, &result);

    return result;
}

static jobject android_hardware_input_ElInputManagerProxy_nativeGetTouchCalibrationForInputDevice(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jinputDeviceDescriptor, jint rotation)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeGetTouchCalibrationForInputDevice()");

    String inputDeviceDescriptor = ElUtil::ToElString(env, jinputDeviceDescriptor);

    IIInputManager* im = (IIInputManager*)jproxy;

    AutoPtr<ITouchCalibration> touchCalibration;
    im->GetTouchCalibrationForInputDevice(inputDeviceDescriptor, rotation, (ITouchCalibration**)&touchCalibration);

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeGetTouchCalibrationForInputDevice()");
    return ElUtil::GetJavaTouchCalibration(env, touchCalibration);
}

static void android_hardware_input_ElInputManagerProxy_nativeSetTouchCalibrationForInputDevice(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jinputDeviceDescriptor, jint rotation, jobject jcalibration)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeSetTouchCalibrationForInputDevice()");

    String inputDeviceDescriptor = ElUtil::ToElString(env, jinputDeviceDescriptor);

    AutoPtr<ITouchCalibration> calibration;
    if (jcalibration != NULL) {
        if (!ElUtil::ToElTouchCalibration(env, jcalibration, (ITouchCalibration**)&calibration)) {
            ALOGE("nativeSetTouchCalibrationForInputDevice: ToElTouchCalibration fail");
        }
    }
    IIInputManager* im = (IIInputManager*)jproxy;

    im->SetTouchCalibrationForInputDevice(inputDeviceDescriptor, rotation, calibration);

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeSetTouchCalibrationForInputDevice()");
}

static jobjectArray android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayouts(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayouts()");

    IIInputManager* im = (IIInputManager*)jproxy;
    AutoPtr<ArrayOf<IKeyboardLayout*> > layouts;
    if (FAILED(im->GetKeyboardLayouts((ArrayOf<IKeyboardLayout*>**)&layouts) ) ) {
        ALOGE("nativeGetKeyboardLayouts() Failed");
    }

    if (NULL == layouts) {
        ALOGE("nativeGetKeyboardLayouts() layouts = NULL at %d", __LINE__);
        return NULL;
    }

    jobjectArray jlayouts = NULL;
    Int32 count = layouts->GetLength();
    jclass kbdlayout = env->FindClass("android/hardware/input/KeyboardLayout");
    ElUtil::CheckErrorAndLog(env, "FindClass: KeyboardLayout : %d!\n", __LINE__);

    jlayouts = env->NewObjectArray((jsize)count, kbdlayout, NULL);
    ElUtil::CheckErrorAndLog(env, "NewObjectArray: KeyboardLayout : %d!\n", __LINE__);
    env->DeleteLocalRef(kbdlayout);

    if (NULL != jlayouts) {
        for(Int32 i = 0; i < count; i++ ) {
            jobject jlayout = ElUtil::GetJavaKeyboardLayout(env, (*layouts)[i]);
            if (jlayout != NULL) {
                env->SetObjectArrayElement(jlayouts, i, jlayout);
                env->DeleteLocalRef(jlayout);
            }
        }
    }
    else {
        ALOGE("nativeGetKeyboardLayouts() jlayouts is NULL");
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayouts()");

    return jlayouts;
}

static jobject android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayout(JNIEnv* env, jobject clazz,
    jlong jproxy, jstring jdescriptor)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayout()");

    IIInputManager* im = (IIInputManager*)jproxy;

    String descriptor = ElUtil::ToElString(env, jdescriptor);
    AutoPtr<IKeyboardLayout> layout;
    if (FAILED(im->GetKeyboardLayout(descriptor, (IKeyboardLayout**)&layout) ) ) {
        ALOGE("nativeGetKeyboardLayouts() Failed");
    }

    jobject jlayout = NULL;
    if (NULL != layout) {
        jlayout = ElUtil::GetJavaKeyboardLayout(env, layout);
    }
    else {
        ALOGE("nativeGetKeyboardLayouts() layout is NULL");
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayout()");

    return jlayout;
}

static jstring android_hardware_input_ElInputManagerProxy_nativeGetCurrentKeyboardLayoutForInputDevice(JNIEnv* env, jobject clazz,
    jlong jproxy, jobject jidentifier)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeGetCurrentKeyboardLayoutForInputDevice()");

    IIInputManager* im = (IIInputManager*)jproxy;

    AutoPtr<IInputDeviceIdentifier> identifier;
    if (jidentifier != NULL) {
        if (!ElUtil::ToElInputDeviceIdentifier(env, jidentifier, (IInputDeviceIdentifier**)&identifier)) {
            ALOGE("nativeGetCurrentKeyboardLayoutForInputDevice: ToElInputDeviceIdentifier fail");
        }
    }

    String layout;
    if (FAILED(im->GetCurrentKeyboardLayoutForInputDevice(identifier, &layout) ) )  {
        ALOGE("nativeGetCurrentKeyboardLayoutForInputDevice() Failed in %d", __LINE__ );
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeGetCurrentKeyboardLayoutForInputDevice()");

    return ElUtil::GetJavaString(env, layout);
}

static void android_hardware_input_ElInputManagerProxy_nativeSetCurrentKeyboardLayoutForInputDevice(JNIEnv* env, jobject clazz,
    jlong jproxy, jobject jidentifier, jstring jlayoutDescriptor)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeSetCurrentKeyboardLayoutForInputDevice()");

    IIInputManager* im = (IIInputManager*)jproxy;

    AutoPtr<IInputDeviceIdentifier> identifier;
    if (jidentifier != NULL) {
        if (!ElUtil::ToElInputDeviceIdentifier(env, jidentifier, (IInputDeviceIdentifier**)&identifier)) {
            ALOGE("nativeSetCurrentKeyboardLayoutForInputDevice: ToElInputDeviceIdentifier fail");
        }
    }

    String layoutDescriptor = ElUtil::ToElString(env, jlayoutDescriptor);

    if (FAILED(im->SetCurrentKeyboardLayoutForInputDevice(identifier, layoutDescriptor) ) ) {
        ALOGE("nativeSetCurrentKeyboardLayoutForInputDevice() Failed in %d", __LINE__ );
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeSetCurrentKeyboardLayoutForInputDevice()");
}

static jobjectArray android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayoutsForInputDevice(JNIEnv* env, jobject clazz,
    jlong jproxy, jobject jidentifier)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayoutsForInputDevice()");

    IIInputManager* im = (IIInputManager*)jproxy;

    AutoPtr<IInputDeviceIdentifier> identifier;
    if (jidentifier != NULL) {
        if (!ElUtil::ToElInputDeviceIdentifier(env, jidentifier, (IInputDeviceIdentifier**)&identifier)) {
            ALOGE("nativeGetKeyboardLayoutsForInputDevice: ToElInputDeviceIdentifier fail");
        }
    }

    AutoPtr<ArrayOf<String> >layoutDescriptors;
    im->GetKeyboardLayoutsForInputDevice(identifier, (ArrayOf<String> **)&layoutDescriptors);

    if (NULL == layoutDescriptors) {
        ALOGE("nativeSetCurrentKeyboardLayoutForInputDevice() layoutDescriptors is NULL");
        return NULL;
    }

    jobjectArray jlayoutDescriptors = NULL;
    Int32 count = layoutDescriptors->GetLength();
    jclass clsstring = env->FindClass("java/lang/String");
    ElUtil::CheckErrorAndLog(env, "FindClass: String : %d!\n", __LINE__);

    jlayoutDescriptors = env->NewObjectArray((jsize)count, clsstring, NULL);
    ElUtil::CheckErrorAndLog(env, "NewObjectArray: String : %d!\n", __LINE__);
    env->DeleteLocalRef(clsstring);

    if (NULL != jlayoutDescriptors) {
        for(Int32 i = 0; i < count; i++ ) {
            jstring jdescriptor = ElUtil::GetJavaString(env, (*(layoutDescriptors.Get()))[i]);
            if (jdescriptor != NULL) {
                env->SetObjectArrayElement(jlayoutDescriptors, i, jdescriptor);
                env->DeleteLocalRef(jdescriptor);
            }
        }
    }
    else {
        ALOGE("nativeGetKeyboardLayoutsForInputDevice() jlayoutDescriptors is NULL");
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayoutsForInputDevice()");

    return jlayoutDescriptors;
}

static void android_hardware_input_ElInputManagerProxy_nativeAddKeyboardLayoutForInputDevice(JNIEnv* env, jobject clazz,
    jlong jproxy, jobject jidentifier, jstring jlayoutDescriptor)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeAddKeyboardLayoutForInputDevice()");

    IIInputManager* im = (IIInputManager*)jproxy;

    AutoPtr<IInputDeviceIdentifier> identifier;
    if (jidentifier != NULL) {
        if (!ElUtil::ToElInputDeviceIdentifier(env, jidentifier, (IInputDeviceIdentifier**)&identifier)) {
            ALOGE("nativeAddKeyboardLayoutForInputDevice: ToElInputDeviceIdentifier fail");
        }
    }

    String layoutDescriptor = ElUtil::ToElString(env, jlayoutDescriptor);

    if (FAILED(im->AddKeyboardLayoutForInputDevice(identifier, layoutDescriptor) )) {
        ALOGE("nativeAddKeyboardLayoutForInputDevice() Failed in %d", __LINE__ );
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeAddKeyboardLayoutForInputDevice()");
}

static void android_hardware_input_ElInputManagerProxy_nativeRemoveKeyboardLayoutForInputDevice(JNIEnv* env, jobject clazz,
    jlong jproxy, jobject jidentifier, jstring jlayoutDescriptor)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeRemoveKeyboardLayoutForInputDevice()");

    IIInputManager* im = (IIInputManager*)jproxy;

    AutoPtr<IInputDeviceIdentifier> identifier;
    if (jidentifier != NULL) {
        if (!ElUtil::ToElInputDeviceIdentifier(env, jidentifier, (IInputDeviceIdentifier**)&identifier)) {
            ALOGE("nativeRemoveKeyboardLayoutForInputDevice: ToElInputDeviceIdentifier fail");
        }
    }

    String layoutDescriptor = ElUtil::ToElString(env, jlayoutDescriptor);

    if (FAILED(im->RemoveKeyboardLayoutForInputDevice(identifier, layoutDescriptor) ) ){
        ALOGE("nativeRemoveKeyboardLayoutForInputDevice() Failed in %d", __LINE__ );
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeRemoveKeyboardLayoutForInputDevice()");
}

static void android_hardware_input_ElInputManagerProxy_nativeVibrate(JNIEnv* env, jobject clazz,
    jlong jproxy, jint deviceId, jlongArray jpatterns, jint jrepeat, jobject jtoken)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeVibrate()");

    IIInputManager* im = (IIInputManager*)jproxy;

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jtoken);
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IBinder> token;
    if(NOERROR != CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&token)) {
        ALOGE("nativeVibrate() new CBinderNative fail!\n");
    }

    if (NULL == token) {
        ALOGE("nativeVibrate() token = NULL");
    }
    else {
        {
            Mutex::Autolock lock(sTokenMapLock);
            sTokenMap[hashCode] = token;
        }

        AutoPtr<ArrayOf<Int64> > patterns;
        if (!ElUtil::ToElLongArray(env, jpatterns, (ArrayOf<Int64>**)&patterns)) {
            ALOGE("nativeVibrate() ToElLongArray failed");
        }
        im->Vibrate(deviceId, *patterns, jrepeat, token);
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeVibrate()");
}

static void android_hardware_input_ElInputManagerProxy_nativeCancelVibrate(JNIEnv* env, jobject clazz,
    jlong jproxy, jint deviceId, jobject jtoken)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeCancelVibrat()");

    IIInputManager* im = (IIInputManager*)jproxy;

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        {
            Mutex::Autolock lock(sTokenMapLock);
            HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sTokenMap.Find(hashCode);
            if (it != sTokenMap.End()) {
                token = IBinder::Probe(it->mSecond);
                sTokenMap.Erase(it);
            }
        }

        if (NULL == token) {
            ALOGE("nativeMoveAppToken() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
        else {
            im->CancelVibrate(deviceId, token);
        }
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeCancelVibrat()");
}

static void android_hardware_input_ElInputManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_hardware_input_ElInputManagerProxy_nativeDestroy()");

    IIInputManager* obj = (IIInputManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_hardware_input_ElInputManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_hardware_input_ElInputManagerProxy_nativeFinalize },
    { "nativeRegisterInputDevicesChangedListener",    "(JLandroid/hardware/input/IInputDevicesChangedListener;)V",
            (void*) android_hardware_input_ElInputManagerProxy_nativeRegisterInputDevicesChangedListener },
    { "nativeGetInputDeviceIds",    "(J)[I",
            (void*) android_hardware_input_ElInputManagerProxy_nativeGetInputDeviceIds },
    { "nativeGetInputDevice",    "(JI)Landroid/view/InputDevice;",
            (void*) android_hardware_input_ElInputManagerProxy_nativeGetInputDevice },
    { "nativeHasKeys",    "(JII[I[Z)Z",
            (void*) android_hardware_input_ElInputManagerProxy_nativeHasKeys },
    { "nativeInjectInputEvent",    "(JLandroid/view/InputEvent;I)Z",
            (void*) android_hardware_input_ElInputManagerProxy_nativeInjectInputEvent },

    { "nativeGetTouchCalibrationForInputDevice",    "(JLjava/lang/String;I)Landroid/hardware/input/TouchCalibration;",
            (void*) android_hardware_input_ElInputManagerProxy_nativeGetTouchCalibrationForInputDevice },

    { "nativeSetTouchCalibrationForInputDevice",    "(JLjava/lang/String;ILandroid/hardware/input/TouchCalibration;)V",
            (void*) android_hardware_input_ElInputManagerProxy_nativeSetTouchCalibrationForInputDevice },

    { "nativeGetKeyboardLayouts",    "(J)[Landroid/hardware/input/KeyboardLayout;",
            (void*) android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayouts },

    { "nativeGetKeyboardLayout",    "(JLjava/lang/String;)Landroid/hardware/input/KeyboardLayout;",
            (void*) android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayout },

    { "nativeGetCurrentKeyboardLayoutForInputDevice",    "(JLandroid/hardware/input/InputDeviceIdentifier;)Ljava/lang/String;",
            (void*) android_hardware_input_ElInputManagerProxy_nativeGetCurrentKeyboardLayoutForInputDevice },

    { "nativeSetCurrentKeyboardLayoutForInputDevice",    "(JLandroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V",
            (void*) android_hardware_input_ElInputManagerProxy_nativeSetCurrentKeyboardLayoutForInputDevice },

    { "nativeGetKeyboardLayoutsForInputDevice",    "(JLandroid/hardware/input/InputDeviceIdentifier;)[Ljava/lang/String;",
            (void*) android_hardware_input_ElInputManagerProxy_nativeGetKeyboardLayoutsForInputDevice },

    { "nativeAddKeyboardLayoutForInputDevice",    "(JLandroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V",
            (void*) android_hardware_input_ElInputManagerProxy_nativeAddKeyboardLayoutForInputDevice },

    { "nativeRemoveKeyboardLayoutForInputDevice",    "(JLandroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V",
            (void*) android_hardware_input_ElInputManagerProxy_nativeRemoveKeyboardLayoutForInputDevice },

    { "nativeVibrate",    "(JI[JILandroid/os/IBinder;)V",
            (void*) android_hardware_input_ElInputManagerProxy_nativeVibrate },

    { "nativeCancelVibrate",    "(JILandroid/os/IBinder;)V",
            (void*) android_hardware_input_ElInputManagerProxy_nativeCancelVibrate }
};

int register_android_hardware_input_ElInputManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/hardware/input/ElInputManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

