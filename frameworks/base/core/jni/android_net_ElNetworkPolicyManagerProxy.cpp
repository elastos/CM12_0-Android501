
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <elastos/utility/etl/HashMap.h>
#include <Elastos.Droid.Net.h>
#include <_Elastos.Droid.JavaProxy.h>

#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;

using Elastos::Droid::Net::IINetworkPolicyListener;
using Elastos::Droid::Net::INetworkPolicy;
using Elastos::Droid::Net::INetworkQuotaInfo;
using Elastos::Droid::Net::INetworkState;
using Elastos::Droid::Net::INetworkTemplate;
using Elastos::Droid::Net::IINetworkPolicyManager;
using Elastos::Droid::JavaProxy::CINetworkPolicyListenerNative;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, AutoPtr<IINetworkPolicyListener> > sListeners;
static Mutex sListenersLock;

static void android_net_ElNetworkPolicyManagerProxy_nativeSetUidPolicy(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jint policy)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeSetUidPolicy()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    npm->SetUidPolicy(uid, policy);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeSetUidPolicy()");
}

static void android_net_ElNetworkPolicyManagerProxy_nativeAddUidPolicy(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jint policy)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeAddUidPolicy()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    npm->AddUidPolicy(uid, policy);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeAddUidPolicy()");
}

static void android_net_ElNetworkPolicyManagerProxy_nativeRemoveUidPolicy(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jint policy)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeRemoveUidPolicy()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    npm->RemoveUidPolicy(uid, policy);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeRemoveUidPolicy()");
}

static jint android_net_ElNetworkPolicyManagerProxy_nativeGetUidPolicy(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeGetUidPolicy()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    Int32 policy;
    npm->GetUidPolicy(uid, &policy);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeGetUidPolicy()");
    return policy;
}

static jintArray android_net_ElNetworkPolicyManagerProxy_nativeGetUidsWithPolicy(
    JNIEnv* env, jobject clazz, jlong jproxy, jint policy)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeGetUidsWithPolicy()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    AutoPtr<ArrayOf<Int32> > uids;
    npm->GetUidsWithPolicy(policy, (ArrayOf<Int32>**)&uids);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeGetUidsWithPolicy()");
    return ElUtil::GetJavaIntArray(env, uids);
}

static jboolean android_net_ElNetworkPolicyManagerProxy_nativeIsUidForeground(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeIsUidForeground()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    Boolean res;
    npm->IsUidForeground(uid, &res);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeIsUidForeground()");
    return res;
}

static jintArray android_net_ElNetworkPolicyManagerProxy_nativeGetPowerSaveAppIdWhitelist(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeGetPowerSaveAppIdWhitelist()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    AutoPtr<ArrayOf<Int32> > list;
    npm->GetPowerSaveAppIdWhitelist((ArrayOf<Int32>**)&list);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeGetPowerSaveAppIdWhitelist()");
    return ElUtil::GetJavaIntArray(env, list);
}

static void android_net_ElNetworkPolicyManagerProxy_nativeRegisterListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jlistener)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeRegisterListener()");

    AutoPtr<IINetworkPolicyListener> listener;
    if (jlistener != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jlistener);
        CINetworkPolicyListenerNative::New((Handle64)jvm, (Handle64)jInstance, (IINetworkPolicyListener**)&listener);
        jint jlistenerHashcode = ElUtil::GetJavaHashCode(env, jlistener);
        Mutex::Autolock lock(sListenersLock);
        sListeners[jlistenerHashcode] = listener;
    }
    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    npm->RegisterListener(listener);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeRegisterListener()");
}

static void android_net_ElNetworkPolicyManagerProxy_nativeUnregisterListener(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jlistener)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeUnregisterListener()");

    AutoPtr<IINetworkPolicyListener> listener;
    if (jlistener != NULL) {
        jint jlistenerHashcode = ElUtil::GetJavaHashCode(env, jlistener);
        Mutex::Autolock lock(sListenersLock);
        HashMap<Int32, AutoPtr<IINetworkPolicyListener> >::Iterator it = sListeners.Find(jlistenerHashcode);
        if (it != sListeners.End()) {
            listener = it->mSecond;
            sListeners.Erase(it);
        }
    }

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    npm->UnregisterListener(listener);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeUnregisterListener()");
}

static void android_net_ElNetworkPolicyManagerProxy_nativeSetNetworkPolicies(
    JNIEnv* env, jobject clazz, jlong jproxy, jobjectArray jpolicies)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeSetNetworkPolicies()");

    AutoPtr<ArrayOf<INetworkPolicy*> > policies;
    if (jpolicies != NULL) {
        ALOGE("nativeSetNetworkPolicies jpolicies != NULL");
    }
    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    npm->SetNetworkPolicies(policies);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeSetNetworkPolicies()");
}

static jobjectArray android_net_ElNetworkPolicyManagerProxy_nativeGetNetworkPolicies(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeGetNetworkPolicies()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    AutoPtr<ArrayOf<INetworkPolicy*> > policies;
    npm->GetNetworkPolicies((ArrayOf<INetworkPolicy*>**)&policies);
    jobjectArray jpolicies = NULL;
    if (policies != NULL) {
        ALOGE("nativeGetNetworkPolicies policies != NULL");
    }

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeGetNetworkPolicies()");
    return jpolicies;
}

static void android_net_ElNetworkPolicyManagerProxy_nativeSnoozeLimit(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtemplate)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeSnoozeLimit()");

    AutoPtr<INetworkTemplate> nt;
    if (jtemplate != NULL) {
        ALOGE("nativeSnoozeLimit jtemplate != NULL");
    }
    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    npm->SnoozeLimit(nt);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeSnoozeLimit()");
}

static void android_net_ElNetworkPolicyManagerProxy_nativeSetRestrictBackground(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean restrictBackground)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeSetRestrictBackground()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    npm->SetRestrictBackground(restrictBackground);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeSetRestrictBackground()");
}

static jboolean android_net_ElNetworkPolicyManagerProxy_nativeGetRestrictBackground(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeGetRestrictBackground()");

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    Boolean restrictBackground;
    npm->GetRestrictBackground(&restrictBackground);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeGetRestrictBackground()");
    return restrictBackground;
}

static jobject android_net_ElNetworkPolicyManagerProxy_nativeGetNetworkQuotaInfo(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jstate)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeGetNetworkQuotaInfo()");

    AutoPtr<INetworkState> state;
    if (jstate != NULL) {
        ALOGE("nativeGetNetworkQuotaInfo jstate != NULL");
    }
    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    AutoPtr<INetworkQuotaInfo> nqi;
    npm->GetNetworkQuotaInfo(state, (INetworkQuotaInfo**)&nqi);

    jobject jqi = NULL;
    if (nqi != NULL) {
        ALOGE("nativeGetNetworkQuotaInfo NetworkQuotaInfo != NULL");
    }
    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeGetNetworkQuotaInfo()");
    return jqi;
}

static jboolean android_net_ElNetworkPolicyManagerProxy_nativeIsNetworkMetered(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jstate)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeIsNetworkMetered()");

    AutoPtr<INetworkState> state;
    if (jstate != NULL) {
        ALOGE("nativeGetNetworkQuotaInfo jstate != NULL");
    }

    IINetworkPolicyManager* npm = (IINetworkPolicyManager*)jproxy;
    Boolean res;
    npm->IsNetworkMetered(state, &res);

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeIsNetworkMetered()");
    return res;
}

static void android_net_ElNetworkPolicyManagerProxy_nativeFinalize(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkPolicyManagerProxy_nativeDestroy()");

    IINetworkPolicyManager* obj = (IINetworkPolicyManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_net_ElNetworkPolicyManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeFinalize },
    { "nativeSetUidPolicy",      "(JII)V",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeSetUidPolicy },
    { "nativeAddUidPolicy",      "(JII)V",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeAddUidPolicy },
    { "nativeRemoveUidPolicy",      "(JII)V",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeRemoveUidPolicy },
    { "nativeGetUidPolicy",      "(JI)I",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeGetUidPolicy },
    { "nativeGetUidsWithPolicy",      "(JI)[I",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeGetUidsWithPolicy },
    { "nativeIsUidForeground",      "(JI)Z",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeIsUidForeground },
    { "nativeGetPowerSaveAppIdWhitelist",      "(J)[I",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeGetPowerSaveAppIdWhitelist },
    { "nativeRegisterListener",      "(JLandroid/net/INetworkPolicyListener;)V",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeRegisterListener },
    { "nativeUnregisterListener",      "(JLandroid/net/INetworkPolicyListener;)V",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeUnregisterListener },
    { "nativeSetNetworkPolicies",      "(J[Landroid/net/NetworkPolicy;)V",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeSetNetworkPolicies },
    { "nativeGetNetworkPolicies",      "(J)[Landroid/net/NetworkPolicy;",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeGetNetworkPolicies },
    { "nativeSnoozeLimit",      "(JLandroid/net/NetworkTemplate;)V",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeSnoozeLimit },
    { "nativeSetRestrictBackground",      "(JZ)V",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeSetRestrictBackground },
    { "nativeGetRestrictBackground",      "(J)Z",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeGetRestrictBackground },
    { "nativeGetNetworkQuotaInfo",      "(JLandroid/net/NetworkState;)Landroid/net/NetworkQuotaInfo;",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeGetNetworkQuotaInfo },
    { "nativeIsNetworkMetered",      "(JLandroid/net/NetworkState;)Z",
            (void*) android_net_ElNetworkPolicyManagerProxy_nativeIsNetworkMetered },
};

jint register_android_net_ElNetworkPolicyManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/net/ElNetworkPolicyManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

