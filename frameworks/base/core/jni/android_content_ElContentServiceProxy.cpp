
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Accounts.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Database.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <elastos/utility/etl/HashMap.h>
#include <Elastos.CoreLibrary.Utility.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Content::IIContentService;
using Elastos::Droid::Content::IISyncStatusObserver;
using Elastos::Droid::Database::IIContentObserver;
using Elastos::Droid::JavaProxy::CIContentObserverNative;
using Elastos::Droid::JavaProxy::CISyncStatusObserver;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, AutoPtr<IIContentObserver> > sContentObserver;
static Mutex sContentObserverLock;
static HashMap<Int32, AutoPtr<IISyncStatusObserver> > sStatusChangeListener;
static Mutex sStatusChangeListenerLock;

static void android_content_ElContentServiceProxy_nativeRegisterContentObserver(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject juri, jboolean jnotifyForDescendants, jobject jobserver, jint jobserverHashCode, jint userHandle)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeRegisterContentObserver()");

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeRegisterContentObserver() ToElUri fail!");
        }
    }

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    AutoPtr<IIContentObserver> observer;
    if (jobserver != NULL) {
        jobject jInstance = env->NewGlobalRef(jobserver);
        CIContentObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIContentObserver**)&observer);
        Mutex::Autolock lock(sContentObserverLock);
        sContentObserver[jobserverHashCode] = observer;
    }

    IIContentService* csm = (IIContentService*)jproxy;
    ECode ec = csm->RegisterContentObserver(uri, (Boolean)jnotifyForDescendants, observer, (Int32)userHandle);

    // ALOGD("- android_content_ElContentServiceProxy_nativeRegisterContentObserver()");
}

static void android_content_ElContentServiceProxy_nativeUnregisterContentObserver(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jobserver)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeUnregisterContentObserver()");
    AutoPtr<IIContentObserver> observer;
    {
        Mutex::Autolock lock(sContentObserverLock);
        HashMap<Int32, AutoPtr<IIContentObserver> >::Iterator it = sContentObserver.Find(jobserver);
        if (it != sContentObserver.End()) {
            observer = it->mSecond;
            sContentObserver.Erase(it);
        }
    }
    if (NULL == observer) {
        ALOGE("nativeUnregisterReceiver() Invalid IIContentObserver!\n");
        return;
    }

    IIContentService* csm = (IIContentService*)jproxy;
    csm->UnregisterContentObserver(observer);

    // ALOGD("- android_content_ElContentServiceProxy_nativeUnregisterContentObserver()");
}

static void android_content_ElContentServiceProxy_nativeNotifyChange(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject juri, jint jobserver, jboolean jobserverWantsSelfNotifications, jboolean jsyncToNetwork, jint juserHandle)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeNotifyChange()");

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeNotifyChange() ToElUri() fail!");
        }
    }

    AutoPtr<IIContentObserver> observer;
    if (jobserver > 0) {
        Mutex::Autolock lock(sContentObserverLock);
        HashMap<Int32, AutoPtr<IIContentObserver> >::Iterator it = sContentObserver.Find(jobserver);
        if (it != sContentObserver.End()) {
            observer = it->mSecond;
        }
        if (NULL == observer) {
            ALOGE("nativeNotifyChange() Invalid IIContentObserver!\n");
            return;
        }
    }

    IIContentService* csm = (IIContentService*)jproxy;
    csm->NotifyChange(uri, observer, (Boolean)jobserverWantsSelfNotifications, (Boolean)jsyncToNetwork, (Int32)juserHandle);

    // ALOGD("- android_content_ElContentServiceProxy_nativeNotifyChange()");
}

static void android_content_ElContentServiceProxy_nativeRequestSync(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccount, jstring jauthority, jobject jextras)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeRequestSync()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeRequestSync() ToElAccount() fail!");
        }
    }

    String authority = ElUtil::ToElString(env, jauthority);

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("nativeRequestSync() ToElBundle() fail!");
        }
    }

    IIContentService* csm = (IIContentService*)jproxy;
    csm->RequestSync(account, authority, extras);

    // ALOGD("- android_content_ElContentServiceProxy_nativeRequestSync()");
}

static void android_content_ElContentServiceProxy_nativeSync(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrequest)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeSync()");

    AutoPtr<ISyncRequest> request;
    if (jrequest != NULL) {
        if (!ElUtil::ToElSyncRequest(env, jrequest, (ISyncRequest**)&request)) {
            ALOGE("nativeSync() ToElSyncRequest() fail!");
        }
    }

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->Sync(request);

    // ALOGD("- android_content_ElContentServiceProxy_nativeSync(), ec = %08x", ec);
}

static void android_content_ElContentServiceProxy_nativeSyncAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrequest, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeSyncAsUser()");

    AutoPtr<ISyncRequest> request;
    if (jrequest != NULL) {
        if (!ElUtil::ToElSyncRequest(env, jrequest, (ISyncRequest**)&request)) {
            ALOGE("nativeSyncAsUser() ToElSyncRequest() fail!");
        }
    }

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->SyncAsUser(request, userId);

    // ALOGD("- android_content_ElContentServiceProxy_nativeSyncAsUser(), ec = %08x", ec);
}

static void android_content_ElContentServiceProxy_nativeCancelSync(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jauthority, jobject jcname)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeCancelSync()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeCancelSync() ToElAccount() fail!");
        }
    }

    String authority = ElUtil::ToElString(env, jauthority);

    AutoPtr<IComponentName> cname;
    if (jcname != NULL) {
        if (!ElUtil::ToElComponentName(env, jcname, (IComponentName**)&cname)) {
            ALOGE("nativeCancelSync() ToElComponentName() fail!");
        }
    }

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->CancelSync(account, authority, cname);

    // ALOGD("- android_content_ElContentServiceProxy_nativeCancelSync(), ec = %08x", ec);
}

static void android_content_ElContentServiceProxy_nativeCancelSyncAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jauthority, jobject jcname, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeCancelSyncAsUser()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeCancelSyncAsUser() ToElAccount() fail!");
        }
    }

    String authority = ElUtil::ToElString(env, jauthority);

    AutoPtr<IComponentName> cname;
    if (jcname != NULL) {
        if (!ElUtil::ToElComponentName(env, jcname, (IComponentName**)&cname)) {
            ALOGE("nativeCancelSyncAsUser() ToElComponentName() fail!");
        }
    }

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->CancelSyncAsUser(account, authority, cname, userId);

    // ALOGD("- android_content_ElContentServiceProxy_nativeCancelSyncAsUser(), ec = %08x", ec);
}

static void android_content_ElContentServiceProxy_nativeCancelRequest(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jrequest)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeCancelRequest()");

    AutoPtr<ISyncRequest> request;
    if (jrequest != NULL) {
        if (!ElUtil::ToElSyncRequest(env, jrequest, (ISyncRequest**)&request)) {
            ALOGE("nativeCancelRequest() ToElSyncRequest() fail!");
        }
    }

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->CancelRequest(request);

    // ALOGD("- android_content_ElContentServiceProxy_nativeCancelRequest(), ec = %08x", ec);
}

static jboolean android_content_ElContentServiceProxy_nativeGetSyncAutomatically(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetSyncAutomatically()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeGetSyncAutomatically() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    Boolean result(FALSE);
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetSyncAutomatically(account, providerName, &result);

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetSyncAutomatically(), ec = %08x", ec);
    return result;
}

static jboolean android_content_ElContentServiceProxy_nativeGetSyncAutomaticallyAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetSyncAutomaticallyAsUser()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeGetSyncAutomaticallyAsUser() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    Boolean result(FALSE);
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetSyncAutomaticallyAsUser(account, providerName, userId, &result);

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetSyncAutomaticallyAsUser(), ec = %08x", ec);
    return result;
}

static void android_content_ElContentServiceProxy_nativeSetSyncAutomatically(JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName, jboolean jsync)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeSetSyncAutomatically()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeSetSyncAutomatically() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->SetSyncAutomatically(account, providerName, jsync);

    // ALOGD("- android_content_ElContentServiceProxy_nativeSetSyncAutomatically(), ec = %08x", ec);
}


static void android_content_ElContentServiceProxy_nativeSetSyncAutomaticallyAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName, jboolean jsync, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeSetSyncAutomaticallyAsUser()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeSetSyncAutomaticallyAsUser() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->SetSyncAutomaticallyAsUser(account, providerName, jsync, userId);

    // ALOGD("- android_content_ElContentServiceProxy_nativeSetSyncAutomaticallyAsUser(), ec = %08x", ec);
}

static jobject android_content_ElContentServiceProxy_nativeGetPeriodicSyncs(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName, jobject jcname)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetPeriodicSyncs()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeGetPeriodicSyncs() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    AutoPtr<IComponentName> cname;
    if (jcname != NULL) {
        if (!ElUtil::ToElComponentName(env, jcname, (IComponentName**)&cname)) {
            ALOGE("nativeGetPeriodicSyncs() ToElComponentName() fail!");
        }
    }

    AutoPtr<IList> listResult;
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetPeriodicSyncs(account, providerName, /*cname, */(IList**)&listResult);
    ALOGD("nativeGetPeriodicSyncs(), ec = %08x", ec);

    jobject jlist = NULL;

    if (listResult != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetPeriodicSyncs Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetPeriodicSyncs Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jlist = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetPeriodicSyncs Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetPeriodicSyncs Fail GetMethodID: add : %d!\n", __LINE__);

        env->DeleteLocalRef(listKlass);

        Int32 size;
        listResult->GetSize(&size);

        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            listResult->Get(i, (IInterface**)&obj);

            AutoPtr<IPeriodicSync> periodicSync = IPeriodicSync::Probe(obj);

            jobject jperiodicSync = NULL;
            if (periodicSync != NULL) {
                jperiodicSync = ElUtil::GetJavaPeriodicSync(env, periodicSync);
            }
            else {
                ALOGE("nativeGetPeriodicSyncs(); periodicSync is null!");
            }

            env->CallBooleanMethod(jlist, mAdd, jperiodicSync);
            ElUtil::CheckErrorAndLog(env, "nativeGetPeriodicSyncs Fail CallBooleanMethod: mAdd : %d!\n", __LINE__);
            env->DeleteLocalRef(jperiodicSync);
        }
    }

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetPeriodicSyncs()");
    return jlist;
}

static void android_content_ElContentServiceProxy_nativeAddPeriodicSync(JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName, jobject jextras, jlong jpollFrequency)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeAddPeriodicSync()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeAddPeriodicSync() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("nativeAddPeriodicSync() ToElBundle() fail!");
        }
    }

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->AddPeriodicSync(account, providerName, extras, (Int64)jpollFrequency);

    // ALOGD("- android_content_ElContentServiceProxy_nativeAddPeriodicSync(), ec = %08x", ec);
}

static void android_content_ElContentServiceProxy_nativeRemovePeriodicSync(JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName, jobject jextras)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeRemovePeriodicSync()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeRemovePeriodicSync() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("nativeRemovePeriodicSync() ToElBundle() fail!");
        }
    }

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->RemovePeriodicSync(account, providerName, extras);

    // ALOGD("- android_content_ElContentServiceProxy_nativeRemovePeriodicSync(), ec = %08x", ec);
}

static jint android_content_ElContentServiceProxy_nativeGetIsSyncable(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetIsSyncable()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeGetIsSyncable() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    Int32 result(0);

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetIsSyncable(account, providerName, &result);

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetIsSyncable(), ec = %08x", ec);
    return result;
}


static jint android_content_ElContentServiceProxy_nativeGetIsSyncableAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetIsSyncableAsUser()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeGetIsSyncableAsUser() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    Int32 result(0);

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetIsSyncableAsUser(account, providerName, userId, &result);

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetIsSyncableAsUser(), ec = %08x", ec);
    return result;
}
static void android_content_ElContentServiceProxy_nativeSetIsSyncable(JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jproviderName, jint jsyncable)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeSetIsSyncable()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeSetIsSyncable() ToElAccount() fail!");
        }
    }

    String providerName = ElUtil::ToElString(env, jproviderName);

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->SetIsSyncable(account, providerName, jsyncable);

    // ALOGD("- android_content_ElContentServiceProxy_nativeSetIsSyncable(), ec = %08x", ec);
}

static void android_content_ElContentServiceProxy_nativeSetMasterSyncAutomatically(JNIEnv* env, jobject clazz, jlong jproxy, jboolean jflag)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeSetMasterSyncAutomatically()");

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->SetMasterSyncAutomatically(jflag);

    // ALOGD("- android_content_ElContentServiceProxy_nativeSetMasterSyncAutomatically(), ec = %08x", ec);
}

static void android_content_ElContentServiceProxy_nativeSetMasterSyncAutomaticallyAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean jflag, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeSetMasterSyncAutomaticallyAsUser()");

    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->SetMasterSyncAutomaticallyAsUser(jflag, userId);

    // ALOGD("- android_content_ElContentServiceProxy_nativeSetMasterSyncAutomaticallyAsUser(), ec = %08x", ec);
}

static jboolean android_content_ElContentServiceProxy_nativeGetMasterSyncAutomatically(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetMasterSyncAutomatically()");

    Boolean result(FALSE);
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetMasterSyncAutomatically(&result);

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetMasterSyncAutomatically(), ec = %08x", ec);
    return result;
}

static jboolean android_content_ElContentServiceProxy_nativeGetMasterSyncAutomaticallyAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetMasterSyncAutomaticallyAsUser()");

    Boolean result(FALSE);
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetMasterSyncAutomaticallyAsUser(userId, &result);

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetMasterSyncAutomaticallyAsUser(), ec = %08x", ec);
    return result;
}

static jboolean android_content_ElContentServiceProxy_nativeIsSyncActive(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jauthority, jobject jcname)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeIsSyncActive()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeIsSyncActive() ToElAccount() fail!");
        }
    }

    String authority = ElUtil::ToElString(env, jauthority);

    AutoPtr<IComponentName> cname;
    if (jcname != NULL) {
        if (!ElUtil::ToElComponentName(env, jcname, (IComponentName**)&cname)) {
            ALOGE("nativeIsSyncActive() ToElComponentName() fail!");
        }
    }

    Boolean result(FALSE);
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->IsSyncActive(account, authority, cname, &result);

    // ALOGD("- android_content_ElContentServiceProxy_nativeIsSyncActive(), ec = %08x", ec);
    return result;
}

static jobject android_content_ElContentServiceProxy_nativeGetCurrentSyncs(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetCurrentSyncs()");

    AutoPtr<IList> listResult;
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetCurrentSyncs((IList**)&listResult);
    ALOGD("nativeGetCurrentSyncs(), ec = %08x", ec);

    jobject jlist = NULL;

    if (listResult != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncs Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncs Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jlist = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncs Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncs Fail GetMethodID: add : %d!\n", __LINE__);

        env->DeleteLocalRef(listKlass);

        Int32 size;
        listResult->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            listResult->Get(i, (IInterface**)&obj);

            AutoPtr<ISyncInfo> syncInfo = ISyncInfo::Probe(obj);

            jobject jsyncInfo = NULL;
            if (syncInfo != NULL) {
                jsyncInfo = ElUtil::GetJavaSyncInfo(env, syncInfo);
            }
            else {
                ALOGE("nativeGetCurrentSyncs(); syncInfo is null!");
            }

            env->CallBooleanMethod(jlist, mAdd, jsyncInfo);
            ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncs Fail CallBooleanMethod: mAdd : %d!\n", __LINE__);
            env->DeleteLocalRef(jsyncInfo);
        }
    }

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetCurrentSyncs()");
    return jlist;
}

static jobject android_content_ElContentServiceProxy_nativeGetCurrentSyncsAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetCurrentSyncsAsUser()");

    AutoPtr<IList> listResult;
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetCurrentSyncsAsUser(userId, (IList**)&listResult);
    ALOGD("nativeGetCurrentSyncsAsUser(), ec = %08x", ec);

    jobject jlist = NULL;

    if (listResult != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncsAsUser Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncsAsUser Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jlist = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncsAsUser Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncsAsUser Fail GetMethodID: add : %d!\n", __LINE__);

        env->DeleteLocalRef(listKlass);

        Int32 size;
        listResult->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            listResult->Get(i, (IInterface**)&obj);

            AutoPtr<ISyncInfo> syncInfo = ISyncInfo::Probe(obj);

            jobject jsyncInfo = NULL;
            if (syncInfo != NULL) {
                jsyncInfo = ElUtil::GetJavaSyncInfo(env, syncInfo);
            }
            else {
                ALOGE("nativeGetCurrentSyncsAsUser(); syncInfo is null!");
            }

            env->CallBooleanMethod(jlist, mAdd, jsyncInfo);
            ElUtil::CheckErrorAndLog(env, "nativeGetCurrentSyncsAsUser Fail CallBooleanMethod: mAdd : %d!\n", __LINE__);
            env->DeleteLocalRef(jsyncInfo);
        }
    }

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetCurrentSyncsAsUser()");
    return jlist;
}

static jobjectArray android_content_ElContentServiceProxy_nativeGetSyncAdapterTypes(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetSyncAdapterTypes()");

    AutoPtr<ArrayOf<ISyncAdapterType*> > result;
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetSyncAdapterTypes((ArrayOf<ISyncAdapterType*>**)&result);
    ALOGD("nativeGetSyncAdapterTypes(), ec = %08x", ec);

    if (result == NULL) {
        ALOGD("nativeGetSyncAdapterTypes(), result is NULL");
        return NULL;
    }

    Int32 count = result->GetLength();

    jclass syncAdapterTypeKlass = env->FindClass("android/content/SyncAdapterType");
    ElUtil::CheckErrorAndLog(env, "FindClass: SyncAdapterType failed: %d!\n", __LINE__);

    jobjectArray jtypesArray = env->NewObjectArray((jsize)count, syncAdapterTypeKlass, 0);
    ElUtil::CheckErrorAndLog(env, "NewObjectArray: SyncAdapterType failed: %d!\n", __LINE__);
    env->DeleteLocalRef(syncAdapterTypeKlass);

    for(Int32 i = 0; i < count; i++ ) {
        jobject jsyncAdapterType = ElUtil::GetJavaSyncAdapterType(env, (*result)[i]);
        if (jsyncAdapterType != NULL) {
            env->SetObjectArrayElement(jtypesArray, i, jsyncAdapterType);
            env->DeleteLocalRef(jsyncAdapterType);
        }
    }

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetSyncAdapterTypes()");
    return jtypesArray;
}


static jobjectArray android_content_ElContentServiceProxy_nativeGetSyncAdapterTypesAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetSyncAdapterTypesAsUser()");

    AutoPtr<ArrayOf<ISyncAdapterType*> > result;
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetSyncAdapterTypesAsUser(userId, (ArrayOf<ISyncAdapterType*>**)&result);
    ALOGD("nativeGetSyncAdapterTypesAsUser(), ec = %08x", ec);

    if (result == NULL) {
        ALOGD("nativeGetSyncAdapterTypesAsUser(), result is NULL");
        return NULL;
    }

    Int32 count = result->GetLength();

    jclass syncAdapterTypeKlass = env->FindClass("android/content/SyncAdapterType");
    ElUtil::CheckErrorAndLog(env, "FindClass: SyncAdapterType failed: %d!\n", __LINE__);

    jobjectArray jtypesArray = env->NewObjectArray((jsize)count, syncAdapterTypeKlass, 0);
    ElUtil::CheckErrorAndLog(env, "NewObjectArray: SyncAdapterType failed: %d!\n", __LINE__);
    env->DeleteLocalRef(syncAdapterTypeKlass);

    for(Int32 i = 0; i < count; i++ ) {
        jobject jsyncAdapterType = ElUtil::GetJavaSyncAdapterType(env, (*result)[i]);
        if (jsyncAdapterType != NULL) {
            env->SetObjectArrayElement(jtypesArray, i, jsyncAdapterType);
            env->DeleteLocalRef(jsyncAdapterType);
        }
    }

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetSyncAdapterTypesAsUser()");
    return jtypesArray;
}

static jobject android_content_ElContentServiceProxy_nativeGetSyncStatus(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jauthority, jobject jcname)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetSyncStatus()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeGetSyncStatus() ToElAccount() fail!");
        }
    }

    String authority = ElUtil::ToElString(env, jauthority);

    AutoPtr<IComponentName> cname;
    if (jcname != NULL) {
        if (!ElUtil::ToElComponentName(env, jcname, (IComponentName**)&cname)) {
            ALOGE("nativeGetSyncStatus() ToElComponentName() fail!");
        }
    }

    AutoPtr<ISyncStatusInfo> statusInfo;
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetSyncStatus(account, authority, cname, (ISyncStatusInfo**)&statusInfo);
    ALOGD("nativeGetSyncStatus(), ec = %08x", ec);

    jobject jstatusInfo = NULL;

    if (statusInfo == NULL) {
        ALOGW("nativeGetSyncStatus(), result is null");
    }
    else {
        jstatusInfo = ElUtil::GetJavaSyncStatusInfo(env, statusInfo);
    }

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetSyncStatus()");
    return jstatusInfo;
}

static jobject android_content_ElContentServiceProxy_nativeGetSyncStatusAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jauthority, jobject jcname, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeGetSyncStatusAsUser()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeGetSyncStatusAsUser() ToElAccount() fail!");
        }
    }

    String authority = ElUtil::ToElString(env, jauthority);

    AutoPtr<IComponentName> cname;
    if (jcname != NULL) {
        if (!ElUtil::ToElComponentName(env, jcname, (IComponentName**)&cname)) {
            ALOGE("nativeGetSyncStatusAsUser() ToElComponentName() fail!");
        }
    }

    AutoPtr<ISyncStatusInfo> statusInfo;
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->GetSyncStatusAsUser(account, authority, cname, userId, (ISyncStatusInfo**)&statusInfo);
    ALOGD("nativeGetSyncStatusAsUser(), ec = %08x", ec);

    jobject jstatusInfo = NULL;

    if (statusInfo == NULL) {
        ALOGW("nativeGetSyncStatusAsUser(), result is null");
    }
    else {
        jstatusInfo = ElUtil::GetJavaSyncStatusInfo(env, statusInfo);
    }

    // ALOGD("- android_content_ElContentServiceProxy_nativeGetSyncStatusAsUser()");
    return jstatusInfo;
}

static jboolean android_content_ElContentServiceProxy_nativeIsSyncPending(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jauthority, jobject jcname)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeIsSyncPending()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeIsSyncPending() ToElAccount() fail!");
        }
    }

    String authority = ElUtil::ToElString(env, jauthority);

    AutoPtr<IComponentName> cname;
    if (jcname != NULL) {
        if (!ElUtil::ToElComponentName(env, jcname, (IComponentName**)&cname)) {
            ALOGE("nativeIsSyncPending() ToElComponentName() fail!");
        }
    }

    Boolean result(FALSE);
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->IsSyncPending(account, authority, cname, &result);

    // ALOGD("- android_content_ElContentServiceProxy_nativeIsSyncPending(), ec = %08x", ec);
    return result;
}

static jboolean android_content_ElContentServiceProxy_nativeIsSyncPendingAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount, jstring jauthority, jobject jcname, jint userId)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeIsSyncPendingAsUser()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if (!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeIsSyncPendingAsUser() ToElAccount() fail!");
        }
    }

    String authority = ElUtil::ToElString(env, jauthority);

    AutoPtr<IComponentName> cname;
    if (jcname != NULL) {
        if (!ElUtil::ToElComponentName(env, jcname, (IComponentName**)&cname)) {
            ALOGE("nativeIsSyncPendingAsUser() ToElComponentName() fail!");
        }
    }

    Boolean result(FALSE);
    IIContentService* cs = (IIContentService*)jproxy;
    ECode ec = cs->IsSyncPendingAsUser(account, authority, cname, userId, &result);

    // ALOGD("- android_content_ElContentServiceProxy_nativeIsSyncPendingAsUser(), ec = %08x", ec);
    return result;
}

static void android_content_ElContentServiceProxy_nativeAddStatusChangeListener(JNIEnv* env, jobject clazz, jlong jproxy, jint jmask, jobject jcallback)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeAddStatusChangeListener()");

    AutoPtr<IISyncStatusObserver> callback;

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    if (jcallback != NULL) {
        jobject jInstance = env->NewGlobalRef(jcallback);
        CISyncStatusObserver::New((Handle64)jvm, (Handle64)jInstance, (IISyncStatusObserver**)&callback);

        Int32 callBackHashCode = ElUtil::GetJavaHashCode(env, jcallback);
        Mutex::Autolock lock(sStatusChangeListenerLock);
        sStatusChangeListener[callBackHashCode] = callback;
    }

    IIContentService* csm = (IIContentService*)jproxy;
    ECode ec = csm->AddStatusChangeListener(jmask, callback);

    // ALOGD("- android_content_ElContentServiceProxy_nativeAddStatusChangeListener()");
}

static void android_content_ElContentServiceProxy_nativeRemoveStatusChangeListener(JNIEnv* env, jobject clazz, jlong jproxy, jobject jcallback)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeRemoveStatusChangeListener()");

    Int32 callBackHashCode = ElUtil::GetJavaHashCode(env, jcallback);

    AutoPtr<IISyncStatusObserver> callback;
    {
        Mutex::Autolock lock(sStatusChangeListenerLock);
        HashMap<Int32, AutoPtr<IISyncStatusObserver> >::Iterator it = sStatusChangeListener.Find(callBackHashCode);
        if (it != sStatusChangeListener.End()) {
            callback = it->mSecond;
            sStatusChangeListener.Erase(it);
        }
        else {
            ALOGE("nativeRemoveStatusChangeListener() Invalid IISyncStatusObserver!\n");
            return;
        }
    }

    IIContentService* cs = (IIContentService*)jproxy;
    cs->RemoveStatusChangeListener(callback);

    // ALOGD("- android_content_ElContentServiceProxy_nativeRemoveStatusChangeListener()");
}

static void android_content_ElContentServiceProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_content_ElContentServiceProxy_nativeDestroy()");

    IIContentService* obj = (IIContentService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_content_ElContentServiceProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_content_ElContentServiceProxy_nativeFinalize },
    { "nativeRegisterContentObserver",    "(JLandroid/net/Uri;ZLandroid/database/IContentObserver;II)V",
            (void*) android_content_ElContentServiceProxy_nativeRegisterContentObserver },
    { "nativeUnregisterContentObserver",    "(JI)V",
            (void*) android_content_ElContentServiceProxy_nativeUnregisterContentObserver },
    { "nativeNotifyChange",    "(JLandroid/net/Uri;IZZI)V",
            (void*) android_content_ElContentServiceProxy_nativeNotifyChange },
    { "nativeRequestSync",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_content_ElContentServiceProxy_nativeRequestSync },
    { "nativeSync",    "(JLandroid/content/SyncRequest;)V",
            (void*) android_content_ElContentServiceProxy_nativeSync },
    { "nativeSyncAsUser",    "(JLandroid/content/SyncRequest;I)V",
            (void*) android_content_ElContentServiceProxy_nativeSyncAsUser },
    { "nativeCancelSync",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/content/ComponentName;)V",
            (void*) android_content_ElContentServiceProxy_nativeCancelSync },
    { "nativeCancelSyncAsUser",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/content/ComponentName;I)V",
            (void*) android_content_ElContentServiceProxy_nativeCancelSyncAsUser },
    { "nativeCancelRequest",    "(JLandroid/content/SyncRequest;)V",
            (void*) android_content_ElContentServiceProxy_nativeCancelRequest },
    { "nativeGetSyncAutomatically",    "(JLandroid/accounts/Account;Ljava/lang/String;)Z",
            (void*) android_content_ElContentServiceProxy_nativeGetSyncAutomatically },
    { "nativeGetSyncAutomaticallyAsUser",    "(JLandroid/accounts/Account;Ljava/lang/String;I)Z",
            (void*) android_content_ElContentServiceProxy_nativeGetSyncAutomaticallyAsUser },
    { "nativeSetSyncAutomatically",    "(JLandroid/accounts/Account;Ljava/lang/String;Z)V",
            (void*) android_content_ElContentServiceProxy_nativeSetSyncAutomatically },
    { "nativeSetSyncAutomaticallyAsUser",    "(JLandroid/accounts/Account;Ljava/lang/String;ZI)V",
            (void*) android_content_ElContentServiceProxy_nativeSetSyncAutomaticallyAsUser },
    { "nativeGetPeriodicSyncs",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/content/ComponentName;)Ljava/util/List;",
            (void*) android_content_ElContentServiceProxy_nativeGetPeriodicSyncs },
    { "nativeAddPeriodicSync",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V",
            (void*) android_content_ElContentServiceProxy_nativeAddPeriodicSync },
    { "nativeRemovePeriodicSync",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_content_ElContentServiceProxy_nativeRemovePeriodicSync },
    { "nativeGetIsSyncable",    "(JLandroid/accounts/Account;Ljava/lang/String;)I",
            (void*) android_content_ElContentServiceProxy_nativeGetIsSyncable },
    { "nativeGetIsSyncableAsUser",    "(JLandroid/accounts/Account;Ljava/lang/String;I)I",
        (void*) android_content_ElContentServiceProxy_nativeGetIsSyncableAsUser },
    { "nativeSetIsSyncable",    "(JLandroid/accounts/Account;Ljava/lang/String;I)V",
            (void*) android_content_ElContentServiceProxy_nativeSetIsSyncable },
    { "nativeSetMasterSyncAutomatically",    "(JZ)V",
            (void*) android_content_ElContentServiceProxy_nativeSetMasterSyncAutomatically },
    { "nativeSetMasterSyncAutomaticallyAsUser",    "(JZI)V",
            (void*) android_content_ElContentServiceProxy_nativeSetMasterSyncAutomaticallyAsUser },
    { "nativeGetMasterSyncAutomatically",    "(J)Z",
            (void*) android_content_ElContentServiceProxy_nativeGetMasterSyncAutomatically },
    { "nativeGetMasterSyncAutomaticallyAsUser",    "(JI)Z",
            (void*) android_content_ElContentServiceProxy_nativeGetMasterSyncAutomaticallyAsUser },
    { "nativeIsSyncActive",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/content/ComponentName;)Z",
            (void*) android_content_ElContentServiceProxy_nativeIsSyncActive },
    { "nativeGetCurrentSyncs",    "(J)Ljava/util/List;",
            (void*) android_content_ElContentServiceProxy_nativeGetCurrentSyncs },
    { "nativeGetCurrentSyncsAsUser",    "(JI)Ljava/util/List;",
            (void*) android_content_ElContentServiceProxy_nativeGetCurrentSyncsAsUser },
    { "nativeGetSyncAdapterTypes",    "(J)[Landroid/content/SyncAdapterType;",
            (void*) android_content_ElContentServiceProxy_nativeGetSyncAdapterTypes },
    { "nativeGetSyncAdapterTypesAsUser",    "(JI)[Landroid/content/SyncAdapterType;",
            (void*) android_content_ElContentServiceProxy_nativeGetSyncAdapterTypesAsUser },
    { "nativeGetSyncStatus",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/content/ComponentName;)Landroid/content/SyncStatusInfo;",
            (void*) android_content_ElContentServiceProxy_nativeGetSyncStatus },
    { "nativeGetSyncStatusAsUser",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/content/ComponentName;I)Landroid/content/SyncStatusInfo;",
            (void*) android_content_ElContentServiceProxy_nativeGetSyncStatusAsUser },
    { "nativeIsSyncPending",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/content/ComponentName;)Z",
            (void*) android_content_ElContentServiceProxy_nativeIsSyncPending },
    { "nativeIsSyncPendingAsUser",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/content/ComponentName;I)Z",
            (void*) android_content_ElContentServiceProxy_nativeIsSyncPendingAsUser },
    { "nativeAddStatusChangeListener",    "(JILandroid/content/ISyncStatusObserver;)V",
            (void*) android_content_ElContentServiceProxy_nativeAddStatusChangeListener },
    { "nativeRemoveStatusChangeListener",    "(JLandroid/content/ISyncStatusObserver;)V",
            (void*) android_content_ElContentServiceProxy_nativeRemoveStatusChangeListener },
};

int register_android_content_ElContentServiceProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/content/ElContentServiceProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

