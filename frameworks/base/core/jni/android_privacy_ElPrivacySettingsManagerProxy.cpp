
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Privacy::IPrivacySettings;
using Elastos::Droid::Privacy::IIPrivacySettingsManager;

static jobject android_privacy_ElPrivacySettingsManagerProxy_nativeGetSettings(JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativeGetSettings()");
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IPrivacySettings> prSettings;
    psm->GetSettings(packageName, (IPrivacySettings**)&prSettings);
    jobject jprSettings = NULL;
    if (prSettings != NULL) {
        jprSettings = ElUtil::GetJavaPrivacySettings(env, prSettings);
    }

    // ALOGD("- android_privacy_ElPrivacySettingsManagerProxy_nativeGetSettings()");
    return jprSettings;
}

static jboolean android_privacy_ElPrivacySettingsManagerProxy_nativeSaveSettings(JNIEnv* env, jobject clazz, jlong jproxy, jobject jsettings)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativeSaveSettings()");
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;
    AutoPtr<IPrivacySettings> prSettings;
    ElUtil::ToElPrivacySettings(env, jsettings, (IPrivacySettings**)&prSettings);
    Boolean result = FALSE;
    psm->SaveSettings(prSettings, &result);

    return result;
}

static jboolean android_privacy_ElPrivacySettingsManagerProxy_nativeDeleteSettings(JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativeDeleteSettings()");
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;
    String packageName = ElUtil::ToElString(env, jpackageName);
    Boolean result = FALSE;
    psm->DeleteSettings(packageName, &result);

    return result;
}

static void android_privacy_ElPrivacySettingsManagerProxy_nativeRegisterObservers(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativeRegisterObserver()");
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;
    psm->RegisterObservers();
}

static void android_privacy_ElPrivacySettingsManagerProxy_nativeAddObserver(JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativeAddObserver()");
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;
    String packageName = ElUtil::ToElString(env, jpackageName);
    psm->AddObserver(packageName);
}

static jboolean android_privacy_ElPrivacySettingsManagerProxy_nativePurgeSettings(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativePurgeSettings()");
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;
    Boolean result = FALSE;
    psm->PurgeSettings(&result);

    return result;
}

static jboolean android_privacy_ElPrivacySettingsManagerProxy_nativeSetEnabled(JNIEnv* env, jobject clazz, jlong jproxy, jboolean enable)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativeSetEnabled()");
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;
    Boolean result = FALSE;
    psm->SetEnabled(enable, &result);

    return result;
}

static void android_privacy_ElPrivacySettingsManagerProxy_nativeSetBootCompleted(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativeSetBootCompleted()");
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;
    psm->SetBootCompleted();
}

static void android_privacy_ElPrivacySettingsManagerProxy_nativeNotification(JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName,
        jbyte jaccessMode, jstring jdataType, jstring joutput)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativeNotification()");
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;

    String packageName = ElUtil::ToElString(env, jpackageName);
    String dataType = ElUtil::ToElString(env, jdataType);
    String output = ElUtil::ToElString(env, joutput);

    AutoPtr<IPrivacySettings> prSettings;
    psm->Notification(packageName, (Byte)jaccessMode, dataType, output);

    // ALOGD("- android_privacy_ElPrivacySettingsManagerProxy_nativeNotification()");
}

static jboolean android_privacy_ElPrivacySettingsManagerProxy_nativesetNotificationsEnabled(JNIEnv* env, jobject clazz, jlong jproxy, jboolean enable)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativesetNotificationsEnabled(), enable = %d", enable);
    IIPrivacySettingsManager* psm = (IIPrivacySettingsManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = psm->SetNotificationsEnabled(enable, &result);
    ALOGE("native SetNotificationsEnabled() ec = %08x", ec);

    // ALOGD("- android_privacy_ElPrivacySettingsManagerProxy_nativesetNotificationsEnabled(), return %d", result);
    return result;
}

static void android_privacy_ElPrivacySettingsManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_privacy_ElPrivacySettingsManagerProxy_nativeDestroy()");

    IIPrivacySettingsManager* obj = (IIPrivacySettingsManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_privacy_ElPrivacySettingsManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_privacy_ElPrivacySettingsManagerProxy_nativeFinalize },
    { "nativeGetSettings", "(JLjava/lang/String;)Landroid/privacy/PrivacySettings;",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativeGetSettings },

    { "nativeSaveSettings", "(JLandroid/privacy/PrivacySettings;)Z",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativeSaveSettings },

    { "nativeDeleteSettings", "(JLjava/lang/String;)Z",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativeDeleteSettings },

    { "nativeRegisterObservers", "(J)V",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativeRegisterObservers },

    { "nativeAddObserver", "(JLjava/lang/String;)V",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativeAddObserver },

    { "nativePurgeSettings", "(J)Z",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativePurgeSettings },

    { "nativeSetEnabled", "(JZ)Z",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativeSetEnabled },

    { "nativeSetBootCompleted", "(J)V",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativeSetBootCompleted },

    { "nativeNotification", "(JLjava/lang/String;BLjava/lang/String;Ljava/lang/String;)V",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativeNotification },

    { "nativesetNotificationsEnabled", "(JZ)Z",
        (void*) android_privacy_ElPrivacySettingsManagerProxy_nativesetNotificationsEnabled }
};



int register_android_privacy_ElPrivacySettingsManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/privacy/ElPrivacySettingsManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

