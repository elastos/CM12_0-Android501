#define LOG_TAG "ElActivityManagerProxyJNI"

#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <elastos/utility/etl/HashMap.h>
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Graphics.h>
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Service.h>
#include <Elastos.Droid.Utility.h>
#include <Elastos.Droid.View.h>
#include <Elastos.Droid.Widget.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Core.h>
#include <Elastos.CoreLibrary.Utility.h>

#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;

using Elastos::Core::ICharSequence;
using Elastos::Core::CString;
using Elastos::Droid::App::CActivityManagerRunningAppProcessInfo;
using Elastos::Droid::App::IActivityManagerRecentTaskInfo;
using Elastos::Droid::App::IApplicationErrorReportCrashInfo;
using Elastos::Droid::App::IApplicationThread;
using Elastos::Droid::App::IIActivityController;
using Elastos::Droid::App::IActivityContainerCallback;
using Elastos::Droid::App::IActivityManagerMemoryInfo;
using Elastos::Droid::App::IActivityManagerProcessErrorStateInfo;
using Elastos::Droid::App::IActivityManagerRunningAppProcessInfo;
using Elastos::Droid::App::IActivityManagerTaskThumbnail;
using Elastos::Droid::App::IActivityManagerWaitResult;
using Elastos::Droid::App::IContentProviderHolder;
using Elastos::Droid::App::IIActivityManager;
using Elastos::Droid::App::IIServiceConnection;
using Elastos::Droid::App::IInstrumentationWatcher;
using Elastos::Droid::App::INotification;
using Elastos::Droid::App::IIProcessObserver;
using Elastos::Droid::App::IStopUserCallback;
using Elastos::Droid::App::IIUiAutomationConnection;
using Elastos::Droid::App::IIUserSwitchObserver;
using Elastos::Droid::Content::IIntentFilter;
using Elastos::Droid::Content::IIntentReceiver;
using Elastos::Droid::Content::IIntentSender;
using Elastos::Droid::Content::IIIntentSender;
using Elastos::Droid::Content::ECLSID_CUriPermission;
using Elastos::Droid::Content::Pm::IIPackageDataObserver;
using Elastos::Droid::Content::Pm::IUserInfo;
using Elastos::Droid::Graphics::IBitmap;
using Elastos::Droid::JavaProxy::CApplicationThreadNative;
using Elastos::Droid::JavaProxy::CIActivityControllerNative;
using Elastos::Droid::JavaProxy::CIActivityContainerCallbackNative;
using Elastos::Droid::JavaProxy::CIBackupAgentNative;
using Elastos::Droid::JavaProxy::CIInstrumentationWatcherNative;
using Elastos::Droid::JavaProxy::CIILocationProviderNative;
using Elastos::Droid::JavaProxy::CIIWallpaperServiceNative;
using Elastos::Droid::JavaProxy::CIPackageDataObserverNative;
using Elastos::Droid::JavaProxy::CIProcessObserverNative;
using Elastos::Droid::JavaProxy::CIStopUserCallbackNative;
using Elastos::Droid::JavaProxy::CIUserSwitchObserverNative;
using Elastos::Droid::JavaProxy::CInputMethodServiceNative;
using Elastos::Droid::JavaProxy::CIntentReceiverNative;
using Elastos::Droid::JavaProxy::CLocalActivityRecordNative;
using Elastos::Droid::JavaProxy::CServiceNative;
using Elastos::Droid::JavaProxy::CMediaContainerServiceNative;
using Elastos::Droid::JavaProxy::CRemoteViewsFactoryNative;
using Elastos::Droid::JavaProxy::CIVoiceInteractorNative;
using Elastos::Droid::JavaProxy::CIVoiceInteractionSessionNative;
using Elastos::Droid::JavaProxy::CIUiAutomationConnectionNative;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Os::IDebugMemoryInfo;
using Elastos::Droid::Os::IParcelFileDescriptor;
using Elastos::Droid::Os::IStrictModeViolationInfo;
using Elastos::Droid::Service::Voice::IIVoiceInteractionSession;
using Elastos::Droid::View::IIWindow;
using Elastos::Droid::Internal::App::IIVoiceInteractor;
using Elastos::Droid::Internal::Location::IILocationProvider;
using Elastos::Droid::Internal::Widget::IIRemoteViewsFactory;
using Elastos::Droid::Utility::CParcelableList;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, AutoPtr<IApplicationThread> > sAppThread;
static Mutex sAppThreadLock;
static HashMap<Int32, AutoPtr<IIntentReceiver> > sIntentReceiver;
static Mutex sIntentReceiverLock;
static HashMap<Int32, AutoPtr<IIServiceConnection> > sISerConnection;
static Mutex sISerConnectionLock;
static HashMap<Int32, AutoPtr<IIProcessObserver> > sProcessObserver;
static Mutex sProcessObserverLock;
static HashMap<Int32, AutoPtr<IIUserSwitchObserver> > sUserSwitchObserver;
static Mutex sUserSwitchObserverLock;

static HashMap<Int32, AutoPtr<IBinder> > slarMap;
static Mutex slarMapLock;

static HashMap<Int32, AutoPtr<IIVoiceInteractionSession> > sVoiceSessions;
static Mutex sVoiceSessionsLock;

static int android_app_ElActivityManagerProxy_nativeStartActivity(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcaller, jstring jcallingPackage, jobject jintent,
    jstring jresolvedType, jobject jresultTo, jstring jresultWho,
    jint jrequestCode, jint jstartFlags, jobject jprofilerInfo, jobject joptions)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartActivity()");

    jint jcallerHashcode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(jcallerHashcode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeStartActivity() ToElIntent fail!\n");
        }
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeStartActivity() ToElBundle fail!\n");
        }
    }

    AutoPtr<IProfilerInfo> profilerInfo;
    if (jprofilerInfo != NULL) {
        if (!ElUtil::ToElProfilerInfo(env, jprofilerInfo, (IProfilerInfo**)&profilerInfo)) {
            ALOGE("nativeStartActivity() ToElProfilerInfo fail!\n");
        }
    }

    String resolvedType;
    if (jresolvedType != NULL) {
        resolvedType = ElUtil::ToElString(env, jresolvedType);
    }

    String resultWho;
    if (jresultWho != NULL) {
        resultWho = ElUtil::ToElString(env, jresultWho);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    AutoPtr<IBinder> token;
    if (jresultTo != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);

        if (env->IsInstanceOf(jresultTo, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jresultTo, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else if (env->IsInstanceOf(jresultTo, larWClass)) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jresultTo);

            AutoPtr<IBinder> resultTo;
            if (NOERROR == CLocalActivityRecordNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&resultTo)) {
                jclass objectClass = env->FindClass("java/lang/Object");
                ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

                jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
                ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);
                env->DeleteLocalRef(objectClass);

                jint jresultToHashCode = env->CallIntMethod(jresultTo, m);
                ElUtil::CheckErrorAndLog(env, "CallIntMethod hashCode : %d!\n", __LINE__);

                Mutex::Autolock lock(slarMapLock);
                slarMap[jresultToHashCode] = resultTo;
            }
            else {
                ALOGE("android_app_ElActivityManagerProxy_nativeStartActivity() new CLocalActivityRecordNative fail ");
            }
        }
        else {
            ALOGE("nativeStartActivity() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(larWClass);
    }

    Int32 result = 0;
    am->StartActivity(appThread, callingPackage, intent, resolvedType, token,
        resultWho, (Int32)jrequestCode, (Int32)jstartFlags, profilerInfo, options, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartActivity()");
    return result;
}

static jint android_app_ElActivityManagerProxy_nativeStartActivityIntentSender(JNIEnv* env, jobject clazz, jlong nativeProxy, /*IApplicationThread*/jobject jcaller,
            /*IntentSender*/jobject jintentSender, /*Intent*/jobject jfillInIntent, jstring jresolvedType,
            /*IBinder*/jobject jresultTo, jstring jresultWho, jint jrequestCode,
            jint jflagsMask, jint jflagsValues, /*Bundle*/jobject joptions){
    // ALOGD("+ android_app_nativeStartActivityIntentSender()");
    jint jcallerHashcode = ElUtil::GetJavaHashCode(env, jcaller);
    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(jcallerHashcode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IIntentSender> intentSender;
    if (jintentSender != NULL) {
        if (!ElUtil::ToElIntentSender(env, jintentSender, (IIntentSender**)&intentSender)) {
            ALOGE("nativeStartActivity() ToElIntentSender fail!\n");
        }
    }

    AutoPtr<IIntent> fillInIntent;
    if (jfillInIntent != NULL) {
        if (!ElUtil::ToElIntent(env, jfillInIntent, (IIntent**)&fillInIntent)) {
            ALOGE("nativeStartActivity() ToElIntent fail!\n");
        }
    }

    String resolvedType;
    if (jresolvedType != NULL) {
        resolvedType = ElUtil::ToElString(env, jresolvedType);
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeStartActivity() ToElBundle fail!\n");
        }
    }

    String resultWho;
    if (jresultWho != NULL) {
        resultWho = ElUtil::ToElString(env, jresultWho);
    }

    IIActivityManager* am = (IIActivityManager*)nativeProxy;

    AutoPtr<IBinder> token = NULL;
    if (jresultTo != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jresultTo, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jresultTo, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else if (env->IsInstanceOf(jresultTo, larWClass)) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jresultTo);

            AutoPtr<IBinder> resultTo;
            if (NOERROR == CLocalActivityRecordNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&resultTo)) {
                jclass objectClass = env->FindClass("java/lang/Object");
                ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

                jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
                ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);
                env->DeleteLocalRef(objectClass);

                jint jresultToHashCode = env->CallIntMethod(jresultTo, m);
                ElUtil::CheckErrorAndLog(env, "CallIntMethod hashCode : %d!\n", __LINE__);

                Mutex::Autolock lock(slarMapLock);
                slarMap[jresultToHashCode] = resultTo;
            }
            else {
                ALOGE("android_app_ElActivityManagerProxy_nativeStartActivity() new CLocalActivityRecordNative fail ");
            }
        }
        else {
            ALOGE("nativeStartActivity() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(larWClass);
    }

    Int32 result = 0;
    ECode ec = am->StartActivityIntentSender(
        appThread, intentSender.Get(), fillInIntent.Get(), resolvedType, token.Get(), resultWho, (Int32)jrequestCode, (Int32)jflagsMask, (Int32)jflagsValues, options, &result);
    ALOGD("android_app_nativeStartActivityIntentSender(), am->StartActivityIntentSender(), ec = %08x", ec);

    // ALOGD("- android_app_nativeStartActivityIntentSender()");
    return result;
}

static int android_app_ElActivityManagerProxy_nativeStartVoiceActivity(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jcallingPid,
    jint jcallingUid, jobject jintent, jstring jresolvedType, jobject jsession, jobject jinteractor,
    jint jstartFlags, jobject jprofilerInfo, jobject joptions, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartVoiceActivity()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeStartVoiceActivity() ToElIntent fail!\n");
        }
    }

    String resolvedType;
    if (jresolvedType != NULL) {
        resolvedType = ElUtil::ToElString(env, jresolvedType);
    }

    AutoPtr<IProfilerInfo> profilerInfo;
    if (jprofilerInfo != NULL) {
        if (!ElUtil::ToElProfilerInfo(env, jprofilerInfo, (IProfilerInfo**)&profilerInfo)) {
            ALOGE("nativeStartVoiceActivity() ToElProfilerInfo fail!\n");
        }
    }

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);

    AutoPtr<IIVoiceInteractionSession> session;
    if (jsession != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jsession);

        jobject jInstance = env->NewGlobalRef(jsession);
        CIVoiceInteractionSessionNative::New((Handle64)jvm, (Handle64)jInstance, (IIVoiceInteractionSession**)&session);
        Mutex::Autolock lock(sVoiceSessionsLock);
        sVoiceSessions[hashCode] = session;
    }

    AutoPtr<IIVoiceInteractor> interactor;
    if (jinteractor != NULL) {
        jobject jInstance = env->NewGlobalRef(jinteractor);
        CIVoiceInteractorNative::New((Handle64)jvm, (Handle64)jInstance, (IIVoiceInteractor**)&interactor);
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeStartVoiceActivity() ToElBundle fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    Int32 result = 0;
    am->StartVoiceActivity(callingPackage, jcallingPid, jcallingUid, intent, resolvedType, session,
        interactor, jstartFlags, profilerInfo, options, juserId, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartVoiceActivity()");
    return result;
}

static void android_app_ElActivityManagerProxy_nativeAttachApplication(JNIEnv* env, jobject clazz, jlong jproxy, jobject japp, jint jappHashCode)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeAttachApplication()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(japp);

    AutoPtr<IApplicationThread> appThread;
    ECode ec = CApplicationThreadNative::New((Handle64)jvm, (Handle64)jInstance, (IApplicationThread**)&appThread);
    if (FAILED(ec)) {
        ALOGE("nativeAttachApplication ec: 0x%08x", ec);
    }

    {
        Mutex::Autolock lock(sAppThreadLock);
        sAppThread[jappHashCode] = appThread;
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->AttachApplication(appThread);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeAttachApplication()");
}

static void android_app_ElActivityManagerProxy_nativeActivityPaused(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeActivityPaused()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeStartActivity() what is this token ?");
            assert(0);
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->ActivityPaused(token);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeActivityPaused()");
}

static int android_app_ElActivityManagerProxy_nativeStartActivityFromRecents(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jtaskId, jobject joptions)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartActivityFromRecents()");

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options))
            ALOGE("nativeStartActivityFromRecents: ToElBundle fail");
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    Int32 result;
    am->StartActivityFromRecents(jtaskId, options, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartActivityFromRecents()");
    return result;
}

static bool android_app_ElActivityManagerProxy_nativeFinishActivity(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jint jresultCode, jobject jresultData, jboolean jfinishTask)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeFinishActivity()");

    AutoPtr<IBinder> token;
    AutoPtr<IBinder> ltoken;
    jint jtokenHashCode = 0;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }  else if (env->IsInstanceOf(jtoken, larWClass)) {
            jclass objectClass = env->FindClass("java/lang/Object");
            ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

            jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
            ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);
            env->DeleteLocalRef(objectClass);

            jtokenHashCode = env->CallIntMethod(jtoken, m);
            ElUtil::CheckErrorAndLog(env, "CallIntMethod hashCode : %d!\n", __LINE__);

            Mutex::Autolock lock(slarMapLock);
            HashMap<Int32, AutoPtr<IBinder> >::Iterator it = slarMap.Find(jtokenHashCode);
            if (it != slarMap.End()) {
                ltoken = it->mSecond;
            }
            if (NULL == ltoken) {
                ALOGE("nativeRelayout() Invalid jwindow\n");
                env->ExceptionDescribe();
            }

            token = ltoken;

            slarMap.Erase(it);
        }
        else {
            ALOGE("nativeStartActivity() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(larWClass);
    }

    AutoPtr<IIntent> resultData;
    if (jresultData != NULL) {
        ElUtil::ToElIntent(env, jresultData, (IIntent**)&resultData);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    Boolean finished;
    am->FinishActivity(token, jresultCode, resultData, jfinishTask, &finished);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeFinishActivity()");
    return finished;
}

static void android_app_ElActivityManagerProxy_nativeActivityDestroyed(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeActivityDestroyed()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeStartActivity() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->ActivityDestroyed(token);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeActivityDestroyed()");
}

static bool android_app_ElActivityManagerProxy_nativeWillActivityBeVisible(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeWillActivityBeVisible()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeStartActivity() what is this token ?");
            assert(0);
        }
        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    am->WillActivityBeVisible(token, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeWillActivityBeVisible()");
    return result;
}

static void android_app_ElActivityManagerProxy_nativeActivityResumed(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeActivityResumed()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeStartActivity() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->ActivityResumed(token);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeActivityResumed()");
}

static void android_app_ElActivityManagerProxy_nativeActivityIdle(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jobject jconfig, jboolean jstopProfiling)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeActivityIdle()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeStartActivity() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IConfiguration> config;
    if (jconfig != NULL) {
        if (!ElUtil::ToElConfiguration(env, jconfig, (IConfiguration**)&config)) {
            ALOGE("android_app_ElActivityManagerProxy_nativeActivityIdle() ToElConfiguration fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->ActivityIdle(token, config, jstopProfiling);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeActivityIdle()");
}

static void android_app_ElActivityManagerProxy_nativePublishContentProviders(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcaller, jobject jproviders)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativePublishContentProviders()");

    jint jcallerHashcode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(jcallerHashcode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("nativePublishContentProviders() Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IList> provider;
    if (jproviders != NULL) {
        CParcelableList::New((IList**)&provider);

        jclass c = env->FindClass("java/util/List");
        ElUtil::CheckErrorAndLog(env, "nativePublishContentProviders() FindClass: List : %d!\n", __LINE__);
        jmethodID m = env->GetMethodID(c, "size","()I");
        ElUtil::CheckErrorAndLog(env, "nativePublishContentProviders() GetMethodID: size : %d!\n", __LINE__);

        jint size = env->CallIntMethod(jproviders, m);
        ElUtil::CheckErrorAndLog(env, "nativePublishContentProviders() CallIntMethod: size : %d!\n", __LINE__);
        if (size > 0) {
            m = env->GetMethodID(c, "get","(I)Ljava/lang/Object;");
            ElUtil::CheckErrorAndLog(env, "nativePublishContentProviders() GetMethodID: get : %d!\n", __LINE__);
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            for (jint i = 0; i < size; i++) {
                jobject jholder = env->CallObjectMethod(jproviders, m, i);
                ElUtil::CheckErrorAndLog(env, "nativePublishContentProviders() CallObjectMethod: get : %d!\n", __LINE__);

                AutoPtr<IContentProviderHolder> holder;
                if (ElUtil::ToElContentProviderHolder(env, jholder, (IContentProviderHolder**)&holder)) {
                    provider->Add(holder);
                }
                else {
                    ALOGE("nativePublishContentProviders() ToElContentProviderHolder fail!");
                }
            }
        }

        env->DeleteLocalRef(c);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->PublishContentProviders(appThread, provider);

    // ALOGD("- android_app_ElActivityManagerProxy_nativePublishContentProviders()");
}

static jobject android_app_ElActivityManagerProxy_nativeRegisterReceiver(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcaller, jstring jpackageName, jobject jreceiver,
            jobject jfilter, jstring jperm, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRegisterReceiver()");

    jint jcallerHashcode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(jcallerHashcode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("nativeRegisterReceiver() Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    AutoPtr<IIntentReceiver> receiver;
    if (jreceiver != NULL) {
        Int32 jreceiverHashCode = ElUtil::GetJavaHashCode(env, jreceiver);
        Mutex::Autolock lock(sIntentReceiverLock);
        HashMap<Int32, AutoPtr<IIntentReceiver> >::Iterator it = sIntentReceiver.Find(jreceiverHashCode);
        if (it != sIntentReceiver.End()) {
            receiver = it->mSecond;
        }
        else {
            jobject jInstance = env->NewGlobalRef(jreceiver);
            CIntentReceiverNative::New((Handle64)jvm, (Handle64)jInstance, (IIntentReceiver**)&receiver);;
            sIntentReceiver[jreceiverHashCode] = receiver;
        }
    }

    AutoPtr<IIntentFilter> filter;
    if (jfilter != NULL) {
        if (!ElUtil::ToElIntentFilter(env, jfilter, (IIntentFilter**)&filter)) {
            ALOGE("nativeRegisterReceiver() ToElIntentFilter fail!\n");
        }
    }

    String perm = ElUtil::ToElString(env, jperm);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IIntent> intent;
    am->RegisterReceiver(appThread, packageName, receiver, filter, perm, (Int32)juserId, (IIntent**)&intent);

    jobject jintent = NULL;
    if (intent != NULL) {
        jintent = ElUtil::GetJavaIntent(env, intent);
    }
    // ALOGD("- android_app_ElActivityManagerProxy_nativeRegisterReceiver()");
    return jintent;
}

static void android_app_ElActivityManagerProxy_nativeUnregisterReceiver(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jreceiver)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnregisterReceiver()");
    AutoPtr<IIntentReceiver> receiver;
    HashMap<Int32, AutoPtr<IIntentReceiver> >::Iterator it;
    {
        Mutex::Autolock lock(sIntentReceiverLock);
        it = sIntentReceiver.Find(jreceiver);
        if (it != sIntentReceiver.End()) {
            receiver = it->mSecond;
        }
    }

    if (NULL == receiver) {
        ALOGE("nativeUnregisterReceiver() Invalid IIntentReceiver!\n");
        env->ExceptionDescribe();
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IIntent> intent;
    am->UnregisterReceiver(receiver);

    Mutex::Autolock lock(sIntentReceiverLock);
    sIntentReceiver.Erase(it);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnregisterReceiver()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetContentProvider(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcaller, jstring jname, jint juserId, jboolean jstable)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetContentProvider()");

    jint jcallerHashcode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(jcallerHashcode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("nativeGetContentProvide() Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    String name = ElUtil::ToElString(env, jname);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IContentProviderHolder> holder;
    am->GetContentProvider(appThread, name, (Int32)juserId, (Boolean)jstable, (IContentProviderHolder**)&holder);

    jobject jholder = NULL;
    if (holder != NULL) {
        jholder = ElUtil::GetJavaContentProviderHolder(env, holder);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetContentProvider()");
    return jholder;
}

static void android_app_ElActivityManagerProxy_nativeSetRequestedOrientation(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jint jrequestedOrientation)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetRequestedOrientation()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeSetRequestedOrientation() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->SetRequestedOrientation(token, (Int32)jrequestedOrientation);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetRequestedOrientation()");
}

static jint android_app_ElActivityManagerProxy_nativeGetRequestedOrientation(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetRequestedOrientation()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeGetRequestedOrientation() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 orientation = 0;
    am->GetRequestedOrientation(token, &orientation);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetRequestedOrientation()");
    return (jint)orientation;
}

static void android_app_ElActivityManagerProxy_nativeActivityStopped(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jobject jstate, jobject jpersistentState, jobject jdescription)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeActivityStopped()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeActivityStopped() what is this token ?");
            assert(0);
        }
    }

    AutoPtr<IBundle> state;
    if (jstate != NULL) {
        if (!ElUtil::ToElBundle(env, jstate, (IBundle**)&state)) {
            ALOGE("nativeActivityStopped() ToElBundle fail!");
        }
    }

    AutoPtr<IPersistableBundle> persistentState;
    if (jpersistentState != NULL) {
        if (!ElUtil::ToElPersistableBundle(env, jpersistentState, (IPersistableBundle**)&persistentState)) {
            ALOGE("nativeActivityStopped() ToElPersistableBundle fail!");
        }
    }

    AutoPtr<ICharSequence> description;
    if (jdescription != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        ElUtil::CheckErrorAndLog(env, "nativeActivityStopped FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "nativeActivityStopped GetMethodID toString : %d!\n", __LINE__);
        env->DeleteLocalRef(csClass);

        jstring jstr = (jstring)env->CallObjectMethod(jdescription, m);
        ElUtil::CheckErrorAndLog(env, "nativeActivityStopped CallObjectMethod toString : %d!\n", __LINE__);

        String str = ElUtil::ToElString(env, jstr);
        env->DeleteLocalRef(jstr);

        if (!str.IsNull()) {
            CString::New(str, (ICharSequence**)&description);
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->ActivityStopped(token, state, persistentState, description);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeActivityStopped()");
}

static void android_app_ElActivityManagerProxy_nativeGetMemoryInfo(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject joutInfo)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetMemoryInfo()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IActivityManagerMemoryInfo> outInfo;
    am->GetMemoryInfo((IActivityManagerMemoryInfo**)&outInfo);

    ElUtil::SetMemoryInfo(env, joutInfo, outInfo);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetMemoryInfo()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetRunningAppProcesses(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetRunningAppProcesses()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr <IList> outInfo;
    am->GetRunningAppProcesses((IList**)&outInfo);

    jobject jrunAppInfo = NULL;

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetRunningAppProcesses Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetRunningAppProcesses Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jrunAppInfo = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetRunningAppProcesses Fail NewObject: ArrayList : %d!\n", __LINE__);
    if (outInfo != NULL) {
        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetRunningAppProcesses Fail GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        outInfo->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            outInfo->Get(i, (IInterface**)&obj);
            AutoPtr<IActivityManagerRunningAppProcessInfo> procInfo = IActivityManagerRunningAppProcessInfo::Probe(obj);
            if (procInfo != NULL) {
                jobject jprocInfo= ElUtil::GetJavaRunningAppProcessInfo(env, procInfo);
                if (jprocInfo != NULL) {
                    env->CallBooleanMethod(jrunAppInfo, mAdd, jprocInfo);
                    ElUtil::CheckErrorAndLog(env, "nativeGetRunningAppProcesses Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(jprocInfo);
                }
                else {
                    ALOGE("nativeGetRunningAppProcesses() jprocInfo is NULL!");
                }
            }
        }
    }

    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetRunningAppProcesses()");
    return jrunAppInfo;
}

static jobject android_app_ElActivityManagerProxy_nativeGetServices(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jmaxNum, jint jflags)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetServices()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IList> list;
    am->GetServices((Int32)jmaxNum, (Int32)jflags, (IList**)&list);
    jobject jservices = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetServices Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetServices Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jservices = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetServices Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetServices Fail GetMethodID: add : %d!\n", __LINE__);

        Int32 size;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IActivityManagerRunningServiceInfo> serviceInfo = IActivityManagerRunningServiceInfo::Probe(obj);
            if (serviceInfo != NULL) {
                jobject jserviceInfo = ElUtil::GetJavaRunningServiceInfo(env, serviceInfo);
                if (jserviceInfo != NULL) {
                    env->CallBooleanMethod(jservices, mAdd, jserviceInfo);
                    ElUtil::CheckErrorAndLog(env, "nativeGetServices Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(jserviceInfo);
                }
                else {
                    ALOGE("android_app_ElActivityManagerProxy_nativeGetServices() jserviceInfo is NULL!");
                }
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetServices()");
    return jservices;
}

static jint android_app_ElActivityManagerProxy_nativeCheckPermission(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpermission, jint jpid, jint juid)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeCheckPermission()");

    String permission = ElUtil::ToElString(env, jpermission);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 result;
    am->CheckPermission(permission, (Int32)jpid, (Int32)juid, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeCheckPermission()");
    return (jint)result;
}

static jobject android_app_ElActivityManagerProxy_nativeGetCurrentUser(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetCurrentUser()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IUserInfo> info;
    am->GetCurrentUser((IUserInfo**)&info);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaUserInfo(env, info);
        if (jinfo == NULL) {
            ALOGE("nativeGetCurrentUser() jinfo is NULL");
        }
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetCurrentUser()");
    return jinfo;
}

static jboolean android_app_ElActivityManagerProxy_nativeRefContentProvider(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jconnection, jint jstable, jint junstable)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRefContentProvider()");

    if (jconnection == NULL) {
        ElUtil::ThrowExceptionByName(env, "java/lang/NullPointerException", "connection is null");
        return JNI_FALSE;
    }

    jclass c = env->FindClass("android/os/ElBinderProxy");
    ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);

    Boolean result = FALSE;
    if (env->IsInstanceOf(jconnection, c))  {
        AutoPtr<IBinder> connection = (IBinder*)ElUtil::GetJavalongField(env, c, jconnection, "mNativeProxy", "nativeRefContentProvider");

        IIActivityManager* am = (IIActivityManager*)jproxy;
        am->RefContentProvider(connection, (Int32)jstable, (Int32)junstable, &result);
    }else {
        String str = ElUtil::GetJavaToString(env, jconnection);
        ALOGE("nativeRefContentProvider() jconnection: %s", str.string());
        ElUtil::ThrowExceptionByName(env, "java/lang/IllegalArgumentException", "nativeRefContentProvider():  jconnection Unknown!");
    }

    env->DeleteLocalRef(c);
    // ALOGD("- android_app_ElActivityManagerProxy_nativeRefContentProvider()");
    return (jboolean)result;
}

static jint android_app_ElActivityManagerProxy_nativeBroadcastIntent(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcaller, jobject jintent, jstring jresolvedType, jint jresultTo, jint jresultCode,
    jstring jresultData, jobject jmap, jstring jrequiredPermission, jint jappOp, jboolean jserialized, jboolean jsticky, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeBroadcastIntent()");

    jint jcallerHashcode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(jcallerHashcode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeBroadcastIntent() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIntentReceiver* resultTo = NULL;
    if (jresultTo != 0) {
        Mutex::Autolock lock(sIntentReceiverLock);
        HashMap<Int32, AutoPtr<IIntentReceiver> >::Iterator it = sIntentReceiver.Find(jresultTo);
        if (it != sIntentReceiver.End()) {
            resultTo = it->mSecond;
        }

        if (NULL == resultTo) {
            ALOGE("nativeBroadcastIntent() Invalid IIntentReceiver!\n");
            env->ExceptionDescribe();
        }
    }

    String resultData = ElUtil::ToElString(env, jresultData);

    AutoPtr<IBundle> map;
    if (jmap != NULL) {
        if (!ElUtil::ToElBundle(env, jmap, (IBundle**)&map)) {
            ALOGE("nativeBroadcastIntent() ToElBundle fail!");
        }
    }

    String requiredPermission = ElUtil::ToElString(env, jrequiredPermission);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 result;
    am->BroadcastIntent(appThread, intent, resolvedType, resultTo, (Int32)jresultCode, resultData, map,
        requiredPermission, jappOp, jserialized, jsticky, (Int32)juserId , &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeBroadcastIntent()");
    return (jint)result;
}

static void android_app_ElActivityManagerProxy_nativeHandleApplicationCrash(
    JNIEnv* env, jobject clazz, jlong jproxy, jint japp, jobject jcrashInfo)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeHandleApplicationCrash()");

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(japp);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }
    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IApplicationErrorReportCrashInfo> crashInfo;
    if (jcrashInfo != NULL) {
        if (!ElUtil::ToElApplicationErrorReportCrashInfo(env, jcrashInfo, (IApplicationErrorReportCrashInfo**)&crashInfo)) {
            ALOGE("nativeHandleApplicationCrash() ToElApplicationErrorReportCrashInfo fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    ECode ec = am->HandleApplicationCrash(IBinder::Probe(appThread), crashInfo);
    if (FAILED(ec))
        ALOGE("nativeHandleApplicationCrash() ec=0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeHandleApplicationCrash()");
}

static jobject android_app_ElActivityManagerProxy_nativeStartService(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcaller, jobject jservice, jstring jresolvedType, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartService()");

    jint jcallerHashcode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(jcallerHashcode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IIntent> service;
    if (jservice != NULL) {
        if (!ElUtil::ToElIntent(env, jservice, (IIntent**)&service)) {
            ALOGE("nativeStartService() ToElIntent fail!!\n");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IComponentName> name;
    am->StartService(appThread, service, resolvedType, (Int32)juserId, (IComponentName**)&name);

    jobject jname = NULL;
    if (name != NULL) {
        jname = ElUtil::GetJavaComponentName(env, name);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartService()");
    return jname;
}

static jint android_app_ElActivityManagerProxy_nativeStopService(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcaller, jobject jservice, jstring jresolvedType, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStopService()");

    jint jcallerHashcode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(jcallerHashcode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IIntent> service;
    if (jservice != NULL) {
        if (!ElUtil::ToElIntent(env, jservice, (IIntent**)&service)) {
            ALOGE("nativeStartService() ToElIntent fail!!\n");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 result;
    am->StopService(appThread, service, resolvedType, (Int32)juserId, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStopService()");
    return (jint)result;
}

static void android_app_ElActivityManagerProxy_nativeServiceDoneExecuting(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jint jtype, jint jstartId, jint jres)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeServiceDoneExecuting()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeServiceDoneExecuting() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->ServiceDoneExecuting(token, (Int32)jtype, (Int32)jstartId, (Int32)jres);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeServiceDoneExecuting()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetIntentSender(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jtype, jstring jpackageName, jobject jtoken, jstring jresultWho, jint jrequestCode,
    jobjectArray jintents, jobjectArray jresolvedTypes, jint jflags, jobject joptions, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetIntentSender()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeGetIntentSender() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    String resultWho = ElUtil::ToElString(env, jresultWho);

    AutoPtr<ArrayOf<IIntent*> > intents;
    if (jintents != NULL) {
        int size = env->GetArrayLength(jintents);
        intents = ArrayOf<IIntent*>::Alloc(size);

        for(int i = 0; i < size; i++){
            jobject jintent = env->GetObjectArrayElement(jintents, i);
            ElUtil::CheckErrorAndLog(env, "nativeGetIntentSender(); GetObjectArrayelement failed : %d!\n", __LINE__);
            AutoPtr<IIntent> intent;
            if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
                ALOGE("nativeGetIntentSender() ToElIntent fail!");
            }
            intents->Set(i, intent);
            env->DeleteLocalRef(jintent);
        }
    }

    AutoPtr<ArrayOf<String> > resolvedTypes;
    if (jresolvedTypes != NULL) {
        if (!ElUtil::ToElStringArray(env, jresolvedTypes, (ArrayOf<String>**)&resolvedTypes)) {
            ALOGE("nativeGetIntentSender() ToElStringArray fail!");
        }
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeGetIntentSender() ToElBundle fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IIIntentSender> intentSender;
    am->GetIntentSender((Int32)jtype, packageName, token, resultWho, (Int32)jrequestCode, intents,
            resolvedTypes, (Int32)jflags, options, (Int32)juserId, (IIIntentSender**)&intentSender);
    jobject jintentSender = NULL;
    if (intentSender != NULL) {
        jintentSender = ElUtil::GetJavaIIntentSender(env, intentSender);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetIntentSender()");
    return jintentSender;
}

static jboolean android_app_ElActivityManagerProxy_nativeMoveActivityTaskToBack(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jboolean jnonRoot)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeMoveActivityTaskToBack()");
    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeMoveActivityTaskToBack() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    am->MoveActivityTaskToBack(token, (Boolean)jnonRoot, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeMoveActivityTaskToBack()");
    return result;
}

static jobject android_app_ElActivityManagerProxy_nativeGetConfiguration(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jboolean jnonRoot)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetConfiguration()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IConfiguration> config;
    jobject jconfig = NULL;
    am->GetConfiguration((IConfiguration**)&config);
    if (config != NULL) {
        jconfig = ElUtil::GetJavaConfiguration(env, config);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetConfiguration()");
    return jconfig;
}

static void android_app_ElActivityManagerProxy_nativeSetServiceForeground(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclassName, jobject jtoken, jint jid, jobject jnotification, jboolean jremoveNotification)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetServiceForeground()");

    AutoPtr<IComponentName> className;
    if (jclassName != NULL) {
        if (!ElUtil::ToElComponentName(env, jclassName, (IComponentName**)&className)) {
            ALOGE("nativeSetServiceForeground ToElComponentName fail!");
        }
    }

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeSetServiceForeground() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<INotification> notification;
    if (jnotification != NULL) {
        if (!ElUtil::ToElNotification(env, jnotification, (INotification**)&notification)) {
            ALOGE("nativeSetServiceForeground ToElNotification fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->SetServiceForeground(className, token, (Int32)jid, notification, (Boolean)jremoveNotification);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetServiceForeground()");
}

static void android_app_ElActivityManagerProxy_nativeRemoveContentProvider(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jconnection, jboolean jstable)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRemoveContentProvider()");

    if (jconnection == NULL) {
        ElUtil::ThrowExceptionByName(env, "java/lang/NullPointerException", "connection is null");
        return;
    }

    jclass c = env->FindClass("android/os/ElBinderProxy");
    ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);

    Boolean result = FALSE;
    if (env->IsInstanceOf(jconnection, c))  {
        AutoPtr<IBinder> connection = (IBinder*)ElUtil::GetJavalongField(env, c, jconnection, "mNativeProxy", "nativeRemoveContentProvider");

        IIActivityManager* am = (IIActivityManager*)jproxy;
        am->RemoveContentProvider(connection, (Boolean)jstable);
    }else {
        String str = ElUtil::GetJavaToString(env, jconnection);
        ALOGE("nativeRemoveContentProvider() jconnection: %s", str.string());
        ElUtil::ThrowExceptionByName(env, "java/lang/IllegalArgumentException", "nativeRemoveContentProvider():  jconnection Unknown!");
    }

    env->DeleteLocalRef(c);
    // ALOGD("- android_app_ElActivityManagerProxy_nativeRemoveContentProvider()");
}

static void android_app_ElActivityManagerProxy_nativeUpdateConfiguration(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jvalues)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUpdateConfiguration()");

    AutoPtr<IConfiguration> values;
    if (jvalues != NULL) {
        if (!ElUtil::ToElConfiguration(env, jvalues, (IConfiguration**)&values)) {
            ALOGE("nativeUpdateConfiguration ToElConfiguration() fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->UpdateConfiguration(values);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUpdateConfiguration()");
}

static jint android_app_ElActivityManagerProxy_nativeBindService(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jcaller, jobject jtoken, jobject jservice, jstring jresolvedType, jobject jconnection, jint jflags, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeBindService()");

    jint jcallerHashcode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(jcallerHashcode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("nativeBindService Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);
        }
        else {
            ALOGE("nativeBindService() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IIntent> service;
    if (jservice != NULL) {
        if (!ElUtil::ToElIntent(env, jservice, (IIntent**)&service)) {
            ALOGE("nativeStartService() ToElIntent fail!!\n");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    AutoPtr<IIServiceConnection> connection;
    if (jconnection != NULL) {
        if (!ElUtil::ToElIServiceConnection(env, jconnection, (IIServiceConnection**)&connection)) {
            ALOGE("nativeBindService() ToIIServiceConnection fail!");
        }

        Int32 hashCode = ElUtil::GetJavaHashCode(env, jconnection);

        Mutex::Autolock lock(sISerConnectionLock);
        sISerConnection[hashCode] = connection;
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 result;
    am->BindService(appThread, token, service, resolvedType, connection, (Int32)jflags, (Int32)juserId, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeBindService()");
    return (jint)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeUnbindService(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jconnection)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnbindService()");

    AutoPtr<IIServiceConnection> connection;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jconnection);
    {
        Mutex::Autolock lock(sISerConnectionLock);
        HashMap<Int32, AutoPtr<IIServiceConnection> >::Iterator it = sISerConnection.Find(hashCode);
        if (it != sISerConnection.End()) {
            connection = it->mSecond;
            sISerConnection.Erase(it);
        }
    }

    if (NULL == connection) {
        ALOGE("nativeUnbindService() Invalid jconnection!\n");
        env->ExceptionDescribe();
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    am->UnbindService(connection, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnbindService()");
    return (jboolean)result;
}

static jobject android_app_ElActivityManagerProxy_nativeGetDeviceConfigurationInfo(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetDeviceConfigurationInfo()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IConfigurationInfo> cfgInfo;
    am->GetDeviceConfigurationInfo((IConfigurationInfo**)&cfgInfo);
    jobject jcfgInfo = NULL;
    if (cfgInfo != NULL) {
        jcfgInfo = ElUtil::GetJavaConfigurationInfo(env, cfgInfo);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetDeviceConfigurationInfo()");
    return jcfgInfo;
}

static jboolean android_app_ElActivityManagerProxy_nativeStopServiceToken(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclassName, jobject jtoken, jint jstartId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStopServiceToken()");

    AutoPtr<IComponentName> className;
    if (jclassName != NULL) {
        if (!ElUtil::ToElComponentName(env, jclassName, (IComponentName**)&className)) {
            ALOGE("nativeStopServiceToken() ToElComponentName fail!");
        }
    }

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeStopServiceToken() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    am->StopServiceToken(className, token, (jint)jstartId, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStopServiceToken()");
    return (jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativePublishService(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jobject jintent, jobject jservice)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativePublishService()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativePublishService() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeStartService() ToElIntent fail!!\n");
        }
    }

    AutoPtr<IBinder> service;
    if (jservice != NULL) {

        jclass imKlass = env->FindClass("android/inputmethodservice/IInputMethodWrapper");
        ElUtil::CheckErrorAndLog(env, "FindClass IInputMethodWrapper fail : %d!\n", __LINE__);
        jclass mcsKlass = env->FindClass("com/android/internal/app/IMediaContainerService");
        ElUtil::CheckErrorAndLog(env, "FindClass IMediaContainerService fail : %d!\n", __LINE__);
        jclass wpsKlass = env->FindClass("android/service/wallpaper/WallpaperService$IWallpaperServiceWrapper");
        ElUtil::CheckErrorAndLog(env, "FindClass fail : %d!\n", __LINE__);
        jclass rvfKlass = env->FindClass("com/android/internal/widget/IRemoteViewsFactory");
        ElUtil::CheckErrorAndLog(env, "FindClass IRemoteViewsFactory fail : %d!\n", __LINE__);
        jclass lpKlass = env->FindClass("com/android/internal/location/ILocationProvider");
        ElUtil::CheckErrorAndLog(env, "FindClass ILocationProvider fail : %d!\n", __LINE__);

        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        if (env->IsInstanceOf(jservice, imKlass)) {
            jobject jInstance = env->NewGlobalRef(jservice);

            CInputMethodServiceNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&service);
        }
        else if (env->IsInstanceOf(jservice, mcsKlass)) {
            jobject jInstance = env->NewGlobalRef(jservice);

            CMediaContainerServiceNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&service);
        }
        else if (env->IsInstanceOf(jservice, wpsKlass)) {
            jobject jInstance = env->NewGlobalRef(jservice);

            CIIWallpaperServiceNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&service);
        }
        else if (env->IsInstanceOf(jservice, rvfKlass)){
            AutoPtr<IIRemoteViewsFactory> factory;
            CRemoteViewsFactoryNative::New((Handle64)jvm, (Handle64)jservice, (IIRemoteViewsFactory**)&factory);
            service = IBinder::Probe(factory.Get());
        }
        else if (env->IsInstanceOf(jservice, lpKlass)){
            AutoPtr<IILocationProvider> locationProvider;
            CIILocationProviderNative::New((Handle64)jvm, (Handle64)jservice, (IILocationProvider**)&locationProvider);
            service = IBinder::Probe(locationProvider.Get());
        }
        else {
            CServiceNative::New((Handle64)jvm, (Handle64)jservice, (IBinder**)&service);
        }

        env->DeleteLocalRef(imKlass);
        env->DeleteLocalRef(mcsKlass);
        env->DeleteLocalRef(wpsKlass);
        env->DeleteLocalRef(rvfKlass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->PublishService(token, intent, service);

    // ALOGD("- android_app_ElActivityManagerProxy_nativePublishService()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetAppTasks(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jcallingPackage)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetAppTasks()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IList> list;
    am->GetAppTasks(callingPackage, (IList**)&list);
    jobject jtasks = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetAppTasks FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetAppTasks GetMethodID: ArrayList : %d!\n", __LINE__);

        jtasks = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetAppTasks NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetAppTasks GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IIAppTask> appTask = IIAppTask::Probe(obj);
            if (appTask != NULL) {
                jobject jappTask = ElUtil::GetJavaIAppTask(env, appTask);
                env->CallBooleanMethod(jtasks, mAdd, jappTask);
                ElUtil::CheckErrorAndLog(env, "nativeGetAppTasks CallBooleanMethod: add : %d!\n", __LINE__);
                env->DeleteLocalRef(jappTask);
            }
            else {
                ALOGE("nativeGetAppTasks() appTask is NULL!");
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetAppTasks()");
    return jtasks;
}

static jint android_app_ElActivityManagerProxy_nativeAddAppTask(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jactivityToken, jobject jintent, jobject jdescription, jobject jthumbnail)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeAddAppTask()");

    IBinder* token = NULL;
    if (jactivityToken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jactivityToken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jactivityToken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeAddAppTask() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeAddAppTask() ToElIntent fail!\n");
        }
    }

    AutoPtr<IActivityManagerTaskDescription> description;
    if (jdescription != NULL) {
        if (!ElUtil::ToElTaskDescription(env, jdescription, (IActivityManagerTaskDescription**)&description)) {
            ALOGE("nativeAddAppTask() ToElTaskDescription fail!\n");
        }
    }

    AutoPtr<IBitmap> thumbnail;
    if (jthumbnail != NULL) {
        if (!ElUtil::ToElBitmap(env, jthumbnail, (IBitmap**)&thumbnail)) {
            ALOGE("nativeAddAppTask() ToElBitmap fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 result;
    am->AddAppTask(token, intent, description, thumbnail, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeAddAppTask()");
    return result;
}

static jobject android_app_ElActivityManagerProxy_nativeGetAppTaskThumbnailSize(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetAppTaskThumbnailSize()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IPoint> point;
    am->GetAppTaskThumbnailSize((IPoint**)&point);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetAppTaskThumbnailSize()");
    return ElUtil::GetJavaPoint(env, point);
}

static jobject android_app_ElActivityManagerProxy_nativeGetTasks(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jmaxNum, jint jflags)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetTasks()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IList> list;
    am->GetTasks((Int32)jmaxNum, (Int32)jflags, (IList**)&list);
    jobject jtasks = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetTasks FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetTasks GetMethodID: ArrayList : %d!\n", __LINE__);

        jtasks = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetTasks NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetTasks GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IActivityManagerRunningTaskInfo> rtInfo = IActivityManagerRunningTaskInfo::Probe(obj);
            if (rtInfo != NULL) {
                jobject jrtInfo = ElUtil::GetJavaRunningTaskInfo(env, rtInfo);
                env->CallBooleanMethod(jtasks, mAdd, jrtInfo);
                ElUtil::CheckErrorAndLog(env, "nativeGetTasks CallBooleanMethod: add : %d!\n", __LINE__);
                env->DeleteLocalRef(jrtInfo);
            }
            else {
                ALOGE("nativeGetTasks() rtInfo is NULL!");
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetTasks()");
    return jtasks;
}

static void android_app_ElActivityManagerProxy_nativeFinishReceiver(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint jresultCode, jstring jresultData, jobject jmap, jboolean jabortBroadcast, jint jflags)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeFinishReceiver()");

    AutoPtr<IBinder> who;
    if (jwho != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jwho);
        {
            Mutex::Autolock lock(sIntentReceiverLock);
            HashMap<Int32, AutoPtr<IIntentReceiver> >::Iterator it = sIntentReceiver.Find(hashCode);
            if (it != sIntentReceiver.End()) {
                who = IBinder::Probe(it->mSecond);
            }
        }

        if (NULL == who) {
            AutoPtr<IApplicationThread> appThread;
            Mutex::Autolock lock(sAppThreadLock);
            HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
            if (it != sAppThread.End()) {
                who = IBinder::Probe(it->mSecond);
            }
        }

        if (NULL == who) {
            jclass c = env->FindClass("java/lang/Object");
            jmethodID m = env->GetMethodID(c, "toString", "()Ljava/lang/String;");
            jstring jstr = (jstring)env->CallObjectMethod(jwho, m);
            String who = ElUtil::ToElString(env, jstr);
            ALOGE("nativeFinishReceiver() unknown hashCode: %d jwho: %s", hashCode, who.string());
        }
    }

    String resultData = ElUtil::ToElString(env, jresultData);

    AutoPtr<IBundle> map;
    if (jmap != NULL) {
        if (!ElUtil::ToElBundle(env, jmap, (IBundle**)&map)) {
            ALOGE("nativeFinishReceiver() ToElBundle fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->FinishReceiver(who, (Int32)jresultCode, resultData, map, (Boolean)jabortBroadcast, jflags);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeFinishReceiver()");
}

static jstring android_app_ElActivityManagerProxy_nativeGetCallingPackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetCallingPackage()");

    IIActivityManager* am = (IIActivityManager*)jproxy;

    AutoPtr<IBinder> token;

    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);

        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);
        }
        else if (env->IsInstanceOf(jtoken, larWClass)) {
            Int32 jtokenHashCode = ElUtil::GetJavaHashCode(env, jtoken);
            Mutex::Autolock lock(slarMapLock);
            HashMap<Int32, AutoPtr<IBinder> >::Iterator it = slarMap.Find(jtokenHashCode);
            if (it != slarMap.End()) {
                token = it->mSecond;
            }
            if (NULL == token) {
                ALOGE("nativeGetCallingPackage() Invalid jtoken!\n");
                env->ExceptionDescribe();
            }
        }
        else {
            ALOGE("nativeGetCallingPackage() what is this token ? class name is : %s\n", ElUtil::GetClassName(env, jtoken).string());
            assert(0);
        }
    }


    String package;
    am->GetCallingPackage(token, &package);

    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetCallingPackage(), package = %s", package.string());
    return ElUtil::GetJavaString(env, package);
}

static jstring android_app_ElActivityManagerProxy_nativeGetCallingPackageForBroadcast(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean jforeground)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetCallingPackageForBroadcast()");

    IIActivityManager* am = (IIActivityManager*)jproxy;

    String package;
    am->GetCallingPackageForBroadcast(jforeground, &package);

    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetCallingPackageForBroadcast(), package = %s", package.string());
    return ElUtil::GetJavaString(env, package);
}

static jint android_app_ElActivityManagerProxy_nativeGetTaskForActivity(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jboolean onlyRoot)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetTaskForActivity()");

    IIActivityManager* am = (IIActivityManager*)jproxy;

    AutoPtr<IBinder> token;

    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);

        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);
        }
        else if (env->IsInstanceOf(jtoken, larWClass)) {
            Int32 jtokenHashCode = ElUtil::GetJavaHashCode(env, jtoken);
            Mutex::Autolock lock(slarMapLock);
            HashMap<Int32, AutoPtr<IBinder> >::Iterator it = slarMap.Find(jtokenHashCode);
            if (it != slarMap.End()) {
                token = it->mSecond;
            }
            if (NULL == token) {
                ALOGE("nativeGetTaskForActivity() Invalid jtoken!\n");
                env->ExceptionDescribe();
            }
        }
        else {
            ALOGE("nativeGetTaskForActivity() what is this token ? class name is : %s\n", ElUtil::GetClassName(env, jtoken).string());
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(larWClass);
    }


    Int32 taskId = 0;
    am->GetTaskForActivity(token, (Boolean)onlyRoot, &taskId);

    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetTaskForActivity(), taskId = %d", taskId);
    return taskId;
}

static void android_app_ElActivityManagerProxy_nativeOverridePendingTransition(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jstring jpackageName, jint jenterAnim, jint jexitAnim)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeOverridePendingTransition()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeBindService() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->OverridePendingTransition(token, packageName, (Int32)jenterAnim, (Int32)jexitAnim);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeOverridePendingTransition()");
}

static jobjectArray android_app_ElActivityManagerProxy_nativeGetProcessMemoryInfo(JNIEnv* env, jobject clazz, jlong jproxy,
    jintArray jpids)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetProcessMemoryInfo()");

    AutoPtr<ArrayOf<Int32> > pids;
    if (jpids != NULL) {
        jint size = env->GetArrayLength(jpids);
        ElUtil::CheckErrorAndLog(env, "nativeGetProcessMemoryInfo() GetArrayLength:(): %d!\n", __LINE__);

        jint* jpayload = (jint*)malloc(size * sizeof(Int32));

        env->GetIntArrayRegion(jpids, 0, size, jpayload);
        ElUtil::CheckErrorAndLog(env, "nativeGetProcessMemoryInfo() GetIntArrayRegion:(): %d!\n", __LINE__);
        pids = ArrayOf<Int32>::Alloc((Int32*)jpayload, size);
        free(jpayload);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<ArrayOf<IDebugMemoryInfo*> > infos;
    am->GetProcessMemoryInfo(pids, (ArrayOf<IDebugMemoryInfo*>**)&infos);
    jobjectArray jinfos = NULL;
    if (infos != NULL) {
        Int32 count = infos->GetLength();

        jclass dmiKlass = env->FindClass("android/os/Debug$MemoryInfo");
        ElUtil::CheckErrorAndLog(env, "FindClass: Debug$MemoryInfo : %d", __LINE__);

        jinfos = env->NewObjectArray((jsize)count, dmiKlass, 0);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: Debug$MemoryInfo : %d", __LINE__);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jinfo = ElUtil::GetJavaDebugMemoryInfo(env, (*infos)[i]);
            if (jinfo != NULL) {
                env->SetObjectArrayElement(jinfos, i, jinfo);
                env->DeleteLocalRef(jinfo);
            }
        }

        env->DeleteLocalRef(dmiKlass);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetProcessMemoryInfo()");
    return jinfos;
}

static jboolean android_app_ElActivityManagerProxy_nativeIsUserAMonkey(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsUserAMonkey()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    am->IsUserAMonkey(&result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsUserAMonkey()");
    return (jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativeForceStopPackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeForceStopPackage()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->ForceStopPackage(packageName, (Int32)juserId);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeForceStopPackage()");
}

static void android_app_ElActivityManagerProxy_nativeUpdatePersistentConfiguration(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jvalues)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUpdatePersistentConfiguration()");

    AutoPtr<IConfiguration> values;
    if (jvalues != NULL) {
        if (!ElUtil::ToElConfiguration(env, jvalues, (IConfiguration**)&values)) {
            ALOGE("nativeUpdatePersistentConfiguration ToElConfiguration fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->UpdatePersistentConfiguration(values);
    if (FAILED(ec))
        ALOGE("nativeUpdatePersistentConfiguration() ec = 0x%08x \n", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUpdatePersistentConfiguration()");
}

static jlongArray android_app_ElActivityManagerProxy_nativeGetProcessPss(JNIEnv* env, jobject clazz, jlong jproxy,
    jintArray jpids)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetProcessPss()");

    AutoPtr<ArrayOf<Int32> > pids;
    if (jpids != NULL) {
        jint size = env->GetArrayLength(jpids);
        ElUtil::CheckErrorAndLog(env, "nativeGetProcessMemoryInfo() GetArrayLength:(): %d!\n", __LINE__);

        jint* jpayload = (jint*)malloc(size * sizeof(Int32));

        env->GetIntArrayRegion(jpids, 0, size, jpayload);
        ElUtil::CheckErrorAndLog(env, "nativeGetProcessMemoryInfo() GetIntArrayRegion:(): %d!\n", __LINE__);
        pids = ArrayOf<Int32>::Alloc((Int32*)jpayload, size);
        free(jpayload);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<ArrayOf<Int64> > processPss;
    am->GetProcessPss(pids, (ArrayOf<Int64>**)&processPss);

    jlongArray jprocessPss = NULL;
    if (processPss != NULL) {
        jprocessPss = ElUtil::GetJavaLongArray(env, processPss);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetProcessPss()");
    return jprocessPss;
}

static jboolean android_app_ElActivityManagerProxy_nativeClearApplicationUserData(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jobject jobserver, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeClearApplicationUserData()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIPackageDataObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        CIPackageDataObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIPackageDataObserver**)&observer);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    am->ClearApplicationUserData(packageName, observer, (Int32)juserId, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeClearApplicationUserData()");
    return (jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativeKillBackgroundProcesses(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeKillBackgroundProcesses()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->KillBackgroundProcesses(packageName, (Int32)juserId);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeKillBackgroundProcesses()");
}

static int android_app_ElActivityManagerProxy_nativeStartActivityAsUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcaller, jstring jcallingPackage, jobject jintent, jstring jresolvedType, jobject jresultTo, jstring jresultWho,
    jint jrequestCode, jint jstartFlags, jobject jprofilerInfo, jobject joptions, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartActivityAsUser()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeStartActivityAsUser() ToElIntent fail!\n");
        }
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeStartActivityAsUser() ToElBundle fail!\n");
        }
    }

    AutoPtr<IProfilerInfo> profilerInfo;
    if (jprofilerInfo != NULL) {
        if (!ElUtil::ToElProfilerInfo(env, jprofilerInfo, (IProfilerInfo**)&profilerInfo)) {
            ALOGE("nativeStartActivityAsUser() ToElProfilerInfo fail!\n");
        }
    }

    String resolvedType;
    if (jresolvedType != NULL) {
        resolvedType = ElUtil::ToElString(env, jresolvedType);
    }

    String resultWho;
    if (jresultWho != NULL) {
        resultWho = ElUtil::ToElString(env, jresultWho);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    IBinder* resultTo = NULL;
    if (jresultTo != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);

        if (env->IsInstanceOf(jresultTo, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            resultTo = (IBinder*)(Int64)env->GetLongField(jresultTo, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else if (env->IsInstanceOf(jresultTo, larWClass)) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jresultTo);

            AutoPtr<IBinder> resultTo;
            if (NOERROR == CLocalActivityRecordNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&resultTo)) {
                jclass objectClass = env->FindClass("java/lang/Object");
                ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

                jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
                ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);
                env->DeleteLocalRef(objectClass);

                jint jresultToHashCode = env->CallIntMethod(jresultTo, m);
                ElUtil::CheckErrorAndLog(env, "CallIntMethod hashCode : %d!\n", __LINE__);

                Mutex::Autolock lock(slarMapLock);
                slarMap[jresultToHashCode] = resultTo;
            }
            else {
                ALOGE("nativeStartActivityAsUser() new CLocalActivityRecordNative fail ");
            }
        }
        else {
            ALOGE("nativeStartActivityAsUser() what is this resultTo ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(larWClass);
    }

    Int32 result = 0;
    am->StartActivityAsUser(appThread, callingPackage, intent, resolvedType, resultTo, resultWho, (Int32)jrequestCode, (Int32)jstartFlags,
        profilerInfo, options, (Int32)juserId, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartActivityAsUser()");
    return result;
}


static int android_app_ElActivityManagerProxy_nativeStartActivityAsCaller(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcaller, jstring jcallingPackage, jobject jintent, jstring jresolvedType, jobject jresultTo, jstring jresultWho,
    jint jrequestCode, jint jstartFlags, jobject jprofilerInfo, jobject joptions, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartActivityAsCaller()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeStartActivityAsCaller() ToElIntent fail!\n");
        }
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeStartActivityAsCaller() ToElBundle fail!\n");
        }
    }

    AutoPtr<IProfilerInfo> profilerInfo;
    if (jprofilerInfo != NULL) {
        if (!ElUtil::ToElProfilerInfo(env, jprofilerInfo, (IProfilerInfo**)&profilerInfo)) {
            ALOGE("nativeStartActivityAsCaller() ToElProfilerInfo fail!\n");
        }
    }

    String resolvedType;
    if (jresolvedType != NULL) {
        resolvedType = ElUtil::ToElString(env, jresolvedType);
    }

    String resultWho;
    if (jresultWho != NULL) {
        resultWho = ElUtil::ToElString(env, jresultWho);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    IBinder* resultTo = NULL;
    if (jresultTo != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);

        if (env->IsInstanceOf(jresultTo, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            resultTo = (IBinder*)(Int64)env->GetLongField(jresultTo, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else if (env->IsInstanceOf(jresultTo, larWClass)) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jresultTo);

            AutoPtr<IBinder> resultTo;
            if (NOERROR == CLocalActivityRecordNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&resultTo)) {
                jclass objectClass = env->FindClass("java/lang/Object");
                ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

                jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
                ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);
                env->DeleteLocalRef(objectClass);

                jint jresultToHashCode = env->CallIntMethod(jresultTo, m);
                ElUtil::CheckErrorAndLog(env, "CallIntMethod hashCode : %d!\n", __LINE__);

                Mutex::Autolock lock(slarMapLock);
                slarMap[jresultToHashCode] = resultTo;
            }
            else {
                ALOGE("nativeStartActivityAsCaller() new CLocalActivityRecordNative fail ");
            }
        }
        else {
            ALOGE("nativeStartActivityAsCaller() what is this resultTo ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(larWClass);
    }

    Int32 result = 0;
    am->StartActivityAsCaller(appThread, callingPackage, intent, resolvedType, resultTo, resultWho, (Int32)jrequestCode, (Int32)jstartFlags,
        profilerInfo, options, (Int32)juserId, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartActivityAsCaller()");
    return result;
}

static jobject android_app_ElActivityManagerProxy_nativeStartActivityAndWait(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcaller, jstring jcallingPackage, jobject jintent, jstring jresolvedType, jobject jresultTo, jstring jresultWho,
    jint jrequestCode, jint jstartFlags, jstring jprofileFile, jobject jprofilerInfo, jobject joptions, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartActivityAndWait()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeStartActivityAndWait() ToElIntent fail!\n");
        }
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeStartActivityAndWait() ToElBundle fail!\n");
        }
    }

    AutoPtr<IProfilerInfo> profilerInfo;
    if (jprofilerInfo != NULL) {
        if (!ElUtil::ToElProfilerInfo(env, jprofilerInfo, (IProfilerInfo**)&profilerInfo)) {
            ALOGE("nativeStartActivityAndWait() ToElProfilerInfo fail!\n");
        }
    }

    String resolvedType;
    if (jresolvedType != NULL) {
        resolvedType = ElUtil::ToElString(env, jresolvedType);
    }

    String resultWho;
    if (jresultWho != NULL) {
        resultWho = ElUtil::ToElString(env, jresultWho);
    }

    String profileFile;
    if (jprofileFile != NULL) {
        profileFile = ElUtil::ToElString(env, jprofileFile);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    IBinder* resultTo = NULL;
    if (jresultTo != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);

        if (env->IsInstanceOf(jresultTo, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            resultTo = (IBinder*)(Int64)env->GetLongField(jresultTo, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else if (env->IsInstanceOf(jresultTo, larWClass)) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jresultTo);

            AutoPtr<IBinder> resultTo;
            if (NOERROR == CLocalActivityRecordNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&resultTo)) {
                jclass objectClass = env->FindClass("java/lang/Object");
                ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

                jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
                ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);
                env->DeleteLocalRef(objectClass);

                jint jresultToHashCode = env->CallIntMethod(jresultTo, m);
                ElUtil::CheckErrorAndLog(env, "CallIntMethod hashCode : %d!\n", __LINE__);

                Mutex::Autolock lock(slarMapLock);
                slarMap[jresultToHashCode] = resultTo;
            }
            else {
                ALOGE("nativeStartActivityAndWait() new CLocalActivityRecordNative fail ");
            }
        }
        else {
            ALOGE("nativeStartActivityAndWait() what is this resultTo ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(larWClass);
    }

    AutoPtr<IActivityManagerWaitResult> result;
    am->StartActivityAndWait(appThread, callingPackage, intent, resolvedType, resultTo, resultWho, (Int32)jrequestCode, (Int32)jstartFlags,
        profilerInfo, options, (Int32)juserId, (IActivityManagerWaitResult**)&result);

    jobject jresult = NULL;
    if (result != NULL) {
        jresult = ElUtil::GetJavaWaitResult(env, result);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartActivityAndWait()");
    return jresult;
}

static int android_app_ElActivityManagerProxy_nativeStartActivityWithConfig(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcaller, jstring jcallingPackage, jobject jintent, jstring jresolvedType, jobject jresultTo, jstring jresultWho,
    jint jrequestCode, jint jstartFlags, jobject jconfig, jobject joptions, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartActivityWithConfig()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeStartActivityWithConfig() ToElIntent fail!\n");
        }
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeStartActivityWithConfig() ToElBundle fail!\n");
        }
    }

    AutoPtr<IConfiguration> config;
    if (jconfig != NULL) {
        if (!ElUtil::ToElConfiguration(env, jconfig, (IConfiguration**)&config)) {
            ALOGE("nativeStartActivityWithConfig() ToElConfiguration fail!\n");
        }
    }

    String resolvedType;
    if (jresolvedType != NULL) {
        resolvedType = ElUtil::ToElString(env, jresolvedType);
    }

    String resultWho;
    if (jresultWho != NULL) {
        resultWho = ElUtil::ToElString(env, jresultWho);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    IBinder* resultTo = NULL;
    if (jresultTo != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);

        if (env->IsInstanceOf(jresultTo, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            resultTo = (IBinder*)(Int64)env->GetLongField(jresultTo, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else if (env->IsInstanceOf(jresultTo, larWClass)) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jresultTo);

            AutoPtr<IBinder> resultTo;
            if (NOERROR == CLocalActivityRecordNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&resultTo)) {
                jclass objectClass = env->FindClass("java/lang/Object");
                ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

                jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
                ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);
                env->DeleteLocalRef(objectClass);

                jint jresultToHashCode = env->CallIntMethod(jresultTo, m);
                ElUtil::CheckErrorAndLog(env, "CallIntMethod hashCode : %d!\n", __LINE__);

                Mutex::Autolock lock(slarMapLock);
                slarMap[jresultToHashCode] = resultTo;
            }
            else {
                ALOGE("android_app_ElActivityManagerProxy_nativeStartActivity() new CLocalActivityRecordNative fail ");
            }
        }
        else {
            ALOGE("nativeStartActivityWithConfig() what is this resultTo ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(larWClass);
    }

    Int32 result = 0;
    am->StartActivityWithConfig(appThread, callingPackage, intent, resolvedType, resultTo, resultWho, (Int32)jrequestCode,
        (Int32)jstartFlags, config, options, (Int32)juserId, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartActivityWithConfig()");
    return result;
}

static jboolean android_app_ElActivityManagerProxy_nativeStartNextMatchingActivity(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcallingActivity, jobject jintent, jobject joptions)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartNextMatchingActivity()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcallingActivity);

    AutoPtr<IBinder> callingActivity;
    if (jcallingActivity != NULL) {
        ALOGE("nativeStartNextMatchingActivity() jcallingActivity not NULL!");
    }

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeStartNextMatchingActivity() ToElIntent fail!\n");
        }
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeStartNextMatchingActivity() ToElBundle fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->StartNextMatchingActivity(callingActivity, intent, options, &result);
    if (FAILED(ec))
        ALOGE("nativeStartNextMatchingActivity() ec = 0x%08x result=%d \n", ec, result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartNextMatchingActivity()");
    return (jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativeFinishSubActivity(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jstring jresultWho, jint jrequestCode)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeFinishSubActivity()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeFinishSubActivity() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    String resultWho = ElUtil::ToElString(env, jresultWho);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->FinishSubActivity(token, resultWho, (Int32)jrequestCode);
    if (FAILED(ec))
        ALOGE("nativeFinishSubActivity() ec = 0x%08x \n", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeFinishSubActivity()");
}

static jboolean android_app_ElActivityManagerProxy_nativeFinishActivityAffinity(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeFinishActivityAffinity()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeFinishActivityAffinity() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->FinishActivityAffinity(token, &result);
    if (FAILED(ec))
        ALOGE("nativeFinishActivityAffinity() ec = 0x%08x \n", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeFinishActivityAffinity()");
    return (jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativeFinishVoiceTask(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jsession)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeFinishVoiceTask()");

    AutoPtr<IIVoiceInteractionSession> session;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jsession);
    {
        Mutex::Autolock lock(sVoiceSessionsLock);
        HashMap<Int32, AutoPtr<IIVoiceInteractionSession> >::Iterator it = sVoiceSessions.Find(hashCode);
        if (it != sVoiceSessions.End()) {
            session = it->mSecond;
            sVoiceSessions.Erase(it);
        }
        else {
            ALOGE("nativeFinishVoiceTask() can't find session!");
            return;
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    ECode ec = am->FinishVoiceTask(session);
    if (FAILED(ec))
        ALOGE("nativeFinishVoiceTask() ec = 0x%08x \n", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeFinishVoiceTask()");
}


static jboolean android_app_ElActivityManagerProxy_nativeReleaseActivityInstance(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeReleaseActivityInstance()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeReleaseActivityInstance() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->ReleaseActivityInstance(token, &result);
    if (FAILED(ec))
        ALOGE("nativeReleaseActivityInstance() ec = 0x%08x \n", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeReleaseActivityInstance()");
    return (jboolean)result;
}


static void android_app_ElActivityManagerProxy_nativeReleaseSomeActivities(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject japp)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeReleaseSomeActivities()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, japp);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    ECode ec = am->ReleaseSomeActivities(appThread);
    if (FAILED(ec))
        ALOGE("nativeReleaseSomeActivities() ec = 0x%08x \n", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeReleaseSomeActivities()");
}

static void android_app_ElActivityManagerProxy_nativeUnbroadcastIntent(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcaller, jobject jintent, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnbroadcastIntent()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeStartNextMatchingActivity() ToElIntent fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->UnbroadcastIntent(appThread, intent, (Int32)juserId);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnbroadcastIntent()");
}

static void android_app_ElActivityManagerProxy_nativeActivitySlept(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeActivitySlept()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeActivitySlept() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->ActivitySlept(token);
    if (FAILED(ec))
        ALOGE("nativeActivitySlept() ec = 0x%08x \n", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeActivitySlept()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetCallingActivity(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetCallingActivity()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeActivitySlept() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IComponentName> result;
    ECode ec = am->GetCallingActivity(token, (IComponentName**)&result);
    if (FAILED(ec))
        ALOGE("nativeGetCallingActivity() ec = 0x%08x \n", ec);

    jobject jresult = NULL;
    if (result != NULL) {
        jresult = ElUtil::GetJavaComponentName(env, result);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetCallingActivity()");
    return jresult;
}

static jobject android_app_ElActivityManagerProxy_nativeGetRecentTasks(JNIEnv* env, jobject clazz, jlong jproxy, jint jmaxNum, jint jflags, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetRecentTasks()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IList> tasks;
    am->GetRecentTasks(jmaxNum, jflags, juserId, (IList**)&tasks);

    jobject jtasks = NULL;

    if (tasks != NULL){
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "nativeGetRecentTasks Fail FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "nativeGetRecentTasks Fail GetMethodID: ArrayList : %d!\n", __LINE__);

        jtasks = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "nativeGetRecentTasks Fail NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetRecentTasks Fail GetMethodID: add : %d!\n", __LINE__);

        Int32 size;
        tasks->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> value;
            tasks->Get(i, (IInterface**)&value);

            if (value != NULL){
                AutoPtr<IActivityManagerRecentTaskInfo> task = IActivityManagerRecentTaskInfo::Probe(value);
                jobject jtask = ElUtil::GetJavaRecentTaskInfo(env, task);

                if (jtask != NULL) {
                    env->CallBooleanMethod(jtasks, mAdd, jtask);
                    ElUtil::CheckErrorAndLog(env, "nativeGetRecentTasks Fail CallBooleanMethod: add : %d!\n", __LINE__);
                    env->DeleteLocalRef(jtask);
                }
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetRecentTasks()");
    return jtasks;
}

static jobject android_app_ElActivityManagerProxy_nativeGetTaskThumbnail(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jid)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetTaskThumbnail()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IActivityManagerTaskThumbnail> thumbnail;
    ECode ec = am->GetTaskThumbnail((Int32)jid, (IActivityManagerTaskThumbnail**)&thumbnail);
    if (FAILED(ec))
        ALOGE("nativeGetTaskThumbnail(), ec = %08x", ec);

    jobject jthumbnail = NULL;
    if (thumbnail != NULL) {
        jthumbnail = ElUtil::GetJavaTaskThumbnail(env, thumbnail);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetTaskThumbnail()");
    return jthumbnail;
}

static jobject android_app_ElActivityManagerProxy_nativeGetProcessesInErrorState(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetProcessesInErrorState()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = am->GetProcessesInErrorState((IList**)&list);
    if (FAILED(ec))
        ALOGE("nativeGetProcessesInErrorState(), ec = %08x", ec);

    jobject jstates = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jstates = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IActivityManagerProcessErrorStateInfo> pesInfo = IActivityManagerProcessErrorStateInfo::Probe(obj);
            if (pesInfo != NULL) {
                jobject jpesInfo = ElUtil::GetJavaProcessErrorStateInfo(env, pesInfo);
                env->CallBooleanMethod(jstates, mAdd, jpesInfo);
                ElUtil::CheckErrorAndLog(env, "CallBooleanMethod: add : %d!\n", __LINE__);
                env->DeleteLocalRef(jpesInfo);
            }else {
                ALOGE("nativeGetProcessesInErrorState() rtInfo is NULL!");
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetProcessesInErrorState()");
    return jstates;
}

static jobject android_app_ElActivityManagerProxy_nativeGetRunningExternalApplications(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetRunningExternalApplications()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = am->GetRunningExternalApplications((IList**)&list);
    if (FAILED(ec))
        ALOGE("nativeGetRunningExternalApplications(), ec = %08x", ec);

    jobject jexAppInfos = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jexAppInfos = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IApplicationInfo> appInfo = IApplicationInfo::Probe(obj);
            if (appInfo != NULL) {
                jobject jappInfo = ElUtil::GetJavaApplicationInfo(env, appInfo);
                env->CallBooleanMethod(jexAppInfos, mAdd, jappInfo);
                ElUtil::CheckErrorAndLog(env, "CallBooleanMethod: add : %d!\n", __LINE__);
                env->DeleteLocalRef(jappInfo);
            }
            else {
                ALOGE("nativeGetProcessesInErrorState() rtInfo is NULL!");
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetRunningExternalApplications()");
    return jexAppInfos;
}

static void android_app_ElActivityManagerProxy_nativeMoveTaskToFront(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtask, jint jflags, jobject joptions)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeMoveTaskToFront()");

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeMoveTaskToFront() ToElBundle fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->MoveTaskToFront((Int32)jtask, (Int32)jflags, options);
    if (FAILED(ec))
        ALOGE("nativeMoveTaskToFront(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeMoveTaskToFront()");
}

static void android_app_ElActivityManagerProxy_nativeMoveTaskToBack(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtask)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeMoveTaskToBack()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->MoveTaskToBack((Int32)jtask);
    if (FAILED(ec))
        ALOGE("nativeMoveTaskToBack(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeMoveTaskToBack()");
}

static void android_app_ElActivityManagerProxy_nativeMoveTaskBackwards(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtask)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeMoveTaskBackwards()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->MoveTaskBackwards((Int32)jtask);
    if (FAILED(ec))
        ALOGE("nativeMoveTaskBackwards(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeMoveTaskBackwards()");
}

static void android_app_ElActivityManagerProxy_nativeMoveTaskToStack(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtaskId, jint jstackId, jboolean jtoTop)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeMoveTaskToStack()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->MoveTaskToStack(jtaskId, jstackId, jtoTop);
    if (FAILED(ec))
        ALOGE("nativeMoveTaskToStack(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeMoveTaskToStack()");
}

static void android_app_ElActivityManagerProxy_nativeResizeStack(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstackId, jobject jbounds)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeResizeStack()");

    AutoPtr<IRect> bounds;
    if (jbounds != NULL) {
        if (!ElUtil::ToElRect(env, jbounds, (IRect**)&bounds))
            ALOGE("nativeResizeStack ToElRect fail");
    }
    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->ResizeStack(jstackId, bounds);
    if (FAILED(ec))
        ALOGE("nativeResizeStack(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeResizeStack()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetAllStackInfos(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtask)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetAllStackInfos()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IList> list;
    am->GetAllStackInfos((IList**)&list);
    jobject jstackInfos = NULL;
    if (list != NULL) {
        jclass listKlass = env->FindClass("java/util/ArrayList");
        ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

        jstackInfos = env->NewObject(listKlass, m);
        ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> obj;
            list->Get(i, (IInterface**)&obj);
            AutoPtr<IActivityManagerStackInfo> stackInfo = IActivityManagerStackInfo::Probe(obj);
            if (stackInfo != NULL) {
                jobject jstackInfo = ElUtil::GetJavaStackInfo(env, stackInfo);
                env->CallBooleanMethod(jstackInfos, mAdd, jstackInfo);
                ElUtil::CheckErrorAndLog(env, "CallBooleanMethod: add : %d!\n", __LINE__);
                env->DeleteLocalRef(jstackInfo);
            }
            else {
                ALOGE("nativeGetAllStackInfos() stackInfo is NULL!");
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetAllStackInfos()");

    return jstackInfos;
}

static jobject android_app_ElActivityManagerProxy_nativeGetStackInfo(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstackId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetStackInfo()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IActivityManagerStackInfo> stackInfo;
    ECode ec = am->GetStackInfo((Int32)jstackId, (IActivityManagerStackInfo**)&stackInfo);
    if (FAILED(ec))
        ALOGE("nativeGetStackInfo(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetStackInfo()");

    return ElUtil::GetJavaStackInfo(env, stackInfo);
}

static jboolean android_app_ElActivityManagerProxy_nativeIsInHomeStack(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtaskId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsInHomeStack()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    ECode ec = am->IsInHomeStack((Int32)jtaskId, &result);
    if (FAILED(ec))
        ALOGE("nativeIsInHomeStack(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsInHomeStack()");
    return result;
}

static void android_app_ElActivityManagerProxy_nativeSetFocusedStack(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstackId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetFocusedStack()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetFocusedStack((Int32)jstackId);
    if (FAILED(ec))
        ALOGE("nativeSetFocusedStack(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetFocusedStack()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetContentProviderExternal(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname, jint juserId, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetContentProviderExternal()");

    String name = ElUtil::ToElString(env, jname);

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeGetContentProviderExternal() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IContentProviderHolder> holder;
    ECode ec = am->GetContentProviderExternal(name, (Int32)juserId, token, (IContentProviderHolder**)&holder);
    if (FAILED(ec))
        ALOGE("nativeGetContentProviderExternal() , ec = %08x", ec);

    jobject jholder = NULL;
    if (holder != NULL) {
        jholder = ElUtil::GetJavaContentProviderHolder(env, holder);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetContentProviderExternal()");
    return jholder;
}

static void android_app_ElActivityManagerProxy_nativeUnstableProviderDied(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jconnection)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnstableProviderDied()");

    IBinder* connection = NULL;
    if (jconnection != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jconnection, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            connection = (IBinder*)(Int64)env->GetLongField(jconnection, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeRemoveContentProviderExternal() what is this connection ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->UnstableProviderDied(connection);
    if (FAILED(ec))
        ALOGE("nativeUnstableProviderDied() , ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnstableProviderDied()");
}

static void android_app_ElActivityManagerProxy_nativeAppNotRespondingViaProvider(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jconnection)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeAppNotRespondingViaProvider()");

    IBinder* connection = NULL;
    if (jconnection != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jconnection, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            connection = (IBinder*)(Int64)env->GetLongField(jconnection, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeRemoveContentProviderExternal() what is this connection ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->AppNotRespondingViaProvider(connection);
    if (FAILED(ec))
        ALOGE("nativeAppNotRespondingViaProvider() , ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeAppNotRespondingViaProvider()");
}

static void android_app_ElActivityManagerProxy_nativeRemoveContentProviderExternal(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRemoveContentProviderExternal()");

    String name = ElUtil::ToElString(env, jname);

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeRemoveContentProviderExternal() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->RemoveContentProviderExternal(name, token);
    if (FAILED(ec))
        ALOGE("nativeRemoveContentProviderExternal() , ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeRemoveContentProviderExternal()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetRunningServiceControlPanel(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jservice)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetRunningServiceControlPanel()");

    AutoPtr<IComponentName> service;
    if (jservice != NULL) {
        if (!ElUtil::ToElComponentName(env, jservice, (IComponentName**)&service)) {
            ALOGE("nativeGetRunningServiceControlPanel() ToElComponentName fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IPendingIntent> intent;
    ECode ec = am->GetRunningServiceControlPanel(service, (IPendingIntent**)&intent);
    if (FAILED(ec))
        ALOGE("nativeGetRunningServiceControlPanel() , ec = %08x", ec);

    jobject jintent = NULL;
    if (intent != NULL) {
        jintent = ElUtil::GetJavaPendingIntent(env, intent);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetRunningServiceControlPanel()");
    return jintent;
}

static void android_app_ElActivityManagerProxy_nativeUnbindFinished(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jobject jintent, jboolean jdoRebind)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnbindFinished()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/os/ElBinderProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElBinderProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeUnbindFinished() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeUnbindFinished() ToElIntent fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->UnbindFinished(token, intent, (Boolean)jdoRebind);
    if (FAILED(ec))
        ALOGE("nativeUnbindFinished() , ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnbindFinished()");
}

static jobject android_app_ElActivityManagerProxy_nativePeekService(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jservice, jstring jresolvedType)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativePeekService()");

    AutoPtr<IIntent> service;
    if (jservice != NULL) {
        if (!ElUtil::ToElIntent(env, jservice, (IIntent**)&service)) {
            ALOGE("nativePeekService() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IBinder> token;
    ECode ec = am->PeekService(service, resolvedType, (IBinder**)&token);
    if (FAILED(ec))
        ALOGE("nativePeekService() , ec = %08x", ec);

    jobject jtoken = NULL;
    if (token != NULL) {
        ALOGE("nativePeekService() token not NULL!");
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativePeekService()");
    return jtoken;
}

static jboolean android_app_ElActivityManagerProxy_nativeBindBackupAgent(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject japp, jint jbackupRestoreMode)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeBindBackupAgent()");

    AutoPtr<IApplicationInfo> app;
    if (japp != NULL) {
        if (!ElUtil::ToElApplicationInfo(env, japp, (IApplicationInfo**)&app)) {
            ALOGE("nativeBindBackupAgent() ToElApplicationInfo fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->BindBackupAgent(app, (Int32)jbackupRestoreMode, &result);
    if (FAILED(ec))
        ALOGE("nativeBindBackupAgent() , ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeBindBackupAgent()");
    return (jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativeClearPendingBackup(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeClearPendingBackup()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->ClearPendingBackup();
    if (FAILED(ec))
        ALOGE("nativeClearPendingBackup() , ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeClearPendingBackup()");
}

static void android_app_ElActivityManagerProxy_nativeBackupAgentCreated(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jobject jagent)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeBackupAgentCreated()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IBinder> agent;
    if (jagent != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jagent);

        jclass ibaClass = env->FindClass("android/app/IBackupAgent");
        ElUtil::CheckErrorAndLog(env, "FindClass IBackupAgent: %d!", __LINE__);

        if (env->IsInstanceOf(jagent, ibaClass)) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jagent);
            CIBackupAgentNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&agent);
        }
        else {
            ALOGE("nativeBackupAgentCreated() jagent NOT IBackupAgent!");
        }

        env->DeleteLocalRef(ibaClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    am->BackupAgentCreated(packageName, agent);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeBackupAgentCreated()");
}

static void android_app_ElActivityManagerProxy_nativeUnbindBackupAgent(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject japp)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnbindBackupAgent()");

    AutoPtr<IApplicationInfo> app;
    if (japp != NULL) {
        if (!ElUtil::ToElApplicationInfo(env, japp, (IApplicationInfo**)&app)) {
            ALOGE("nativeBindBackupAgent() ToElApplicationInfo fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->UnbindBackupAgent(app);
    if (FAILED(ec))
        ALOGE("nativeUnbindBackupAgent() , ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnbindBackupAgent()");
}

static jboolean android_app_ElActivityManagerProxy_nativeStartInstrumentation(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jclassName, jstring jprofileFile, jint jflags, jobject jarguments, jobject jwatcher,
    jobject jconnection, jint juserId, jstring jabiOverride)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartInstrumentation()");

    AutoPtr<IComponentName> className;
    if (jclassName != NULL) {
        if (!ElUtil::ToElComponentName(env, jclassName, (IComponentName**)&className)) {
            ALOGE("nativeStartInstrumentation() ToElComponentName fail!");
        }
    }

    String profileFile = ElUtil::ToElString(env, jprofileFile);

    AutoPtr<IBundle> arguments;
    if (jarguments != NULL) {
        if (!ElUtil::ToElBundle(env, jarguments, (IBundle**)&arguments)) {
            ALOGE("nativeStartInstrumentation() ToElBundle fail!\n");
        }
    }

    AutoPtr<IInstrumentationWatcher> watcher;
    if (jwatcher != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jwatcher);

        CIInstrumentationWatcherNative::New((Handle64)jvm, (Handle64)jInstance, (IInstrumentationWatcher**)&watcher);
    }

    AutoPtr<IIUiAutomationConnection> connection;
    if (jconnection != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jconnection);

        CIUiAutomationConnectionNative::New((Handle64)jvm, (Handle64)jInstance, (IIUiAutomationConnection**)&connection);
    }

    String abiOverride = ElUtil::ToElString(env, jabiOverride);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->StartInstrumentation(className, profileFile, (Int32)jflags, arguments,
        watcher, connection, (Int32)juserId, abiOverride, &result);
    if (FAILED(ec))
        ALOGE("nativeStartInstrumentation() , ec = %08x result: %d", ec, result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartInstrumentation()");
    return result;
}

static void android_app_ElActivityManagerProxy_nativeFinishInstrumentation(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtarget, jint jresultCode, jobject jresults)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeFinishInstrumentation()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtarget);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IBundle> results;
    if (jresults != NULL) {
        if (!ElUtil::ToElBundle(env, jresults, (IBundle**)&results)) {
            ALOGE("nativeFinishInstrumentation() ToElBundle fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->FinishInstrumentation(appThread, (Int32)jresultCode, results);
    if (FAILED(ec))
        ALOGE("nativeFinishInstrumentation() , ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeFinishInstrumentation()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetActivityClassForToken(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetActivityClassForToken()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeGetActivityClassForToken() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IComponentName> activityCls;
    ECode ec = am->GetActivityClassForToken(token, (IComponentName**)&activityCls);
    if (FAILED(ec))
        ALOGE("nativeGetActivityClassForToken() , ec = %08x", ec);

    jobject jactivityCls = NULL;
    if (activityCls != NULL) {
        jactivityCls = ElUtil::GetJavaComponentName(env, activityCls);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetActivityClassForToken()");
    return jactivityCls;
}

static jstring android_app_ElActivityManagerProxy_nativeGetPackageForToken(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetPackageForToken()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeGetPackageForToken() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    String pkg;
    ECode ec = am->GetPackageForToken(token, &pkg);
    if (FAILED(ec))
        ALOGE("nativeGetPackageForToken() , ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetPackageForToken()");
    return ElUtil::GetJavaString(env, pkg);
}

static void android_app_ElActivityManagerProxy_nativeCancelIntentSender(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jintentSender)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeCancelIntentSender()");

    AutoPtr<IIIntentSender> intentSender;
    if (jintentSender != NULL) {
        if (!ElUtil::ToElIIntentSender(env, jintentSender, (IIIntentSender**)&intentSender)) {
            ALOGE("nativeCancelIntentSender() ToElIIntentSender fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->CancelIntentSender(intentSender);
    if (FAILED(ec))
        ALOGE("nativeCancelIntentSender(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeCancelIntentSender()");
}

static jstring android_app_ElActivityManagerProxy_nativeGetPackageForIntentSender(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jintentSender)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetPackageForIntentSender()");

    AutoPtr<IIIntentSender> intentSender;
    if (jintentSender != NULL) {
        if (!ElUtil::ToElIIntentSender(env, jintentSender, (IIIntentSender**)&intentSender)) {
            ALOGE("nativeCancelIntentSender() ToElIIntentSender fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;

    String packageName;
    am->GetPackageForIntentSender(intentSender, &packageName);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetPackageForIntentSender()");
    return ElUtil::GetJavaString(env, packageName);
}

static jint android_app_ElActivityManagerProxy_nativeGetUidForIntentSender(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jintentSender)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetUidForIntentSender()");

    AutoPtr<IIIntentSender> iiSender;
    if (jintentSender != NULL) {
        if (!ElUtil::ToElIIntentSender(env, jintentSender, (IIIntentSender**)&iiSender)) {
            ALOGE("nativeCancelIntentSender() ToElIIntentSender fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 uid = 0;
    ECode ec = am->GetUidForIntentSender(iiSender, &uid);
    if (FAILED(ec))
        ALOGE("nativeGetUidForIntentSender(), ec = %08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetUidForIntentSender()");
    return (jint)uid;
}

static jint android_app_ElActivityManagerProxy_nativeHandleIncomingUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jcallingPid, jint jcallingUid, jint juserId, jboolean jallowAll, jboolean jrequireFull, jstring jname, jstring jcallerPackage)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeHandleIncomingUser()");

    String name = ElUtil::ToElString(env, jname);
    String callerPackage = ElUtil::ToElString(env, jcallerPackage);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 result = -10000; // USER_NULL
    ECode ec = am->HandleIncomingUser((Int32)jcallingPid, (Int32)jcallingUid, (Int32)juserId,
        (Boolean)jallowAll, (Boolean)jrequireFull, name, callerPackage, &result);
    if (FAILED(ec))
        ALOGE("nativeHandleIncomingUser() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeHandleIncomingUser()");
    return (jint)result;
}

static void android_app_ElActivityManagerProxy_nativeSetProcessLimit(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jmax)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetProcessLimit()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetProcessLimit((Int32)jmax);
    if (FAILED(ec))
        ALOGE("nativeSetProcessLimit() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetProcessLimit()");
}

static jint android_app_ElActivityManagerProxy_nativeGetProcessLimit(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetProcessLimit()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 limit = 0;
    ECode ec = am->GetProcessLimit(&limit);
    if (FAILED(ec))
        ALOGE("nativeGetProcessLimit() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetProcessLimit()");
    return (jint)limit;
}

static void android_app_ElActivityManagerProxy_nativeSetProcessForeground(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jint jpid, jboolean jisForeground)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetProcessForeground()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeSetProcessForeground() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetProcessForeground(token, (Int32)jpid, (Boolean)jisForeground);
    if (FAILED(ec))
        ALOGE("nativeSetProcessForeground() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetProcessForeground()");
}

static jint android_app_ElActivityManagerProxy_nativeCheckUriPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject juri, jint jpid, jint juid, jint jmode, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeCheckUriPermission()");

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeCheckUriPermission() ToElUri fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 permission = -1; // PERMISSION_DENIED
    ECode ec = am->CheckUriPermission(uri, (Int32)jpid, (Int32)juid, (Int32)jmode, juserId, &permission);
    if (FAILED(ec))
        ALOGE("nativeCheckUriPermission() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeCheckUriPermission()");
    return (jint)permission;
}

static void android_app_ElActivityManagerProxy_nativeGrantUriPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcaller, jstring jtargetPkg, jobject juri, jint jmode, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGrantUriPermission()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    String targetPkg = ElUtil::ToElString(env, jtargetPkg);

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeGrantUriPermission() ToElUri fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->GrantUriPermission(appThread, targetPkg, uri, (Int32)jmode, juserId);
    if (FAILED(ec))
        ALOGE("nativeGrantUriPermission() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGrantUriPermission()");
}

static void android_app_ElActivityManagerProxy_nativeRevokeUriPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcaller, jobject juri, jint jmode, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRevokeUriPermission()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeRevokeUriPermission() ToElUri fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->RevokeUriPermission(appThread, uri, (Int32)jmode, juserId);
    if (FAILED(ec))
        ALOGE("nativeRevokeUriPermission() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeRevokeUriPermission()");
}

static void android_app_ElActivityManagerProxy_nativeTakePersistableUriPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject juri, jint jmodeFlags, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeTakePersistableUriPermission()");

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeTakePersistableUriPermission() ToElUri fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->TakePersistableUriPermission(uri, (Int32)jmodeFlags, juserId);
    if (FAILED(ec))
        ALOGE("nativeTakePersistableUriPermission() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeTakePersistableUriPermission()");
}


static void android_app_ElActivityManagerProxy_nativeReleasePersistableUriPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject juri, jint jmodeFlags, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeReleasePersistableUriPermission()");

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeReleasePersistableUriPermission() ToElUri fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->ReleasePersistableUriPermission(uri, (Int32)jmodeFlags, juserId);
    if (FAILED(ec))
        ALOGE("nativeReleasePersistableUriPermission() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeReleasePersistableUriPermission()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetPersistedUriPermissions(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jboolean jincoming)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetPersistedUriPermissions()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IParceledListSlice> permissions;
    ECode ec = am->GetPersistedUriPermissions(packageName, jincoming, (IParceledListSlice**)&permissions);
    if (FAILED(ec))
        ALOGE("nativeGetPersistedUriPermissions() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetPersistedUriPermissions()");
    return ElUtil::GetJavaParceledListSlice(env, ECLSID_CUriPermission, permissions);
}

static void android_app_ElActivityManagerProxy_nativeShowWaitingForDebugger(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jboolean jwaiting)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeShowWaitingForDebugger()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jwho);

    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->ShowWaitingForDebugger(appThread,(Boolean)jwaiting);
    if (FAILED(ec))
        ALOGE("nativeShowWaitingForDebugger() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeShowWaitingForDebugger()");
}

static void android_app_ElActivityManagerProxy_nativeUnhandledBack(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnhandledBack()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->UnhandledBack();
    if (FAILED(ec))
        ALOGE("nativeUnhandledBack() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnhandledBack()");
}

static jobject android_app_ElActivityManagerProxy_nativeOpenContentUri(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject juri)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeOpenContentUri()");

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeOpenContentUri() ToElUri fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IParcelFileDescriptor> pfd;
    ECode ec = am->OpenContentUri(uri, (IParcelFileDescriptor**)&pfd);
    if (FAILED(ec))
        ALOGE("nativeOpenContentUri() ec:0x%08x", ec);

    jobject jpfd = NULL;
    if (pfd != NULL) {
        jpfd = ElUtil::GetJavaParcelFileDescriptor(env, pfd);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeOpenContentUri()");
    return jpfd;
}

static void android_app_ElActivityManagerProxy_nativeSetLockScreenShown(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jshown)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetLockScreenShown()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetLockScreenShown((Boolean)jshown);
    if (FAILED(ec))
        ALOGE("nativeSetLockScreenShown() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetLockScreenShown()");
}

static void android_app_ElActivityManagerProxy_nativeSetDebugApp(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jboolean jwaitForDebugger, jboolean jpersistent)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetDebugApp()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetDebugApp(packageName, (Boolean)jwaitForDebugger, (Boolean)jpersistent);
    if (FAILED(ec))
        ALOGE("nativeSetDebugApp() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetDebugApp()");
}

static void android_app_ElActivityManagerProxy_nativeSetAlwaysFinish(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jenabled)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetAlwaysFinish()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetAlwaysFinish((Boolean)jenabled);
    if (FAILED(ec))
        ALOGE("nativeSetAlwaysFinish() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetAlwaysFinish()");
}

static void android_app_ElActivityManagerProxy_nativeSetActivityController(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwatcher)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetActivityController()");

    AutoPtr<IIActivityController> watcher;
    if (jwatcher != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jwatcher);

        CIActivityControllerNative::New((Handle64)jvm, (Handle64)jInstance, (IIActivityController**)&watcher);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetActivityController(watcher);
    if (FAILED(ec))
        ALOGE("nativeSetActivityController() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetActivityController()");
}

static void android_app_ElActivityManagerProxy_nativeEnterSafeMode(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeEnterSafeMode()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->EnterSafeMode();
    if (FAILED(ec))
        ALOGE("nativeEnterSafeMode() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeEnterSafeMode()");
}

static void android_app_ElActivityManagerProxy_nativeNoteWakeupAlarm(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jsender, jint jsourceUid, jstring jsourcePkg)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeNoteWakeupAlarm()");

    AutoPtr<IIIntentSender> sender;
    if (jsender != NULL) {
        if (!ElUtil::ToElIIntentSender(env, jsender, (IIIntentSender**)&sender)) {
            ALOGE("nativeNoteWakeupAlarm() ToElIIntentSender fail!");
        }
    }

    String sourcePkg = ElUtil::ToElString(env, jsourcePkg);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->NoteWakeupAlarm(sender, jsourceUid, sourcePkg);
    if (FAILED(ec))
        ALOGE("nativeNoteWakeupAlarm() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeNoteWakeupAlarm()");
}

static jboolean android_app_ElActivityManagerProxy_nativeKillPids(JNIEnv* env, jobject clazz, jlong jproxy,
    jintArray jpids, jstring jreason, jboolean jsecure)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeKillPids()");

    AutoPtr<ArrayOf<Int32> > pids;
    if (jpids != NULL) {
        jint size = env->GetArrayLength(jpids);
        ElUtil::CheckErrorAndLog(env, "nativeKillPids() GetArrayLength:(): %d!\n", __LINE__);

        jint* jpayload = (jint*)malloc(size * sizeof(Int32));

        env->GetIntArrayRegion(jpids, 0, size, jpayload);
        ElUtil::CheckErrorAndLog(env, "nativeKillPids() GetIntArrayRegion:(): %d!\n", __LINE__);
        pids = ArrayOf<Int32>::Alloc((Int32*)jpayload, size);
        free(jpayload);
    }

    String reason = ElUtil::ToElString(env, jreason);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->KillPids(pids, reason, (Boolean)jsecure, &result);
    if (FAILED(ec))
        ALOGE("nativeKillPids() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeKillPids()");
    return (jboolean)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeKillProcessesBelowForeground(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jreason)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeKillProcessesBelowForeground()");

    String reason = ElUtil::ToElString(env, jreason);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->KillProcessesBelowForeground(reason, &result);
    if (FAILED(ec))
        ALOGE("nativeKillProcessesBelowForeground() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeKillProcessesBelowForeground()");
    return (jboolean)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeHandleApplicationWtf(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject japp, jstring jtag, jboolean jsystem, jobject jcrashInfo)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeHandleApplicationWtf()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, japp);

    AutoPtr<IBinder> app;
    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("nativeHandleApplicationWtf() Invalid app!\n");
        env->ExceptionDescribe();
    }

    app = IBinder::Probe(appThread);

    String tag = ElUtil::ToElString(env, jtag);

    AutoPtr<IApplicationErrorReportCrashInfo> crashInfo;
    if (jcrashInfo != NULL) {
        if (!ElUtil::ToElApplicationErrorReportCrashInfo(env, jcrashInfo, (IApplicationErrorReportCrashInfo**)&crashInfo)) {
            ALOGE("nativeHandleApplicationWtf() ToElApplicationErrorReportCrashInfo fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->HandleApplicationWtf(app, tag, jsystem, crashInfo, &result);
    if (FAILED(ec))
        ALOGE("nativeHandleApplicationWtf() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeHandleApplicationWtf()");
    return (jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativeHandleApplicationStrictModeViolation(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject japp, jint jviolationMask, jobject jinfo)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeHandleApplicationStrictModeViolation()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, japp);

    AutoPtr<IBinder> app;
    AutoPtr<IApplicationThread> appThread;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            appThread = it->mSecond;
        }
    }

    if (NULL == appThread) {
        ALOGE("nativeHandleApplicationStrictModeViolation() Invalid app!\n");
        env->ExceptionDescribe();
    }

    app = IBinder::Probe(appThread);

    AutoPtr<IStrictModeViolationInfo> info;
    if (jinfo != NULL) {
        if (!ElUtil::ToElStrictModeViolationInfo(env, jinfo, (IStrictModeViolationInfo**)&info)) {
            ALOGE("nativeHandleApplicationWtf() ToElApplicationErrorReportCrashInfo fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->HandleApplicationStrictModeViolation(app, (Int32)jviolationMask, info);
    if (FAILED(ec))
        ALOGE("nativeHandleApplicationStrictModeViolation() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeHandleApplicationStrictModeViolation()");
}

static void android_app_ElActivityManagerProxy_nativeSignalPersistentProcesses(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jsig)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSignalPersistentProcesses()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SignalPersistentProcesses((Int32)jsig);
    if (FAILED(ec))
        ALOGE("nativeSignalPersistentProcesses() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSignalPersistentProcesses()");
}

static void android_app_ElActivityManagerProxy_nativeKillAllBackgroundProcesses(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeKillAllBackgroundProcesses()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->KillAllBackgroundProcesses();
    if (FAILED(ec))
        ALOGE("nativeKillAllBackgroundProcesses() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeKillAllBackgroundProcesses()");
}

static void android_app_ElActivityManagerProxy_nativeGetMyMemoryState(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject joutInfo)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetMyMemoryState()");

    AutoPtr<IActivityManagerRunningAppProcessInfo> outInfo;
    CActivityManagerRunningAppProcessInfo::New((IActivityManagerRunningAppProcessInfo**)&outInfo);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->GetMyMemoryState(outInfo);
    if (FAILED(ec))
        ALOGE("nativeGetMyMemoryState() ec:0x%08x", ec);

    ElUtil::GetJavaRunningAppProcessInfo(env,joutInfo, outInfo);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetMyMemoryState()");
}

static jboolean android_app_ElActivityManagerProxy_nativeProfileControl(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jprocess, jint juserId, jboolean jstart, jobject jprofilerInfo, jint jprofileType)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeProfileControl()");

    String process = ElUtil::ToElString(env, jprocess);

    AutoPtr<IProfilerInfo> profilerInfo;
    if (jprofilerInfo != NULL) {
        if (!ElUtil::ToElProfilerInfo(env, jprofilerInfo, (IProfilerInfo**)&profilerInfo)) {
            ALOGE("nativeProfileControl() ToElProfilerInfo fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->ProfileControl(process, (Int32)juserId, (Boolean)jstart, profilerInfo, (Int32)jprofileType, &result);
    if (FAILED(ec))
        ALOGE("nativeProfileControl() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeProfileControl()");
    return (jboolean)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeShutdown(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtimeout)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeShutdown()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->Shutdown((Int32)jtimeout, &result);
    if (FAILED(ec))
        ALOGE("nativeShutdown() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeShutdown()");
    return (jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativeStopAppSwitches(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStopAppSwitches()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->StopAppSwitches();
    if (FAILED(ec))
        ALOGE("nativeStopAppSwitches() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStopAppSwitches()");
}

static void android_app_ElActivityManagerProxy_nativeResumeAppSwitches(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeResumeAppSwitches()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->ResumeAppSwitches();
    if (FAILED(ec))
        ALOGE("nativeResumeAppSwitches() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeResumeAppSwitches()");
}

static void android_app_ElActivityManagerProxy_nativeAddPackageDependency(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeAddPackageDependency()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->AddPackageDependency(packageName);
    if (FAILED(ec))
        ALOGE("nativeAddPackageDependency() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeAddPackageDependency()");
}

static void android_app_ElActivityManagerProxy_nativeKillApplicationWithAppId(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpkg, jint jappid, jstring jreason)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeKillApplicationWithAppId()");

    String pkg = ElUtil::ToElString(env, jpkg);
    String reason = ElUtil::ToElString(env, jreason);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->KillApplicationWithAppId(pkg, (Int32)jappid, reason);
    if (FAILED(ec))
        ALOGE("nativeKillApplicationWithAppId() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeKillApplicationWithAppId()");
}

static void android_app_ElActivityManagerProxy_nativeCloseSystemDialogs(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jreason)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeCloseSystemDialogs()");

    String reason = ElUtil::ToElString(env, jreason);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->CloseSystemDialogs(reason);
    if (FAILED(ec))
        ALOGE("nativeCloseSystemDialogs() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeCloseSystemDialogs()");
}

static void android_app_ElActivityManagerProxy_nativeKillApplicationProcess(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jprocessName, jint juid)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeKillApplicationProcess()");

    String processName = ElUtil::ToElString(env, jprocessName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->KillApplicationProcess(processName, (Int32)juid);
    if (FAILED(ec))
        ALOGE("nativeKillApplicationProcess() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeKillApplicationProcess()");
}

static void android_app_ElActivityManagerProxy_nativeSetUserIsMonkey(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean jmonkey)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetUserIsMonkey()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetUserIsMonkey(jmonkey);
    if (FAILED(ec))
        ALOGE("nativeSetUserIsMonkey() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetUserIsMonkey()");
}

static void android_app_ElActivityManagerProxy_nativeFinishHeavyWeightApp(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeFinishHeavyWeightApp()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->FinishHeavyWeightApp();
    if (FAILED(ec))
        ALOGE("nativeFinishHeavyWeightApp() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeFinishHeavyWeightApp()");
}

static jboolean android_app_ElActivityManagerProxy_nativeConvertFromTranslucent(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeConvertFromTranslucent()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeConvertFromTranslucent() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    ECode ec = am->ConvertFromTranslucent(token, &result);
    if (FAILED(ec))
        ALOGE("nativeConvertFromTranslucent() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeConvertFromTranslucent()");
    return result;
}

static jboolean android_app_ElActivityManagerProxy_nativeConvertToTranslucent(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jobject joptions)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeConvertToTranslucent()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeConvertToTranslucent() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IActivityOptions> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElActivityOptions(env, joptions, (IActivityOptions**)&options))
            ALOGE("nativeConvertToTranslucent: ToElActivityOptions fail");
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    ECode ec = am->ConvertToTranslucent(token, options, &result);
    if (FAILED(ec))
        ALOGE("nativeConvertToTranslucent() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeConvertToTranslucent()");
    return result;
}

static void android_app_ElActivityManagerProxy_nativeNotifyActivityDrawn(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeNotifyActivityDrawn()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeSetProcessForeground() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->NotifyActivityDrawn(token);
    if (FAILED(ec))
        ALOGE("nativeNotifyActivityDrawn() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeNotifyActivityDrawn()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetActivityOptions(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetActivityOptions()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeSetProcessForeground() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IActivityOptions> options;
    ECode ec = am->GetActivityOptions(token, (IActivityOptions**)&options);
    if (FAILED(ec))
        ALOGE("nativeGetActivityOptions() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetActivityOptions()");
    return ElUtil::GetJavaActivityOptions(env, options);
}

static void android_app_ElActivityManagerProxy_nativeBootAnimationComplete(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeBootAnimationComplete()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->BootAnimationComplete();
    if (FAILED(ec))
        ALOGE("nativeBootAnimationComplete() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeBootAnimationComplete()");
}

static void android_app_ElActivityManagerProxy_nativeSetImmersive(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jboolean jimmersive)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetImmersive()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeSetImmersive() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetImmersive(token, (Boolean)jimmersive);
    if (FAILED(ec))
        ALOGE("nativeSetImmersive() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetImmersive()");
}

static jboolean android_app_ElActivityManagerProxy_nativeIsImmersive(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsImmersive()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeIsImmersive() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->IsImmersive(token, &result);
    if (FAILED(ec))
        ALOGE("nativeIsImmersive() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsImmersive()");
    return(jboolean)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeIsTopActivityImmersive(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsImmersive()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->IsTopActivityImmersive(&result);
    if (FAILED(ec))
        ALOGE("nativeIsTopActivityImmersive() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsImmersive()");
    return(jboolean)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeIsTopOfTask(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsTopOfTask()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeIsTopOfTask() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->IsTopOfTask(token, &result);
    if (FAILED(ec))
        ALOGE("nativeIsTopOfTask() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsTopOfTask()");
    return(jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativeCrashApplication(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juid, jint jinitialPid, jstring jpackageName, jstring jmessage)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeCrashApplication()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    String message = ElUtil::ToElString(env, jmessage);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->CrashApplication((Int32)juid, (Int32)jinitialPid, packageName, message);
    if (FAILED(ec))
        ALOGE("nativeCrashApplication() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeCrashApplication()");
}

static jstring android_app_ElActivityManagerProxy_nativeGetProviderMimeType(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject juri, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetProviderMimeType()");

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeGetProviderMimeType() ToElUri fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    String type;
    ECode ec = am->GetProviderMimeType(uri, (Int32)juserId, &type);
    if (FAILED(ec))
        ALOGE("nativeGetProviderMimeType() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetProviderMimeType()");
    return ElUtil::GetJavaString(env, type);
}

static jobject android_app_ElActivityManagerProxy_nativeNewUriPermissionOwner(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jname)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeNewUriPermissionOwner()");

    String name = ElUtil::ToElString(env, jname);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IBinder> token;
    ECode ec = am->NewUriPermissionOwner(name, (IBinder**)&token);
    if (FAILED(ec))
        ALOGE("nativeNewUriPermissionOwner() ec:0x%08x", ec);

    jobject jtoken = NULL;
    if (token != NULL) {
        ALOGE("nativeNewUriPermissionOwner() token not NULL!");
        AutoPtr<IObject> object = (IObject*)token->Probe(EIID_IObject);
        ClassID clsid;
        object->GetClassID(&clsid);
        DUMP_CLSID(clsid, "nativeNewUriPermissionOwner()");
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeNewUriPermissionOwner()");
    return jtoken;
}

static void android_app_ElActivityManagerProxy_nativeGrantUriPermissionFromOwner(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jowner, jint jfromUid, jstring jtargetPkg, jobject juri, jint jmode, jint jsourceUserId, jint jtargetUserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGrantUriPermissionFromOwner()");

    IBinder* owner = NULL;
    if (jowner != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jowner, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            owner = (IBinder*)(Int64)env->GetLongField(jowner, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeGrantUriPermissionFromOwner() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    String targetPkg = ElUtil::ToElString(env, jtargetPkg);

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeGrantUriPermissionFromOwner() ToElUri fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->GrantUriPermissionFromOwner(owner, (Int32)jfromUid, targetPkg, uri, (Int32)jmode, jsourceUserId, jtargetUserId);
    if (FAILED(ec))
        ALOGE("nativeGrantUriPermissionFromOwner() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGrantUriPermissionFromOwner()");
}

static void android_app_ElActivityManagerProxy_nativeRevokeUriPermissionFromOwner(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jowner, jobject juri, jint jmode, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRevokeUriPermissionFromOwner()");

    IBinder* owner = NULL;
    if (jowner != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jowner, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            owner = (IBinder*)(Int64)env->GetLongField(jowner, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeRevokeUriPermissionFromOwner() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeRevokeUriPermissionFromOwner() ToElUri fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->RevokeUriPermissionFromOwner(owner, uri, (Int32)jmode, juserId);
    if (FAILED(ec))
        ALOGE("nativeRevokeUriPermissionFromOwner() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeRevokeUriPermissionFromOwner()");
}

static jint android_app_ElActivityManagerProxy_nativeCheckGrantUriPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jcallingUid, jstring jtargetPkg, jobject juri, jint jmodeFlags, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeCheckGrantUriPermission()");

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeCheckGrantUriPermission() ToElUri fail!\n");
        }
    }

    String targetPkg = ElUtil::ToElString(env, jtargetPkg);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 permission = -1; // PERMISSION_DENIED
    ECode ec = am->CheckGrantUriPermission((Int32)jcallingUid, targetPkg, uri, (Int32)jmodeFlags, juserId, &permission);
    if (FAILED(ec))
        ALOGE("nativeCheckGrantUriPermission() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeCheckGrantUriPermission()");
    return (jint)permission;
}

static jboolean android_app_ElActivityManagerProxy_nativeDumpHeap(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jprocess, jint juserId, jboolean jmanaged, jstring jpath, jobject jfd)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeDumpHeap()");

    String process = ElUtil::ToElString(env, jprocess);
    String path = ElUtil::ToElString(env, jpath);

    AutoPtr<IParcelFileDescriptor> fd;
    if (jfd != NULL) {
        if (!ElUtil::ToElParcelFileDescriptor(env, jfd, (IParcelFileDescriptor**)&fd)) {
            ALOGE("nativeDumpHeap() ToElParcelFileDescriptor fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE; // PERMISSION_DENIED
    ECode ec = am->DumpHeap(process, (Int32)juserId, (Boolean)jmanaged, path, fd, &result);
    if (FAILED(ec))
        ALOGE("nativeDumpHeap() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeDumpHeap()");
    return (jboolean)result;
}

static jint android_app_ElActivityManagerProxy_nativeStartActivities(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcaller, jstring jcallingPackage, jobjectArray jintents, jobjectArray jresolvedTypes, jobject jresultTo, jobject joptions, jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartActivities()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jcaller);

    AutoPtr<IApplicationThread> caller;
    {
        Mutex::Autolock lock(sAppThreadLock);
        HashMap<Int32, AutoPtr<IApplicationThread> >::Iterator it = sAppThread.Find(hashCode);
        if (it != sAppThread.End()) {
            caller = it->mSecond;
        }
    }

    if (NULL == caller) {
        ALOGE("nativeStartActivities() Invalid IApplicationThread!\n");
        env->ExceptionDescribe();
    }

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<ArrayOf<IIntent*> > intents;
    if (jintents != NULL) {
        int size = env->GetArrayLength(jintents);
        intents = ArrayOf<IIntent*>::Alloc(size);

        for(int i = 0; i < size; i++){
            jobject jintent = env->GetObjectArrayElement(jintents, i);
            ElUtil::CheckErrorAndLog(env, "GetObjectArrayelement failed : %d!\n", __LINE__);
            AutoPtr<IIntent> intent;
            if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
                ALOGE("nativeStartActivities() ToElIntent fail!");
            }
            intents->Set(i, intent);
            env->DeleteLocalRef(jintent);
        }
    }

    AutoPtr<ArrayOf<String> > resolvedTypes;
    if (jresolvedTypes != NULL) {
        if (!ElUtil::ToElStringArray(env, jresolvedTypes, (ArrayOf<String>**)&resolvedTypes)) {
            ALOGE("nativeStartActivities() ToElStringArray fail!");
        }
    }

    IBinder* resultTo = NULL;
    if (jresultTo != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        jclass larWClass = env->FindClass("android/app/LocalActivityManager$LocalActivityRecord");
        ElUtil::CheckErrorAndLog(env, "FindClass LocalActivityManager$LocalActivityRecord fail : %d!\n", __LINE__);

        if (env->IsInstanceOf(jresultTo, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            resultTo = (IBinder*)(Int64)env->GetLongField(jresultTo, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else if (env->IsInstanceOf(jresultTo, larWClass)) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jresultTo);

            if (NOERROR == CLocalActivityRecordNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&resultTo)) {
                jclass objectClass = env->FindClass("java/lang/Object");
                ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

                jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
                ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);
                env->DeleteLocalRef(objectClass);

                jint jresultToHashCode = env->CallIntMethod(jresultTo, m);
                ElUtil::CheckErrorAndLog(env, "CallIntMethod hashCode : %d!\n", __LINE__);

                Mutex::Autolock lock(slarMapLock);
                slarMap[jresultToHashCode] = resultTo;
            }
            else {
                ALOGE("nativeStartActivities() new CLocalActivityRecordNative fail ");
            }
        }
        else {
            ALOGE("nativeStartActivities() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
        env->DeleteLocalRef(larWClass);
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if (!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeStartActivities() ToElBundle fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 result = 0;
    ECode ec = am->StartActivities(caller, callingPackage, intents, resolvedTypes, resultTo, options, (Int32)juserId, &result);
    if (FAILED(ec))
        ALOGE("nativeStartActivities() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartActivities()");
    return (jint)result;
}

static jint android_app_ElActivityManagerProxy_nativeGetFrontActivityScreenCompatMode(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetFrontActivityScreenCompatMode()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 mode = -3; // COMPAT_MODE_UNKNOWN
    ECode ec = am->GetFrontActivityScreenCompatMode(&mode);
    if (FAILED(ec))
        ALOGE("nativeGetFrontActivityScreenCompatMode() ec:0x%08x mode: %d", ec, mode);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetFrontActivityScreenCompatMode()");
    return (jint)mode;
}

static void android_app_ElActivityManagerProxy_nativeSetFrontActivityScreenCompatMode(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jmode)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetFrontActivityScreenCompatMode()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetFrontActivityScreenCompatMode((Int32)jmode);
    if (FAILED(ec))
        ALOGE("nativeSetFrontActivityScreenCompatMode() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetFrontActivityScreenCompatMode()");
}

static jint android_app_ElActivityManagerProxy_nativeGetPackageScreenCompatMode(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetPackageScreenCompatMode()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 mode = -3; // COMPAT_MODE_UNKNOWN
    ECode ec = am->GetPackageScreenCompatMode(packageName, &mode);
    if (FAILED(ec))
        ALOGE("nativeGetPackageScreenCompatMode() ec:0x%08x mode: %d", ec, mode);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetPackageScreenCompatMode()");
    return (jint)mode;
}

static void android_app_ElActivityManagerProxy_nativeSetPackageScreenCompatMode(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint jmode)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetPackageScreenCompatMode()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetPackageScreenCompatMode(packageName, (Int32)jmode);
    if (FAILED(ec))
        ALOGE("nativeSetPackageScreenCompatMode() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetPackageScreenCompatMode()");
}

static jboolean android_app_ElActivityManagerProxy_nativeGetPackageAskScreenCompat(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetPackageAskScreenCompat()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean ask = FALSE;
    ECode ec = am->GetPackageAskScreenCompat(packageName, &ask);
    if (FAILED(ec))
        ALOGE("nativeGetPackageAskScreenCompat() ec:0x%08x ask: %d", ec, ask);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetPackageAskScreenCompat()");
    return (jboolean)ask;
}

static void android_app_ElActivityManagerProxy_nativeSetPackageAskScreenCompat(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jboolean jask)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetPackageAskScreenCompat()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetPackageAskScreenCompat(packageName, (Boolean)jask);
    if (FAILED(ec))
        ALOGE("nativeSetPackageAskScreenCompat() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetPackageAskScreenCompat()");
}

static jboolean android_app_ElActivityManagerProxy_nativeSwitchUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSwitchUser()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->SwitchUser((Int32)juserId, &result);
    if (FAILED(ec))
        ALOGE("nativeSwitchUser() ec:0x%08x result: %d", ec, result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSwitchUser()");
    return (jboolean)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeStartUserInBackground(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartUserInBackground()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->StartUserInBackground((Int32)juserId, &result);
    if (FAILED(ec))
        ALOGE("nativeStartUserInBackground() ec:0x%08x result: %d", ec, result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartUserInBackground()");
    return (jboolean)result;
}

static jint android_app_ElActivityManagerProxy_nativeStopUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserId, jobject jcallback)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStopUser()");

    AutoPtr<IStopUserCallback> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);

        CIStopUserCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IStopUserCallback**)&callback);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 result = -1; // USER_OP_UNKNOWN_USER
    ECode ec = am->StopUser((Int32)juserId, callback, &result);
    if (FAILED(ec))
        ALOGE("nativeStopUser() ec:0x%08x result: %d", ec, result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStopUser()");
    return (jint)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeIsUserRunning(JNIEnv* env, jobject clazz, jlong jproxy,
    jint juserId, jboolean jorStopping)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsUserRunning()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->IsUserRunning((Int32)juserId, (Boolean)jorStopping, &result);
    if (FAILED(ec))
        ALOGE("nativeIsUserRunning() ec:0x%08x result: %d", ec, result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsUserRunning()");
    return (jboolean)result;
}

static jintArray android_app_ElActivityManagerProxy_nativeGetRunningUserIds(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetRunningUserIds()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<ArrayOf<Int32> > ids;
    ECode ec = am->GetRunningUserIds((ArrayOf<Int32>**)&ids);
    if (FAILED(ec))
        ALOGE("nativeGetRunningUserIds() ec:0x%08x", ec);

    jintArray jids = NULL;
    if (ids != NULL) {
        jids = ElUtil::GetJavaIntArray(env, ids);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetRunningUserIds()");
    return jids;
}

static jboolean android_app_ElActivityManagerProxy_nativeRemoveTask(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtaskId, jint jflags)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRemoveTask()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->RemoveTask((Int32)jtaskId, (Int32)jflags, &result);
    if (FAILED(ec))
        ALOGE("nativeRemoveTask() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeRemoveTask()");
    return (jboolean)result;
}

static void android_app_ElActivityManagerProxy_nativeRegisterProcessObserver( JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jobserver)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRegisterProcessObserver()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    AutoPtr<IIProcessObserver> observer;
    if (jobserver != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jobserver);
        jobject jInstance = env->NewGlobalRef(jobserver);
        CIProcessObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIProcessObserver**)&observer);
        Mutex::Autolock lock(sProcessObserverLock);
        sProcessObserver[hashCode] = observer;
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->RegisterProcessObserver(observer);
    if (FAILED(ec))
        ALOGE("nativeRegisterProcessObserver() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeRegisterProcessObserver()");
}

static void android_app_ElActivityManagerProxy_nativeUnregisterProcessObserver(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jobserver)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnregisterProcessObserver()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jobserver);
    AutoPtr<IIProcessObserver> observer;
    {
        Mutex::Autolock lock(sProcessObserverLock);
        HashMap<Int32, AutoPtr<IIProcessObserver> >::Iterator it = sProcessObserver.Find(hashCode);
        if (it != sProcessObserver.End()) {
            observer = it->mSecond;
            sProcessObserver.Erase(it);
        }
    }

    if (NULL == observer) {
        ALOGE("nativeUnregisterProcessObserver() Invalid IIntentReceiver!\n");
        env->ExceptionDescribe();
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->UnregisterProcessObserver(observer);
    if (FAILED(ec))
        ALOGE("nativeUnregisterProcessObserver() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnregisterProcessObserver()");
}

static jboolean android_app_ElActivityManagerProxy_nativeIsIntentSenderTargetedToPackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jsender)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsIntentSenderTargetedToPackage()");

    AutoPtr<IIIntentSender> sender;
    if (jsender != NULL) {
        if (!ElUtil::ToElIIntentSender(env, jsender, (IIIntentSender**)&sender)) {
            ALOGE("nativeIsIntentSenderTargetedToPackage() ToElIIntentSender fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    am->IsIntentSenderTargetedToPackage(sender, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsIntentSenderTargetedToPackage()");
    return (jboolean)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeIsIntentSenderAnActivity(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jsender)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsIntentSenderAnActivity()");

    AutoPtr<IIIntentSender> sender;
    if (jsender != NULL) {
        if (!ElUtil::ToElIIntentSender(env, jsender, (IIIntentSender**)&sender)) {
            ALOGE("nativeIsIntentSenderAnActivity() ToElIIntentSender fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->IsIntentSenderAnActivity(sender, &result);
    if (FAILED(ec))
        ALOGE("nativeIsIntentSenderAnActivity() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsIntentSenderAnActivity()");
    return (jboolean)result;
}

static jobject android_app_ElActivityManagerProxy_nativeGetIntentForIntentSender(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jsender)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetIntentForIntentSender()");

    AutoPtr<IIIntentSender> sender;
    if (jsender != NULL) {
        if (!ElUtil::ToElIIntentSender(env, jsender, (IIIntentSender**)&sender)) {
            ALOGE("nativeGetIntentForIntentSender() ToElIIntentSender fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IIntent> intent;
    ECode ec = am->GetIntentForIntentSender(sender, (IIntent**)&intent);
    if (FAILED(ec))
        ALOGE("nativeGetIntentForIntentSender() ec:0x%08x", ec);

    jobject jintent = NULL;
    if (intent != NULL) {
        jintent = ElUtil::GetJavaIntent(env, intent);
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetIntentForIntentSender()");
    return jintent;
}


static jstring android_app_ElActivityManagerProxy_nativeGetTagForIntentSender(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jsender, jstring jprefix)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetTagForIntentSender()");

    AutoPtr<IIIntentSender> sender;
    if (jsender != NULL) {
        if (!ElUtil::ToElIIntentSender(env, jsender, (IIIntentSender**)&sender)) {
            ALOGE("nativeGetTagForIntentSender() ToElIIntentSender fail!");
        }
    }

    String prefix = ElUtil::ToElString(env, jprefix);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    String tag;
    ECode ec = am->GetTagForIntentSender(sender, prefix, &tag);
    if (FAILED(ec))
        ALOGE("nativeGetTagForIntentSender() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetTagForIntentSender()");
    return ElUtil::GetJavaString(env, tag);
}

static void android_app_ElActivityManagerProxy_nativeShowBootMessage(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jmsg, jboolean jalways)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeShowBootMessage()");

    AutoPtr<ICharSequence> msg;
    if (jmsg != NULL) {
        if (!ElUtil::ToElCharSequence(env, jmsg, (ICharSequence**)&msg)) {
            ALOGE("nativeShowBootMessage() ToElCharSequence fail!");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->ShowBootMessage(msg, (Boolean)jalways);
    if (FAILED(ec))
        ALOGE("nativeShowBootMessage() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeShowBootMessage()");
}

static void android_app_ElActivityManagerProxy_nativeKeyguardWaitingForActivityDrawn(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jmsg, jboolean jalways)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeKeyguardWaitingForActivityDrawn()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->KeyguardWaitingForActivityDrawn();
    if (FAILED(ec))
        ALOGE("nativeKeyguardWaitingForActivityDrawn() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeKeyguardWaitingForActivityDrawn()");
}

static jboolean android_app_ElActivityManagerProxy_nativeShouldUpRecreateTask(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jstring jdestAffinity)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeShouldUpRecreateTask()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeShouldUpRecreateTask() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    String destAffinity = ElUtil::ToElString(env, jdestAffinity);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->ShouldUpRecreateTask(token, destAffinity, &result);
    if (FAILED(ec))
        ALOGE("nativeShouldUpRecreateTask() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeShouldUpRecreateTask()");
    return (jboolean)result;
}

static jboolean android_app_ElActivityManagerProxy_nativeNavigateUpTo(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jobject jtarget, jint jresultCode, jobject jresultData)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeNavigateUpTo()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeNavigateUpTo() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IIntent> target;
    if (jtarget != NULL) {
        if (!ElUtil::ToElIntent(env, jtarget, (IIntent**)&target)) {
            ALOGE("nativeNavigateUpTo() ToElIntent jtarget fail!\n");
        }
    }

    AutoPtr<IIntent> resultData;
    if (jresultData != NULL) {
        if (!ElUtil::ToElIntent(env, jresultData, (IIntent**)&resultData)) {
            ALOGE("nativeNavigateUpTo() ToElIntent jresultData fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = am->NavigateUpTo(token, target, (Int32)jresultCode, resultData, &result);
    if (FAILED(ec))
        ALOGE("nativeNavigateUpTo() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeNavigateUpTo()");
    return (jboolean)result;
}

static jint android_app_ElActivityManagerProxy_nativeGetLaunchedFromUid(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetLaunchedFromUid()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeActivityStopped() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 result = -1;
    am->GetLaunchedFromUid(token, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetLaunchedFromUid()");
    return result;
}

static jstring android_app_ElActivityManagerProxy_nativeGetLaunchedFromPackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetLaunchedFromPackage()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeActivityStopped() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    String result;
    am->GetLaunchedFromPackage(token, &result);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetLaunchedFromPackage()");
    return ElUtil::GetJavaString(env, result);
}

static void android_app_ElActivityManagerProxy_nativeRegisterUserSwitchObserver( JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jobserver)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRegisterUserSwitchObserver()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    AutoPtr<IIUserSwitchObserver> observer;
    if (jobserver != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jobserver);
        jobject jInstance = env->NewGlobalRef(jobserver);
        CIUserSwitchObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIUserSwitchObserver**)&observer);
        Mutex::Autolock lock(sUserSwitchObserverLock);
        sUserSwitchObserver[hashCode] = observer;
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->RegisterUserSwitchObserver(observer);
    if (FAILED(ec))
        ALOGE("nativeRegisterUserSwitchObserver() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeRegisterUserSwitchObserver()");
}

static void android_app_ElActivityManagerProxy_nativeUnregisterUserSwitchObserver(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jobserver)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnregisterUserSwitchObserver()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jobserver);
    AutoPtr<IIUserSwitchObserver> observer;
    {
        Mutex::Autolock lock(sUserSwitchObserverLock);
        HashMap<Int32, AutoPtr<IIUserSwitchObserver> >::Iterator it = sUserSwitchObserver.Find(hashCode);
        if (it != sUserSwitchObserver.End()) {
            observer = it->mSecond;
            sUserSwitchObserver.Erase(it);
        }
    }

    if (NULL == observer) {
        ALOGE("nativeUnregisterUserSwitchObserver() Invalid IIntentReceiver!\n");
        env->ExceptionDescribe();
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->UnregisterUserSwitchObserver(observer);
    if (FAILED(ec))
        ALOGE("nativeUnregisterUserSwitchObserver() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnregisterUserSwitchObserver()");
}

static void android_app_ElActivityManagerProxy_nativeRequestBugReport(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRequestBugReport()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->RequestBugReport();
    if (FAILED(ec))
        ALOGE("nativeRequestBugReport() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeRequestBugReport()");
}

static jlong android_app_ElActivityManagerProxy_nativeInputDispatchingTimedOut(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jpid, jboolean jaboveSystem, jstring jreason)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeUnregisterUserSwitchObserver()");

    String reason = ElUtil::ToElString(env, jreason);

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int64 timeout = 0;
    ECode ec = am->InputDispatchingTimedOut((Int32)jpid, (Boolean)jaboveSystem, reason, &timeout);
    if (FAILED(ec))
        ALOGE("nativeInputDispatchingTimedOut() ec:0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeUnregisterUserSwitchObserver()");
    return (jlong)timeout;
}

static jobject android_app_ElActivityManagerProxy_nativeGetAssistContextExtras(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jrequestType)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetAssistContextExtras()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IBundle> extras;
    ECode ec = am->GetAssistContextExtras(jrequestType, (IBundle**)&extras);
    if (FAILED(ec))
        ALOGE("nativeGetAssistContextExtras, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetAssistContextExtras()");
    return ElUtil::GetJavaBundle(env, extras);
}

static void android_app_ElActivityManagerProxy_nativeReportAssistContextExtras(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jobject jextras)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeReportAssistContextExtras()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeReportAssistContextExtras() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras))
            ALOGE("nativeReportAssistContextExtras: ToElBundle failed");
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->ReportAssistContextExtras(token, extras);
    if (FAILED(ec))
        ALOGE("nativeReportAssistContextExtras, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeReportAssistContextExtras()");
}

static jboolean android_app_ElActivityManagerProxy_nativeLaunchAssistIntent(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jintent, jint jrequestType, jstring jhint, jint juserHandle)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeLaunchAssistIntent()");

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent))
            ALOGE("nativeLaunchAssistIntent: ToElIntent fail");
    }
    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    ECode ec = am->LaunchAssistIntent(intent, jrequestType, ElUtil::ToElString(env, jhint), juserHandle, &result);
    if (FAILED(ec))
        ALOGE("nativeLaunchAssistIntent, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeLaunchAssistIntent()");
    return result;
}

static void android_app_ElActivityManagerProxy_nativeKillUid(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juid, jstring jreason)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeKillUid()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->KillUid(juid, ElUtil::ToElString(env, jreason));
    if (FAILED(ec))
        ALOGE("nativeKillUid, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeKillUid()");
}

static void android_app_ElActivityManagerProxy_nativeHang(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jboolean jallowRestart)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeHang()");

    IBinder* who = NULL;
    if (jwho != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jwho, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            who = (IBinder*)(Int64)env->GetLongField(jwho, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeHang() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->Hang(who, jallowRestart);
    if (FAILED(ec))
        ALOGE("nativeHang, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeHang()");
}

static void android_app_ElActivityManagerProxy_nativeReportActivityFullyDrawn(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeReportActivityFullyDrawn()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeReportActivityFullyDrawn() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->ReportActivityFullyDrawn(token);
    if (FAILED(ec))
        ALOGE("nativeReportActivityFullyDrawn, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeReportActivityFullyDrawn()");
}

static void android_app_ElActivityManagerProxy_nativeRestart(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRestart()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->Restart();
    if (FAILED(ec))
        ALOGE("nativeRestart, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeRestart()");
}

static void android_app_ElActivityManagerProxy_nativePerformIdleMaintenance(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativePerformIdleMaintenance()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->PerformIdleMaintenance();
    if (FAILED(ec))
        ALOGE("nativePerformIdleMaintenance, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativePerformIdleMaintenance()");
}

static jobject android_app_ElActivityManagerProxy_nativeCreateActivityContainer(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jparentActivityToken, jobject jcallback)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeCreateActivityContainer()");

    IBinder* parentActivityToken = NULL;
    if (jparentActivityToken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jparentActivityToken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            parentActivityToken = (IBinder*)(Int64)env->GetLongField(jparentActivityToken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeCreateActivityContainer() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IActivityContainerCallback> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);
        CIActivityContainerCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IActivityContainerCallback**)&callback);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IIActivityContainer> ac;
    ECode ec = am->CreateActivityContainer(parentActivityToken, callback, (IIActivityContainer**)&ac);
    if (FAILED(ec))
        ALOGE("nativeCreateActivityContainer, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeCreateActivityContainer()");

    return ElUtil::GetJavaIActivityContainer(env, ac);
}

static void android_app_ElActivityManagerProxy_nativeDeleteActivityContainer(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcontainer)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeDeleteActivityContainer()");

    IIActivityContainer* container = NULL;
    if (jcontainer != NULL) {
        jclass containerClass = env->FindClass("android/app/ElActivityContainerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElActivityContainerProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jcontainer, containerClass)) {
            jfieldID f = env->GetFieldID(containerClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            container = (IIActivityContainer*)(Int64)env->GetLongField(jcontainer, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeDeleteActivityContainer() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(containerClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->DeleteActivityContainer(container);
    if (FAILED(ec))
        ALOGE("nativeDeleteActivityContainer, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeDeleteActivityContainer()");
}

static jint android_app_ElActivityManagerProxy_nativeGetActivityDisplayId(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jactivityToken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetActivityDisplayId()");

    IBinder* token = NULL;
    if (jactivityToken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jactivityToken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jactivityToken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeGetActivityDisplayId() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Int32 id;
    ECode ec = am->GetActivityDisplayId(token, &id);
    if (FAILED(ec))
        ALOGE("nativeGetActivityDisplayId, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetActivityDisplayId()");
    return id;
}

static jobject android_app_ElActivityManagerProxy_nativeGetHomeActivityToken(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetHomeActivityToken()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IBinder> token;
    ECode ec = am->GetHomeActivityToken((IBinder**)&token);
    if (FAILED(ec))
        ALOGE("nativeGetHomeActivityToken, ec = 0x%08x", ec);

    jclass c = env->FindClass("android/view/ElApplicationTokenProxy");
    ElUtil::CheckErrorAndLog(env, "nativeGetHomeActivityToken FindClass: ElApplicationTokenProxy", __LINE__);

    jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
    ElUtil::CheckErrorAndLog(env, "nativeGetHomeActivityToken GetMethodID: ElApplicationTokenProxy", __LINE__);

    jobject jtoken = env->NewObject(c, m, (jlong)token.Get());
    ElUtil::CheckErrorAndLog(env, "nativeGetHomeActivityToken NewObject: ElApplicationTokenProxy", __LINE__);
    token->AddRef();

    env->DeleteLocalRef(c);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetHomeActivityToken()");
    return jtoken;
}

static void android_app_ElActivityManagerProxy_nativeStartLockTaskModeOnCurrent(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartLockTaskModeOnCurrent()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->StartLockTaskModeOnCurrent();
    if (FAILED(ec))
        ALOGE("nativeStartLockTaskModeOnCurrent, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartLockTaskModeOnCurrent()");
}

static void android_app_ElActivityManagerProxy_nativeStartLockTaskMode(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jtaskId)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartLockTaskMode()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->StartLockTaskMode(jtaskId);
    if (FAILED(ec))
        ALOGE("nativeStartLockTaskMode, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartLockTaskMode()");
}

static void android_app_ElActivityManagerProxy_nativeStartLockTaskModeEx(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStartLockTaskModeEx()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeStartLockTaskMode() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->StartLockTaskMode(token);
    if (FAILED(ec))
        ALOGE("nativeStartLockTaskModeEx, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStartLockTaskModeEx()");
}

static void android_app_ElActivityManagerProxy_nativeStopLockTaskMode(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStopLockTaskMode()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->StopLockTaskMode();
    if (FAILED(ec))
        ALOGE("nativeStopLockTaskMode, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStopLockTaskMode()");
}

static void android_app_ElActivityManagerProxy_nativeStopLockTaskModeOnCurrent(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeStopLockTaskModeOnCurrent()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->StopLockTaskModeOnCurrent();
    if (FAILED(ec))
        ALOGE("nativeStopLockTaskModeOnCurrent, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeStopLockTaskModeOnCurrent()");
}

static jboolean android_app_ElActivityManagerProxy_nativeIsInLockTaskMode(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsInLockTaskMode()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    ECode ec = am->IsInLockTaskMode(&result);
    if (FAILED(ec))
        ALOGE("nativeIsInLockTaskMode, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsInLockTaskMode()");
    return result;
}

static void android_app_ElActivityManagerProxy_nativeSetTaskDescription(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jobject jvalues)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeSetTaskDescription()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeSetTaskDescription() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    AutoPtr<IActivityManagerTaskDescription> values;
    if (jvalues != NULL) {
        if (!ElUtil::ToElTaskDescription(env, jvalues, (IActivityManagerTaskDescription**)&values)) {
            ALOGE("nativeSetTaskDescription() ToElTaskDescription fail!\n");
        }
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->SetTaskDescription(token, values);
    if (FAILED(ec))
        ALOGE("nativeSetTaskDescription, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeSetTaskDescription()");
}

static jobject android_app_ElActivityManagerProxy_nativeGetTaskDescriptionIcon(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jfilename)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeGetTaskDescriptionIcon()");

    IIActivityManager* am = (IIActivityManager*)jproxy;
    AutoPtr<IBitmap> icon;
    ECode ec = am->GetTaskDescriptionIcon(ElUtil::ToElString(env, jfilename), (IBitmap**)&icon);
    if (FAILED(ec))
        ALOGE("nativeGetTaskDescriptionIcon, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeGetTaskDescriptionIcon()");
    return ElUtil::GetJavaBitmap(env, icon);
}

static jboolean android_app_ElActivityManagerProxy_nativeRequestVisibleBehind(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken, jboolean jvisible)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeRequestVisibleBehind()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeRequestVisibleBehind() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    ECode ec = am->RequestVisibleBehind(token, jvisible, &result);
    if (FAILED(ec))
        ALOGE("nativeRequestVisibleBehind, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeRequestVisibleBehind()");
    return result;
}

static jboolean android_app_ElActivityManagerProxy_nativeIsBackgroundVisibleBehind(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeIsBackgroundVisibleBehind()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeIsBackgroundVisibleBehind() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    Boolean result;
    ECode ec = am->IsBackgroundVisibleBehind(token, &result);
    if (FAILED(ec))
        ALOGE("nativeIsBackgroundVisibleBehind, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeIsBackgroundVisibleBehind()");
    return result;
}

static void android_app_ElActivityManagerProxy_nativeBackgroundResourcesReleased(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeBackgroundResourcesReleased()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeBackgroundResourcesReleased() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->BackgroundResourcesReleased(token);
    if (FAILED(ec))
        ALOGE("nativeBackgroundResourcesReleased, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeBackgroundResourcesReleased()");
}

static void android_app_ElActivityManagerProxy_nativeNotifyLaunchTaskBehindComplete(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeNotifyLaunchTaskBehindComplete()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeNotifyLaunchTaskBehindComplete() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->NotifyLaunchTaskBehindComplete(token);
    if (FAILED(ec))
        ALOGE("nativeNotifyLaunchTaskBehindComplete, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeNotifyLaunchTaskBehindComplete()");
}

static void android_app_ElActivityManagerProxy_nativeNotifyEnterAnimationComplete(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtoken)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeNotifyEnterAnimationComplete()");

    IBinder* token = NULL;
    if (jtoken != NULL) {
        jclass tokenClass = env->FindClass("android/view/ElApplicationTokenProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass ElApplicationTokenProxy fail : %d!\n", __LINE__);
        if (env->IsInstanceOf(jtoken, tokenClass)) {
            jfieldID f = env->GetFieldID(tokenClass, "mNativeProxy", "J");
            ElUtil::CheckErrorAndLog(env, "GetFieldID mNativeProxy : %d!\n", __LINE__);

            token = (IBinder*)(Int64)env->GetLongField(jtoken, f);
            ElUtil::CheckErrorAndLog(env, "GetLongField mNativeProxy : %d!\n", __LINE__);

        }
        else {
            ALOGE("nativeNotifyEnterAnimationComplete() what is this token ?");
            assert(0);
        }

        env->DeleteLocalRef(tokenClass);
    }

    IIActivityManager* am = (IIActivityManager*)jproxy;
    ECode ec = am->NotifyEnterAnimationComplete(token);
    if (FAILED(ec))
        ALOGE("nativeNotifyEnterAnimationComplete, ec = 0x%08x", ec);

    // ALOGD("- android_app_ElActivityManagerProxy_nativeNotifyEnterAnimationComplete()");
}

static void android_app_ElActivityManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_app_ElActivityManagerProxy_nativeDestroy()");

    IIActivityManager* obj = (IIActivityManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_app_ElActivityManagerProxy_nativeDestroy()");
}

namespace android
{
static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeFinalize },
    { "nativeStartActivity",    "(JLandroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/app/ProfilerInfo;Landroid/os/Bundle;)I",
            (void*) android_app_ElActivityManagerProxy_nativeStartActivity },
    { "nativeStartActivityIntentSender",    "(JLandroid/app/IApplicationThread;Landroid/content/IntentSender;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)I",
            (void*) android_app_ElActivityManagerProxy_nativeStartActivityIntentSender },
    { "nativeStartVoiceActivity",    "(JLjava/lang/String;IILandroid/content/Intent;Ljava/lang/String;Landroid/service/voice/IVoiceInteractionSession;Lcom/android/internal/app/IVoiceInteractor;ILandroid/app/ProfilerInfo;Landroid/os/Bundle;I)I",
            (void*) android_app_ElActivityManagerProxy_nativeStartVoiceActivity },
    { "nativeAttachApplication",    "(JLandroid/app/IApplicationThread;I)V",
            (void*) android_app_ElActivityManagerProxy_nativeAttachApplication },
    { "nativeActivityPaused",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeActivityPaused },
    { "nativeStartActivityFromRecents",    "(JILandroid/os/Bundle;)I",
            (void*) android_app_ElActivityManagerProxy_nativeStartActivityFromRecents },
    { "nativeFinishActivity",    "(JLandroid/os/IBinder;ILandroid/content/Intent;Z)Z",
            (void*) android_app_ElActivityManagerProxy_nativeFinishActivity },
    { "nativeActivityDestroyed",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeActivityDestroyed },
    { "nativeWillActivityBeVisible",    "(JLandroid/os/IBinder;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeWillActivityBeVisible },
    { "nativeActivityResumed",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeActivityResumed },
    { "nativeActivityIdle",    "(JLandroid/os/IBinder;Landroid/content/res/Configuration;Z)V",
            (void*) android_app_ElActivityManagerProxy_nativeActivityIdle },
    { "nativePublishContentProviders",    "(JLandroid/app/IApplicationThread;Ljava/util/List;)V",
            (void*) android_app_ElActivityManagerProxy_nativePublishContentProviders },
    { "nativeRegisterReceiver",    "(JLandroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/IIntentReceiver;Landroid/content/IntentFilter;Ljava/lang/String;I)Landroid/content/Intent;",
            (void*) android_app_ElActivityManagerProxy_nativeRegisterReceiver },
    { "nativeUnregisterReceiver",    "(JI)V",
            (void*) android_app_ElActivityManagerProxy_nativeUnregisterReceiver },
    { "nativeGetContentProvider",    "(JLandroid/app/IApplicationThread;Ljava/lang/String;IZ)Landroid/app/IActivityManager$ContentProviderHolder;",
            (void*) android_app_ElActivityManagerProxy_nativeGetContentProvider },
    { "nativeSetRequestedOrientation",    "(JLandroid/os/IBinder;I)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetRequestedOrientation },
    { "nativeGetRequestedOrientation",    "(JLandroid/os/IBinder;)I",
            (void*) android_app_ElActivityManagerProxy_nativeGetRequestedOrientation },
    { "nativeActivityStopped",    "(JLandroid/os/IBinder;Landroid/os/Bundle;Landroid/os/PersistableBundle;Ljava/lang/CharSequence;)V",
            (void*) android_app_ElActivityManagerProxy_nativeActivityStopped },
    { "nativeGetMemoryInfo",    "(JLandroid/app/ActivityManager$MemoryInfo;)V",
            (void*) android_app_ElActivityManagerProxy_nativeGetMemoryInfo },
    { "nativeGetRunningAppProcesses",    "(J)Ljava/util/List;",
            (void*) android_app_ElActivityManagerProxy_nativeGetRunningAppProcesses },
    { "nativeGetServices",    "(JII)Ljava/util/List;",
            (void*) android_app_ElActivityManagerProxy_nativeGetServices },
    { "nativeCheckPermission",    "(JLjava/lang/String;II)I",
            (void*) android_app_ElActivityManagerProxy_nativeCheckPermission },
    { "nativeGetCurrentUser",    "(J)Landroid/content/pm/UserInfo;",
            (void*) android_app_ElActivityManagerProxy_nativeGetCurrentUser },
    { "nativeRefContentProvider",    "(JLandroid/os/IBinder;II)Z",
            (void*) android_app_ElActivityManagerProxy_nativeRefContentProvider },
    { "nativeBroadcastIntent",    "(JLandroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;IILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;IZZI)I",
            (void*) android_app_ElActivityManagerProxy_nativeBroadcastIntent },
    { "nativeHandleApplicationCrash",    "(JILandroid/app/ApplicationErrorReport$CrashInfo;)V",
            (void*) android_app_ElActivityManagerProxy_nativeHandleApplicationCrash },
    { "nativeStartService",    "(JLandroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)Landroid/content/ComponentName;",
            (void*) android_app_ElActivityManagerProxy_nativeStartService },
    { "nativeStopService",    "(JLandroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)I",
            (void*) android_app_ElActivityManagerProxy_nativeStopService },
    { "nativeServiceDoneExecuting",    "(JLandroid/os/IBinder;III)V",
            (void*) android_app_ElActivityManagerProxy_nativeServiceDoneExecuting },
    { "nativeGetIntentSender",    "(JILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;",
            (void*) android_app_ElActivityManagerProxy_nativeGetIntentSender },
    { "nativeMoveActivityTaskToBack",    "(JLandroid/os/IBinder;Z)Z",
            (void*) android_app_ElActivityManagerProxy_nativeMoveActivityTaskToBack },
    { "nativeGetConfiguration",    "(J)Landroid/content/res/Configuration;",
            (void*) android_app_ElActivityManagerProxy_nativeGetConfiguration },
    { "nativeSetServiceForeground",    "(JLandroid/content/ComponentName;Landroid/os/IBinder;ILandroid/app/Notification;Z)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetServiceForeground },
    { "nativeRemoveContentProvider",    "(JLandroid/os/IBinder;Z)V",
            (void*) android_app_ElActivityManagerProxy_nativeRemoveContentProvider },
    { "nativeUpdateConfiguration",    "(JLandroid/content/res/Configuration;)V",
            (void*) android_app_ElActivityManagerProxy_nativeUpdateConfiguration },
    { "nativeBindService",    "(JLandroid/app/IApplicationThread;Landroid/os/IBinder;Landroid/content/Intent;Ljava/lang/String;Landroid/app/IServiceConnection;II)I",
            (void*) android_app_ElActivityManagerProxy_nativeBindService },
    { "nativeUnbindService",    "(JLandroid/app/IServiceConnection;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeUnbindService },
    { "nativeGetDeviceConfigurationInfo",    "(J)Landroid/content/pm/ConfigurationInfo;",
            (void*) android_app_ElActivityManagerProxy_nativeGetDeviceConfigurationInfo },
    { "nativeStopServiceToken",    "(JLandroid/content/ComponentName;Landroid/os/IBinder;I)Z",
            (void*) android_app_ElActivityManagerProxy_nativeStopServiceToken },
    { "nativePublishService",    "(JLandroid/os/IBinder;Landroid/content/Intent;Landroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativePublishService },
    { "nativeGetAppTasks",    "(JLjava/lang/String;)Ljava/util/List;",
            (void*) android_app_ElActivityManagerProxy_nativeGetAppTasks },
    { "nativeAddAppTask",    "(JLandroid/os/IBinder;Landroid/content/Intent;Landroid/app/ActivityManager$TaskDescription;Landroid/graphics/Bitmap;)I",
            (void*) android_app_ElActivityManagerProxy_nativeAddAppTask },
    { "nativeGetAppTaskThumbnailSize",    "(J)Landroid/graphics/Point;",
            (void*) android_app_ElActivityManagerProxy_nativeGetAppTaskThumbnailSize },
    { "nativeGetTasks",    "(JII)Ljava/util/List;",
            (void*) android_app_ElActivityManagerProxy_nativeGetTasks },
    { "nativeFinishReceiver",    "(JLandroid/os/IBinder;ILjava/lang/String;Landroid/os/Bundle;ZI)V",
            (void*) android_app_ElActivityManagerProxy_nativeFinishReceiver },
    { "nativeGetCallingPackage",    "(JLandroid/os/IBinder;)Ljava/lang/String;",
            (void*) android_app_ElActivityManagerProxy_nativeGetCallingPackage },
    { "nativeGetCallingPackageForBroadcast",    "(JZ)Ljava/lang/String;",
            (void*) android_app_ElActivityManagerProxy_nativeGetCallingPackageForBroadcast },
    { "nativeGetTaskForActivity",    "(JLandroid/os/IBinder;Z)I",
            (void*) android_app_ElActivityManagerProxy_nativeGetTaskForActivity },
    { "nativeOverridePendingTransition",    "(JLandroid/os/IBinder;Ljava/lang/String;II)V",
            (void*) android_app_ElActivityManagerProxy_nativeOverridePendingTransition },
    { "nativeGetProcessMemoryInfo",    "(J[I)[Landroid/os/Debug$MemoryInfo;",
            (void*) android_app_ElActivityManagerProxy_nativeGetProcessMemoryInfo },
    { "nativeIsUserAMonkey",    "(J)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsUserAMonkey },
    { "nativeForceStopPackage",    "(JLjava/lang/String;I)V",
            (void*) android_app_ElActivityManagerProxy_nativeForceStopPackage },
    { "nativeUpdatePersistentConfiguration",    "(JLandroid/content/res/Configuration;)V",
            (void*) android_app_ElActivityManagerProxy_nativeUpdatePersistentConfiguration },
    { "nativeGetProcessPss",    "(J[I)[J",
            (void*) android_app_ElActivityManagerProxy_nativeGetProcessPss },
    { "nativeClearApplicationUserData",    "(JLjava/lang/String;Landroid/content/pm/IPackageDataObserver;I)Z",
            (void*) android_app_ElActivityManagerProxy_nativeClearApplicationUserData },
    { "nativeKillBackgroundProcesses",    "(JLjava/lang/String;I)V",
            (void*) android_app_ElActivityManagerProxy_nativeKillBackgroundProcesses },
    { "nativeStartActivityAsUser",    "(JLandroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/app/ProfilerInfo;Landroid/os/Bundle;I)I",
            (void*) android_app_ElActivityManagerProxy_nativeStartActivityAsUser },
    { "nativeStartActivityAsCaller",    "(JLandroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/app/ProfilerInfo;Landroid/os/Bundle;I)I",
            (void*) android_app_ElActivityManagerProxy_nativeStartActivityAsCaller },
    { "nativeStartActivityAndWait",    "(JLandroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/app/ProfilerInfo;Landroid/os/Bundle;I)Landroid/app/IActivityManager$WaitResult;",
            (void*) android_app_ElActivityManagerProxy_nativeStartActivityAndWait },
    { "nativeStartActivityWithConfig",    "(JLandroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/res/Configuration;Landroid/os/Bundle;I)I",
            (void*) android_app_ElActivityManagerProxy_nativeStartActivityWithConfig },
    { "nativeStartNextMatchingActivity",    "(JLandroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeStartNextMatchingActivity },
    { "nativeFinishSubActivity",    "(JLandroid/os/IBinder;Ljava/lang/String;I)V",
            (void*) android_app_ElActivityManagerProxy_nativeFinishSubActivity },
    { "nativeFinishActivityAffinity",    "(JLandroid/os/IBinder;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeFinishActivityAffinity },
    { "nativeFinishVoiceTask",    "(JLandroid/service/voice/IVoiceInteractionSession;)V",
            (void*) android_app_ElActivityManagerProxy_nativeFinishVoiceTask },
    { "nativeReleaseActivityInstance",    "(JLandroid/os/IBinder;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeReleaseActivityInstance },
    { "nativeReleaseSomeActivities",    "(JLandroid/app/IApplicationThread;)V",
            (void*) android_app_ElActivityManagerProxy_nativeReleaseSomeActivities },
    { "nativeUnbroadcastIntent",    "(JLandroid/app/IApplicationThread;Landroid/content/Intent;I)V",
            (void*) android_app_ElActivityManagerProxy_nativeUnbroadcastIntent },
    { "nativeActivitySlept",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeActivitySlept },
    { "nativeGetCallingActivity",    "(JLandroid/os/IBinder;)Landroid/content/ComponentName;",
            (void*) android_app_ElActivityManagerProxy_nativeGetCallingActivity },
    { "nativeGetRecentTasks",    "(JIII)Ljava/util/List;",
            (void*) android_app_ElActivityManagerProxy_nativeGetRecentTasks },
    { "nativeGetTaskThumbnail",    "(JI)Landroid/app/ActivityManager$TaskThumbnail;",
            (void*) android_app_ElActivityManagerProxy_nativeGetTaskThumbnail },
    { "nativeGetProcessesInErrorState",    "(J)Ljava/util/List;",
            (void*) android_app_ElActivityManagerProxy_nativeGetProcessesInErrorState },
    { "nativeGetRunningExternalApplications",    "(J)Ljava/util/List;",
            (void*) android_app_ElActivityManagerProxy_nativeGetRunningExternalApplications },
    { "nativeMoveTaskToFront",    "(JIILandroid/os/Bundle;)V",
            (void*) android_app_ElActivityManagerProxy_nativeMoveTaskToFront },
    { "nativeMoveTaskToBack",    "(JI)V",
            (void*) android_app_ElActivityManagerProxy_nativeMoveTaskToBack },
    { "nativeMoveTaskBackwards",    "(JI)V",
            (void*) android_app_ElActivityManagerProxy_nativeMoveTaskBackwards },
    { "nativeMoveTaskToStack",    "(JIIZ)V",
            (void*) android_app_ElActivityManagerProxy_nativeMoveTaskToStack },
    { "nativeResizeStack",    "(JILandroid/graphics/Rect;)V",
            (void*) android_app_ElActivityManagerProxy_nativeResizeStack },
    { "nativeGetAllStackInfos",    "(J)Ljava/util/List;",
            (void*) android_app_ElActivityManagerProxy_nativeGetAllStackInfos },
    { "nativeGetStackInfo",    "(JI)Landroid/app/ActivityManager$StackInfo;",
            (void*) android_app_ElActivityManagerProxy_nativeGetStackInfo },
    { "nativeIsInHomeStack",    "(JI)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsInHomeStack },
    { "nativeSetFocusedStack",    "(JI)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetFocusedStack },
    { "nativeGetContentProviderExternal",    "(JLjava/lang/String;ILandroid/os/IBinder;)Landroid/app/IActivityManager$ContentProviderHolder;",
            (void*) android_app_ElActivityManagerProxy_nativeGetContentProviderExternal },
    { "nativeUnstableProviderDied",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeUnstableProviderDied },
    { "nativeAppNotRespondingViaProvider",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeAppNotRespondingViaProvider },
    { "nativeRemoveContentProviderExternal",    "(JLjava/lang/String;Landroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeRemoveContentProviderExternal },
    { "nativeGetRunningServiceControlPanel",    "(JLandroid/content/ComponentName;)Landroid/app/PendingIntent;",
            (void*) android_app_ElActivityManagerProxy_nativeGetRunningServiceControlPanel },
    { "nativeUnbindFinished",    "(JLandroid/os/IBinder;Landroid/content/Intent;Z)V",
            (void*) android_app_ElActivityManagerProxy_nativeUnbindFinished },
    { "nativePeekService",    "(JLandroid/content/Intent;Ljava/lang/String;)Landroid/os/IBinder;",
            (void*) android_app_ElActivityManagerProxy_nativePeekService },
    { "nativeBindBackupAgent",    "(JLandroid/content/pm/ApplicationInfo;I)Z",
            (void*) android_app_ElActivityManagerProxy_nativeBindBackupAgent },
    { "nativeClearPendingBackup",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeClearPendingBackup },
    { "nativeBackupAgentCreated",    "(JLjava/lang/String;Landroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeBackupAgentCreated },
    { "nativeUnbindBackupAgent",    "(JLandroid/content/pm/ApplicationInfo;)V",
            (void*) android_app_ElActivityManagerProxy_nativeUnbindBackupAgent },
    { "nativeStartInstrumentation",    "(JLandroid/content/ComponentName;Ljava/lang/String;ILandroid/os/Bundle;Landroid/app/IInstrumentationWatcher;Landroid/app/IUiAutomationConnection;ILjava/lang/String;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeStartInstrumentation },
    { "nativeFinishInstrumentation",    "(JLandroid/app/IApplicationThread;ILandroid/os/Bundle;)V",
            (void*) android_app_ElActivityManagerProxy_nativeFinishInstrumentation },
    { "nativeGetActivityClassForToken",    "(JLandroid/os/IBinder;)Landroid/content/ComponentName;",
            (void*) android_app_ElActivityManagerProxy_nativeGetActivityClassForToken },
    { "nativeGetPackageForToken",    "(JLandroid/os/IBinder;)Ljava/lang/String;",
            (void*) android_app_ElActivityManagerProxy_nativeGetPackageForToken },
    { "nativeCancelIntentSender",    "(JLandroid/content/IIntentSender;)V",
            (void*) android_app_ElActivityManagerProxy_nativeCancelIntentSender },
    { "nativeGetPackageForIntentSender",    "(JLandroid/content/IIntentSender;)Ljava/lang/String;",
            (void*) android_app_ElActivityManagerProxy_nativeGetPackageForIntentSender },
    { "nativeGetUidForIntentSender",    "(JLandroid/content/IIntentSender;)I",
            (void*) android_app_ElActivityManagerProxy_nativeGetUidForIntentSender },
    { "nativeHandleIncomingUser",    "(JIIIZZLjava/lang/String;Ljava/lang/String;)I",
            (void*) android_app_ElActivityManagerProxy_nativeHandleIncomingUser },
    { "nativeSetProcessLimit",    "(JI)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetProcessLimit },
    { "nativeGetProcessLimit",    "(J)I",
            (void*) android_app_ElActivityManagerProxy_nativeGetProcessLimit },
    { "nativeSetProcessForeground",    "(JLandroid/os/IBinder;IZ)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetProcessForeground },
    { "nativeCheckUriPermission",    "(JLandroid/net/Uri;IIII)I",
            (void*) android_app_ElActivityManagerProxy_nativeCheckUriPermission },
    { "nativeGrantUriPermission",    "(JLandroid/app/IApplicationThread;Ljava/lang/String;Landroid/net/Uri;II)V",
            (void*) android_app_ElActivityManagerProxy_nativeGrantUriPermission },
    { "nativeRevokeUriPermission",    "(JLandroid/app/IApplicationThread;Landroid/net/Uri;II)V",
            (void*) android_app_ElActivityManagerProxy_nativeRevokeUriPermission },
    { "nativeTakePersistableUriPermission",    "(JLandroid/net/Uri;II)V",
            (void*) android_app_ElActivityManagerProxy_nativeTakePersistableUriPermission },
    { "nativeReleasePersistableUriPermission",    "(JLandroid/net/Uri;II)V",
            (void*) android_app_ElActivityManagerProxy_nativeReleasePersistableUriPermission },
    { "nativeGetPersistedUriPermissions",    "(JLjava/lang/String;Z)Landroid/content/pm/ParceledListSlice;",
            (void*) android_app_ElActivityManagerProxy_nativeGetPersistedUriPermissions },
    { "nativeShowWaitingForDebugger",    "(JLandroid/app/IApplicationThread;Z)V",
            (void*) android_app_ElActivityManagerProxy_nativeShowWaitingForDebugger },
    { "nativeUnhandledBack",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeUnhandledBack },
    { "nativeOpenContentUri",    "(JLandroid/net/Uri;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_app_ElActivityManagerProxy_nativeOpenContentUri },
    { "nativeSetLockScreenShown",    "(JZ)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetLockScreenShown },
    { "nativeSetDebugApp",    "(JLjava/lang/String;ZZ)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetDebugApp },
    { "nativeSetAlwaysFinish",    "(JZ)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetAlwaysFinish },
    { "nativeSetActivityController",    "(JLandroid/app/IActivityController;)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetActivityController },
    { "nativeEnterSafeMode",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeEnterSafeMode },
    { "nativeNoteWakeupAlarm",    "(JLandroid/content/IIntentSender;ILjava/lang/String;)V",
            (void*) android_app_ElActivityManagerProxy_nativeNoteWakeupAlarm },
    { "nativeKillPids",    "(J[ILjava/lang/String;Z)Z",
            (void*) android_app_ElActivityManagerProxy_nativeKillPids },
    { "nativeKillProcessesBelowForeground",    "(JLjava/lang/String;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeKillProcessesBelowForeground },
    { "nativeHandleApplicationWtf",    "(JLandroid/os/IBinder;Ljava/lang/String;ZLandroid/app/ApplicationErrorReport$CrashInfo;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeHandleApplicationWtf },
    { "nativeHandleApplicationStrictModeViolation",    "(JLandroid/os/IBinder;ILandroid/os/StrictMode$ViolationInfo;)V",
            (void*) android_app_ElActivityManagerProxy_nativeHandleApplicationStrictModeViolation },
    { "nativeSignalPersistentProcesses",    "(JI)V",
            (void*) android_app_ElActivityManagerProxy_nativeSignalPersistentProcesses },
    { "nativeKillAllBackgroundProcesses",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeKillAllBackgroundProcesses },
    { "nativeGetMyMemoryState",    "(JLandroid/app/ActivityManager$RunningAppProcessInfo;)V",
            (void*) android_app_ElActivityManagerProxy_nativeGetMyMemoryState },
    { "nativeProfileControl",    "(JLjava/lang/String;IZLandroid/app/ProfilerInfo;I)Z",
            (void*) android_app_ElActivityManagerProxy_nativeProfileControl },
    { "nativeShutdown",    "(JI)Z",
            (void*) android_app_ElActivityManagerProxy_nativeShutdown },
    { "nativeStopAppSwitches",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeStopAppSwitches },
    { "nativeResumeAppSwitches",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeResumeAppSwitches },
    { "nativeAddPackageDependency",    "(JLjava/lang/String;)V",
            (void*) android_app_ElActivityManagerProxy_nativeAddPackageDependency },
    { "nativeKillApplicationWithAppId",    "(JLjava/lang/String;ILjava/lang/String;)V",
            (void*) android_app_ElActivityManagerProxy_nativeKillApplicationWithAppId },
    { "nativeCloseSystemDialogs",    "(JLjava/lang/String;)V",
            (void*) android_app_ElActivityManagerProxy_nativeCloseSystemDialogs },
    { "nativeKillApplicationProcess",    "(JLjava/lang/String;I)V",
            (void*) android_app_ElActivityManagerProxy_nativeKillApplicationProcess },
    { "nativeSetUserIsMonkey",    "(JZ)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetUserIsMonkey },
    { "nativeFinishHeavyWeightApp",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeFinishHeavyWeightApp },
    { "nativeConvertFromTranslucent",    "(JLandroid/os/IBinder;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeConvertFromTranslucent },
    { "nativeConvertToTranslucent",    "(JLandroid/os/IBinder;Landroid/app/ActivityOptions;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeConvertToTranslucent },
    { "nativeNotifyActivityDrawn",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeNotifyActivityDrawn },
    { "nativeGetActivityOptions",    "(JLandroid/os/IBinder;)Landroid/app/ActivityOptions;",
            (void*) android_app_ElActivityManagerProxy_nativeGetActivityOptions },
    { "nativeBootAnimationComplete",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeBootAnimationComplete },
    { "nativeSetImmersive",    "(JLandroid/os/IBinder;Z)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetImmersive },
    { "nativeIsImmersive",    "(JLandroid/os/IBinder;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsImmersive },
    { "nativeIsTopActivityImmersive",    "(J)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsTopActivityImmersive },
    { "nativeIsTopOfTask",    "(JLandroid/os/IBinder;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsTopOfTask },
    { "nativeCrashApplication",    "(JIILjava/lang/String;Ljava/lang/String;)V",
            (void*) android_app_ElActivityManagerProxy_nativeCrashApplication },
    { "nativeGetProviderMimeType",    "(JLandroid/net/Uri;I)Ljava/lang/String;",
            (void*) android_app_ElActivityManagerProxy_nativeGetProviderMimeType },
    { "nativeNewUriPermissionOwner",    "(JLjava/lang/String;)Landroid/os/IBinder;",
            (void*) android_app_ElActivityManagerProxy_nativeNewUriPermissionOwner },
    { "nativeGrantUriPermissionFromOwner",    "(JLandroid/os/IBinder;ILjava/lang/String;Landroid/net/Uri;III)V",
            (void*) android_app_ElActivityManagerProxy_nativeGrantUriPermissionFromOwner },
    { "nativeRevokeUriPermissionFromOwner",    "(JLandroid/os/IBinder;Landroid/net/Uri;II)V",
            (void*) android_app_ElActivityManagerProxy_nativeRevokeUriPermissionFromOwner },
    { "nativeCheckGrantUriPermission",    "(JILjava/lang/String;Landroid/net/Uri;II)I",
            (void*) android_app_ElActivityManagerProxy_nativeCheckGrantUriPermission },
    { "nativeDumpHeap",    "(JLjava/lang/String;IZLjava/lang/String;Landroid/os/ParcelFileDescriptor;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeDumpHeap },
    { "nativeStartActivities",    "(JLandroid/app/IApplicationThread;Ljava/lang/String;[Landroid/content/Intent;[Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;I)I",
            (void*) android_app_ElActivityManagerProxy_nativeStartActivities },
    { "nativeGetFrontActivityScreenCompatMode",    "(J)I",
            (void*) android_app_ElActivityManagerProxy_nativeGetFrontActivityScreenCompatMode },
    { "nativeSetFrontActivityScreenCompatMode",    "(JI)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetFrontActivityScreenCompatMode },
    { "nativeGetPackageScreenCompatMode",    "(JLjava/lang/String;)I",
            (void*) android_app_ElActivityManagerProxy_nativeGetPackageScreenCompatMode },
    { "nativeSetPackageScreenCompatMode",    "(JLjava/lang/String;I)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetPackageScreenCompatMode },
    { "nativeGetPackageAskScreenCompat",    "(JLjava/lang/String;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeGetPackageAskScreenCompat },
    { "nativeSetPackageAskScreenCompat",    "(JLjava/lang/String;Z)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetPackageAskScreenCompat },
    { "nativeSwitchUser",    "(JI)Z",
            (void*) android_app_ElActivityManagerProxy_nativeSwitchUser },
    { "nativeStartUserInBackground",    "(JI)Z",
            (void*) android_app_ElActivityManagerProxy_nativeStartUserInBackground },
    { "nativeStopUser",    "(JILandroid/app/IStopUserCallback;)I",
            (void*) android_app_ElActivityManagerProxy_nativeStopUser },
    { "nativeIsUserRunning",    "(JIZ)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsUserRunning },
    { "nativeGetRunningUserIds",    "(J)[I",
            (void*) android_app_ElActivityManagerProxy_nativeGetRunningUserIds },
    { "nativeRemoveTask",    "(JII)Z",
            (void*) android_app_ElActivityManagerProxy_nativeRemoveTask },
    { "nativeRegisterProcessObserver",    "(JLandroid/app/IProcessObserver;)V",
            (void*) android_app_ElActivityManagerProxy_nativeRegisterProcessObserver },
    { "nativeUnregisterProcessObserver",    "(JLandroid/app/IProcessObserver;)V",
            (void*) android_app_ElActivityManagerProxy_nativeUnregisterProcessObserver },
    { "nativeIsIntentSenderTargetedToPackage",    "(JLandroid/content/IIntentSender;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsIntentSenderTargetedToPackage },
    { "nativeIsIntentSenderAnActivity",    "(JLandroid/content/IIntentSender;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsIntentSenderAnActivity },
    { "nativeGetIntentForIntentSender",    "(JLandroid/content/IIntentSender;)Landroid/content/Intent;",
            (void*) android_app_ElActivityManagerProxy_nativeGetIntentForIntentSender },
    { "nativeGetTagForIntentSender",    "(JLandroid/content/IIntentSender;Ljava/lang/String;)Ljava/lang/String;",
            (void*) android_app_ElActivityManagerProxy_nativeGetTagForIntentSender },
    { "nativeShowBootMessage",    "(JLjava/lang/CharSequence;Z)V",
            (void*) android_app_ElActivityManagerProxy_nativeShowBootMessage },
    { "nativeKeyguardWaitingForActivityDrawn",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeKeyguardWaitingForActivityDrawn },
    { "nativeShouldUpRecreateTask",    "(JLandroid/os/IBinder;Ljava/lang/String;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeShouldUpRecreateTask },
    { "nativeNavigateUpTo",    "(JLandroid/os/IBinder;Landroid/content/Intent;ILandroid/content/Intent;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeNavigateUpTo },
    { "nativeGetLaunchedFromUid",    "(JLandroid/os/IBinder;)I",
            (void*) android_app_ElActivityManagerProxy_nativeGetLaunchedFromUid },
    { "nativeGetLaunchedFromPackage",    "(JLandroid/os/IBinder;)Ljava/lang/String;",
            (void*) android_app_ElActivityManagerProxy_nativeGetLaunchedFromPackage },
    { "nativeRegisterUserSwitchObserver",    "(JLandroid/app/IUserSwitchObserver;)V",
            (void*) android_app_ElActivityManagerProxy_nativeRegisterUserSwitchObserver },
    { "nativeUnregisterUserSwitchObserver",    "(JLandroid/app/IUserSwitchObserver;)V",
            (void*) android_app_ElActivityManagerProxy_nativeUnregisterUserSwitchObserver },
    { "nativeRequestBugReport",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeRequestBugReport },
    { "nativeInputDispatchingTimedOut",    "(JIZLjava/lang/String;)J",
            (void*) android_app_ElActivityManagerProxy_nativeInputDispatchingTimedOut },
    { "nativeGetAssistContextExtras",    "(JI)Landroid/os/Bundle;",
            (void*) android_app_ElActivityManagerProxy_nativeGetAssistContextExtras },
    { "nativeReportAssistContextExtras",    "(JLandroid/os/IBinder;Landroid/os/Bundle;)V",
            (void*) android_app_ElActivityManagerProxy_nativeReportAssistContextExtras },
    { "nativeLaunchAssistIntent",    "(JLandroid/content/Intent;ILjava/lang/String;I)Z",
            (void*) android_app_ElActivityManagerProxy_nativeLaunchAssistIntent },
    { "nativeKillUid",    "(JILjava/lang/String;)V",
            (void*) android_app_ElActivityManagerProxy_nativeKillUid },
    { "nativeHang",    "(JLandroid/os/IBinder;Z)V",
            (void*) android_app_ElActivityManagerProxy_nativeHang },
    { "nativeReportActivityFullyDrawn",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeReportActivityFullyDrawn },
    { "nativeRestart",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeRestart },
    { "nativePerformIdleMaintenance",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativePerformIdleMaintenance },
    { "nativeCreateActivityContainer",    "(JLandroid/os/IBinder;Landroid/app/IActivityContainerCallback;)Landroid/app/IActivityContainer;",
            (void*) android_app_ElActivityManagerProxy_nativeCreateActivityContainer },
    { "nativeDeleteActivityContainer",    "(JLandroid/app/IActivityContainer;)V",
            (void*) android_app_ElActivityManagerProxy_nativeDeleteActivityContainer },
    { "nativeGetActivityDisplayId",    "(JLandroid/os/IBinder;)I",
            (void*) android_app_ElActivityManagerProxy_nativeGetActivityDisplayId },
    { "nativeGetHomeActivityToken",    "(J)Landroid/os/IBinder;",
            (void*) android_app_ElActivityManagerProxy_nativeGetHomeActivityToken },
    { "nativeStartLockTaskModeOnCurrent",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeStartLockTaskModeOnCurrent },
    { "nativeStartLockTaskMode",    "(JI)V",
            (void*) android_app_ElActivityManagerProxy_nativeStartLockTaskMode },
    { "nativeStartLockTaskMode",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeStartLockTaskModeEx },
    { "nativeStopLockTaskMode",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeStopLockTaskMode },
    { "nativeStopLockTaskModeOnCurrent",    "(J)V",
            (void*) android_app_ElActivityManagerProxy_nativeStopLockTaskModeOnCurrent },
    { "nativeIsInLockTaskMode",    "(J)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsInLockTaskMode },
    { "nativeSetTaskDescription",    "(JLandroid/os/IBinder;Landroid/app/ActivityManager$TaskDescription;)V",
            (void*) android_app_ElActivityManagerProxy_nativeSetTaskDescription },
    { "nativeGetTaskDescriptionIcon",    "(JLjava/lang/String;)Landroid/graphics/Bitmap;",
            (void*) android_app_ElActivityManagerProxy_nativeGetTaskDescriptionIcon },
    { "nativeRequestVisibleBehind",    "(JLandroid/os/IBinder;Z)Z",
            (void*) android_app_ElActivityManagerProxy_nativeRequestVisibleBehind },
    { "nativeIsBackgroundVisibleBehind",    "(JLandroid/os/IBinder;)Z",
            (void*) android_app_ElActivityManagerProxy_nativeIsBackgroundVisibleBehind },
    { "nativeBackgroundResourcesReleased",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeBackgroundResourcesReleased },
    { "nativeNotifyLaunchTaskBehindComplete",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeNotifyLaunchTaskBehindComplete },
    { "nativeNotifyEnterAnimationComplete",    "(JLandroid/os/IBinder;)V",
            (void*) android_app_ElActivityManagerProxy_nativeNotifyEnterAnimationComplete },
};

int register_android_app_ElActivityManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/app/ElActivityManagerProxy",
        gMethods, NELEM(gMethods));
}

};
