
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Net.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Net::IINetworkStatsService;
using Elastos::Droid::Net::IINetworkStatsSession;

static jobject android_net_ElNetworkStatsServiceProxy_nativeOpenSession(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkStatsServiceProxy_nativeOpenSession()");

    IINetworkStatsService* nss = (IINetworkStatsService*)jproxy;
    AutoPtr<IINetworkStatsSession> session;
    nss->OpenSession((IINetworkStatsSession**)&session);

    jobject jsession = NULL;
    if (session != NULL) {
        jclass c = env->FindClass("android/net/ElNetworkStatsSessionProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNetworkStatsSessionProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNetworkStatsSessionProxy : %d!\n", __LINE__);

        jsession = env->NewObject(c, m, (jlong)session.Get());
        ElUtil::CheckErrorAndLog(env, "FindClass: ElNetworkStatsSessionProxy : %d!\n", __LINE__);
        session->AddRef();
        env->DeleteLocalRef(c);
    }

    // ALOGD("- android_net_ElNetworkStatsServiceProxy_nativeOpenSession()");
    return jsession;
}

static jlong android_net_ElNetworkStatsServiceProxy_nativeGetNetworkTotalBytes(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jtemplate, jlong start, jlong end)
{
    // ALOGD("+ android_net_ElNetworkStatsServiceProxy_nativeGetNetworkTotalBytes()");

    AutoPtr<INetworkTemplate> templ;
    if (jtemplate != NULL) {
        if (!ElUtil::ToElNetworkTemplate(env, jtemplate, (INetworkTemplate**)&templ))
            ALOGE("nativeGetNetworkTotalBytes: ToElNetworkTemplate fail");
    }

    IINetworkStatsService* nss = (IINetworkStatsService*)jproxy;
    Int64 bytes;
    nss->GetNetworkTotalBytes(templ, start, end, &bytes);

    // ALOGD("- android_net_ElNetworkStatsServiceProxy_nativeGetNetworkTotalBytes()");
    return bytes;
}

static jobject android_net_ElNetworkStatsServiceProxy_nativeGetDataLayerSnapshotForUid(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid)
{
    // ALOGD("+ android_net_ElNetworkStatsServiceProxy_nativeGetDataLayerSnapshotForUid()");

    IINetworkStatsService* nss = (IINetworkStatsService*)jproxy;
    AutoPtr<INetworkStats> stats;
    nss->GetDataLayerSnapshotForUid(uid, (INetworkStats**)&stats);

    jobject jstats = NULL;
    if (stats != NULL) {
        jstats = ElUtil::GetJavaNetworkStats(env, stats);
    }

    // ALOGD("- android_net_ElNetworkStatsServiceProxy_nativeGetDataLayerSnapshotForUid()");
    return jstats;
}

static jobjectArray android_net_ElNetworkStatsServiceProxy_nativeGetMobileIfaces(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkStatsServiceProxy_nativeGetMobileIfaces()");

    IINetworkStatsService* nss = (IINetworkStatsService*)jproxy;
    AutoPtr<ArrayOf<String> > ifaces;
    nss->GetMobileIfaces((ArrayOf<String>**)&ifaces);

    jobjectArray jifaces = NULL;
    if (ifaces != NULL) {
        jifaces = ElUtil::GetJavaStringArray(env, ifaces);
    }

    // ALOGD("- android_net_ElNetworkStatsServiceProxy_nativeGetMobileIfaces()");
    return jifaces;
}

static void android_net_ElNetworkStatsServiceProxy_nativeIncrementOperationCount(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jint tag, jint operationCount)
{
    // ALOGD("+ android_net_ElNetworkStatsServiceProxy_nativeIncrementOperationCount()");

    IINetworkStatsService* nss = (IINetworkStatsService*)jproxy;
    nss->IncrementOperationCount(uid, tag, operationCount);

    // ALOGD("- android_net_ElNetworkStatsServiceProxy_nativeIncrementOperationCount()");
}

static void android_net_ElNetworkStatsServiceProxy_nativeSetUidForeground(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jboolean uidForeground)
{
    // ALOGD("+ android_net_ElNetworkStatsServiceProxy_nativeSetUidForeground()");

    IINetworkStatsService* nss = (IINetworkStatsService*)jproxy;
    nss->SetUidForeground(uid, uidForeground);

    // ALOGD("- android_net_ElNetworkStatsServiceProxy_nativeSetUidForeground()");
}

static void android_net_ElNetworkStatsServiceProxy_nativeForceUpdate(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkStatsServiceProxy_nativeForceUpdate()");

    IINetworkStatsService* nss = (IINetworkStatsService*)jproxy;
    nss->ForceUpdate();

    // ALOGD("- android_net_ElNetworkStatsServiceProxy_nativeForceUpdate()");
}

static void android_net_ElNetworkStatsServiceProxy_nativeAdvisePersistThreshold(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong thresholdBytes)
{
    // ALOGD("+ android_net_ElNetworkStatsServiceProxy_nativeAdvisePersistThreshold()");

    IINetworkStatsService* nss = (IINetworkStatsService*)jproxy;
    nss->AdvisePersistThreshold(thresholdBytes);

    // ALOGD("- android_net_ElNetworkStatsServiceProxy_nativeAdvisePersistThreshold()");
}

static void android_net_ElNetworkStatsServiceProxy_nativeFinalize(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElNetworkStatsServiceProxy_nativeDestroy()");

    IINetworkStatsService* obj = (IINetworkStatsService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_net_ElNetworkStatsServiceProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeOpenSession",      "(J)Landroid/net/INetworkStatsSession;",
            (void*) android_net_ElNetworkStatsServiceProxy_nativeOpenSession },
    { "nativeGetNetworkTotalBytes",      "(JLandroid/net/NetworkTemplate;JJ)J",
            (void*) android_net_ElNetworkStatsServiceProxy_nativeGetNetworkTotalBytes },
    { "nativeGetDataLayerSnapshotForUid",      "(JI)Landroid/net/NetworkStats;",
            (void*) android_net_ElNetworkStatsServiceProxy_nativeGetDataLayerSnapshotForUid },
    { "nativeGetMobileIfaces",      "(J)[Ljava/lang/String;",
            (void*) android_net_ElNetworkStatsServiceProxy_nativeGetMobileIfaces },
    { "nativeIncrementOperationCount",      "(JIII)V",
            (void*) android_net_ElNetworkStatsServiceProxy_nativeIncrementOperationCount },\
    { "nativeSetUidForeground",      "(JIZ)V",
            (void*) android_net_ElNetworkStatsServiceProxy_nativeSetUidForeground },
    { "nativeForceUpdate",      "(J)V",
            (void*) android_net_ElNetworkStatsServiceProxy_nativeForceUpdate },
    { "nativeAdvisePersistThreshold",      "(JJ)V",
            (void*) android_net_ElNetworkStatsServiceProxy_nativeAdvisePersistThreshold },
    { "nativeFinalize",      "(J)V",
            (void*) android_net_ElNetworkStatsServiceProxy_nativeFinalize },
};

int register_android_net_ElNetworkStatsServiceProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/net/ElNetworkStatsServiceProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

