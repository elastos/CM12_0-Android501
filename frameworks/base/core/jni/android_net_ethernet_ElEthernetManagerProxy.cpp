
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Net.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Net::IIEthernetManager;

static jobject android_net_ElEthernetManagerProxy_nativeGetConfiguration(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_ElEthernetManagerProxy_nativeGetConfiguration()");

    IIEthernetManager* ethermagr = (IIEthernetManager*)jproxy;

    AutoPtr<IIpConfiguration> configuration;
    ethermagr->GetConfiguration((IIpConfiguration**)&configuration);
    jobject jconfiguration = NULL;
    if (configuration != NULL) {
        jconfiguration = ElUtil::GetJavaIpConfiguration(env, configuration);
    }

    // ALOGD("- android_net_ElEthernetManagerProxy_nativeGetConfiguration()");
    return jconfiguration;
}

static void android_net_ElEthernetManagerProxy_nativeSetConfiguration(JNIEnv* env, jobject clazz, jlong jproxy, jobject jconfig)
{
    // ALOGD("+ android_net_ElEthernetManagerProxy_nativeSetConfiguration()");

    AutoPtr<IIpConfiguration> config;
    if (jconfig != NULL) {
        if (!ElUtil::ToElIpConfiguration(env, jconfig, (IIpConfiguration**)&config))
            ALOGE("nativeSetConfiguration: ToElIpConfiguration fail");
    }

    IIEthernetManager* ethermagr = (IIEthernetManager*)jproxy;
    ethermagr->SetConfiguration(config);

    // ALOGD("- android_net_ElEthernetManagerProxy_nativeSetConfiguration()");
}

static void android_net_ElEthernetManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_net_ElEthernetManagerProxy_nativeDestroy()");

    IIEthernetManager* obj = (IIEthernetManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_net_ElEthernetManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_net_ElEthernetManagerProxy_nativeFinalize },
    { "nativeGetConfiguration",    "(J)Landroid/net/IpConfiguration;",
            (void*) android_net_ElEthernetManagerProxy_nativeGetConfiguration },
    { "nativeSetConfiguration",    "(JLandroid/net/IpConfiguration;)V",
            (void*) android_net_ElEthernetManagerProxy_nativeSetConfiguration },
};

int register_android_net_ElEthernetManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/net/ElEthernetManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

