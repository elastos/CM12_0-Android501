
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <elastos/utility/etl/HashMap.h>

#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Os::IWorkSource;
using Elastos::Droid::Os::IIPowerManager;
using Elastos::Droid::JavaProxy::CBinderNative;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, AutoPtr<IBinder> > sLock;
static Mutex sLockLock;

static void android_os_ElPowerManagerProxy_nativeAcquireWakeLock(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlock, jint jflags, jstring jtag, jstring jpackageName, jobject jws, jstring jhistoryTag)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeAcquireWakeLock()");

    AutoPtr<IBinder> lock = NULL;
    if (jlock != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlock);
        Mutex::Autolock alock(sLockLock);
        if (sLock.Find(hashCode) != sLock.End()) {
            lock = sLock[hashCode];
        }

        if (lock == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);

            AutoPtr<IBinder> lock;
            jobject jInstance = env->NewGlobalRef(jlock);
            CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&lock);
            sLock[hashCode] = lock;
        }
    }

    String tag = ElUtil::ToElString(env, jtag);
    String packageName = ElUtil::ToElString(env, jpackageName);
    String historyTag = ElUtil::ToElString(env, jhistoryTag);

    AutoPtr<IWorkSource> ws;
    if (jws != NULL) {
        if (!ElUtil::ToElWorkSource(env, jws, (IWorkSource**)&ws)) {
            ALOGE("nativeAcquireWakeLock() ToElWorkSource fail!");
        }
    }

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    Boolean result;
    pm->AcquireWakeLock(lock, (Int32)jflags, tag, packageName, ws, historyTag);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeAcquireWakeLock()");
}

static void android_os_ElPowerManagerProxy_nativeAcquireWakeLockWithUid(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlock, jint jflags, jstring jtag, jstring jpackageName, jint uidtoblame)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeAcquireWakeLockWithUid()");

    AutoPtr<IBinder> lock = NULL;
    if (jlock != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlock);
        Mutex::Autolock alock(sLockLock);
        if (sLock.Find(hashCode) != sLock.End()) {
            lock = sLock[hashCode];
        }

        if (lock == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);

            AutoPtr<IBinder> lock;
            jobject jInstance = env->NewGlobalRef(jlock);
            CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&lock);

            sLock[hashCode] = lock;
        }
    }

    String tag = ElUtil::ToElString(env, jtag);
    String packageName = ElUtil::ToElString(env, jpackageName);

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    pm->AcquireWakeLockWithUid(lock, (Int32)jflags, tag, packageName, uidtoblame);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeAcquireWakeLockWithUid()");
}

static void android_os_ElPowerManagerProxy_nativeUpdateWakeLockWorkSource(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jlock, jobject jws, jstring jhistoryTag)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeUpdateWakeLockWorkSource()");

    AutoPtr<IBinder> lock = NULL;
    {
        Mutex::Autolock alock(sLockLock);
        if (sLock.Find(jlock) != sLock.End()) {
            lock = sLock[jlock];
        }
    }

    if (NULL == lock) {
        ALOGE("nativeUpdateWakeLockWorkSource() Invalid jlock!\n");
        env->ExceptionDescribe();
        return;
    }

    AutoPtr<IWorkSource> ws;
    if (jws != NULL) {
        if (!ElUtil::ToElWorkSource(env, jws, (IWorkSource**)&ws)) {
            ALOGE("nativeUpdateWakeLockWorkSource() ToElWorkSource fail!");
        }
    }

    String historyTag = ElUtil::ToElString(env, jhistoryTag);

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->UpdateWakeLockWorkSource(lock, ws, historyTag);
    ALOGE("nativeUpdateWakeLockWorkSource() ec=0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeUpdateWakeLockWorkSource()");
}

static void android_os_ElPowerManagerProxy_nativeReleaseWakeLock(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jlock, jint jflags)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeReleaseWakeLock()");

    AutoPtr<IBinder> lock = NULL;
    {
        Mutex::Autolock alock(sLockLock);
        if (sLock.Find(jlock) != sLock.End()) {
            lock = sLock[jlock];
            sLock.Erase(jlock);
        }
    }

    if (NULL == lock) {
        ALOGE("nativeReleaseWakeLock() Invalid jlock!\n");
        env->ExceptionDescribe();
        return;
    }

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    Boolean result;
    pm->ReleaseWakeLock(lock, (Int32)jflags);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeReleaseWakeLock()");
}

static void android_os_ElPowerManagerProxy_nativeUpdateWakeLockUids(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jlock, jintArray juids)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeUpdateWakeLockUids()");

    AutoPtr<IBinder> lock = NULL;
    {
        Mutex::Autolock alock(sLockLock);
        if (sLock.Find(jlock) != sLock.End()) {
            lock = sLock[jlock];
        }
    }

    if (NULL == lock) {
        ALOGE("nativeUpdateWakeLockUids() Invalid jlock!\n");
        env->ExceptionDescribe();
        return;
    }

    AutoPtr<ArrayOf<Int32> > uids;
    if (juids != NULL) {
        if (!ElUtil::ToElIntArray(env, juids, (ArrayOf<Int32>**)&uids)) {
            ALOGE("nativeUpdateWakeLockUids: ToElIntArray fail");
        }
    }

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    pm->UpdateWakeLockUids(lock, uids);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeUpdateWakeLockUids()");
}

static void android_os_ElPowerManagerProxy_nativePowerHint(JNIEnv* env, jobject clazz, jlong jproxy,
    jint hintId, jint data)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativePowerHint()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    pm->PowerHint(hintId, data);

    // ALOGD("- android_os_ElPowerManagerProxy_nativePowerHint()");
}

static jboolean android_os_ElPowerManagerProxy_nativeIsWakeLockLevelSupported(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jlevel)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeIsWakeLockLevelSupported()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = pm->IsWakeLockLevelSupported((Int32)jlevel, &result);
    if (FAILED(ec))
        ALOGE("nativeIsWakeLockLevelSupported() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeIsWakeLockLevelSupported()");
    return (jboolean)result;
}

static void android_os_ElPowerManagerProxy_nativeUserActivity(JNIEnv* env, jobject clazz, jlong jproxy,
     jlong jtime, jint jevent, jint jflags)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeUserActivity()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->UserActivity((Int64)jtime, (Int32)jevent, (Int32)jflags);
    if (FAILED(ec))
        ALOGE("nativeUserActivity() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeUserActivity()");
}

static void android_os_ElPowerManagerProxy_nativeWakeUp(JNIEnv* env, jobject clazz, jlong jproxy,
     jlong jtime)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeWakeUp()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->WakeUp((Int64)jtime);
    if (FAILED(ec))
        ALOGE("nativeWakeUp() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeWakeUp()");
}

static void android_os_ElPowerManagerProxy_nativeGoToSleep(JNIEnv* env, jobject clazz, jlong jproxy,
     jlong jtime, jint jreason, jint flags)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeGoToSleep()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->GoToSleep((Int64)jtime, (Int32)jreason, flags);
    if (FAILED(ec))
        ALOGE("nativeGoToSleep() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeGoToSleep()");
}

static void android_os_ElPowerManagerProxy_nativeNap(JNIEnv* env, jobject clazz, jlong jproxy,
     jlong jtime)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeNap()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->Nap((Int64)jtime);
    if (FAILED(ec))
        ALOGE("nativeNap() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeNap()");
}

static jboolean android_os_ElPowerManagerProxy_nativeIsInteractive(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeIsInteractive()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    Boolean result;
    ECode ec = pm->IsInteractive(&result);
    if (FAILED(ec))
        ALOGE("nativeIsInteractive() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeIsInteractive()");
    return result;
}

static jboolean android_os_ElPowerManagerProxy_nativeIsPowerSaveMode(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeIsPowerSaveMode()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    Boolean result;
    ECode ec = pm->IsPowerSaveMode(&result);
    if (FAILED(ec))
        ALOGE("nativeIsPowerSaveMode() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeIsPowerSaveMode()");
    return result;
}

static jboolean android_os_ElPowerManagerProxy_nativeSetPowerSaveMode(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean mode)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeSetPowerSaveMode()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    Boolean result;
    ECode ec = pm->SetPowerSaveMode(mode, &result);
    if (FAILED(ec))
        ALOGE("nativeSetPowerSaveMode() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeSetPowerSaveMode()");
    return result;
}

static void android_os_ElPowerManagerProxy_nativeReboot(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jconfirm, jstring jreason, jboolean jwait)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeReboot()");

    String reason = ElUtil::ToElString(env, jreason);

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    pm->Reboot((Boolean)jconfirm, reason, (Boolean)jwait);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeReboot()");
}

static void android_os_ElPowerManagerProxy_nativeShutdown(JNIEnv* env, jobject clazz, jlong jproxy,
     jboolean jconfirm, jboolean jwait)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeShutdown()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->Shutdown((Boolean)jconfirm, (Boolean)jwait);
    if (FAILED(ec))
        ALOGE("nativeShutdown() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeShutdown()");
}

static void android_os_ElPowerManagerProxy_nativeCrash(JNIEnv* env, jobject clazz, jlong jproxy,
     jstring jmessage)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeCrash()");

    String message = ElUtil::ToElString(env, jmessage);

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->Crash(message);
    if (FAILED(ec))
        ALOGE("nativeCrash() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeCrash()");
}

static void android_os_ElPowerManagerProxy_nativeSetStayOnSetting(JNIEnv* env, jobject clazz, jlong jproxy,
     jint jval)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeSetStayOnSetting()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->SetStayOnSetting((Int32)jval);
    if (FAILED(ec))
        ALOGE("nativeSetStayOnSetting() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeSetStayOnSetting()");
}

static void android_os_ElPowerManagerProxy_nativeSetMaximumScreenOffTimeoutFromDeviceAdmin(JNIEnv* env, jobject clazz, jlong jproxy,
     jint jtimeMs)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeSetMaximumScreenOffTimeoutFromDeviceAdmin()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->SetMaximumScreenOffTimeoutFromDeviceAdmin((Int32)jtimeMs);
    if (FAILED(ec))
        ALOGE("nativeSetMaximumScreenOffTimeoutFromDeviceAdmin() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeSetMaximumScreenOffTimeoutFromDeviceAdmin()");
}

static void android_os_ElPowerManagerProxy_nativeSetTemporaryScreenBrightnessSettingOverride(JNIEnv* env, jobject clazz, jlong jproxy,
     jint jbrightness)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeSetTemporaryScreenBrightnessSettingOverride()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->SetTemporaryScreenBrightnessSettingOverride((Int32)jbrightness);
    if (FAILED(ec))
        ALOGE("nativeSetTemporaryScreenBrightnessSettingOverride() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeSetTemporaryScreenBrightnessSettingOverride()");
}

static void android_os_ElPowerManagerProxy_nativeSetTemporaryScreenAutoBrightnessAdjustmentSettingOverride(JNIEnv* env, jobject clazz, jlong jproxy,
     jfloat jadj)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeSetTemporaryScreenAutoBrightnessAdjustmentSettingOverride()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->SetTemporaryScreenAutoBrightnessAdjustmentSettingOverride((Float)jadj);
    if (FAILED(ec))
        ALOGE("nativeSetTemporaryScreenAutoBrightnessAdjustmentSettingOverride() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeSetTemporaryScreenAutoBrightnessAdjustmentSettingOverride()");
}

static void android_os_ElPowerManagerProxy_nativeSetAttentionLight(JNIEnv* env, jobject clazz, jlong jproxy,
     jboolean jon, jint jcolor)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeSetAttentionLight()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->SetAttentionLight((Boolean)jon, (Int32)jcolor);
    if (FAILED(ec))
        ALOGE("nativeSetAttentionLight() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeSetAttentionLight()");
}

static void android_os_ElPowerManagerProxy_nativeUpdateBlockedUids(
    JNIEnv* env, jobject clazz, jlong jproxy, jint uid, jboolean isBlocked)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeUpdateBlockedUids()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->UpdateBlockedUids(uid, isBlocked);
    if (FAILED(ec))
        ALOGE("nativeUpdateBlockedUids() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeUpdateBlockedUids()");
}

static void android_os_ElPowerManagerProxy_nativeCpuBoost(
    JNIEnv* env, jobject clazz, jlong jproxy, jint duration)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeCpuBoost()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->CpuBoost(duration);
    if (FAILED(ec))
        ALOGE("nativeCpuBoost() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeCpuBoost()");
}

static void android_os_ElPowerManagerProxy_nativeSetKeyboardVisibility(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean visible)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeSetKeyboardVisibility()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->SetKeyboardVisibility(visible);
    if (FAILED(ec))
        ALOGE("nativeSetKeyboardVisibility() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeSetKeyboardVisibility()");
}

static void android_os_ElPowerManagerProxy_nativeSetKeyboardLight(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean on, jint key)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeSetKeyboardLight()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->SetKeyboardLight(on, key);
    if (FAILED(ec))
        ALOGE("nativeSetKeyboardLight() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeSetKeyboardLight()");
}

static void android_os_ElPowerManagerProxy_nativeWakeUpWithProximityCheck(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong time)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeWakeUpWithProximityCheck()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->WakeUpWithProximityCheck(time);
    if (FAILED(ec))
        ALOGE("nativeWakeUpWithProximityCheck() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeWakeUpWithProximityCheck()");
}

static jboolean android_os_ElPowerManagerProxy_nativeSetPowerProfile(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jprofile)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeSetPowerProfile()");

    String profile = ElUtil::ToElString(env, jprofile);

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    Boolean result;
    ECode ec = pm->SetPowerProfile(profile, &result);
    if (FAILED(ec))
        ALOGE("nativeSetPowerProfile() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeSetPowerProfile()");
    return result;
}

static jstring android_os_ElPowerManagerProxy_nativeGetPowerProfile(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeGetPowerProfile()");

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    String profile;
    ECode ec = pm->GetPowerProfile(&profile);
    if (FAILED(ec))
        ALOGE("nativeGetPowerProfile() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeGetPowerProfile()");
    return ElUtil::GetJavaString(env, profile);
}

static void android_os_ElPowerManagerProxy_nativeActivityResumed(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcomponentName)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeActivityResumed()");

    String componentName = ElUtil::ToElString(env, jcomponentName);

    IIPowerManager* pm = (IIPowerManager*)jproxy;
    ECode ec = pm->ActivityResumed(componentName);
    if (FAILED(ec))
        ALOGE("nativeActivityResumed() ec: 0x%08x", ec);

    // ALOGD("- android_os_ElPowerManagerProxy_nativeActivityResumed()");
}

static void android_os_ElPowerManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_os_ElPowerManagerProxy_nativeDestroy()");

    IIPowerManager* obj = (IIPowerManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_os_ElPowerManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_os_ElPowerManagerProxy_nativeFinalize },
    { "nativeAcquireWakeLock",    "(JLandroid/os/IBinder;ILjava/lang/String;Ljava/lang/String;Landroid/os/WorkSource;Ljava/lang/String;)V",
            (void*) android_os_ElPowerManagerProxy_nativeAcquireWakeLock },
    { "nativeAcquireWakeLockWithUid",    "(JLandroid/os/IBinder;ILjava/lang/String;Ljava/lang/String;I)V",
            (void*) android_os_ElPowerManagerProxy_nativeAcquireWakeLockWithUid },
    { "nativeUpdateWakeLockWorkSource",    "(JILandroid/os/WorkSource;Ljava/lang/String;)V",
            (void*) android_os_ElPowerManagerProxy_nativeUpdateWakeLockWorkSource },
    { "nativeReleaseWakeLock",    "(JII)V",
            (void*) android_os_ElPowerManagerProxy_nativeReleaseWakeLock },
    { "nativeUpdateWakeLockUids",    "(JI[I)V",
            (void*) android_os_ElPowerManagerProxy_nativeUpdateWakeLockUids },
    { "nativePowerHint",    "(JII)V",
            (void*) android_os_ElPowerManagerProxy_nativePowerHint },
    { "nativeIsWakeLockLevelSupported",    "(JI)Z",
            (void*) android_os_ElPowerManagerProxy_nativeIsWakeLockLevelSupported },
    { "nativeUserActivity",    "(JJII)V",
            (void*) android_os_ElPowerManagerProxy_nativeUserActivity },
    { "nativeWakeUp",    "(JJ)V",
            (void*) android_os_ElPowerManagerProxy_nativeWakeUp },
    { "nativeGoToSleep",    "(JJII)V",
            (void*) android_os_ElPowerManagerProxy_nativeGoToSleep },
    { "nativeNap",    "(JJ)V",
            (void*) android_os_ElPowerManagerProxy_nativeNap },
    { "nativeIsInteractive",    "(J)Z",
            (void*) android_os_ElPowerManagerProxy_nativeIsInteractive },
    { "nativeIsPowerSaveMode",    "(J)Z",
            (void*) android_os_ElPowerManagerProxy_nativeIsPowerSaveMode },
    { "nativeSetPowerSaveMode",    "(JZ)Z",
            (void*) android_os_ElPowerManagerProxy_nativeSetPowerSaveMode },
    { "nativeReboot",    "(JZLjava/lang/String;Z)V",
            (void*) android_os_ElPowerManagerProxy_nativeReboot },
    { "nativeShutdown",    "(JZZ)V",
            (void*) android_os_ElPowerManagerProxy_nativeShutdown },
    { "nativeCrash",    "(JLjava/lang/String;)V",
            (void*) android_os_ElPowerManagerProxy_nativeCrash },
    { "nativeSetStayOnSetting",    "(JI)V",
            (void*) android_os_ElPowerManagerProxy_nativeSetStayOnSetting },
    { "nativeSetMaximumScreenOffTimeoutFromDeviceAdmin",    "(JI)V",
            (void*) android_os_ElPowerManagerProxy_nativeSetMaximumScreenOffTimeoutFromDeviceAdmin },
    { "nativeSetTemporaryScreenBrightnessSettingOverride",    "(JI)V",
            (void*) android_os_ElPowerManagerProxy_nativeSetTemporaryScreenBrightnessSettingOverride },
    { "nativeSetTemporaryScreenAutoBrightnessAdjustmentSettingOverride",    "(JF)V",
            (void*) android_os_ElPowerManagerProxy_nativeSetTemporaryScreenAutoBrightnessAdjustmentSettingOverride },
    { "nativeSetAttentionLight",    "(JZI)V",
            (void*) android_os_ElPowerManagerProxy_nativeSetAttentionLight },
    { "nativeUpdateBlockedUids",    "(JIZ)V",
            (void*) android_os_ElPowerManagerProxy_nativeUpdateBlockedUids },
    { "nativeCpuBoost",    "(JI)V",
            (void*) android_os_ElPowerManagerProxy_nativeCpuBoost },
    { "nativeSetKeyboardVisibility",    "(JZ)V",
            (void*) android_os_ElPowerManagerProxy_nativeSetKeyboardVisibility },
    { "nativeSetKeyboardLight",    "(JZI)V",
            (void*) android_os_ElPowerManagerProxy_nativeSetKeyboardLight },
    { "nativeWakeUpWithProximityCheck",    "(JJ)V",
            (void*) android_os_ElPowerManagerProxy_nativeWakeUpWithProximityCheck },
    { "nativeSetPowerProfile",    "(JLjava/lang/String;)Z",
            (void*) android_os_ElPowerManagerProxy_nativeSetPowerProfile },
    { "nativeGetPowerProfile",    "(J)Ljava/lang/String;",
            (void*) android_os_ElPowerManagerProxy_nativeGetPowerProfile },
    { "nativeActivityResumed",    "(JLjava/lang/String;)V",
            (void*) android_os_ElPowerManagerProxy_nativeActivityResumed },
};

int register_android_os_ElPowerManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/os/ElPowerManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

