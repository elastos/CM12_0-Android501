
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Content::Pm::IIPackageInstaller;

static void android_content_pm_ElPackageInstallerProxy_nativeUninstall(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jint flags, jobject jstatusReceiver, jint userId)
{
    // ALOGD("+ android_content_pm_ElPackageInstallerProxy_nativeUninstall");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIntentSender> statusReceiver;
    if (jstatusReceiver != NULL) {
        if (!ElUtil::ToElIntentSender(env, jstatusReceiver, (IIntentSender**)&statusReceiver))
            ALOGD("ElPackageInstallerProxy nativeUninstall: ToElIntentSender fail!");
    }

    IIPackageInstaller* pi = (IIPackageInstaller*)jproxy;
    pi->Uninstall(packageName, flags, statusReceiver, userId);

    // ALOGD("- android_content_pm_ElPackageInstallerProxy_nativeUninstall");
}

static void android_content_pm_ElPackageInstallerProxy_nativeFinalize(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_pm_ElPackageInstallerProxy_nativeFinalize()");

    IIPackageInstaller* pi = (IIPackageInstaller*)jproxy;
    if (pi != NULL)
        pi->Release();

    // ALOGD("- android_content_pm_ElPackageInstallerProxy_nativeFinalize()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeUninstall",    "(JLjava/lang/String;ILandroid/content/IntentSender;I)V",
            (void*) android_content_pm_ElPackageInstallerProxy_nativeUninstall },
    { "nativeFinalize",    "(J)V",
            (void*) android_content_pm_ElPackageInstallerProxy_nativeFinalize },
};

int register_android_content_pm_ElPackageInstallerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/content/pm/ElPackageInstallerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android
