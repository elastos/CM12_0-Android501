
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Net::Nsd::IINsdManager;
using Elastos::Droid::Os::IMessenger;

static jobject android_net_nsd_ElNsdManagerProxy_nativeGetMessenger(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_nsd_ElNsdManagerProxy_nativeGetMessenger()");

    IINsdManager* nm = (IINsdManager*)jproxy;
    AutoPtr<IMessenger> messenger;
    nm->GetMessenger((IMessenger**)&messenger);
    jobject jmessenger = NULL;
    if (messenger != NULL) {
        jmessenger = ElUtil::GetJavaMessenger(env, messenger);
    }

    // ALOGD("- android_net_nsd_ElNsdManagerProxy_nativeGetMessenger()");
    return jmessenger;
}

static void android_net_nsd_ElNsdManagerProxy_nativeSetEnabled(JNIEnv* env, jobject clazz, jlong jproxy, jboolean enable)
{
    // ALOGD("+ android_net_nsd_ElNsdManagerProxy_nativeSetEnabled()");

    IINsdManager* nm = (IINsdManager*)jproxy;
    ECode ec = nm->SetEnabled((Boolean)enable);
    ALOGE("nativeSetEnabled() ec = 0x%08x", ec);

    // ALOGD("- android_net_nsd_ElNsdManagerProxy_nativeSetEnabled()");
}

static void android_net_nsd_ElNsdManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_net_nsd_ElNsdManagerProxy_nativeFinalize()");

    IINsdManager* obj = (IINsdManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_net_nsd_ElNsdManagerProxy_nativeFinalize()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",        "(J)V",
            (void*) android_net_nsd_ElNsdManagerProxy_nativeFinalize },
    { "nativeGetMessenger",    "(J)Landroid/os/Messenger;",
            (void*) android_net_nsd_ElNsdManagerProxy_nativeGetMessenger },
    { "nativeSetEnabled",      "(JZ)V",
            (void*) android_net_nsd_ElNsdManagerProxy_nativeSetEnabled },
};

int register_android_net_nsd_ElNsdManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/net/nsd/ElNsdManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android
