
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <elastos/utility/etl/HashMap.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Os::Storage::IIMountService;
using Elastos::Droid::Os::Storage::IIMountServiceListener;
using Elastos::Droid::Os::Storage::IIMountShutdownObserver;
using Elastos::Droid::Os::Storage::IIObbActionListener;
using Elastos::Droid::Os::Storage::IStorageVolume;

using Elastos::Droid::JavaProxy::CIMountShutdownObserverNative;
using Elastos::Droid::JavaProxy::CIObbActionListenerNative;
using Elastos::Droid::JavaProxy::CMountServiceListener;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, AutoPtr<IIMountServiceListener> > sMountSerListener;
static Mutex sMountSerListenerLock;
static HashMap<Int32, AutoPtr<IIObbActionListener> > sObbActionListener;
static Mutex sObbActionListenerLock;

static jobjectArray android_os_storage_ElMountServiceProxy_nativeGetVolumeList(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeGetVolumeList()");

    IIMountService* mSer = (IIMountService*)jproxy;
    AutoPtr<ArrayOf<IStorageVolume*> > list;
    mSer->GetVolumeList((ArrayOf<IStorageVolume*>**)&list);
    jobjectArray jlist = NULL;
    if (list != NULL) {
        jclass svolKlass = env->FindClass("android/os/storage/StorageVolume");
        ElUtil::CheckErrorAndLog(env, "nativeGetVolumeList Fail FindClass: StorageVolume : %d!\n", __LINE__);

        Int32 count = list->GetLength();
        jlist = env->NewObjectArray((jint)count, svolKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "nativeGetVolumeList Fail NewObjectArray: svolKlass : %d!\n", __LINE__);
        env->DeleteLocalRef(svolKlass);

        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IStorageVolume> volume = (*list)[i];
            if (volume != NULL) {
                jobject jvolume = ElUtil::GetJavaStorageVolume(env, volume);
                if (jvolume != NULL) {
                    env->SetObjectArrayElement(jlist, i, jvolume);
                    ElUtil::CheckErrorAndLog(env, "nativeGetVolumeList Fail NewObject: StorageVolume : %d!\n", __LINE__);
                    env->DeleteLocalRef(jvolume);
                }
            }
        }
    }

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetVolumeList()");
    return jlist;
}

static void android_os_storage_ElMountServiceProxy_nativeRegisterListener(JNIEnv* env, jobject clazz, jlong jproxy, jobject jlistener)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeRegisterListener()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    AutoPtr<IIMountServiceListener> listener;
    if (jlistener != NULL) {
        jobject jInstance = env->NewGlobalRef(jlistener);
        CMountServiceListener::New((Handle64)jvm, (Handle64)jInstance, (IIMountServiceListener**)&listener);
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
        Mutex::Autolock lock(sMountSerListenerLock);
        sMountSerListener[hashCode] = listener;
    }

    IIMountService* mSer = (IIMountService*)jproxy;
    ECode ec = mSer->RegisterListener(listener);
    if (FAILED(ec))
        ALOGE("nativeRegisterListener() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeRegisterListener()");
}

static void android_os_storage_ElMountServiceProxy_nativeUnregisterListener(JNIEnv* env, jobject clazz, jlong jproxy, jint jlistener)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeUnregisterListener()");

    AutoPtr<IIMountServiceListener> listener;
    {
        Mutex::Autolock lock(sMountSerListenerLock);
        HashMap<Int32, AutoPtr<IIMountServiceListener> >::Iterator it = sMountSerListener.Find(jlistener);
        if (it != sMountSerListener.End()){
            listener = it->mSecond;
            sMountSerListener.Erase(it);
        }
    }

    if (NULL == listener) {
        ALOGE("nativeUnregisterListener() Invalid IIMountServiceListener!\n");
        env->ExceptionDescribe();
        return;
    }

    IIMountService* mSer = (IIMountService*)jproxy;
    mSer->UnregisterListener(listener);


    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeUnregisterListener()");
}

static jstring android_os_storage_ElMountServiceProxy_nativeGetVolumeState(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jmountPoint)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeGetVolumeState()");

    String mountPoint = ElUtil::ToElString(env, jmountPoint);

    IIMountService* mSer = (IIMountService*)jproxy;
    String result;
    mSer->GetVolumeState(mountPoint, &result);

    jstring jresult = ElUtil::GetJavaString(env, result);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetVolumeState()");
    return jresult;
}

static jboolean android_os_storage_ElMountServiceProxy_nativeIsUsbMassStorageConnected(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeIsUsbMassStorageConnected()");

    IIMountService* mSer = (IIMountService*)jproxy;
    Boolean result = FALSE;
    ECode ec = mSer->IsUsbMassStorageConnected(&result);
    if (FAILED(ec))
        ALOGE("nativeIsUsbMassStorageConnected() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeIsUsbMassStorageConnected()");
    return (jboolean)result;
}

static void android_os_storage_ElMountServiceProxy_nativeSetUsbMassStorageEnabled(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jenable)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeSetUsbMassStorageEnabled()");

    IIMountService* mSer = (IIMountService*)jproxy;
    ECode ec = mSer->SetUsbMassStorageEnabled((Boolean)jenable);
    if (FAILED(ec))
        ALOGE("nativeSetUsbMassStorageEnabled() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeSetUsbMassStorageEnabled()");
}

static jboolean android_os_storage_ElMountServiceProxy_nativeIsUsbMassStorageEnabled(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeIsUsbMassStorageEnabled()");

    IIMountService* mSer = (IIMountService*)jproxy;
    Boolean result = FALSE;
    ECode ec = mSer->IsUsbMassStorageEnabled(&result);
    if (FAILED(ec))
        ALOGE("nativeIsUsbMassStorageEnabled() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeIsUsbMassStorageEnabled()");
    return (jboolean)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeMountVolume(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jmountPoint)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeMountVolume()");

    String mountPoint = ElUtil::ToElString(env, jmountPoint);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->MountVolume(mountPoint, &result);
    if (FAILED(ec))
        ALOGE("nativeMountVolume() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeMountVolume()");
    return (jint)result;
}

static void android_os_storage_ElMountServiceProxy_nativeUnmountVolume(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jmountPoint, jboolean jforce, jboolean jremoveEncryption)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeUnmountVolume()");

    String mountPoint = ElUtil::ToElString(env, jmountPoint);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->UnmountVolume(mountPoint, (Boolean)jforce, (Boolean)jremoveEncryption);
    if (FAILED(ec))
        ALOGE("nativeUnmountVolume() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeUnmountVolume()");
}

static jint android_os_storage_ElMountServiceProxy_nativeFormatVolume(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jmountPoint)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeFormatVolume()");

    String mountPoint = ElUtil::ToElString(env, jmountPoint);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->FormatVolume(mountPoint, &result);
    if (FAILED(ec))
        ALOGE("nativeFormatVolume() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeFormatVolume()");
    return (jint)result;
}

static jintArray android_os_storage_ElMountServiceProxy_nativeGetStorageUsers(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpath)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeGetStorageUsers()");

    String path = ElUtil::ToElString(env, jpath);

    IIMountService* mSer = (IIMountService*)jproxy;
    AutoPtr<ArrayOf<Int32> > users;
    ECode ec = mSer->GetStorageUsers(path, (ArrayOf<Int32>**)&users);
    if (FAILED(ec))
        ALOGE("nativeGetStorageUsers() ec: 0x%08x", ec);

    jintArray jusers = NULL;
    if (users != NULL) {
        jusers = ElUtil::GetJavaIntArray(env, users);
    }

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetStorageUsers()");
    return jusers;
}

static jint android_os_storage_ElMountServiceProxy_nativeCreateSecureContainer(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid, jint jsizeMb, jstring jfstype, jstring jkey, jint jownerUid, jboolean jexternal)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeCreateSecureContainer()");

    String id = ElUtil::ToElString(env, jid);
    String fstype = ElUtil::ToElString(env, jfstype);
    String key = ElUtil::ToElString(env, jkey);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->CreateSecureContainer(id, (Int32)jsizeMb, fstype, key, (Int32)jownerUid, (Boolean)jexternal, &result);
    if (FAILED(ec))
        ALOGE("nativeCreateSecureContainer() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeCreateSecureContainer()");
    return (jint)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeDestroySecureContainer(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid, jboolean jforce)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeDestroySecureContainer()");

    String id = ElUtil::ToElString(env, jid);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->DestroySecureContainer(id, (Boolean)jforce, &result);
    if (FAILED(ec))
        ALOGE("nativeDestroySecureContainer() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeDestroySecureContainer()");
    return (jint)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeFinalizeSecureContainer(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeFinalizeSecureContainer()");

    String id = ElUtil::ToElString(env, jid);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->FinalizeSecureContainer(id, &result);
    if (FAILED(ec))
        ALOGE("nativeFinalizeSecureContainer() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeFinalizeSecureContainer()");
    return (jint)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeMountSecureContainer(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid, jstring jkey, jint jownerUid, jboolean readOnly)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeMountSecureContainer()");

    String id = ElUtil::ToElString(env, jid);
    String key = ElUtil::ToElString(env, jkey);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->MountSecureContainer(id, key, (Int32)jownerUid, readOnly, &result);
    if (FAILED(ec))
        ALOGE("nativeMountSecureContainer() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeMountSecureContainer()");
    return (jint)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeUnmountSecureContainer(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid, jint jforce)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeUnmountSecureContainer()");

    String id = ElUtil::ToElString(env, jid);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->UnmountSecureContainer(id, (Int32)jforce, &result);
    if (FAILED(ec))
        ALOGE("nativeUnmountSecureContainer() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeUnmountSecureContainer()");
    return (jint)result;
}

static jboolean android_os_storage_ElMountServiceProxy_nativeIsSecureContainerMounted(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeIsSecureContainerMounted()");

    String id = ElUtil::ToElString(env, jid);

    IIMountService* mSer = (IIMountService*)jproxy;
    Boolean result = FALSE;
    ECode ec = mSer->IsSecureContainerMounted(id, &result);
    if (FAILED(ec))
        ALOGE("nativeIsSecureContainerMounted() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeIsSecureContainerMounted()");
    return (jboolean)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeRenameSecureContainer(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring joldId, jstring jnewId)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeRenameSecureContainer()");

    String oldId = ElUtil::ToElString(env, joldId);
    String newId = ElUtil::ToElString(env, jnewId);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->RenameSecureContainer(oldId, newId, &result);
    if (FAILED(ec))
        ALOGE("nativeRenameSecureContainer() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeRenameSecureContainer()");
    return (jint)result;
}

static jstring android_os_storage_ElMountServiceProxy_nativeGetSecureContainerPath(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeGetSecureContainerPath()");

    String id = ElUtil::ToElString(env, jid);

    IIMountService* mSer = (IIMountService*)jproxy;
    String path;
    ECode ec = mSer->GetSecureContainerPath(id, &path);
    if (FAILED(ec))
        ALOGE("nativeGetSecureContainerPath() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetSecureContainerPath()");
    return ElUtil::GetJavaString(env, path);
}

static jobjectArray android_os_storage_ElMountServiceProxy_nativeGetSecureContainerList(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeGetSecureContainerList()");

    IIMountService* mSer = (IIMountService*)jproxy;
    AutoPtr<ArrayOf<String> > list;
    ECode ec = mSer->GetSecureContainerList((ArrayOf<String>**)&list);
    if (FAILED(ec))
        ALOGE("nativeGetSecureContainerList() ec: 0x%08x", ec);

    jobjectArray jlist = NULL;
    if (list != NULL) {
        jlist = ElUtil::GetJavaStringArray(env, list);
    }

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetSecureContainerList()");
    return jlist;
}

static void android_os_storage_ElMountServiceProxy_nativeShutdown(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jobserver)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeShutdown()");

    AutoPtr<IIMountShutdownObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);

        CIMountShutdownObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IIMountShutdownObserver**)&observer);
    }

    IIMountService* mSer = (IIMountService*)jproxy;
    ECode ec = mSer->Shutdown(observer);
    if (FAILED(ec))
        ALOGE("nativeShutdown() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeShutdown()");
}

static void android_os_storage_ElMountServiceProxy_nativeFinishMediaUpdate(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeFinishMediaUpdate()");

    IIMountService* mSer = (IIMountService*)jproxy;
    ECode ec = mSer->FinishMediaUpdate();
    if (FAILED(ec))
        ALOGE("nativeFinishMediaUpdate() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeFinishMediaUpdate()");
}

static void android_os_storage_ElMountServiceProxy_nativeMountObb(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jrawPath, jstring jcanonicalPath, jstring jkey, jobject jtoken, jint jnonce)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeMountObb()");

    String rawPath = ElUtil::ToElString(env, jrawPath);
    String canonicalPath = ElUtil::ToElString(env, jcanonicalPath);
    String key = ElUtil::ToElString(env, jkey);

    AutoPtr<IIObbActionListener> token;
    if (jtoken != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jtoken);

        CIObbActionListenerNative::New((Handle64)jvm, (Handle64)jInstance, (IIObbActionListener**)&token);

        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sObbActionListenerLock);
        sObbActionListener[hashCode] = token;
    }

    IIMountService* mSer = (IIMountService*)jproxy;
    ECode ec = mSer->MountObb(rawPath, canonicalPath, key, token, (Int32)jnonce);
    if (FAILED(ec))
        ALOGE("nativeMountObb() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeMountObb()");
}

static void android_os_storage_ElMountServiceProxy_nativeUnmountObb(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jrawPath, jboolean jforce, jobject jtoken, jint jnonce)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeUnmountObb()");

    String rawPath = ElUtil::ToElString(env, jrawPath);

    AutoPtr<IIObbActionListener> token;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
    {
        Mutex::Autolock lock(sObbActionListenerLock);
        HashMap<Int32, AutoPtr<IIObbActionListener> >::Iterator it = sObbActionListener.Find(hashCode);
        if (it != sObbActionListener.End()){
            token = it->mSecond;
            sObbActionListener.Erase(it);
        }
    }

    if (NULL == token) {
        ALOGE("nativeUnmountObb() Invalid IIObbActionListener!\n");
        env->ExceptionDescribe();
        return;
    }

    IIMountService* mSer = (IIMountService*)jproxy;
    ECode ec = mSer->UnmountObb(rawPath, (Boolean)jforce, token, (Int32)jnonce);
    if (FAILED(ec))
        ALOGE("nativeUnmountObb() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeUnmountObb()");
}

static jboolean android_os_storage_ElMountServiceProxy_nativeIsObbMounted(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jrawPath)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeIsObbMounted()");

    String rawPath = ElUtil::ToElString(env, jrawPath);

    IIMountService* mSer = (IIMountService*)jproxy;
    Boolean result = FALSE;
    ECode ec = mSer->IsObbMounted(rawPath, &result);
    if (FAILED(ec))
        ALOGE("nativeIsObbMounted() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeUnmountObb()");
    return (jboolean)result;
}

static jstring android_os_storage_ElMountServiceProxy_nativeGetMountedObbPath(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jrawPath)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeIsObbMounted()");

    String rawPath = ElUtil::ToElString(env, jrawPath);

    IIMountService* mSer = (IIMountService*)jproxy;
    String path;
    ECode ec = mSer->GetMountedObbPath(rawPath, &path);
    if (FAILED(ec))
        ALOGE("nativeGetMountedObbPath() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetMountedObbPath()");
    return ElUtil::GetJavaString(env, path);
}

static jboolean android_os_storage_ElMountServiceProxy_nativeIsExternalStorageEmulated(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeIsObbMounted()");

    IIMountService* mSer = (IIMountService*)jproxy;
    Boolean result = FALSE;
    ECode ec = mSer->IsExternalStorageEmulated(&result);
    if (FAILED(ec))
        ALOGE("nativeIsExternalStorageEmulated() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeIsExternalStorageEmulated()");
    return (jboolean)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeGetEncryptionState(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeIsObbMounted()");

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 state = -1; // ENCRYPTION_STATE_ERROR_UNKNOWN
    ECode ec = mSer->GetEncryptionState(&state);
    if (FAILED(ec))
        ALOGE("nativeGetEncryptionState() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetEncryptionState()");
    return (jint)state;
}

static jint android_os_storage_ElMountServiceProxy_nativeDecryptStorage(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpassword)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeDecryptStorage()");

    String password = ElUtil::ToElString(env, jpassword);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->DecryptStorage(password, &result);
    if (FAILED(ec))
        ALOGE("nativeDecryptStorage() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeDecryptStorage()");
    return (jint)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeEncryptStorage(JNIEnv* env, jobject clazz, jlong jproxy,
    jint type, jstring jpassword)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeEncryptStorage()");

    String password = ElUtil::ToElString(env, jpassword);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->EncryptStorage(type, password, &result);
    if (FAILED(ec))
        ALOGE("nativeEncryptStorage() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeEncryptStorage()");
    return (jint)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeChangeEncryptionPassword(JNIEnv* env, jobject clazz, jlong jproxy,
    jint type, jstring jpassword)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeChangeEncryptionPassword()");

    String password = ElUtil::ToElString(env, jpassword);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->ChangeEncryptionPassword(type, password, &result);
    if (FAILED(ec))
        ALOGE("nativeChangeEncryptionPassword() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeChangeEncryptionPassword()");
    return (jint)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeVerifyEncryptionPassword(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpassword)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeVerifyEncryptionPassword()");

    String password = ElUtil::ToElString(env, jpassword);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->VerifyEncryptionPassword(password, &result);
    if (FAILED(ec))
        ALOGE("nativeVerifyEncryptionPassword() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeVerifyEncryptionPassword()");
    return (jint)result;
}

static jstring android_os_storage_ElMountServiceProxy_nativeGetSecureContainerFilesystemPath(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeGetSecureContainerFilesystemPath()");

    String id = ElUtil::ToElString(env, jid);

    IIMountService* mSer = (IIMountService*)jproxy;
    String path;
    ECode ec = mSer->GetSecureContainerFilesystemPath(id, &path);
    if (FAILED(ec))
        ALOGE("nativeGetSecureContainerFilesystemPath() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetSecureContainerFilesystemPath()");
    return ElUtil::GetJavaString(env, path);
}

static jint android_os_storage_ElMountServiceProxy_nativeFixPermissionsSecureContainer(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jid, jint jgid, jstring jfilename)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeFixPermissionsSecureContainer()");

    String id = ElUtil::ToElString(env, jid);
    String filename = ElUtil::ToElString(env, jfilename);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result = 0;
    ECode ec = mSer->FixPermissionsSecureContainer(id, (Int32)jgid, filename, &result);
    if (FAILED(ec))
        ALOGE("nativeFixPermissionsSecureContainer() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeFixPermissionsSecureContainer()");
    return (jint)result;
}

static jint android_os_storage_ElMountServiceProxy_nativeMkdirs(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jstring jpath)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeMkdirs()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    String path = ElUtil::ToElString(env, jpath);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result;
    ECode ec = mSer->Mkdirs(callingPkg, path, &result);
    if (FAILED(ec))
        ALOGE("nativeMkdirs() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeMkdirs()");
    return result;
}

static jint android_os_storage_ElMountServiceProxy_nativeGetPasswordType(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeGetPasswordType()");

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result;
    ECode ec = mSer->GetPasswordType(&result);
    if (FAILED(ec))
        ALOGE("nativeGetPasswordType() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetPasswordType()");
    return result;
}

static jstring android_os_storage_ElMountServiceProxy_nativeGetPassword(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeGetPassword()");

    IIMountService* mSer = (IIMountService*)jproxy;
    String result;
    ECode ec = mSer->GetPassword(&result);
    if (FAILED(ec))
        ALOGE("nativeGetPassword() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetPassword()");
    return ElUtil::GetJavaString(env, result);
}

static void android_os_storage_ElMountServiceProxy_nativeClearPassword(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeClearPassword()");

    IIMountService* mSer = (IIMountService*)jproxy;
    ECode ec = mSer->ClearPassword();
    if (FAILED(ec))
        ALOGE("nativeClearPassword() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeClearPassword()");
}

static void android_os_storage_ElMountServiceProxy_nativeSetField(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jfield, jstring jcontents)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeSetField()");

    String field = ElUtil::ToElString(env, jfield);
    String contents = ElUtil::ToElString(env, jcontents);

    IIMountService* mSer = (IIMountService*)jproxy;
    ECode ec = mSer->SetField(field, contents);
    if (FAILED(ec))
        ALOGE("nativeSetField() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeSetField()");
}

static jstring android_os_storage_ElMountServiceProxy_nativeGetField(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jfield)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeGetField()");

    String field = ElUtil::ToElString(env, jfield);

    IIMountService* mSer = (IIMountService*)jproxy;
    String result;
    ECode ec = mSer->GetField(field, &result);
    if (FAILED(ec))
        ALOGE("nativeGetField() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeGetField()");
    return ElUtil::GetJavaString(env, result);
}

static jint android_os_storage_ElMountServiceProxy_nativeResizeSecureContainer(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jid, jint sizeMb, jstring jkey)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeResizeSecureContainer()");

    String id = ElUtil::ToElString(env, jid);
    String key = ElUtil::ToElString(env, jkey);

    IIMountService* mSer = (IIMountService*)jproxy;
    Int32 result;
    ECode ec = mSer->ResizeSecureContainer(id, sizeMb, key, &result);
    if (FAILED(ec))
        ALOGE("nativeResizeSecureContainer() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeResizeSecureContainer()");
    return result;
}

static jlong android_os_storage_ElMountServiceProxy_nativeLastMaintenance(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeLastMaintenance()");

    IIMountService* mSer = (IIMountService*)jproxy;
    Int64 result;
    ECode ec = mSer->LastMaintenance(&result);
    if (FAILED(ec))
        ALOGE("nativeLastMaintenance() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeLastMaintenance()");
    return result;
}

static void android_os_storage_ElMountServiceProxy_nativeRunMaintenance(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeRunMaintenance()");

    IIMountService* mSer = (IIMountService*)jproxy;
    ECode ec = mSer->RunMaintenance();
    if (FAILED(ec))
        ALOGE("nativeRunMaintenance() ec: 0x%08x", ec);

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeRunMaintenance()");
}

static void android_os_storage_ElMountServiceProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_os_storage_ElMountServiceProxy_nativeDestroy()");

    IIMountService* obj = (IIMountService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_os_storage_ElMountServiceProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeFinalize },
    { "nativeGetVolumeList",    "(J)[Landroid/os/storage/StorageVolume;",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetVolumeList },
    { "nativeRegisterListener",    "(JLandroid/os/storage/IMountServiceListener;)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeRegisterListener },
    { "nativeUnregisterListener",    "(JI)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeUnregisterListener },
    { "nativeGetVolumeState",    "(JLjava/lang/String;)Ljava/lang/String;",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetVolumeState },
    { "nativeIsUsbMassStorageConnected",    "(J)Z",
            (void*) android_os_storage_ElMountServiceProxy_nativeIsUsbMassStorageConnected },
    { "nativeSetUsbMassStorageEnabled",    "(JZ)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeSetUsbMassStorageEnabled },
    { "nativeIsUsbMassStorageEnabled",    "(J)Z",
            (void*) android_os_storage_ElMountServiceProxy_nativeIsUsbMassStorageEnabled },
    { "nativeMountVolume",    "(JLjava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeMountVolume },
    { "nativeUnmountVolume",    "(JLjava/lang/String;ZZ)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeUnmountVolume },
    { "nativeFormatVolume",    "(JLjava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeFormatVolume },
    { "nativeGetStorageUsers",    "(JLjava/lang/String;)[I",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetStorageUsers },
    { "nativeCreateSecureContainer",    "(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IZ)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeCreateSecureContainer },
    { "nativeDestroySecureContainer",    "(JLjava/lang/String;Z)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeDestroySecureContainer },
    { "nativeFinalizeSecureContainer",    "(JLjava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeFinalizeSecureContainer },
    { "nativeMountSecureContainer",    "(JLjava/lang/String;Ljava/lang/String;IZ)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeMountSecureContainer },
    { "nativeUnmountSecureContainer",    "(JLjava/lang/String;Z)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeUnmountSecureContainer },
    { "nativeIsSecureContainerMounted",    "(JLjava/lang/String;)Z",
            (void*) android_os_storage_ElMountServiceProxy_nativeIsSecureContainerMounted },
    { "nativeRenameSecureContainer",    "(JLjava/lang/String;Ljava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeRenameSecureContainer },
    { "nativeGetSecureContainerPath",    "(JLjava/lang/String;)Ljava/lang/String;",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetSecureContainerPath },
    { "nativeGetSecureContainerList",    "(J)[Ljava/lang/String;",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetSecureContainerList },
    { "nativeShutdown",    "(JLandroid/os/storage/IMountShutdownObserver;)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeShutdown },
    { "nativeFinishMediaUpdate",    "(J)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeFinishMediaUpdate },
    { "nativeMountObb",    "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/storage/IObbActionListener;I)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeMountObb },
    { "nativeUnmountObb",    "(JLjava/lang/String;ZLandroid/os/storage/IObbActionListener;I)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeUnmountObb },
    { "nativeIsObbMounted",    "(JLjava/lang/String;)Z",
            (void*) android_os_storage_ElMountServiceProxy_nativeIsObbMounted },
    { "nativeGetMountedObbPath",    "(JLjava/lang/String;)Ljava/lang/String;",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetMountedObbPath },
    { "nativeIsExternalStorageEmulated",    "(J)Z",
            (void*) android_os_storage_ElMountServiceProxy_nativeIsExternalStorageEmulated },
    { "nativeGetEncryptionState",    "(J)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetEncryptionState },
    { "nativeDecryptStorage",    "(JLjava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeDecryptStorage },
    { "nativeEncryptStorage",    "(JILjava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeEncryptStorage },
    { "nativeChangeEncryptionPassword",    "(JILjava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeChangeEncryptionPassword },
    { "nativeVerifyEncryptionPassword",    "(JLjava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeVerifyEncryptionPassword },
    { "nativeGetSecureContainerFilesystemPath",    "(JLjava/lang/String;)Ljava/lang/String;",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetSecureContainerFilesystemPath },
    { "nativeFixPermissionsSecureContainer",    "(JLjava/lang/String;ILjava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeFixPermissionsSecureContainer },
    { "nativeMkdirs",    "(JLjava/lang/String;Ljava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeMkdirs },
    { "nativeGetPasswordType",    "(J)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetPasswordType },
    { "nativeGetPassword",    "(J)Ljava/lang/String;",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetPassword },
    { "nativeClearPassword",    "(J)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeClearPassword },
    { "nativeSetField",    "(JLjava/lang/String;Ljava/lang/String;)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeSetField },
    { "nativeGetField",    "(JLjava/lang/String;)Ljava/lang/String;",
            (void*) android_os_storage_ElMountServiceProxy_nativeGetField },
    { "nativeResizeSecureContainer",    "(JLjava/lang/String;ILjava/lang/String;)I",
            (void*) android_os_storage_ElMountServiceProxy_nativeResizeSecureContainer },
    { "nativeLastMaintenance",    "(J)J",
            (void*) android_os_storage_ElMountServiceProxy_nativeLastMaintenance },
    { "nativeRunMaintenance",    "(J)V",
            (void*) android_os_storage_ElMountServiceProxy_nativeRunMaintenance },
};

int register_android_os_storage_ElMountServiceProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/os/storage/ElMountServiceProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

