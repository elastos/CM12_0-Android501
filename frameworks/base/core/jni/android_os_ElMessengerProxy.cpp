
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Os::IMessage;
using Elastos::Droid::Os::CMessage;
using Elastos::Droid::Os::IIMessenger;

static void android_os_ElMessengerProxy_nativeSend(JNIEnv* env, jobject clazz, jlong jproxy, jobject jmsg)
{
    // ALOGD("+ android_os_ElMessengerProxy_nativeSend()");

    AutoPtr<IMessage> msg;
    if (jmsg != NULL) {
        if (!ElUtil::ToElMessage(env, jmsg, (IMessage**)&msg)) {
            ALOGE("android_os_ElMessengerProxy_nativeSend ToElMessage fail!");
            return;
        }
    }

    IIMessenger *imsgr = (IIMessenger*)jproxy;
    imsgr->Send(msg);

    // ALOGD("- android_os_ElMessengerProxy_nativeSend()");
}

static void android_os_ElMessengerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_os_ElMessengerProxy_nativeDestroy()");

    IIMessenger* obj = (IIMessenger*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_os_ElMessengerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_os_ElMessengerProxy_nativeFinalize },
    { "nativeSend",    "(JLandroid/os/Message;)V",
            (void*) android_os_ElMessengerProxy_nativeSend },
};

int register_android_os_ElMessengerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/os/ElMessengerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

