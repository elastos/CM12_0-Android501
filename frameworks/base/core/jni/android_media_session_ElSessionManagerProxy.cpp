
#define LOG_TAG "ElSessionManagerProxyJNI"
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
//#include <elastos/core/Object.h>
//#include <elastos/core/AutoLock.h>
//#include <elastos/utility/etl/HashMap.h>
//#include <Elastos.Droid.App.h>
//#include <Elastos.Droid.Telephony.h>
//#include <Elastos.Droid.Internal.h>
//#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.Droid.Media.h>
#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::JavaProxy::CMediaSessionCallbackStubNative;
using Elastos::Droid::Media::Session::IISessionManager;
using Elastos::Droid::Media::Session::IISession;
using Elastos::Droid::Media::Session::IISessionCallback;

static jobject android__media_session_ElSessionManager_nativeCreateSession(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring jpackageName,
    /* [in] */ jobject jcb,
    /* [in] */ jstring jtag,
    /* [in] */ jint userId)
{
    IISessionManager* ism = (IISessionManager*)jproxy;
    String pa = ElUtil::ToElString(env, jpackageName);
    String tag = ElUtil::ToElString(env, jtag);

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    AutoPtr<IISessionCallback> icb;
    if (jcb != NULL) {
        jobject jInstance = env->NewGlobalRef(jcb);

        if (NOERROR != CMediaSessionCallbackStubNative::New((Handle64)jvm, (Handle64)jInstance, (IISessionCallback**)&icb)) {
            ALOGE("android_media_session_ElSessionManagerProxy new CMediaSessionCallbackStubNative fail!, icb:%p", icb.Get());
        }
        env->DeleteLocalRef(jcb);
    }

    AutoPtr<IISession> session;
    ism->CreateSession(pa, icb, tag, (Int32)userId, (IISession**)&session);

    if (session != NULL)
    {
        jclass c = env->FindClass("android/media/session/ElSessionProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSessionProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ElSessionProxy : %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)session.Get());
        ElUtil::CheckErrorAndLog(env, "NewObject: ElSessionProxy : %d!\n", __LINE__);
        session->AddRef();
        env->DeleteLocalRef(c);
        return obj;
    }
    ALOGD("TODO leliang, ongoing , no impl, android_media_session_ElSessionManagerProxy.cpp line:%d", __LINE__);

    return NULL;
}

static jobject android__media_session_ElSessionManager_nativeGetSessions(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject comName,
    /* [in] */ jint userId)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionManagerProxy.cpp line:%d", __LINE__);
    return NULL;
}

static void android__media_session_ElSessionManager_nativeDispatchMediaKeyEvent(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject keyEvent,
    /* [in] */ jboolean needWakeLock)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionManagerProxy.cpp line:%d", __LINE__);
}

static void android__media_session_ElSessionManager_nativeDispatchAdjustVolume(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jint suggestedStream,
    /* [in] */ jint delta,
    /* [in] */ jint flags)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionManagerProxy.cpp line:%d", __LINE__);
}

static void android__media_session_ElSessionManager_nativeAddSessionsListener(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject listener,
    /* [in] */ jobject compName,
    /* [in] */ jint userId)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionManagerProxy.cpp line:%d", __LINE__);
}

static void android__media_session_ElSessionManager_nativeRemoveSessionsListener(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject listener)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionManagerProxy.cpp line:%d", __LINE__);
}

static void android__media_session_ElSessionManager_nativeSetRemoteVolumeController(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject rvc)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionManagerProxy.cpp line:%d", __LINE__);
}

static jboolean android__media_session_ElSessionManager_nativeIsGlobalPriorityActive(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionManagerProxy.cpp line:%d", __LINE__);
    return false;
}


namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeCreateSession",      "(JLjava/lang/String;Landroid/media/session/ISessionCallback;Ljava/lang/String;I)Landroid/media/session/ISession;",
            (void*) android__media_session_ElSessionManager_nativeCreateSession},
    { "nativeGetSessions",      "(JLandroid/content/ComponentName;I)Ljava/util/List;",
            (void*) android__media_session_ElSessionManager_nativeGetSessions},
    { "nativeDispatchMediaKeyEvent",      "(JLandroid/view/KeyEvent;Z)V",
            (void*) android__media_session_ElSessionManager_nativeDispatchMediaKeyEvent},
    { "nativeDispatchAdjustVolume",      "(JIII)V",
            (void*) android__media_session_ElSessionManager_nativeDispatchAdjustVolume},
    { "nativeAddSessionsListener",      "(JLandroid/media/session/IActiveSessionsListener;Landroid/content/ComponentName;I)V",
            (void*) android__media_session_ElSessionManager_nativeAddSessionsListener},
    { "nativeRemoveSessionsListener",      "(JLandroid/media/session/IActiveSessionsListener;)V",
            (void*) android__media_session_ElSessionManager_nativeRemoveSessionsListener},
    { "nativeSetRemoteVolumeController",      "(JLandroid/media/IRemoteVolumeController;)V",
            (void*) android__media_session_ElSessionManager_nativeSetRemoteVolumeController},
    { "nativeIsGlobalPriorityActive",      "(J)Z",
            (void*) android__media_session_ElSessionManager_nativeIsGlobalPriorityActive},
};

int register_android_media_session_ElSessionManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/media/session/ElSessionManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // end namespace android
