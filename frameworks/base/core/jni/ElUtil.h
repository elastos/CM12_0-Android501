#ifndef __ELUTIL_H_
#define __ELUTIL_H_

#include <jni.h>
#include <ElAndroid.h>

#include "Elastos.Droid.Core.h"
#include "Elastos.Droid.JavaProxy.h"

using Elastos::Core::ICharSequence;

using Elastos::Droid::Accounts::IAccount;
using Elastos::Droid::Accounts::IAuthenticatorDescription;
using Elastos::Droid::App::IActivityManagerMemoryInfo;
using Elastos::Droid::App::IActivityManagerProcessErrorStateInfo;
using Elastos::Droid::App::IActivityManagerRecentTaskInfo;
using Elastos::Droid::App::IActivityManagerRunningAppProcessInfo;
using Elastos::Droid::App::IActivityManagerRunningServiceInfo;
using Elastos::Droid::App::IActivityManagerRunningTaskInfo;
using Elastos::Droid::App::IActivityManagerStackInfo;
using Elastos::Droid::App::IActivityManagerTaskDescription;
using Elastos::Droid::App::IActivityManagerTaskThumbnail;
using Elastos::Droid::App::IActivityManagerWaitResult;
using Elastos::Droid::App::IAlarmClockInfo;
using Elastos::Droid::App::IActivityOptions;
using Elastos::Droid::App::IApplicationErrorReportCrashInfo;
using Elastos::Droid::App::IComposedIconInfo;
using Elastos::Droid::App::IContentProviderHolder;
using Elastos::Droid::App::IFragmentManagerState;
using Elastos::Droid::App::IIActivityContainer;
using Elastos::Droid::App::IIAppTask;
using Elastos::Droid::App::IIServiceConnection;
using Elastos::Droid::App::INotification;
using Elastos::Droid::App::IPendingIntent;
using Elastos::Droid::App::IProfilerInfo;
using Elastos::Droid::App::ISearchableInfo;
using Elastos::Droid::App::IWallpaperInfo;
using Elastos::Droid::AppWidget::IAppWidgetProviderInfo;
using Elastos::Droid::Content::IComponentName;
using Elastos::Droid::Content::IContentProviderOperation;
using Elastos::Droid::Content::IContentProviderResult;
using Elastos::Droid::Content::IContentResolver;
using Elastos::Droid::Content::IContentValues;
using Elastos::Droid::Content::IClipData;
using Elastos::Droid::Content::IClipDataItem;
using Elastos::Droid::Content::IClipDescription;
using Elastos::Droid::Content::IOnPrimaryClipChangedListener;
using Elastos::Droid::Content::IIntent;
using Elastos::Droid::Content::IIntentSender;
using Elastos::Droid::Content::IIntentFilter;
using Elastos::Droid::Content::IIntentShortcutIconResource;
using Elastos::Droid::Content::IIIntentSender;
using Elastos::Droid::Content::IPeriodicSync;
using Elastos::Droid::Content::ISyncAdapterType;
using Elastos::Droid::Content::ISyncInfo;
using Elastos::Droid::Content::ISyncRequest;
using Elastos::Droid::Content::ISyncStatusInfo;
using Elastos::Droid::Content::IUriPermission;
using Elastos::Droid::Content::IIntentFilterAuthorityEntry;
using Elastos::Droid::Content::Pm::IActivityInfo;
using Elastos::Droid::Content::Pm::IApplicationInfo;
using Elastos::Droid::Content::Pm::IComponentInfo;
using Elastos::Droid::Content::Pm::IConfigurationInfo;
using Elastos::Droid::Content::Pm::IFeatureInfo;
using Elastos::Droid::Content::Pm::IFeatureGroupInfo;
using Elastos::Droid::Content::Pm::IInstrumentationInfo;
using Elastos::Droid::Content::Pm::IKeySet;
using Elastos::Droid::Content::Pm::IParceledListSlice;
using Elastos::Droid::Content::Pm::IPackageCleanItem;
using Elastos::Droid::Content::Pm::IPackageInfo;
using Elastos::Droid::Content::Pm::IPackageItemInfo;
using Elastos::Droid::Content::Pm::IPackageInfoLite;
using Elastos::Droid::Content::Pm::IPathPermission;
using Elastos::Droid::Content::Pm::IPermissionInfo;
using Elastos::Droid::Content::Pm::IPermissionGroupInfo;
using Elastos::Droid::Content::Pm::IProviderInfo;
using Elastos::Droid::Content::Pm::IResolveInfo;
using Elastos::Droid::Content::Pm::IServiceInfo;
using Elastos::Droid::Content::Pm::IUserInfo;
using Elastos::Droid::Content::Pm::IManifestDigest;
using Elastos::Droid::Content::Pm::IVerificationParams;
using Elastos::Droid::Content::Pm::IVerifierDeviceIdentity;
using Elastos::Droid::Content::Pm::IContainerEncryptionParams;
using Elastos::Droid::Content::Pm::IThemeInfo;
using Elastos::Droid::Content::Pm::IVerifierInfo;
using Elastos::Droid::Content::Res::IAssetFileDescriptor;
using Elastos::Droid::Content::Res::ICompatibilityInfo;
using Elastos::Droid::Content::Res::IConfiguration;
using Elastos::Droid::Content::Res::IObbInfo;
using Elastos::Droid::Database::ICharArrayBuffer;
using Elastos::Droid::Graphics::IBitmap;
using Elastos::Droid::Graphics::IRect;
using Elastos::Droid::Graphics::IRegion;
using Elastos::Droid::Graphics::IPoint;
using Elastos::Droid::Graphics::PorterDuffMode;
using Elastos::Droid::Hardware::Display::IWifiDisplay;
using Elastos::Droid::Hardware::Display::IWifiDisplayStatus;
using Elastos::Droid::Hardware::Display::IWifiDisplaySessionInfo;
using Elastos::Droid::Hardware::Input::IKeyboardLayout;
using Elastos::Droid::Hardware::Input::IInputDeviceIdentifier;
using Elastos::Droid::Hardware::Input::ITouchCalibration;
using Elastos::Droid::Location::IAddress;
using Elastos::Droid::Hardware::Usb::IUsbAccessory;
using Elastos::Droid::Hardware::Usb::IUsbDevice;
using Elastos::Droid::Hardware::Usb::IUsbConfiguration;
using Elastos::Droid::Hardware::Usb::IUsbInterface;
using Elastos::Droid::Hardware::Usb::IUsbEndpoint;
using Elastos::Droid::Location::ICountry;
using Elastos::Droid::Location::ICriteria;
using Elastos::Droid::Location::IGeocoderParams;
using Elastos::Droid::Location::IGeofence;
using Elastos::Droid::Location::ILocation;
using Elastos::Droid::Location::ILocationRequest;
using Elastos::Droid::Media::IAudioRoutesInfo;
using Elastos::Droid::Media::IMediaMetadata;
using Elastos::Droid::Media::IMediaRouterClientState;
using Elastos::Droid::Media::IMediaRouterClientStateRouteInfo;
using Elastos::Droid::Media::AudioPolicy::IAudioPolicyConfig;
using Elastos::Droid::Media::Session::IPlaybackState;
using Elastos::Droid::Media::Session::IPlaybackStateCustomAction;
using Elastos::Droid::Net::IDhcpInfo;
using Elastos::Droid::Net::IUri;
using Elastos::Droid::Net::ILinkAddress;
using Elastos::Droid::Net::INetworkCapabilities;
using Elastos::Droid::Net::ILinkProperties;
using Elastos::Droid::Net::IIpConfiguration;
using Elastos::Droid::Net::IIpPrefix;
using Elastos::Droid::Net::IRouteInfo;
using Elastos::Droid::Net::INetworkInfo;
using Elastos::Droid::Net::INetworkQuotaInfo;
using Elastos::Droid::Net::INetworkState;
using Elastos::Droid::Net::INetworkStats;
using Elastos::Droid::Net::INetworkStatsHistory;
using Elastos::Droid::Net::INetworkTemplate;
using Elastos::Droid::Net::IProxyInfo;
using Elastos::Droid::Net::IStaticIpConfiguration;
using Elastos::Droid::Os::IBaseBundle;
using Elastos::Droid::Os::IBundle;
using Elastos::Droid::Os::IDebugMemoryInfo;
using Elastos::Droid::Os::IDropBoxManagerEntry;
using Elastos::Droid::Os::IMessage;
using Elastos::Droid::Os::IMessenger;
using Elastos::Droid::Os::IPatternMatcher;
using Elastos::Droid::Os::IParcelFileDescriptor;
using Elastos::Droid::Os::IPersistableBundle;
using Elastos::Droid::Os::IStrictModeViolationInfo;
using Elastos::Droid::Os::IUserHandle;
using Elastos::Droid::Os::IWorkSource;
using Elastos::Droid::Os::Storage::IStorageVolume;
// using Elastos::Droid::Privacy::IPrivacySettings;
using Elastos::Droid::Service::Notification::ICondition;
using Elastos::Droid::Service::Notification::IStatusBarNotification;
using Elastos::Droid::Service::Notification::IZenModeConfig;
using Elastos::Droid::Text::Style::ISuggestionSpan;
using Elastos::Droid::Utility::ISparseArray;
using Elastos::Droid::View::IDisplayInfo;
using Elastos::Droid::View::IInputChannel;
using Elastos::Droid::View::ISurface;
using Elastos::Droid::View::IWindowInfo;
using Elastos::Droid::View::IWindowManagerLayoutParams;
using Elastos::Droid::View::InputMethod::ICompletionInfo;
using Elastos::Droid::View::InputMethod::ICorrectionInfo;
using Elastos::Droid::View::InputMethod::IEditorInfo;
using Elastos::Droid::View::InputMethod::IExtractedText;
using Elastos::Droid::View::InputMethod::IExtractedTextRequest;
using Elastos::Droid::View::InputMethod::IInputMethodInfo;
using Elastos::Droid::View::InputMethod::IInputMethodSubtype;
using Elastos::Droid::View::IInputDevice;
using Elastos::Droid::View::IInputEvent;
using Elastos::Droid::View::IKeyCharacterMap;
using Elastos::Droid::View::IKeyEvent;
using Elastos::Droid::View::IMotionEvent;
using Elastos::Droid::View::IWindowContentFrameStats;
using Elastos::Droid::View::TextService::ISpellCheckerInfo;
using Elastos::Droid::View::TextService::ISpellCheckerSubtype;
using Elastos::Droid::Wifi::IBatchedScanSettings;
using Elastos::Droid::Wifi::ISupplicantState;
using Elastos::Droid::Wifi::IWifiActivityEnergyInfo;
using Elastos::Droid::Wifi::IWifiChannel;
using Elastos::Droid::Wifi::IWifiConnectionStatistics;
using Elastos::Droid::Wifi::IWifiInfo;
using Elastos::Droid::Wifi::IWifiSsid;
using Elastos::Droid::Wifi::IScanResult;
using Elastos::Droid::Wifi::IScanSettings;
using Elastos::Droid::Wifi::IWifiConfiguration;
using Elastos::Droid::Wifi::IWifiEnterpriseConfig;
using Elastos::Droid::Wifi::SupplicantState;
using Elastos::Droid::Wifi::IWpsInfo;
using Elastos::Droid::Wifi::P2p::IWifiP2pConfig;
using Elastos::Droid::Wifi::P2p::IWifiP2pDevice;
using Elastos::Droid::Wifi::P2p::IWifiP2pInfo;
using Elastos::Droid::Wifi::P2p::IWifiP2pWfdInfo;
using Elastos::Droid::Widget::IRemoteViews;
using Elastos::Droid::Internal::Location::IProviderProperties;
using Elastos::Droid::Internal::Net::ILegacyVpnInfo;
using Elastos::Droid::Internal::Net::IVpnConfig;
using Elastos::Droid::Internal::Net::IVpnProfile;
using Elastos::Droid::Internal::View::IInputBindResult;
using Elastos::IO::IFile;
using Elastos::IO::IFileDescriptor;
using Elastos::IO::ISerializable;
using Elastos::Net::IInetAddress;
using Elastos::Security::IPublicKey;
using Elastos::Utility::IBitSet;
using Elastos::Utility::IList;
using Elastos::Utility::ILocale;
using Elastos::Utility::IHashMap;
using Elastosx::Crypto::Spec::IIvParameterSpec;

#ifndef DUMP_CLSID
#define DUMP_CLSID(CLSID, TAG) \
    do { \
        ALOGD("======== DUMP_CLSID ========\n"); \
        ALOGD("{%08X, %04X, %04X, {%02X, %02X, %02X, %02X, %02X, %02X, %02X, %02X} }\n", \
                CLSID.mClsid.mData1, CLSID.mClsid.mData2, CLSID.mClsid.mData3, \
                CLSID.mClsid.mData4[0], CLSID.mClsid.mData4[1], \
                CLSID.mClsid.mData4[2], CLSID.mClsid.mData4[3], \
                CLSID.mClsid.mData4[4], CLSID.mClsid.mData4[5], \
                CLSID.mClsid.mData4[6], CLSID.mClsid.mData4[7]); \
        ALOGD("============================\n"); \
    } while(0);
#endif

namespace android {

class ElUtil
{
public:

    static jint GetJavaIntegerField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ jint defaultInt,
        /* [in] */ const char* tag);

    static jint GetJavaIntField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static jlong GetJavalongField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static jbyte GetJavabyteField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static jshort GetJavaShortField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static jchar GetJavaCharField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static jboolean GetJavaBoolField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static jfloat GetJavafloatField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static jdouble GetJavadoubleField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static String GetJavaStringField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static bool SetJavaIntegerField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ Int32 jintvalue,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static bool SetJavaIntField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ Int32 jintvalue,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static bool SetJavalongField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ Int64 longValue,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static bool SetJavabyteField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ Byte jbytevalue,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static bool SetJavaBoolField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ Boolean boolValue,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static bool SetJavafloatField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ Float floatValue,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static bool SetJavadoubleField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ Double floatValue,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static bool SetJavaStringField(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass klass,
        /* [in] */ jobject jobj,
        /* [in] */ String strValue,
        /* [in] */ const char* fieldName,
        /* [in] */ const char* tag);

    static void ThrowExceptionByName(
        /* [in] */ JNIEnv* env,
        /* [in] */ const char* className,
        /* [in] */ const char* tag);

//    static jint JavaIntegerToInt(
//       /* [in] */ JNIEnv* env,
//        /* [in] */ jobject jinteger);

    static String ToElString(
        /* [in] */ JNIEnv* env,
        /* [in] */ jstring str);

    static jstring GetJavaString(
        /* [in] */ JNIEnv* env,
        /* [in] */ const String& str);

    static jobjectArray GetJavaStringArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ ArrayOf<String>* strArray);

    static bool ToElStringArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobjectArray jarr,
        /* [out] */ ArrayOf<String>** strArray);

    static jintArray GetJavaIntArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ ArrayOf<Int32>* intArray);

    static jlongArray GetJavaLongArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ ArrayOf<Int64>* lArray);

    static jfloatArray GetJavaFloatArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ ArrayOf<Float>* fArray);

    static bool ToElIntArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ jintArray jarr,
        /* [out] */ ArrayOf<Int32>** intArray);

    static bool ToElLongArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ jlongArray jarr,
        /* [out] */ ArrayOf<Int64>** intArray);

    static bool ToElFloatArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ jfloatArray jarr,
        /* [out] */ ArrayOf<Float>** fArray);

    static jbyteArray GetJavaByteArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ ArrayOf<Byte>* byteArray);

    static bool ToElByteArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ jbyteArray jarray,
        /* [out] */ ArrayOf<Byte>** byteArray);

    static bool ToElCharArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ jcharArray jarray,
        /* [out] */ ArrayOf<Char32>** array);

    static jobject GetJavaBundle(
        /* [in] */ JNIEnv* env,
        /* [in] */ IBundle* bundle);

    static bool SetJavaBaseBundle(
        /* [in] */ JNIEnv* env,
        /* [in] */ IBaseBundle* bundle,
        /* [in] */ jobject jbundle);

    static jobject GetJavaApplicationInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IApplicationInfo* appInfo);

    static jobject GetJavaDisplayInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IDisplayInfo* disInfo);

    static jobject GetJavaActivityInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityInfo* actInfo);

    static jobject GetJavaServiceInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IServiceInfo* serInfo);

    static jobject GetJavaIntentFilter(
        /* [in] */ JNIEnv* env,
        /* [in] */ IIntentFilter* filter);

    static jobject GetJavaPackageInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IPackageInfo* pkgInfo);

    static jobject GetJavaThemeInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IThemeInfo* themeInfo);

    static jobject GetJavaRect(
        /* [in] */ JNIEnv* env,
        /* [in] */ IRect* rect);

    static jobject GetJavaConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ IConfiguration* bundle);

    static bool SetJavaConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ IConfiguration* configuration,
        /* [in] */ jobject jConfiguration);

    static jobject GetJavaLocale(
        /* [in] */ JNIEnv* env,
        /* [in] */ ILocale* locale);

    static jint GetNativeSurface(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jsurface);

    static jobject GetJavaKeyCharacterMap(
        /* [in] */ JNIEnv* env,
        /* [in] */ IKeyCharacterMap* keyMap);

    static jobject GetJavaInputDevice(
        /* [in] */ JNIEnv* env,
        /* [in] */ IInputDevice* inpDevice);

    static jboolean ToElInputEvent(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jevent,
        /* [out] */ IInputEvent** ev);

    static bool ToElKeyEvent(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jevent,
        /* [out] */ IKeyEvent** event);

    static bool ToElMotionEvent(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jevent,
        /* [out] */ IMotionEvent** ev);

    static jobject GetJavaIntent(
        /* [in] */ JNIEnv* env,
        /* [in] */ IIntent* intent);

    static jobject GetJavaComponentName(
        /* [in] */ JNIEnv* env,
        /* [in] */ IComponentName* componentName);

    static jobject GetJavaResolveInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IResolveInfo* resolveInfo);

    static jobject GetJavaInputBindResult(
        /* [in] */ JNIEnv* env,
        /* [in] */ IInputBindResult* result);

    static jobject GetJavaInputChannel(
        /* [in] */ JNIEnv* env,
        /* [in] */ IInputChannel* channel);

    // static jobject GetJavaPrivacySettings(
    //     /* [in] */ JNIEnv* env,
    //     /* [in] */ IPrivacySettings* prSettings);

    static jobject GetJavaParceledListSlice(
        /* [in] */ JNIEnv* env,
        /* [in] */ const ClassID& clsid,
        /* [in] */ IParceledListSlice* slice);

    static jobject GetJavaRunningServiceInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityManagerRunningServiceInfo* rSerInfo);

    static jobject GetJavaContentProviderHolder(
        /* [in] */ JNIEnv* env,
        /* [in] */ IContentProviderHolder* holder);

    static jobject GetJavaProviderInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IProviderInfo* providerInfo);

    static jobject GetJavaUserInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IUserInfo* userInfo);

    static jobject GetJavaInputMethodInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IInputMethodInfo* info);

    static jobject GetJavaInputMethodSubtype(
        /* [in] */ JNIEnv* env,
        /* [in] */ IInputMethodSubtype* subType);

    static jobject GetJavaUri(
        /* [in] */ JNIEnv* env,
        /* [in] */ IUri* uri);

    static jobject GetJavaClipData(
        /* [in] */ JNIEnv* env,
        /* [in] */ IClipData* clipData);

    static jobject GetJavaClipDataItem(
        /* [in] */ JNIEnv* env,
        /* [in] */ IClipDataItem* item);

    static jobject GetJavaAccount(
        /* [in] */ JNIEnv* env,
        /* [in] */ IAccount* account);

    static jobject GetJavaMessenger(
        /* [in] */ JNIEnv* env,
        /* [in] */ IMessenger* messenger);

    static jobject GetJavaWifiInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiInfo* info);

    static jobject GetJavaInetAddress(
        /* [in] */ JNIEnv* env,
        /* [in] */ IInetAddress* address);

    static jobject GetJavaInetAddress(
        /* [in] */ JNIEnv* env,
        /* [in] */ Int32 family,
        /* [in] */ Int32 ipaddress,
        /* [in] */ String hostName);

    static jobject GetJavaWifiSsid(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiSsid* address);

    static jobject GetJavaSupplicantState(
        /* [in] */ JNIEnv* env,
        /* [in] */ SupplicantState state);

    static jobject GetJavaSupplicantState(
        /* [in] */ JNIEnv* env,
        /* [in] */ ISupplicantState* state);

    static jobject GetJavaScanResult(
        /* [in] */ JNIEnv* env,
        /* [in] */ IScanResult* result);

    static jobject GetJavaDhcpInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IDhcpInfo* info);

    static jobject GetJavaLinkProperties(
        /* [in] */ JNIEnv* env,
        /* [in] */ ILinkProperties* properties);

    static jobject GetJavaProxyInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IProxyInfo* proxyInfo);

    static jobject GetJavaRouteInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IRouteInfo* info);

    static jobject GetJavaIpPrefix(
        /* [in] */ JNIEnv* env,
        /* [in] */ IIpPrefix* ipPrefix);

    static jobject GetJavaWifiConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiConfiguration* config);

    static jobject GetJavaWifiEnterpriseConfig(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiEnterpriseConfig* config);

    static jobject GetJavaIpConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ IIpConfiguration* config);

    static jobject GetJavaStaticIpConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ IStaticIpConfiguration* config);

    static jobject GetJavaBitSet(
        /* [in] */ JNIEnv* env,
        /* [in] */ IBitSet* bitset);

    static jobject GetJavaPermissionGroupInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IPermissionGroupInfo* info);

    static jobject GetJavaPermissionInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IPermissionInfo* info);

    static jobject GetJavaPendingIntent(
        /* [in] */ JNIEnv* env,
        /* [in] */ IPendingIntent* pendingIntent);

    static jobject GetJavaNetworkInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ INetworkInfo* info);

    static jobject GetJavaStorageVolume(
        /* [in] */ JNIEnv* env,
        /* [in] */ IStorageVolume* volume);

    static jobject GetJavaUserHandle(
        /* [in] */ JNIEnv* env,
        /* [in] */ IUserHandle* uHandle);

    static jobject GetJavaConfigurationInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IConfigurationInfo* cfgInfo);

    static jobject GetJavaRunningAppProcessInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityManagerRunningAppProcessInfo* procInfo);

    static void GetJavaRunningAppProcessInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jprocInfo,
        /* [in] */ IActivityManagerRunningAppProcessInfo* procInfo);

    static jobject GetJavaAuthenticatorDescription(
        /* [in] */ JNIEnv* env,
        /* [in] */ IAuthenticatorDescription* authDesc);

    static jobject GetJavaSpellCheckerInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ ISpellCheckerInfo* info);

    static jobject GetJavaSpellCheckerSubtype(
        /* [in] */ JNIEnv* env,
        /* [in] */ ISpellCheckerSubtype* subType);

    static jobject GetJavaRunningTaskInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityManagerRunningTaskInfo* info);

    static jobject GetJavaClipDescription(
        /* [in] */ JNIEnv* env,
        /* [in] */ IClipDescription* clipDesc);

    static jobject GetJavaSearchableInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ ISearchableInfo* info);

    static jobject GetJavaPackageInfoLite(
        /* [in] */ JNIEnv* env,
        /* [in] */ IPackageInfoLite* pkgLite);

    static jobject GetJavaObbInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IObbInfo* obb);

    static jobject GetJavaParcelFileDescriptor(
        /* [in] */ JNIEnv* env,
        /* [in] */ IParcelFileDescriptor* descptor);

    static jobject GetJavaFileDescriptor(
        /* [in] */ JNIEnv* env,
        /* [in] */ IFileDescriptor* descptor);

    static jobject GetJavaWallpaperInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWallpaperInfo* wallpaper);

    static jobject GetJavaAssetFileDescriptor(
        /* [in] */ JNIEnv* env,
        /* [in] */ IAssetFileDescriptor* asdescptor);

    static jobject GetJavaDropBoxManagerEntry(
        /* [in] */ JNIEnv* env,
        /* [in] */ IDropBoxManagerEntry* entry);

    static jobject GetJavaFeatureInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IFeatureInfo* info);

    static jobject GetJavaFeatureGroupInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IFeatureGroupInfo* info);

    static jobject GetJavaPackageCleanItem(
        /* [in] */ JNIEnv* env,
        /* [in] */ IPackageCleanItem* pkgCItem);

    static jobject GetJavaWaitResult(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityManagerWaitResult* amResult);

    static jobject GetJavaTaskThumbnail(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityManagerTaskThumbnail* thumbnails);

    static jobject GetJavaProcessErrorStateInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityManagerProcessErrorStateInfo* info);

    static jobject GetJavaInstrumentationInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IInstrumentationInfo* info);

    static jobject GetJavaVerifierDeviceIdentity(
        /* [in] */ JNIEnv* env,
        /* [in] */ IVerifierDeviceIdentity* identity);

    static jobject GetJavaWindowInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWindowInfo* info);

    static jobject GetJavaWifiDisplay(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiDisplay* display);

    static jobject GetJavaWifiDisplayStatus(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiDisplayStatus* status);

    static jobject GetJavaWifiDisplaySessionInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiDisplaySessionInfo* info);

    static jobject GetJavaUsbAccessory(
        /* [in] */ JNIEnv* env,
        /* [in] */ IUsbAccessory* accessory);

    static jobject GetJavaNetworkState(
        /* [in] */ JNIEnv* env,
        /* [in] */ INetworkState* state);

    static jobject GetJavaNetworkQuotaInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ INetworkQuotaInfo* info);

    static jobject GetJavaNetworkCapabilities(
        /* [in] */ JNIEnv* env,
        /* [in] */ INetworkCapabilities* info);

    static jobject GetJavaLegacyVpnInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ ILegacyVpnInfo* info);

    static jobject GetJavaLinkAddress(
        /* [in] */ JNIEnv* env,
        /* [in] */ ILinkAddress* info);

    static jobject GetJavaVerifierInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IVerifierInfo* info);

    static jobject GetJavaPublicKey(
        /* [in] */ JNIEnv* env,
        /* [in] */ IPublicKey* pkey);

    static jobject GetJavaLocation(
        /* [in] */ JNIEnv* env,
        /* [in] */ ILocation* location);

    static jobject GetJavaProviderProperties(
        /* [in] */ JNIEnv* env,
        /* [in] */ IProviderProperties* properties);

    static jobject GetJavaBitmap(
        /* [in] */ JNIEnv* env,
        /* [in] */ IBitmap* bitmap);

    static jobject GetJavaRemoteViews(
        /* [in] */ JNIEnv* env,
        /* [in] */ IRemoteViews* obj);

    static jobject GetJavaPorterDuffMode(
        /* [in] */ JNIEnv* env,
        /* [in] */ PorterDuffMode mode);

    static jobject GetJavaAppWidgetProviderInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IAppWidgetProviderInfo* obj);

    static jobject GetJavaDebugMemoryInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IDebugMemoryInfo* info);

    static jobject GetJavaCountry(
        /* [in] */ JNIEnv* env,
        /* [in] */ ICountry* country);

    static bool ToElIntent(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jentent,
        /* [out] */ IIntent** elIntent);

    static bool ToElComponentName(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jcomponentName,
        /* [out] */ IComponentName** elcomponentName);

    static bool ToElBundle(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jentent,
        /* [out] */ IBundle** elBundle);

    static bool SetElBaseBundle(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jBundle,
        /* [in] */ IBaseBundle* elBundle);

    static bool ToElBitmap(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jbitmap,
        /* [out] */ IBitmap** bitmap);

    static bool ToElWindowManagerLayoutParams(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jparams,
        /* [out] */ IWindowManagerLayoutParams** elParams);

    static bool ToElRect(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jrect,
        /* [out] */ IRect** elRect);

    static bool ToElConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jConfiguration,
        /* [out] */ IConfiguration** elConfiguration);

    static bool ToElEditorInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jEditorInfo,
        /* [out] */ IEditorInfo** elEditorInfo);

    static bool ToElCharSequence(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jEditorInfo,
        /* [out] */ ICharSequence** elEditorInfo);

    static bool ToElIntentFilter(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jfilter,
        /* [out] */ IIntentFilter** filter);

    static bool ToElUri(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jUri,
        /* [out] */ IUri** elUri);

    // static bool ToElPrivacySettings(
    //     /* [in] */ JNIEnv* env,
    //     /* [in] */ jobject jSettings,
    //     /* [out] */ IPrivacySettings** settings);

    static bool ToElCompletionInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jcompletion,
        /* [out] */ ICompletionInfo** completion);

    static bool ToElRegion(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jregion,
        /* [out] */ IRegion** region);

    static bool ToElAccount(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jaccount,
        /* [out] */ IAccount** account);

    static bool ToElMessage(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jmsg,
        /* [out] */ IMessage** msg);

    static bool ToElMessenger(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jmsgr,
        /* [out] */ IMessenger** msgr);

    static bool ToElWifiConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconfig,
        /* [out] */ IWifiConfiguration** config);

    static bool ToElWifiEnterpriseConfig(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconfig,
        /* [out] */ IWifiEnterpriseConfig** config);

    static bool ToElIpConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconfig,
        /* [out] */ IIpConfiguration** config);

    static bool ToElStaticIpConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconfig,
        /* [out] */ IStaticIpConfiguration** config);

    static bool ToElProxyInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IProxyInfo** info);

    static bool ToElBitSet(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jbitset,
        /* [out] */ IBitSet** bitset);

    static bool ToElLinkProperties(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jlinkProperties,
        /* [out] */ ILinkProperties** linkProperties);

    static bool ToElNotification(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jnotification,
        /* [out] */ INotification** notification);

    static bool ToElPendingIntent(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jpintent,
        /* [out] */ IPendingIntent** pintent);

    static bool ToElIIntentSender(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jiiSender,
        /* [out] */ IIIntentSender** iiSender);

    static bool ToElIntentSender(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jiSender,
        /* [out] */ IIntentSender** iSender);

    static void SetMemoryInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jmemInfo,
        /* [in] */ IActivityManagerMemoryInfo* memInfo);

    static void SetJavaRect(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jrect,
        /* [in] */ IRect* rect);

    static bool ToElProviderProperties(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jproperties,
        /* [out] */ IProviderProperties** properties);

    static bool ToElLocationRequest(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jrequest,
        /* [out] */ ILocationRequest** locationRequest);

    static bool ToElLocation(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jlocation,
        /* [out] */ ILocation** location);

    static bool ToElGeofence(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jGeofence,
        /* [out] */ IGeofence** geofence);

    static bool ToElCriteria(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jCriteria,
        /* [out] */ ICriteria** criteria);

    static bool ToElGeocoderParams(
            /* [in] */ JNIEnv* env,
            /* [in] */ jobject jgeocoderParams,
            /* [out] */ IGeocoderParams** params);

    static bool ToElAddress(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jaddress,
        /* [out] */ IAddress** address);

    static bool ToElIServiceConnection(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconnection,
        /* [out] */ IIServiceConnection** connection);

    static bool ToElWorkSource(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jws,
        /* [out] */ IWorkSource** ws);

    static bool ToElRemoteViews(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jremoteview,
        /* [out] */ IRemoteViews** remoteview);

    static bool ToElNetworkInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ INetworkInfo** info);

    static bool ToElAppWidgetProviderInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jproviderInfo,
        /* [out] */ IAppWidgetProviderInfo** providerInfo);

    static bool ToElIntentShortcutIconResource(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jresource,
        /* [out] */ IIntentShortcutIconResource** resource);

    static bool ToElWifiInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IWifiInfo** info);

    static bool ToElSupplicantState(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ SupplicantState* state);

    static bool ToElSupplicantState(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ ISupplicantState** state);

    static bool ToElInetAddress(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jaddr,
        /* [out] */ IInetAddress** addr);

    static bool ToElWifiSsid(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jwifiSsid,
        /* [out] */ IWifiSsid** wifiSsid);

    static bool ToElLinkAddress(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jlinkAddr,
        /* [out] */ ILinkAddress** linkAddr);

    static bool ToElExtractedText(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jert,
        /* [out] */ IExtractedText** ert);

    static bool ToElExtractedTextRequest(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jertq,
        /* [out] */ IExtractedTextRequest** ertq);

    static bool ToElCorrectionInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jcorrection,
        /* [out] */ ICorrectionInfo** correction);

    static bool ToElClipData(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jclip,
        /* [out] */ IClipData** clip);

    static bool ToElClipDescription(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jclipDesc,
        /* [out] */ IClipDescription** clipDesc);

    static bool ToElContainerEncryptionParams(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jparams,
        /* [out] */ IContainerEncryptionParams** params);

    static bool ToElParcelFileDescriptor(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jfDescriptor,
        /* [out] */ IParcelFileDescriptor** fDescriptor);

    static bool ToElFileDescriptor(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jfDescptor,
        /* [out] */ IFileDescriptor** fDescptor);

    static bool ToElStorageVolume(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jvolume,
        /* [out] */ IStorageVolume** volume);

    static bool ToElFile(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jfile,
        /* [out] */ IFile** file);

    static bool ToElUserHandle(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jhandle,
        /* [out] */ IUserHandle** handle);

    static bool ToElApplicationInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jappInfo,
        /* [out] */ IApplicationInfo** appInfo);

    static bool ToElLocale(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jlocale,
        /* [out] */ ILocale** locale);

    static bool ToElApplicationErrorReportCrashInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IApplicationErrorReportCrashInfo** info);

    static bool ToElDropBoxManagerEntry(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jentry,
        /* [out] */ IDropBoxManagerEntry** entry);

    static bool ToElHashMap(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jmap,
        /* [out] */ IHashMap** map);

    static bool ToElVpnProfile(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jprofile,
        /* [out] */ IVpnProfile** profile);

    static bool ToElCharArrayBuffer(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jbuffer,
        /* [out] */ ICharArrayBuffer** buffer);

    static bool ToElPackageCleanItem(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jpkgCItem,
        /* [out] */ IPackageCleanItem** pkgCItem);

    static bool ToElStrictModeViolationInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IStrictModeViolationInfo** info);

    static bool ToElPermissionInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IPermissionInfo** info);

    static bool ToElWifiP2pWfdInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IWifiP2pWfdInfo** info);

    static bool ToElWifiP2pDevice(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jdev,
        /* [out] */ IWifiP2pDevice** dev);

    static bool ToElWifiP2pInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IWifiP2pInfo** info);

    static bool ToElWifiP2pConfig(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconfig,
        /* [out] */ IWifiP2pConfig** config);

    static bool ToElWpsInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IWpsInfo** info);

    static bool ToElCompatibilityInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ ICompatibilityInfo** info);

    static bool ToElInputMethodInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IInputMethodInfo** info);

    static bool ToElResolveInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IResolveInfo** info);

    static bool ToElInputMethodSubtype(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jsubtype,
        /* [out] */ IInputMethodSubtype** subtype);

    static bool ToElActivityInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IActivityInfo** info);

    static bool ToElServiceInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IServiceInfo** info);

    static bool ToElSuggestionSpan(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jspan,
        /* [out] */ ISuggestionSpan** span);

    static bool ToElRouteInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IRouteInfo** info);

    static bool ToElIpPrefix(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jipPrefix,
        /* [out] */ IIpPrefix** ipPrefix);

    static bool ToElContentProviderHolder(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jholder,
        /* [out] */ IContentProviderHolder** holder);

    static bool ToElProviderInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IProviderInfo** info);

    static bool ToElContentProviderOperation(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject joperation,
        /* [out] */ IContentProviderOperation** operation);

    static jobject GetJavaContentProviderResult(
        /* [in] */ JNIEnv* env,
        /* [in] */ IContentProviderResult* result);

    static bool ToElPatternMatcher(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jpmatcher,
        /* [out] */ IPatternMatcher** pmatcher);

    static bool ToElPathPermission(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jpperm,
        /* [out] */ IPathPermission** pperm);

    static Int32 GetJavaHashCode(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject obj);

    static void CheckErrorAndLog(
        /* [in] */ JNIEnv* env,
        /* [in] */ const char* errlog,
        /* [in] */ int line);

    static void CheckErrorAndLog(
        /* [in] */ JNIEnv* env,
        /* [in] */ const char* errlog,
        /* [in] */ const char* paramname,
        /* [in] */ const char* tag,
        /* [in] */ Int32 line);

    static String GetClassName(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject obj);

    static String GetJavaToString(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jobj);

    static bool ToElManifestDigest(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jmanifestDigest,
        /* [out] */ IManifestDigest** manifestDigest);

    static bool ToElVerificationParams(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jverificationParams,
        /* [out] */ IVerificationParams** verificationParams);

    static bool ToElContentValues(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jContentValues,
        /* [out] */ IContentValues** contentValues);

    static bool ToElFragmentManagerState(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jstate,
        /* [in] */ IFragmentManagerState** state);

    static bool ToElUsbAccessory(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jaccessory,
        /* [out] */ IUsbAccessory** accessory);

    static bool ToElUsbDevice(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jdevice,
        /* [out] */ IUsbDevice** device);

    static bool ToElUsbConfiguration(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconfiguration,
        /* [out] */ IUsbConfiguration** configuration);

    static bool ToElUsbInterface(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jusbInterface,
        /* [out] */ IUsbInterface** usbInterface);

    static bool ToElUsbEndpoint(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jendpoint,
        /* [out] */ IUsbEndpoint** endpoint);

    static bool ToElVpnConfig(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconfig,
        /* [out] */ IVpnConfig** config);

    static bool ToElIvParameterSpec(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jspec,
        /* [out] */ IIvParameterSpec** spec);

    static bool ToElParcelable(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jparcelable,
        /* [out] */ IParcelable** parcelable);

    static bool ToElSerializable(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jserializable,
        /* [out] */ ISerializable** serializable);

    static bool ToElClipDataItem(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jitem,
        /* [out] */ IClipDataItem** item);

    static jobject GetJavaRecentTaskInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityManagerRecentTaskInfo* info);

    static jobject GetJavaTaskDescription(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityManagerTaskDescription* taskDescription);

    static jobject GetJavaPeriodicSync(
        /* [in] */ JNIEnv* env,
        /* [in] */ IPeriodicSync* periodicSync);

    static jobject GetJavaSyncInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ ISyncInfo* syncInfo);

    static jobject GetJavaSyncAdapterType(
        /* [in] */ JNIEnv* env,
        /* [in] */ ISyncAdapterType* syncAdapterType);

    static jobject GetJavaSyncStatusInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ ISyncStatusInfo* syncAdapterType);

    static jobject GetJavaKeyboardLayout(
        /* [in] */ JNIEnv* env,
        /* [in] */ IKeyboardLayout* kbdlayout);

    static bool ToElAuthorityEntry(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jauthorityEntry,
        /* [out] */ IIntentFilterAuthorityEntry** authorityEntry);

    static bool JavaObjectToElByteArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jobj,
        /* [out, callee] */ ArrayOf<Byte>** objArray);

    static bool ToElAlarmClockInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IAlarmClockInfo** info);

    static jobject GetJavaAlarmClockInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IAlarmClockInfo* info);

    static bool ToElProfilerInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo,
        /* [out] */ IProfilerInfo** info);

    static bool ToElPersistableBundle(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jbundle,
        /* [out] */ IPersistableBundle** bundle);

    static jobject GetJavaIAppTask(
        /* [in] */ JNIEnv* env,
        /* [in] */ IIAppTask* appTask);

    static bool ToElTaskDescription(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jdescription,
        /* [out] */ IActivityManagerTaskDescription** description);

    static bool ToElPoint(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jpoint,
        /* [out] */ IPoint** point);

    static jobject GetJavaPoint(
        /* [in] */ JNIEnv* env,
        /* [in] */ IPoint* point);

    static jobject GetJavaStackInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityManagerStackInfo* stackInfo);

    static jobject GetJavaUriPermission(
        /* [in] */ JNIEnv* env,
        /* [in] */ IUriPermission* uriPermission);

    static bool ToElActivityOptions(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject joptions,
        /* [out] */ IActivityOptions** options);

    static jobject GetJavaActivityOptions(
        /* [in] */ JNIEnv* env,
        /* [in] */ IActivityOptions* options);

    static jobject GetJavaIActivityContainer(
        /* [in] */ JNIEnv* env,
        /* [in] */ IIActivityContainer* container);

    static bool ToElStringList(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jlist,
        /* [out] */ IList** list);

    static jobject GetJavaStringList(
        /* [in] */ JNIEnv* env,
        /* [in] */ IList* list);

    static jobject GetJavaStatusBarNotification(
        /* [in] */ JNIEnv* env,
        /* [in] */ IStatusBarNotification* notification);

    static bool ToElZenModeConfig(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconfig,
        /* [out] */ IZenModeConfig** config);

    static jobject GetJavaZenModeConfig(
        /* [in] */ JNIEnv* env,
        /* [in] */ IZenModeConfig* config);

    static bool ToElCondition(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jcondition,
        /* [out] */ ICondition** condition);

    static jobject GetJavaCondition(
        /* [in] */ JNIEnv* env,
        /* [in] */ ICondition* condition);

    static jobject GetJavaIntentSender(
        /* [in] */ JNIEnv* env,
        /* [in] */ IIntentSender* intentSender);

    static jobject GetJavaIIntentSender(
        /* [in] */ JNIEnv* env,
        /* [in] */ IIIntentSender* isender);

    static bool ToElSyncRequest(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jrequest,
        /* [out] */ ISyncRequest** request);

    static bool ToElKeySet(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jkeySet,
        /* [out] */ IKeySet** keySet);

    static jobject GetJavaKeySet(
        /* [in] */ JNIEnv* env,
        /* [in] */ IKeySet* keySet);

    static jobject GetJavaComposedIconInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IComposedIconInfo* info);

    static bool ToElInputDeviceIdentifier(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jidentifier,
        /* [out] */ IInputDeviceIdentifier** identifier);

    static bool ToElTouchCalibration(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jcalibration,
        /* [out] */ ITouchCalibration** calibration);

    static jobject GetJavaTouchCalibration(
        /* [in] */ JNIEnv* env,
        /* [in] */ ITouchCalibration* calibration);

    static jobject GetJavaWindowContentFrameStats(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWindowContentFrameStats* stats);

    static jobject GetJavaWifiActivityEnergyInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiActivityEnergyInfo* info);

    static jobject GetJavaWifiChannel(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiChannel* channel);

    static bool ToElScanSettings(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jscanSettings,
        /* [out] */ IScanSettings** scanSettings);

    static bool ToElBatchedScanSettings(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jsettings,
        /* [out] */ IBatchedScanSettings** settings);

    static bool ToElMediaMetadata(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jmetadata,
        /* [out] */ IMediaMetadata** metadata);

    static bool ToElPlaybackState(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jstate,
        /* [out] */ IPlaybackState** state);

    static bool ToElPlaybackStateCustomAction(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jcustomeAction,
        /* [out] */ IPlaybackStateCustomAction** customeAction);

    static jobject GetJavaBatchedScanSettings(
        /* [in] */ JNIEnv* env,
        /* [in] */ IBatchedScanSettings* settings);

    static jobject GetJavaWifiConnectionStatistics(
        /* [in] */ JNIEnv* env,
        /* [in] */ IWifiConnectionStatistics* settings);

    static bool ToElNetworkTemplate(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jtempl,
        /* [out] */ INetworkTemplate** templ);

    static jobject GetJavaNetworkStats(
        /* [in] */ JNIEnv* env,
        /* [in] */ INetworkStats* stats);

    static jobject GetJavaNetworkStatsHistory(
        /* [in] */ JNIEnv* env,
        /* [in] */ INetworkStatsHistory* statsHistory);

    static bool ToElSparseArray(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject value,
        /* [out] */ ISparseArray** array);

    static bool ToElAudioPolicyConfig(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jconfig,
        /* [out] */ IAudioPolicyConfig** config);

    static jobject GetJavaAudioRoutesInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IAudioRoutesInfo* event);

    static jobject GetJavaMediaRouterClientState(
        /* [in] */ JNIEnv* env,
        /* [in] */ IMediaRouterClientState* state);

    static jobject GetJavaMediaRouterClientStateRouteInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ IMediaRouterClientStateRouteInfo* routeInfo);

    static jobject GetJavaMediaMetadata(
        /* [in] */ JNIEnv* env,
        /* [in] */ IMediaMetadata* data);

private:
    static void SetJavaComponentInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass parentClass,
        /* [in] */ jobject jparent,
        /* [in] */ IComponentInfo* comInfo);

    static void SetElComponentInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass c,
        /* [in] */ jobject jinfo,
        /* [in] */ IComponentInfo* info);

    static void SetJavaPackageItemInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass parentClass,
        /* [in] */ jobject jparent,
        /* [in] */ IPackageItemInfo* pkgInfo);

    static void SetElPackageItemInfo(
        /* [in] */ JNIEnv* env,
        /* [in] */ jclass c,
        /* [in] */ jobject jinfo,
        /* [in] */ IPackageItemInfo* info);

    static String FindPackageForObject(
        /* [in] */ JNIEnv* env,
        /* [in] */ jobject jinfo);
};

}

#endif // __ELUTIL_H_
