
#define LOG_TAG "ElSessionCallbackProxyJNI"
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
//#include <elastos/core/Object.h>
//#include <elastos/core/AutoLock.h>
//#include <elastos/utility/etl/HashMap.h>
//#include <Elastos.Droid.App.h>
//#include <Elastos.Droid.Telephony.h>
//#include <Elastos.Droid.Internal.h>
//#include <Elastos.Droid.Os.h>
//#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.Droid.Media.h>
#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::Media::Session::IISessionCallback;

static void android_media_session_ElSessionCallbackProxy_nativeOnCommand(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring command,
    /* [in] */ jobject args,
    /* [in] */ jobject cb)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnMediaButton(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject mediaButtonIntent,
    /* [in] */ jint sequenceNumber,
    /* [in] */ jobject cb)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnPlay(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnPlayFromMediaId(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring uri,
    /* [in] */ jobject extras)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnPlayFromSearch(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring query,
    /* [in] */ jobject extras)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnSkipToTrack(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jlong id)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnPause(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnStop(
    /* [in] */ JNIEnv *evn,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnNext(
    /* [in] */ JNIEnv *evn,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnPrevious(
    /* [in] */ JNIEnv *evn,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnFastForward(
    /* [in] */ JNIEnv *evn,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnRewind(
    /* [in] */ JNIEnv *evn,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnSeekTo(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jlong pos)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeSetRemoteControlClientBrowsedPlayer(
    /* [in] */ JNIEnv *evn,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeSetRemoteControlClientPlayItem(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jlong uid,
    /* [in] */ jint scope)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeGetRemoteControlClientNowPlayingEntries(
    /* [in] */ JNIEnv *evn,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnRate(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject rating)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnCustomAction(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring action,
    /* [in] */ jobject args)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnAdjustVolume(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jint direction)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionCallbackProxy_nativeOnSetVolumeTo(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jint value)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionCallbackProxy.cpp line:%d", __LINE__);
}


namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeOnCommand",      "(JLjava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnCommand},
    { "nativeOnMediaButton",      "(JLandroid/content/Intent;ILandroid/os/ResultReceiver;)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnMediaButton},
    { "nativeOnPlay",      "(J)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnPlay},
    { "nativeOnPlayFromMediaId",      "(JLjava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnPlayFromMediaId},
    { "nativeOnPlayFromSearch",      "(JLjava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnPlayFromSearch},
    { "nativeOnSkipToTrack",      "(JJ)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnSkipToTrack},
    { "nativeOnPause",      "(J)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnPause},
    { "nativeOnStop",      "(J)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnStop},
    { "nativeOnNext",      "(J)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnNext},
    { "nativeOnPrevious",      "(J)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnPrevious},
    { "nativeOnFastForward",      "(J)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnFastForward},
    { "nativeOnRewind",      "(J)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnRewind},
    { "nativeOnSeekTo",      "(JJ)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnSeekTo},
    { "nativeSetRemoteControlClientBrowsedPlayer",      "(J)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeSetRemoteControlClientBrowsedPlayer},
    { "nativeSetRemoteControlClientPlayItem",      "(JJI)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeSetRemoteControlClientPlayItem},
    { "nativeGetRemoteControlClientNowPlayingEntries",      "(J)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeGetRemoteControlClientNowPlayingEntries},
    { "nativeOnRate",      "(JLandroid/media/Rating;)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnRate},
    { "nativeOnCustomAction",      "(JLjava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnCustomAction},
    { "nativeOnAdjustVolume",      "(JI)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnAdjustVolume},
    { "nativeOnSetVolumeTo",      "(JI)V",
            (void*) android_media_session_ElSessionCallbackProxy_nativeOnSetVolumeTo},
};

int register_android_media_session_ElSessionCallbackProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/media/session/ElSessionCallbackProxy",
        gMethods, NELEM(gMethods));
}

}; // end namespace android
