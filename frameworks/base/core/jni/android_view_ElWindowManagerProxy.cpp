#define LOG_TAG "ElWindowManagerProxyJNI"

#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Graphics.h>
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Utility.h>
#include <Elastos.Droid.View.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Core.h>
#include <elastos/utility/etl/HashMap.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Core::ICharSequence;
using Elastos::Core::CString;
using Elastos::Droid::Content::Res::ICompatibilityInfo;
using Elastos::Droid::Graphics::IBitmap;
using Elastos::Droid::JavaProxy::CBinderNative;
using Elastos::Droid::JavaProxy::CIApplicationTokenNative;
using Elastos::Droid::JavaProxy::CIInputFilterNative;
using Elastos::Droid::JavaProxy::CIOnKeyguardExitResultNative;
using Elastos::Droid::JavaProxy::CIRemoteCallbackNative;
using Elastos::Droid::JavaProxy::CIRotationWatcherNative;
using Elastos::Droid::JavaProxy::CInputMethodClientNative;
using Elastos::Droid::JavaProxy::CInputContextNative;
using Elastos::Droid::JavaProxy::CIWindowSessionCallbackNative;
using Elastos::Droid::JavaProxy::ECLSID_CBinderNative;
using Elastos::Droid::Internal::View::IIInputMethodManager;
using Elastos::Droid::Internal::View::IInputMethodClient;
using Elastos::Droid::Internal::View::IIInputContext;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Os::IIRemoteCallback;
using Elastos::Droid::Utility::CParcelableList;
using Elastos::Droid::View::IApplicationToken;
using Elastos::Droid::View::IIWindowManager;
using Elastos::Droid::View::IIInputFilter;
using Elastos::Droid::View::IOnKeyguardExitResult;
using Elastos::Droid::View::IRotationWatcher;
using Elastos::Droid::View::IWindowSession;
using Elastos::Droid::View::IIWindowSessionCallback;
using Elastos::Utility::Etl::HashMap;

HashMap<Int32, AutoPtr<IInputMethodClient> > sIMClientMap;
Mutex sIMClientMapLock;
HashMap<Int32, AutoPtr<IIInputContext> > sInContextMap;
Mutex sInContextMapLock;
static HashMap<Int32, AutoPtr<IApplicationToken> > sAppTokenMap;
static Mutex sAppTokenMapLock;
static HashMap<Int32, AutoPtr<IBinder> > sWinTokenMap;
static Mutex sKeyguardTokenMapLock;
static HashMap<Int32, AutoPtr<IBinder> > sKeyguardTokenMap;
static Mutex sWinTokenMapLock;
static HashMap<Int32, AutoPtr<IRotationWatcher> > sWatchers;
static Mutex sWatchersLock;

static jobject android_view_ElWindowManagerProxy_nativeOpenSession(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcallback, jobject jclient, jint jclientHashCode, jobject jcontext, jint jContextHashCode)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeOpenSession()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jclient);

    AutoPtr<IInputMethodClient> client;
    ECode ec = CInputMethodClientNative::New((Handle64)jvm, (Handle64)jInstance, (IInputMethodClient**)&client);
    if (FAILED(ec)) {
        ALOGE("nativeOpenSession() new CInputMethodClient fail ec: 0x%x\n", ec);
    }
    {
        Mutex::Autolock lock(sIMClientMapLock);
        sIMClientMap[jclientHashCode] = client;
    }

    jobject jContextgInstance = env->NewGlobalRef(jcontext);

    AutoPtr<IIInputContext> context;
    ec = CInputContextNative::New((Handle64)jvm, (Handle64)jContextgInstance, (IIInputContext**)&context);
    if (FAILED(ec)) {
        ALOGE("nativeOpenSession() new CInputContextNative fail ec: 0x%x\n", ec);
    }
    {
        Mutex::Autolock lock(sInContextMapLock);
        sInContextMap[jContextHashCode] = context;
    }

    AutoPtr<IIWindowSessionCallback> callback;
    if (jcallback != NULL) {
        jobject jInstance = env->NewGlobalRef(jcallback);
        CIWindowSessionCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIWindowSessionCallback**)&callback);
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;

    AutoPtr<IWindowSession> session;
    ec = wm->OpenSession(callback, client, context, (IWindowSession**)&session);

    jclass c = env->FindClass("android/view/ElWindowSessionProxy");
    ElUtil::CheckErrorAndLog(env, "FindClass: ElWindowSessionProxy : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: ElWindowSessionProxy : %d!\n", __LINE__);

    jobject obj = env->NewObject(c, m, (jlong)session.Get());
    ElUtil::CheckErrorAndLog(env, "NewObject: ElWindowSessionProxy : %d!\n", __LINE__);
    session->AddRef();

    env->DeleteLocalRef(c);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeOpenSession()");
    return obj;
}

static jboolean android_view_ElWindowManagerProxy_nativeHasNavigationBar(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeHasNavigationBar()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean hasNavBar = FALSE;
    wm->HasNavigationBar(&hasNavBar);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeHasNavigationBar()");
    return (jboolean)hasNavBar;
}

static jboolean android_view_ElWindowManagerProxy_nativeHasPermanentMenuKey(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeHasPermanentMenuKey()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    wm->HasPermanentMenuKey(&result);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeHasPermanentMenuKey()");
    return (jboolean)result;
}

static jboolean android_view_ElWindowManagerProxy_nativeNeedsNavigationBar(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeNeedsNavigationBar()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    wm->NeedsNavigationBar(&result);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeNeedsNavigationBar()");
    return (jboolean)result;
}

static jfloat android_view_ElWindowManagerProxy_nativeGetAnimationScale(JNIEnv* env, jobject clazz, jlong jproxy, jint jwhich)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetAnimationScale()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Float scale = 0;
    wm->GetAnimationScale(jwhich, &scale);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetAnimationScale()");
    return (jfloat)scale;
}

static jboolean android_view_ElWindowManagerProxy_nativeInKeyguardRestrictedInputMode(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeInKeyguardRestrictedInputMode()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wm->InKeyguardRestrictedInputMode(&result);
    if (FAILED(ec))
        ALOGE("nativeInKeyguardRestrictedInputMode() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeInKeyguardRestrictedInputMode()");
    return (jboolean)result;
}

static jint android_view_ElWindowManagerProxy_nativeGetRotation(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetRotation()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Int32 rotation = -1;
    ECode ec = wm->GetRotation(&rotation);
    if (FAILED(ec))
        ALOGE("nativeGetRotation() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetRotation()");
    return (jint)rotation;
}

static void android_view_ElWindowManagerProxy_nativeAddAppToken(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jaddPos, jobject jtoken, jint jgroupId, jint jstackId, jint jrequestedOrientation, jboolean jfullscreen, jboolean jshowWhenLocked,
    jint juserId, jint jconfigChanges, jboolean jvoiceInteraction, jboolean jlaunchTaskBehind)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeAddAppToken()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jtoken);
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IApplicationToken> token;
    if(NOERROR != CIApplicationTokenNative::New((Handle64)jvm, (Handle64)jInstance, (IApplicationToken**)&token)) {
        ALOGE("nativeAddAppToken() new CIApplicationTokenNative fail!\n");
    }

    {
        Mutex::Autolock lock(sAppTokenMapLock);
        sAppTokenMap[hashCode] = token;
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->AddAppToken(jaddPos, token, jgroupId, jstackId, jrequestedOrientation, jfullscreen,
        jshowWhenLocked, juserId, jconfigChanges, jvoiceInteraction, jlaunchTaskBehind);
    if (FAILED(ec))
        ALOGE("nativeAddAppToken() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeAddAppToken()");
}

static void android_view_ElWindowManagerProxy_nativeAddWindowToken(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jint jtype)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeAddWindowToken()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jtoken);
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IBinder> token;
    if(NOERROR != CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&token)) {
        ALOGE("nativeAddWindowToken() new CBinderNative fail!\n");
    }

    {
        Mutex::Autolock lock(sWinTokenMapLock);
        sWinTokenMap[hashCode] = token;
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->AddWindowToken(token, (Int32)jtype);
    if (FAILED(ec))
        ALOGE("nativeAddWindowToken() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeAddWindowToken()");
}

static void android_view_ElWindowManagerProxy_nativeClearForcedDisplaySize(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeClearForcedDisplaySize()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->ClearForcedDisplaySize((Int32)jdisplayId);
    if (FAILED(ec))
        ALOGE("nativeClearForcedDisplaySize() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeClearForcedDisplaySize()");
}

static void android_view_ElWindowManagerProxy_nativeClearForcedDisplayDensity(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeClearForcedDisplayDensity()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->ClearForcedDisplayDensity((Int32)jdisplayId);
    if (FAILED(ec))
        ALOGE("nativeClearForcedDisplayDensity() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeClearForcedDisplayDensity()");
}

static void android_view_ElWindowManagerProxy_nativeSetOverscan(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId, jint left, jint top, jint right, jint bottom)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetOverscan()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetOverscan((Int32)jdisplayId, left, top, right, bottom);
    if (FAILED(ec))
        ALOGE("nativeSetOverscan() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetOverscan()");
}

static void android_view_ElWindowManagerProxy_nativeCloseSystemDialogs(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jreason)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeCloseSystemDialogs()");

    String reason = ElUtil::ToElString(env, jreason);

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->CloseSystemDialogs(reason);
    if (FAILED(ec))
        ALOGE("nativeCloseSystemDialogs() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeCloseSystemDialogs()");
}

static void android_view_ElWindowManagerProxy_nativeStartFreezingScreen(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jexitAnim, jint jenterAnim)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeStartFreezingScreen()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->StartFreezingScreen((Int32)jexitAnim, (Int32)jenterAnim);
    if (FAILED(ec))
        ALOGE("nativeStartFreezingScreen() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeStartFreezingScreen()");
}

static void android_view_ElWindowManagerProxy_nativeStopFreezingScreen(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeStopFreezingScreen()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->StopFreezingScreen();
    if (FAILED(ec))
        ALOGE("nativeStopFreezingScreen() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeStopFreezingScreen()");
}

static void android_view_ElWindowManagerProxy_nativeDisableKeyguard(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jstring jtag)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeDisableKeyguard()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IBinder> token;
    if (NULL == token) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jtoken);

        if(NOERROR != CBinderNative::New((Handle64)jvm, (Handle64)jInstance, (IBinder**)&token)) {
            ALOGE("nativeDisableKeyguard() new CBinderNative fail!\n");
        }

        Mutex::Autolock lock(sKeyguardTokenMapLock);
        sKeyguardTokenMap[hashCode] = token;
    }

    String tag = ElUtil::ToElString(env, jtag);

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    wm->DisableKeyguard(token, tag);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeDisableKeyguard()");
}

static void android_view_ElWindowManagerProxy_nativeExecuteAppTransition(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeExecuteAppTransition()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->ExecuteAppTransition();
    if (FAILED(ec))
        ALOGE("nativeExecuteAppTransition() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeExecuteAppTransition()");
}

static void android_view_ElWindowManagerProxy_nativeExitKeyguardSecurely(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcallback)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeExitKeyguardSecurely()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jcallback);

    AutoPtr<IOnKeyguardExitResult> callback;
    if(NOERROR != CIOnKeyguardExitResultNative::New((Handle64)jvm, (Handle64)jInstance, (IOnKeyguardExitResult**)&callback)) {
        ALOGE("nativeExitKeyguardSecurely() new CIOnKeyguardExitResultNative fail!\n");
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    wm->ExitKeyguardSecurely(callback);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeExitKeyguardSecurely()");
}

static void android_view_ElWindowManagerProxy_nativeFreezeRotation(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jrotation)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeFreezeRotation()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->FreezeRotation((Int32)jrotation);
    if (FAILED(ec))
        ALOGE("nativeFreezeRotation() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeFreezeRotation()");
}

static jfloatArray android_view_ElWindowManagerProxy_nativeGetAnimationScales(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jrotation)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetAnimationScales()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;

    AutoPtr<ArrayOf<Float> > scales;
    ECode ec = wm->GetAnimationScales((ArrayOf<Float>**)&scales);
    if (FAILED(ec))
        ALOGE("nativeGetAnimationScales() ec: 0x%08x", ec);

    jfloatArray jscales = NULL;
    if (scales != NULL) {
        jscales = ElUtil::GetJavaFloatArray(env, scales);
    }

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetAnimationScales()");
    return jscales;
}

static jint android_view_ElWindowManagerProxy_nativeGetAppOrientation(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetAppOrientation()");

    AutoPtr<IApplicationToken> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = it->mSecond;
        }

        if (NULL == token) {
            ALOGE("nativeGetAppOrientation() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Int32 rotation = -1;
    ECode ec = wm->GetAppOrientation(token, &rotation);
    if (FAILED(ec))
        ALOGE("nativeGetAppOrientation() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetAppOrientation()");
    return (jint)rotation;
}

static jint android_view_ElWindowManagerProxy_nativeGetPendingAppTransition(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetPendingAppTransition()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Int32 transition = 0;
    ECode ec = wm->GetPendingAppTransition(&transition);
    if (FAILED(ec))
        ALOGE("nativeGetPendingAppTransition() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetPendingAppTransition()");
    return (jint)transition;
}

static jboolean android_view_ElWindowManagerProxy_nativeInputMethodClientHasFocus(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jclient)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeInputMethodClientHasFocus()");

    AutoPtr<IInputMethodClient> client;
    if (jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);
        Mutex::Autolock lock(sIMClientMapLock);
        HashMap<Int32, AutoPtr<IInputMethodClient> >::Iterator it = sIMClientMap.Find(hashCode);
        if (it != sIMClientMap.End()) {
            client = it->mSecond;
        }

        if (NULL == client) {
            ALOGE("nativeInputMethodClientHasFocus() Invalid jclient!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean hasFocus = FALSE;
    ECode ec = wm->InputMethodClientHasFocus(client, &hasFocus);
    if (FAILED(ec))
        ALOGE("nativeInputMethodClientHasFocus() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeInputMethodClientHasFocus()");
    return (jboolean)hasFocus;
}

static jboolean android_view_ElWindowManagerProxy_nativeIsKeyguardLocked(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeIsKeyguardLocked()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wm->IsKeyguardLocked(&result);
    if (FAILED(ec))
        ALOGE("nativeIsKeyguardLocked() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeIsKeyguardLocked()");
    return (jboolean)result;
}

static jboolean android_view_ElWindowManagerProxy_nativeIsKeyguardSecure(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeIsKeyguardSecure()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wm->IsKeyguardSecure(&result);
    if (FAILED(ec))
        ALOGE("nativeIsKeyguardSecure() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeIsKeyguardSecure()");
    return (jboolean)result;
}

static jboolean android_view_ElWindowManagerProxy_nativeIsViewServerRunning(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeIsViewServerRunning()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wm->IsViewServerRunning(&result);
    if (FAILED(ec))
        ALOGE("nativeIsViewServerRunning() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeIsViewServerRunning()");
    return (jboolean)result;
}

static void android_view_ElWindowManagerProxy_nativeOverridePendingAppTransition(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint jenterAnim, jint jexitAnim, jobject jstartedCallback)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeOverridePendingAppTransition()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IIRemoteCallback> startedCallback;
    if (jstartedCallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jstartedCallback);

        if(NOERROR != CIRemoteCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIRemoteCallback**)&startedCallback)) {
            ALOGE("nativeOverridePendingAppTransition() new CIRemoteCallbackNative fail!\n");
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->OverridePendingAppTransition(packageName, (Int32)jenterAnim, (Int32)jexitAnim, startedCallback);
    if (FAILED(ec))
        ALOGE("nativeOverridePendingAppTransition() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeOverridePendingAppTransition()");
}

static void android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionScaleUp(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstartX, jint jstartY, jint jstartWidth, jint jstartHeight)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionScaleUp()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->OverridePendingAppTransitionScaleUp((Int32)jstartX, (Int32)jstartY, (Int32)jstartWidth, (Int32)jstartHeight);
    if (FAILED(ec))
        ALOGE("nativeOverridePendingAppTransitionScaleUp() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionScaleUp()");
}

static void android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionThumb(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jsrcThumb, jint jstartX, jint jstartY, jobject jstartedCallback, jboolean jscaleUp)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionThumb()");

    AutoPtr<IBitmap> srcThumb;
    if (jsrcThumb != NULL) {
        if (!ElUtil::ToElBitmap(env, jsrcThumb, (IBitmap**)&srcThumb)) {
            ALOGE("nativeOverridePendingAppTransition() ToElBitmap fail!");
        }
    }

    AutoPtr<IIRemoteCallback> startedCallback;
    if (jstartedCallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jstartedCallback);

        if(NOERROR != CIRemoteCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIRemoteCallback**)&startedCallback)) {
            ALOGE("nativeOverridePendingAppTransition() new CIRemoteCallbackNative fail!\n");
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->OverridePendingAppTransitionThumb(srcThumb, (Int32)jstartX, (Int32)jstartY, startedCallback, (Boolean)jscaleUp);
    if (FAILED(ec))
        ALOGE("nativeOverridePendingAppTransitionThumb() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionThumb()");
}

static void android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionAspectScaledThumb(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jsrcThumb, jint jstartX, jint jstartY, jint jtargetWidth, jint jtargetHeight, jobject jstartedCallback, jboolean jscaleUp)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionAspectScaledThumb()");

    AutoPtr<IBitmap> srcThumb;
    if (jsrcThumb != NULL) {
        if (!ElUtil::ToElBitmap(env, jsrcThumb, (IBitmap**)&srcThumb)) {
            ALOGE("nativeOverridePendingAppTransition() ToElBitmap fail!");
        }
    }

    AutoPtr<IIRemoteCallback> startedCallback;
    if (jstartedCallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jstartedCallback);

        if(NOERROR != CIRemoteCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIRemoteCallback**)&startedCallback)) {
            ALOGE("nativeOverridePendingAppTransition() new CIRemoteCallbackNative fail!\n");
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->OverridePendingAppTransitionAspectScaledThumb(srcThumb, (Int32)jstartX, (Int32)jstartY,
        jtargetWidth, jtargetHeight, startedCallback, (Boolean)jscaleUp);
    if (FAILED(ec))
        ALOGE("nativeOverridePendingAppTransitionAspectScaledThumb() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionAspectScaledThumb()");
}

static void android_view_ElWindowManagerProxy_nativePauseKeyDispatching(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativePauseKeyDispatching()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sWinTokenMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sWinTokenMap.Find(hashCode);
        if (it != sWinTokenMap.End()) {
            token = it->mSecond;
        }

        if (NULL == token) {
            ALOGE("nativePauseKeyDispatching() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->PauseKeyDispatching(token);
    if (FAILED(ec))
        ALOGE("nativePauseKeyDispatching() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativePauseKeyDispatching()");
}

static void android_view_ElWindowManagerProxy_nativePrepareAppTransition(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jtransit, jboolean jalwaysKeepCurrent)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativePrepareAppTransition()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->PrepareAppTransition((Int32)jtransit, (Boolean)jalwaysKeepCurrent);
    if (FAILED(ec))
        ALOGE("nativePrepareAppTransition() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativePrepareAppTransition()");
}

static void android_view_ElWindowManagerProxy_nativeReenableKeyguard(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeReenableKeyguard()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IBinder> token;
    {
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sKeyguardTokenMap.Find(hashCode);
        if (it != sKeyguardTokenMap.End()) {
            token = it->mSecond;
            sKeyguardTokenMap.Erase(it);
        }
    }

    if (NULL == token) {
        ALOGE("nativeReenableKeyguard() Invalid jtoken!");
        return;
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    wm->ReenableKeyguard(token);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeReenableKeyguard()");
}

static void android_view_ElWindowManagerProxy_nativeRemoveAppToken(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeRemoveAppToken()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IBinder> token;
    {
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
            sAppTokenMap.Erase(it);
        }
    }

    if (NULL == token) {
        ALOGE("nativeRemoveAppToken() Invalid jtoken!\n");
        env->ExceptionDescribe();
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->RemoveAppToken(token);
    if (FAILED(ec))
        ALOGE("nativeRemoveAppToken() ec: 0x%08x", ec);
    // ALOGD("- android_view_ElWindowManagerProxy_nativeRemoveAppToken()");
}

static void android_view_ElWindowManagerProxy_nativeRemoveWindowToken(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeRemoveWindowToken()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);

    AutoPtr<IBinder> token;
    {
        Mutex::Autolock lock(sWinTokenMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sWinTokenMap.Find(hashCode);
        if (it != sWinTokenMap.End()) {
            token = it->mSecond;
            sWinTokenMap.Erase(it);
        }
    }

    if (NULL == token) {
        ALOGE("nativeRemoveWindowToken() Invalid jtoken!\n");
        env->ExceptionDescribe();
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->RemoveWindowToken(token);
    if (FAILED(ec))
        ALOGE("nativeRemoveWindowToken() ec: 0x%08x", ec);


    // ALOGD("- android_view_ElWindowManagerProxy_nativeRemoveWindowToken()");
}

static void android_view_ElWindowManagerProxy_nativeResumeKeyDispatching(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeResumeKeyDispatching()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sWinTokenMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sWinTokenMap.Find(hashCode);
        if (it != sWinTokenMap.End()) {
            token = it->mSecond;
        }

        if (NULL == token) {
            ALOGE("nativeResumeKeyDispatching() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->ResumeKeyDispatching(token);
    if (FAILED(ec))
        ALOGE("nativeResumeKeyDispatching() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeResumeKeyDispatching()");
}

static jboolean android_view_ElWindowManagerProxy_nativeIsRotationFrozen(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeIsRotationFrozen()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result;
    ECode ec = wm->IsRotationFrozen(&result);
    if (FAILED(ec))
        ALOGE("nativeIsRotationFrozen() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeIsRotationFrozen()");
    return result;
}

static jobject android_view_ElWindowManagerProxy_nativeScreenshotApplications(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jint jdisplayId, jint jwidth, jint jheight, jboolean force565)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeScreenshotApplications()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeScreenshotApplications() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    AutoPtr<IBitmap> bitmap;
    ECode ec = wm->ScreenshotApplications(token, (Int32)jdisplayId, (Int32)jwidth, (Int32)jheight, force565, (IBitmap**)&bitmap);
    if (FAILED(ec))
        ALOGE("nativeScreenshotApplications() ec: 0x%08x", ec);

    jobject jbitmap = NULL;
    if (bitmap != NULL) {
        jbitmap = ElUtil::GetJavaBitmap(env, bitmap);
    }

    // ALOGD("- android_view_ElWindowManagerProxy_nativeScreenshotApplications()");
    return jbitmap;
}

static void android_view_ElWindowManagerProxy_nativeSetAnimationScale(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jwhich, jfloat jscale)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetAnimationScale()");


    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetAnimationScale((Int32)jwhich, (Float)jscale);
    if (FAILED(ec))
        ALOGE("nativeSetAnimationScale() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetAnimationScale()");
}

static void android_view_ElWindowManagerProxy_nativeSetAnimationScales(JNIEnv* env, jobject clazz, jlong jproxy,
    jfloatArray jscales)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetAnimationScales()");

    AutoPtr<ArrayOf<Float> > scales;
    if (jscales != NULL) {
        if (!ElUtil::ToElFloatArray(env, jscales, (ArrayOf<Float>**)&scales)) {
            ALOGE("nativeSetAnimationScales() ToElFloatArray fail!");
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetAnimationScales(scales);
    if (FAILED(ec))
        ALOGE("nativeSetAnimationScales() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetAnimationScales()");
}

static jfloat android_view_ElWindowManagerProxy_nativeGetCurrentAnimatorScale(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetCurrentAnimatorScale()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Float scale;
    ECode ec = wm->GetCurrentAnimatorScale(&scale);
    if (FAILED(ec))
        ALOGE("nativeGetCurrentAnimatorScale() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetCurrentAnimatorScale()");
    return scale;
}

static void android_view_ElWindowManagerProxy_nativeSetAppGroupId(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jint jgroupId)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetAppGroupId()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeSetAppGroupId() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetAppGroupId(token, (Int32)jgroupId);
    if (FAILED(ec))
        ALOGE("nativeSetAppGroupId() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetAppGroupId()");
}

static void android_view_ElWindowManagerProxy_nativeSetAppOrientation(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jint jrequestedOrientation)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetAppOrientation()");

    AutoPtr<IApplicationToken> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = it->mSecond;
        }

        if (NULL == token) {
            ALOGE("nativeSetAppOrientation() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetAppOrientation(token, (Int32)jrequestedOrientation);
    if (FAILED(ec))
        ALOGE("nativeSetAppOrientation() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetAppOrientation()");
}

static void android_view_ElWindowManagerProxy_nativeSetAppStartingWindow(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jstring jpkg, jint jtheme, jobject jcompatInfo, jobject jnonLocalizedLabel, jint jlabelRes,
    jint jicon, jint jlogo, jint jwindowFlags, jobject jtransferFrom, jboolean jcreateIfNeeded)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetAppStartingWindow()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeSetAppStartingWindow() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    String pkg = ElUtil::ToElString(env, jpkg);

    AutoPtr<ICompatibilityInfo> compatInfo;
    if (jcompatInfo != NULL) {
        if (!ElUtil::ToElCompatibilityInfo(env, jcompatInfo, (ICompatibilityInfo**)&compatInfo)) {
            ALOGE("nativeSetAppStartingWindow() ToElCompatibilityInfo fail!");
        }
    }

    AutoPtr<ICharSequence> nonLocalizedLabel;
    if (jnonLocalizedLabel != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        ElUtil::CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jsnonLocalizedLabel = (jstring)env->CallObjectMethod(jnonLocalizedLabel, m);
        ElUtil::CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String snonLocalizedLabel = ElUtil::ToElString(env, jsnonLocalizedLabel);

        CString::New(snonLocalizedLabel, (ICharSequence**)&nonLocalizedLabel);
        env->DeleteLocalRef(csClass);
        env->DeleteLocalRef(jsnonLocalizedLabel);
    }

    AutoPtr<IBinder> transferFrom;
    if (jtransferFrom != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtransferFrom);
        Mutex::Autolock lock(sWinTokenMapLock);
        HashMap<Int32, AutoPtr<IBinder> >::Iterator it = sWinTokenMap.Find(hashCode);
        if (it != sWinTokenMap.End()) {
            transferFrom = it->mSecond;
        }

        if (NULL == transferFrom) {
            ALOGE("nativeSetAppStartingWindow() Invalid jtransferFrom!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetAppStartingWindow(token, pkg, (Int32)jtheme, compatInfo, nonLocalizedLabel, (Int32)jlabelRes,
        (Int32)jicon, jlogo, (Int32)jwindowFlags, transferFrom, (Boolean)jcreateIfNeeded);
    if (FAILED(ec))
        ALOGE("nativeSetAppStartingWindow() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetAppStartingWindow()");
}

static void android_view_ElWindowManagerProxy_nativeSetAppVisibility(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jboolean jvisible)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetAppVisibility()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeSetAppVisibility() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetAppVisibility(token, (Boolean)jvisible);
    if (FAILED(ec))
        ALOGE("nativeSetAppVisibility() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetAppVisibility()");
}

static void android_view_ElWindowManagerProxy_nativeSetAppWillBeHidden(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetAppWillBeHidden()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeSetAppWillBeHidden() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetAppWillBeHidden(token);
    if (FAILED(ec))
        ALOGE("nativeSetAppWillBeHidden() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetAppWillBeHidden()");
}

static void android_view_ElWindowManagerProxy_nativeSetEventDispatching(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jenabled)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetEventDispatching()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetEventDispatching((Boolean)jenabled);
    if (FAILED(ec))
        ALOGE("nativeSetEventDispatching() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetEventDispatching()");
}

static void android_view_ElWindowManagerProxy_nativeSetFocusedApp(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jboolean jmoveFocusNow)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetFocusedApp()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeSetFocusedApp() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetFocusedApp(token, (Boolean)jmoveFocusNow);
    if (FAILED(ec))
        ALOGE("nativeSetFocusedApp() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetFocusedApp()");
}

static void android_view_ElWindowManagerProxy_nativeGetInitialDisplaySize(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId, jobject jsize)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetInitialDisplaySize()");

    AutoPtr<IPoint> size;
    if (jsize != NULL) {
        if (!ElUtil::ToElPoint(env, jsize, (IPoint**)&size))
            ALOGE("nativeGetInitialDisplaySize: ToElPoint fail");
    }
    IIWindowManager* wm = (IIWindowManager*)jproxy;
    AutoPtr<IPoint> outSize;
    ECode ec = wm->GetInitialDisplaySize((Int32)jdisplayId, size, (IPoint**)&outSize);
    if (FAILED(ec))
        ALOGE("nativeGetInitialDisplaySize() ec: 0x%08x", ec);

    if (outSize != NULL) {
        jclass pointKlass = env->FindClass("android/graphics/Point");
        ElUtil::CheckErrorAndLog(env, "nativeGetInitialDisplaySize Fail FindClass: Point %d! \n", __LINE__);

        Int32 x, y;
        outSize->GetX(&x);
        outSize->GetY(&y);
        ElUtil::SetJavaIntField(env, pointKlass, jsize, x, "x", "nativeGetInitialDisplaySize");
        ElUtil::SetJavaIntField(env, pointKlass, jsize, y, "y", "nativeGetInitialDisplaySize");
    }

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetInitialDisplaySize()");
}

static void android_view_ElWindowManagerProxy_nativeGetBaseDisplaySize(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId, jobject jsize)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetBaseDisplaySize()");

    AutoPtr<IPoint> size;
    if (jsize != NULL) {
        if (!ElUtil::ToElPoint(env, jsize, (IPoint**)&size))
            ALOGE("nativeGetBaseDisplaySize: ToElPoint fail");
    }
    IIWindowManager* wm = (IIWindowManager*)jproxy;
    AutoPtr<IPoint> outSize;
    ECode ec = wm->GetBaseDisplaySize((Int32)jdisplayId, size, (IPoint**)&outSize);
    if (FAILED(ec))
        ALOGE("nativeGetBaseDisplaySize() ec: 0x%08x", ec);

    if (outSize != NULL) {
        jclass pointKlass = env->FindClass("android/graphics/Point");
        ElUtil::CheckErrorAndLog(env, "nativeGetInitialDisplaySize Fail FindClass: Point %d!\n", __LINE__);

        Int32 x, y;
        outSize->GetX(&x);
        outSize->GetY(&y);
        ElUtil::SetJavaIntField(env, pointKlass, jsize, x, "x", "nativeGetInitialDisplaySize");
        ElUtil::SetJavaIntField(env, pointKlass, jsize, y, "y", "nativeGetInitialDisplaySize");
    }

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetBaseDisplaySize()");
}

static jint android_view_ElWindowManagerProxy_nativeGetInitialDisplayDensity(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetInitialDisplayDensity()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Int32 density;
    ECode ec = wm->GetInitialDisplayDensity((Int32)jdisplayId, &density);
    if (FAILED(ec))
        ALOGE("nativeGetInitialDisplayDensity() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetInitialDisplayDensity()");
    return density;
}

static jint android_view_ElWindowManagerProxy_nativeGetBaseDisplayDensity(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetBaseDisplayDensity()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Int32 density;
    ECode ec = wm->GetBaseDisplayDensity((Int32)jdisplayId, &density);
    if (FAILED(ec))
        ALOGE("nativeGetBaseDisplayDensity() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetBaseDisplayDensity()");
    return density;
}

static void android_view_ElWindowManagerProxy_nativeSetForcedDisplaySize(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId, jint jwidth, jint jheight)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetForcedDisplaySize()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetForcedDisplaySize((Int32)jdisplayId, (Int32)jwidth, (Int32)jheight);
    if (FAILED(ec))
        ALOGE("nativeSetForcedDisplaySize() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetForcedDisplaySize()");
}

static void android_view_ElWindowManagerProxy_nativeSetForcedDisplayDensity(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId, jint jdensity)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetForcedDisplayDensity()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetForcedDisplayDensity((Int32)jdisplayId, (Int32)jdensity);
    if (FAILED(ec))
        ALOGE("nativeSetForcedDisplayDensity() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetForcedDisplayDensity()");
}

static void android_view_ElWindowManagerProxy_nativeSetInTouchMode(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jmode)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetInTouchMode()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetInTouchMode((Boolean)jmode);
    if (FAILED(ec))
        ALOGE("nativeSetInTouchMode() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetInTouchMode()");
}

static void android_view_ElWindowManagerProxy_nativeSetNewConfiguration(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jconfig)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetNewConfiguration()");

    AutoPtr<IConfiguration> config;
    if (jconfig != NULL) {
        if (!ElUtil::ToElConfiguration(env, jconfig, (IConfiguration**)&config)) {
            ALOGE("nativeSetNewConfiguration() ToElConfiguration fail!");
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetNewConfiguration(config);
    if (FAILED(ec))
        ALOGE("nativeSetNewConfiguration() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetNewConfiguration()");
}

static void android_view_ElWindowManagerProxy_nativeUpdateRotation(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jalwaysSendConfiguration, jboolean jforceRelayout)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeUpdateRotation()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->UpdateRotation((Boolean)jalwaysSendConfiguration, (Boolean)jforceRelayout);
    if (FAILED(ec))
        ALOGE("nativeUpdateRotation() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeUpdateRotation()");
}

static void android_view_ElWindowManagerProxy_nativeSetStrictModeVisualIndicatorPreference(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jvalue)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetStrictModeVisualIndicatorPreference()");

    String value = ElUtil::ToElString(env, jvalue);

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetStrictModeVisualIndicatorPreference(value);
    if (FAILED(ec))
        ALOGE("nativeSetStrictModeVisualIndicatorPreference() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetStrictModeVisualIndicatorPreference()");
}

static void android_view_ElWindowManagerProxy_nativeSetScreenCaptureDisabled(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jalwaysSendConfiguration, jint userId, jboolean disabled)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeSetScreenCaptureDisabled()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->SetScreenCaptureDisabled(userId, disabled);
    if (FAILED(ec))
        ALOGE("nativeSetScreenCaptureDisabled() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeSetScreenCaptureDisabled()");
}

static void android_view_ElWindowManagerProxy_nativeShowStrictModeViolation(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jon)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeShowStrictModeViolation()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->ShowStrictModeViolation((Boolean)jon);
    if (FAILED(ec))
        ALOGE("nativeShowStrictModeViolation() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeShowStrictModeViolation()");
}

static void android_view_ElWindowManagerProxy_nativeStartAppFreezingScreen(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jint jconfigChanges)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeStartAppFreezingScreen()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeSetFocusedApp() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->StartAppFreezingScreen(token, (Int32)jconfigChanges);
    if (FAILED(ec))
        ALOGE("nativeStartAppFreezingScreen() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeStartAppFreezingScreen()");
}

static jboolean android_view_ElWindowManagerProxy_nativeStartViewServer(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jport)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeStartViewServer()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wm->StartViewServer((Int32)jport, &result);
    if (FAILED(ec))
        ALOGE("nativeStartViewServer() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeStartViewServer()");
    return (jboolean)result;
}

static jint android_view_ElWindowManagerProxy_nativeGetLastWallpaperX(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetLastWallpaperX()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Int32 result;
    ECode ec = wm->GetLastWallpaperX(&result);
    if (FAILED(ec))
        ALOGE("nativeGetLastWallpaperX() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetLastWallpaperX()");
    return result;
}

static jint android_view_ElWindowManagerProxy_nativeGetLastWallpaperY(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetLastWallpaperY()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Int32 result;
    ECode ec = wm->GetLastWallpaperY(&result);
    if (FAILED(ec))
        ALOGE("nativeGetLastWallpaperY() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetLastWallpaperY()");
    return result;
}

static void android_view_ElWindowManagerProxy_nativeStatusBarVisibilityChanged(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jvisibility)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeStatusBarVisibilityChanged()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->StatusBarVisibilityChanged((Int32)jvisibility);
    if (FAILED(ec))
        ALOGE("nativeStatusBarVisibilityChanged() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeStatusBarVisibilityChanged()");
}

static void android_view_ElWindowManagerProxy_nativeStopAppFreezingScreen(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken, jboolean jforce)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeStopAppFreezingScreen()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeSetFocusedApp() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->StopAppFreezingScreen(token, (Boolean)jforce);
    if (FAILED(ec))
        ALOGE("nativeStopAppFreezingScreen() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeStopAppFreezingScreen()");
}

static jboolean android_view_ElWindowManagerProxy_nativeStopViewServer(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeStopViewServer()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wm->StopViewServer(&result);
    if (FAILED(ec))
        ALOGE("nativeStopViewServer() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeStopViewServer()");
    return (jboolean)result;
}

static void android_view_ElWindowManagerProxy_nativeThawRotation(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeThawRotation()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->ThawRotation();
    if (FAILED(ec))
        ALOGE("nativeThawRotation() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeThawRotation()");
}

static jobject android_view_ElWindowManagerProxy_nativeUpdateOrientationFromAppTokens(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcurrentConfig, jobject jfreezeThisOneIfNeeded)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeUpdateOrientationFromAppTokens()");

    AutoPtr<IConfiguration> currentConfig;
    if (jcurrentConfig != NULL) {
        if (!ElUtil::ToElConfiguration(env, jcurrentConfig, (IConfiguration**)&currentConfig)) {
            ALOGE("nativeUpdateOrientationFromAppTokens() ToElConfiguration fail!");
        }
    }

    AutoPtr<IBinder> freezeThisOneIfNeeded;
    if (jfreezeThisOneIfNeeded != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jfreezeThisOneIfNeeded);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            freezeThisOneIfNeeded = IBinder::Probe(it->mSecond);
        }

        if (NULL == freezeThisOneIfNeeded) {
            ALOGE("nativeUpdateOrientationFromAppTokens() Invalid jfreezeThisOneIfNeeded!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    AutoPtr<IConfiguration> config;
    ECode ec = wm->UpdateOrientationFromAppTokens(currentConfig, freezeThisOneIfNeeded, (IConfiguration**)&config);
    if (FAILED(ec))
        ALOGE("nativeUpdateOrientationFromAppTokens() ec: 0x%08x", ec);

    jobject jconfig = NULL;
    if (config != NULL) {
        jconfig = ElUtil::GetJavaConfiguration(env, config);
    }

    // ALOGD("- android_view_ElWindowManagerProxy_nativeUpdateOrientationFromAppTokens()");
    return jconfig;
}

static jint android_view_ElWindowManagerProxy_nativeWatchRotation(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwatcher)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeWatchRotation()");

    AutoPtr<IRotationWatcher> watcher;
    if (jwatcher != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jwatcher);

        Mutex::Autolock lock(sWatchersLock);
        if (sWatchers.Find(hashCode) != sWatchers.End()) {
            watcher = sWatchers[hashCode];
        }

        if (watcher == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jwatcher);

            if(NOERROR != CIRotationWatcherNative::New((Handle64)jvm, (Handle64)jInstance, (IRotationWatcher**)&watcher)) {
                ALOGE("nativeWatchRotation() new CIRotationWatcherNative fail!\n");
            }

            sWatchers[hashCode] = watcher;
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Int32 rotation = -1;
    ECode ec = wm->WatchRotation(watcher, &rotation);
    if (FAILED(ec))
        ALOGE("nativeWatchRotation() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeWatchRotation()");
    return (jint)rotation;
}

static void android_view_ElWindowManagerProxy_nativeRemoveRotationWatcher(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwatcher)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeRemoveRotationWatcher()");

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jwatcher);
    AutoPtr<IRotationWatcher> watcher;
    {
        Mutex::Autolock lock(sWatchersLock);
        if (sWatchers.Find(hashCode) != sWatchers.End()) {
            watcher = sWatchers[hashCode];
            sWatchers.Erase(hashCode);
        }
    }

    if (NULL == watcher) {
        ALOGE("nativeRemoveRotationWatcher() Invalid jwatcher!\n");
        env->ExceptionDescribe();
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->RemoveRotationWatcher(watcher);
    if (FAILED(ec))
        ALOGE("nativeRemoveRotationWatcher() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeRemoveRotationWatcher()");
}

static jint android_view_ElWindowManagerProxy_nativeGetPreferredOptionsPanelGravity(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetPreferredOptionsPanelGravity()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Int32 value = 0;
    ECode ec = wm->GetPreferredOptionsPanelGravity(&value);
    if (FAILED(ec))
        ALOGE("nativeGetPreferredOptionsPanelGravity() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetPreferredOptionsPanelGravity()");
    return (jint)value;
}

static void android_view_ElWindowManagerProxy_nativeDismissKeyguard(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeDismissKeyguard()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->DismissKeyguard();
    if (FAILED(ec))
        ALOGE("nativeDismissKeyguard() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeDismissKeyguard()");
}

static void android_view_ElWindowManagerProxy_nativeKeyguardGoingAway(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean disableWindowAnimations, jboolean keyguardGoingToNotificationShade, jboolean keyguardShowingMedia)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeKeyguardGoingAway()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->KeyguardGoingAway(disableWindowAnimations, keyguardGoingToNotificationShade, keyguardShowingMedia);
    if (FAILED(ec))
        ALOGE("nativeKeyguardGoingAway() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeKeyguardGoingAway()");
}

static void android_view_ElWindowManagerProxy_nativeLockNow(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject joptions)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeLockNow()");

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if(!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeLockNow() ToElBundle fail!\n");
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->LockNow(options);
    if (FAILED(ec))
        ALOGE("nativeLockNow() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeLockNow()");
}

static jboolean android_view_ElWindowManagerProxy_nativeIsSafeModeEnabled(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeIsSafeModeEnabled()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wm->IsSafeModeEnabled(&result);
    if (FAILED(ec))
        ALOGE("nativeIsSafeModeEnabled() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeIsSafeModeEnabled()");
    return (jboolean)result;
}

static void android_view_ElWindowManagerProxy_nativeEnableScreenIfNeeded(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeEnableScreenIfNeeded()");

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    ECode ec = wm->EnableScreenIfNeeded();
    if (FAILED(ec))
        ALOGE("nativeEnableScreenIfNeeded() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeEnableScreenIfNeeded()");
}

static jboolean android_view_ElWindowManagerProxy_nativeClearWindowContentFrameStats(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeClearWindowContentFrameStats()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeClearWindowContentFrameStats() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    Boolean result = FALSE;
    ECode ec = wm->ClearWindowContentFrameStats(token, &result);
    if (FAILED(ec))
        ALOGE("nativeClearWindowContentFrameStats() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeClearWindowContentFrameStats()");
    return (jboolean)result;
}

static jobject android_view_ElWindowManagerProxy_nativeGetWindowContentFrameStats(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtoken)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeGetWindowContentFrameStats()");

    AutoPtr<IBinder> token;
    if (jtoken != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jtoken);
        Mutex::Autolock lock(sAppTokenMapLock);
        HashMap<Int32, AutoPtr<IApplicationToken> >::Iterator it = sAppTokenMap.Find(hashCode);
        if (it != sAppTokenMap.End()) {
            token = IBinder::Probe(it->mSecond);
        }

        if (NULL == token) {
            ALOGE("nativeGetWindowContentFrameStats() Invalid jtoken!\n");
            env->ExceptionDescribe();
        }
    }

    IIWindowManager* wm = (IIWindowManager*)jproxy;
    AutoPtr<IWindowContentFrameStats> result;
    ECode ec = wm->GetWindowContentFrameStats(token, (IWindowContentFrameStats**)&result);
    if (FAILED(ec))
        ALOGE("nativeGetWindowContentFrameStats() ec: 0x%08x", ec);

    // ALOGD("- android_view_ElWindowManagerProxy_nativeGetWindowContentFrameStats()");
    return ElUtil::GetJavaWindowContentFrameStats(env, result);
}

static void android_view_ElWindowManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_view_ElWindowManagerProxy_nativeDestroy()");

    IIWindowManager* obj = (IIWindowManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_view_ElWindowManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_view_ElWindowManagerProxy_nativeFinalize },
    { "nativeOpenSession",    "(JLandroid/view/IWindowSessionCallback;Lcom/android/internal/view/IInputMethodClient;ILcom/android/internal/view/IInputContext;I)Landroid/view/IWindowSession;",
            (void*) android_view_ElWindowManagerProxy_nativeOpenSession },
    { "nativeHasNavigationBar",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeHasNavigationBar },
    { "nativeHasPermanentMenuKey",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeHasPermanentMenuKey },
    { "nativeNeedsNavigationBar",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeNeedsNavigationBar },
    { "nativeGetAnimationScale",    "(JI)F",
            (void*) android_view_ElWindowManagerProxy_nativeGetAnimationScale },
    { "nativeInKeyguardRestrictedInputMode",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeInKeyguardRestrictedInputMode },
    { "nativeGetRotation",    "(J)I",
            (void*) android_view_ElWindowManagerProxy_nativeGetRotation },
    { "nativeAddAppToken",    "(JILandroid/view/IApplicationToken;IIIZZIIZZ)V",
            (void*) android_view_ElWindowManagerProxy_nativeAddAppToken },
    { "nativeAddWindowToken",    "(JLandroid/os/IBinder;I)V",
            (void*) android_view_ElWindowManagerProxy_nativeAddWindowToken },
    { "nativeClearForcedDisplaySize",    "(JI)V",
            (void*) android_view_ElWindowManagerProxy_nativeClearForcedDisplaySize },
    { "nativeClearForcedDisplayDensity",    "(JI)V",
            (void*) android_view_ElWindowManagerProxy_nativeClearForcedDisplayDensity },
    { "nativeSetOverscan",    "(JIIIII)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetOverscan },
    { "nativeCloseSystemDialogs",    "(JLjava/lang/String;)V",
            (void*) android_view_ElWindowManagerProxy_nativeCloseSystemDialogs },
    { "nativeStartFreezingScreen",    "(JII)V",
            (void*) android_view_ElWindowManagerProxy_nativeStartFreezingScreen },
    { "nativeStopFreezingScreen",    "(J)V",
            (void*) android_view_ElWindowManagerProxy_nativeStopFreezingScreen },
    { "nativeDisableKeyguard",    "(JLandroid/os/IBinder;Ljava/lang/String;)V",
            (void*) android_view_ElWindowManagerProxy_nativeDisableKeyguard },
    { "nativeExecuteAppTransition",    "(J)V",
            (void*) android_view_ElWindowManagerProxy_nativeExecuteAppTransition },
    { "nativeExitKeyguardSecurely",    "(JLandroid/view/IOnKeyguardExitResult;)V",
            (void*) android_view_ElWindowManagerProxy_nativeExitKeyguardSecurely },
    { "nativeFreezeRotation",    "(JI)V",
            (void*) android_view_ElWindowManagerProxy_nativeFreezeRotation },
    { "nativeGetAnimationScales",    "(J)[F",
            (void*) android_view_ElWindowManagerProxy_nativeGetAnimationScales },
    { "nativeGetAppOrientation",    "(JLandroid/view/IApplicationToken;)I",
            (void*) android_view_ElWindowManagerProxy_nativeGetAppOrientation },
    { "nativeGetPendingAppTransition",    "(J)I",
            (void*) android_view_ElWindowManagerProxy_nativeGetPendingAppTransition },
    { "nativeInputMethodClientHasFocus",    "(JLcom/android/internal/view/IInputMethodClient;)Z",
            (void*) android_view_ElWindowManagerProxy_nativeInputMethodClientHasFocus },
    { "nativeIsKeyguardLocked",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeIsKeyguardLocked },
    { "nativeIsKeyguardSecure",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeIsKeyguardSecure },
    { "nativeIsViewServerRunning",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeIsViewServerRunning },
    { "nativeOverridePendingAppTransition",    "(JLjava/lang/String;IILandroid/os/IRemoteCallback;)V",
            (void*) android_view_ElWindowManagerProxy_nativeOverridePendingAppTransition },
    { "nativeOverridePendingAppTransitionScaleUp",    "(JIIII)V",
            (void*) android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionScaleUp },
    { "nativeOverridePendingAppTransitionThumb",    "(JLandroid/graphics/Bitmap;IILandroid/os/IRemoteCallback;Z)V",
            (void*) android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionThumb },
    { "nativeOverridePendingAppTransitionAspectScaledThumb",    "(JLandroid/graphics/Bitmap;IIIILandroid/os/IRemoteCallback;Z)V",
            (void*) android_view_ElWindowManagerProxy_nativeOverridePendingAppTransitionAspectScaledThumb },
    { "nativePauseKeyDispatching",    "(JLandroid/os/IBinder;)V",
            (void*) android_view_ElWindowManagerProxy_nativePauseKeyDispatching },
    { "nativePrepareAppTransition",    "(JIZ)V",
            (void*) android_view_ElWindowManagerProxy_nativePrepareAppTransition },
    { "nativeReenableKeyguard",    "(JLandroid/os/IBinder;)V",
            (void*) android_view_ElWindowManagerProxy_nativeReenableKeyguard },
    { "nativeRemoveAppToken",    "(JLandroid/os/IBinder;)V",
            (void*) android_view_ElWindowManagerProxy_nativeRemoveAppToken },
    { "nativeRemoveWindowToken",    "(JLandroid/os/IBinder;)V",
            (void*) android_view_ElWindowManagerProxy_nativeRemoveWindowToken },
    { "nativeResumeKeyDispatching",    "(JLandroid/os/IBinder;)V",
            (void*) android_view_ElWindowManagerProxy_nativeResumeKeyDispatching },
    { "nativeIsRotationFrozen",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeIsRotationFrozen },
    { "nativeScreenshotApplications",    "(JLandroid/os/IBinder;IIIZ)Landroid/graphics/Bitmap;",
            (void*) android_view_ElWindowManagerProxy_nativeScreenshotApplications },
    { "nativeSetAnimationScale",    "(JIF)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetAnimationScale },
    { "nativeSetAnimationScales",    "(J[F)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetAnimationScales },
    { "nativeGetCurrentAnimatorScale",    "(J)F",
            (void*) android_view_ElWindowManagerProxy_nativeGetCurrentAnimatorScale },
    { "nativeSetAppGroupId",    "(JLandroid/os/IBinder;I)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetAppGroupId },
    { "nativeSetAppOrientation",    "(JLandroid/view/IApplicationToken;I)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetAppOrientation },
    { "nativeSetAppStartingWindow",    "(JLandroid/os/IBinder;Ljava/lang/String;ILandroid/content/res/CompatibilityInfo;Ljava/lang/CharSequence;IIIILandroid/os/IBinder;Z)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetAppStartingWindow },
    { "nativeSetAppVisibility",    "(JLandroid/os/IBinder;Z)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetAppVisibility },
    { "nativeSetAppWillBeHidden",    "(JLandroid/os/IBinder;)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetAppWillBeHidden },
    { "nativeSetEventDispatching",    "(JZ)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetEventDispatching },
    { "nativeSetFocusedApp",    "(JLandroid/os/IBinder;Z)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetFocusedApp },
    { "nativeGetInitialDisplaySize",    "(JILandroid/graphics/Point;)V",
            (void*) android_view_ElWindowManagerProxy_nativeGetInitialDisplaySize },
    { "nativeGetBaseDisplaySize",    "(JILandroid/graphics/Point;)V",
            (void*) android_view_ElWindowManagerProxy_nativeGetBaseDisplaySize },
    { "nativeGetInitialDisplayDensity",    "(JI)I",
            (void*) android_view_ElWindowManagerProxy_nativeGetInitialDisplayDensity },
    { "nativeGetBaseDisplayDensity",    "(JI)I",
            (void*) android_view_ElWindowManagerProxy_nativeGetBaseDisplayDensity },
    { "nativeSetForcedDisplaySize",    "(JIII)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetForcedDisplaySize },
    { "nativeSetForcedDisplayDensity",    "(JII)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetForcedDisplayDensity },
    { "nativeSetInTouchMode",    "(JZ)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetInTouchMode },
    { "nativeSetNewConfiguration",    "(JLandroid/content/res/Configuration;)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetInTouchMode },
    { "nativeUpdateRotation",    "(JZZ)V",
            (void*) android_view_ElWindowManagerProxy_nativeUpdateRotation },
    { "nativeSetStrictModeVisualIndicatorPreference",    "(JLjava/lang/String;)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetStrictModeVisualIndicatorPreference },
    { "nativeSetScreenCaptureDisabled",    "(JIZ)V",
            (void*) android_view_ElWindowManagerProxy_nativeSetScreenCaptureDisabled },
    { "nativeShowStrictModeViolation",    "(JZ)V",
            (void*) android_view_ElWindowManagerProxy_nativeShowStrictModeViolation },
    { "nativeStartAppFreezingScreen",    "(JLandroid/os/IBinder;I)V",
            (void*) android_view_ElWindowManagerProxy_nativeStartAppFreezingScreen },
    { "nativeStartViewServer",    "(JI)Z",
            (void*) android_view_ElWindowManagerProxy_nativeStartViewServer },
    { "nativeGetLastWallpaperX",    "(J)I",
            (void*) android_view_ElWindowManagerProxy_nativeGetLastWallpaperX },
    { "nativeGetLastWallpaperY",    "(J)I",
            (void*) android_view_ElWindowManagerProxy_nativeGetLastWallpaperY },
    { "nativeStatusBarVisibilityChanged",    "(JI)V",
            (void*) android_view_ElWindowManagerProxy_nativeStatusBarVisibilityChanged },
    { "nativeStopAppFreezingScreen",    "(JLandroid/os/IBinder;Z)V",
            (void*) android_view_ElWindowManagerProxy_nativeStopAppFreezingScreen },
    { "nativeStopViewServer",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeStopViewServer },
    { "nativeThawRotation",    "(J)V",
            (void*) android_view_ElWindowManagerProxy_nativeThawRotation },
    { "nativeUpdateOrientationFromAppTokens",    "(JLandroid/content/res/Configuration;Landroid/os/IBinder;)Landroid/content/res/Configuration;",
            (void*) android_view_ElWindowManagerProxy_nativeUpdateOrientationFromAppTokens },
    { "nativeWatchRotation",    "(JLandroid/view/IRotationWatcher;)I",
            (void*) android_view_ElWindowManagerProxy_nativeWatchRotation },
    { "nativeRemoveRotationWatcher",    "(JLandroid/view/IRotationWatcher;)V",
            (void*) android_view_ElWindowManagerProxy_nativeRemoveRotationWatcher },
    { "nativeGetPreferredOptionsPanelGravity",    "(J)I",
            (void*) android_view_ElWindowManagerProxy_nativeGetPreferredOptionsPanelGravity },
    { "nativeDismissKeyguard",    "(J)V",
            (void*) android_view_ElWindowManagerProxy_nativeDismissKeyguard },
    { "nativeKeyguardGoingAway",    "(JZZZ)V",
            (void*) android_view_ElWindowManagerProxy_nativeKeyguardGoingAway },
    { "nativeLockNow",    "(JLandroid/os/Bundle;)V",
            (void*) android_view_ElWindowManagerProxy_nativeLockNow },
    { "nativeIsSafeModeEnabled",    "(J)Z",
            (void*) android_view_ElWindowManagerProxy_nativeIsSafeModeEnabled },
    { "nativeEnableScreenIfNeeded",    "(J)V",
            (void*) android_view_ElWindowManagerProxy_nativeEnableScreenIfNeeded },
    { "nativeClearWindowContentFrameStats",    "(JLandroid/os/IBinder;)Z",
            (void*) android_view_ElWindowManagerProxy_nativeClearWindowContentFrameStats },
    { "nativeGetWindowContentFrameStats",    "(JLandroid/os/IBinder;)Landroid/view/WindowContentFrameStats;",
            (void*) android_view_ElWindowManagerProxy_nativeGetWindowContentFrameStats },
};

int register_android_view_ElWindowManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/view/ElWindowManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

