
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.View.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Core.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::JavaProxy::CIInputContextCallbackNative;

using Elastos::Droid::Internal::View::IIInputContext;
using Elastos::Droid::Internal::View::IIInputContextCallback;
using Elastos::Droid::View::InputMethod::IExtractedTextRequest;

static void com_android_internal_view_ElIInputContextProxy_nativeReportFullscreenMode(JNIEnv* env, jobject clazz, jlong jproxy,
        jboolean jenabled)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeReportFullscreenMode()");
    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->ReportFullscreenMode((Boolean)jenabled);
    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeReportFullscreenMode()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeGetTextBeforeCursor(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jlength, jint jflags, jint jseq, jobject jcallback)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeGetTextBeforeCursor()");

    AutoPtr<IIInputContextCallback> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);

        if(NOERROR != CIInputContextCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIInputContextCallback**)&callback)) {
            ALOGE("nativeGetTextBeforeCursor new CIInputContextCallbackNative fail!\n");
        }
    }
    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->GetTextBeforeCursor((Int32)jlength, (Int32)jflags, (Int32)jseq, callback);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeGetTextBeforeCursor()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeFinishComposingText(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeFinishComposingText()");

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->FinishComposingText();

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeFinishComposingText()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeCommitText(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtext, jint jnewCursorPosition)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeCommitText()");

    AutoPtr<ICharSequence> text;
    if(jtext != NULL) {
        if(!ElUtil::ToElCharSequence(env, jtext, (ICharSequence**)&text)) {
            ALOGE("nativeCommitText() ToElCharSequence fail!");
        }
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->CommitText(text, (Int32)jnewCursorPosition);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeCommitText()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeSendKeyEvent(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jevent)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeSendKeyEvent()");

    AutoPtr<IKeyEvent> event;
    if(jevent != NULL) {
        if(!ElUtil::ToElKeyEvent(env, jevent, (IKeyEvent**)&event)) {
            ALOGE("nativeCommitText() ToElKeyEvent fail!");
        }
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->SendKeyEvent(event);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeSendKeyEvent()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeClearMetaKeyStates(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstates)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeClearMetaKeyStates()");

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->ClearMetaKeyStates((Int32)jstates);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeClearMetaKeyStates()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeBeginBatchEdit(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeBeginBatchEdit()");

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->BeginBatchEdit();

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeBeginBatchEdit()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeEndBatchEdit(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeEndBatchEdit()");

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->EndBatchEdit();

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeEndBatchEdit()");
}

static void com_android_internal_view_ElIInputContextProxy_nativePerformEditorAction(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jactionCode)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativePerformEditorAction()");

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->PerformEditorAction((Int32)jactionCode);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativePerformEditorAction()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeSetComposingText(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jtext, jint jnewCursorPosition)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeSetComposingText()");

    AutoPtr<ICharSequence> text;
    if(jtext != NULL) {
        if(!ElUtil::ToElCharSequence(env, jtext, (ICharSequence**)&text)) {
            ALOGE("nativeSetComposingText() ToElCharSequence fail!");
        }
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->SetComposingText(text, (Int32)jnewCursorPosition);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeSetComposingText()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeGetExtractedText(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jrequest, jint jflags, jint jseq, jobject jcallback)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeGetExtractedText()");

    AutoPtr<IExtractedTextRequest> request;
    if (jrequest != NULL) {
        if (!ElUtil::ToElExtractedTextRequest(env, jrequest, (IExtractedTextRequest**)&request)) {
            ALOGE("nativeGetExtractedText ToElExtractedText fail!");
        }
    }

    AutoPtr<IIInputContextCallback> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);

        if(NOERROR != CIInputContextCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIInputContextCallback**)&callback)) {
            ALOGE("nativeGetExtractedText new CIInputContextCallbackNative fail!\n");
        }
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->GetExtractedText(request, (Int32)jflags, (Int32)jseq, callback);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeGetExtractedText()");
}

static void com_android_internal_view_ElIInputContextProxy_nativePerformContextMenuAction(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jid)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativePerformContextMenuAction()");

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->PerformContextMenuAction((Int32)jid);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativePerformContextMenuAction()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeGetTextAfterCursor(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jlength, jint jflags, jint jseq, jobject jcallback)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeGetTextAfterCursor()");

    AutoPtr<IIInputContextCallback> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);

        if(NOERROR != CIInputContextCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIInputContextCallback**)&callback)) {
            ALOGE("nativeGetTextAfterCursor new CIInputContextCallbackNative fail!\n");
        }
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->GetTextAfterCursor((Int32)jlength, (Int32)jflags, (Int32)jseq, callback);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeGetTextAfterCursor()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeGetCursorCapsMode(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jreqModes, jint jseq, jobject jcallback)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeGetCursorCapsMode()");

    AutoPtr<IIInputContextCallback> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);

        if(NOERROR != CIInputContextCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIInputContextCallback**)&callback)) {
            ALOGE("nativeGetCursorCapsMode new CIInputContextCallbackNative fail!\n");
        }
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->GetCursorCapsMode((Int32)jreqModes, (Int32)jseq, callback);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeGetCursorCapsMode()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeDeleteSurroundingText(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jleftLength, jint jrightLength)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeDeleteSurroundingText()");

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->DeleteSurroundingText((Int32)jleftLength, (Int32)jrightLength);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeDeleteSurroundingText()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeCommitCompletion(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcompletion)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeCommitCompletion()");

    AutoPtr<ICompletionInfo> completion;
    if(!ElUtil::ToElCompletionInfo(env, jcompletion, (ICompletionInfo**)&completion)) {
        ALOGE("nativeCommitCompletion() ToElCompletionInfo fail!");
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->CommitCompletion(completion);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeCommitCompletion()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeCommitCorrection(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcorrection)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeCommitCorrection()");

    AutoPtr<ICorrectionInfo> correction;
    if(!ElUtil::ToElCorrectionInfo(env, jcorrection, (ICorrectionInfo**)&correction)) {
        ALOGE("nativeCommitCorrection() ToElCorrectionInfo fail!");
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->CommitCorrection(correction);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeCommitCorrection()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeSetSelection(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstart, jint jend)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeSetSelection()");

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->SetSelection((Int32)jstart, (Int32)jend);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeSetSelection()");
}

static void com_android_internal_view_ElIInputContextProxy_nativePerformPrivateCommand(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jaction, jobject jdata)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativePerformPrivateCommand()");

    String action = ElUtil::ToElString(env, jaction);

    AutoPtr<IBundle> data;
    if (jdata != NULL) {
        if (!ElUtil::ToElBundle(env, jdata, (IBundle**)&data)) {
            ALOGE("nativePerformPrivateCommand() ToElBundle fail!");
        }
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->PerformPrivateCommand(action, data);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativePerformPrivateCommand()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeSetComposingRegion(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jstart, jint jend)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeSetComposingRegion()");

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->SetComposingRegion((Int32)jstart, (Int32)jend);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeSetComposingRegion()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeGetSelectedText(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jflags, jint jseq, jobject jcallback)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeGetSelectedText()");

    AutoPtr<IIInputContextCallback> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);

        if(NOERROR != CIInputContextCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIInputContextCallback**)&callback)) {
            ALOGE("nativeGetSelectedText new CIInputContextCallbackNative fail!\n");
        }
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->GetSelectedText((Int32)jflags, (Int32)jseq, callback);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeGetSelectedText()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeRequestUpdateCursorAnchorInfo(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jcursorUpdateMode, jint jseq, jobject jcallback)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeRequestUpdateCursorAnchorInfo()");

    AutoPtr<IIInputContextCallback> callback;
    if (jcallback != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcallback);

        if(NOERROR != CIInputContextCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIInputContextCallback**)&callback)) {
            ALOGE("nativeRequestUpdateCursorAnchorInfo new CIInputContextCallbackNative fail!\n");
        }
    }

    IIInputContext* iic = (IIInputContext*)jproxy;
    iic->RequestUpdateCursorAnchorInfo((Int32)jcursorUpdateMode, (Int32)jseq, callback);

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeRequestUpdateCursorAnchorInfo()");
}

static void com_android_internal_view_ElIInputContextProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jint token)
{
    // ALOGD("+ com_android_internal_view_ElIInputContextProxy_nativeFinalize()");

    IIInputContext* osb = (IIInputContext*)token;
    if (osb != NULL) {
        osb->Release();
    }

    // ALOGD("- com_android_internal_view_ElIInputContextProxy_nativeFinalize()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeFinalize },
    { "nativeReportFullscreenMode",    "(JZ)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeReportFullscreenMode },
    { "nativeGetTextBeforeCursor",    "(JIIILcom/android/internal/view/IInputContextCallback;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeGetTextBeforeCursor },
    { "nativeFinishComposingText",    "(J)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeFinishComposingText },
    { "nativeCommitText",    "(JLjava/lang/CharSequence;I)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeCommitText },
    { "nativeSendKeyEvent",    "(JLandroid/view/KeyEvent;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeSendKeyEvent },
    { "nativeClearMetaKeyStates",    "(JI)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeClearMetaKeyStates },
    { "nativeBeginBatchEdit",    "(J)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeBeginBatchEdit },
    { "nativeEndBatchEdit",    "(J)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeEndBatchEdit },
    { "nativePerformEditorAction",    "(JI)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativePerformEditorAction },
    { "nativeSetComposingText",    "(JLjava/lang/CharSequence;I)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeSetComposingText },
    { "nativeGetExtractedText",    "(JLandroid/view/inputmethod/ExtractedTextRequest;IILcom/android/internal/view/IInputContextCallback;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeGetExtractedText },
    { "nativePerformContextMenuAction",    "(JI)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativePerformContextMenuAction },
    { "nativeGetTextAfterCursor",    "(JIIILcom/android/internal/view/IInputContextCallback;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeGetTextAfterCursor },
    { "nativeGetCursorCapsMode",    "(JIILcom/android/internal/view/IInputContextCallback;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeGetCursorCapsMode },
    { "nativeDeleteSurroundingText",    "(JII)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeDeleteSurroundingText },
    { "nativeCommitCompletion",    "(JLandroid/view/inputmethod/CompletionInfo;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeCommitCompletion },
    { "nativeCommitCorrection",    "(JLandroid/view/inputmethod/CorrectionInfo;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeCommitCorrection },
    { "nativeSetSelection",    "(JII)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeSetSelection },
    { "nativePerformPrivateCommand",    "(JLjava/lang/String;Landroid/os/Bundle;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativePerformPrivateCommand },
    { "nativeSetComposingRegion",    "(JII)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeSetComposingRegion },
    { "nativeGetSelectedText",    "(JIILcom/android/internal/view/IInputContextCallback;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeGetSelectedText },
    { "nativeRequestUpdateCursorAnchorInfo",    "(JIILcom/android/internal/view/IInputContextCallback;)V",
            (void*) com_android_internal_view_ElIInputContextProxy_nativeRequestUpdateCursorAnchorInfo },
};

int register_com_android_internal_view_ElIInputContextProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/internal/view/ElIInputContextProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

