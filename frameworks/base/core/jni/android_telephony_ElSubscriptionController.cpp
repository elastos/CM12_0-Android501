
#define LOG_TAG "ElSubscriptionControllerJNI"
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Telephony.h>
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Utility.h>
#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::Internal::Telephony::IISub;
using Elastos::Droid::Telephony::ISubInfoRecord;

static jobject GetSubInfoRecord(JNIEnv* env, ISubInfoRecord* sr)
{
    Int64 lv;
    sr->GetSubId(&lv);
    jlong subId = lv;
    String s;
    sr->GetIccId(&s);
    jstring iccId = ElUtil::GetJavaString(env, s);

    Int32 v;
    sr->GetSlotId(&v);
    jint slotId = v;
    sr->GetDisplayName(&s);
    jstring displayName = ElUtil::GetJavaString(env, s);
    sr->GetNameSource(&v);
    jint nameSource = v;
    sr->GetColor(&v);
    jint color = v;
    sr->GetNumber(&s);
    jstring number = ElUtil::GetJavaString(env, s);
    sr->GetDisplayNumberFormat(&v);
    jint displayNumberFormat = v;
    sr->GetDataRoaming(&v);
    jint dataRoaming = v;
    AutoPtr<ArrayOf<Int32> > res;
    sr->GetSimIconRes((ArrayOf<Int32>**)&res);
    jintArray simIconRes = ElUtil::GetJavaIntArray(env, res);
    sr->GetMcc(&v);
    jint mcc = v;
    sr->GetMnc(&v);
    jint mnc = v;
    sr->GetStatus(&v);
    jint status = v;
    sr->GetNwMode(&v);
    jint nwMode = v;

    jclass sKlass = env->FindClass("android/telephony/SubInfoRecord");
    ElUtil::CheckErrorAndLog(env, "GetSubInfoRecord Error FindClass: SubInfoRecord : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(sKlass, "<init>", "(JLjava/lang/String;ILjava/lang/String;IILjava/lang/String;II[IIIII)V");
    ElUtil::CheckErrorAndLog(env, "GetSubInfoRecord GetMethodID: SubInfoRecord : %d!\n", __LINE__);

    jobject jsubInfoRecord = env->NewObject(sKlass, m, subId, iccId, slotId, displayName, nameSource
            , color, number, displayNumberFormat, dataRoaming, simIconRes, mcc, mnc, status, nwMode);
    ElUtil::CheckErrorAndLog(env, "GetSubInfoRecord NewObject: SubInfoRecord : %d!\n", __LINE__);

    env->DeleteLocalRef(sKlass);
    return jsubInfoRecord;
}

static jobject GetSubInfoRecordJavaList(JNIEnv* env, IList* list)
{
    if (list == NULL) {
        return NULL;
    }

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "GetSubInfoRecordJavaList FindClass: ArrayList : %d!", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "GetSubInfoRecordJavaList Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jlist = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "GetSubInfoRecordJavaList Fail NewObject: ArrayList : %d!\n", __LINE__);

    jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
    ElUtil::CheckErrorAndLog(env, "GetSubInfoRecordJavaList Fail GetMethodID: add : %d!\n", __LINE__);
    env->DeleteLocalRef(listKlass);

    Int32 size;
    list->GetSize(&size);
    for (Int32 i = 0; i < size; i++) {
        AutoPtr<IInterface> obj;
        list->Get(i, (IInterface**)&obj);

        jobject info = GetSubInfoRecord(env, ISubInfoRecord::Probe(obj));

        env->CallBooleanMethod(jlist, mAdd, info);
        ElUtil::CheckErrorAndLog(env, "GetSubInfoRecordJavaList Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
        env->DeleteLocalRef(info);
    }

    ALOGD("- GetSubInfoRecordJavaList()");
    return jlist;
}

static jobject android_telephony_ElSubscriptionController_nativeGetSubInfoForSubscriber(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetSubInfoForSubscriber()");
    IISub* sub = (IISub*)jproxy;
    AutoPtr<ISubInfoRecord> sr;
    sub->GetSubInfoForSubscriber((Int64)subId, (ISubInfoRecord**)&sr);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetSubInfoForSubscriber()");
    return GetSubInfoRecord(env, sr);
}

static jobject android_telephony_ElSubscriptionController_nativeGetSubInfoUsingIccId(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring iccId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetSubInfoUsingIccId()");
    IISub* sub = (IISub*)jproxy;
    String str = ElUtil::ToElString(env, iccId);
    AutoPtr<IList> list;
    sub->GetSubInfoUsingIccId(str, (IList**)&list);
    jobject jlist = GetSubInfoRecordJavaList(env, list);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetSubInfoUsingIccId()");
    return jlist;
}

static jobject android_telephony_ElSubscriptionController_nativeGetSubInfoUsingSlotId(
    JNIEnv* env, jobject clazz, jlong jproxy, jint slotId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetSubInfoUsingSlotId()");
    IISub* sub = (IISub*)jproxy;
    AutoPtr<IList> list;
    sub->GetSubInfoUsingSlotId((Int32)slotId, (IList**)&list);
    jobject jlist = GetSubInfoRecordJavaList(env, list);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetSubInfoUsingSlotId()");
    return jlist;
}

static jobject android_telephony_ElSubscriptionController_nativeGetAllSubInfoList(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetAllSubInfoList()");
    IISub* sub = (IISub*)jproxy;
    AutoPtr<IList> list;
    sub->GetAllSubInfoList((IList**)&list);
    jobject jlist = GetSubInfoRecordJavaList(env, list);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetAllSubInfoList()");
    return jlist;
}

static jobject android_telephony_ElSubscriptionController_nativeGetActiveSubInfoList(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetActiveSubInfoList()");
    IISub* sub = (IISub*)jproxy;
    AutoPtr<IList> list;
    sub->GetActiveSubInfoList((IList**)&list);
    jobject jlist = GetSubInfoRecordJavaList(env, list);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetActiveSubInfoList()");
    return jlist;
}

static jint android_telephony_ElSubscriptionController_nativeGetAllSubInfoCount(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetAllSubInfoCount()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->GetAllSubInfoCount(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetAllSubInfoCount()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeGetActiveSubInfoCount(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetActiveSubInfoCount()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->GetActiveSubInfoCount(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetActiveSubInfoCount()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeAddSubInfoRecord(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring iccId, jint slotId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeAddSubInfoRecord()");
    IISub* sub = (IISub*)jproxy;
    String s = ElUtil::ToElString(env, iccId);
    Int32 v;
    sub->AddSubInfoRecord(s, (Int32)slotId, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeAddSubInfoRecord()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeSetColor(
    JNIEnv* env, jobject clazz, jlong jproxy, jint color, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetColor()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->SetColor((Int32)color, (Int64)subId, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeSetColor()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeSetDisplayName(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring displayName, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetDisplayName()");
    IISub* sub = (IISub*)jproxy;
    String s = ElUtil::ToElString(env, displayName);
    Int32 v;
    sub->SetDisplayName(s, (Int64)subId, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeSetDisplayName()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeSetDisplayNameUsingSrc(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring displayName, jlong subId, jlong nameSource)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetDisplayNameUsingSrc()");
    IISub* sub = (IISub*)jproxy;
    String s = ElUtil::ToElString(env, displayName);
    Int32 v;
    sub->SetDisplayNameUsingSrc(s, (Int64)subId, (Int64)nameSource, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeSetDisplayNameUsingSrc()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeSetDisplayNumber(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring number, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetDisplayNumber()");
    IISub* sub = (IISub*)jproxy;
    String s = ElUtil::ToElString(env, number);
    Int32 v;
    sub->SetDisplayNumber(s, (Int64)subId, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeSetDisplayNumber()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeSetDisplayNumberFormat(
    JNIEnv* env, jobject clazz, jlong jproxy, jint format, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetDisplayNumberFormat()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->SetDisplayNumberFormat((Int32)format, (Int64)subId, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeSetDisplayNumberFormat()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeSetDataRoaming(
    JNIEnv* env, jobject clazz, jlong jproxy, jint roaming, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetDataRoaming()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->SetDataRoaming((Int32)roaming, (Int64)subId, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeSetDataRoaming()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeGetSlotId(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetSlotId()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->GetSlotId((Int64)subId, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetSlotId()");
    return (jint)v;
}

static jlongArray android_telephony_ElSubscriptionController_nativeGetSubId(
    JNIEnv* env, jobject clazz, jlong jproxy, jint slotId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetSubId()");
    IISub* sub = (IISub*)jproxy;
    AutoPtr<ArrayOf<Int64> > array;
    sub->GetSubId((Int32)slotId, (ArrayOf<Int64>**)&array);
    jlongArray ids = ElUtil::GetJavaLongArray(env, array);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetSubId()");
    return ids;
}

static jlong android_telephony_ElSubscriptionController_nativeGetDefaultSubId(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetDefaultSubId()");
    IISub* sub = (IISub*)jproxy;
    Int64 v;
    sub->GetDefaultSubId(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetDefaultSubId()");
    return (jlong)v;
}

static jint android_telephony_ElSubscriptionController_nativeClearSubInfo(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeClearSubInfo()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->ClearSubInfo(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeClearSubInfo()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeGetPhoneId(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetPhoneId()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->GetPhoneId((Int64)subId, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetPhoneId()");
    return (jint)v;
}

static jlong android_telephony_ElSubscriptionController_nativeGetDefaultDataSubId(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetDefaultDataSubId()");
    IISub* sub = (IISub*)jproxy;
    Int64 v;
    sub->GetDefaultDataSubId(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetDefaultDataSubId()");
    return (jlong)v;
}

static void android_telephony_ElSubscriptionController_nativeSetDefaultDataSubId(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetDefaultDataSubId()");
    IISub* sub = (IISub*)jproxy;
    sub->SetDefaultDataSubId((Int64)subId);
    ALOGD("- android_telephony_ElSubscriptionController_nativeSetDefaultDataSubId()");
}

static jlong android_telephony_ElSubscriptionController_nativeGetDefaultVoiceSubId(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetDefaultVoiceSubId()");
    IISub* sub = (IISub*)jproxy;
    Int64 v;
    sub->GetDefaultVoiceSubId(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetDefaultVoiceSubId()");
    return (jlong)v;
}

static void android_telephony_ElSubscriptionController_nativeSetDefaultVoiceSubId(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetDefaultVoiceSubId()");
    IISub* sub = (IISub*)jproxy;
    sub->SetDefaultVoiceSubId((Int64)subId);
    ALOGD("- android_telephony_ElSubscriptionController_nativeSetDefaultVoiceSubId()");
}

static jlong android_telephony_ElSubscriptionController_nativeGetDefaultSmsSubId(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetDefaultSmsSubId()");
    IISub* sub = (IISub*)jproxy;
    Int64 v;
    sub->GetDefaultSmsSubId(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetDefaultSmsSubId()");
    return (jlong)v;
}

static void android_telephony_ElSubscriptionController_nativeSetDefaultSmsSubId(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetDefaultSmsSubId()");
    IISub* sub = (IISub*)jproxy;
    sub->SetDefaultSmsSubId((Int64)subId);
    ALOGD("- android_telephony_ElSubscriptionController_nativeSetDefaultSmsSubId()");
}

static void android_telephony_ElSubscriptionController_nativeClearDefaultsForInactiveSubIds(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeClearDefaultsForInactiveSubIds()");
    IISub* sub = (IISub*)jproxy;
    sub->ClearDefaultsForInactiveSubIds();
    ALOGD("- android_telephony_ElSubscriptionController_nativeClearDefaultsForInactiveSubIds()");
}

static jlongArray android_telephony_ElSubscriptionController_nativeGetActiveSubIdList(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetActiveSubIdList()");
    IISub* sub = (IISub*)jproxy;
    AutoPtr<ArrayOf<Int64> > array;
    sub->GetActiveSubIdList((ArrayOf<Int64>**)&array);
    jlongArray ids = ElUtil::GetJavaLongArray(env, array);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetActiveSubIdList()");
    return ids;
}

static jboolean android_telephony_ElSubscriptionController_nativeIsSMSPromptEnabled(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeIsSMSPromptEnabled()");
    IISub* sub = (IISub*)jproxy;
    Boolean v;
    sub->IsSMSPromptEnabled(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeIsSMSPromptEnabled()");
    return (jboolean)v;
}

static void android_telephony_ElSubscriptionController_nativeSetSMSPromptEnabled(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean enabled)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetSMSPromptEnabled()");
    IISub* sub = (IISub*)jproxy;
    sub->SetSMSPromptEnabled(enabled);

    ALOGD("- android_telephony_ElSubscriptionController_nativeSetSMSPromptEnabled()");
}

static jboolean android_telephony_ElSubscriptionController_nativeIsVoicePromptEnabled(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeIsVoicePromptEnabled()");
    IISub* sub = (IISub*)jproxy;
    Boolean v;
    sub->IsVoicePromptEnabled(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeIsVoicePromptEnabled()");
    return (jboolean)v;
}

static void android_telephony_ElSubscriptionController_nativeSetVoicePromptEnabled(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean enabled)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetVoicePromptEnabled()");
    IISub* sub = (IISub*)jproxy;
    sub->SetVoicePromptEnabled(enabled);

    ALOGD("- android_telephony_ElSubscriptionController_nativeSetVoicePromptEnabled()");
}

static void android_telephony_ElSubscriptionController_nativeActivateSubId(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeActivateSubId()");
    IISub* sub = (IISub*)jproxy;
    sub->ActivateSubId((Int64)subId);

    ALOGD("- android_telephony_ElSubscriptionController_nativeActivateSubId()");
}

static void android_telephony_ElSubscriptionController_nativeDeactivateSubId(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeDeactivateSubId()");
    IISub* sub = (IISub*)jproxy;
    sub->DeactivateSubId((Int64)subId);

    ALOGD("- android_telephony_ElSubscriptionController_nativeDeactivateSubId()");
}

static jint android_telephony_ElSubscriptionController_nativeSetSubState(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId, jint subStatus)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeSetSubState()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->SetSubState((Int64)subId, (Int32)subStatus, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeSetSubState()");
    return (jint)v;
}

static jint android_telephony_ElSubscriptionController_nativeGetSubState(
    JNIEnv* env, jobject clazz, jlong jproxy, jlong subId)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetSubState()");
    IISub* sub = (IISub*)jproxy;
    Int32 v;
    sub->GetSubState((Int64)subId, &v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetSubState()");
    return (jint)v;
}

static jlong android_telephony_ElSubscriptionController_nativeGetOnDemandDataSubId(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_telephony_ElSubscriptionController_nativeGetOnDemandDataSubId()");
    IISub* sub = (IISub*)jproxy;
    Int64 v;
    sub->GetOnDemandDataSubId(&v);

    ALOGD("- android_telephony_ElSubscriptionController_nativeGetOnDemandDataSubId()");
    return (jlong)v;
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeGetSubInfoForSubscriber",      "(JJ)Landroid/telephony/SubInfoRecord;",
            (void*) android_telephony_ElSubscriptionController_nativeGetSubInfoForSubscriber },
    { "nativeGetSubInfoUsingIccId",      "(JLjava/lang/String;)Ljava/util/List;",
            (void*) android_telephony_ElSubscriptionController_nativeGetSubInfoUsingIccId },
    { "nativeGetSubInfoUsingSlotId",      "(JI)Ljava/util/List;",
            (void*) android_telephony_ElSubscriptionController_nativeGetSubInfoUsingSlotId },
    { "nativeGetAllSubInfoList",      "(J)Ljava/util/List;",
            (void*) android_telephony_ElSubscriptionController_nativeGetAllSubInfoList },
    { "nativeGetActiveSubInfoList",      "(J)Ljava/util/List;",
            (void*) android_telephony_ElSubscriptionController_nativeGetActiveSubInfoList },
    { "nativeGetAllSubInfoCount",      "(J)I",
            (void*) android_telephony_ElSubscriptionController_nativeGetAllSubInfoCount },
    { "nativeGetActiveSubInfoCount",      "(J)I",
            (void*) android_telephony_ElSubscriptionController_nativeGetActiveSubInfoCount },
    { "nativeAddSubInfoRecord",      "(JLjava/lang/String;I)I",
            (void*) android_telephony_ElSubscriptionController_nativeAddSubInfoRecord },
    { "nativeSetColor",      "(JIJ)I",
            (void*) android_telephony_ElSubscriptionController_nativeSetColor },
    { "nativeSetDisplayName",      "(JLjava/lang/String;J)I",
            (void*) android_telephony_ElSubscriptionController_nativeSetDisplayName },
    { "nativeSetDisplayNameUsingSrc",      "(JLjava/lang/String;JJ)I",
            (void*) android_telephony_ElSubscriptionController_nativeSetDisplayNameUsingSrc },
    { "nativeSetDisplayNumber",      "(JLjava/lang/String;J)I",
            (void*) android_telephony_ElSubscriptionController_nativeSetDisplayNumber },
    { "nativeSetDisplayNumberFormat",      "(JIJ)I",
            (void*) android_telephony_ElSubscriptionController_nativeSetDisplayNumberFormat },
    { "nativeSetDataRoaming",      "(JIJ)I",
            (void*) android_telephony_ElSubscriptionController_nativeSetDataRoaming },
    { "nativeGetSlotId",      "(JJ)I",
            (void*) android_telephony_ElSubscriptionController_nativeGetSlotId },
    { "nativeGetSubId",      "(JI)[J",
            (void*) android_telephony_ElSubscriptionController_nativeGetSubId },
    { "nativeGetDefaultSubId",      "(J)J",
            (void*) android_telephony_ElSubscriptionController_nativeGetDefaultSubId },
    { "nativeClearSubInfo",      "(J)I",
            (void*) android_telephony_ElSubscriptionController_nativeClearSubInfo },
    { "nativeGetPhoneId",      "(JJ)I",
            (void*) android_telephony_ElSubscriptionController_nativeGetPhoneId },
    { "nativeGetDefaultDataSubId",      "(J)J",
            (void*) android_telephony_ElSubscriptionController_nativeGetDefaultDataSubId },
    { "nativeSetDefaultDataSubId",      "(JJ)V",
            (void*) android_telephony_ElSubscriptionController_nativeSetDefaultDataSubId },
    { "nativeGetDefaultVoiceSubId",      "(J)J",
            (void*) android_telephony_ElSubscriptionController_nativeGetDefaultVoiceSubId },
    { "nativeSetDefaultVoiceSubId",      "(JJ)V",
            (void*) android_telephony_ElSubscriptionController_nativeSetDefaultVoiceSubId },
    { "nativeGetDefaultSmsSubId",      "(J)J",
            (void*) android_telephony_ElSubscriptionController_nativeGetDefaultSmsSubId },
    { "nativeSetDefaultSmsSubId",      "(JJ)V",
            (void*) android_telephony_ElSubscriptionController_nativeSetDefaultSmsSubId },
    { "nativeClearDefaultsForInactiveSubIds",      "(J)V",
            (void*) android_telephony_ElSubscriptionController_nativeClearDefaultsForInactiveSubIds },
    { "nativeGetActiveSubIdList",      "(J)[J",
            (void*) android_telephony_ElSubscriptionController_nativeGetActiveSubIdList },
    { "nativeIsSMSPromptEnabled",      "(J)Z",
            (void*) android_telephony_ElSubscriptionController_nativeIsSMSPromptEnabled },
    { "nativeSetSMSPromptEnabled",      "(JZ)V",
            (void*) android_telephony_ElSubscriptionController_nativeSetSMSPromptEnabled },
    { "nativeIsVoicePromptEnabled",      "(J)Z",
            (void*) android_telephony_ElSubscriptionController_nativeIsVoicePromptEnabled },
    { "nativeSetVoicePromptEnabled",      "(JZ)V",
            (void*) android_telephony_ElSubscriptionController_nativeSetVoicePromptEnabled },
    { "nativeActivateSubId",      "(JJ)V",
            (void*) android_telephony_ElSubscriptionController_nativeActivateSubId },
    { "nativeDeactivateSubId",      "(JJ)V",
            (void*) android_telephony_ElSubscriptionController_nativeDeactivateSubId },
    { "nativeSetSubState",      "(JJI)I",
            (void*) android_telephony_ElSubscriptionController_nativeSetSubState },
    { "nativeGetSubState",      "(JJ)I",
            (void*) android_telephony_ElSubscriptionController_nativeGetSubState },
    { "nativeGetOnDemandDataSubId",      "(J)J",
            (void*) android_telephony_ElSubscriptionController_nativeGetOnDemandDataSubId },
};

int register_android_telephony_ElSubscriptionController(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/internal/telephony/ElSubscriptionController",
        gMethods, NELEM(gMethods));
}

}; // end namespace android
