
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Database.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Utility.h>
#include <Elastos.CoreLibrary.Utility.h>
#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Content::IContentProviderOperation;
using Elastos::Droid::Content::IContentProviderResult;
using Elastos::Droid::Content::IIContentProvider;
using Elastos::Droid::Content::Res::IAssetFileDescriptor;
using Elastos::Droid::Database::ICursor;
using Elastos::Droid::Os::IBundle;
using Elastos::Droid::Os::IICancellationSignal;
using Elastos::Droid::Utility::CParcelableList;
using Elastos::Utility::IArrayList;

static jobject android_content_ElContentProviderProxy_nativeCall(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jstring jmethod, jstring jarg, jobject jextras)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeCall()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    String method = ElUtil::ToElString(env, jmethod);
    String arg = ElUtil::ToElString(env, jarg);
    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("_nativeCall() ToElBundle fail!");
        }
    }

    IIContentProvider* cp = (IIContentProvider*)jproxy;
    AutoPtr<IBundle> result;
    cp->Call(callingPkg, method, arg, extras, (IBundle**)&result);
    jobject jresult = NULL;
    if (result != NULL) {
        jresult = ElUtil::GetJavaBundle(env, result);
    }

    // ALOGD("- android_content_ElContentProviderProxy_nativeCall()");
    return jresult;
}

static jobject android_content_ElContentProviderProxy_nativeQuery(JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject jurl,
    jobjectArray jprojection, jstring jselection, jobjectArray jselectionArgs, jstring jsortOrder, jobject jcancellationSignal)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeQuery()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);

    AutoPtr<IUri> url;
    if (jurl != NULL) {
        if(!ElUtil::ToElUri(env, jurl, (IUri**)&url)) {
            ALOGE("nativeQuery() ToElUri fail!");
        }
    } else {
        ALOGE("nativeQuery() jurl is NULL");
    }

    AutoPtr<ArrayOf<String> > projection;
    if (jprojection != NULL) {
        if (!ElUtil::ToElStringArray(env, jprojection, (ArrayOf<String>**)&projection)) {
            ALOGE("nativeGetIntentSender() ToElStringArray fail!");
        }
    }

    String selection = ElUtil::ToElString(env, jselection);

    AutoPtr<ArrayOf<String> > selectionArgs;
    if (jselectionArgs != NULL) {
        if (!ElUtil::ToElStringArray(env, jselectionArgs, (ArrayOf<String>**)&selectionArgs)) {
            ALOGE("nativeGetIntentSender() ToElStringArray fail!");
        }
    }

    String sortOrder = ElUtil::ToElString(env, jsortOrder);

    AutoPtr<IICancellationSignal> cancellationSignal;
    if (jcancellationSignal != NULL) {
        ALOGE("nativeQuery() jcancellationSignal not NULL!");
    }

    IIContentProvider* cp = (IIContentProvider*)jproxy;
    AutoPtr<ICursor> cursor;
    ECode ec = cp->Query(callingPkg, url, projection, selection, selectionArgs, sortOrder, cancellationSignal, (ICursor**)&cursor);
    if (ec == E_SQLITE_EXCEPTION) {
        String urlStr;
        IObject::Probe(url)->ToString(&urlStr);
        String sb("nativeQuery failed! url: ");
        sb += urlStr;
        if (projection != NULL) {
            sb += ", projections: {";
            for (Int32 i = 0; i < projection->GetLength(); ++i) {
                if (i != 0) {
                    sb += ", ";
                }
                sb += (*projection)[i];
            }
            sb += "}";
        }

        if (!selection.IsNullOrEmpty()) {
            sb += ", selection: ";
            sb += selection;
        }

        if (selectionArgs != NULL) {
            sb += ", selectionArgs: {";
            for (Int32 i = 0; i < selectionArgs->GetLength(); ++i) {
                if (i != 0) {
                    sb += ", ";
                }
                sb += (*selectionArgs)[i];
            }
            sb += "}";
        }

        jniThrowException(env, "android/database/sqlite/SQLiteException", sb.string());
        return NULL;
    }

    jobject jcursor = NULL;
    if (cursor != NULL) {
        jclass cursorKlass = env->FindClass("android/database/ElCursorProxy");
        ElUtil::CheckErrorAndLog(env, "nativeQuery Fail FindClass: ElCursorProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(cursorKlass, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "nativeQuery Fail GetMethodID: ElCursorProxy : %d!\n", __LINE__);

        jcursor = env->NewObject(cursorKlass, m, (jlong)cursor.Get());
        ElUtil::CheckErrorAndLog(env, "nativeQuery Fail NewObject: ElCursorProxy : %d!\n", __LINE__);
        cursor->AddRef();

        env->DeleteLocalRef(cursorKlass);
    }

    // ALOGD("- android_content_ElContentProviderProxy_nativeQuery()");
    return jcursor;
}

static jobject android_content_ElContentProviderProxy_nativeOpenTypedAssetFile(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject jurl,
    jstring jmimeType, jobject jopts, jobject jsignal)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeOpenTypedAssetFile()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    AutoPtr<IUri> url;
    if (jurl != NULL) {
        if (!ElUtil::ToElUri(env, jurl, (IUri**)&url)) {
            ALOGE("nativeOpenTypedAssetFile() UriToUri fail!");
        }
    }

    String mimeType = ElUtil::ToElString(env, jmimeType);

    AutoPtr<IBundle> opts;
    if (jopts != NULL) {
        if (!ElUtil::ToElBundle(env, jopts, (IBundle**)&opts)) {
            ALOGE("nativeOpenTypedAssetFile() ToElBundle fail!");
        }
    }

    AutoPtr<IICancellationSignal> cancellationSignal;
    if (jsignal != NULL) {
        ALOGE("nativeQuery() jsignal not NULL!");
    }

    IIContentProvider* cp = (IIContentProvider*)jproxy;
    AutoPtr<IAssetFileDescriptor> descriptor;
    ECode ec = cp->OpenTypedAssetFile(callingPkg, url, mimeType, opts, cancellationSignal, (IAssetFileDescriptor**)&descriptor);
    if (ec == E_FILE_NOT_FOUND_EXCEPTION) {
        jniThrowException(env, "java/io/FileNotFoundException", NULL);
        return NULL;
    }

    jobject jdescriptor = NULL;
    if (descriptor != NULL) {
        jdescriptor = ElUtil::GetJavaAssetFileDescriptor(env, descriptor);
    }

    // ALOGD("- android_content_ElContentProviderProxy_nativeOpenTypedAssetFile()");
    return jdescriptor;
}

static jobject android_content_ElContentProviderProxy_nativeInsert(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject jurl, jobject jInitialValues)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeInsert()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    AutoPtr<IUri> url;
    if (jurl != NULL) {
        if (!ElUtil::ToElUri(env, jurl, (IUri**)&url)) {
            ALOGE("nativeInsert() UriToUri fail!");
        }
    }

    AutoPtr<IContentValues> initialValues;
    if (jInitialValues != NULL) {
        if (!ElUtil::ToElContentValues(env, jInitialValues, (IContentValues**)&initialValues)) {
            ALOGE("nativeInsert() ToElContentValues fail!");
        }
    }

    IIContentProvider* cp = (IIContentProvider*)jproxy;
    AutoPtr<IUri> rUri;
    cp->Insert(callingPkg, url, initialValues, (IUri**)&rUri);

    if (rUri != NULL) {
        return ElUtil::GetJavaUri(env, rUri);
    }

    // ALOGD("- android_content_ElContentProviderProxy_nativeInsert()");
    return NULL;
}

static jstring android_content_ElContentProviderProxy_nativeGetType(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jurl)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeGetType()");

    AutoPtr<IUri> url;
    if (jurl != NULL) {
        if (!ElUtil::ToElUri(env, jurl, (IUri**)&url)) {
            ALOGE("nativeGetType() UriToUri fail!");
        }
    }

     IIContentProvider* cp = (IIContentProvider*)jproxy;
     String type;
     cp->GetType(url, &type);
     // ALOGD("+ android_content_ElContentProviderProxy_nativeGetType()");
     return ElUtil::GetJavaString(env, type);
}

static jint android_content_ElContentProviderProxy_nativeBulkInsert(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject juri, jobjectArray jinitialValues)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeBulkInsert()");
     AutoPtr<IUri> uri;
        String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    if (juri != NULL) {
        if(!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("ElContentProviderProxy_nativeBulkInsert() ToElUri fail!");
        }
    } else {
        ALOGE("ElContentProviderProxy_nativeBulkInsert() juri is NULL");
    }

    AutoPtr<ArrayOf<IContentValues *> >initialValues;
    if (jinitialValues != NULL) {
        int size = env->GetArrayLength(jinitialValues);
        initialValues = ArrayOf<IContentValues*>::Alloc(size);

        for(int i = 0; i < size; i++){
            jobject jcontentValues = env->GetObjectArrayElement(jinitialValues, i);
            ElUtil::CheckErrorAndLog(env, "ElContentProviderProxy_nativeBulkInsert(); GetObjectArrayelement failed : %d!\n", __LINE__);
            AutoPtr<IContentValues> contentValues;
            if(!ElUtil::ToElContentValues(env, jcontentValues, (IContentValues**)&contentValues)) {
                ALOGE("ElContentProviderProxy_nativeBulkInsert() ToElContentValues fail!");
            }
            initialValues->Set(i, contentValues);
            env->DeleteLocalRef(jcontentValues);
        }
    }


    IIContentProvider* cp = (IIContentProvider*)jproxy;
    Int32 ret = 0;
    cp->BulkInsert(callingPkg, uri, initialValues, &ret);

    // ALOGD("+ android_content_ElContentProviderProxy_nativeBulkInsert()");
    return ret;
}

static jint android_content_ElContentProviderProxy_nativeDelete(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject juri, jstring jselection, jobjectArray jselectionArgs)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeDelete()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if(!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("ElContentProviderProxy_nativeDelete() ToElUri fail!");
        }
    } else {
        ALOGE("ElContentProviderProxy_nativeDelete() juri is NULL");
    }

    String selection = ElUtil::ToElString(env, jselection);

    AutoPtr<ArrayOf<String> > selectionArgs;
    if (jselectionArgs != NULL) {
        if (!ElUtil::ToElStringArray(env, jselectionArgs, (ArrayOf<String>**)&selectionArgs)) {
            ALOGE("ElContentProviderProxy_nativeDelete() ToElStringArray fail!");
        }
    }

    IIContentProvider* cp = (IIContentProvider*)jproxy;
    Int32 count = 0;
    cp->Delete(callingPkg, uri, selection, selectionArgs, &count);

    // ALOGD("- android_content_ElContentProviderProxy_nativeDelete(), count = %d", count);
    return (jint)count;
}

static jint android_content_ElContentProviderProxy_nativeUpdate(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject juri, jobject jcontentValues, jstring jselection, jobjectArray jselectionArgs)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeUpdate()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if(!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("ElContentProviderProxy_nativeUpdate() ToElUri fail!");
        }
    } else {
        ALOGE("ElContentProviderProxy_nativeUpdate() juri is NULL");
    }

    AutoPtr<IContentValues> contentValues;
    if (jcontentValues != NULL) {
        if (!ElUtil::ToElContentValues(env, jcontentValues, (IContentValues**)&contentValues)) {
            ALOGE("nativeUpdate() ToElContentValues fail!");
        }
    }
    else {
        ALOGE("nativeUpdate jcontentValues is NULL");
    }

    String selection = ElUtil::ToElString(env, jselection);

    AutoPtr<ArrayOf<String> > selectionArgs;
    if (jselectionArgs != NULL) {
        if (!ElUtil::ToElStringArray(env, jselectionArgs, (ArrayOf<String>**)&selectionArgs)) {
            ALOGE("ElContentProviderProxy_nativeUpdate() ToElStringArray fail!");
        }
    }
    else {
        ALOGE("ElContentProviderProxy_nativeUpdate jselectionArgs is NULL");
    }

     IIContentProvider* cp = (IIContentProvider*)jproxy;
     Int32 ret = 0;
     cp->Update(callingPkg, uri, contentValues, selection, selectionArgs, &ret);

    // ALOGD("- android_content_ElContentProviderProxy_nativeUpdate()");
     return ret;
}

static jobject android_content_ElContentProviderProxy_nativeOpenFile(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject juri, jstring jmode, jobject jcancellationSignal)
{
    String callingPkg = ElUtil::ToElString(env, jcallingPkg);

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if(!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("ElContentProviderProxy_nativeOpenFile() ToElUri fail!");
        }
    }
    else {
        ALOGE("ElContentProviderProxy_nativeOpenFile() juri is NULL");
    }

    AutoPtr<IICancellationSignal> cancellationSignal;
    if (jcancellationSignal != NULL) {
        ALOGE("nativeQuery() jcancellationSignal not NULL!");
    }

    String mode = ElUtil::ToElString(env, jmode);
    IIContentProvider* cp = (IIContentProvider*)jproxy;
    AutoPtr<IParcelFileDescriptor> parcelFd;
    cp->OpenFile(callingPkg, uri, mode, cancellationSignal, (IParcelFileDescriptor**)&parcelFd);

    return ElUtil::GetJavaParcelFileDescriptor(env, parcelFd);
}

static jobject android_content_ElContentProviderProxy_nativeOpenAssetFile(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject juri, jstring jmode, jobject jcancellationSignal)
{
    String callingPkg = ElUtil::ToElString(env, jcallingPkg);

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if(!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("ElContentProviderProxy_nativeOpenAssetFile() ToElUri fail!");
        }
    } else {
        ALOGE("ElContentProviderProxy_nativeOpenAssetFile() juri is NULL");
    }

    AutoPtr<IICancellationSignal> cancellationSignal;
    if (jcancellationSignal != NULL) {
        ALOGE("nativeQuery() jcancellationSignal not NULL!");
    }

    String mode = ElUtil::ToElString(env, jmode);
    IIContentProvider* cp = (IIContentProvider*)jproxy;
    AutoPtr<IAssetFileDescriptor> assetFd;
    cp->OpenAssetFile(callingPkg, uri, mode, cancellationSignal, (IAssetFileDescriptor**)&assetFd);

    return ElUtil::GetJavaAssetFileDescriptor(env, assetFd);
}

static jobjectArray android_content_ElContentProviderProxy_nativeApplyBatch(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject joperations)
{
    ALOGD("+ android_content_ElContentProviderProxy_nativeApplyBatch()");
    String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    AutoPtr<IArrayList> operations;
    CParcelableList::New((IArrayList**)&operations);
    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID mSize = env->GetMethodID(listKlass, "size", "()I");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);

    jmethodID mGet = env->GetMethodID(listKlass, "get", "(I)Ljava/lang/Object;");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);
    env->DeleteLocalRef(listKlass);

    jint jsize = env->CallIntMethod(joperations, mSize);
    ElUtil::CheckErrorAndLog(env, "CallIntMethod: mActions : %d!\n", __LINE__);
    ALOGD("android_content_ElContentProviderProxy_nativeApplyBatch(), operation lenght = %d", jsize);
    if (jsize > 0) {
        for (jint i = 0; i < jsize; i++) {
            jobject joperation = env->CallObjectMethod(joperations, mGet, i);
            ElUtil::CheckErrorAndLog(env, "CallObjectMethod: get : %d!\n", __LINE__);
            AutoPtr<IContentProviderOperation> operation;
            ElUtil::ToElContentProviderOperation(env, joperation, (IContentProviderOperation**)&operation);
            env->DeleteLocalRef(joperation);
            if(operation != NULL) {
                operations->Add((IInterface*)operation);
            }
        }
    }

    AutoPtr<ArrayOf<IContentProviderResult*> > providerResults;
    IIContentProvider* cp = (IIContentProvider*)jproxy;
    ECode ec = cp->ApplyBatch(callingPkg, operations, (ArrayOf<IContentProviderResult*>**)&providerResults);
    ALOGD("android_content_ElContentProviderProxy_nativeApplyBatch, ApplyBatch() ec = %08x", ec);

    jobjectArray jproviderResults = NULL;
    if (providerResults != NULL) {
        Int32 resultSize = providerResults->GetLength();
        ALOGD("android_content_ElContentProviderProxy_nativeApplyBatch(), resultSize = %d", resultSize);

        jclass cprklass = env->FindClass("android/content/ContentProviderResult");
        ElUtil::CheckErrorAndLog(env, "FindClass: ContentProviderResult : %d!\n", __LINE__);

        jproviderResults = env->NewObjectArray(resultSize, cprklass, NULL);

        for (Int32 i = 0; i < resultSize; i++) {
            AutoPtr<IContentProviderResult> result = (*providerResults)[i];
            jobject jresult = ElUtil::GetJavaContentProviderResult(env, result);
            env->SetObjectArrayElement(jproviderResults, i, jresult);
            ElUtil::CheckErrorAndLog(env, "SetObjectArrayElement: ContentProviderResult : %d!\n", __LINE__);
            env->DeleteLocalRef(jresult);
        }

        env->DeleteLocalRef(cprklass);
    }

    ALOGD("- android_content_ElContentProviderProxy_nativeApplyBatch()");
    return jproviderResults;
}

static jobject android_content_ElContentProviderProxy_nativeCreateCancellationSignal(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeCreateCancellationSignal()");

    IIContentProvider* cp = (IIContentProvider*)jproxy;
    AutoPtr<IICancellationSignal> csignal;
    cp->CreateCancellationSignal((IICancellationSignal**)&csignal);

    jobject jcsignal = NULL;
    if (csignal != NULL) {
        jclass csKlass = env->FindClass("android/os/ElCancellationSignalProxy");
        ElUtil::CheckErrorAndLog(env, "nativeCreateCancellationSignal Fail FindClass: ElCancellationSignalProxy : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csKlass, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "nativeCreateCancellationSignal Fail GetMethodID: ElCancellationSignalProxy : %d!\n", __LINE__);

        jcsignal = env->NewObject(csKlass, m, (jlong)csignal.Get());
        ElUtil::CheckErrorAndLog(env, "nativeCreateCancellationSignal Fail NewObject: ElCancellationSignalProxy : %d!\n", __LINE__);
        csignal->AddRef();

        env->DeleteLocalRef(csKlass);
    }

    // ALOGD("- android_content_ElContentProviderProxy_nativeCreateCancellationSignal()");
    return jcsignal;
}

static jobject android_content_ElContentProviderProxy_nativeCanonicalize(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject jurl)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeCanonicalize()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    AutoPtr<IUri> url;
    if (jurl != NULL) {
        if (!ElUtil::ToElUri(env, jurl, (IUri**)&url)) {
            ALOGE("nativeCanonicalize() UriToUri fail!");
        }
    }

    IIContentProvider* cp = (IIContentProvider*)jproxy;
    AutoPtr<IUri> rUri;
    cp->Canonicalize(callingPkg, url, (IUri**)&rUri);

    if (rUri != NULL) {
        return ElUtil::GetJavaUri(env, rUri);
    }

    // ALOGD("- android_content_ElContentProviderProxy_nativeCanonicalize()");
    return NULL;
}

static jobject android_content_ElContentProviderProxy_nativeUncanonicalize(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPkg, jobject jurl)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeUncanonicalize()");

    String callingPkg = ElUtil::ToElString(env, jcallingPkg);
    AutoPtr<IUri> url;
    if (jurl != NULL) {
        if (!ElUtil::ToElUri(env, jurl, (IUri**)&url)) {
            ALOGE("nativeUncanonicalize() UriToUri fail!");
        }
    }

    IIContentProvider* cp = (IIContentProvider*)jproxy;
    AutoPtr<IUri> rUri;
    cp->Uncanonicalize(callingPkg, url, (IUri**)&rUri);

    if (rUri != NULL) {
        return ElUtil::GetJavaUri(env, rUri);
    }

    // ALOGD("- android_content_ElContentProviderProxy_nativeUncanonicalize()");
    return NULL;
}

static void android_content_ElContentProviderProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_content_ElContentProviderProxy_nativeDestroy()");

    IIContentProvider* obj = (IIContentProvider*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_content_ElContentProviderProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_content_ElContentProviderProxy_nativeFinalize },
    { "nativeCall",    "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;",
            (void*) android_content_ElContentProviderProxy_nativeCall },
    { "nativeQuery",    "(JLjava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;",
            (void*) android_content_ElContentProviderProxy_nativeQuery },
    { "nativeOpenTypedAssetFile",    "(JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ICancellationSignal;)Landroid/content/res/AssetFileDescriptor;",
            (void*) android_content_ElContentProviderProxy_nativeOpenTypedAssetFile },
    { "nativeInsert",    "(JLjava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;",
            (void*) android_content_ElContentProviderProxy_nativeInsert },
    { "nativeDelete",    "(JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I",
            (void*) android_content_ElContentProviderProxy_nativeDelete },
    { "nativeUpdate",    "(JLjava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I",
            (void*) android_content_ElContentProviderProxy_nativeUpdate},
    { "nativeGetType",    "(JLandroid/net/Uri;)Ljava/lang/String;",
            (void*) android_content_ElContentProviderProxy_nativeGetType},
    { "nativeBulkInsert",   "(JLjava/lang/String;Landroid/net/Uri;[Landroid/content/ContentValues;)I",
            (void*) android_content_ElContentProviderProxy_nativeBulkInsert},
    { "nativeOpenFile",    "(JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/os/ParcelFileDescriptor;",
            (void*) android_content_ElContentProviderProxy_nativeOpenFile},
    { "nativeOpenAssetFile",    "(JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/content/res/AssetFileDescriptor;",
            (void*) android_content_ElContentProviderProxy_nativeOpenAssetFile},
    { "nativeApplyBatch",    "(JLjava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;",
            (void*) android_content_ElContentProviderProxy_nativeApplyBatch},
    { "nativeCreateCancellationSignal",    "(J)Landroid/os/ICancellationSignal;",
            (void*) android_content_ElContentProviderProxy_nativeCreateCancellationSignal},
    { "nativeCanonicalize",    "(JLjava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;",
            (void*) android_content_ElContentProviderProxy_nativeCanonicalize },
    { "nativeUncanonicalize",    "(JLjava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;",
            (void*) android_content_ElContentProviderProxy_nativeUncanonicalize },
};

int register_android_content_ElContentProviderProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/content/ElContentProviderProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

