
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.View.h>
#include <Elastos.CoreLibrary.Core.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Core::ICharSequence;
using Elastos::Core::CString;

using Elastos::Droid::Internal::View::IIInputContextCallback;

static void com_android_internal_view_ElInputContextCallbackProxy_nativeSetCursorCapsMode(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jcapsMode, jint jseq)
{
    // ALOGD("+ com_android_internal_view_ElInputContextCallbackProxy_nativeSetCursorCapsMode()");
    IIInputContextCallback* callback = (IIInputContextCallback*)jproxy;
    callback->SetCursorCapsMode((Int32)jcapsMode, (Int32)jseq);
    // ALOGD("- com_android_internal_view_ElInputContextCallbackProxy_nativeSetCursorCapsMode()");
}

static void com_android_internal_view_ElInputContextCallbackProxy_nativeSetTextBeforeCursor(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jtextBeforeCursor, jint jseq)
{
    // ALOGD("+ com_android_internal_view_ElInputContextCallbackProxy_nativeSetTextBeforeCursor()");

    AutoPtr<ICharSequence> textBeforeCursor;
    if (jtextBeforeCursor != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        ElUtil::CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);
        env->DeleteLocalRef(csClass);

        jstring jcsTextBeforeCursor = (jstring)env->CallObjectMethod(jtextBeforeCursor, m);
        ElUtil::CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String csTextBeforeCursor = ElUtil::ToElString(env, jcsTextBeforeCursor);
        env->DeleteLocalRef(jcsTextBeforeCursor);

        CString::New(csTextBeforeCursor, (ICharSequence**)&textBeforeCursor);
    }

    IIInputContextCallback* callback = (IIInputContextCallback*)jproxy;
    callback->SetTextBeforeCursor(textBeforeCursor, (Int32)jseq);

    // ALOGD("- com_android_internal_view_ElInputContextCallbackProxy_nativeSetTextBeforeCursor()");
}

static void com_android_internal_view_ElInputContextCallbackProxy_nativeSetTextAfterCursor(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jtextAfterCursor, jint jseq)
{
    // ALOGD("+ com_android_internal_view_ElInputContextCallbackProxy_nativeSetTextAfterCursor()");

    AutoPtr<ICharSequence> textAfterCursor;
    if (jtextAfterCursor != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        ElUtil::CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);
        env->DeleteLocalRef(csClass);

        jstring jcstextAfterCursor = (jstring)env->CallObjectMethod(jtextAfterCursor, m);
        ElUtil::CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String cstextAfterCursor = ElUtil::ToElString(env, jcstextAfterCursor);
        env->DeleteLocalRef(jcstextAfterCursor);

        CString::New(cstextAfterCursor, (ICharSequence**)&textAfterCursor);
    }

    IIInputContextCallback* callback = (IIInputContextCallback*)jproxy;
    callback->SetTextAfterCursor(textAfterCursor, (Int32)jseq);

    // ALOGD("- com_android_internal_view_ElInputContextCallbackProxy_nativeSetTextAfterCursor()");
}

static void com_android_internal_view_ElInputContextCallbackProxy_nativeSetExtractedText(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jextractedText, jint jseq)
{
    // ALOGD("+ com_android_internal_view_ElInputContextCallbackProxy_nativeSetExtractedText()");

    AutoPtr<IExtractedText> extractedText;
    if (jextractedText != NULL) {
        if (!ElUtil::ToElExtractedText(env, jextractedText, (IExtractedText**)&extractedText)) {
            ALOGE("nativeSetExtractedText ToElExtractedText fail!");
        }
    }

    IIInputContextCallback* callback = (IIInputContextCallback*)jproxy;
    callback->SetExtractedText(extractedText, (Int32)jseq);

    // ALOGD("- com_android_internal_view_ElInputContextCallbackProxy_nativeSetExtractedText()");
}

static void com_android_internal_view_ElInputContextCallbackProxy_nativeSetSelectedText(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jselectedText, jint jseq)
{
    // ALOGD("+ com_android_internal_view_ElInputContextCallbackProxy_nativeSetSelectedText()");

    AutoPtr<ICharSequence> selectedText;
    if (jselectedText != NULL) {
        jclass csClass = env->FindClass("java/lang/CharSequence");
        ElUtil::CheckErrorAndLog(env, "FindClass CharSequence : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(csClass, "toString", "()Ljava/lang/String;");
        ElUtil::CheckErrorAndLog(env, "GetMethodID toString : %d!\n", __LINE__);

        jstring jcsselectedText = (jstring)env->CallObjectMethod(jselectedText, m);
        ElUtil::CheckErrorAndLog(env, "CallObjectMethod toString : %d!\n", __LINE__);

        String csselectedText = ElUtil::ToElString(env, jcsselectedText);
        env->DeleteLocalRef(jcsselectedText);

        CString::New(csselectedText, (ICharSequence**)&selectedText);
    }

    IIInputContextCallback* callback = (IIInputContextCallback*)jproxy;
    callback->SetSelectedText(selectedText, (Int32)jseq);

    // ALOGD("- com_android_internal_view_ElInputContextCallbackProxy_nativeSetSelectedText()");
}

static void com_android_internal_view_ElInputContextCallbackProxy_nativeSetRequestUpdateCursorAnchorInfoResult(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jresult, jint jseq)
{
    // ALOGD("+ com_android_internal_view_ElInputContextCallbackProxy_nativeSetRequestUpdateCursorAnchorInfoResult()");

    IIInputContextCallback* callback = (IIInputContextCallback*)jproxy;
    callback->SetRequestUpdateCursorAnchorInfoResult(jresult, jseq);

    // ALOGD("- com_android_internal_view_ElInputContextCallbackProxy_nativeSetRequestUpdateCursorAnchorInfoResult()");
}

static void com_android_internal_view_ElInputContextCallbackProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ com_android_internal_view_ElInputContextCallbackProxy_nativeDestroy()");

    IIInputContextCallback* obj = (IIInputContextCallback*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- com_android_internal_view_ElInputContextCallbackProxy_nativeDestroy()");
}


namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) com_android_internal_view_ElInputContextCallbackProxy_nativeFinalize },
    { "nativeSetCursorCapsMode",    "(JII)V",
            (void*) com_android_internal_view_ElInputContextCallbackProxy_nativeSetCursorCapsMode },
    { "nativeSetTextBeforeCursor",    "(JLjava/lang/CharSequence;I)V",
            (void*) com_android_internal_view_ElInputContextCallbackProxy_nativeSetTextBeforeCursor },
    { "nativeSetTextAfterCursor",    "(JLjava/lang/CharSequence;I)V",
            (void*) com_android_internal_view_ElInputContextCallbackProxy_nativeSetTextAfterCursor },
    { "nativeSetExtractedText",    "(JLandroid/view/inputmethod/ExtractedText;I)V",
            (void*) com_android_internal_view_ElInputContextCallbackProxy_nativeSetExtractedText },
    { "nativeSetSelectedText",    "(JLjava/lang/CharSequence;I)V",
            (void*) com_android_internal_view_ElInputContextCallbackProxy_nativeSetSelectedText },
    { "nativeSetRequestUpdateCursorAnchorInfoResult",    "(JZI)V",
            (void*) com_android_internal_view_ElInputContextCallbackProxy_nativeSetRequestUpdateCursorAnchorInfoResult },
};

int register_com_android_internal_view_ElInputContextCallbackProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/internal/view/ElInputContextCallbackProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

