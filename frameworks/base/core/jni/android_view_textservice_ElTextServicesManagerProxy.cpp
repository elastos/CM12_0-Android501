
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.View.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <utils/Log.h>
#include <elastos/utility/etl/HashMap.h>

using android::ElUtil;

using Elastos::Utility::Etl::HashMap;
using Elastos::Droid::JavaProxy::CSpellCheckerSessionListenerNative;
using Elastos::Droid::JavaProxy::CTextServicesSessionListenerNative;
using Elastos::Droid::View::TextService::ISpellCheckerInfo;
using Elastos::Droid::View::TextService::ISpellCheckerSubtype;
using Elastos::Droid::Internal::TextService::IISpellCheckerSessionListener;
using Elastos::Droid::Internal::TextService::IITextServicesSessionListener;
using Elastos::Droid::Internal::TextService::IITextServicesManager;

static HashMap<Int32, IISpellCheckerSessionListener*> sSCSListener;

static jboolean android_view_textservice_ElTextServicesManagerProxy_nativeIsSpellCheckerEnabled(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeIsSpellCheckerEnabled()");

    IITextServicesManager* tsm = (IITextServicesManager*)jproxy;

    Boolean isEnable = FALSE;
    tsm->IsSpellCheckerEnabled(&isEnable);

    // ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeIsSpellCheckerEnabled()");
    return (jboolean)isEnable;
}

static jobject android_view_textservice_ElTextServicesManagerProxy_nativeGetCurrentSpellCheckerSubtype(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jlocale, jboolean jallowImplicitlySelectedSubtype)
{
    // ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeGetCurrentSpellCheckerSubtype()");

    IITextServicesManager* tsm = (IITextServicesManager*)jproxy;

    String locale = ElUtil::ToElString(env, jlocale);

    AutoPtr<ISpellCheckerSubtype> subtype;
    tsm->GetCurrentSpellCheckerSubtype(locale, (Boolean)jallowImplicitlySelectedSubtype, (ISpellCheckerSubtype**)&subtype);
    jobject jsubtype = NULL;
    if (subtype != NULL) {
        jsubtype = ElUtil::GetJavaSpellCheckerSubtype(env, subtype);
    }

    // ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeGetCurrentSpellCheckerSubtype()");
    return jsubtype;
}

static jobject android_view_textservice_ElTextServicesManagerProxy_nativeGetCurrentSpellChecker(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jlocale)
{
    ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeGetCurrentSpellChecker()");

    IITextServicesManager* tsm = (IITextServicesManager*)jproxy;

    String locale = ElUtil::ToElString(env, jlocale);

    AutoPtr<ISpellCheckerInfo> info;
    ECode ec = tsm->GetCurrentSpellChecker(locale, (ISpellCheckerInfo**)&info);
    ALOGE("nativeGetCurrentSpellChecker() ec: 0x%08x", ec);
    jobject jinfo = NULL;
    if (info != NULL) {
        jinfo = ElUtil::GetJavaSpellCheckerInfo(env, info);
    }

    ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeGetCurrentSpellChecker()");
    return jinfo;
}

static void android_view_textservice_ElTextServicesManagerProxy_nativeSetCurrentSpellChecker(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jlocale, jstring jsciId)
{
    ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeSetCurrentSpellChecker()");

    IITextServicesManager* tsm = (IITextServicesManager*)jproxy;

    String locale = ElUtil::ToElString(env, jlocale);
    String sciId = ElUtil::ToElString(env, jsciId);

    AutoPtr<ISpellCheckerInfo> info;
    ECode ec = tsm->SetCurrentSpellChecker(locale, sciId);
    ALOGE("nativeSetCurrentSpellChecker() ec: 0x%08x", ec);

    ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeSetCurrentSpellChecker()");
}

static void android_view_textservice_ElTextServicesManagerProxy_nativeSetCurrentSpellCheckerSubtype(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jlocale, jint jhashCode)
{
    ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeSetCurrentSpellCheckerSubtype()");

    IITextServicesManager* tsm = (IITextServicesManager*)jproxy;
    String locale = ElUtil::ToElString(env, jlocale);

    ECode ec = tsm->SetCurrentSpellCheckerSubtype(locale, (Int32)jhashCode);
    ALOGE("nativeSetCurrentSpellCheckerSubtype() ec: 0x%08x", ec);

    ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeSetCurrentSpellCheckerSubtype()");
}

static void android_view_textservice_ElTextServicesManagerProxy_nativeSetSpellCheckerEnabled(JNIEnv* env, jobject clazz, jlong jproxy,
    jboolean jenabled)
{
    ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeSetSpellCheckerEnabled()");

    IITextServicesManager* tsm = (IITextServicesManager*)jproxy;

    ECode ec = tsm->SetSpellCheckerEnabled((Boolean)jenabled);
    ALOGE("nativeSetSpellCheckerEnabled() ec: 0x%08x", ec);

    ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeSetSpellCheckerEnabled()");
}

static jobjectArray android_view_textservice_ElTextServicesManagerProxy_nativeGetEnabledSpellCheckers(JNIEnv* env, jobject clazz, jlong jproxy)
{
    ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeGetEnabledSpellCheckers()");

    IITextServicesManager* tsm = (IITextServicesManager*)jproxy;

    AutoPtr<ArrayOf<ISpellCheckerInfo*> > infos;
    ECode ec = tsm->GetEnabledSpellCheckers((ArrayOf<ISpellCheckerInfo*>**)&infos);
    ALOGE("nativeGetEnabledSpellCheckers() ec: 0x%08x", ec);

    jobjectArray jinfos = NULL;
    if (infos != NULL) {
        jclass sciKlass = env->FindClass("android/view/textservice/SpellCheckerInfo");
        ElUtil::CheckErrorAndLog(env, "FindClass: SpellCheckerInfo : %d!\n", __LINE__);

        Int32 count = infos->GetLength();
        jinfos = env->NewObjectArray((jsize)count, sciKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: SpellCheckerInfo : %d!\n", __LINE__);

        for (Int32 i = 0; i < count; i++) {
            AutoPtr<ISpellCheckerInfo> info = (*infos)[i];

            jobject jinfo = ElUtil::GetJavaSpellCheckerInfo(env, info);

            env->SetObjectArrayElement(jinfos, i, jinfo);
            ElUtil::CheckErrorAndLog(env, "SetObjectArrayElement: SpellCheckerInfo : %d!\n", __LINE__);

            env->DeleteLocalRef(jinfo);
        }

        env->DeleteLocalRef(sciKlass);
    }

    ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeGetEnabledSpellCheckers()");
    return jinfos;
}

static void android_view_textservice_ElTextServicesManagerProxy_nativeGetSpellCheckerService(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jsciId, jstring jlocale, jobject jtsListener, jobject jscListener, jobject jbundle)
{
    ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeGetSpellCheckerService()");

    IITextServicesManager* tsm = (IITextServicesManager*)jproxy;

    String sciId = ElUtil::ToElString(env, jsciId);
    String locale = ElUtil::ToElString(env, jlocale);

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);

    AutoPtr<IITextServicesSessionListener> tsListener;
    if (jtsListener != NULL) {
        jobject jInstance = env->NewGlobalRef(jtsListener);

        if(NOERROR != CTextServicesSessionListenerNative::New((Handle64)jvm, (Handle64)jInstance, (IITextServicesSessionListener**)&tsListener)) {
            ALOGE("nativeGetSpellCheckerService()  new CTextServicesSessionListenerNative fail!\n");
        }
    }

    AutoPtr<IISpellCheckerSessionListener> scListener;
    if (jscListener != NULL) {
        jobject jInstance = env->NewGlobalRef(jscListener);

        if(NOERROR != CSpellCheckerSessionListenerNative::New((Handle64)jvm, (Handle64)jInstance, (IISpellCheckerSessionListener**)&tsListener)) {
            ALOGE("nativeGetSpellCheckerService()  new CSpellCheckerSessionListenerNative fail!\n");
        } else {
            scListener->AddRef();
            Int32 hashCode = ElUtil::GetJavaHashCode(env, jscListener);
            sSCSListener[hashCode] = scListener;
        }
    }

    AutoPtr<IBundle> bundle;
    if (jbundle != NULL) {
        if (!ElUtil::ToElBundle(env, jbundle, (IBundle**)&bundle)) {
            ALOGE("nativeGetSpellCheckerService() ToElBundle fail!");
        }
    }

    ECode ec = tsm->GetSpellCheckerService(sciId, locale, tsListener, scListener, bundle);
    ALOGE("nativeGetSpellCheckerService() ec: 0x%08x", ec);

    ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeGetSpellCheckerService()");
}

static void android_view_textservice_ElTextServicesManagerProxy_nativeFinishSpellCheckerService(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlistener)
{
    ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeFinishSpellCheckerService()");

    IITextServicesManager* tsm = (IITextServicesManager*)jproxy;

    Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);

    AutoPtr<IISpellCheckerSessionListener> listener;
    if (sSCSListener.Find(hashCode) != sSCSListener.End()) {
        listener = sSCSListener[hashCode];
        sSCSListener.Erase(hashCode);
    }

    if (NULL == listener) {
        ALOGE("nativeFinishSpellCheckerService() Invalid IISpellCheckerSessionListener!\n");
        env->ExceptionDescribe();
    }

    ECode ec = tsm->FinishSpellCheckerService(listener);
    ALOGE("nativeFinishSpellCheckerService() ec: 0x%08x", ec);

    listener->Release();

    ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeFinishSpellCheckerService()");
}

static void android_view_textservice_ElTextServicesManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_view_textservice_ElTextServicesManagerProxy_nativeDestroy()");

    IITextServicesManager* obj = (IITextServicesManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_view_textservice_ElTextServicesManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeFinalize },
    { "nativeIsSpellCheckerEnabled",    "(J)Z",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeIsSpellCheckerEnabled },
    { "nativeGetCurrentSpellCheckerSubtype",    "(JLjava/lang/String;Z)Landroid/view/textservice/SpellCheckerSubtype;",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeGetCurrentSpellCheckerSubtype },
    { "nativeGetCurrentSpellChecker",    "(JLjava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeGetCurrentSpellChecker },
    { "nativeSetCurrentSpellChecker",    "(JLjava/lang/String;Ljava/lang/String;)V",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeSetCurrentSpellChecker },
    { "nativeSetCurrentSpellCheckerSubtype",    "(JLjava/lang/String;I)V",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeSetCurrentSpellCheckerSubtype },
    { "nativeSetSpellCheckerEnabled",    "(JZ)V",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeSetSpellCheckerEnabled },
    { "nativeGetEnabledSpellCheckers",    "(J)[Landroid/view/textservice/SpellCheckerInfo;",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeGetEnabledSpellCheckers },
    { "nativeGetSpellCheckerService",    "(JLjava/lang/String;Ljava/lang/String;Lcom/android/internal/textservice/ITextServicesSessionListener;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;)V",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeGetSpellCheckerService },
    { "nativeFinishSpellCheckerService",    "(JLcom/android/internal/textservice/ISpellCheckerSessionListener;)V",
            (void*) android_view_textservice_ElTextServicesManagerProxy_nativeFinishSpellCheckerService },
};

int register_android_view_textservice_ElTextServicesManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/view/textservice/ElTextServicesManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

