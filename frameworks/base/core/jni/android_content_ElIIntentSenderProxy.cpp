
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>
#include <_Elastos.Droid.JavaProxy.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Content::IIntent;
using Elastos::Droid::Content::IIntentReceiver;
using Elastos::Droid::Content::IIIntentSender;
using Elastos::Droid::JavaProxy::CIntentReceiverNative;

static jint android_content_ElIIntentSenderProxy_nativeSend(JNIEnv* env, jobject clazz, jlong jproxy,
        jint jcode, jobject jintent, jstring jresolvedType, jobject jfinishedReceiver, jstring jrequiredPermission)
{
    // ALOGD("+ android_content_ElIIntentSenderProxy_nativeSend()");

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent)) {
            ALOGE("nativeSend() ToElIntent fail!");
        }
    }

    String resolvedType = ElUtil::ToElString(env, jresolvedType);

    AutoPtr<IIntentReceiver> finishedReceiver;
    if (jfinishedReceiver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jfinishedReceiver);
        CIntentReceiverNative::New((Handle64)jvm, (Handle64)jInstance, (IIntentReceiver**)&finishedReceiver);
    }

    String requiredPermission = ElUtil::ToElString(env, jrequiredPermission);

    IIIntentSender* iinSender = (IIIntentSender*)jproxy;

    Int32 result = 0;
    iinSender->Send((Int32)jcode, intent, resolvedType, finishedReceiver, requiredPermission, &result);

    // ALOGD("- android_content_ElIIntentSenderProxy_nativeSend()");
    return (jint)result;
}

static void android_content_ElIIntentSenderProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_content_ElIIntentSenderProxy_nativeDestroy()");

    IIIntentSender* obj = (IIIntentSender*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_content_ElIIntentSenderProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_content_ElIIntentSenderProxy_nativeFinalize },
    { "nativeSend",    "(JILandroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;Ljava/lang/String;)I",
            (void*) android_content_ElIIntentSenderProxy_nativeSend },
};

int register_android_content_ElIIntentSenderProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/content/ElIIntentSenderProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

