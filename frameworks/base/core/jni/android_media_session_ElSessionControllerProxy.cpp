
#define LOG_TAG "ElSessionControllerProxyJNI"
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
//#include <elastos/core/Object.h>
//#include <elastos/core/AutoLock.h>
//#include <elastos/utility/etl/HashMap.h>
//#include <Elastos.Droid.App.h>
//#include <Elastos.Droid.Telephony.h>
//#include <Elastos.Droid.Internal.h>
//#include <Elastos.Droid.Os.h>
//#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.Droid.Media.h>
#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::Media::Session::IISessionController;


static void android_media_session_ElSessionControllerProxy_nativeSendCommand(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring command,
    /* [in] */ jobject args,
    /* [in] */ jobject cb)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static jboolean android_media_session_ElSessionControllerProxy_nativeSendMediaButton(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject mediaButton)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return false;
}

static void android_media_session_ElSessionControllerProxy_nativeRegisterCallbackListener(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject cb)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeUnregisterCallbackListener(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject cb)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static jboolean android_media_session_ElSessionControllerProxy_nativeIsTransportControlEnabled(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return false;
}

static jstring android_media_session_ElSessionControllerProxy_nativeGetPackageName(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return NULL;
}

static jstring android_media_session_ElSessionControllerProxy_nativeGetTag(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return NULL;
}

static jobject android_media_session_ElSessionControllerProxy_nativeGetLaunchPendingIntent(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return NULL;
}

static jlong android_media_session_ElSessionControllerProxy_nativeGetFlags(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return 0;
}

static jobject android_media_session_ElSessionControllerProxy_nativeGetVolumeAttributes(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return NULL;
}

static void android_media_session_ElSessionControllerProxy_nativeAdjustVolume(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jint direction, jint flags, jstring packageName)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeSetVolumeTo(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jint value,
    /* [in] */ jint flags,
    /* [in] */ jstring packageName)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativePlay(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativePlayFromMediaId(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring uri,
    /* [in] */ jobject extras)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativePlayFromSearch(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring uri,
    /* [in] */ jobject extras)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeSkipToQueueItem(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jlong id)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativePause(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeStop(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeNext(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativePrevious(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeFastForward(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeRewind(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeSeekTo(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jlong pos)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeSetRemoteControlClientBrowsedPlayer(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeSetRemoteControlClientPlayItem(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jlong uid,
    /* [in] */ jint scope)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeGetRemoteControlClientNowPlayingEntries(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeRate(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject rating)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionControllerProxy_nativeSendCustomAction(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring action,
    /* [in] */ jobject args)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
}

static jobject android_media_session_ElSessionControllerProxy_nativeGetMetadata(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    IISessionController* sessionController = (IISessionController*)jproxy;
    AutoPtr<IMediaMetadata> data;
    sessionController->GetMetadata((IMediaMetadata**)&data);
    jobject jdata = NULL;
    if (data != NULL) {
        jdata = ElUtil::GetJavaMediaMetadata(env, data);
    }
    return jdata;
}

static jobject android_media_session_ElSessionControllerProxy_nativeGetPlaybackState(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return NULL;
}

static jobject android_media_session_ElSessionControllerProxy_nativeGetQueue(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return NULL;
}

static jobject android_media_session_ElSessionControllerProxy_nativeGetQueueTitle(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return NULL;
}

static jobject android_media_session_ElSessionControllerProxy_nativeGetExtras(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return NULL;
}

static jint android_media_session_ElSessionControllerProxy_nativeGetRatingType(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionControllerProxy.cpp line:%d", __LINE__);
    return 0;
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeSendCommand",      "(JLjava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeSendCommand},
    { "nativeSendMediaButton",      "(JLandroid/view/KeyEvent;)Z",
            (void*) android_media_session_ElSessionControllerProxy_nativeSendMediaButton},
    { "nativeRegisterCallbackListener",      "(JLandroid/media/session/ISessionControllerCallback;)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeRegisterCallbackListener},
    { "nativeUnregisterCallbackListener",      "(JLandroid/media/session/ISessionControllerCallback;)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeUnregisterCallbackListener},
    { "nativeIsTransportControlEnabled",      "(J)Z",
            (void*) android_media_session_ElSessionControllerProxy_nativeIsTransportControlEnabled},
    { "nativeGetPackageName",      "(J)Ljava/lang/String;",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetPackageName},
    { "nativeGetTag",      "(J)Ljava/lang/String;",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetTag},
    { "nativeGetLaunchPendingIntent",      "(J)Landroid/app/PendingIntent;",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetLaunchPendingIntent},
    { "nativeGetFlags",      "(J)J",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetFlags},
    { "nativeGetVolumeAttributes",      "(J)Landroid/media/session/ParcelableVolumeInfo;",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetVolumeAttributes},
    { "nativeAdjustVolume",      "(JIILjava/lang/String;)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeAdjustVolume},
    { "nativeSetVolumeTo",      "(JIILjava/lang/String;)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeSetVolumeTo},
    { "nativePlay",      "(J)V",
            (void*) android_media_session_ElSessionControllerProxy_nativePlay},
    { "nativePlayFromMediaId",      "(JLjava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_media_session_ElSessionControllerProxy_nativePlayFromMediaId},
    { "nativePlayFromSearch",      "(JLjava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_media_session_ElSessionControllerProxy_nativePlayFromSearch},
    { "nativeSkipToQueueItem",      "(JJ)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeSkipToQueueItem},
    { "nativePause",      "(J)V",
            (void*) android_media_session_ElSessionControllerProxy_nativePause},
    { "nativeStop",      "(J)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeStop},
    { "nativeNext",      "(J)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeNext},
    { "nativePrevious",      "(J)V",
            (void*) android_media_session_ElSessionControllerProxy_nativePrevious},
    { "nativeFastForward",      "(J)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeFastForward},
    { "nativeRewind",      "(J)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeRewind},
    { "nativeSeekTo",      "(JJ)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeSeekTo},
    { "nativeSetRemoteControlClientBrowsedPlayer",      "(J)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeSetRemoteControlClientBrowsedPlayer},
    { "nativeSetRemoteControlClientPlayItem",      "(JJI)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeSetRemoteControlClientPlayItem},
    { "nativeGetRemoteControlClientNowPlayingEntries",      "(J)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetRemoteControlClientNowPlayingEntries},
    { "nativeRate",      "(JLandroid/media/Rating;)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeRate},
    { "nativeSendCustomAction",      "(JLjava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_media_session_ElSessionControllerProxy_nativeSendCustomAction},
    { "nativeGetMetadata",      "(J)Landroid/media/MediaMetadata;",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetMetadata},
    { "nativeGetPlaybackState",      "(J)Landroid/media/session/PlaybackState;",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetPlaybackState},
    { "nativeGetQueue",      "(J)Landroid/content/pm/ParceledListSlice;",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetQueue},
    { "nativeGetQueueTitle",      "(J)Ljava/lang/CharSequence;",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetQueueTitle},
    { "nativeGetExtras",      "(J)Landroid/os/Bundle;",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetExtras},
    { "nativeGetRatingType",      "(J)I",
            (void*) android_media_session_ElSessionControllerProxy_nativeGetRatingType},
};

int register_android_media_session_ElSessionControllerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/media/session/ElSessionControllerProxy",
        gMethods, NELEM(gMethods));
}

}; // end namespace android
