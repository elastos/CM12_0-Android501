
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Database.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.IO.h>
#include <elastos/utility/etl/HashMap.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Content::IContentResolver;
using Elastos::Droid::Database::ICursor;
using Elastos::Droid::Database::ICharArrayBuffer;
using Elastos::Droid::Database::IContentObserver;
using Elastos::Droid::Database::IDataSetObserver;

using Elastos::Droid::JavaProxy::CContentObserverNative;
using Elastos::Droid::JavaProxy::CContentResolverNative;
using Elastos::Droid::JavaProxy::CDataSetObserverNative;
using Elastos::Utility::Etl::HashMap;
using Elastos::IO::ICloseable;

static HashMap<Int32, AutoPtr<IContentObserver> > sContentObserver;
static Mutex sContentObserverLock;
static HashMap<Int32, AutoPtr<IDataSetObserver> > sDataSetObserver;
static Mutex sDataSetObserverLock;

static jint android_database_ElCursorProxy_nativeGetCount(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetCount()");

    ICursor* cursor = (ICursor*)jproxy;
    Int32 count;
    cursor->GetCount(&count);

    // ALOGD("- android_database_ElCursorProxy_nativeGetCount()");
    return (jint)count;
}

static jint android_database_ElCursorProxy_nativeGetColumnIndexOrThrow(JNIEnv* env, jobject clazz, jlong jproxy, jstring jcolumnName)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetColumnIndexOrThrow()");

    String columnName = ElUtil::ToElString(env, jcolumnName);

    ICursor* cursor = (ICursor*)jproxy;
    Int32 columnIndex;
    cursor->GetColumnIndexOrThrow(columnName, &columnIndex);
    if (columnIndex < 0) {
        jniThrowException(env, "java/lang/IllegalArgumentException", String("column '") + columnName + "' does not exist");
    }

    // ALOGD("- android_database_ElCursorProxy_nativeGetColumnIndexOrThrow()");
    return (jint)columnIndex;
}

static jint android_database_ElCursorProxy_nativeGetColumnIndex(JNIEnv* env, jobject clazz, jlong jproxy, jstring jcolumnName)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetColumnIndex()");

    String columnName = ElUtil::ToElString(env, jcolumnName);

    ICursor* cursor = (ICursor*)jproxy;
    Int32 columnIndex;
    cursor->GetColumnIndex(columnName, &columnIndex);

    // ALOGD("- android_database_ElCursorProxy_nativeGetColumnIndex()");
    return (jint)columnIndex;
}

static jboolean android_database_ElCursorProxy_nativeMoveToPosition(JNIEnv* env, jobject clazz, jlong jproxy, jint jposition)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeMoveToPosition()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean succeeded;
    cursor->MoveToPosition((Int32)jposition, &succeeded);

    // ALOGD("- android_database_ElCursorProxy_nativeMoveToPosition()");
    return (jboolean)succeeded;
}

static jlong android_database_ElCursorProxy_nativeGetLong(JNIEnv* env, jobject clazz, jlong jproxy, jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetLong()");

    ICursor* cursor = (ICursor*)jproxy;
    Int64 columnValue;
    cursor->GetInt64((Int32)jcolumnIndex, &columnValue);

    // ALOGD("- android_database_ElCursorProxy_nativeGetLong()");
    return (jlong)columnValue;
}

static jboolean android_database_ElCursorProxy_nativeIsClosed(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeIsClosed()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean closed;
    cursor->IsClosed(&closed);

    // ALOGD("- android_database_ElCursorProxy_nativeIsClosed()");
    return (jboolean)closed;
}

static void android_database_ElCursorProxy_nativeClose(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeClose()");

    ICursor* cursor = (ICursor*)jproxy;
    ICloseable::Probe(cursor)->Close();

    // ALOGD("- android_database_ElCursorProxy_nativeClose()");
}

static jboolean android_database_ElCursorProxy_nativeMove(JNIEnv* env, jobject clazz, jlong jproxy, jint joffset)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeMove()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean succeeded;
    cursor->Move((Int32)joffset, &succeeded);

    // ALOGD("- android_database_ElCursorProxy_nativeMove()");
    return (jboolean)succeeded;
}

static jint android_database_ElCursorProxy_nativeGetPosition(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetPosition()");

    ICursor* cursor = (ICursor*)jproxy;
    Int32 position;
    cursor->GetPosition(&position);

    // ALOGD("- android_database_ElCursorProxy_nativeGetPosition()");
    return (jint)position;
}

static jboolean android_database_ElCursorProxy_nativeMoveToFirst(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeMoveToFirst()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean succeeded;
    cursor->MoveToFirst(&succeeded);

    // ALOGD("- android_database_ElCursorProxy_nativeMoveToFirst()");
    return (jboolean)succeeded;
}

static jboolean android_database_ElCursorProxy_nativeMoveToLast(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeMoveToLast()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean succeeded;
    cursor->MoveToLast(&succeeded);

    // ALOGD("- android_database_ElCursorProxy_nativeMoveToLast()");
    return (jboolean)succeeded;
}

static jboolean android_database_ElCursorProxy_nativeMoveToNext(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeMoveToNext()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean succeeded;
    cursor->MoveToNext(&succeeded);

    // ALOGD("- android_database_ElCursorProxy_nativeMoveToNext()");
    return (jboolean)succeeded;
}

static jboolean android_database_ElCursorProxy_nativeMoveToPrevious(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeMoveToPrevious()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean succeeded;
    cursor->MoveToPrevious(&succeeded);

    // ALOGD("- android_database_ElCursorProxy_nativeMoveToPrevious()");
    return (jboolean)succeeded;
}

static jboolean android_database_ElCursorProxy_nativeIsFirst(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeIsFirst()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean isFirst;
    cursor->IsFirst(&isFirst);

    // ALOGD("- android_database_ElCursorProxy_nativeIsFirst()");
    return (jboolean)isFirst;
}

static jboolean android_database_ElCursorProxy_nativeIsLast(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeIsLast()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean isLast;
    cursor->IsLast(&isLast);

    // ALOGD("- android_database_ElCursorProxy_nativeIsLast()");
    return (jboolean)isLast;
}

static jboolean android_database_ElCursorProxy_nativeIsBeforeFirst(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeIsBeforeFirst()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean isLast;
    cursor->IsBeforeFirst(&isLast);

    // ALOGD("- android_database_ElCursorProxy_nativeIsBeforeFirst()");
    return (jboolean)isLast;
}

static jboolean android_database_ElCursorProxy_nativeIsAfterLast(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeIsAfterLast()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean isLast;
    cursor->IsAfterLast(&isLast);

    // ALOGD("- android_database_ElCursorProxy_nativeIsAfterLast()");
    return (jboolean)isLast;
}

static jstring android_database_ElCursorProxy_nativeGetColumnName(JNIEnv* env, jobject clazz, jlong jproxy, jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetColumnName()");

    ICursor* cursor = (ICursor*)jproxy;
    String columnName;
    cursor->GetColumnName((Int32)jcolumnIndex, &columnName);

    // ALOGD("- android_database_ElCursorProxy_nativeGetColumnName()");
    return ElUtil::GetJavaString(env, columnName);
}

static jobjectArray android_database_ElCursorProxy_nativeGetColumnNames(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetColumnNames()");

    ICursor* cursor = (ICursor*)jproxy;
    AutoPtr<ArrayOf<String> > columnNames;
    cursor->GetColumnNames((ArrayOf<String>**)&columnNames);

    // ALOGD("- android_database_ElCursorProxy_nativeGetColumnNames()");
    return ElUtil::GetJavaStringArray(env, columnNames);
}

static jint android_database_ElCursorProxy_nativeGetColumnCount(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetColumnCount()");

    ICursor* cursor = (ICursor*)jproxy;
    Int32 count;
    cursor->GetColumnCount(&count);

    // ALOGD("- android_database_ElCursorProxy_nativeGetColumnCount()");
    return (jint)count;
}

static jbyteArray android_database_ElCursorProxy_nativeGetBlob(JNIEnv* env, jobject clazz, jlong jproxy , jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetBlob()");

    ICursor* cursor = (ICursor*)jproxy;
    AutoPtr<ArrayOf<Byte> > blob;
    ECode ec = cursor->GetBlob((Int32)jcolumnIndex, (ArrayOf<Byte>**)&blob);
    if (FAILED(ec))
        ALOGE("GetBlob() ec:%0x ", ec);

    jbyteArray jblob = NULL;
    if (blob != NULL) {
        Int32 count = blob->GetLength();

        jblob = env->NewByteArray((jsize)count);
        ElUtil::CheckErrorAndLog(env, "NewByteArray: %d!\n", __LINE__);

        Byte* bytePayLoad = blob->GetPayload();

        env->SetByteArrayRegion(jblob, 0, (jint)count, (jbyte*)bytePayLoad);
    }

    // ALOGD("- android_database_ElCursorProxy_nativeGetBlob()");
    return jblob;
}

static jstring android_database_ElCursorProxy_nativeGetString(JNIEnv* env, jobject clazz, jlong jproxy , jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetString()");

    ICursor* cursor = (ICursor*)jproxy;
    String str;
    cursor->GetString((Int32)jcolumnIndex, &str);

    // ALOGD("- android_database_ElCursorProxy_nativeGetString()");
    return ElUtil::GetJavaString(env, str);
}

static jint android_database_ElCursorProxy_nativeGetInt(JNIEnv* env, jobject clazz, jlong jproxy , jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetInt()");

    ICursor* cursor = (ICursor*)jproxy;
    Int32 columnValue;
    cursor->GetInt32((Int32)jcolumnIndex, &columnValue);

    // ALOGD("- android_database_ElCursorProxy_nativeGetInt()");
    return (jint)columnValue;
}

static jint android_database_ElCursorProxy_nativeGetType(JNIEnv* env, jobject clazz, jlong jproxy , jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetType()");

    ICursor* cursor = (ICursor*)jproxy;
    Int32 columnValue;
    cursor->GetType((Int32)jcolumnIndex, &columnValue);

    // ALOGD("- android_database_ElCursorProxy_nativeGetType()");
    return (jint)columnValue;
}

static void android_database_ElCursorProxy_nativeRegisterContentObserver(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jobserver)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeRegisterContentObserver()");

    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    AutoPtr<IContentObserver> observer;
    if (jobserver != NULL) {
        jobject jInstance = env->NewGlobalRef(jobserver);
        CContentObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IContentObserver**)&observer);
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jobserver);
        Mutex::Autolock lock(sContentObserverLock);
        sContentObserver[hashCode] = observer;
    }

    ICursor* cursor = (ICursor*)jproxy;
    cursor->RegisterContentObserver(observer);

    // ALOGD("- android_database_ElCursorProxy_nativeRegisterContentObserver()");
}

static void android_database_ElCursorProxy_nativeUnregisterContentObserver(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jobserver)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeUnregisterContentObserver()");

    AutoPtr<IContentObserver> observer;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jobserver);
    {
        Mutex::Autolock lock(sContentObserverLock);
        HashMap<Int32, AutoPtr<IContentObserver> >::Iterator it = sContentObserver.Find(hashCode);
        if (it != sContentObserver.End()){
            observer = it->mSecond;
            sContentObserver.Erase(it);
        }
    }

    if (NULL == observer) {
        ALOGE("nativeUnregisterContentObserver() Invalid IContentObserver!\n");
        env->ExceptionDescribe();
        return;
    }

    ICursor* cursor = (ICursor*)jproxy;
    Int32 columnValue;
    cursor->UnregisterContentObserver(observer);

    // ALOGD("- android_database_ElCursorProxy_nativeUnregisterContentObserver()");
}

static jboolean android_database_ElCursorProxy_nativeRequery(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeRequery()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean result = FALSE;
    cursor->Requery(&result);

    // ALOGD("- android_database_ElCursorProxy_nativeRequery()");
    return (jboolean)result;
}

static jboolean android_database_ElCursorProxy_nativeIsNull(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeIsNull()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean result = FALSE;
    ECode ec = cursor->IsNull((Int32)jcolumnIndex, &result);
    if (FAILED(ec))
        ALOGE("nativeIsNull() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeIsNull()");
    return (jboolean)result;
}

static void android_database_ElCursorProxy_nativeCopyStringToBuffer(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jcolumnIndex, jobject jbuffer)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeCopyStringToBuffer()");

    AutoPtr<ICharArrayBuffer> buffer;
    if (jbuffer != NULL) {
        if (ElUtil::ToElCharArrayBuffer(env, jbuffer, (ICharArrayBuffer**)&buffer)) {
            ALOGE("nativeCopyStringToBuffer() ToElCharArrayBuffer fail!");
        }
    }

    ICursor* cursor = (ICursor*)jproxy;
    ECode ec = cursor->CopyStringToBuffer((Int32)jcolumnIndex, buffer);
    if (FAILED(ec))
        ALOGE("nativeCopyStringToBuffer() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeCopyStringToBuffer()");
}

static jshort android_database_ElCursorProxy_nativeGetShort(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetShort()");

    ICursor* cursor = (ICursor*)jproxy;
    Int16 result = 0;
    ECode ec = cursor->GetInt16((Int32)jcolumnIndex, &result);
    if (FAILED(ec))
        ALOGE("nativeGetShort() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeGetShort()");
    return (jshort)result;
}

static jfloat android_database_ElCursorProxy_nativeGetFloat(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetFloat()");

    ICursor* cursor = (ICursor*)jproxy;
    Float result = 0;
    ECode ec = cursor->GetFloat((Int32)jcolumnIndex, &result);
    if (FAILED(ec))
        ALOGE("nativeGetFloat() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeGetFloat()");
    return (jfloat)result;
}

static jdouble android_database_ElCursorProxy_nativeGetDouble(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jcolumnIndex)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetDouble()");

    ICursor* cursor = (ICursor*)jproxy;
    Double result = 0;
    ECode ec = cursor->GetDouble((Int32)jcolumnIndex, &result);
    if (FAILED(ec))
        ALOGE("nativeGetDouble() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeGetDouble()");
    return (jdouble)result;
}

static void android_database_ElCursorProxy_nativeDeactivate(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeDeactivate()");

    ICursor* cursor = (ICursor*)jproxy;
    ECode ec = cursor->Deactivate();
    if (FAILED(ec))
        ALOGE("nativeDeactivate() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeDeactivate()");
}

static void android_database_ElCursorProxy_nativeRegisterDataSetObserver(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jobserver)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeRegisterDataSetObserver()");

    AutoPtr<IDataSetObserver> observer;
    if (jobserver != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jobserver);
        CDataSetObserverNative::New((Handle64)jvm, (Handle64)jInstance, (IDataSetObserver**)&observer);
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jobserver);
        Mutex::Autolock lock(sDataSetObserverLock);
        sDataSetObserver[hashCode] = observer;
    }

    ICursor* cursor = (ICursor*)jproxy;
    ECode ec = cursor->RegisterDataSetObserver(observer);
    if (FAILED(ec))
        ALOGE("nativeRegisterDataSetObserver() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeRegisterDataSetObserver()");
}

static void android_database_ElCursorProxy_nativeUnregisterDataSetObserver(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jobserver)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeUnregisterDataSetObserver()");

    AutoPtr<IDataSetObserver> observer;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jobserver);
    {
        Mutex::Autolock lock(sDataSetObserverLock);
        HashMap<Int32, AutoPtr<IDataSetObserver> >::Iterator it = sDataSetObserver.Find(hashCode);
        if (it != sDataSetObserver.End()){
            observer = it->mSecond;
            sDataSetObserver.Erase(it);
        }
    }

    if (NULL == observer) {
        ALOGE("nativeUnregisterDataSetObserver() Invalid IDataSetObserver!\n");
        env->ExceptionDescribe();
        return;
    }

    ICursor* cursor = (ICursor*)jproxy;
    ECode ec = cursor->UnregisterDataSetObserver(observer);
    if (FAILED(ec))
        ALOGE("nativeRegisterDataSetObserver() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeUnregisterDataSetObserver()");
}

static void android_database_ElCursorProxy_nativeSetNotificationUri(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcr, jobject juri)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeSetNotificationUri()");

    AutoPtr<IContentResolver> cr;
    if (jcr != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jcr);
        CContentResolverNative::New((Handle64)jvm, (Handle64)jInstance, (IContentResolver**)&cr);
    }

    AutoPtr<IUri> uri;
    if (juri != NULL) {
        if (!ElUtil::ToElUri(env, juri, (IUri**)&uri)) {
            ALOGE("nativeSetNotificationUri() ToElUri fail!");
        }
    }

    ICursor* cursor = (ICursor*)jproxy;
    ECode ec = cursor->SetNotificationUri(cr, uri);
    if (FAILED(ec))
        ALOGE("nativeSetNotificationUri() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeSetNotificationUri()");
}

static jobject android_database_ElCursorProxy_nativeGetNotificationUri(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetNotificationUri()");

    ICursor* cursor = (ICursor*)jproxy;
    AutoPtr<IUri> uri;
    ECode ec = cursor->GetNotificationUri((IUri**)&uri);
    if (FAILED(ec))
        ALOGE("nativeGetNotificationUri() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeGetNotificationUri()");
    return ElUtil::GetJavaUri(env, uri);
}

static jboolean android_database_ElCursorProxy_nativeGetWantsAllOnMoveCalls(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetWantsAllOnMoveCalls()");

    ICursor* cursor = (ICursor*)jproxy;
    Boolean result = FALSE;
    ECode ec = cursor->GetWantsAllOnMoveCalls(&result);
    if (FAILED(ec))
        ALOGE("nativeGetWantsAllOnMoveCalls() ec: 0x%08x", ec);

    // ALOGD("- android_database_ElCursorProxy_nativeGetWantsAllOnMoveCalls()");
    return (jboolean)result;
}

static jobject android_database_ElCursorProxy_nativeGetExtras(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeGetExtras()");

    ICursor* cursor = (ICursor*)jproxy;
    AutoPtr<IBundle> extras;
    ECode ec = cursor->GetExtras((IBundle**)&extras);
    if (FAILED(ec))
        ALOGE("nativeGetExtras() ec: 0x%08x", ec);

    jobject jextras = NULL;
    if (extras != NULL) {
        jextras = ElUtil::GetJavaBundle(env, extras);
    }

    // ALOGD("- android_database_ElCursorProxy_nativeGetExtras()");
    return jextras;
}

static jobject android_database_ElCursorProxy_nativeRespond(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jextras)
{
    // ALOGD("+ android_database_ElCursorProxy_nativeRespond()");

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("nativeRespond() ToElBundle fail!");
        }
    }

    ICursor* cursor = (ICursor*)jproxy;
    AutoPtr<IBundle> result;
    ECode ec = cursor->Respond(extras, (IBundle**)&result);
    if (FAILED(ec))
        ALOGE("nativeRespond() ec: 0x%08x", ec);

    jobject jresult = NULL;
    if (result != NULL) {
        jresult = ElUtil::GetJavaBundle(env, result);
    }

    // ALOGD("- android_database_ElCursorProxy_nativeRespond()");
    return jresult;
}

static void android_content_ElIIntentSenderProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_content_ElIIntentSenderProxy_nativeDestroy()");

    ICursor* obj = (ICursor*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_content_ElIIntentSenderProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_content_ElIIntentSenderProxy_nativeFinalize },
    { "nativeGetCount",    "(J)I",
            (void*) android_database_ElCursorProxy_nativeGetCount },
    { "nativeGetColumnIndexOrThrow",    "(JLjava/lang/String;)I",
            (void*) android_database_ElCursorProxy_nativeGetColumnIndexOrThrow },
    { "nativeGetColumnIndex",    "(JLjava/lang/String;)I",
            (void*) android_database_ElCursorProxy_nativeGetColumnIndex },
    { "nativeMoveToPosition",    "(JI)Z",
            (void*) android_database_ElCursorProxy_nativeMoveToPosition },
    { "nativeGetLong",    "(JI)J",
            (void*) android_database_ElCursorProxy_nativeGetLong },
    { "nativeIsClosed",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeIsClosed },
    { "nativeClose",    "(J)V",
            (void*) android_database_ElCursorProxy_nativeClose },
    { "nativeMove",    "(JI)Z",
            (void*) android_database_ElCursorProxy_nativeMove },
    { "nativeGetPosition",    "(J)I",
            (void*) android_database_ElCursorProxy_nativeGetPosition },
    { "nativeMoveToFirst",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeMoveToFirst },
    { "nativeMoveToLast",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeMoveToLast },
    { "nativeMoveToNext",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeMoveToNext },
    { "nativeMoveToPrevious",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeMoveToPrevious },
    { "nativeIsFirst",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeIsFirst },
    { "nativeIsLast",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeIsLast },
    { "nativeIsBeforeFirst",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeIsBeforeFirst },
    { "nativeIsAfterLast",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeIsAfterLast },
    { "nativeGetColumnName",    "(JI)Ljava/lang/String;",
            (void*) android_database_ElCursorProxy_nativeGetColumnName },
    { "nativeGetColumnNames",    "(J)[Ljava/lang/String;",
            (void*) android_database_ElCursorProxy_nativeGetColumnNames },
    { "nativeGetColumnCount",    "(J)I",
            (void*) android_database_ElCursorProxy_nativeGetColumnCount },
    { "nativeGetBlob",    "(JI)[B",
            (void*) android_database_ElCursorProxy_nativeGetBlob },
    { "nativeGetString",    "(JI)Ljava/lang/String;",
            (void*) android_database_ElCursorProxy_nativeGetString },
    { "nativeGetInt",    "(JI)I",
            (void*) android_database_ElCursorProxy_nativeGetInt },
    { "nativeGetType",    "(JI)I",
            (void*) android_database_ElCursorProxy_nativeGetType },
    { "nativeRegisterContentObserver",    "(JLandroid/database/ContentObserver;)V",
            (void*) android_database_ElCursorProxy_nativeRegisterContentObserver },
    { "nativeUnregisterContentObserver",    "(JLandroid/database/ContentObserver;)V",
            (void*) android_database_ElCursorProxy_nativeUnregisterContentObserver },
    { "nativeRequery",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeRequery },
    { "nativeIsNull",    "(JI)Z",
            (void*) android_database_ElCursorProxy_nativeIsNull },
    { "nativeCopyStringToBuffer",    "(JILandroid/database/CharArrayBuffer;)V",
            (void*) android_database_ElCursorProxy_nativeCopyStringToBuffer },
    { "nativeGetShort",    "(JI)S",
            (void*) android_database_ElCursorProxy_nativeGetShort },
    { "nativeGetFloat",    "(JI)F",
            (void*) android_database_ElCursorProxy_nativeGetFloat },
    { "nativeGetDouble",    "(JI)D",
            (void*) android_database_ElCursorProxy_nativeGetDouble },
    { "nativeDeactivate",    "(J)V",
            (void*) android_database_ElCursorProxy_nativeDeactivate },
    { "nativeRegisterDataSetObserver",    "(JLandroid/database/DataSetObserver;)V",
            (void*) android_database_ElCursorProxy_nativeRegisterDataSetObserver },
    { "nativeUnregisterDataSetObserver",    "(JLandroid/database/DataSetObserver;)V",
            (void*) android_database_ElCursorProxy_nativeUnregisterDataSetObserver },
    { "nativeSetNotificationUri",    "(JLandroid/content/ContentResolver;Landroid/net/Uri;)V",
            (void*) android_database_ElCursorProxy_nativeSetNotificationUri },
    { "nativeGetNotificationUri",    "(J)Landroid/net/Uri;",
            (void*) android_database_ElCursorProxy_nativeGetNotificationUri },
    { "nativeGetWantsAllOnMoveCalls",    "(J)Z",
            (void*) android_database_ElCursorProxy_nativeGetWantsAllOnMoveCalls },
    { "nativeGetExtras",    "(J)Landroid/os/Bundle;",
            (void*) android_database_ElCursorProxy_nativeGetExtras },
    { "nativeRespond",    "(JLandroid/os/Bundle;)Landroid/os/Bundle;",
            (void*) android_database_ElCursorProxy_nativeRespond },
};

int register_android_database_ElCursorProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/database/ElCursorProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

