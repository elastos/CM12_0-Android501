
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Accounts.h>
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Accounts::IAccountManagerResponse;
using Elastos::Droid::Accounts::IAuthenticatorDescription;
using Elastos::Droid::Accounts::IIAccountManager;
using Elastos::Droid::JavaProxy::CAccountManagerResponseNative;

static jobjectArray android_accounts_ElAccountManagerProxy_nativeGetAccountsAsUser(JNIEnv* env, jobject clazz, jlong jproxy,
        jstring jaccountType, jint juserId)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetAccountsAsUser()");

    String accountType = ElUtil::ToElString(env, jaccountType);

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<ArrayOf<IAccount*> > accounts;
    acm->GetAccountsAsUser(accountType, (Int32)juserId, (ArrayOf<IAccount*>**)&accounts);
    jobjectArray jaccounts = NULL;
    if (accounts != NULL) {
        Int32 count = accounts->GetLength();

        jclass actKlass = env->FindClass("android/accounts/Account");
        ElUtil::CheckErrorAndLog(env, "FindClass: Account : %d!\n", __LINE__);

        jaccounts = env->NewObjectArray((jsize)count, actKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: Account : %d!\n", __LINE__);
        env->DeleteLocalRef(actKlass);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jaccount = ElUtil::GetJavaAccount(env, (*accounts)[i]);
            if (jaccount != NULL) {
                env->SetObjectArrayElement(jaccounts, i, jaccount);
                env->DeleteLocalRef(jaccount);
            }
        }
    }

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetAccountsAsUser()");
    return jaccounts;
}

static jboolean android_accounts_ElAccountManagerProxy_nativeAddAccountExplicitly(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jaccount, jstring jpassword, jobject jextras)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeAddAccountExplicitly()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeAddAccountExplicitly() ToElAccount fail!");
        }
    }

    String password = ElUtil::ToElString(env, jpassword);

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if(!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("nativeAddAccountExplicitly() ToElBundle fail!");
        }
    }

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    Boolean result;
    ECode ec = acm->AddAccountExplicitly(account, password, extras, &result);
    if (FAILED(ec))
        ALOGE("nativeAddAccountExplicitly() ec:%0x result: %d", ec, result);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeAddAccountExplicitly()");
    return (jboolean)result;
}

static void android_accounts_ElAccountManagerProxy_nativeSetUserData(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jaccount, jstring jkey, jstring jvalue)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeSetUserData()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeSetUserData() ToElAccount fail!");
        }
    }

    String key = ElUtil::ToElString(env, jkey);
    String value = ElUtil::ToElString(env, jvalue);

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    ECode ec = acm->SetUserData(account, key, value);
    if (FAILED(ec))
        ALOGE("nativeSetUserData() ec:%0x ", ec);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeSetUserData()");
}

static jstring android_accounts_ElAccountManagerProxy_nativeGetPassword(JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetPassword()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeSetUserData() ToElAccount fail!");
        }
    }

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    String password;
    ECode ec = acm->GetPassword(account, &password);
    if (FAILED(ec))
        ALOGE("nativeGetPassword() ec:%0x ", ec);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetPassword()");
    return ElUtil::GetJavaString(env, password);
}

static jstring android_accounts_ElAccountManagerProxy_nativeGetUserData(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccount, jstring jkey)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetUserData()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeSetUserData() ToElAccount fail!");
        }
    }

    String key = ElUtil::ToElString(env, jkey);

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    String value;
    ECode ec = acm->GetUserData(account, key, &value);
    if (FAILED(ec))
        ALOGE("nativeGetUserData() ec:%0x ", ec);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetUserData()");
    return ElUtil::GetJavaString(env, value);
}

static void android_accounts_ElAccountManagerProxy_nativeSetPassword(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccount, jstring jpassword)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeSetPassword()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeSetUserData() ToElAccount fail!");
        }
    }

    String password = ElUtil::ToElString(env, jpassword);

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    ECode ec = acm->SetPassword(account, password);
    if (FAILED(ec))
        ALOGE("nativeSetPassword() ec:%0x ", ec);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeSetPassword()");
}

static void android_accounts_ElAccountManagerProxy_nativeClearPassword(JNIEnv* env, jobject clazz, jlong jproxy, jobject jaccount)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeClearPassword()");

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeSetUserData() ToElAccount fail!");
        }
    }

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    ECode ec = acm->ClearPassword(account);
    if (FAILED(ec))
        ALOGE("nativeClearPassword() ec:%0x ", ec);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeClearPassword()");
}

static void android_accounts_ElAccountManagerProxy_nativeAddAccount(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jresponse, jstring jaccountType, jstring jauthTokenType, jobjectArray jrequiredFeatures,
        jboolean jexpectActivityLaunch, jobject joptions)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeAddAccount()");

    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeAddAccount new CAccountManagerResponseNative fail!\n");
        }
    }

    String accountType = ElUtil::ToElString(env, jaccountType);
    String authTokenType = ElUtil::ToElString(env, jauthTokenType);

    AutoPtr<ArrayOf<String> > requiredFeatures;
    if (jrequiredFeatures != NULL) {
        if (!ElUtil::ToElStringArray(env, jrequiredFeatures, (ArrayOf<String>**)&requiredFeatures)) {
            ALOGE("nativeAddAccount() ToElStringArray fail!");
        }
    } else {
        requiredFeatures = ArrayOf<String>::Alloc(0);
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if(!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeAddAccount() ToElBundle fail!");
        }
    }

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    ECode ec = acm->AddAccount(response, accountType, authTokenType, requiredFeatures,
        (Boolean)jexpectActivityLaunch, options);
    if (FAILED(ec))
        ALOGE("nativeAddAccount() ec:%0x ", ec);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeAddAccount()");
}

static void android_accounts_ElAccountManagerProxy_nativeAddAccountAsUser(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jresponse, jstring jaccountType, jstring jauthTokenType, jobjectArray jrequiredFeatures,
        jboolean jexpectActivityLaunch, jobject joptions, jint userId)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeAddAccountAsUser()");

    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeAddAccountAsUser new CAccountManagerResponseNative fail!\n");
        }
    }

    String accountType = ElUtil::ToElString(env, jaccountType);
    String authTokenType = ElUtil::ToElString(env, jauthTokenType);

    AutoPtr<ArrayOf<String> > requiredFeatures;
    if (jrequiredFeatures != NULL) {
        if (!ElUtil::ToElStringArray(env, jrequiredFeatures, (ArrayOf<String>**)&requiredFeatures)) {
            ALOGE("nativeAddAccountAsUser() ToElStringArray fail!");
        }
    } else {
        requiredFeatures = ArrayOf<String>::Alloc(0);
    }

    AutoPtr<IBundle> options;
    if (joptions != NULL) {
        if(!ElUtil::ToElBundle(env, joptions, (IBundle**)&options)) {
            ALOGE("nativeAddAccountAsUser() ToElBundle fail!");
        }
    }

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    ECode ec = acm->AddAccountAsUser(response, accountType, authTokenType, requiredFeatures,
        (Boolean)jexpectActivityLaunch, options, userId);
    if (FAILED(ec))
        ALOGE("nativeAddAccountAsUser() ec:%0x ", ec);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeAddAccountAsUser()");
}

static jobjectArray android_accounts_ElAccountManagerProxy_nativeGetAuthenticatorTypes(JNIEnv* env, jobject clazz, jlong jproxy,
    jint userId)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetAuthenticatorTypes()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;

    AutoPtr<ArrayOf<IAuthenticatorDescription*> > authTypes;
    acm->GetAuthenticatorTypes(userId, (ArrayOf<IAuthenticatorDescription*>**)&authTypes);

    jobjectArray jauthTypes = NULL;
    if (authTypes != NULL) {
        Int32 count = authTypes->GetLength();

        jclass authKlass = env->FindClass("android/accounts/AuthenticatorDescription");
        ElUtil::CheckErrorAndLog(env, "FindClass: AuthenticatorDescription : %d!\n", __LINE__);

        jauthTypes = env->NewObjectArray((jsize)count, authKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: AuthenticatorDescription : %d!\n", __LINE__);
        env->DeleteLocalRef(authKlass);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jauthType = ElUtil::GetJavaAuthenticatorDescription(env, (*authTypes)[i]);
            if (jauthType != NULL) {
                env->SetObjectArrayElement(jauthTypes, i, jauthType);
                env->DeleteLocalRef(jauthType);
            }
        }
    }

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetAuthenticatorTypes()");
    return jauthTypes;
}

static jobjectArray android_accounts_ElAccountManagerProxy_nativeGetAccounts(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jaccountType)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetAccounts()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;

    String accountType = ElUtil::ToElString(env, jaccountType);

    AutoPtr<ArrayOf<IAccount*> > accounts;
    acm->GetAccounts(accountType, (ArrayOf<IAccount*>**)&accounts);

    jobjectArray jaccounts = NULL;
    if (accounts != NULL) {
        Int32 count = accounts->GetLength();

        jclass accountKlass = env->FindClass("android/accounts/Account");
        ElUtil::CheckErrorAndLog(env, "FindClass: Account : %d!\n", __LINE__);

        jaccounts = env->NewObjectArray((jsize)count, accountKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: Account : %d!\n", __LINE__);
        env->DeleteLocalRef(accountKlass);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jaccount = ElUtil::GetJavaAccount(env, (*accounts)[i]);
            if (jaccount != NULL) {
                env->SetObjectArrayElement(jaccounts, i, jaccount);
                env->DeleteLocalRef(jaccount);
            }
        }
    }

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetAccounts()");
    return jaccounts;
}

static jobjectArray android_accounts_ElAccountManagerProxy_nativeGetAccountsForPackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint uid)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetAccountsForPackage()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<ArrayOf<IAccount*> > accounts;
    acm->GetAccountsForPackage(packageName, uid, (ArrayOf<IAccount*>**)&accounts);

    jobjectArray jaccounts = NULL;
    if (accounts != NULL) {
        Int32 count = accounts->GetLength();

        jclass accountKlass = env->FindClass("android/accounts/Account");
        ElUtil::CheckErrorAndLog(env, "FindClass: Account : %d!\n", __LINE__);

        jaccounts = env->NewObjectArray((jsize)count, accountKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: Account : %d!\n", __LINE__);
        env->DeleteLocalRef(accountKlass);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jaccount = ElUtil::GetJavaAccount(env, (*accounts)[i]);
            if (jaccount != NULL) {
                env->SetObjectArrayElement(jaccounts, i, jaccount);
                env->DeleteLocalRef(jaccount);
            }
        }
    }

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetAccountsForPackage()");
    return jaccounts;
}

static jobjectArray android_accounts_ElAccountManagerProxy_nativeGetAccountsByTypeForPackage(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jtype, jstring jpackageName)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetAccountsByTypeForPackage()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;

    String type = ElUtil::ToElString(env, jtype);
    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<ArrayOf<IAccount*> > accounts;
    acm->GetAccountsByTypeForPackage(type, packageName, (ArrayOf<IAccount*>**)&accounts);

    jobjectArray jaccounts = NULL;
    if (accounts != NULL) {
        Int32 count = accounts->GetLength();

        jclass accountKlass = env->FindClass("android/accounts/Account");
        ElUtil::CheckErrorAndLog(env, "FindClass: Account : %d!\n", __LINE__);

        jaccounts = env->NewObjectArray((jsize)count, accountKlass, NULL);
        ElUtil::CheckErrorAndLog(env, "NewObjectArray: Account : %d!\n", __LINE__);
        env->DeleteLocalRef(accountKlass);

        for(Int32 i = 0; i < count; i++ ) {
            jobject jaccount = ElUtil::GetJavaAccount(env, (*accounts)[i]);
            if (jaccount != NULL) {
                env->SetObjectArrayElement(jaccounts, i, jaccount);
                env->DeleteLocalRef(jaccount);
            }
        }
    }

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetAccountsByTypeForPackage()");
    return jaccounts;
}

static void android_accounts_ElAccountManagerProxy_nativeHasFeatures(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jresponse, jobject jaccount, jobjectArray jrequiredFeatures)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeHasFeatures()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeHasFeatures() new CAccountManagerResponseNative fail!\n");
        }
    }

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeHasFeatures() ToElAccount fail!");
        }
    }

    AutoPtr<ArrayOf<String> > requiredFeatures;
    if (jrequiredFeatures != NULL) {
        if (!ElUtil::ToElStringArray(env, jrequiredFeatures, (ArrayOf<String>**)&requiredFeatures)) {
            ALOGE("nativeHasFeatures() ToElStringArray fail!");
        }
    } else {
        requiredFeatures = ArrayOf<String>::Alloc(0);
    }

    acm->HasFeatures(response, account, requiredFeatures);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeHasFeatures()");
}

static void android_accounts_ElAccountManagerProxy_nativeGetAccountsByFeatures(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jresponse, jstring jaccountType, jobjectArray jrequiredFeatures)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetAccountsByFeatures()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeGetAccountsByFeatures() new CAccountManagerResponseNative fail!\n");
        }
    }

    String accountType = ElUtil::ToElString(env, jaccountType);

    AutoPtr<ArrayOf<String> > requiredFeatures;
    if (jrequiredFeatures != NULL) {
        if (!ElUtil::ToElStringArray(env, jrequiredFeatures, (ArrayOf<String>**)&requiredFeatures)) {
            ALOGE("nativeGetAccountsByFeatures() ToElStringArray fail!");
        }
    } else {
        requiredFeatures = ArrayOf<String>::Alloc(0);
    }

    acm->GetAccountsByFeatures(response, accountType, requiredFeatures);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetAccountsByFeatures()");
}

static void android_accounts_ElAccountManagerProxy_nativeRemoveAccount(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jresponse, jobject jaccount)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeRemoveAccount()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeRemoveAccount() new CAccountManagerResponseNative fail!\n");
        }
    }

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeRemoveAccount() ToElAccount fail!");
        }
    }

    acm->RemoveAccount(response, account);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeRemoveAccount()");
}

static void android_accounts_ElAccountManagerProxy_nativeRemoveAccountAsUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jresponse, jobject jaccount, jint userId)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeRemoveAccountAsUser()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeRemoveAccountAsUser() new CAccountManagerResponseNative fail!\n");
        }
    }

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeRemoveAccountAsUser() ToElAccount fail!");
        }
    }

    acm->RemoveAccountAsUser(response, account, userId);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeRemoveAccountAsUser()");
}

static void android_accounts_ElAccountManagerProxy_nativeInvalidateAuthToken(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jaccountType, jstring jauthToken)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeInvalidateAuthToken()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    String accountType = ElUtil::ToElString(env, jaccountType);
    String authToken = ElUtil::ToElString(env, jauthToken);

    acm->InvalidateAuthToken(accountType, authToken);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeInvalidateAuthToken()");
}

static jstring android_accounts_ElAccountManagerProxy_nativePeekAuthToken(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccount, jstring jtokenType)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativePeekAuthToken()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativePeekAuthToken() ToElAccount fail!");
        }
    }

    String tokenType = ElUtil::ToElString(env, jtokenType);

    String token;
    if (FAILED( acm->PeekAuthToken(account, tokenType, &token) ) ) {
        ALOGE("Failed acm->PeekAuthToken()");
    }

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativePeekAuthToken()");

    return ElUtil::GetJavaString(env, token);
}

static void android_accounts_ElAccountManagerProxy_nativeSetAuthToken(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccount, jstring jauthTokenType, jstring jauthToken)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeSetAuthToken()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeSetAuthToken() ToElAccount fail!");
        }
    }

    String authTokenType = ElUtil::ToElString(env, jauthTokenType);
    String authToken = ElUtil::ToElString(env, jauthToken);

    acm->SetAuthToken(account, authTokenType, authToken);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeSetAuthToken()");
}

static void android_accounts_ElAccountManagerProxy_nativeUpdateAppPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jaccount, jstring jauthTokenType, jint juid, jboolean jvalue)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeUpdateAppPermission()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeUpdateAppPermission() ToElAccount fail!");
        }
    }

    String authTokenType = ElUtil::ToElString(env, jauthTokenType);

    acm->UpdateAppPermission(account, authTokenType, juid, jvalue);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeUpdateAppPermission()");
}

static void android_accounts_ElAccountManagerProxy_nativeGetAuthToken(JNIEnv* env, jobject clazz,
    jlong jproxy, jobject jresponse, jobject jaccount, jstring jauthTokenType,
    jboolean jnotifyOnAuthFailure, jboolean jexpectActivityLaunch, jobject jbundle)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetAuthToken()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeGetAuthToken() new CAccountManagerResponseNative fail!\n");
        }
    }

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeGetAuthToken() ToElAccount fail!");
        }
    }

    String authTokenType = ElUtil::ToElString(env, jauthTokenType);
    AutoPtr<IBundle> bundle;
    if (jbundle != NULL) {
        if(!ElUtil::ToElBundle(env, jbundle, (IBundle**)&bundle)) {
            ALOGE("nativeGetAuthToken() ToElBundle fail!");
        }
    }

    acm->GetAuthToken(response, account, authTokenType,
        jnotifyOnAuthFailure, jexpectActivityLaunch, bundle);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetAuthToken()");
}

static void android_accounts_ElAccountManagerProxy_nativeUpdateCredentials(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jresponse, jobject jaccount, jstring jauthTokenType, jboolean jexpectActivityLaunch, jobject jbundle)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeUpdateCredentials()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeUpdateCredentials() new CAccountManagerResponseNative fail!\n");
        }
    }

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeUpdateCredentials() ToElAccount fail!");
        }
    }

    String authTokenType = ElUtil::ToElString(env, jauthTokenType);
    AutoPtr<IBundle> bundle;
    if (jbundle != NULL) {
        if(!ElUtil::ToElBundle(env, jbundle, (IBundle**)&bundle)) {
            ALOGE("nativeUpdateCredentials() ToElBundle fail!");
        }
    }

    acm->UpdateCredentials(response, account, authTokenType, jexpectActivityLaunch, bundle);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeUpdateCredentials()");
}

static void android_accounts_ElAccountManagerProxy_nativeEditProperties(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jresponse, jstring jaccountType, jboolean jexpectActivityLaunch)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeEditProperties()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeEditProperties() new CAccountManagerResponseNative fail!\n");
        }
    }

    String accountType = ElUtil::ToElString(env, jaccountType);

    acm->EditProperties(response, accountType, jexpectActivityLaunch);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeEditProperties()");
}

static void android_accounts_ElAccountManagerProxy_nativeConfirmCredentialsAsUser(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jresponse, jobject jaccount, jobject jbundle, jboolean expectActivityLaunch, jint userId)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeConfirmCredentialsAsUser()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeConfirmCredentialsAsUser() new CAccountManagerResponseNative fail!\n");
        }
    }

    AutoPtr<IAccount> account;
    if (jaccount != NULL) {
        if(!ElUtil::ToElAccount(env, jaccount, (IAccount**)&account)) {
            ALOGE("nativeConfirmCredentialsAsUser() ToElAccount fail!");
        }
    }

    AutoPtr<IBundle> bundle;
    if (jbundle != NULL) {
        if(!ElUtil::ToElBundle(env, jbundle, (IBundle**)&bundle)) {
            ALOGE("nativeConfirmCredentialsAsUser() ToElBundle fail!");
        }
    }

    acm->ConfirmCredentialsAsUser(response, account, bundle, expectActivityLaunch, userId);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeConfirmCredentialsAsUser()");
}

static void android_accounts_ElAccountManagerProxy_nativeGetAuthTokenLabel(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jresponse, jstring jaccountType, jstring jauthTokenType)
{
    // ALOGD("+ android_accounts_ElAccountManagerProxy_nativeGetAuthTokenLabel()");

    IIAccountManager* acm = (IIAccountManager*)jproxy;
    AutoPtr<IAccountManagerResponse> response;
    if (jresponse != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jresponse);

        if(NOERROR != CAccountManagerResponseNative::New((Handle64)jvm, (Handle64)jInstance, (IAccountManagerResponse**)&response)) {
            ALOGE("nativeGetAuthTokenLabel() new CAccountManagerResponseNative fail!\n");
        }
    }

    String accountType = ElUtil::ToElString(env, jaccountType);
    String authTokenType = ElUtil::ToElString(env, jauthTokenType);

    acm->GetAuthTokenLabel(response, accountType, authTokenType);

    // ALOGD("- android_accounts_ElAccountManagerProxy_nativeGetAuthTokenLabel()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeGetAccountsAsUser",    "(JLjava/lang/String;I)[Landroid/accounts/Account;",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetAccountsAsUser },
    { "nativeAddAccountExplicitly",    "(JLandroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z",
            (void*) android_accounts_ElAccountManagerProxy_nativeAddAccountExplicitly },
    { "nativeSetUserData",    "(JLandroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeSetUserData },
    { "nativeGetPassword",    "(JLandroid/accounts/Account;)Ljava/lang/String;",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetPassword },
    { "nativeGetUserData",    "(JLandroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetUserData },
    { "nativeSetPassword",    "(JLandroid/accounts/Account;Ljava/lang/String;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeSetPassword },
    { "nativeClearPassword",    "(JLandroid/accounts/Account;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeClearPassword },
    { "nativeAddAccount",    "(JLandroid/accounts/IAccountManagerResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLandroid/os/Bundle;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeAddAccount },
    { "nativeAddAccountAsUser",    "(JLandroid/accounts/IAccountManagerResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLandroid/os/Bundle;I)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeAddAccountAsUser },
    { "nativeGetAuthenticatorTypes",    "(JI)[Landroid/accounts/AuthenticatorDescription;",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetAuthenticatorTypes },
    { "nativeGetAccounts",    "(JLjava/lang/String;)[Landroid/accounts/Account;",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetAccounts },
    { "nativeGetAccountsForPackage",    "(JLjava/lang/String;I)[Landroid/accounts/Account;",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetAccountsForPackage },
    { "nativeGetAccountsByTypeForPackage",    "(JLjava/lang/String;Ljava/lang/String;)[Landroid/accounts/Account;",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetAccountsByTypeForPackage },

    { "nativeHasFeatures",   "(JLandroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;[Ljava/lang/String;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeHasFeatures },

    { "nativeGetAccountsByFeatures",   "(JLandroid/accounts/IAccountManagerResponse;Ljava/lang/String;[Ljava/lang/String;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetAccountsByFeatures },

    { "nativeRemoveAccount",   "(JLandroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeRemoveAccount },

    { "nativeRemoveAccountAsUser",   "(JLandroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;I)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeRemoveAccountAsUser },

    { "nativeInvalidateAuthToken",   "(JLjava/lang/String;Ljava/lang/String;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeInvalidateAuthToken },

    { "nativePeekAuthToken",   "(JLandroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;",
            (void*) android_accounts_ElAccountManagerProxy_nativePeekAuthToken },

    { "nativeSetAuthToken",   "(JLandroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeSetAuthToken },

    { "nativeUpdateAppPermission",   "(JLandroid/accounts/Account;Ljava/lang/String;IZ)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeUpdateAppPermission },

    { "nativeGetAuthToken",   "(JLandroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Ljava/lang/String;ZZLandroid/os/Bundle;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetAuthToken },

    { "nativeUpdateCredentials",   "(JLandroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Ljava/lang/String;ZLandroid/os/Bundle;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeUpdateCredentials },

    { "nativeEditProperties",   "(JLandroid/accounts/IAccountManagerResponse;Ljava/lang/String;Z)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeEditProperties },

    { "nativeConfirmCredentialsAsUser",   "(JLandroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Landroid/os/Bundle;ZI)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeConfirmCredentialsAsUser },

    { "nativeGetAuthTokenLabel",   "(JLandroid/accounts/IAccountManagerResponse;Ljava/lang/String;Ljava/lang/String;)V",
            (void*) android_accounts_ElAccountManagerProxy_nativeGetAuthTokenLabel }
};

int register_android_accounts_ElAccountManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/accounts/ElAccountManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

