
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Net.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.CoreLibrary.Utility.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::App::Admin::IIDevicePolicyManager;

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativePackageHasActiveAdmins(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint juserHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativePackageHasActiveAdmins()");

    if (jproxy == 0) {
        ALOGE("TODO: need CDevicePolicyManagerService");
        return FALSE;
    }
    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;

    Boolean result = FALSE;
    ECode ec = dpm->PackageHasActiveAdmins(packageName, (Int32)juserHandle, &result);
    if (FAILED(ec)) {
        ALOGE("PackageHasActiveAdmins() return FAILED");
    }


    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativePackageHasActiveAdmins()");
    return (jboolean)result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordQuality(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint quality, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordQuality()");

    if (jproxy == 0) {
        ALOGE("TODO: need CDevicePolicyManagerService");
        return ;
    }
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordQuality(who, quality, userHandle);
        if (FAILED(ec)) {
            ALOGE("SetPasswordQuality() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordQuality()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordQuality(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordQuality()");

    Int32 quality;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordQuality(who, userHandle, &quality);
        if (FAILED(ec)) {
            ALOGE("GetPasswordQuality() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordQuality()");
    return quality;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLength(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint length, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLength()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordMinimumLength(who, length, userHandle);
        if (FAILED(ec)) {
            ALOGE("SetPasswordMinimumLength() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLength()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLength(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLength()");

    Int32 length;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordMinimumLength(who, userHandle, &length);
        if (FAILED(ec)) {
            ALOGE("GetPasswordMinimumLength() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLength()");
    return length;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumUpperCase(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint length, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumUpperCase()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordMinimumUpperCase(who, length, userHandle);
        if (FAILED(ec)) {
            ALOGE("SetPasswordMinimumUpperCase() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumUpperCase()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumUpperCase(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumUpperCase()");

    Int32 length;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordMinimumUpperCase(who, userHandle, &length);
        if (FAILED(ec)) {
            ALOGE("GetPasswordMinimumUpperCase() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumUpperCase()");
    return length;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLowerCase(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint length, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLowerCase()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordMinimumLowerCase(who, length, userHandle);
        if (FAILED(ec)) {
            ALOGE("SetPasswordMinimumLowerCase() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLowerCase()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLowerCase(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLowerCase()");

    Int32 length;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordMinimumLowerCase(who, userHandle, &length);
        if (FAILED(ec)) {
            ALOGE("GetPasswordMinimumLowerCase() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLowerCase()");
    return length;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLetters(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint length, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLetters()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordMinimumLetters(who, length, userHandle);
        if (FAILED(ec)) {
            ALOGE("SetPasswordMinimumLetters() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLetters()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLetters(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLetters()");

    Int32 length;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordMinimumLetters(who, userHandle, &length);
        if (FAILED(ec)) {
            ALOGE("GetPasswordMinimumLetters() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLetters");
    return length;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumNumeric(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint length, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumNumeric()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordMinimumNumeric(who, length, userHandle);
        if (FAILED(ec)) {
            ALOGE("SetPasswordMinimumNumeric() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumNumeric()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNumeric(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNumeric()");

    Int32 length;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordMinimumNumeric(who, userHandle, &length);
        if (FAILED(ec)) {
            ALOGE("GetPasswordMinimumNumeric() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNumeric");
    return length;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumSymbols(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint length, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumSymbols()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordMinimumSymbols(who, length, userHandle);
        if (FAILED(ec)) {
            ALOGE("SetPasswordMinimumSymbols() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumSymbols()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumSymbols(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNumeric()");

    Int32 length;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordMinimumSymbols(who, userHandle, &length);
        if (FAILED(ec)) {
            ALOGE("GetPasswordMinimumSymbols() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumSymbols");
    return length;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumNonLetter(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint length, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumNonLetter()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordMinimumNonLetter(who, length, userHandle);
        if (FAILED(ec)) {
            ALOGE("SetPasswordMinimumNonLetter() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumNonLetter()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNonLetter(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNonLetter()");

    Int32 length;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordMinimumNonLetter(who, userHandle, &length);
        if (FAILED(ec)) {
            ALOGE("GetPasswordMinimumNonLetter() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNonLetter");
    return length;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordHistoryLength(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint length, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordHistoryLength()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordHistoryLength(who, length, userHandle);
        if (FAILED(ec)) {
            ALOGE("nativeSetPasswordHistoryLength() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordHistoryLength()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordHistoryLength(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNonLetter()");

    Int32 length;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordHistoryLength(who, userHandle, &length);
        if (FAILED(ec)) {
            ALOGE("GetPasswordHistoryLength() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordHistoryLength");
    return length;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordExpirationTimeout(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jlong expiration, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordExpirationTimeout()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetPasswordExpirationTimeout(who, expiration, userHandle);
        if (FAILED(ec)) {
            ALOGE("nativeSetPasswordExpirationTimeout() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordExpirationTimeout()");
}

static jlong android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordExpirationTimeout(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordExpirationTimeout()");

    Int64 expiration;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordExpirationTimeout(who, userHandle, &expiration);
        if (FAILED(ec)) {
            ALOGE("nativeGetPasswordExpirationTimeout() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordExpirationTimeout");
    return expiration;
}

static jlong android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordExpiration(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordExpiration()");

    Int64 expiration;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetPasswordExpiration(who, userHandle, &expiration);
        if (FAILED(ec)) {
            ALOGE("nativeGetPasswordExpiration() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordExpiration");
    return expiration;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeIsActivePasswordSufficient(JNIEnv* env, jobject clazz, jlong jproxy,
    jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeIsActivePasswordSufficient()");

    Boolean flag;
    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->IsActivePasswordSufficient(userHandle, &flag);
    if (FAILED(ec)) {
        ALOGE("IsActivePasswordSufficient() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeIsActivePasswordSufficient");
    return flag;
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetCurrentFailedPasswordAttempts(JNIEnv* env, jobject clazz, jlong jproxy,
    jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetCurrentFailedPasswordAttempts()");

    Int32 attempts;
    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->GetCurrentFailedPasswordAttempts(userHandle, &attempts);
    if (FAILED(ec)) {
        ALOGE("GetCurrentFailedPasswordAttempts() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetCurrentFailedPasswordAttempts");
    return attempts;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetMaximumFailedPasswordsForWipe(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint num, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetMaximumFailedPasswordsForWipe()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetMaximumFailedPasswordsForWipe(who, num, userHandle);
        if (FAILED(ec)) {
            ALOGE("SetMaximumFailedPasswordsForWipe() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetMaximumFailedPasswordsForWipe()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetMaximumFailedPasswordsForWipe(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetMaximumFailedPasswordsForWipe()");

    Int32 expiration;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetMaximumFailedPasswordsForWipe(who, userHandle, &expiration);
        if (FAILED(ec)) {
            ALOGE("GetMaximumFailedPasswordsForWipe() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetMaximumFailedPasswordsForWipe");
    return expiration;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeResetPassword(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpassword, jint flags, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetMaximumFailedPasswordsForWipe()");

    Boolean result = FALSE;
    String password;
    password = ElUtil::ToElString(env, jpassword);
    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->ResetPassword(password, flags, userHandle, &result);
    if (FAILED(ec)) {
        ALOGE("nativeResetPassword() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeResetPassword");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetMaximumTimeToLock(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jlong timeMS, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetMaximumTimeToLock()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetMaximumTimeToLock(who, timeMS, userHandle);
        if (FAILED(ec)) {
            ALOGE("nativeSetMaximumTimeToLock() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetMaximumTimeToLock");
}

static jlong android_app_admin_ElDevicePolicyManagerProxy_nativeGetMaximumTimeToLock(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetMaximumTimeToLock()");

    Int64 time;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetMaximumTimeToLock(who, userHandle, &time);
        if (FAILED(ec)) {
            ALOGE("GetMaximumTimeToLock() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetMaximumTimeToLock");
    return time;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeLockNow(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeLockNow()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->LockNow();
    if (FAILED(ec)) {
        ALOGE("LockNow() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeLockNow");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeWipeData(JNIEnv* env, jobject clazz, jlong jproxy,
    jint flag, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeWipeData()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->WipeData(flag, userHandle);
    if (FAILED(ec)) {
        ALOGE("nativeWipeData() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeWipeData");
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeSetGlobalProxy(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jstring jproxySpec, jstring jexclusionList, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetGlobalProxy()");

    AutoPtr<IComponentName> cmpntName;

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        String proxySpec = ElUtil::ToElString(env, jproxySpec);
        String exclusionList = ElUtil::ToElString(env, jexclusionList);
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetGlobalProxy(who, proxySpec, exclusionList, userHandle, (IComponentName**)&cmpntName);
        if (FAILED(ec)) {
            ALOGE("SetGlobalProxy() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetGlobalProxy");
    return ElUtil::GetJavaComponentName(env, cmpntName);
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetGlobalProxyAdmin(JNIEnv* env, jobject clazz, jlong jproxy,
        jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetGlobalProxyAdmin()");

    AutoPtr<IComponentName> cmpntName;
    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->GetGlobalProxyAdmin(userHandle, (IComponentName**)&cmpntName);
    if (FAILED(ec)) {
        ALOGE("GetGlobalProxyAdmin() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetGlobalProxyAdmin");
    return ElUtil::GetJavaComponentName(env, cmpntName);
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetRecommendedGlobalProxy(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jadmin, jobject jproxyInfo)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetRecommendedGlobalProxy()");

    AutoPtr<IComponentName> admin;
    if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin) ) {
        ALOGE("nativeSetRecommendedGlobalProxy ToElComponentName() Failed");
    }

    AutoPtr<IProxyInfo> proxyInfo;
    if (jproxyInfo) {
        if (!ElUtil::ToElProxyInfo(env, jproxyInfo, (IProxyInfo**)&proxyInfo) ) {
            ALOGE("nativeSetRecommendedGlobalProxy ToElProxyInfo() Failed");
        }
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetRecommendedGlobalProxy(admin, proxyInfo);
    if (FAILED(ec)) {
        ALOGE("SetRecommendedGlobalProxy() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetRecommendedGlobalProxy");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeSetStorageEncryption(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jwho, jboolean encrypt, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetStorageEncryption()");

    Int32 result;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetStorageEncryption(who, encrypt, userHandle, &result);
        if (FAILED(ec)) {
            ALOGE("SetStorageEncryption() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetStorageEncryption");
    return result;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeGetStorageEncryption(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetStorageEncryption()");

    Boolean result;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetStorageEncryption(who, userHandle, &result);
        if (FAILED(ec)) {
            ALOGE("GetStorageEncryption() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetStorageEncryption");
    return result;
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetStorageEncryptionStatus(JNIEnv* env, jobject clazz, jlong jproxy,
        jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetStorageEncryptionStatus()");

    Int32 result;
    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->GetStorageEncryptionStatus(userHandle, &result);
    if (FAILED(ec)) {
        ALOGE("GetStorageEncryptionStatus() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetStorageEncryptionStatus");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetCameraDisabled(JNIEnv* env, jobject clazz, jlong jproxy,
       jobject jwho, jboolean disabled, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetCameraDisabled()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetCameraDisabled(who, disabled, userHandle);
        if (FAILED(ec)) {
           ALOGE("SetCameraDisabled() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetCameraDisabled");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeGetCameraDisabled(JNIEnv* env, jobject clazz, jlong jproxy,
       jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetCameraDisabled()");

    Boolean disabled = FALSE;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetCameraDisabled(who, userHandle, &disabled);
        if (FAILED(ec)) {
           ALOGE("GetCameraDisabled() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetCameraDisabled");
    return disabled;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetScreenCaptureDisabled(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jwho, jint userHandle, jboolean disabled)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetScreenCaptureDisabled()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetScreenCaptureDisabled(who, userHandle, disabled);
        if (FAILED(ec)) {
           ALOGE("SetScreenCaptureDisabled() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetScreenCaptureDisabled");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeGetScreenCaptureDisabled(JNIEnv* env, jobject clazz, jlong jproxy,
       jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetScreenCaptureDisabled()");

    Boolean disabled = FALSE;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetScreenCaptureDisabled(who, userHandle, &disabled);
        if (FAILED(ec)) {
           ALOGE("GetScreenCaptureDisabled() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetScreenCaptureDisabled");
    return disabled;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetKeyguardDisabledFeatures(JNIEnv* env, jobject clazz, jlong jproxy,
       jobject jwho, jint which, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetKeyguardDisabledFeatures()");

    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetKeyguardDisabledFeatures(who, which, userHandle);
        if (FAILED(ec)) {
           ALOGE("SetKeyguardDisabledFeatures() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetKeyguardDisabledFeatures");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeGetKeyguardDisabledFeatures(JNIEnv* env, jobject clazz, jlong jproxy,
       jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetKeyguardDisabledFeatures()");

    Int32 which;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->GetKeyguardDisabledFeatures(who, userHandle, &which);
        if (FAILED(ec)) {
           ALOGE("GetKeyguardDisabledFeatures() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetKeyguardDisabledFeatures");

    return which;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetActiveAdmin(JNIEnv* env, jobject clazz, jlong jproxy,
       jobject jwho, jboolean refreshing, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetActiveAdmin()");

    Int32 which;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->SetActiveAdmin(who, refreshing, userHandle);
        if (FAILED(ec)) {
           ALOGE("SetActiveAdmin() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetActiveAdmin");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeIsAdminActive(JNIEnv* env, jobject clazz, jlong jproxy,
       jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeIsAdminActive()");

    Boolean result;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->IsAdminActive(who, userHandle, &result);
        if (FAILED(ec)) {
           ALOGE("IsAdminActive() return FAILED");
        }
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeIsAdminActive");

    return result;
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetActiveAdmins(JNIEnv* env, jobject clazz, jlong jproxy,
        jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetActiveAdmins()");

    AutoPtr<IList> admins;
    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->GetActiveAdmins(userHandle, (IList**)&admins);
    if (FAILED(ec)) {
        ALOGE("GetActiveAdmins() return FAILED");
        return NULL;
    }

    jclass arrayListCls = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID constructor = env->GetMethodID(arrayListCls, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetActiveAdmins Failed GetMethodID <init>: %d!\n", __LINE__);

    jmethodID mid_add = env->GetMethodID(arrayListCls, "add", "(Ljava/lang/Object;)Z");
    ElUtil::CheckErrorAndLog(env, "nativeGetActiveAdmins Failed GetMethodID add : %d!\n", __LINE__);

    jobject arrayList = env->NewObject(arrayListCls, constructor);
    ElUtil::CheckErrorAndLog(env, "nativeGetActiveAdmins Failed NewObject : %d!\n", __LINE__);
    env->DeleteLocalRef(arrayListCls);

    if (NULL != admins) {
        Int32 size;
        admins->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> key;
            admins->Get(i, (IInterface**)&key);
            AutoPtr<IComponentName> cmpnt = IComponentName::Probe(key);
            if (cmpnt != NULL) {
                jobject jcmpnt = ElUtil::GetJavaComponentName(env, cmpnt);
                env->CallBooleanMethod(arrayList, mid_add, jcmpnt);
                env->DeleteLocalRef(jcmpnt);
            }
        }

    }
    env->DeleteLocalRef(arrayListCls);

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetActiveAdmins");

    return arrayList;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeGetRemoveWarning(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jwho, jobject /* RemoteCallback */ result, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_naitveGetRemoveWarning()");

    ALOGD("nativeGetRemoveWarning Not Implemented ...");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_naitveGetRemoveWarning()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveActiveAdmin(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jwho, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveActiveAdmin()");
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->RemoveActiveAdmin(who, userHandle);
        if (FAILED(ec)) {
           ALOGE("RemoveActiveAdmin() return FAILED");
        }
    }
    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveActiveAdmin()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeHasGrantedPolicy(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jwho, jint usesPolicy, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeHasGrantedPolicy()");

    Boolean ret;
    AutoPtr<IComponentName> who;
    if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who) ) {
        ALOGE("ToElComponentName() Failed");
    }
    else {
        IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
        ECode ec = dpm->HasGrantedPolicy(who, usesPolicy, userHandle, &ret);
        if (FAILED(ec)) {
           ALOGE("HasGrantedPolicy() return FAILED");
        }
    }
    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeHasGrantedPolicy()");

    return ret;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetActivePasswordState(JNIEnv* env, jobject clazz, jlong jproxy, jint quality,
    jint length, jint letters, jint uppercase, jint lowercase, jint numbers, jint symbols, jint nonletter, jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetActivePasswordState()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetActivePasswordState(quality, length, letters, uppercase, lowercase, numbers, symbols, nonletter, userHandle);
    if (FAILED(ec)) {
        ALOGE("SetActivePasswordState() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetActivePasswordState()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeReportFailedPasswordAttempt(JNIEnv* env, jobject clazz, jlong jproxy, jint quality,
    jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeReportFailedPasswordAttempt()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->ReportFailedPasswordAttempt(userHandle);
    if (FAILED(ec)) {
        ALOGE("ReportFailedPasswordAttempt() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeReportFailedPasswordAttempt()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeReportSuccessfulPasswordAttempt(JNIEnv* env, jobject clazz, jlong jproxy, jint quality,
    jint userHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeReportSuccessfulPasswordAttempt()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->ReportSuccessfulPasswordAttempt(userHandle);
    if (FAILED(ec)) {
        ALOGE("ReportSuccessfulPasswordAttempt() return FAILED");
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeReportSuccessfulPasswordAttempt()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeSetDeviceOwner(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jstring jownerName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetDeviceOwner()");

    String packageName = ElUtil::ToElString(env, jpackageName);
    String ownerName = ElUtil::ToElString(env, jownerName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->SetDeviceOwner(packageName, ownerName, &result);
    if (FAILED(ec))
        ALOGE("SetDeviceOwner() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetDeviceOwner()");
    return result;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeIsDeviceOwner(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeIsDeviceOwner()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->IsDeviceOwner(packageName, &result);
    if (FAILED(ec))
        ALOGE("IsDeviceOwner() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeIsDeviceOwner()");
    return result;
}

static jstring android_app_admin_ElDevicePolicyManagerProxy_nativeGetDeviceOwner(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetDeviceOwner()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    String result;
    ECode ec = dpm->GetDeviceOwner(&result);
    if (FAILED(ec))
        ALOGE("GetDeviceOwner() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetDeviceOwner()");
    return ElUtil::GetJavaString(env, result);
}

static jstring android_app_admin_ElDevicePolicyManagerProxy_nativeGetDeviceOwnerName(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetDeviceOwnerName()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    String result;
    ECode ec = dpm->GetDeviceOwnerName(&result);
    if (FAILED(ec))
        ALOGE("GetDeviceOwnerName() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetDeviceOwnerName()");
    return ElUtil::GetJavaString(env, result);
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeClearDeviceOwner(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeClearDeviceOwner()");

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->ClearDeviceOwner(packageName);
    if (FAILED(ec))
        ALOGE("ClearDeviceOwner() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeClearDeviceOwner()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileOwner(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jownerName, jint juserHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileOwner()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetProfileOwner: jwho is NULL!");
    }

    String ownerName = ElUtil::ToElString(env, jownerName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->SetProfileOwner(who, ownerName, juserHandle, &result);
    if (FAILED(ec))
        ALOGE("SetProfileOwner() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileOwner()");
    return result;
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetProfileOwner(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juserHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetProfileOwner()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IComponentName> owner;
    ECode ec = dpm->GetProfileOwner(juserHandle, (IComponentName**)&owner);
    if (FAILED(ec))
        ALOGE("GetProfileOwner() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetProfileOwner()");
    return ElUtil::GetJavaComponentName(env, owner);
}

static jstring android_app_admin_ElDevicePolicyManagerProxy_nativeGetProfileOwnerName(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juserHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetProfileOwnerName()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    String result;
    ECode ec = dpm->GetProfileOwnerName(juserHandle, &result);
    if (FAILED(ec))
        ALOGE("GetProfileOwnerName() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetProfileOwnerName()");
    return ElUtil::GetJavaString(env, result);
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileEnabled(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileEnabled()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetProfileEnabled: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetProfileEnabled: jwho is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetProfileEnabled(who);
    if (FAILED(ec))
        ALOGE("SetProfileEnabled() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileEnabled()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileName(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jprofileName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileName()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetProfileName: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetProfileName: jwho is NULL!");
    }

    String profileName = ElUtil::ToElString(env, jprofileName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetProfileName(who, profileName);
    if (FAILED(ec))
        ALOGE("SetProfileName() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileName()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeClearProfileOwner(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeClearProfileOwner()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jwho is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->ClearProfileOwner(who);
    if (FAILED(ec))
        ALOGE("ClearProfileOwner() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeClearProfileOwner()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeHasUserSetupCompleted(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeHasUserSetupCompleted()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->HasUserSetupCompleted(&result);
    if (FAILED(ec))
        ALOGE("HasUserSetupCompleted() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeHasUserSetupCompleted()");
    return result;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeInstallCaCert(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jbyteArray jcertBuffer)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeInstallCaCert()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    AutoPtr<ArrayOf<Byte> > certBuffer;
    if (jcertBuffer != NULL)
        ElUtil::ToElByteArray(env, jcertBuffer, (ArrayOf<Byte>**)&certBuffer);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->InstallCaCert(admin, certBuffer, &result);
    if (FAILED(ec))
        ALOGE("InstallCaCert() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeInstallCaCert()");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeUninstallCaCert(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jstring jalias)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeUninstallCaCert()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    String alias = ElUtil::ToElString(env, jalias);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->UninstallCaCert(admin, alias);
    if (FAILED(ec))
        ALOGE("UninstallCaCert() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeUninstallCaCert()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeEnforceCanManageCaCerts(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeEnforceCanManageCaCerts()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->EnforceCanManageCaCerts(admin);
    if (FAILED(ec))
        ALOGE("EnforceCanManageCaCerts() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeEnforceCanManageCaCerts()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeInstallKeyPair(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jbyteArray jprivKeyBuffer, jbyteArray jcertBuffer, jstring jalias)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeInstallKeyPair()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeInstallKeyPair: ToElComponentName fail");
    }
    else {
        ALOGE("nativeInstallKeyPair: jwho is NULL!");
    }

    AutoPtr<ArrayOf<Byte> > privKeyBuffer;
    if (jprivKeyBuffer != NULL)
        ElUtil::ToElByteArray(env, jprivKeyBuffer, (ArrayOf<Byte>**)&privKeyBuffer);

    AutoPtr<ArrayOf<Byte> > certBuffer;
    if (jcertBuffer != NULL)
        ElUtil::ToElByteArray(env, jcertBuffer, (ArrayOf<Byte>**)&certBuffer);

    String alias = ElUtil::ToElString(env, jalias);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->InstallKeyPair(who, privKeyBuffer, certBuffer, alias, &result);
    if (FAILED(ec))
        ALOGE("InstallKeyPair() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeInstallKeyPair()");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeAddPersistentPreferredActivity(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jobject jfilter, jobject jactivity)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeAddPersistentPreferredActivity()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    AutoPtr<IIntentFilter> filter;
    if (jfilter != NULL) {
        if (!ElUtil::ToElIntentFilter(env, jfilter, (IIntentFilter**)&filter))
            ALOGE("nativeClearProfileOwner: ToElIntentFilter fail");
    }

    AutoPtr<IComponentName> activity;
    if (jactivity != NULL) {
        if (!ElUtil::ToElComponentName(env, jactivity, (IComponentName**)&activity))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->AddPersistentPreferredActivity(admin, filter, activity);
    if (FAILED(ec))
        ALOGE("AddPersistentPreferredActivity() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeAddPersistentPreferredActivity()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeClearPackagePersistentPreferredActivities(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jstring jpackageName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeClearPackagePersistentPreferredActivities()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->ClearPackagePersistentPreferredActivities(admin, packageName);
    if (FAILED(ec))
        ALOGE("ClearPackagePersistentPreferredActivities() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeClearPackagePersistentPreferredActivities()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetApplicationRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jpackageName, jobject jsettings)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetApplicationRestrictions()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetApplicationRestrictions: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetApplicationRestrictions: jwho is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    AutoPtr<IBundle> settings;
    if (jsettings != NULL) {
        if (!ElUtil::ToElBundle(env, jsettings, (IBundle**)&settings))
            ALOGE("nativeSetApplicationRestrictions: ToElBundle fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetApplicationRestrictions(who, packageName, settings);
    if (FAILED(ec))
        ALOGE("SetApplicationRestrictions() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetApplicationRestrictions()");
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetApplicationRestrictions(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jpackageName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetApplicationRestrictions()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeGetApplicationRestrictions: ToElComponentName fail");
    }
    else {
        ALOGE("nativeGetApplicationRestrictions: jwho is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IBundle> bundle;
    ECode ec = dpm->GetApplicationRestrictions(who, packageName, (IBundle**)&bundle);
    if (FAILED(ec))
        ALOGE("GetApplicationRestrictions() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetApplicationRestrictions()");
    return ElUtil::GetJavaBundle(env, bundle);
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetRestrictionsProvider(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jobject jprovider)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetRestrictionsProvider()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetRestrictionsProvider: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetRestrictionsProvider: jwho is NULL!");
    }

    AutoPtr<IComponentName> provider;
    if (jprovider != NULL) {
        if (!ElUtil::ToElComponentName(env, jprovider, (IComponentName**)&provider))
            ALOGE("nativeSetRestrictionsProvider: ToElComponentName fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetRestrictionsProvider(who, provider);
    if (FAILED(ec))
        ALOGE("SetRestrictionsProvider() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetRestrictionsProvider()");
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetRestrictionsProvider(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juserHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetRestrictionsProvider()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IComponentName> provider;
    ECode ec = dpm->GetRestrictionsProvider(juserHandle, (IComponentName**)&provider);
    if (FAILED(ec))
        ALOGE("GetRestrictionsProvider() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetRestrictionsProvider()");
    return ElUtil::GetJavaComponentName(env, provider);
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetUserRestriction(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jkey, jboolean jenable)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetUserRestriction()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetUserRestriction: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetUserRestriction: jwho is NULL!");
    }

    String key = ElUtil::ToElString(env, jkey);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetUserRestriction(who, key, jenable);
    if (FAILED(ec))
        ALOGE("SetUserRestriction() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetUserRestriction()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeAddCrossProfileIntentFilter(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jobject jfilter, jint jflags)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeAddCrossProfileIntentFilter()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    AutoPtr<IIntentFilter> filter;
    if (jfilter != NULL) {
        if (!ElUtil::ToElIntentFilter(env, jfilter, (IIntentFilter**)&filter))
            ALOGE("nativeClearProfileOwner: ToElIntentFilter fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->AddCrossProfileIntentFilter(admin, filter, jflags);
    if (FAILED(ec))
        ALOGE("AddCrossProfileIntentFilter() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeAddCrossProfileIntentFilter()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeClearCrossProfileIntentFilters(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeClearCrossProfileIntentFilters()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->ClearCrossProfileIntentFilters(admin);
    if (FAILED(ec))
        ALOGE("ClearCrossProfileIntentFilters() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeClearCrossProfileIntentFilters()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeSetPermittedAccessibilityServices(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jobject jpackageList)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPermittedAccessibilityServices()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    AutoPtr<IList> packageList;
    if (jpackageList != NULL) {
        if (!ElUtil::ToElStringList(env, jpackageList, (IList**)&packageList))
            ALOGE("nativeClearProfileOwner: ToElStringList fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->SetPermittedAccessibilityServices(admin, packageList, &result);
    if (FAILED(ec))
        ALOGE("SetPermittedAccessibilityServices() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPermittedAccessibilityServices()");
    return result;
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedAccessibilityServices(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedAccessibilityServices()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = dpm->GetPermittedAccessibilityServices(admin, (IList**)&list);
    if (FAILED(ec))
        ALOGE("GetPermittedAccessibilityServices() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedAccessibilityServices()");
    return ElUtil::GetJavaStringList(env, list);
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedAccessibilityServicesForUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juserId)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedAccessibilityServicesForUser()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = dpm->GetPermittedAccessibilityServicesForUser(juserId, (IList**)&list);
    if (FAILED(ec))
        ALOGE("GetPermittedAccessibilityServicesForUser() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedAccessibilityServicesForUser()");
    return ElUtil::GetJavaStringList(env, list);
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeSetPermittedInputMethods(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jobject jpackageList)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetPermittedInputMethods()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    AutoPtr<IList> packageList;
    if (jpackageList != NULL) {
        if (!ElUtil::ToElStringList(env, jpackageList, (IList**)&packageList))
            ALOGE("nativeClearProfileOwner: ToElStringList fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->SetPermittedInputMethods(admin, packageList, &result);
    if (FAILED(ec))
        ALOGE("SetPermittedInputMethods() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetPermittedInputMethods()");
    return result;
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedInputMethods(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedInputMethods()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = dpm->GetPermittedInputMethods(admin, (IList**)&list);
    if (FAILED(ec))
        ALOGE("GetPermittedInputMethods() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedInputMethods()");

    return ElUtil::GetJavaStringList(env, list);
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedInputMethodsForCurrentUser(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedInputMethodsForCurrentUser()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = dpm->GetPermittedInputMethodsForCurrentUser((IList**)&list);
    if (FAILED(ec))
        ALOGE("GetPermittedInputMethodsForCurrentUser() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedInputMethodsForCurrentUser()");
    return ElUtil::GetJavaStringList(env, list);
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeSetApplicationHidden(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jstring jpackageName, jboolean jhidden)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetApplicationHidden()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->SetApplicationHidden(admin, packageName, jhidden, &result);
    if (FAILED(ec))
        ALOGE("SetApplicationHidden() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetApplicationHidden()");
    return result;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeIsApplicationHidden(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jstring jpackageName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeIsApplicationHidden()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->IsApplicationHidden(admin, packageName, &result);
    if (FAILED(ec))
        ALOGE("IsApplicationHidden() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeIsApplicationHidden()");
    return result;
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeCreateUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jname)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeCreateUser()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeCreateUser: ToElComponentName fail");
    }
    else {
        ALOGE("nativeCreateUser: jwho is NULL!");
    }

    String name = ElUtil::ToElString(env, jname);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IUserHandle> user;
    ECode ec = dpm->CreateUser(who, name, (IUserHandle**)&user);
    if (FAILED(ec))
        ALOGE("CreateUser() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeCreateUser()");
    return ElUtil::GetJavaUserHandle(env, user);
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeCreateAndInitializeUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jname, jstring jprofileOwnerName,
    jobject jprofileOwnerComponent, jobject jadminExtras)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeCreateAndInitializeUser()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeCreateAndInitializeUser: ToElComponentName fail");
    }
    else {
        ALOGE("nativeCreateAndInitializeUser: jwho is NULL!");
    }

    String name = ElUtil::ToElString(env, jname);
    String profileOwnerName = ElUtil::ToElString(env, jprofileOwnerName);

    AutoPtr<IComponentName> profileOwnerComponent;
    if (jprofileOwnerComponent != NULL) {
        if (!ElUtil::ToElComponentName(env, jprofileOwnerComponent, (IComponentName**)&profileOwnerComponent))
            ALOGE("nativeCreateAndInitializeUser: ToElComponentName fail");
    }

    AutoPtr<IBundle> adminExtras;
    if (jadminExtras != NULL) {
        if (!ElUtil::ToElBundle(env, jadminExtras, (IBundle**)&adminExtras))
            ALOGE("nativeCreateAndInitializeUser: ToElBundle fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IUserHandle> user;
    ECode ec = dpm->CreateAndInitializeUser(who, name, profileOwnerName, profileOwnerComponent, adminExtras, (IUserHandle**)&user);
    if (FAILED(ec))
        ALOGE("CreateAndInitializeUser() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeCreateAndInitializeUser()");
    return ElUtil::GetJavaUserHandle(env, user);
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jobject juserHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveUser()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeRemoveUser: ToElComponentName fail");
    }
    else {
        ALOGE("nativeRemoveUser: jwho is NULL!");
    }

    AutoPtr<IUserHandle> userHandle;
    if (juserHandle != NULL) {
        if (!ElUtil::ToElUserHandle(env, juserHandle, (IUserHandle**)&userHandle))
            ALOGE("nativeRemoveUser: ToElUserHandle fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->RemoveUser(who, userHandle, &result);
    if (FAILED(ec))
        ALOGE("RemoveUser() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveUser()");
    return result;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeSwitchUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jobject juserHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSwitchUser()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSwitchUser: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSwitchUser: jwho is NULL!");
    }

    AutoPtr<IUserHandle> userHandle;
    if (juserHandle != NULL) {
        if (!ElUtil::ToElUserHandle(env, juserHandle, (IUserHandle**)&userHandle))
            ALOGE("nativeSwitchUser: ToElUserHandle fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->SwitchUser(who, userHandle, &result);
    if (FAILED(ec))
        ALOGE("SwitchUser() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSwitchUser()");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeEnableSystemApp(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jstring jpackageName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeEnableSystemApp()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->EnableSystemApp(admin, packageName);
    if (FAILED(ec))
        ALOGE("EnableSystemApp() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeEnableSystemApp()");
}

static jint android_app_admin_ElDevicePolicyManagerProxy_nativeEnableSystemAppWithIntent(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jobject jintent)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeEnableSystemAppWithIntent()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    AutoPtr<IIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElIntent(env, jintent, (IIntent**)&intent))
            ALOGE("nativeClearProfileOwner: ToElIntent fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Int32 result;
    ECode ec = dpm->EnableSystemAppWithIntent(admin, intent, &result);
    if (FAILED(ec))
        ALOGE("EnableSystemAppWithIntent() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeEnableSystemAppWithIntent()");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetAccountManagementDisabled(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jaccountType, jboolean jdisabled)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetAccountManagementDisabled()");

    String accountType = ElUtil::ToElString(env, jaccountType);

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetAccountManagementDisabled: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetAccountManagementDisabled: jwho is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetAccountManagementDisabled(who, accountType, jdisabled);
    if (FAILED(ec))
        ALOGE("SetAccountManagementDisabled() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetAccountManagementDisabled()");
}

static jobjectArray android_app_admin_ElDevicePolicyManagerProxy_nativeGetAccountTypesWithManagementDisabled(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetAccountTypesWithManagementDisabled()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<ArrayOf<String> > strArray;
    ECode ec = dpm->GetAccountTypesWithManagementDisabled((ArrayOf<String>**)&strArray);
    if (FAILED(ec))
        ALOGE("GetAccountTypesWithManagementDisabled() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetAccountTypesWithManagementDisabled()");
    return ElUtil::GetJavaStringArray(env, strArray);
}

static jobjectArray android_app_admin_ElDevicePolicyManagerProxy_nativeGetAccountTypesWithManagementDisabledAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juserId)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetAccountTypesWithManagementDisabledAsUser()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<ArrayOf<String> > strArray;
    ECode ec = dpm->GetAccountTypesWithManagementDisabledAsUser(juserId, (ArrayOf<String>**)&strArray);
    if (FAILED(ec))
        ALOGE("GetAccountTypesWithManagementDisabledAsUser() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetAccountTypesWithManagementDisabledAsUser()");
    return ElUtil::GetJavaStringArray(env, strArray);
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetLockTaskPackages(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jobjectArray jpackages)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetLockTaskPackages()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetLockTaskPackages: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetLockTaskPackages: jwho is NULL!");
    }

    AutoPtr<ArrayOf<String> > packages;
    if (jpackages != NULL) {
        ElUtil::ToElStringArray(env, jpackages, (ArrayOf<String>**)&packages);
    }
    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetLockTaskPackages(who, packages);
    if (FAILED(ec))
        ALOGE("SetLockTaskPackages() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetLockTaskPackages()");
}

static jobjectArray android_app_admin_ElDevicePolicyManagerProxy_nativeGetLockTaskPackages(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetLockTaskPackages()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeGetLockTaskPackages: ToElComponentName fail");
    }
    else {
        ALOGE("nativeGetLockTaskPackages: jwho is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<ArrayOf<String> > strArray;
    ECode ec = dpm->GetLockTaskPackages(who, (ArrayOf<String>**)&strArray);
    if (FAILED(ec))
        ALOGE("GetLockTaskPackages() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetLockTaskPackages()");
    return ElUtil::GetJavaStringArray(env, strArray);
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeIsLockTaskPermitted(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpkg)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeIsLockTaskPermitted()");

    String pkg = ElUtil::ToElString(env, jpkg);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->IsLockTaskPermitted(pkg, &result);
    if (FAILED(ec))
        ALOGE("IsLockTaskPermitted() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeIsLockTaskPermitted()");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetGlobalSetting(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jsetting, jstring jvalue)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetGlobalSetting()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetGlobalSetting: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetGlobalSetting: jwho is NULL!");
    }

    String setting = ElUtil::ToElString(env, jsetting);
    String value = ElUtil::ToElString(env, jvalue);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetGlobalSetting(who, setting, value);
    if (FAILED(ec))
        ALOGE("SetGlobalSetting() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetGlobalSetting()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetSecureSetting(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jstring jsetting, jstring jvalue)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetSecureSetting()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetSecureSetting: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetSecureSetting: jwho is NULL!");
    }

    String setting = ElUtil::ToElString(env, jsetting);
    String value = ElUtil::ToElString(env, jvalue);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetSecureSetting(who, setting, value);
    if (FAILED(ec))
        ALOGE("SetSecureSetting() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetSecureSetting()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetMasterVolumeMuted(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jboolean jon)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetMasterVolumeMuted()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetMasterVolumeMuted(admin, jon);
    if (FAILED(ec))
        ALOGE("SetMasterVolumeMuted() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetMasterVolumeMuted()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeIsMasterVolumeMuted(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeIsMasterVolumeMuted()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->IsMasterVolumeMuted(admin, &result);
    if (FAILED(ec))
        ALOGE("IsMasterVolumeMuted() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeIsMasterVolumeMuted()");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeNotifyLockTaskModeChanged(
    JNIEnv* env, jobject clazz, jlong jproxy, jboolean jisEnabled, jstring jpkg, jint juserId)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeNotifyLockTaskModeChanged()");

    String pkg = ElUtil::ToElString(env, jpkg);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->NotifyLockTaskModeChanged(jisEnabled, pkg, juserId);
    if (FAILED(ec))
        ALOGE("NotifyLockTaskModeChanged() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeNotifyLockTaskModeChanged()");
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetUninstallBlocked(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jstring jpackageName, jboolean juninstallBlocked)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetUninstallBlocked()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetUninstallBlocked(admin, packageName, juninstallBlocked);
    if (FAILED(ec))
        ALOGE("SetUninstallBlocked() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetUninstallBlocked()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeIsUninstallBlocked(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jstring jpackageName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeIsUninstallBlocked()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->IsUninstallBlocked(admin, packageName, &result);
    if (FAILED(ec))
        ALOGE("IsUninstallBlocked() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeIsUninstallBlocked()");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetCrossProfileCallerIdDisabled(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jboolean jdisabled)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetCrossProfileCallerIdDisabled()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetCrossProfileCallerIdDisabled: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetCrossProfileCallerIdDisabled: jwho is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetCrossProfileCallerIdDisabled(who, jdisabled);
    if (FAILED(ec))
        ALOGE("SetCrossProfileCallerIdDisabled() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetCrossProfileCallerIdDisabled()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileCallerIdDisabled(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileCallerIdDisabled()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeGetCrossProfileCallerIdDisabled: ToElComponentName fail");
    }
    else {
        ALOGE("nativeGetCrossProfileCallerIdDisabled: jwho is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->GetCrossProfileCallerIdDisabled(who, &result);
    if (FAILED(ec))
        ALOGE("GetCrossProfileCallerIdDisabled() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileCallerIdDisabled()");
    return result;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileCallerIdDisabledForUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juserId)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileCallerIdDisabledForUser()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->GetCrossProfileCallerIdDisabledForUser(juserId, &result);
    if (FAILED(ec))
        ALOGE("GetCrossProfileCallerIdDisabledForUser() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileCallerIdDisabledForUser()");
    return result;
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetTrustAgentFeaturesEnabled(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jobjectArray jagent, jobject jfeatures, jint juserId)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetTrustAgentFeaturesEnabled()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    AutoPtr<IComponentName> agent;
    if (jagent != NULL) {
        if (!ElUtil::ToElComponentName(env, jagent, (IComponentName**)&agent))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }

    AutoPtr<IList> features;
    if (jfeatures != NULL) {
        ElUtil::ToElStringList(env, jfeatures, (IList**)&features);
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetTrustAgentFeaturesEnabled(admin, agent, features, juserId);
    if (FAILED(ec))
        ALOGE("SetTrustAgentFeaturesEnabled() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetTrustAgentFeaturesEnabled()");
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetTrustAgentFeaturesEnabled(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jobject jagent, jint juserId)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetTrustAgentFeaturesEnabled()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    AutoPtr<IComponentName> agent;
    if (jagent != NULL) {
        if (!ElUtil::ToElComponentName(env, jagent, (IComponentName**)&agent))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = dpm->GetTrustAgentFeaturesEnabled(admin, agent, juserId, (IList**)&list);
    if (FAILED(ec))
        ALOGE("GetTrustAgentFeaturesEnabled() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetTrustAgentFeaturesEnabled()");
    return ElUtil::GetJavaStringList(env, list);
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeAddCrossProfileWidgetProvider(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jstring jpackageName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeAddCrossProfileWidgetProvider()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->AddCrossProfileWidgetProvider(admin, packageName, &result);
    if (FAILED(ec))
        ALOGE("AddCrossProfileWidgetProvider() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeAddCrossProfileWidgetProvider()");
    return result;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveCrossProfileWidgetProvider(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin, jstring jpackageName)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveCrossProfileWidgetProvider()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->RemoveCrossProfileWidgetProvider(admin, packageName, &result);
    if (FAILED(ec))
        ALOGE("RemoveCrossProfileWidgetProvider() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveCrossProfileWidgetProvider()");
    return result;
}

static jobject android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileWidgetProviders(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jadmin)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileWidgetProviders()");

    AutoPtr<IComponentName> admin;
    if (jadmin != NULL) {
        if (!ElUtil::ToElComponentName(env, jadmin, (IComponentName**)&admin))
            ALOGE("nativeClearProfileOwner: ToElComponentName fail");
    }
    else {
        ALOGE("nativeClearProfileOwner: jadmin is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    AutoPtr<IList> list;
    ECode ec = dpm->GetCrossProfileWidgetProviders(admin, (IList**)&list);
    if (FAILED(ec))
        ALOGE("GetCrossProfileWidgetProviders() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileWidgetProviders()");
    return ElUtil::GetJavaStringList(env, list);
}

static void android_app_admin_ElDevicePolicyManagerProxy_nativeSetAutoTimeRequired(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jwho, jint juserHandle, jboolean jrequired)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeSetAutoTimeRequired()");

    AutoPtr<IComponentName> who;
    if (jwho != NULL) {
        if (!ElUtil::ToElComponentName(env, jwho, (IComponentName**)&who))
            ALOGE("nativeSetAutoTimeRequired: ToElComponentName fail");
    }
    else {
        ALOGE("nativeSetAutoTimeRequired: jwho is NULL!");
    }

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    ECode ec = dpm->SetAutoTimeRequired(who, juserHandle, jrequired);
    if (FAILED(ec))
        ALOGE("SetAutoTimeRequired() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeSetAutoTimeRequired()");
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeGetAutoTimeRequired(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeGetAutoTimeRequired()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->GetAutoTimeRequired(&result);
    if (FAILED(ec))
        ALOGE("GetAutoTimeRequired() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeGetAutoTimeRequired()");
    return result;
}

static jboolean android_app_admin_ElDevicePolicyManagerProxy_nativeRequireSecureKeyguard(
    JNIEnv* env, jobject clazz, jlong jproxy, jint juserHandle)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeRequireSecureKeyguard()");

    IIDevicePolicyManager* dpm = (IIDevicePolicyManager*)jproxy;
    Boolean result;
    ECode ec = dpm->RequireSecureKeyguard(juserHandle, &result);
    if (FAILED(ec))
        ALOGE("RequireSecureKeyguard() return FAILED");

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeRequireSecureKeyguard()");
    return result;
}


static void android_app_admin_ElDevicePolicyManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_app_admin_ElDevicePolicyManagerProxy_nativeDestroy()");

    IIDevicePolicyManager* obj = (IIDevicePolicyManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_app_admin_ElDevicePolicyManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeFinalize },

    { "nativePackageHasActiveAdmins",    "(JLjava/lang/String;I)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativePackageHasActiveAdmins },

    { "nativeSetPasswordQuality",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordQuality },

    { "nativeGetPasswordQuality",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordQuality },

    { "nativeSetPasswordMinimumLength",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLength },

    { "nativeGetPasswordMinimumLength",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLength },

    { "nativeSetPasswordMinimumUpperCase",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumUpperCase },

    { "nativeGetPasswordMinimumUpperCase",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumUpperCase },

    { "nativeSetPasswordMinimumLowerCase",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLowerCase },

    { "nativeGetPasswordMinimumLowerCase",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLowerCase },

    { "nativeSetPasswordMinimumLetters",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumLetters },

    { "nativeGetPasswordMinimumLetters",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumLetters },

    { "nativeSetPasswordMinimumNumeric",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumNumeric },

    { "nativeGetPasswordMinimumNumeric",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNumeric },

    { "nativeSetPasswordMinimumSymbols",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumSymbols },

    { "nativeGetPasswordMinimumSymbols",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumSymbols },

    { "nativeSetPasswordMinimumNonLetter",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordMinimumNonLetter },

    { "nativeGetPasswordMinimumNonLetter",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordMinimumNonLetter },

    { "nativeSetPasswordHistoryLength",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordHistoryLength },

    { "nativeGetPasswordHistoryLength",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordHistoryLength },

    { "nativeSetPasswordExpirationTimeout",    "(JLandroid/content/ComponentName;JI)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPasswordExpirationTimeout },

    { "nativeGetPasswordExpirationTimeout",    "(JLandroid/content/ComponentName;I)J",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordExpirationTimeout },

    { "nativeGetPasswordExpiration",    "(JLandroid/content/ComponentName;I)J",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPasswordExpiration },

    { "nativeIsActivePasswordSufficient",    "(JI)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeIsActivePasswordSufficient },

    { "nativeGetCurrentFailedPasswordAttempts",    "(JI)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetCurrentFailedPasswordAttempts },

    { "nativeSetMaximumFailedPasswordsForWipe",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetMaximumFailedPasswordsForWipe },

    { "nativeGetMaximumFailedPasswordsForWipe",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetMaximumFailedPasswordsForWipe },

    { "nativeResetPassword",    "(JLjava/lang/String;II)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeResetPassword },

    { "nativeSetMaximumTimeToLock",    "(JLandroid/content/ComponentName;JI)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetMaximumTimeToLock },

    { "nativeGetMaximumTimeToLock",    "(JLandroid/content/ComponentName;I)J",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetMaximumTimeToLock },

    { "nativeLockNow",    "(J)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeLockNow },

    { "nativeWipeData",    "(JII)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeWipeData },

    { "nativeSetGlobalProxy",    "(JLandroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/ComponentName;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetGlobalProxy },

    { "nativeGetGlobalProxyAdmin",    "(JI)Landroid/content/ComponentName;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetGlobalProxyAdmin },

    { "nativeSetRecommendedGlobalProxy",    "(JLandroid/content/ComponentName;Landroid/net/ProxyInfo;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetRecommendedGlobalProxy },

    { "nativeSetStorageEncryption",    "(JLandroid/content/ComponentName;ZI)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetStorageEncryption },

    { "nativeGetStorageEncryption",    "(JLandroid/content/ComponentName;I)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetStorageEncryption },

    { "nativeGetStorageEncryptionStatus",    "(JI)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetStorageEncryptionStatus },

    { "nativeSetCameraDisabled",    "(JLandroid/content/ComponentName;ZI)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetCameraDisabled },

    { "nativeGetCameraDisabled",    "(JLandroid/content/ComponentName;I)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetCameraDisabled },

    { "nativeSetScreenCaptureDisabled",    "(JLandroid/content/ComponentName;IZ)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetScreenCaptureDisabled },

    { "nativeGetScreenCaptureDisabled",    "(JLandroid/content/ComponentName;I)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetScreenCaptureDisabled },

    { "nativeSetKeyguardDisabledFeatures",    "(JLandroid/content/ComponentName;II)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetKeyguardDisabledFeatures },

    { "nativeGetKeyguardDisabledFeatures",    "(JLandroid/content/ComponentName;I)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetKeyguardDisabledFeatures },

    { "nativeSetActiveAdmin",    "(JLandroid/content/ComponentName;ZI)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetActiveAdmin },

    { "nativeIsAdminActive",    "(JLandroid/content/ComponentName;I)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeIsAdminActive },

    { "nativeGetActiveAdmins",    "(JI)Ljava/util/List;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetActiveAdmins },

    { "nativeGetRemoveWarning",    "(JLandroid/content/ComponentName;Landroid/os/RemoteCallback;I)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetRemoveWarning },

    { "nativeRemoveActiveAdmin",    "(JLandroid/content/ComponentName;I)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveActiveAdmin },

    { "nativeHasGrantedPolicy",    "(JLandroid/content/ComponentName;II)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeHasGrantedPolicy },

    { "nativeSetActivePasswordState",    "(JIIIIIIIII)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetActivePasswordState },

    { "nativeReportFailedPasswordAttempt",    "(JI)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeReportFailedPasswordAttempt },

    { "nativeReportSuccessfulPasswordAttempt",    "(JI)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeReportSuccessfulPasswordAttempt },

    { "nativeSetDeviceOwner",    "(JLjava/lang/String;Ljava/lang/String;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetDeviceOwner },

    { "nativeIsDeviceOwner",    "(JLjava/lang/String;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeIsDeviceOwner },

    { "nativeGetDeviceOwner",    "(J)Ljava/lang/String;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetDeviceOwner },

    { "nativeGetDeviceOwnerName",    "(J)Ljava/lang/String;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetDeviceOwnerName },

    { "nativeClearDeviceOwner",    "(JLjava/lang/String;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeClearDeviceOwner },

    { "nativeSetProfileOwner",    "(JLandroid/content/ComponentName;Ljava/lang/String;I)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileOwner },

    { "nativeGetProfileOwner",    "(JI)Landroid/content/ComponentName;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetProfileOwner },

    { "nativeGetProfileOwnerName",    "(JI)Ljava/lang/String;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetProfileOwnerName },

    { "nativeSetProfileEnabled",    "(JLandroid/content/ComponentName;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileEnabled },

    { "nativeSetProfileName",    "(JLandroid/content/ComponentName;Ljava/lang/String;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetProfileName },

    { "nativeClearProfileOwner",    "(JLandroid/content/ComponentName;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeClearProfileOwner },

    { "nativeHasUserSetupCompleted",    "(J)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeHasUserSetupCompleted },

    { "nativeInstallCaCert",    "(JLandroid/content/ComponentName;[B)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeInstallCaCert },

    { "nativeUninstallCaCert",    "(JLandroid/content/ComponentName;Ljava/lang/String;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeUninstallCaCert },

    { "nativeEnforceCanManageCaCerts",    "(JLandroid/content/ComponentName;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeEnforceCanManageCaCerts },

    { "nativeInstallKeyPair",    "(JLandroid/content/ComponentName;[B[BLjava/lang/String;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeInstallKeyPair },

    { "nativeAddPersistentPreferredActivity",    "(JLandroid/content/ComponentName;Landroid/content/IntentFilter;Landroid/content/ComponentName;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeAddPersistentPreferredActivity },

    { "nativeClearPackagePersistentPreferredActivities",    "(JLandroid/content/ComponentName;Ljava/lang/String;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeClearPackagePersistentPreferredActivities },

    { "nativeSetApplicationRestrictions",    "(JLandroid/content/ComponentName;Ljava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetApplicationRestrictions },

    { "nativeGetApplicationRestrictions",    "(JLandroid/content/ComponentName;Ljava/lang/String;)Landroid/os/Bundle;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetApplicationRestrictions },

    { "nativeSetRestrictionsProvider",    "(JLandroid/content/ComponentName;Landroid/content/ComponentName;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetRestrictionsProvider },

    { "nativeGetRestrictionsProvider",    "(JI)Landroid/content/ComponentName;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetRestrictionsProvider },

    { "nativeSetUserRestriction",    "(JLandroid/content/ComponentName;Ljava/lang/String;Z)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetUserRestriction },

    { "nativeAddCrossProfileIntentFilter",    "(JLandroid/content/ComponentName;Landroid/content/IntentFilter;I)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeAddCrossProfileIntentFilter },

    { "nativeClearCrossProfileIntentFilters",    "(JLandroid/content/ComponentName;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeClearCrossProfileIntentFilters },

    { "nativeSetPermittedAccessibilityServices",    "(JLandroid/content/ComponentName;Ljava/util/List;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPermittedAccessibilityServices },

    { "nativeGetPermittedAccessibilityServices",    "(JLandroid/content/ComponentName;)Ljava/util/List;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedAccessibilityServices },

    { "nativeGetPermittedAccessibilityServicesForUser",    "(JI)Ljava/util/List;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedAccessibilityServicesForUser },

    { "nativeSetPermittedInputMethods",    "(JLandroid/content/ComponentName;Ljava/util/List;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetPermittedInputMethods },

    { "nativeGetPermittedInputMethods",    "(JLandroid/content/ComponentName;)Ljava/util/List;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedInputMethods },

    { "nativeGetPermittedInputMethodsForCurrentUser",    "(J)Ljava/util/List;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetPermittedInputMethodsForCurrentUser },

    { "nativeSetApplicationHidden",    "(JLandroid/content/ComponentName;Ljava/lang/String;Z)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetApplicationHidden },

    { "nativeIsApplicationHidden",    "(JLandroid/content/ComponentName;Ljava/lang/String;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeIsApplicationHidden },

    { "nativeCreateUser",    "(JLandroid/content/ComponentName;Ljava/lang/String;)Landroid/os/UserHandle;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeCreateUser },

    { "nativeCreateAndInitializeUser",    "(JLandroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;Landroid/os/Bundle;)Landroid/os/UserHandle;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeCreateAndInitializeUser },

    { "nativeRemoveUser",    "(JLandroid/content/ComponentName;Landroid/os/UserHandle;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveUser },

    { "nativeSwitchUser",    "(JLandroid/content/ComponentName;Landroid/os/UserHandle;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSwitchUser },

    { "nativeEnableSystemApp",    "(JLandroid/content/ComponentName;Ljava/lang/String;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeEnableSystemApp },

    { "nativeEnableSystemAppWithIntent",    "(JLandroid/content/ComponentName;Landroid/content/Intent;)I",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeEnableSystemAppWithIntent },

    { "nativeSetAccountManagementDisabled",    "(JLandroid/content/ComponentName;Ljava/lang/String;Z)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetAccountManagementDisabled },

    { "nativeGetAccountTypesWithManagementDisabled",    "(J)[Ljava/lang/String;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetAccountTypesWithManagementDisabled },

    { "nativeGetAccountTypesWithManagementDisabledAsUser",    "(JI)[Ljava/lang/String;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetAccountTypesWithManagementDisabledAsUser },

    { "nativeSetLockTaskPackages",    "(JLandroid/content/ComponentName;[Ljava/lang/String;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetLockTaskPackages },

    { "nativeGetLockTaskPackages",    "(JLandroid/content/ComponentName;)[Ljava/lang/String;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetLockTaskPackages },

    { "nativeIsLockTaskPermitted",    "(JLjava/lang/String;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeIsLockTaskPermitted },

    { "nativeSetGlobalSetting",    "(JLandroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetGlobalSetting },

    { "nativeSetSecureSetting",    "(JLandroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetSecureSetting },

    { "nativeSetMasterVolumeMuted",    "(JLandroid/content/ComponentName;Z)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetMasterVolumeMuted },

    { "nativeIsMasterVolumeMuted",    "(JLandroid/content/ComponentName;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeIsMasterVolumeMuted },

    { "nativeNotifyLockTaskModeChanged",    "(JZLjava/lang/String;I)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeNotifyLockTaskModeChanged },

    { "nativeSetUninstallBlocked",    "(JLandroid/content/ComponentName;Ljava/lang/String;Z)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetUninstallBlocked },

    { "nativeIsUninstallBlocked",    "(JLandroid/content/ComponentName;Ljava/lang/String;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeIsUninstallBlocked },

    { "nativeSetCrossProfileCallerIdDisabled",    "(JLandroid/content/ComponentName;Z)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetCrossProfileCallerIdDisabled },

    { "nativeGetCrossProfileCallerIdDisabled",    "(JLandroid/content/ComponentName;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileCallerIdDisabled },

    { "nativeGetCrossProfileCallerIdDisabledForUser",    "(JI)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileCallerIdDisabledForUser },

    { "nativeSetTrustAgentFeaturesEnabled",    "(JLandroid/content/ComponentName;Landroid/content/ComponentName;Ljava/util/List;I)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetTrustAgentFeaturesEnabled },

    { "nativeGetTrustAgentFeaturesEnabled",    "(JLandroid/content/ComponentName;Landroid/content/ComponentName;I)Ljava/util/List;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetTrustAgentFeaturesEnabled },

    { "nativeAddCrossProfileWidgetProvider",    "(JLandroid/content/ComponentName;Ljava/lang/String;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeAddCrossProfileWidgetProvider },

    { "nativeRemoveCrossProfileWidgetProvider",    "(JLandroid/content/ComponentName;Ljava/lang/String;)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeRemoveCrossProfileWidgetProvider },

    { "nativeGetCrossProfileWidgetProviders",    "(JLandroid/content/ComponentName;)Ljava/util/List;",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetCrossProfileWidgetProviders },

    { "nativeSetAutoTimeRequired",    "(JLandroid/content/ComponentName;IZ)V",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeSetAutoTimeRequired },

    { "nativeGetAutoTimeRequired",    "(J)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeGetAutoTimeRequired },

    { "nativeRequireSecureKeyguard",    "(JI)Z",
            (void*) android_app_admin_ElDevicePolicyManagerProxy_nativeRequireSecureKeyguard }

};

int register_android_app_admin_ElDevicePolicyManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/app/admin/ElDevicePolicyManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

