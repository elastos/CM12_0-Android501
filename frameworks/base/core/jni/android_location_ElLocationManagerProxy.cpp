
#define LOG_TAG "ElLocationManagerProxyJNI"
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <elastos/utility/etl/HashMap.h>
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.Location.h>
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Os.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Core.h>
#include <Elastos.CoreLibrary.Utility.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::App::IPendingIntent;
using Elastos::Droid::Location::IIGpsStatusListener;
using Elastos::Droid::Location::IIGpsMeasurementsListener;
using Elastos::Droid::Location::IIGpsNavigationMessageListener;
using Elastos::Droid::Location::IILocationManager;
using Elastos::Droid::Location::IILocationListener;
using Elastos::Droid::Location::CLocation;
using Elastos::Droid::JavaProxy::CGpsStatusListener;
using Elastos::Droid::JavaProxy::CLocationListener;
using Elastos::Droid::JavaProxy::CIGpsMeasurementsListenerNative;
using Elastos::Droid::JavaProxy::CIGpsNavigationMessageListenerNative;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, IIGpsStatusListener*> sGpsStatusListener;
static Mutex sGpsStatusListenerLock;
static HashMap<Int32, IILocationListener*> sLocationListener;
static Mutex sLocationListenerLock;
static HashMap<Int32, IIGpsMeasurementsListener*> sGpsMeasurementsListener;
static Mutex sGpsMeasurementsListenerLock;
static HashMap<Int32, IIGpsNavigationMessageListener*> sGpsNavigationMessageListener;
static Mutex sGpsNavigationMessageListenerLock;

static jobject android_location_ElLocationManagerProxy_nativeGetProviderProperties(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jprovider)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeGetProviderProperties()");

    IILocationManager* locationManager = (IILocationManager*)jproxy;

    String provider = ElUtil::ToElString(env, jprovider);

    AutoPtr<IProviderProperties> pproperty;
    locationManager->GetProviderProperties(provider, (IProviderProperties**)&pproperty);

    jobject jpproperty = NULL;
    if (pproperty != NULL) {
        jpproperty = ElUtil::GetJavaProviderProperties(env, pproperty);
    }

    // ALOGD("- android_location_ElLocationManagerProxy_nativeGetProviderProperties()");
    return jpproperty;
}

static jboolean android_location_ElLocationManagerProxy_nativeAddGpsStatusListener(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlistener, jstring jpackageName)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeAddGpsStatusListener()");

    AutoPtr<IIGpsStatusListener> listener;
    if (jlistener != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        CGpsStatusListener::New((Handle64)jvm, (Handle64)jlistener, (IIGpsStatusListener**)&listener);
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
        Mutex::Autolock lock(sGpsStatusListenerLock);
        sGpsStatusListener[hashCode] = listener;
        listener->AddRef();
    }

    IILocationManager* locationManager = (IILocationManager*)jproxy;
    String packageName = ElUtil::ToElString(env, jpackageName);

    Boolean result = FALSE;
    ECode ec = locationManager->AddGpsStatusListener(listener, packageName, &result);
    if (FAILED(ec))
        ALOGE("nativeAddGpsStatusListener() ec: 0x%08x result: %d", ec, result);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeAddGpsStatusListener()");
    return (jboolean)result;
}

static void android_location_ElLocationManagerProxy_nativeRequestLocationUpdates(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jrequest, jobject jlistener, jobject jintent, jstring jpackageName)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeRequestLocationUpdates");

    AutoPtr<ILocationRequest> request;
    ElUtil::ToElLocationRequest(env, jrequest, (ILocationRequest**)&request);

    AutoPtr<IILocationListener> listener;
    if (jlistener != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        CLocationListener::New((Handle64)jvm, (Handle64)jlistener, (IILocationListener**)&listener);
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
        Mutex::Autolock lock(sLocationListenerLock);
        sLocationListener[hashCode] = listener;
        listener->AddRef();
    }

    AutoPtr<IPendingIntent> pendingIntent;
    if (jintent != NULL) {
        ElUtil::ToElPendingIntent(env, jintent, (IPendingIntent**)&pendingIntent);
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IILocationManager* locationManager = (IILocationManager*)jproxy;
    locationManager->RequestLocationUpdates(request, listener, pendingIntent, packageName);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeRequestLocationUpdates");
}

static void android_location_ElLocationManagerProxy_nativeRemoveUpdates(JNIEnv* env, jobject clazz, jlong jproxy,
        jobject jlistener, jobject jintent, jstring jpackageName)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeRemoveUpdates()");

    AutoPtr<IILocationListener> listener;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
    {
        Mutex::Autolock lock(sLocationListenerLock);
        if (sLocationListener.Find(hashCode) != sLocationListener.End()){
            listener = sLocationListener[hashCode];
            sLocationListener.Erase(hashCode);
        }
    }

    if (NULL == listener) {
        ALOGE("nativeRemoveUpdates() Invalid IILocationListener!\n");
        env->ExceptionDescribe();
        return;
    }

    AutoPtr<IPendingIntent> pendingIntent;
    if (jintent != NULL) {
        ElUtil::ToElPendingIntent(env, jintent, (IPendingIntent**)&pendingIntent);
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IILocationManager* locationManager = (IILocationManager*)jproxy;
    locationManager->RemoveUpdates(listener, pendingIntent, packageName);

    listener->Release();
    // ALOGD("- android_location_ElLocationManagerProxy_nativeRemoveUpdates()");
}

static void android_location_ElLocationManagerProxy_nativeRemoveGeofence(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jfence, jobject jintent, jstring jpackageName)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeRemoveGeofence");

    IILocationManager* locationManager = (IILocationManager*)jproxy;

    AutoPtr<IGeofence> geofence;
    if (jfence != NULL) {
        ElUtil::ToElGeofence(env, jfence, (IGeofence**)&geofence);
    }

    AutoPtr<IPendingIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElPendingIntent(env, jintent, (IPendingIntent**)&intent)) {
            ALOGE("nativeRemoveGeofence() ToElPendingIntent fail!");
        }
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    ECode ec = locationManager->RemoveGeofence(geofence, intent, packageName);
    ALOGE("nativeRemoveGeofence() ec: 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeRemoveGeofence");
}

static jobject android_location_ElLocationManagerProxy_nativeGetLastLocation(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jobject jrequest, jstring jpackageName)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeGetLastLocation");

    AutoPtr<ILocationRequest> locationRequest;
    if(jrequest != NULL){
        ElUtil::ToElLocationRequest(env, jrequest, (ILocationRequest**)&locationRequest);
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    AutoPtr<ILocation> location;
    locationManager->GetLastLocation(locationRequest, packageName, (ILocation**)&location);

    jobject jlocation = NULL;
    if (location != NULL) {
        jlocation = ElUtil::GetJavaLocation(env, location);
    }

    // ALOGD("- android_location_ElLocationManagerProxy_nativeGetLastLocation");
    return jlocation;
}

static void android_location_ElLocationManagerProxy_nativeRemoveGpsStatusListener(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jobject jGpsStatusListener)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeRemoveGpsStatusListener");

    AutoPtr<IIGpsStatusListener> listener;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jGpsStatusListener);
    {
        Mutex::Autolock lock(sGpsStatusListenerLock);
        if (sGpsStatusListener.Find(hashCode) != sGpsStatusListener.End()){
            listener = sGpsStatusListener[hashCode];
            sGpsStatusListener.Erase(hashCode);
        }
    }

    if (NULL == listener) {
        ALOGE("nativeRemoveGpsStatusListener() Invalid IIGpsStatusListener!\n");
        env->ExceptionDescribe();
        return;
    }

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->RemoveGpsStatusListener(listener);
    ALOGE("nativeRemoveGpsStatusListener() ec: 0x%08x", ec);

    listener->Release();

    // ALOGD("- android_location_ElLocationManagerProxy_nativeRemoveGpsStatusListener");
}

static jobject android_location_ElLocationManagerProxy_nativeGetAllProviders(JNIEnv* env, jobject clazz, jlong nativeProxy)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeGetAllProviders");

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    AutoPtr<IList> list;
    locationManager->GetAllProviders((IList**)&list);

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetAllProviders Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetAllProviders Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jProviderList = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetAllProviders Fail NewObject: ArrayList : %d!\n", __LINE__);

    if (list != NULL) {
        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetAllProviders Fail GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> provider;
            list->Get(i, (IInterface**)&provider);
            assert(provider != NULL && ICharSequence::Probe(provider) != NULL);
            String providerStr;
            ICharSequence::Probe(provider)->ToString(&providerStr);
            jstring jprovider = ElUtil::GetJavaString(env, providerStr);

            if(jprovider != NULL){
                env->CallBooleanMethod(jProviderList, mAdd, jprovider);
                ElUtil::CheckErrorAndLog(env, "GetJavaInputDevice Fail CallObjectMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jprovider);
            }else{
                ALOGE("nativeGetAllProviders, got a null provider");
            }
        }
    }

    env->DeleteLocalRef(listKlass);
    // ALOGD("- android_location_ElLocationManagerProxy_nativeGetAllProviders");
    return jProviderList;
}

static void android_location_ElLocationManagerProxy_nativeRequestGeofence(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jobject jrequest, jobject jgeofence, jobject jintent, jstring jpackageName)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeRequestGeofence");

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    AutoPtr<ILocationRequest> request;
    if (jrequest != NULL) {
        ElUtil::ToElLocationRequest(env, jrequest, (ILocationRequest**)&request);
    }

    AutoPtr<IGeofence> geofence;
    if (jgeofence != NULL) {
        ElUtil::ToElGeofence(env, jgeofence, (IGeofence**)&geofence);
    }

    AutoPtr<IPendingIntent> intent;
    if (jintent != NULL) {
        if (!ElUtil::ToElPendingIntent(env, jintent, (IPendingIntent**)&intent)) {
            ALOGE("nativeRequestGeofence() ToElPendingIntent fail!");
        }
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    ECode ec = locationManager->RequestGeofence(request, geofence, intent, packageName);
    ALOGE("nativeRequestGeofence ec: 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeRequestGeofence");
}

static void android_location_ElLocationManagerProxy_nativeLocationCallbackFinished(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jlistener)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeLocationCallbackFinished");

    IILocationManager* locationManager = (IILocationManager*)jproxy;

    AutoPtr<IILocationListener> listener;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
    {
        Mutex::Autolock lock(sLocationListenerLock);
        if (sLocationListener.Find(hashCode) != sLocationListener.End()){
            listener = sLocationListener[hashCode];
        }
    }

    if (NULL == listener) {
        ALOGE("nativeRemoveUpdates() Invalid IILocationListener!\n");
        env->ExceptionDescribe();
        return;
    }

    locationManager->LocationCallbackFinished(listener);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeLocationCallbackFinished");
}

static void android_location_ElLocationManagerProxy_nativeReportLocation(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jobject jlocation, jboolean passive)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeReportLocation");

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    AutoPtr<ILocation> location;
    if (jlocation != NULL) {
        ElUtil::ToElLocation(env, jlocation, (ILocation**)&location);
    }

    ECode ec = locationManager->ReportLocation(location, (Boolean)passive);
    ALOGE("nativeReportLocation ec: 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeReportLocation");
}

static jboolean android_location_ElLocationManagerProxy_nativeIsProviderEnabled(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jstring jprovider)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeIsProviderEnabled");

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    String provider = ElUtil::ToElString(env, jprovider);

    Boolean enable = FALSE;
    locationManager->IsProviderEnabled(provider, &enable);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeIsProviderEnabled");
    return (jboolean)enable;
}

static jstring android_location_ElLocationManagerProxy_nativeGetBestProvider(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jobject jcriteria, jboolean enabledOnly)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeGetBestProvider");

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    AutoPtr<ICriteria> criteria;
    if (jcriteria != NULL) {
        ElUtil::ToElCriteria(env, jcriteria, (ICriteria**)&criteria);
    }

    String provider;
    ECode ec = locationManager->GetBestProvider(criteria, (Boolean)enabledOnly, &provider);
    ALOGE("nativeGetBestProvider ec: 0x%08x provider: %s", ec, provider.string());

    // ALOGD("- android_location_ElLocationManagerProxy_nativeGetBestProvider");
    return ElUtil::GetJavaString(env, provider);
}

static jboolean android_location_ElLocationManagerProxy_nativeSendExtraCommand(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jstring jprovider, jstring jcommand, jobject jextras)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeSendExtraCommand");

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    String provider = ElUtil::ToElString(env, jprovider);
    String command = ElUtil::ToElString(env, jcommand);

    AutoPtr<IBundle> extras;
    if (jextras != NULL) {
        if (!ElUtil::ToElBundle(env, jextras, (IBundle**)&extras)) {
            ALOGE("nativeSendExtraCommand() ToElBundle failed!");
        }
    }

    Boolean result = FALSE;
    IBundle* ex = extras.Get();
    ECode ec = locationManager->SendExtraCommand(provider, command, &ex, &result);
    ALOGE("nativeSendExtraCommand() ec: 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeSendExtraCommand");
    return (jboolean)result;
}

static jobject android_location_ElLocationManagerProxy_nativeGetProviders(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jobject jcriteria, jboolean enabledOnly)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeGetProviders");

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    AutoPtr<ICriteria> criteria;
    if (jcriteria != NULL) {
        ElUtil::ToElCriteria(env, jcriteria, (ICriteria**)&criteria);
    }

    AutoPtr<IList> list;
    ECode ec = locationManager->GetProviders(criteria, (Boolean)enabledOnly, (IList**)&list);
    ALOGE("nativeGetProviders() ec: 0x%08x", ec);

    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jproviders = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "NewObject: ArrayList : %d!\n", __LINE__);

    if (list != NULL) {
        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: add : %d!\n", __LINE__);

        Int32 size = 0;
        list->GetSize(&size);
        for (Int32 i = 0; i < size; i++) {
            AutoPtr<IInterface> provider;
            list->Get(i, (IInterface**)&provider);
            assert(provider != NULL && ICharSequence::Probe(provider) != NULL);
            String providerStr;
            ICharSequence::Probe(provider)->ToString(&providerStr);
            jstring jprovider = ElUtil::GetJavaString(env, providerStr);

            if(jprovider != NULL){
                env->CallBooleanMethod(jproviders, mAdd, jprovider);
                ElUtil::CheckErrorAndLog(env, "CallBooleanMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jprovider);
            }else{
                ALOGE("nativeGetProviders, got a null provider");
            }
        }
    }

    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeGetProviders");
    return jproviders;
}

static jboolean android_location_ElLocationManagerProxy_nativeGeocoderIsPresent(JNIEnv* env, jobject clazz, jlong nativeProxy)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeGeocoderIsPresent");

    Boolean result;
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->GeocoderIsPresent(&result);
    ALOGE("nativeGeocoderIsPresent() ec= 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeGeocoderIsPresent");
    return result;
}

static jstring android_location_ElLocationManagerProxy_nativeGetFromLocation(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jdouble jlatitude, jdouble jlongitude, jint jmaxResults, jobject jparams, jobject jaddrs)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeGetFromLocation");

    AutoPtr<IGeocoderParams> params;
    ElUtil::ToElGeocoderParams(env, jparams, (IGeocoderParams**)&params);

    AutoPtr<IList> addrs;

    String result;
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    ECode ec = locationManager->GetFromLocation((Double)jlatitude, (Double)jlongitude, (Int32)jmaxResults, params, (IList**)&addrs, &result);
    ALOGE("GetFromLocation() ec= 0x%08x", ec);

    ALOGE("TODO: translate addrs to jaddrs ?");

    jstring jresult = ElUtil::GetJavaString(env, result);
    ALOGE("android_location_ElLocationManagerProxy_nativeGetFromLocation not complete!");
    // ALOGD("- android_location_ElLocationManagerProxy_nativeGetFromLocation");
    return jresult;
}

static jstring android_location_ElLocationManagerProxy_nativeGetFromLocationName(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jstring jlocationName, jdouble jlowerLeftLatitude, jdouble jlowerLeftLongitude,
    jdouble jupperRightLatitude, jdouble jupperRightLongitude, jint jmaxResults, jobject jparams, jobject jaddrs)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeGetFromLocationName");
    AutoPtr<IGeocoderParams> params;
    ElUtil::ToElGeocoderParams(env, jparams, (IGeocoderParams**)&params);

    String locationName = ElUtil::ToElString(env, jlocationName);

    AutoPtr<IList> addrs;

    String result;
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;

    ECode ec = locationManager->GetFromLocationName(locationName, jlowerLeftLatitude, jlowerLeftLongitude, jupperRightLatitude, jupperRightLongitude, (Int32)jmaxResults, params, (IList**)&addrs, &result);
    ALOGE("GetFromLocationName() ec= 0x%08x", ec);

    ALOGE("TODO: translate addrs to jaddrs ?");

    jstring jresult = ElUtil::GetJavaString(env, result);
    // ALOGD("- android_location_ElLocationManagerProxy_nativeGetFromLocationName");
    return jresult;
}

static jboolean android_location_ElLocationManagerProxy_nativeSendNiResponse(JNIEnv* env, jobject clazz, jlong nativeProxy, jint jnotifId, jint juserResponse)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeSendNiResponse");

    Boolean result;
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->SendNiResponse(jnotifId, juserResponse, &result);
    ALOGE("SendNiResponse() ec= 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeSendNiResponse");
    return result;
}

static jboolean android_location_ElLocationManagerProxy_nativeAddGpsMeasurementsListener(
    JNIEnv* env, jobject clazz, jlong nativeProxy, jobject jlistener, jstring jpackageName)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeAddGpsMeasurementsListener");

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    AutoPtr<IIGpsMeasurementsListener> listener;
    if (jlistener != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        CIGpsMeasurementsListenerNative::New((Handle64)jvm, (Handle64)jlistener, (IIGpsMeasurementsListener**)&listener);
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
        Mutex::Autolock lock(sGpsMeasurementsListenerLock);
        sGpsMeasurementsListener[hashCode] = listener;
        listener->AddRef();
    }

    String packageName = ElUtil::ToElString(env, jpackageName);
    Boolean result;
    locationManager->AddGpsMeasurementsListener(listener, packageName, &result);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeAddGpsMeasurementsListener");
    return result;
}

static jboolean android_location_ElLocationManagerProxy_nativeRemoveGpsMeasurementsListener(
    JNIEnv* env, jobject clazz, jlong nativeProxy, jobject jlistener)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeRemoveGpsMeasurementsListener");

    AutoPtr<IIGpsMeasurementsListener> listener;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
    {
        Mutex::Autolock lock(sGpsMeasurementsListenerLock);
        if (sGpsMeasurementsListener.Find(hashCode) != sGpsMeasurementsListener.End()){
            listener = sGpsMeasurementsListener[hashCode];
            sGpsMeasurementsListener.Erase(hashCode);
        }
    }

    if (NULL == listener) {
        ALOGE("nativeRemoveGpsMeasurementsListener() Invalid IIGpsMeasurementsListener!\n");
        env->ExceptionDescribe();
        return FALSE;
    }

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    Boolean result;
    locationManager->RemoveGpsMeasurementsListener(listener, &result);

    listener->Release();

    // ALOGD("- android_location_ElLocationManagerProxy_nativeRemoveGpsMeasurementsListener");
    return result;
}


static jboolean android_location_ElLocationManagerProxy_nativeAddGpsNavigationMessageListener(
    JNIEnv* env, jobject clazz, jlong nativeProxy, jobject jlistener, jstring jpackageName)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeAddGpsNavigationMessageListener");

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    AutoPtr<IIGpsNavigationMessageListener> listener;
    if (jlistener != NULL) {
        JavaVM* jvm = NULL;
        env->GetJavaVM(&jvm);
        CIGpsNavigationMessageListenerNative::New((Handle64)jvm, (Handle64)jlistener, (IIGpsNavigationMessageListener**)&listener);
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
        Mutex::Autolock lock(sGpsNavigationMessageListenerLock);
        sGpsNavigationMessageListener[hashCode] = listener;
        listener->AddRef();
    }

    String packageName = ElUtil::ToElString(env, jpackageName);
    Boolean result;
    locationManager->AddGpsNavigationMessageListener(listener, packageName, &result);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeAddGpsNavigationMessageListener");
    return result;
}

static jboolean android_location_ElLocationManagerProxy_nativeRemoveGpsNavigationMessageListener(
    JNIEnv* env, jobject clazz, jlong nativeProxy, jobject jlistener)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeRemoveGpsNavigationMessageListener");

    AutoPtr<IIGpsNavigationMessageListener> listener;
    Int32 hashCode = ElUtil::GetJavaHashCode(env, jlistener);
    {
        Mutex::Autolock lock(sGpsNavigationMessageListenerLock);
        if (sGpsNavigationMessageListener.Find(hashCode) != sGpsNavigationMessageListener.End()){
            listener = sGpsNavigationMessageListener[hashCode];
            sGpsNavigationMessageListener.Erase(hashCode);
        }
    }

    if (NULL == listener) {
        ALOGE("nativeRemoveGpsNavigationMessageListener() Invalid IIGpsNavigationMessageListener!\n");
        env->ExceptionDescribe();
        return FALSE;
    }

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    Boolean result;
    locationManager->RemoveGpsNavigationMessageListener(listener, &result);

    listener->Release();

    // ALOGD("- android_location_ElLocationManagerProxy_nativeRemoveGpsNavigationMessageListener");
    return result;
}

static jboolean android_location_ElLocationManagerProxy_nativeProviderMeetsCriteria(JNIEnv* env, jobject clazz, jlong nativeProxy, jstring jprovider, jobject jcriteria)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeProviderMeetsCriteria");
    String provider = ElUtil::ToElString(env, jprovider);

    AutoPtr<ICriteria> criteria;
    ElUtil::ToElCriteria(env, jcriteria, (ICriteria**)&criteria);

    Boolean result;
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->ProviderMeetsCriteria(provider, criteria, &result);
    ALOGE("ProviderMeetsCriteria() ec= 0x%08x", ec);
    // ALOGD("- android_location_ElLocationManagerProxy_nativeProviderMeetsCriteria");
    return result;
}

static void android_location_ElLocationManagerProxy_nativeAddTestProvider(JNIEnv* env, jobject clazz, jlong nativeProxy, jstring jname, jobject jproperties)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeAddTestProvider");

    String name = ElUtil::ToElString(env, jname);
    AutoPtr<IProviderProperties> properties;
    ElUtil::ToElProviderProperties(env, jproperties, (IProviderProperties**)&properties);

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->AddTestProvider(name, properties);
    ALOGE("AddTestProvider() ec= 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeAddTestProvider");
}

static void android_location_ElLocationManagerProxy_nativeRemoveTestProvider(JNIEnv* env, jobject clazz, jlong nativeProxy, jstring jprovider)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeRemoveTestProvider");

    String provider = ElUtil::ToElString(env, jprovider);
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->RemoveTestProvider(provider);
    ALOGE("RemoveTestProvider() ec= 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeRemoveTestProvider");
}

static void android_location_ElLocationManagerProxy_nativeSetTestProviderLocation(JNIEnv* env, jobject clazz, jlong nativeProxy, jstring jprovider, jobject jloc)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeSetTestProviderLocation");
    AutoPtr<ILocation> location;
    ElUtil::ToElLocation(env, jloc, (ILocation**)&location);

    String provider = ElUtil::ToElString(env, jprovider);
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->SetTestProviderLocation(provider, location);
    ALOGE("SetTestProviderLocation() ec= 0x%08x", ec);
    // ALOGD("- android_location_ElLocationManagerProxy_nativeSetTestProviderLocation");
}

static void android_location_ElLocationManagerProxy_nativeClearTestProviderLocation(JNIEnv* env, jobject clazz, jlong nativeProxy, jstring jprovider)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeClearTestProviderLocation");

    String provider = ElUtil::ToElString(env, jprovider);
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->ClearTestProviderLocation(provider);
    ALOGE("ClearTestProviderLocation() ec= 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeClearTestProviderLocation");
}

static void android_location_ElLocationManagerProxy_nativeSetTestProviderEnabled(JNIEnv* env, jobject clazz, jlong nativeProxy, jstring jprovider, jboolean jenabled)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeSetTestProviderEnabled");

    String provider = ElUtil::ToElString(env, jprovider);
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->SetTestProviderEnabled(provider, jenabled);
    ALOGE("SetTestProviderEnabled() ec= 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeSetTestProviderEnabled");
}

static void android_location_ElLocationManagerProxy_nativeClearTestProviderEnabled(JNIEnv* env, jobject clazz, jlong nativeProxy, jstring jprovider)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeClearTestProviderEnabled");

    String provider = ElUtil::ToElString(env, jprovider);
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->ClearTestProviderEnabled(provider);
    ALOGE("ClearTestProviderEnabled() ec= 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeClearTestProviderEnabled");
}

static void android_location_ElLocationManagerProxy_nativeSetTestProviderStatus(JNIEnv* env, jobject clazz, jlong nativeProxy,
    jstring jprovider, jint jstatus, jobject jextras, jlong jupdateTime)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeSetTestProviderStatus");

    AutoPtr<IBundle> extras;
    ElUtil::ToElBundle(env, jextras, (IBundle**)&extras);
    String provider = ElUtil::ToElString(env, jprovider);

    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->SetTestProviderStatus(provider, jstatus, extras, jupdateTime);
    ALOGE("SetTestProviderStatus() ec= 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeSetTestProviderStatus");
}

static void android_location_ElLocationManagerProxy_nativeClearTestProviderStatus(JNIEnv* env, jobject clazz, jlong nativeProxy, jstring jprovider)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeClearTestProviderStatus");

    String provider = ElUtil::ToElString(env, jprovider);
    IILocationManager* locationManager = (IILocationManager*)nativeProxy;
    ECode ec = locationManager->ClearTestProviderStatus(provider);
    ALOGE("ClearTestProviderStatus() ec= 0x%08x", ec);

    // ALOGD("- android_location_ElLocationManagerProxy_nativeClearTestProviderStatus");
}

static void android_location_ElLocationManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_location_ElLocationManagerProxy_nativeDestroy()");

    IILocationManager* obj = (IILocationManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_location_ElLocationManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_location_ElLocationManagerProxy_nativeFinalize },
    { "nativeGetProviderProperties", "(JLjava/lang/String;)Lcom/android/internal/location/ProviderProperties;",
            (void*) android_location_ElLocationManagerProxy_nativeGetProviderProperties },
    { "nativeAddGpsStatusListener", "(JLandroid/location/IGpsStatusListener;Ljava/lang/String;)Z",
            (void*) android_location_ElLocationManagerProxy_nativeAddGpsStatusListener },
    { "nativeRemoveUpdates", "(JLandroid/location/ILocationListener;Landroid/app/PendingIntent;Ljava/lang/String;)V",
            (void*) android_location_ElLocationManagerProxy_nativeRemoveUpdates },
    { "nativeRequestLocationUpdates",
      "(JLandroid/location/LocationRequest;Landroid/location/ILocationListener;Landroid/app/PendingIntent;Ljava/lang/String;)V",
            (void*) android_location_ElLocationManagerProxy_nativeRequestLocationUpdates},
    { "nativeRemoveGeofence", "(JLandroid/location/Geofence;Landroid/app/PendingIntent;Ljava/lang/String;)V",
            (void*) android_location_ElLocationManagerProxy_nativeRemoveGeofence},
    { "nativeGetLastLocation", "(JLandroid/location/LocationRequest;Ljava/lang/String;)Landroid/location/Location;",
            (void*) android_location_ElLocationManagerProxy_nativeGetLastLocation},
    { "nativeRemoveGpsStatusListener", "(JLandroid/location/IGpsStatusListener;)V",
            (void*) android_location_ElLocationManagerProxy_nativeRemoveGpsStatusListener},
    { "nativeGetAllProviders", "(J)Ljava/util/List;",
            (void*) android_location_ElLocationManagerProxy_nativeGetAllProviders},
    { "nativeRequestGeofence",
      "(JLandroid/location/LocationRequest;Landroid/location/Geofence;Landroid/app/PendingIntent;Ljava/lang/String;)V",
            (void*) android_location_ElLocationManagerProxy_nativeRequestGeofence},
    { "nativeLocationCallbackFinished", "(JLandroid/location/ILocationListener;)V",
            (void*) android_location_ElLocationManagerProxy_nativeLocationCallbackFinished},
    { "nativeReportLocation", "(JLandroid/location/Location;Z)V",
            (void*) android_location_ElLocationManagerProxy_nativeReportLocation},
    { "nativeGetBestProvider", "(JLandroid/location/Criteria;Z)Ljava/lang/String;",
            (void*) android_location_ElLocationManagerProxy_nativeGetBestProvider},
    { "nativeIsProviderEnabled", "(JLjava/lang/String;)Z",
            (void*) android_location_ElLocationManagerProxy_nativeIsProviderEnabled},
    { "nativeSendExtraCommand", "(JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z",
            (void*) android_location_ElLocationManagerProxy_nativeSendExtraCommand},
    { "nativeGetProviders", "(JLandroid/location/Criteria;Z)Ljava/util/List;",
            (void*) android_location_ElLocationManagerProxy_nativeGetProviders},
    { "nativeGeocoderIsPresent", "(J)Z",
            (void*) android_location_ElLocationManagerProxy_nativeGeocoderIsPresent},
    { "nativeGetFromLocation", "(JDDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;",
            (void*) android_location_ElLocationManagerProxy_nativeGetFromLocation},
    { "nativeGetFromLocationName", "(JLjava/lang/String;DDDDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;",
            (void*) android_location_ElLocationManagerProxy_nativeGetFromLocationName},
    { "nativeSendNiResponse", "(JII)Z",
            (void*) android_location_ElLocationManagerProxy_nativeSendNiResponse},
    { "nativeAddGpsMeasurementsListener", "(JLandroid/location/IGpsMeasurementsListener;Ljava/lang/String;)Z",
            (void*) android_location_ElLocationManagerProxy_nativeAddGpsMeasurementsListener},
    { "nativeRemoveGpsMeasurementsListener", "(JLandroid/location/IGpsMeasurementsListener;)Z",
            (void*) android_location_ElLocationManagerProxy_nativeRemoveGpsMeasurementsListener},
    { "nativeAddGpsNavigationMessageListener", "(JLandroid/location/IGpsNavigationMessageListener;Ljava/lang/String;)Z",
            (void*) android_location_ElLocationManagerProxy_nativeAddGpsNavigationMessageListener},
    { "nativeRemoveGpsNavigationMessageListener", "(JLandroid/location/IGpsNavigationMessageListener;)Z",
            (void*) android_location_ElLocationManagerProxy_nativeRemoveGpsNavigationMessageListener},
    { "nativeProviderMeetsCriteria", "(JLjava/lang/String;Landroid/location/Criteria;)Z",
            (void*) android_location_ElLocationManagerProxy_nativeProviderMeetsCriteria},
    { "nativeAddTestProvider", "(JLjava/lang/String;Lcom/android/internal/location/ProviderProperties;)V",
            (void*) android_location_ElLocationManagerProxy_nativeAddTestProvider},
    { "nativeRemoveTestProvider", "(JLjava/lang/String;)V",
            (void*) android_location_ElLocationManagerProxy_nativeRemoveTestProvider},
    { "nativeSetTestProviderLocation", "(JLjava/lang/String;Landroid/location/Location;)V",
            (void*) android_location_ElLocationManagerProxy_nativeSetTestProviderLocation},
    { "nativeClearTestProviderLocation", "(JLjava/lang/String;)V",
            (void*) android_location_ElLocationManagerProxy_nativeClearTestProviderLocation},
    { "nativeSetTestProviderEnabled", "(JLjava/lang/String;Z)V",
            (void*) android_location_ElLocationManagerProxy_nativeSetTestProviderEnabled},
    { "nativeClearTestProviderEnabled", "(JLjava/lang/String;)V",
            (void*) android_location_ElLocationManagerProxy_nativeClearTestProviderEnabled},
    { "nativeSetTestProviderStatus", "(JLjava/lang/String;ILandroid/os/Bundle;J)V",
            (void*) android_location_ElLocationManagerProxy_nativeSetTestProviderStatus},
    { "nativeClearTestProviderStatus", "(JLjava/lang/String;)V",
            (void*) android_location_ElLocationManagerProxy_nativeClearTestProviderStatus},
};

int register_android_location_ElLocationManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/location/ElLocationManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // end namespace android
