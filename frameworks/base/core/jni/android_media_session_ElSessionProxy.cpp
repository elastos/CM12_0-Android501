
#define LOG_TAG "ElSessionProxyJNI"
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
//#include <elastos/core/Object.h>
//#include <elastos/core/AutoLock.h>
//#include <elastos/utility/etl/HashMap.h>
#include <Elastos.Droid.App.h>
//#include <Elastos.Droid.Telephony.h>
//#include <Elastos.Droid.Internal.h>
//#include <Elastos.Droid.Os.h>
//#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.Droid.Media.h>
#include <utils/Log.h>

using android::ElUtil;
using Elastos::Droid::App::IPendingIntent;
using Elastos::Droid::Media::IMediaMetadata;
using Elastos::Droid::Media::Session::IPlaybackState;
using Elastos::Droid::Media::Session::IISession;
using Elastos::Droid::Media::Session::IISessionController;


static void android_media_session_ElSessionProxy_nativeSendEvent(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring event,
    /* [in] */ jobject data)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static jobject android_media_session_ElSessionProxy_nativeGetController(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    IISession* session = (IISession*)jproxy;

    AutoPtr<IISessionController> controller;
    session->GetController((IISessionController**)&controller);

    if (controller != NULL)
    {
        jclass c = env->FindClass("android/media/session/ElSessionControllerProxy");
        ElUtil::CheckErrorAndLog(env, "FindClass: ElSessionControllerProxy: %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(c, "<init>", "(J)V");
        ElUtil::CheckErrorAndLog(env, "GetMethodID: ElSessionControllerProxy: %d!\n", __LINE__);

        jobject obj = env->NewObject(c, m, (jlong)controller.Get());
        ElUtil::CheckErrorAndLog(env, "NewObject: ElWindowSessionProxy : %d!\n", __LINE__);
        controller->AddRef();

        env->DeleteLocalRef(c);
        return obj;
    }
    ALOGD("android_media_session_ElSessionProxy.cpp line:%d, controller:%p", __LINE__, controller.Get());
    return NULL;
}

static void android_media_session_ElSessionProxy_nativeSetFlags(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jint flags)
{
    //ALOGD("ongoing,  android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
    IISession* session = (IISession*)jproxy;
    session->SetFlags((Int32)flags);
}

static void android_media_session_ElSessionProxy_nativeSetActive(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jboolean active)
{
    //ALOGD("ongoing,  android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
    IISession* session = (IISession*)jproxy;
    session->SetActive((Boolean)active);
}

static void android_media_session_ElSessionProxy_nativeSetMediaButtonReceiver(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject jmbr)
{
    //ALOGD("ongoing,  android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
    IISession* session = (IISession*)jproxy;
    AutoPtr<IPendingIntent> mbr;
    ElUtil::ToElPendingIntent(env, jmbr, (IPendingIntent**)&mbr);
    session->SetMediaButtonReceiver(mbr);
}

static void android_media_session_ElSessionProxy_nativeSetLaunchPendingIntent(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject pi)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeDestroy(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, ongoing,  android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
    IISession* session = (IISession*)jproxy;
    session->Destroy();
}

static void android_media_session_ElSessionProxy_nativeSetMetadata(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject jmetadata)
{
    //ALOGD("ongoing,  android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
    IISession* session = (IISession*)jproxy;

    AutoPtr<IMediaMetadata> metadata;
    if (jmetadata != NULL) {
        if(!ElUtil::ToElMediaMetadata(env, jmetadata, (IMediaMetadata**)&metadata)) {
            ALOGE("android_media_session_ElSessionProxy_nativeSetMetadata() ToElBundle fail!");
        }
    }
    session->SetMetadata(metadata);
}

static void android_media_session_ElSessionProxy_nativeSetPlaybackState(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject jstate)
{
    //ALOGD("android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
    IISession* session = (IISession*)jproxy;

    AutoPtr<IPlaybackState> state;
    if (jstate != NULL) {
        if(!ElUtil::ToElPlaybackState(env, jstate, (IPlaybackState**)&state)) {
            ALOGE("android_media_session_ElSessionProxy_nativeSetMetadata() ToElPlaybackState fail!");
        }
    }
    session->SetPlaybackState(state);
}

static void android_media_session_ElSessionProxy_nativeSetQueue(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject queue)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeSetQueueTitle(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject title)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeSetExtras(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject extras)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeSetRatingType(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jint type)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativePlayItemResponse(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jboolean success)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeUpdateNowPlayingEntries(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jlongArray playList)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeUpdateFolderInfoBrowsedPlayer(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jstring stringUri)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeUpdateNowPlayingContentChange(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeSetPlaybackToLocal(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jobject attributes)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeSetPlaybackToRemote(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jint control,
    /* [in] */ jint max)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

static void android_media_session_ElSessionProxy_nativeSetCurrentVolume(
    /* [in] */ JNIEnv *env,
    /* [in] */ jobject clazz,
    /* [in] */ jlong jproxy,
    /* [in] */ jint currentVolume)
{
    ALOGD("TODO leliang, no impl, android_media_session_ElSessionProxy.cpp line:%d", __LINE__);
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeSendEvent",      "(JLjava/lang/String;Landroid/os/Bundle;)V",
            (void*) android_media_session_ElSessionProxy_nativeSendEvent},
    { "nativeGetController",      "(J)Landroid/media/session/ISessionController;",
            (void*) android_media_session_ElSessionProxy_nativeGetController},
    { "nativeSetFlags",      "(JI)V",
            (void*) android_media_session_ElSessionProxy_nativeSetFlags},
    { "nativeSetActive",      "(JZ)V",
            (void*) android_media_session_ElSessionProxy_nativeSetActive},
    { "nativeSetMediaButtonReceiver",      "(JLandroid/app/PendingIntent;)V",
            (void*) android_media_session_ElSessionProxy_nativeSetMediaButtonReceiver},
    { "nativeSetLaunchPendingIntent",      "(JLandroid/app/PendingIntent;)V",
            (void*) android_media_session_ElSessionProxy_nativeSetLaunchPendingIntent},
    { "nativeDestroy",      "(J)V",
            (void*) android_media_session_ElSessionProxy_nativeDestroy},
    { "nativeSetMetadata",      "(JLandroid/media/MediaMetadata;)V",
            (void*) android_media_session_ElSessionProxy_nativeSetMetadata},
    { "nativeSetPlaybackState",      "(JLandroid/media/session/PlaybackState;)V",
            (void*) android_media_session_ElSessionProxy_nativeSetPlaybackState},
    { "nativeSetQueue",      "(JLandroid/content/pm/ParceledListSlice;)V",
            (void*) android_media_session_ElSessionProxy_nativeSetQueue},
    { "nativeSetQueueTitle",      "(JLjava/lang/CharSequence;)V",
            (void*) android_media_session_ElSessionProxy_nativeSetQueueTitle},
    { "nativeSetExtras",      "(JLandroid/os/Bundle;)V",
            (void*) android_media_session_ElSessionProxy_nativeSetExtras},
    { "nativeSetRatingType",      "(JI)V",
            (void*) android_media_session_ElSessionProxy_nativeSetRatingType},
    { "nativePlayItemResponse",      "(JZ)V",
            (void*) android_media_session_ElSessionProxy_nativePlayItemResponse},
    { "nativeUpdateNowPlayingEntries",      "(J[J)V",
            (void*) android_media_session_ElSessionProxy_nativeUpdateNowPlayingEntries},
    { "nativeUpdateFolderInfoBrowsedPlayer",      "(JLjava/lang/String;)V",
            (void*) android_media_session_ElSessionProxy_nativeUpdateFolderInfoBrowsedPlayer},
    { "nativeUpdateNowPlayingContentChange",      "(J)V",
            (void*) android_media_session_ElSessionProxy_nativeUpdateNowPlayingContentChange},
    { "nativeSetPlaybackToLocal",      "(JLandroid/media/AudioAttributes;)V",
            (void*) android_media_session_ElSessionProxy_nativeSetPlaybackToLocal},
    { "nativeSetPlaybackToRemote",      "(JII)V",
            (void*) android_media_session_ElSessionProxy_nativeSetPlaybackToRemote},
    { "nativeSetCurrentVolume",      "(JI)V",
            (void*) android_media_session_ElSessionProxy_nativeSetCurrentVolume},
};

int register_android_media_session_ElSessionProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/media/session/ElSessionProxy",
        gMethods, NELEM(gMethods));
}

}; // end namespace android
