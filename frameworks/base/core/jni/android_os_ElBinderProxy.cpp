
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Os::IBinder;

static void android_os_ElBinderProxy_nativeFinalize(JNIEnv* env, jobject clazz, jlong jtoken)
{
    // ALOGD("+ android_os_ElBinderProxy_nativeFinalize()");

    IBinder* token = (IBinder*)jtoken;
    token->Release();

    // ALOGD("- android_os_ElBinderProxy_nativeFinalize()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",    "(J)V",
            (void*) android_os_ElBinderProxy_nativeFinalize },
};

int register_android_os_ElBinderProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/os/ElBinderProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android
