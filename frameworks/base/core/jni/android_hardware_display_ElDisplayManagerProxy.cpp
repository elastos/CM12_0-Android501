
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Hardware.h>
#include <Elastos.Droid.View.h>
#include <_Elastos.Droid.JavaProxy.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::JavaProxy::CDisplayManagerCallbackNative;
using Elastos::Droid::Hardware::Display::IIDisplayManagerCallback;;
using Elastos::Droid::Hardware::Display::IIDisplayManager;
using Elastos::Droid::Hardware::Display::IWifiDisplayStatus;
using Elastos::Droid::View::IDisplayInfo;

static jobject android_hardware_display_ElDisplayManagerProxy_nativeGetDisplayInfo(JNIEnv* env, jobject clazz, jlong jproxy,
    jint jdisplayId)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeGetDisplayInfo()");

    jobject jdisplayInfo = NULL;

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    AutoPtr<IDisplayInfo> disInfo;
    dm->GetDisplayInfo((Int32)jdisplayId, (IDisplayInfo**)&disInfo);
    if (disInfo != NULL) {
        jdisplayInfo = ElUtil::GetJavaDisplayInfo(env, disInfo);
    }

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeGetDisplayInfo()");
    return jdisplayInfo;
}

static void android_hardware_display_ElDisplayManagerProxy_nativeRegisterCallback(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jcallback)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeRegisterCallback()");

    JavaVM* jvm;
    env->GetJavaVM(&jvm);
    jobject jInstance = env->NewGlobalRef(jcallback);
    AutoPtr<IIDisplayManagerCallback> callback;
    ECode ec = CDisplayManagerCallbackNative::New((Handle64)jvm, (Handle64)jInstance, (IIDisplayManagerCallback**)&callback);
    if (FAILED(ec)) {
        ALOGD("====== CDisplayManagerCallbackNative::New ec: 0x%x =====\n", ec);
    }

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    dm->RegisterCallback(callback);

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeGetDisplayInfo()");
}

static jintArray android_hardware_display_ElDisplayManagerProxy_nativeGetDisplayIds(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeGetDisplayIds()");

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    AutoPtr<ArrayOf<Int32> > ids;
    ECode ec = dm->GetDisplayIds((ArrayOf<Int32>**)&ids);
    if (FAILED(ec))
        ALOGE("nativeGetDisplayIds() ec: 0x%08x", ec);

    jintArray jids = NULL;
    if (ids != NULL) {
        jids = ElUtil::GetJavaIntArray(env, ids);
    }

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeGetDisplayIds()");
    return jids;
}

static void android_hardware_display_ElDisplayManagerProxy_nativeStartWifiDisplayScan(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeStartWifiDisplayScan()");

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    ECode ec = dm->StartWifiDisplayScan();
    if (FAILED(ec))
        ALOGE("nativeStartWifiDisplayScan() ec: 0x%08x", ec);

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeStartWifiDisplayScan()");
}

static void android_hardware_display_ElDisplayManagerProxy_nativeStopWifiDisplayScan(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeStopWifiDisplayScan()");

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    ECode ec = dm->StopWifiDisplayScan();
    if (FAILED(ec))
        ALOGE("nativeStopWifiDisplayScan() ec: 0x%08x", ec);

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeStopWifiDisplayScan()");
}

static void android_hardware_display_ElDisplayManagerProxy_nativeConnectWifiDisplay(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jaddress)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeConnectWifiDisplay()");

    String address = ElUtil::ToElString(env, jaddress);

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    ECode ec = dm->ConnectWifiDisplay(address);
    if (FAILED(ec))
        ALOGE("nativeConnectWifiDisplay() ec: 0x%08x", ec);

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeConnectWifiDisplay()");
}

static void android_hardware_display_ElDisplayManagerProxy_nativeDisconnectWifiDisplay(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeDisconnectWifiDisplay()");

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    ECode ec = dm->DisconnectWifiDisplay();
    if (FAILED(ec))
        ALOGE("nativeDisconnectWifiDisplay() ec: 0x%08x", ec);

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeDisconnectWifiDisplay()");
}

static void android_hardware_display_ElDisplayManagerProxy_nativeRenameWifiDisplay(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jaddress, jstring jalias)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeRenameWifiDisplay()");

    String address = ElUtil::ToElString(env, jaddress);
    String alias = ElUtil::ToElString(env, jalias);

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    ECode ec = dm->RenameWifiDisplay(address, alias);
    if (FAILED(ec))
        ALOGE("nativeRenameWifiDisplay() ec: 0x%08x", ec);

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeRenameWifiDisplay()");
}

static void android_hardware_display_ElDisplayManagerProxy_nativeForgetWifiDisplay(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jaddress)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeForgetWifiDisplay()");

    String address = ElUtil::ToElString(env, jaddress);

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    ECode ec = dm->ForgetWifiDisplay(address);
    if (FAILED(ec))
        ALOGE("nativeForgetWifiDisplay() ec: 0x%08x", ec);

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeForgetWifiDisplay()");
}

static void android_hardware_display_ElDisplayManagerProxy_nativePauseWifiDisplay(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativePauseWifiDisplay()");

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    ECode ec = dm->PauseWifiDisplay();
    if (FAILED(ec))
        ALOGE("nativePauseWifiDisplay() ec: 0x%08x", ec);

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativePauseWifiDisplay()");
}

static void android_hardware_display_ElDisplayManagerProxy_nativeResumeWifiDisplay(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeResumeWifiDisplay()");

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    ECode ec = dm->ResumeWifiDisplay();
    if (FAILED(ec))
        ALOGE("nativeResumeWifiDisplay() ec: 0x%08x", ec);

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeResumeWifiDisplay()");
}

static jobject android_hardware_display_ElDisplayManagerProxy_nativeGetWifiDisplayStatus(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeGetWifiDisplayStatus()");

    IIDisplayManager* dm = (IIDisplayManager*)jproxy;
    AutoPtr<IWifiDisplayStatus> status;
    ECode ec = dm->GetWifiDisplayStatus((IWifiDisplayStatus**)&status);
    if (FAILED(ec))
        ALOGE("nativeGetWifiDisplayStatus() ec: 0x%08x", ec);

    jobject jstatus = NULL;
    if (status != NULL) {
        jstatus = ElUtil::GetJavaWifiDisplayStatus(env, status);
    }

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeGetWifiDisplayStatus()");
    return jstatus;
}

static void android_hardware_display_ElDisplayManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_hardware_display_ElDisplayManagerProxy_nativeDestroy()");

    IIDisplayManager* obj = (IIDisplayManager*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_hardware_display_ElDisplayManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeFinalize },
    { "nativeGetDisplayInfo",    "(JI)Landroid/view/DisplayInfo;",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeGetDisplayInfo },
    { "nativeRegisterCallback",    "(JLandroid/hardware/display/IDisplayManagerCallback;)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeRegisterCallback },
    { "nativeGetDisplayIds",    "(J)[I",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeGetDisplayIds },
    { "nativeStartWifiDisplayScan",    "(J)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeStartWifiDisplayScan },
    { "nativeStopWifiDisplayScan",    "(J)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeStopWifiDisplayScan },
    { "nativeConnectWifiDisplay",    "(JLjava/lang/String;)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeConnectWifiDisplay },
    { "nativeDisconnectWifiDisplay",    "(J)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeDisconnectWifiDisplay },
    { "nativeRenameWifiDisplay",    "(JLjava/lang/String;Ljava/lang/String;)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeRenameWifiDisplay },
    { "nativeForgetWifiDisplay",    "(JLjava/lang/String;)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeForgetWifiDisplay },
    { "nativePauseWifiDisplay",    "(J)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativePauseWifiDisplay },
    { "nativeResumeWifiDisplay",    "(J)V",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeResumeWifiDisplay },
    { "nativeGetWifiDisplayStatus",    "(J)Landroid/hardware/display/WifiDisplayStatus;",
            (void*) android_hardware_display_ElDisplayManagerProxy_nativeGetWifiDisplayStatus },
};

int register_android_hardware_display_ElDisplayManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/hardware/display/ElDisplayManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

