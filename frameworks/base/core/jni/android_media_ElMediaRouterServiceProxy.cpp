
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include "android_runtime/AndroidRuntime.h"

#include <utils/Log.h>
#include <utils/Mutex.h>
#include <elastos/utility/etl/HashMap.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.Droid.Media.h>

using android::ElUtil;
using android::AndroidRuntime;
using android::Mutex;
using Elastos::Droid::Media::IIMediaRouterService;
using Elastos::Droid::Media::IIMediaRouterClient;
using Elastos::Droid::Media::IMediaRouterClientState;
using Elastos::Droid::JavaProxy::CIMediaRouterClientNative;
using Elastos::Utility::Etl::HashMap;

static Mutex sClientsLock;
static HashMap<Int32, AutoPtr<IIMediaRouterClient> > sClients;

static void android_media_ElMediaRouterServiceProxy_nativeRegisterClientAsUser(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclient, jstring jpackageName, jint userId)
{
    // ALOGD("+ android_media_ElMediaRouterServiceProxy_nativeRegisterClientAsUser()");

    AutoPtr<IIMediaRouterClient> client;
    if (jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);

        Mutex::Autolock lock(sClientsLock);
        HashMap<Int32, AutoPtr<IIMediaRouterClient> >::Iterator it = sClients.Find(hashCode);

        if (it != sClients.End()) {
            client = it->mSecond;
        }

        if (client == NULL) {
            JavaVM* jvm = NULL;
            env->GetJavaVM(&jvm);
            jobject jInstance = env->NewGlobalRef(jclient);
            CIMediaRouterClientNative::New((Handle64)jvm, (Handle64)jInstance, (IIMediaRouterClient**)&client);
            // Warn: Here we stored the obj, but did not remove
            sClients[hashCode] = client;
        }
    }

    String packageName = ElUtil::ToElString(env, jpackageName);

    IIMediaRouterService* mrs = (IIMediaRouterService*)jproxy;
    mrs->RegisterClientAsUser(client, packageName, userId);

    // ALOGD("- android_media_ElMediaRouterServiceProxy_nativeRegisterClientAsUser()");
}

static void android_media_ElMediaRouterServiceProxy_nativeUnregisterClient(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclient)
{
    // ALOGD("+ android_media_ElMediaRouterServiceProxy_nativeUnregisterClient()");

    AutoPtr<IIMediaRouterClient> client;
    if (jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);

        Mutex::Autolock lock(sClientsLock);
        HashMap<Int32, AutoPtr<IIMediaRouterClient> >::Iterator it = sClients.Find(hashCode);

        if (it != sClients.End()) {
            client = it->mSecond;
        }
        if (client == NULL) {
            ALOGE("nativeUnregisterClient client == NULL !");
            return;
        }
        sClients.Erase(it);
    }
    IIMediaRouterService* mrs = (IIMediaRouterService*)jproxy;
    mrs->UnregisterClient(client);

    // ALOGD("- android_media_ElMediaRouterServiceProxy_nativeUnregisterClient()");
}

static jobject android_media_ElMediaRouterServiceProxy_nativeGetState(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclient)
{
    // ALOGD("+ android_media_ElMediaRouterServiceProxy_nativeGetState()");

    AutoPtr<IIMediaRouterClient> client;
    if (jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);

        Mutex::Autolock lock(sClientsLock);
        HashMap<Int32, AutoPtr<IIMediaRouterClient> >::Iterator it = sClients.Find(hashCode);

        if (it != sClients.End()) {
            client = it->mSecond;
        }
    }
    if (client == NULL) {
        ALOGE("nativeGetState client == NULL !");
        return NULL;
    }

    IIMediaRouterService* mrs = (IIMediaRouterService*)jproxy;
    AutoPtr<IMediaRouterClientState> state;
    mrs->GetState(client, (IMediaRouterClientState**)&state);

    jobject jstate = NULL;
    if (state != NULL) {
        jstate = ElUtil::GetJavaMediaRouterClientState(env, state);
    }
    // ALOGD("- android_media_ElMediaRouterServiceProxy_nativeGetState()");
    return jstate;
}

static void android_media_ElMediaRouterServiceProxy_nativeSetDiscoveryRequest(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclient, jint routeTypes, jboolean activeScan)
{
    // ALOGD("+ android_media_ElMediaRouterServiceProxy_nativeSetDiscoveryRequest()");

    AutoPtr<IIMediaRouterClient> client;
    if (jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);

        Mutex::Autolock lock(sClientsLock);
        HashMap<Int32, AutoPtr<IIMediaRouterClient> >::Iterator it = sClients.Find(hashCode);

        if (it != sClients.End()) {
            client = it->mSecond;
        }
    }
    if (client == NULL) {
        ALOGE("nativeGetState client == NULL !");
        return;
    }

    IIMediaRouterService* mrs = (IIMediaRouterService*)jproxy;
    mrs->SetDiscoveryRequest(client, routeTypes, activeScan);

    // ALOGD("- android_media_ElMediaRouterServiceProxy_nativeSetDiscoveryRequest()");
}

static void android_media_ElMediaRouterServiceProxy_nativeSetSelectedRoute(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclient, jstring jrouteId, jboolean expl)
{
    // ALOGD("+ android_media_ElMediaRouterServiceProxy_nativeSetSelectedRoute()");

    AutoPtr<IIMediaRouterClient> client;
    if (jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);

        Mutex::Autolock lock(sClientsLock);
        HashMap<Int32, AutoPtr<IIMediaRouterClient> >::Iterator it = sClients.Find(hashCode);

        if (it != sClients.End()) {
            client = it->mSecond;
        }
    }
    if (client == NULL) {
        ALOGE("nativeGetState client == NULL !");
        return;
    }

    String routeId = ElUtil::ToElString(env, jrouteId);

    IIMediaRouterService* mrs = (IIMediaRouterService*)jproxy;
    mrs->SetSelectedRoute(client, routeId, expl);

    // ALOGD("- android_media_ElMediaRouterServiceProxy_nativeSetSelectedRoute()");
}

static void android_media_ElMediaRouterServiceProxy_nativeRequestSetVolume(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclient, jstring jrouteId, jint volume)
{
    // ALOGD("+ android_media_ElMediaRouterServiceProxy_nativeRequestSetVolume()");

    AutoPtr<IIMediaRouterClient> client;
    if (jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);

        Mutex::Autolock lock(sClientsLock);
        HashMap<Int32, AutoPtr<IIMediaRouterClient> >::Iterator it = sClients.Find(hashCode);

        if (it != sClients.End()) {
            client = it->mSecond;
        }
    }
    if (client == NULL) {
        ALOGE("nativeGetState client == NULL !");
        return;
    }

    String routeId = ElUtil::ToElString(env, jrouteId);

    IIMediaRouterService* mrs = (IIMediaRouterService*)jproxy;
    mrs->RequestSetVolume(client, routeId, volume);

    // ALOGD("- android_media_ElMediaRouterServiceProxy_nativeRequestSetVolume()");
}

static void android_media_ElMediaRouterServiceProxy_nativeRequestUpdateVolume(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jclient, jstring jrouteId, jint direction)
{
    // ALOGD("+ android_media_ElMediaRouterServiceProxy_nativeRequestUpdateVolume()");

    AutoPtr<IIMediaRouterClient> client;
    if (jclient != NULL) {
        Int32 hashCode = ElUtil::GetJavaHashCode(env, jclient);

        Mutex::Autolock lock(sClientsLock);
        HashMap<Int32, AutoPtr<IIMediaRouterClient> >::Iterator it = sClients.Find(hashCode);

        if (it != sClients.End()) {
            client = it->mSecond;
        }
    }
    if (client == NULL) {
        ALOGE("nativeGetState client == NULL !");
        return;
    }

    String routeId = ElUtil::ToElString(env, jrouteId);

    IIMediaRouterService* mrs = (IIMediaRouterService*)jproxy;
    mrs->RequestUpdateVolume(client, routeId, direction);

    // ALOGD("- android_media_ElMediaRouterServiceProxy_nativeRequestUpdateVolume()");
}


static void android_media_ElMediaRouterServiceProxy_nativeFinalize(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_media_ElMediaRouterServiceProxy_nativeDestroy()");

    IIMediaRouterService* obj = (IIMediaRouterService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_media_ElMediaRouterServiceProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_media_ElMediaRouterServiceProxy_nativeFinalize },
    { "nativeRegisterClientAsUser",    "(JLandroid/media/IMediaRouterClient;Ljava/lang/String;I)V",
            (void*) android_media_ElMediaRouterServiceProxy_nativeRegisterClientAsUser },
    { "nativeUnregisterClient",    "(JLandroid/media/IMediaRouterClient;)V",
            (void*) android_media_ElMediaRouterServiceProxy_nativeUnregisterClient },
    { "nativeGetState",    "(JLandroid/media/IMediaRouterClient;)Landroid/media/MediaRouterClientState;",
            (void*) android_media_ElMediaRouterServiceProxy_nativeGetState },
    { "nativeSetDiscoveryRequest",    "(JLandroid/media/IMediaRouterClient;IZ)V",
            (void*) android_media_ElMediaRouterServiceProxy_nativeSetDiscoveryRequest },
    { "nativeSetSelectedRoute",    "(JLandroid/media/IMediaRouterClient;Ljava/lang/String;Z)V",
            (void*) android_media_ElMediaRouterServiceProxy_nativeSetSelectedRoute },
    { "nativeRequestSetVolume",    "(JLandroid/media/IMediaRouterClient;Ljava/lang/String;I)V",
            (void*) android_media_ElMediaRouterServiceProxy_nativeRequestSetVolume },
    { "nativeRequestUpdateVolume",    "(JLandroid/media/IMediaRouterClient;Ljava/lang/String;I)V",
            (void*) android_media_ElMediaRouterServiceProxy_nativeRequestUpdateVolume },
};

int register_android_media_ElMediaRouterServiceProxy(JNIEnv *env) {
    return AndroidRuntime::registerNativeMethods(env, "android/media/ElMediaRouterServiceProxy",
                gMethods, NELEM(gMethods));
}

}; // namespace android
