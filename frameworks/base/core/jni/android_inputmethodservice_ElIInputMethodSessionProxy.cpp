
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.View.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <elastos/utility/etl/HashMap.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::JavaProxy::CInputMethodCallbackNative;

using Elastos::Droid::Internal::View::IIInputMethodSession;
using Elastos::Droid::View::IKeyEvent;
using Elastos::Droid::View::IInputMethodCallback;
using Elastos::Droid::View::InputMethod::ICompletionInfo;
using Elastos::Utility::Etl::HashMap;

static HashMap<Int32, AutoPtr<IInputMethodCallback> > sCallbackMap;
static Mutex sCallbackMapLock;

static void android_inputmethodservice_ElIInputMethodSessionProxy_nativeViewClicked(JNIEnv* env, jobject clazz, jlong jproxy, jboolean jfocusChanged)
{
    // ALOGD("+ android_inputmethodservice_ElIInputMethodSessionProxy_nativeViewClicked()");

    IIInputMethodSession* inSession = (IIInputMethodSession*)jproxy;

    inSession->ViewClicked((Boolean)jfocusChanged);
    // ALOGD("- android_inputmethodservice_ElIInputMethodSessionProxy_nativeViewClicked()");
}

static void android_inputmethodservice_ElIInputMethodSessionProxy_nativeUpdateSelection(JNIEnv* env, jobject clazz, jlong jproxy,
        jint joldSelStart, jint joldSelEnd, jint jnewSelStart, jint jnewSelEnd, jint jcandidatesStart, jint jcandidatesEnd)
{
    // ALOGD("+ android_inputmethodservice_ElIInputMethodSessionProxy_nativeUpdateSelection()");

    IIInputMethodSession* inSession = (IIInputMethodSession*)jproxy;

    inSession->UpdateSelection((jint)joldSelStart, (jint)joldSelEnd, (jint)jnewSelStart, (jint)jnewSelEnd, (jint)jcandidatesStart, (jint)jcandidatesEnd);
    // ALOGD("- android_inputmethodservice_ElIInputMethodSessionProxy_nativeUpdateSelection()");
}

static void android_inputmethodservice_ElIInputMethodSessionProxy_nativeDisplayCompletions(JNIEnv* env, jobject clazz, jlong jproxy, jobjectArray jcompletions)
{
    // ALOGD("+ android_inputmethodservice_ElIInputMethodSessionProxy_nativeDisplayCompletions()");

    IIInputMethodSession* inSession = (IIInputMethodSession*)jproxy;

    AutoPtr<ArrayOf<ICompletionInfo*> > completions;
    if (jcompletions != NULL) {
        int size = env->GetArrayLength(jcompletions);
        completions = ArrayOf<ICompletionInfo*>::Alloc(size);

        for(int i = 0; i < size; i++) {
            jobject jcompletion = env->GetObjectArrayElement(jcompletions, i);
            ElUtil::CheckErrorAndLog(env, "nativeDisplayCompletions(); GetObjectArrayelement failed : %d!\n", __LINE__);

            AutoPtr<ICompletionInfo> completion;
            if(!ElUtil::ToElCompletionInfo(env, jcompletion, (ICompletionInfo**)&completion)) {
                ALOGE("nativeDisplayCompletions() ToElCompletionInfo fail!");
            }

            env->DeleteLocalRef(jcompletion);
        }
    }

    inSession->DisplayCompletions(completions);

    // ALOGD("- android_inputmethodservice_ElIInputMethodSessionProxy_nativeDisplayCompletions()");
}

static void android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jint token)
{
    ALOGD("+ android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinalize()");

    IIInputMethodSession* osb = (IIInputMethodSession*)token;
    if (osb != NULL) {
        osb->Release();
    }

    ALOGD("- android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinalize()");
}

static void android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinishInput(JNIEnv* env, jobject clazz,
                                                   jint token)
{
    ALOGD("+ android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinishInput()");

    IIInputMethodSession* osb = (IIInputMethodSession*)token;
    if (osb != NULL) {
        osb->FinishInput();
    }

    ALOGD("- android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinishInput()");
}

static void android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinishSession(JNIEnv* env, jobject clazz,
                                                   jint token)
{
    ALOGD("+ android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinishSession()");

    IIInputMethodSession* osb = (IIInputMethodSession*)token;
    if (osb != NULL) {
        osb->FinishSession();
    }

    ALOGD("- android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinishSession()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinalize },
    { "nativeViewClicked",    "(JZ)V",
            (void*) android_inputmethodservice_ElIInputMethodSessionProxy_nativeViewClicked },
    { "nativeUpdateSelection",    "(JIIIIII)V",
            (void*) android_inputmethodservice_ElIInputMethodSessionProxy_nativeUpdateSelection },
    { "nativeDisplayCompletions",    "(J[Landroid/view/inputmethod/CompletionInfo;)V",
            (void*) android_inputmethodservice_ElIInputMethodSessionProxy_nativeDisplayCompletions },
    { "nativeFinishInput",      "(J)V",
            (void*) android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinishInput },
    { "nativeFinishSession",      "(J)V",
            (void*) android_inputmethodservice_ElIInputMethodSessionProxy_nativeFinishSession },
};

int register_android_inputmethodservice_ElIInputMethodSessionProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/inputmethodservice/ElIInputMethodSessionProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

