#define LOG_TAG "ElAppWidgetManagerProxyJNI"

#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.App.h>
#include <Elastos.Droid.AppWidget.h>
#include <Elastos.Droid.Content.h>
#include <Elastos.Droid.Internal.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Widget.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <Elastos.CoreLibrary.Utility.h>


#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Content::IComponentName;
using Elastos::Droid::Internal::AppWidget::IIAppWidgetHost;
using Elastos::Droid::Internal::AppWidget::IIAppWidgetService;
using Elastos::Droid::Internal::Widget::IIRemoteViewsAdapterConnection;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::JavaProxy::CAppWidgetHostCallbacksNative;
using Elastos::Droid::JavaProxy::CRemoteViewsAdapterConnectionNative;

static jintArray android_appwidget_ElAppWidgetManagerProxy_nativeStartListening(JNIEnv* env, jobject clazz, jlong jproxy, jobject jhost,
        jstring jcallingPackage, jint jhostId, jobject jupdatedViews)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeStartListening()");

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;

    AutoPtr<IIAppWidgetHost> host;
    JavaVM* jvm = NULL;
    env->GetJavaVM(&jvm);
    CAppWidgetHostCallbacksNative::New((Handle64)jvm, (Handle64)jhost, (IIAppWidgetHost**)&host);

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<ArrayOf<Int32> > values;
    AutoPtr<IList> updatedViews;

    jclass alistKlass = env->GetObjectClass(jupdatedViews);
    ElUtil::CheckErrorAndLog(env, "GetObjectClass: ArrayList : %d!\n", __LINE__);

    jmethodID mSize = env->GetMethodID(alistKlass, "size", "()I");
    ElUtil::CheckErrorAndLog(env, "GetMethodID: get : %d!\n", __LINE__);
    env->DeleteLocalRef(alistKlass);

    jint jsize = env->CallIntMethod(jupdatedViews, mSize);
    ElUtil::CheckErrorAndLog(env, "CallIntMethod: jupdatedViews mSize : %d!\n", __LINE__);

    appwidgetService->StartListening(host, callingPackage, (Int32)jhostId, (IList**)&updatedViews, (ArrayOf<Int32>**)&values);

    if (jupdatedViews == NULL) {
        ALOGE("android_appwidget_ElAppWidgetManagerProxy_nativeStartListening(): jupdatedViews is NULL");
    }
    else {
        // Update java jupdatedViews
        Int32 count = 0;
        updatedViews->GetSize(&count);

        Int32 length = values->GetLength();
        ALOGD("StartListening(): id length = %d; view size = %d", length, count);

        jclass listKlass = env->GetObjectClass(jupdatedViews);
        if (length != count) {
            jmethodID mclear = env->GetMethodID(listKlass, "clear", "()V");
            ElUtil::CheckErrorAndLog(env, "nativeStartListening Fail GetMethodID: clear : %d!\n", __LINE__);
            env->CallVoidMethod(jupdatedViews, mclear);
            ElUtil::CheckErrorAndLog(env, "nativeStartListening Fail CallVoidMethod: clear : %d!\n", __LINE__);
            // add null remoteview to list
            jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
            ElUtil::CheckErrorAndLog(env, "nativeStartListening Fail GetMethodID: add : %d!\n", __LINE__);

            for (Int32 i = 0; i < length; i++) {
                env->CallBooleanMethod(jupdatedViews, mAdd, NULL);
                ElUtil::CheckErrorAndLog(env, "nativeStartListening Fail CallBooleanMethod: add : %d!\n", __LINE__);
            }

            jsize = env->CallIntMethod(jupdatedViews, mSize);
            ElUtil::CheckErrorAndLog(env, "CallIntMethod: jupdatedViews mSize : %d!\n", __LINE__);
        }
        else if (count > 0) {
            jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
            ElUtil::CheckErrorAndLog(env, "nativeStartListening Fail GetMethodID: add : %d!\n", __LINE__);

            for (Int32 i = 0; i < count; i++) {
                AutoPtr<IInterface> obj;
                updatedViews->Get(i, (IInterface**)&obj);
                AutoPtr<IRemoteViews> remoteViews = IRemoteViews::Probe(obj);

                jobject jremoteViews = NULL;
                if (remoteViews != NULL) {
                    jremoteViews = ElUtil::GetJavaRemoteViews(env, remoteViews);
                }
                else {
                    ALOGE("nativeStartListening(); remoteViews is null!");
                }

                env->CallBooleanMethod(jupdatedViews, mAdd, jremoteViews);
                ElUtil::CheckErrorAndLog(env, "nativeStartListening Fail CallBooleanMethod: mAdd : %d!\n", __LINE__);
                env->DeleteLocalRef(jremoteViews);
            }
        }

        env->DeleteLocalRef(listKlass);
    }

    jintArray jupdatedIds = ElUtil::GetJavaIntArray(env, values);
    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeStartListening()");
    return jupdatedIds;
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeStopListening(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jhostId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeStopListening()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->StopListening(callingPackage, (Int32)jhostId);
    if (FAILED(ec))
        ALOGD("StopListening(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeStopListening()");
}

static jobject android_appwidget_ElAppWidgetManagerProxy_nativeGetInstalledProvidersForProfile(
    JNIEnv* env, jobject clazz, jlong jproxy, jint jcategoryFilter, jint jprofileId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeGetInstalledProvidersForProfile()");
    jclass listKlass = env->FindClass("java/util/ArrayList");
    ElUtil::CheckErrorAndLog(env, "nativeGetInstalledProvidersForProfile Fail FindClass: ArrayList : %d!\n", __LINE__);

    jmethodID m = env->GetMethodID(listKlass, "<init>", "()V");
    ElUtil::CheckErrorAndLog(env, "nativeGetInstalledProvidersForProfile Fail GetMethodID: ArrayList : %d!\n", __LINE__);

    jobject jlist = env->NewObject(listKlass, m);
    ElUtil::CheckErrorAndLog(env, "nativeGetInstalledProvidersForProfile Fail NewObject: ArrayList : %d!\n", __LINE__);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    AutoPtr<IList> providers;
    appwidgetService->GetInstalledProvidersForProfile((Int32)jcategoryFilter, jprofileId, (IList**)&providers);

    Int32 count = 0;

    if (providers != NULL) {
        providers->GetSize(&count);
    }

    if (count > 0) {
        jmethodID mAdd = env->GetMethodID(listKlass, "add", "(Ljava/lang/Object;)Z");
        ElUtil::CheckErrorAndLog(env, "nativeGetInstalledProvidersForProfile Fail GetMethodID: add : %d!\n", __LINE__);

        for (Int32 i = 0; i < count; i++) {
            AutoPtr<IInterface> obj;
            providers->Get(i, (IInterface**)&obj);
            AutoPtr<IAppWidgetProviderInfo> providerInfo = IAppWidgetProviderInfo::Probe(obj);

            jobject jproviderinfo = ElUtil::GetJavaAppWidgetProviderInfo(env, providerInfo);

            env->CallBooleanMethod(jlist, mAdd, jproviderinfo);
            ElUtil::CheckErrorAndLog(env, "nativeGetInstalledProvidersForProfile Fail CallBooleanMethod: mAdd : %d!\n", __LINE__);
            env->DeleteLocalRef(jproviderinfo);
        }
    }

    env->DeleteLocalRef(listKlass);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeGetInstalledProvidersForProfile()");
    return jlist;
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeDeleteHost(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jhostId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeDeleteHost()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->DeleteHost(callingPackage, (Int32)jhostId);
    if (FAILED(ec))
        ALOGD("DeleteHost(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeDeleteHost()");
}

static jboolean android_appwidget_ElAppWidgetManagerProxy_nativeHasBindAppWidgetPermission(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jpackageName, jint juserId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeHasBindAppWidgetPermission()");

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    String packageName = ElUtil::ToElString(env, jpackageName);
    Boolean result = FALSE;
    ECode ec = appwidgetService->HasBindAppWidgetPermission(packageName, juserId, &result);
    if (FAILED(ec))
        ALOGD("HasBindAppWidgetPermission(), ec = %08x; result = %d", ec, result);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeHasBindAppWidgetPermission()");
    return (jboolean)result;
}

static jint android_appwidget_ElAppWidgetManagerProxy_nativeAllocateAppWidgetId(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jhostId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeAllocateAppWidgetId()");

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;

    Int32 widgetId = 0;
    ECode ec = appwidgetService->AllocateAppWidgetId(ElUtil::ToElString(env, jcallingPackage), (Int32)jhostId, &widgetId);
    if (FAILED(ec))
        ALOGD("AllocateAppWidgetId(), ec = %08x;", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeAllocateAppWidgetId()");
    return (jint)widgetId;
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeDeleteAppWidgetId(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jappWidgetId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeDeleteAppWidgetId()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->DeleteAppWidgetId(callingPackage, (Int32)jappWidgetId);
    if (FAILED(ec))
        ALOGD("DeleteAppWidgetId(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeDeleteAppWidgetId()");
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeDeleteAllHosts(JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeDeleteAllHosts()");

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->DeleteAllHosts();
    if (FAILED(ec))
        ALOGD("DeleteAllHosts(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeDeleteAllHosts()");
}

static jobject android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetViews(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jappWidgetId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetViews(), jappWidgetId = %d", jappWidgetId);

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    AutoPtr<IRemoteViews> remoteViews;

    ECode ec = appwidgetService->GetAppWidgetViews(callingPackage, (Int32)jappWidgetId, (IRemoteViews**)&remoteViews);
    if (FAILED(ec))
        ALOGD("GetAppWidgetViews(), ec = %08x", ec);

    jobject jremoteViews = NULL;

    if (remoteViews != NULL) {
        jremoteViews = ElUtil::GetJavaRemoteViews(env, remoteViews.Get());
    }
    else {
        ALOGE("nativeGetAppWidgetViews(); remoteViews is null!");
    }


    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetViews()");
    return jremoteViews;
}

static jintArray android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetIdsForHost(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jhostId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetIdsForHost()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    AutoPtr<ArrayOf<Int32> > ids;
    ECode ec = appwidgetService->GetAppWidgetIdsForHost(callingPackage, (Int32)jhostId, (ArrayOf<Int32>**)&ids);
    if (FAILED(ec))
        ALOGD("GetAppWidgetIdsForHost(), ec = %08x", ec);
    jintArray jIds = ElUtil::GetJavaIntArray(env, ids.Get());

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetIdsForHost()");
    return jIds;
}

static jobject android_appwidget_ElAppWidgetManagerProxy_nativeCreateAppWidgetConfigIntentSender(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jhostId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeCreateAppWidgetConfigIntentSender()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    AutoPtr<IIntentSender> sender;
    ECode ec = appwidgetService->CreateAppWidgetConfigIntentSender(callingPackage, (Int32)jhostId, (IIntentSender**)&sender);
    if (FAILED(ec))
        ALOGD("CreateAppWidgetConfigIntentSender(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeCreateAppWidgetConfigIntentSender()");
    return ElUtil::GetJavaIntentSender(env, sender);
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetIds(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jintArray jappWidgetIds, jobject jviews)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetIds()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<ArrayOf<Int32> > appWidgetId;
    AutoPtr<IRemoteViews> remoteView;

    ElUtil::ToElIntArray(env, jappWidgetIds, (ArrayOf<Int32>**)&appWidgetId);
    if (jviews == NULL) {
        ALOGE("ElAppWidgetManagerProxy_nativeUpdateAppWidgetIds, jviews is NULL");
    }
    else {
        ElUtil::ToElRemoteViews(env, jviews, (IRemoteViews**)&remoteView);
    }

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->UpdateAppWidgetIds(callingPackage, appWidgetId, remoteView);
    if (FAILED(ec))
        ALOGD("UpdateAppWidgetIds(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetIds()");
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetOptions(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jappWidgetId, jobject jextras)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetOptions()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    AutoPtr<IBundle> options;
    ElUtil::ToElBundle(env, jextras, (IBundle**)&options);
    ECode ec = appwidgetService->UpdateAppWidgetOptions(callingPackage, (Int32)jappWidgetId, options.Get());
    if (FAILED(ec))
        ALOGD("UpdateAppWidgetOptions(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetOptions()");
}

static jobject android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetOptions(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jappWidgetId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetOptions()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    AutoPtr<IBundle> options;
    ECode ec = appwidgetService->GetAppWidgetOptions(callingPackage, (Int32)jappWidgetId, (IBundle**)&options);
    if (FAILED(ec))
        ALOGD("GetAppWidgetOptions(), ec = %08x", ec);

    jobject jbundle = ElUtil::GetJavaBundle(env, options.Get());

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetOptions()");
    return jbundle;
}

static void android_appwidget_ElAppWidgetManagerProxy_nativePartiallyUpdateAppWidgetIds(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jintArray jappWidgetIds, jobject jviews)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativePartiallyUpdateAppWidgetIds()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<ArrayOf<Int32> > appWidgetId;
    AutoPtr<IRemoteViews> remoteView;

    ElUtil::ToElIntArray(env, jappWidgetIds, (ArrayOf<Int32>**)&appWidgetId);

    if (jviews == NULL) {
        ALOGE("ElAppWidgetManagerProxy_nativePartiallyUpdateAppWidgetIds(), jviews is NULL");
    }
    else {
        ElUtil::ToElRemoteViews(env, jviews, (IRemoteViews**)&remoteView);
    }

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->PartiallyUpdateAppWidgetIds(callingPackage, appWidgetId, remoteView);
    if (FAILED(ec))
        ALOGD("PartiallyUpdateAppWidgetIds(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativePartiallyUpdateAppWidgetIds()");
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetProvider(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jprovider, jobject jviews)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetProvider()");

    AutoPtr<IComponentName> provider;
    AutoPtr<IRemoteViews> remoteView;

    ElUtil::ToElComponentName(env, jprovider, (IComponentName**)&provider);
    if (jviews == NULL) {
        ALOGE("ElAppWidgetManagerProxy_nativeUpdateAppWidgetProvider, jviews is NULL");
    }
    else {
        ElUtil::ToElRemoteViews(env, jviews, (IRemoteViews**)&remoteView);
    }

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->UpdateAppWidgetProvider(provider, remoteView);
    if (FAILED(ec))
        ALOGD("UpdateAppWidgetProvider(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetProvider()");
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeNotifyAppWidgetViewDataChanged(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jintArray jappWidgetIds, jint jviewId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeNotifyAppWidgetViewDataChanged()");

    AutoPtr<ArrayOf<Int32> > appWidgetId;
        String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    ElUtil::ToElIntArray(env, jappWidgetIds, (ArrayOf<Int32>**)&appWidgetId);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->NotifyAppWidgetViewDataChanged(callingPackage, appWidgetId, (Int32)jviewId);
    if (FAILED(ec))
        ALOGD("NotifyAppWidgetViewDataChanged(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeNotifyAppWidgetViewDataChanged()");
}

static jobject android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetInfo(
    JNIEnv* env, jobject clazz, jlong jproxy, jstring jcallingPackage, jint jappWidgetId)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetInfo()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);

    AutoPtr<IAppWidgetProviderInfo> providerInfo;

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->GetAppWidgetInfo(callingPackage, (Int32)jappWidgetId, (IAppWidgetProviderInfo**)&providerInfo);
    if (FAILED(ec))
        ALOGD("GetAppWidgetInfo(), ec = %08x", ec);
    jobject jproviderinfo = ElUtil::GetJavaAppWidgetProviderInfo(env, providerInfo.Get());


    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetInfo()");
    return jproviderinfo;
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeSetBindAppWidgetPermission(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jpackageName, jint juserId, jboolean jpermission)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeSetBindAppWidgetPermission()");

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->SetBindAppWidgetPermission(ElUtil::ToElString(env, jpackageName), juserId, (Boolean)jpermission);
    if (FAILED(ec))
        ALOGD("SetBindAppWidgetPermission(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeSetBindAppWidgetPermission()");
}

static jboolean android_appwidget_ElAppWidgetManagerProxy_nativeBindAppWidgetId(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jcallingPackage, jint jappWidgetId, jint jproviderProfileId, jobject jprovider, jobject joptions)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeBindAppWidgetId()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);


    AutoPtr<IComponentName> provider;
    AutoPtr<IBundle> options;
    ElUtil::ToElBundle(env, joptions, (IBundle**)&options);
    ElUtil::ToElComponentName(env, jprovider, (IComponentName**)&provider);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    Boolean result;
    ECode ec = appwidgetService->BindAppWidgetId(callingPackage, (Int32)jappWidgetId, jproviderProfileId, provider, options, &result);
    if (FAILED(ec))
        ALOGD("BindAppWidgetId(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeBindAppWidgetId()");
    return result;
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeBindRemoteViewsService(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jcallingPackage, jint jappWidgetId, jobject jintent, jobject jconnection)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeBindRemoteViewsService()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);


    AutoPtr<IIntent> intent;
    AutoPtr<IIRemoteViewsAdapterConnection> connection;
    JavaVM* jvm = NULL;
    ElUtil::ToElIntent(env, jintent, (IIntent**)&intent);
    env->GetJavaVM(&jvm);
    CRemoteViewsAdapterConnectionNative::New((Handle64)jvm, (Handle64)jconnection, (IIRemoteViewsAdapterConnection**)&connection);

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->BindRemoteViewsService(callingPackage, (Int32)jappWidgetId, intent, IBinder::Probe(connection.Get()));
    if (FAILED(ec))
        ALOGD("BindRemoteViewsService(), ec = %08x", ec);

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeBindRemoteViewsService()");
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeUnbindRemoteViewsService(JNIEnv* env, jobject clazz, jlong jproxy,
    jstring jcallingPackage, jint jappWidgetId, jobject jintent)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeUnbindRemoteViewsService()");

    String callingPackage = ElUtil::ToElString(env, jcallingPackage);


    AutoPtr<IIntent> intent;
    ElUtil::ToElIntent(env, jintent, (IIntent**)&intent);
    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;
    ECode ec = appwidgetService->UnbindRemoteViewsService(callingPackage, (Int32)jappWidgetId, intent);
    if (FAILED(ec))
        ALOGD("UnbindRemoteViewsService(), ec = %08x", ec);


    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeUnbindRemoteViewsService()");
}

static jintArray android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetIds(
    JNIEnv* env, jobject clazz, jlong jproxy, jobject jcomponentName)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetIds()");

    IIAppWidgetService* appwidgetService = (IIAppWidgetService*)jproxy;

    AutoPtr<IComponentName> componentName;
    ElUtil::ToElComponentName(env, jcomponentName, (IComponentName**)&componentName);

    AutoPtr<ArrayOf<Int32> > ids;
    ECode ec = appwidgetService->GetAppWidgetIds(componentName, (ArrayOf<Int32>**)&ids);
    if (FAILED(ec))
        ALOGD("GetAppWidgetIds(), ec = %08x", ec);


    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetIds()");
    return ElUtil::GetJavaIntArray(env, ids);
}

static void android_appwidget_ElAppWidgetManagerProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_appwidget_ElAppWidgetManagerProxy_nativeDestroy()");

    IIAppWidgetService* obj = (IIAppWidgetService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_appwidget_ElAppWidgetManagerProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeFinalize },
    { "nativeStartListening", "(JLcom/android/internal/appwidget/IAppWidgetHost;Ljava/lang/String;ILjava/util/List;)[I",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeStartListening },
    { "nativeStopListening", "(JLjava/lang/String;I)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeStopListening },
    { "nativeGetInstalledProvidersForProfile", "(JII)Ljava/util/List;",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeGetInstalledProvidersForProfile },
    { "nativeDeleteHost", "(JLjava/lang/String;I)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeDeleteHost },
    { "nativeHasBindAppWidgetPermission", "(JLjava/lang/String;I)Z",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeHasBindAppWidgetPermission },
    { "nativeAllocateAppWidgetId", "(JLjava/lang/String;I)I",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeAllocateAppWidgetId },
    { "nativeDeleteAppWidgetId", "(JLjava/lang/String;I)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeDeleteAppWidgetId },
    { "nativeDeleteAllHosts", "(J)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeDeleteAllHosts },
    { "nativeGetAppWidgetViews", "(JLjava/lang/String;I)Landroid/widget/RemoteViews;",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetViews },
    { "nativeGetAppWidgetIdsForHost", "(JLjava/lang/String;I)[I",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetIdsForHost },
    { "nativeCreateAppWidgetConfigIntentSender", "(JLjava/lang/String;I)Landroid/content/IntentSender;",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeCreateAppWidgetConfigIntentSender },
    { "nativeUpdateAppWidgetIds", "(JLjava/lang/String;[ILandroid/widget/RemoteViews;)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetIds },
    { "nativeUpdateAppWidgetOptions", "(JLjava/lang/String;ILandroid/os/Bundle;)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetOptions },
    { "nativeGetAppWidgetOptions", "(JLjava/lang/String;I)Landroid/os/Bundle;",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetOptions },
    { "nativePartiallyUpdateAppWidgetIds", "(JLjava/lang/String;[ILandroid/widget/RemoteViews;)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativePartiallyUpdateAppWidgetIds },
    { "nativeUpdateAppWidgetProvider", "(JLandroid/content/ComponentName;Landroid/widget/RemoteViews;)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeUpdateAppWidgetProvider },
    { "nativeNotifyAppWidgetViewDataChanged", "(JLjava/lang/String;[II)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeNotifyAppWidgetViewDataChanged },
    { "nativeGetAppWidgetInfo", "(JLjava/lang/String;I)Landroid/appwidget/AppWidgetProviderInfo;",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetInfo },
    { "nativeSetBindAppWidgetPermission", "(JLjava/lang/String;IZ)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeSetBindAppWidgetPermission },
    { "nativeBindAppWidgetId", "(JLjava/lang/String;IILandroid/content/ComponentName;Landroid/os/Bundle;)Z",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeBindAppWidgetId },
    { "nativeBindRemoteViewsService", "(JLjava/lang/String;ILandroid/content/Intent;Landroid/os/IBinder;)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeBindRemoteViewsService },
    { "nativeUnbindRemoteViewsService", "(JLjava/lang/String;ILandroid/content/Intent;)V",
        (void*) android_appwidget_ElAppWidgetManagerProxy_nativeUnbindRemoteViewsService },
    { "nativeGetAppWidgetIds",    "(JLandroid/content/ComponentName;)[I",
            (void*) android_appwidget_ElAppWidgetManagerProxy_nativeGetAppWidgetIds },
};

int register_android_appwidget_ElAppWidgetManagerProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/appwidget/ElAppWidgetManagerProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

