
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <elastos/utility/etl/HashMap.h>
#include <Elastos.Droid.Graphics.h>
#include <Elastos.Droid.Os.h>
#include <Elastos.Droid.Service.h>
#include <Elastos.Droid.View.h>
#include <_Elastos.Droid.JavaProxy.h>
#include <utils/Log.h>
#include <utils/Mutex.h>

using android::ElUtil;
using android::Mutex;
using Elastos::Droid::Os::IBinder;
using Elastos::Droid::Service::Wallpaper::IIWallpaperConnection;
using Elastos::Droid::Service::Wallpaper::IIWallpaperService;
using Elastos::Droid::JavaProxy::CIWallpaperConnectionNative;
using Elastos::Droid::View::IIWindow;
using Elastos::Utility::Etl::HashMap;

extern HashMap<Int32, AutoPtr<IIWindow> > sWindowMap;
extern Mutex sWindowMapLock;

static void android_service_wallpaper_ElWallpaperServiceProxy_nativeAttach(JNIEnv* env, jobject clazz, jlong jproxy,
    jobject jconnection, jobject jwindowToken, jint jwindowType, jboolean jisPreview, jint jreqWidth, jint jreqHeight, jobject jpadding)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperServiceProxy_nativeAttach()");

    AutoPtr<IIWallpaperConnection> connection;
    if (jconnection != NULL) {
        JavaVM* jvm;
        env->GetJavaVM(&jvm);
        jobject jInstance = env->NewGlobalRef(jconnection);
        CIWallpaperConnectionNative::New((Handle64)jvm, (Handle64)jInstance, (IIWallpaperConnection**)&connection);
    }

    AutoPtr<IBinder> windowToken;
    if (jwindowToken != NULL) {
        jclass viewRootWClass = env->FindClass("android/view/ViewRootImpl$W");
        ElUtil::CheckErrorAndLog(env, "FindClass ViewRootImpl$W fail : %d!\n", __LINE__);
        jclass objectClass = env->FindClass("java/lang/Object");
        ElUtil::CheckErrorAndLog(env, "FindClass Object : %d!\n", __LINE__);

        jmethodID m = env->GetMethodID(objectClass, "hashCode", "()I");
        ElUtil::CheckErrorAndLog(env, "GetMethodID hashCode : %d!\n", __LINE__);
        env->DeleteLocalRef(objectClass);

        jint jTokenHashCode = env->CallIntMethod(jwindowToken, m);
        if (env->IsInstanceOf(jwindowToken, viewRootWClass)) {
            Mutex::Autolock lock(sWindowMapLock);
            HashMap<Int32, AutoPtr<IIWindow> >::Iterator it = sWindowMap.Find(jTokenHashCode);
            if (it != sWindowMap.End()) {
                windowToken = IBinder::Probe(it->mSecond);
            }
            else {
                ALOGE("nativeWindowGainedFocus() can't find token in sWindowMap");
                assert(0);
            }
        }
        else {
            String objClassName = ElUtil::GetClassName(env, jwindowToken);
            ALOGE("nativeWindowGainedFocus() token[%s] not Support!", objClassName.string());
        }

        env->DeleteLocalRef(viewRootWClass);
    }
    else {
        ALOGE("nativeWindowGainedFocus() what is this token ?");
        assert(0);
    }

    AutoPtr<IRect> padding;
    if (jpadding != NULL) {
        if (!ElUtil::ToElRect(env, jpadding, (IRect**)&padding))
            ALOGE("nativeWindowGainedFocus() ToElRect fail");
    }

    IIWallpaperService* iws = (IIWallpaperService*)jproxy;

    iws->Attach(connection, windowToken, jwindowType, jisPreview, jreqWidth, jreqHeight, padding);

    // ALOGD("- android_service_wallpaper_ElWallpaperServiceProxy_nativeAttach()");
}

static void android_service_wallpaper_ElWallpaperServiceProxy_nativeFinalize(JNIEnv* env, jobject clazz,
                                                   jlong jproxy)
{
    // ALOGD("+ android_service_wallpaper_ElWallpaperServiceProxy_nativeDestroy()");

    IIWallpaperService* obj = (IIWallpaperService*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_service_wallpaper_ElWallpaperServiceProxy_nativeDestroy()");
}

namespace android
{
static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_service_wallpaper_ElWallpaperServiceProxy_nativeFinalize },
    { "nativeAttach",    "(JLandroid/service/wallpaper/IWallpaperConnection;Landroid/os/IBinder;IZIILandroid/graphics/Rect;)V",
            (void*) android_service_wallpaper_ElWallpaperServiceProxy_nativeAttach },
};

int register_android_service_wallpaper_ElWallpaperServiceProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/service/wallpaper/ElWallpaperServiceProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

