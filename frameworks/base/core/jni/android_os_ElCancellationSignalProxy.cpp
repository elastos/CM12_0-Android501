
#include "JNIHelp.h"
#include "jni.h"
#include "ElUtil.h"
#include <Elastos.Droid.Os.h>

#include <utils/Log.h>

using android::ElUtil;

using Elastos::Droid::Os::IICancellationSignal;

static void android_os_ElCancellationSignalProxy_nativeCancel(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_ElCancellationSignalProxy_nativeCancel()");

    IICancellationSignal* cs = (IICancellationSignal*)jproxy;
    cs->Cancel();

    // ALOGD("- android_os_ElCancellationSignalProxy_nativeCancel()");
}

static void android_os_ElCancellationSignalProxy_nativeFinalize(
    JNIEnv* env, jobject clazz, jlong jproxy)
{
    // ALOGD("+ android_os_ElCancellationSignalProxy_nativeDestroy()");

    IICancellationSignal* obj = (IICancellationSignal*)jproxy;
    if (obj != NULL) {
        obj->Release();
    }

    // ALOGD("- android_os_ElCancellationSignalProxy_nativeDestroy()");
}

namespace android
{

static JNINativeMethod gMethods[] = {
    { "nativeFinalize",      "(J)V",
            (void*) android_os_ElCancellationSignalProxy_nativeFinalize },
    { "nativeCancel",    "(J)V",
            (void*) android_os_ElCancellationSignalProxy_nativeCancel },
};

int register_android_os_ElCancellationSignalProxy(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "android/os/ElCancellationSignalProxy",
        gMethods, NELEM(gMethods));
}

}; // namespace android

