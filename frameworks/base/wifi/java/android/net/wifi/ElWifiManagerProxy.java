
package android.net.wifi;

import android.net.wifi.BatchedScanResult;
import android.net.wifi.BatchedScanSettings;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.ScanSettings;
import android.net.wifi.WifiChannel;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConnectionStatistics;
import android.net.wifi.WifiActivityEnergyInfo;

import android.net.DhcpInfo;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.Messenger;
import android.os.WorkSource;
import android.util.Log;

import java.util.List;

/** @hide */
public class ElWifiManagerProxy extends IWifiManager.Stub
{
    private long mNativeProxy;

    private native int nativeGetSupportedFeatures(long nativeProxy);

    private native WifiActivityEnergyInfo nativeReportActivityInfo(long nativeProxy);

    private native Messenger nativeGetWifiServiceMessenger(long nativeProxy);

    private native WifiInfo nativeGetConnectionInfo(long nativeProxy);

    private native int nativeGetWifiEnabledState(long nativeProxy);

    private native int nativeGetWifiApEnabledState(long nativeProxy);

    private native boolean nativeSetWifiEnabled(long nativeProxy, boolean enable);

    private native List<WifiChannel> nativeGetChannelList(long nativeProxy);

    private native void nativeStartScan(long nativeProxy, ScanSettings requested, WorkSource ws);

    private native List<ScanResult> nativeGetScanResults(long nativeProxy, String callingPackage);

    private native List<WifiConfiguration> nativeGetConfiguredNetworks(long nativeProxy);

    private native List<WifiConfiguration> nativeGetPrivilegedConfiguredNetworks(long nativeProxy);

    private native void nativeStartWifi(long nativeProxy);

    private native void nativeStopWifi(long nativeProxy);

    private native void nativeDisconnect(long nativeProxy);

    private native void nativeReconnect(long nativeProxy);

    private native boolean nativeEnableNetwork(long nativeProxy, int netId, boolean disableOthers);

    private native boolean nativeDisableNetwork(long nativeProxy, int netId);

    private native boolean nativeRemoveNetwork(long nativeProxy, int netId);

    private native boolean nativeAcquireWifiLock(long nativeProxy, IBinder lock, int lockType, String tag, WorkSource ws);

    private native void nativeUpdateWifiLockWorkSource(long nativeProxy, IBinder lock, WorkSource ws);

    private native boolean nativeReleaseWifiLock(long nativeProxy, IBinder lock);

    private native int nativeAddOrUpdateNetwork(long nativeProxy, WifiConfiguration config);

    private native boolean nativePingSupplicant(long nativeProxy);

    private native void nativeReassociate(long nativeProxy);

    private native void nativeSetCountryCode(long nativeProxy, String country, boolean persist);

    private native void nativeSetFrequencyBand(long nativeProxy, int band, boolean persist);

    private native int nativeGetFrequencyBand(long nativeProxy);

    private native boolean nativeIsDualBandSupported(long nativeProxy);

    private native boolean nativeIsIbssSupported(long nativeProxy);

    private native boolean nativeSaveConfiguration(long nativeProxy);

    private native DhcpInfo nativeGetDhcpInfo(long nativeProxy);

    private native boolean nativeIsScanAlwaysAvailable(long nativeProxy);

    private native void nativeInitializeMulticastFiltering(long nativeProxy);

    private native boolean nativeIsMulticastEnabled(long nativeProxy);

    private native void nativeAcquireMulticastLock(long nativeProxy, IBinder binder, String tag);

    private native void nativeReleaseMulticastLock(long nativeProxy);

    private native void nativeSetWifiApEnabled(long nativeProxy, WifiConfiguration wifiConfig, boolean enable);

    private native WifiConfiguration nativeGetWifiApConfiguration(long nativeProxy);

    private native void nativeSetWifiApConfiguration(long nativeProxy, WifiConfiguration wifiConfig);

    private native void nativeAddToBlacklist(long nativeProxy, String bssid);

    private native void nativeClearBlacklist(long nativeProxy);

    private native String nativeGetConfigFile(long nativeProxy);

    private native void nativeEnableTdls(long nativeProxy, String remoteIPAddress, boolean enable);

    private native void nativeEnableTdlsWithMacAddress(long nativeProxy, String remoteMacAddress, boolean enable);

    private native boolean nativeRequestBatchedScan(long nativeProxy, BatchedScanSettings requested, IBinder binder, WorkSource ws);

    private native void nativeStopBatchedScan(long nativeProxy, BatchedScanSettings requested);

    private native List<BatchedScanResult> nativeGetBatchedScanResults(long nativeProxy, String callingPackage);

    private native boolean nativeIsBatchedScanSupported(long nativeProxy);

    private native void nativePollBatchedScan(long nativeProxy);

    private native String nativeGetWpsNfcConfigurationToken(long nativeProxy, int netId);

    private native void nativeEnableVerboseLogging(long nativeProxy, int verbose);

    private native int nativeGetVerboseLoggingLevel(long nativeProxy);

    private native int nativeGetAggressiveHandover(long nativeProxy);

    private native void nativeEnableAggressiveHandover(long nativeProxy, int enabled);

    private native int nativeGetAllowScansWithTraffic(long nativeProxy);

    private native void nativeSetAllowScansWithTraffic(long nativeProxy, int enabled);

    private native WifiConnectionStatistics nativeGetConnectionStatistics(long nativeProxy);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElWifiManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public int getSupportedFeatures() throws RemoteException {
        return nativeGetSupportedFeatures(mNativeProxy);
    }

    @Override
    public WifiActivityEnergyInfo reportActivityInfo() throws RemoteException {
        return nativeReportActivityInfo(mNativeProxy);
    }

    @Override
    public Messenger getWifiServiceMessenger() throws RemoteException {
        return nativeGetWifiServiceMessenger(mNativeProxy);
    }

    @Override
    public WifiInfo getConnectionInfo() throws RemoteException {
        return nativeGetConnectionInfo(mNativeProxy);
    }

    @Override
    public int getWifiEnabledState() throws RemoteException {
        return nativeGetWifiEnabledState(mNativeProxy);
    }

    @Override
    public int getWifiApEnabledState() throws RemoteException {
        return nativeGetWifiApEnabledState(mNativeProxy);
    }

    @Override
    public boolean setWifiEnabled(boolean enable) throws RemoteException {
        return nativeSetWifiEnabled(mNativeProxy, enable);
    }

    @Override
    public List<WifiChannel> getChannelList() throws RemoteException {
        return nativeGetChannelList(mNativeProxy);
    }

    @Override
    public void startScan(ScanSettings requested, WorkSource ws) throws RemoteException {
        nativeStartScan(mNativeProxy, requested, ws);
    }

    @Override
    public List<ScanResult> getScanResults(String callingPackage) throws RemoteException {
        return nativeGetScanResults(mNativeProxy, callingPackage);
    }

    @Override
    public List<WifiConfiguration> getConfiguredNetworks() throws RemoteException {
        return nativeGetConfiguredNetworks(mNativeProxy);
    }

    @Override
    public List<WifiConfiguration> getPrivilegedConfiguredNetworks() throws RemoteException {
        return nativeGetPrivilegedConfiguredNetworks(mNativeProxy);
    }

    @Override
    public void startWifi() throws RemoteException {
        nativeStartWifi(mNativeProxy);
    }

    @Override
    public void stopWifi() throws RemoteException {
        nativeStopWifi(mNativeProxy);
    }

    @Override
    public void disconnect() throws RemoteException {
        nativeDisconnect(mNativeProxy);
    }

    @Override
    public void reconnect() throws RemoteException {
        nativeReconnect(mNativeProxy);
    }

    @Override
    public boolean enableNetwork(int netId, boolean disableOthers) throws RemoteException {
        return nativeEnableNetwork(mNativeProxy, netId, disableOthers);
    }

    @Override
    public boolean disableNetwork(int netId) throws RemoteException {
        return nativeDisableNetwork(mNativeProxy, netId);
    }

    @Override
    public boolean removeNetwork(int netId) throws RemoteException {
        return nativeRemoveNetwork(mNativeProxy, netId);
    }

    @Override
    public boolean acquireWifiLock(IBinder lock, int lockType, String tag, WorkSource ws) throws RemoteException {
        return nativeAcquireWifiLock(mNativeProxy, lock, lockType, tag, ws);
    }

    @Override
    public void updateWifiLockWorkSource(IBinder lock, WorkSource ws) throws RemoteException {
        nativeUpdateWifiLockWorkSource(mNativeProxy, lock, ws);
    }

    @Override
    public boolean releaseWifiLock(IBinder lock) throws RemoteException {
        return nativeReleaseWifiLock(mNativeProxy, lock);
    }

    @Override
    public int addOrUpdateNetwork(WifiConfiguration config) throws RemoteException {
        return nativeAddOrUpdateNetwork(mNativeProxy, config);
    }

    @Override
    public boolean pingSupplicant() throws RemoteException {
        return nativePingSupplicant(mNativeProxy);
    }

    @Override
    public void reassociate() throws RemoteException {
        nativeReassociate(mNativeProxy);
    }

    @Override
    public void setCountryCode(String country, boolean persist) throws RemoteException {
        nativeSetCountryCode(mNativeProxy, country, persist);
    }

    @Override
    public void setFrequencyBand(int band, boolean persist) throws RemoteException {
        nativeSetFrequencyBand(mNativeProxy, band, persist);
    }

    @Override
    public int getFrequencyBand() throws RemoteException {
        return nativeGetFrequencyBand(mNativeProxy);
    }

    @Override
    public boolean isDualBandSupported() throws RemoteException {
        return nativeIsDualBandSupported(mNativeProxy);
    }

    @Override
    public boolean isIbssSupported() throws RemoteException {
        return nativeIsIbssSupported(mNativeProxy);
    }

    @Override
    public boolean saveConfiguration() throws RemoteException {
        return nativeSaveConfiguration(mNativeProxy);
    }

    @Override
    public DhcpInfo getDhcpInfo() throws RemoteException {
        return nativeGetDhcpInfo(mNativeProxy);
    }

    @Override
    public boolean isScanAlwaysAvailable() throws RemoteException {
        return nativeIsScanAlwaysAvailable(mNativeProxy);
    }

    @Override
    public void initializeMulticastFiltering() throws RemoteException {
        nativeInitializeMulticastFiltering(mNativeProxy);
    }

    @Override
    public boolean isMulticastEnabled() throws RemoteException {
        return nativeIsMulticastEnabled(mNativeProxy);
    }

    @Override
    public void acquireMulticastLock(IBinder binder, String tag) throws RemoteException {
        nativeAcquireMulticastLock(mNativeProxy, binder, tag);
    }

    @Override
    public void releaseMulticastLock() throws RemoteException {
        nativeReleaseMulticastLock(mNativeProxy);
    }

    @Override
    public void setWifiApEnabled(WifiConfiguration wifiConfig, boolean enable) throws RemoteException {
        nativeSetWifiApEnabled(mNativeProxy, wifiConfig, enable);
    }

    @Override
    public WifiConfiguration getWifiApConfiguration() throws RemoteException {
        return nativeGetWifiApConfiguration(mNativeProxy);
    }

    @Override
    public void setWifiApConfiguration(WifiConfiguration wifiConfig) throws RemoteException {
        nativeSetWifiApConfiguration(mNativeProxy, wifiConfig);
    }

    @Override
    public void addToBlacklist(String bssid) throws RemoteException {
        nativeAddToBlacklist(mNativeProxy, bssid);
    }

    @Override
    public void clearBlacklist() throws RemoteException {
        nativeClearBlacklist(mNativeProxy);
    }

    @Override
    public String getConfigFile() throws RemoteException {
        return nativeGetConfigFile(mNativeProxy);
    }

    @Override
    public void enableTdls(String remoteIPAddress, boolean enable) throws RemoteException {
        nativeEnableTdls(mNativeProxy, remoteIPAddress, enable);
    }

    @Override
    public void enableTdlsWithMacAddress(String remoteMacAddress, boolean enable) throws RemoteException {
        nativeEnableTdlsWithMacAddress(mNativeProxy, remoteMacAddress, enable);
    }

    @Override
    public boolean requestBatchedScan(BatchedScanSettings requested, IBinder binder, WorkSource ws) throws RemoteException {
        return nativeRequestBatchedScan(mNativeProxy, requested, binder, ws);
    }

    @Override
    public void stopBatchedScan(BatchedScanSettings requested) throws RemoteException {
        nativeStopBatchedScan(mNativeProxy, requested);
    }

    @Override
    public List<BatchedScanResult> getBatchedScanResults(String callingPackage) throws RemoteException {
        return nativeGetBatchedScanResults(mNativeProxy, callingPackage);
    }

    @Override
    public boolean isBatchedScanSupported() throws RemoteException {
        return nativeIsBatchedScanSupported(mNativeProxy);
    }

    @Override
    public void pollBatchedScan() throws RemoteException {
        nativePollBatchedScan(mNativeProxy);
    }

    @Override
    public String getWpsNfcConfigurationToken(int netId) throws RemoteException {
        return nativeGetWpsNfcConfigurationToken(mNativeProxy, netId);
    }

    @Override
    public void enableVerboseLogging(int verbose) throws RemoteException {
        nativeEnableVerboseLogging(mNativeProxy, verbose);
    }

    @Override
    public int getVerboseLoggingLevel() throws RemoteException {
        return nativeGetVerboseLoggingLevel(mNativeProxy);
    }

    @Override
    public int getAggressiveHandover() throws RemoteException {
        return nativeGetAggressiveHandover(mNativeProxy);
    }

    @Override
    public void enableAggressiveHandover(int enabled) throws RemoteException {
        nativeEnableAggressiveHandover(mNativeProxy, enabled);
    }

    @Override
    public int getAllowScansWithTraffic() throws RemoteException {
        return nativeGetAllowScansWithTraffic(mNativeProxy);
    }

    @Override
    public void setAllowScansWithTraffic(int enabled) throws RemoteException {
        nativeSetAllowScansWithTraffic(mNativeProxy, enabled);
    }

    @Override
    public WifiConnectionStatistics getConnectionStatistics() throws RemoteException {
        return nativeGetConnectionStatistics(mNativeProxy);
    }
}

