
package android.net.wifi.p2p;

import android.os.Messenger;
import android.os.RemoteException;

/**
 * @hide
 */
public class ElWifiP2pServiceProxy extends IWifiP2pManager.Stub {

    private long mNativeProxy;

    private native Messenger nativeGetMessenger(long proxy);

    private native Messenger nativeGetP2pStateMachineMessenger(long proxy);

    private native void nativeSetMiracastMode(long proxy, int mode);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElWifiP2pServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public Messenger getMessenger() throws RemoteException {
        return nativeGetMessenger(mNativeProxy);
    }

    @Override
    public Messenger getP2pStateMachineMessenger() throws RemoteException {
        return nativeGetP2pStateMachineMessenger(mNativeProxy);
    }

    @Override
    public void setMiracastMode(int mode) throws RemoteException {
        nativeSetMiracastMode(mNativeProxy, mode);
    }
}
