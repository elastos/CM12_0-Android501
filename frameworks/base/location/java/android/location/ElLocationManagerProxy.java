
package android.location;

import android.app.PendingIntent;
import android.location.Address;
import android.location.Criteria;
import android.location.GeocoderParams;
import android.location.Geofence;
import android.location.IGeocodeProvider;
import android.location.IGpsStatusListener;
import android.location.ILocationListener;
import android.location.Location;
import android.location.LocationRequest;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.List;

import com.android.internal.location.ProviderProperties;

/**
 * @hide
 */
public class ElLocationManagerProxy extends ILocationManager.Stub
{
    private long mNativeProxy;

    private native ProviderProperties nativeGetProviderProperties(long nativeProxy, String provider);
    private native boolean nativeAddGpsStatusListener(long nativeProxy, IGpsStatusListener listener, String packageName);
    private native void nativeRequestLocationUpdates(long nativeProxy, LocationRequest request, ILocationListener listener, PendingIntent intent, String packageName);
    private native void nativeRemoveUpdates(long nativeProxy, ILocationListener listener, PendingIntent intent, String packageName);
    private native void nativeRemoveGeofence(long nativeProxy, Geofence fence, PendingIntent intent, String packageName);
    private native Location nativeGetLastLocation(long nativeProxy, LocationRequest request, String packageName);
    private native void nativeRemoveGpsStatusListener(long nativeProxy, IGpsStatusListener listener);
    private native List<String> nativeGetAllProviders(long nativeProxy);
    private native void nativeRequestGeofence(long nativeProxy, LocationRequest request, Geofence geofence, PendingIntent intent, String packageName);
    private native void nativeLocationCallbackFinished(long nativeProxy, ILocationListener listener);
    private native void nativeReportLocation(long nativeProxy, Location location, boolean passive);
    private native String nativeGetBestProvider(long nativeProxy, Criteria criteria, boolean enabledOnly);
    private native boolean nativeIsProviderEnabled(long nativeProxy, String provider);
    private native boolean nativeSendExtraCommand(long nativeProxy, String provider, String command, Bundle extras);
    private native List<String> nativeGetProviders(long nativeProxy, Criteria criteria, boolean enabledOnly);
    private native boolean nativeGeocoderIsPresent(long proxy);
    private native String nativeGetFromLocation(long proxy, double latitude, double longitude, int maxResults, GeocoderParams params, List<Address> addrs);
    private native String nativeGetFromLocationName(long proxy, String locationName, double lowerLeftLatitude, double lowerLeftLongitude,
        double upperRightLatitude, double upperRightLongitude, int maxResults, GeocoderParams params, List<Address> addrs);
    private native boolean nativeSendNiResponse(long proxy, int notifId, int userResponse);
    private native boolean nativeAddGpsMeasurementsListener(long proxy, IGpsMeasurementsListener listener, String packageName);
    private native boolean nativeRemoveGpsMeasurementsListener(long proxy, IGpsMeasurementsListener listener);
    private native boolean nativeAddGpsNavigationMessageListener(long proxy, IGpsNavigationMessageListener listener, String packageName);
    private native boolean nativeRemoveGpsNavigationMessageListener(long proxy, IGpsNavigationMessageListener listener);
    private native boolean nativeProviderMeetsCriteria(long proxy, String provider, Criteria criteria);
    private native void nativeAddTestProvider(long proxy, String name, ProviderProperties properties);
    private native void nativeRemoveTestProvider(long proxy, String provider);
    private native void nativeSetTestProviderLocation(long proxy, String provider, Location loc);
    private native void nativeClearTestProviderLocation(long proxy, String provider);
    private native void nativeSetTestProviderEnabled(long proxy, String provider, boolean enabled);
    private native void nativeClearTestProviderEnabled(long proxy, String provider);
    private native void nativeSetTestProviderStatus(long proxy, String provider, int status, Bundle extras, long updateTime);
    private native void nativeClearTestProviderStatus(long proxy, String provider);
    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElLocationManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public ProviderProperties getProviderProperties(String provider) throws RemoteException {
        return nativeGetProviderProperties(mNativeProxy, provider);
    }

    @Override
    public boolean addGpsStatusListener(IGpsStatusListener listener, String packageName) throws RemoteException {
        return nativeAddGpsStatusListener(mNativeProxy, listener, packageName);
    }

    @Override
    public void removeUpdates(ILocationListener listener, PendingIntent intent, String packageName) throws RemoteException {
        nativeRemoveUpdates(mNativeProxy, listener, intent, packageName);
    }

    @Override
    public void requestLocationUpdates(LocationRequest request, ILocationListener listener,
            PendingIntent intent, String packageName) throws RemoteException {
        nativeRequestLocationUpdates(mNativeProxy, request, listener, intent, packageName);
    }

    @Override
    public void requestGeofence(LocationRequest request, Geofence geofence,
            PendingIntent intent, String packageName) throws RemoteException {
        nativeRequestGeofence(mNativeProxy, request, geofence, intent, packageName);
    }

    @Override
    public void removeGeofence(Geofence fence, PendingIntent intent, String packageName) throws RemoteException {
        nativeRemoveGeofence(mNativeProxy, fence, intent, packageName);
    }

    @Override
    public Location getLastLocation(LocationRequest request, String packageName) throws RemoteException {
        return nativeGetLastLocation(mNativeProxy, request, packageName);
    }

    @Override
    public void removeGpsStatusListener(IGpsStatusListener listener) throws RemoteException {
        nativeRemoveGpsStatusListener(mNativeProxy, listener);
    }

    @Override
    public boolean geocoderIsPresent() throws RemoteException {
        return nativeGeocoderIsPresent(mNativeProxy);
    }

    @Override
    public String getFromLocation(double latitude, double longitude, int maxResults,
        GeocoderParams params, List<Address> addrs) throws RemoteException {
        return nativeGetFromLocation(mNativeProxy, latitude, longitude, maxResults, params, addrs);
    }

    @Override
    public String getFromLocationName(String locationName,
        double lowerLeftLatitude, double lowerLeftLongitude,
        double upperRightLatitude, double upperRightLongitude, int maxResults,
        GeocoderParams params, List<Address> addrs) throws RemoteException {
        return nativeGetFromLocationName(mNativeProxy, locationName, lowerLeftLatitude, lowerLeftLongitude, upperRightLatitude, upperRightLongitude, maxResults, params, addrs);
    }

    @Override
    public boolean sendNiResponse(int notifId, int userResponse) throws RemoteException {
        return nativeSendNiResponse(mNativeProxy, notifId, userResponse);
    }

    @Override
    public boolean addGpsMeasurementsListener(IGpsMeasurementsListener listener, String packageName) throws RemoteException {
        return nativeAddGpsMeasurementsListener(mNativeProxy, listener, packageName);
    }
    @Override
    public boolean removeGpsMeasurementsListener(IGpsMeasurementsListener listener) throws RemoteException {
        return nativeRemoveGpsMeasurementsListener(mNativeProxy, listener);
    }

    @Override
    public boolean addGpsNavigationMessageListener(IGpsNavigationMessageListener listener, String packageName) throws RemoteException {
        return nativeAddGpsNavigationMessageListener(mNativeProxy, listener, packageName);
    }
    @Override
    public boolean removeGpsNavigationMessageListener(IGpsNavigationMessageListener listener) throws RemoteException {
        return nativeRemoveGpsNavigationMessageListener(mNativeProxy, listener);
    }

    @Override
    public List<String> getAllProviders() throws RemoteException {
        return nativeGetAllProviders(mNativeProxy);
    }

    @Override
    public List<String> getProviders(Criteria criteria, boolean enabledOnly) throws RemoteException {
        return nativeGetProviders(mNativeProxy, criteria, enabledOnly);
    }

    @Override
    public String getBestProvider(Criteria criteria, boolean enabledOnly) throws RemoteException {
        return nativeGetBestProvider(mNativeProxy, criteria, enabledOnly);
    }

    @Override
    public boolean providerMeetsCriteria(String provider, Criteria criteria) throws RemoteException {
        return nativeProviderMeetsCriteria(mNativeProxy, provider, criteria);
    }

    @Override
    public boolean isProviderEnabled(String provider) throws RemoteException {
        return nativeIsProviderEnabled(mNativeProxy, provider);
    }

    @Override
    public void addTestProvider(String name, ProviderProperties properties) throws RemoteException {
        nativeAddTestProvider(mNativeProxy, name, properties);
    }

    @Override
    public void removeTestProvider(String provider) throws RemoteException {
        nativeRemoveTestProvider(mNativeProxy, provider);
    }

    @Override
    public void setTestProviderLocation(String provider, Location loc) throws RemoteException {
        nativeSetTestProviderLocation(mNativeProxy, provider, loc);
    }

    @Override
    public void clearTestProviderLocation(String provider) throws RemoteException {
        nativeClearTestProviderLocation(mNativeProxy, provider);
    }

    @Override
    public void setTestProviderEnabled(String provider, boolean enabled) throws RemoteException {
        nativeSetTestProviderEnabled(mNativeProxy, provider, enabled);
    }

    @Override
    public void clearTestProviderEnabled(String provider) throws RemoteException {
        nativeClearTestProviderEnabled(mNativeProxy, provider);
    }

    @Override
    public void setTestProviderStatus(String provider, int status, Bundle extras, long updateTime) throws RemoteException {
        nativeSetTestProviderStatus(mNativeProxy, provider, status, extras, updateTime);
    }

    @Override
    public void clearTestProviderStatus(String provider) throws RemoteException {
        nativeClearTestProviderStatus(mNativeProxy, provider);
    }

    @Override
    public boolean sendExtraCommand(String provider, String command, Bundle extras) throws RemoteException {
        return nativeSendExtraCommand(mNativeProxy, provider, command, extras);
    }

    @Override
    public void reportLocation(Location location, boolean passive) throws RemoteException {
        nativeReportLocation(mNativeProxy, location, passive);
    }

    @Override
    public void locationCallbackFinished(ILocationListener listener) throws RemoteException {
        nativeLocationCallbackFinished(mNativeProxy, listener);
    }
}
