
package android.location;

import android.location.Country;
import android.location.ICountryListener;
import android.os.RemoteException;

/**
 * {@hide}
 */
public class ElCountryDetectorProxy extends ICountryDetector.Stub {
    private long mNativeProxy;

    private native Country nativeDetectCountry(long proxy);

    private native void nativeAddCountryListener(long proxy, ICountryListener listener);

    private native void nativeRemoveCountryListener(long proxy, ICountryListener listener);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElCountryDetectorProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public Country detectCountry() throws RemoteException {
        return nativeDetectCountry(mNativeProxy);
    }

    @Override
    public void addCountryListener(ICountryListener listener) throws RemoteException {
        nativeAddCountryListener(mNativeProxy, listener);
    }

    @Override
    public void removeCountryListener(ICountryListener listener) throws RemoteException {
        nativeRemoveCountryListener(mNativeProxy, listener);
    }
}
