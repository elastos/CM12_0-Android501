package android.media.session;

class ElSessionProxy extends ISession.Stub {

    private long mNativeProxy;

    public ElSessionProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    //start native--
    private native void nativeSendEvent(long nativeProxy, java.lang.String event, android.os.Bundle data);
    private native android.media.session.ISessionController nativeGetController(long nativeProxy);
    private native void nativeSetFlags(long nativeProxy, int flags);
    private native void nativeSetActive(long nativeProxy, boolean active);
    private native void nativeSetMediaButtonReceiver(long nativeProxy, android.app.PendingIntent mbr);
    private native void nativeSetLaunchPendingIntent(long nativeProxy, android.app.PendingIntent pi);
    private native void nativeDestroy(long nativeProxy) throws android.os.RemoteException;
    // These commands are for the TransportPerformer

    private native void nativeSetMetadata(long nativeProxy, android.media.MediaMetadata metadata);
    private native void nativeSetPlaybackState(long nativeProxy, android.media.session.PlaybackState state);
    private native void nativeSetQueue(long nativeProxy, android.content.pm.ParceledListSlice queue);
    private native void nativeSetQueueTitle(long nativeProxy, java.lang.CharSequence title);
    private native void nativeSetExtras(long nativeProxy, android.os.Bundle extras);
    private native void nativeSetRatingType(long nativeProxy, int type);
    private native void nativePlayItemResponse(long nativeProxy, boolean success);
    private native void nativeUpdateNowPlayingEntries(long nativeProxy, long[] playList);
    private native void nativeUpdateFolderInfoBrowsedPlayer(long nativeProxy, java.lang.String stringUri);
    private native void nativeUpdateNowPlayingContentChange(long nativeProxy);
    // These commands relate to volume handling

    private native void nativeSetPlaybackToLocal(long nativeProxy, android.media.AudioAttributes attributes);
    private native void nativeSetPlaybackToRemote(long nativeProxy, int control, int max);
    private native void nativeSetCurrentVolume(long nativeProxy, int currentVolume);
    //end native--

    @Override
    public void sendEvent(java.lang.String event, android.os.Bundle data) throws android.os.RemoteException {
        nativeSendEvent(mNativeProxy, event, data);
    }

    @Override
    public android.media.session.ISessionController getController() throws android.os.RemoteException {
        return nativeGetController(mNativeProxy);
    }

    @Override
    public void setFlags(int flags) throws android.os.RemoteException {
        nativeSetFlags(mNativeProxy, flags);
    }

    @Override
    public void setActive(boolean active) throws android.os.RemoteException {
        nativeSetActive(mNativeProxy, active);
    }

    @Override
    public void setMediaButtonReceiver(android.app.PendingIntent mbr) throws android.os.RemoteException {
        nativeSetMediaButtonReceiver(mNativeProxy, mbr);
    }

    @Override
    public void setLaunchPendingIntent(android.app.PendingIntent pi) throws android.os.RemoteException {
        nativeSetLaunchPendingIntent(mNativeProxy, pi);
    }

    @Override
    public void destroy() throws android.os.RemoteException {
        nativeDestroy(mNativeProxy);
    }
    // These commands are for the TransportPerformer


    @Override
    public void setMetadata(android.media.MediaMetadata metadata) throws android.os.RemoteException {
        nativeSetMetadata(mNativeProxy, metadata);
    }

    @Override
    public void setPlaybackState(android.media.session.PlaybackState state) throws android.os.RemoteException {
        nativeSetPlaybackState(mNativeProxy, state);
    }

    @Override
    public void setQueue(android.content.pm.ParceledListSlice queue) throws android.os.RemoteException {
        nativeSetQueue(mNativeProxy, queue);
    }

    @Override
    public void setQueueTitle(java.lang.CharSequence title) throws android.os.RemoteException {
        nativeSetQueueTitle(mNativeProxy, title);
    }

    @Override
    public void setExtras(android.os.Bundle extras) throws android.os.RemoteException {
        nativeSetExtras(mNativeProxy, extras);
    }

    @Override
    public void setRatingType(int type) throws android.os.RemoteException {
        nativeSetRatingType(mNativeProxy, type);
    }

    @Override
    public void playItemResponse(boolean success) throws android.os.RemoteException {
        nativePlayItemResponse(mNativeProxy, success);
    }

    @Override
    public void updateNowPlayingEntries(long[] playList) throws android.os.RemoteException {
        nativeUpdateNowPlayingEntries(mNativeProxy, playList);
    }

    @Override
    public void updateFolderInfoBrowsedPlayer(java.lang.String stringUri) throws android.os.RemoteException {
        nativeUpdateFolderInfoBrowsedPlayer(mNativeProxy, stringUri);
    }

    @Override
    public void updateNowPlayingContentChange() throws android.os.RemoteException {
        nativeUpdateNowPlayingContentChange(mNativeProxy);
    }
    // These commands relate to volume handling

    @Override
    public void setPlaybackToLocal(android.media.AudioAttributes attributes) throws android.os.RemoteException {
        nativeSetPlaybackToLocal(mNativeProxy, attributes);
    }

    @Override
    public void setPlaybackToRemote(int control, int max) throws android.os.RemoteException {
        nativeSetPlaybackToRemote(mNativeProxy, control, max);
    }

    @Override
    public void setCurrentVolume(int currentVolume) throws android.os.RemoteException {
        nativeSetCurrentVolume(mNativeProxy, currentVolume);
    }

}
