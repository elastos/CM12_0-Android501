package android.media.session;

import android.content.ComponentName;
import android.media.IRemoteVolumeController;
import android.media.session.IActiveSessionsListener;
import android.media.session.ISession;
import android.media.session.ISessionCallback;
import android.media.session.ISessionManager;
import android.view.KeyEvent;
import android.os.IBinder;

import java.util.List;

class ElSessionManagerProxy extends ISessionManager.Stub {

    private long mNativeProxy;

    public ElSessionManagerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    private native ISession nativeCreateSession(long nativeProxy, String packageName, ISessionCallback cb, String tag, int userId);
    private native List<IBinder> nativeGetSessions(long nativeProxy, ComponentName compName, int userId);
    private native void nativeDispatchMediaKeyEvent(long nativeProxy, KeyEvent keyEvent, boolean needWakeLock);
    private native void nativeDispatchAdjustVolume(long nativeProxy, int suggestedStream, int delta, int flags);
    private native void nativeAddSessionsListener(long nativeProxy, IActiveSessionsListener listener, ComponentName compName, int userId);
    private native void nativeRemoveSessionsListener(long nativeProxy, IActiveSessionsListener listener);

    // This is for the system volume UI only
    private native void nativeSetRemoteVolumeController(long nativeProxy, IRemoteVolumeController rvc);

    // For PhoneWindowManager to precheck media keys
    private native boolean nativeIsGlobalPriorityActive(long nativeProxy);

    @Override
    public ISession createSession(String packageName, ISessionCallback cb, String tag, int userId) {
        return nativeCreateSession(mNativeProxy, packageName, cb, tag, userId);
    }

    @Override
    public List<IBinder> getSessions(ComponentName compName, int userId) {
        return nativeGetSessions(mNativeProxy, compName, userId);
    }

    @Override
    public void dispatchMediaKeyEvent(KeyEvent keyEvent, boolean needWakeLock) {
        nativeDispatchMediaKeyEvent(mNativeProxy, keyEvent, needWakeLock);
    }

    @Override
    public void dispatchAdjustVolume(int suggestedStream, int delta, int flags) {
        nativeDispatchAdjustVolume(mNativeProxy, suggestedStream, delta, flags);
    }

    @Override
    public void addSessionsListener(IActiveSessionsListener listener, ComponentName compName, int userId) {
        nativeAddSessionsListener(mNativeProxy, listener, compName, userId);
    }

    @Override
    public void removeSessionsListener(IActiveSessionsListener listener) {
        nativeRemoveSessionsListener(mNativeProxy, listener);
    }

    // This is for the system volume UI only
    @Override
    public void setRemoteVolumeController(IRemoteVolumeController rvc) {
        nativeSetRemoteVolumeController(mNativeProxy, rvc);
    }

    // For PhoneWindowManager to precheck media keys
    @Override
    public boolean isGlobalPriorityActive() {
        return nativeIsGlobalPriorityActive(mNativeProxy);
    }

}
