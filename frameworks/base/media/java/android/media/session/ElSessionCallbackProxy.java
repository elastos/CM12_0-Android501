package android.media.session;

class ElSessionCallbackProxy extends ISessionCallback.Stub {

    private long mNativeProxy;

    public ElSessionCallbackProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }
    //start native
    private native void nativeOnCommand(long nativeProxy, java.lang.String command, android.os.Bundle args, android.os.ResultReceiver cb);
    private native void nativeOnMediaButton(long nativeProxy, android.content.Intent mediaButtonIntent, int sequenceNumber, android.os.ResultReceiver cb);
    // These callbacks are for the TransportPerformer

    private native void nativeOnPlay(long nativeProxy);
    private native void nativeOnPlayFromMediaId(long nativeProxy, java.lang.String uri, android.os.Bundle extras);
    private native void nativeOnPlayFromSearch(long nativeProxy, java.lang.String query, android.os.Bundle extras);
    private native void nativeOnSkipToTrack(long nativeProxy, long id);
    private native void nativeOnPause(long nativeProxy);
    private native void nativeOnStop(long nativeProxy);
    private native void nativeOnNext(long nativeProxy);
    private native void nativeOnPrevious(long nativeProxy);
    private native void nativeOnFastForward(long nativeProxy);
    private native void nativeOnRewind(long nativeProxy);
    private native void nativeOnSeekTo(long nativeProxy, long pos);
    private native void nativeSetRemoteControlClientBrowsedPlayer(long nativeProxy);
    private native void nativeSetRemoteControlClientPlayItem(long nativeProxy, long uid, int scope);
    private native void nativeGetRemoteControlClientNowPlayingEntries(long nativeProxy);
    private native void nativeOnRate(long nativeProxy, android.media.Rating rating);
    private native void nativeOnCustomAction(long nativeProxy, java.lang.String action, android.os.Bundle args);
    // These callbacks are for volume handling

    private native void nativeOnAdjustVolume(long nativeProxy, int direction);
    private native void nativeOnSetVolumeTo(long nativeProxy, int value);
    //end native

    @Override
    public void onCommand(java.lang.String command, android.os.Bundle args, android.os.ResultReceiver cb) throws android.os.RemoteException {
        nativeOnCommand(mNativeProxy, command, args, cb);
    }

    @Override
    public void onMediaButton(android.content.Intent mediaButtonIntent, int sequenceNumber, android.os.ResultReceiver cb) throws android.os.RemoteException {
        nativeOnMediaButton(mNativeProxy, mediaButtonIntent, sequenceNumber, cb);
    }
    // These callbacks are for the TransportPerformer

    @Override
    public void onPlay() throws android.os.RemoteException {
        nativeOnPlay(mNativeProxy);
    }

    @Override
    public void onPlayFromMediaId(java.lang.String uri, android.os.Bundle extras) throws android.os.RemoteException {
        nativeOnPlayFromMediaId(mNativeProxy, uri, extras);
    }

    @Override
    public void onPlayFromSearch(java.lang.String query, android.os.Bundle extras) throws android.os.RemoteException {
        nativeOnPlayFromSearch(mNativeProxy, query, extras);
    }

    @Override
    public void onSkipToTrack(long id) throws android.os.RemoteException {
        nativeOnSkipToTrack(mNativeProxy, id);
    }

    @Override
    public void onPause() throws android.os.RemoteException {
        nativeOnPause(mNativeProxy);
    }

    @Override
    public void onStop() throws android.os.RemoteException {
        nativeOnStop(mNativeProxy);
    }

    @Override
    public void onNext() throws android.os.RemoteException {
        nativeOnNext(mNativeProxy);
    }

    @Override
    public void onPrevious() throws android.os.RemoteException {
        nativeOnPrevious(mNativeProxy);
    }

    @Override
    public void onFastForward() throws android.os.RemoteException {
        nativeOnFastForward(mNativeProxy);
    }

    @Override
    public void onRewind() throws android.os.RemoteException {
        nativeOnRewind(mNativeProxy);
    }

    @Override
    public void onSeekTo(long pos) throws android.os.RemoteException {
        nativeOnSeekTo(mNativeProxy, pos);
    }

    @Override
    public void setRemoteControlClientBrowsedPlayer() throws android.os.RemoteException {
        nativeSetRemoteControlClientBrowsedPlayer(mNativeProxy);
    }

    @Override
    public void setRemoteControlClientPlayItem(long uid, int scope) throws android.os.RemoteException {
        nativeSetRemoteControlClientPlayItem(mNativeProxy, uid, scope);
    }

    @Override
    public void getRemoteControlClientNowPlayingEntries() throws android.os.RemoteException {
        nativeGetRemoteControlClientNowPlayingEntries(mNativeProxy);
    }

    @Override
    public void onRate(android.media.Rating rating) throws android.os.RemoteException {
        nativeOnRate(mNativeProxy, rating);
    }

    @Override
    public void onCustomAction(java.lang.String action, android.os.Bundle args) throws android.os.RemoteException {
        nativeOnCustomAction(mNativeProxy, action, args);
    }
    // These callbacks are for volume handling

    @Override
    public void onAdjustVolume(int direction) throws android.os.RemoteException {
        nativeOnAdjustVolume(mNativeProxy, direction);
    }

    @Override
    public void onSetVolumeTo(int value) throws android.os.RemoteException {
        nativeOnSetVolumeTo(mNativeProxy, value);
    }
}
