package android.media.session;

class ElSessionControllerProxy extends ISessionController.Stub {

    private long mNativeProxy;

    public ElSessionControllerProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    //start native--
    private native void nativeSendCommand(long nativeProxy, java.lang.String command, android.os.Bundle args, android.os.ResultReceiver cb);
    private native boolean nativeSendMediaButton(long nativeProxy, android.view.KeyEvent mediaButton);
    private native void nativeRegisterCallbackListener(long nativeProxy, android.media.session.ISessionControllerCallback cb);
    private native void nativeUnregisterCallbackListener(long nativeProxy, android.media.session.ISessionControllerCallback cb);
    private native boolean nativeIsTransportControlEnabled(long nativeProxy);
    private native java.lang.String nativeGetPackageName(long nativeProxy);
    private native java.lang.String nativeGetTag(long nativeProxy);
    private native android.app.PendingIntent nativeGetLaunchPendingIntent(long nativeProxy);
    private native long nativeGetFlags(long nativeProxy);
    private native android.media.session.ParcelableVolumeInfo nativeGetVolumeAttributes(long nativeProxy);
    private native void nativeAdjustVolume(long nativeProxy, int direction, int flags, java.lang.String packageName);
    private native void nativeSetVolumeTo(long nativeProxy, int value, int flags, java.lang.String packageName);
    // These commands are for the TransportControls

    private native void nativePlay(long nativeProxy);
    private native void nativePlayFromMediaId(long nativeProxy, java.lang.String uri, android.os.Bundle extras);
    private native void nativePlayFromSearch(long nativeProxy, java.lang.String string, android.os.Bundle extras);
    private native void nativeSkipToQueueItem(long nativeProxy, long id);
    private native void nativePause(long nativeProxy);
    private native void nativeStop(long nativeProxy);
    private native void nativeNext(long nativeProxy);
    private native void nativePrevious(long nativeProxy);
    private native void nativeFastForward(long nativeProxy);
    private native void nativeRewind(long nativeProxy);
    private native void nativeSeekTo(long nativeProxy, long pos);
    private native void nativeSetRemoteControlClientBrowsedPlayer(long nativeProxy);
    private native void nativeSetRemoteControlClientPlayItem(long nativeProxy, long uid, int scope);
    private native void nativeGetRemoteControlClientNowPlayingEntries(long nativeProxy);
    private native void nativeRate(long nativeProxy, android.media.Rating rating);
    private native void nativeSendCustomAction(long nativeProxy, java.lang.String action, android.os.Bundle args);
    private native android.media.MediaMetadata nativeGetMetadata(long nativeProxy);
    private native android.media.session.PlaybackState nativeGetPlaybackState(long nativeProxy);
    private native android.content.pm.ParceledListSlice nativeGetQueue(long nativeProxy);
    private native java.lang.CharSequence nativeGetQueueTitle(long nativeProxy);
    private native android.os.Bundle nativeGetExtras(long nativeProxy);
    private native int nativeGetRatingType(long nativeProxy);
    //end native--

    @Override
    public void sendCommand(java.lang.String command, android.os.Bundle args, android.os.ResultReceiver cb) throws android.os.RemoteException {
        nativeSendCommand(mNativeProxy, command, args, cb);
    }

    @Override
    public boolean sendMediaButton(android.view.KeyEvent mediaButton) throws android.os.RemoteException {
        return nativeSendMediaButton(mNativeProxy, mediaButton);
    }

    @Override
    public void registerCallbackListener(android.media.session.ISessionControllerCallback cb) throws android.os.RemoteException {
        nativeRegisterCallbackListener(mNativeProxy, cb);
    }

    @Override
    public void unregisterCallbackListener(android.media.session.ISessionControllerCallback cb) throws android.os.RemoteException {
        nativeUnregisterCallbackListener(mNativeProxy, cb);
    }

    @Override
    public boolean isTransportControlEnabled() throws android.os.RemoteException {
        return nativeIsTransportControlEnabled(mNativeProxy);
    }

    @Override
    public java.lang.String getPackageName() throws android.os.RemoteException {
        return nativeGetPackageName(mNativeProxy);
    }

    @Override
    public java.lang.String getTag() throws android.os.RemoteException {
        return nativeGetTag(mNativeProxy);
    }

    @Override
    public android.app.PendingIntent getLaunchPendingIntent() throws android.os.RemoteException {
        return nativeGetLaunchPendingIntent(mNativeProxy);
    }

    @Override
    public long getFlags() throws android.os.RemoteException {
        return nativeGetFlags(mNativeProxy);
    }

    @Override
    public android.media.session.ParcelableVolumeInfo getVolumeAttributes() throws android.os.RemoteException {
        return nativeGetVolumeAttributes(mNativeProxy);
    }

    @Override
    public void adjustVolume(int direction, int flags, java.lang.String packageName) throws android.os.RemoteException {
        nativeAdjustVolume(mNativeProxy, direction, flags, packageName);
    }

    @Override
    public void setVolumeTo(int value, int flags, java.lang.String packageName) throws android.os.RemoteException {
        nativeSetVolumeTo(mNativeProxy, value, flags, packageName);
    }
    // These commands are for the TransportControls

    @Override
    public void play() throws android.os.RemoteException {
        nativePlay(mNativeProxy);
    }

    @Override
    public void playFromMediaId(java.lang.String uri, android.os.Bundle extras) throws android.os.RemoteException {
        nativePlayFromMediaId(mNativeProxy, uri, extras);
    }

    @Override
    public void playFromSearch(java.lang.String string, android.os.Bundle extras) throws android.os.RemoteException {
        nativePlayFromSearch(mNativeProxy, string, extras);
    }

    @Override
    public void skipToQueueItem(long id) throws android.os.RemoteException {
        nativeSkipToQueueItem(mNativeProxy, id);
    }

    @Override
    public void pause() throws android.os.RemoteException {
        nativePause(mNativeProxy);
    }

    @Override
    public void stop() throws android.os.RemoteException {
        nativeStop(mNativeProxy);
    }

    @Override
    public void next() throws android.os.RemoteException {
        nativeNext(mNativeProxy);
    }

    @Override
    public void previous() throws android.os.RemoteException {
        nativePrevious(mNativeProxy);
    }

    @Override
    public void fastForward() throws android.os.RemoteException {
        nativeFastForward(mNativeProxy);
    }

    @Override
    public void rewind() throws android.os.RemoteException {
        nativeRewind(mNativeProxy);
    }

    @Override
    public void seekTo(long pos) throws android.os.RemoteException {
        nativeSeekTo(mNativeProxy, pos);
    }

    @Override
    public void setRemoteControlClientBrowsedPlayer() throws android.os.RemoteException {
        nativeSetRemoteControlClientBrowsedPlayer(mNativeProxy);
    }

    @Override
    public void setRemoteControlClientPlayItem(long uid, int scope) throws android.os.RemoteException {
        nativeSetRemoteControlClientPlayItem(mNativeProxy, uid, scope);
    }

    @Override
    public void getRemoteControlClientNowPlayingEntries() throws android.os.RemoteException {
        nativeGetRemoteControlClientNowPlayingEntries(mNativeProxy);
    }

    @Override
    public void rate(android.media.Rating rating) throws android.os.RemoteException {
        nativeRate(mNativeProxy, rating);
    }

    @Override
    public void sendCustomAction(java.lang.String action, android.os.Bundle args) throws android.os.RemoteException {
        nativeSendCustomAction(mNativeProxy, action, args);
    }

    @Override
    public android.media.MediaMetadata getMetadata() throws android.os.RemoteException {
        return nativeGetMetadata(mNativeProxy);
    }

    @Override
    public android.media.session.PlaybackState getPlaybackState() throws android.os.RemoteException {
        return nativeGetPlaybackState(mNativeProxy);
    }

    @Override
    public android.content.pm.ParceledListSlice getQueue() throws android.os.RemoteException {
        return nativeGetQueue(mNativeProxy);
    }

    @Override
    public java.lang.CharSequence getQueueTitle() throws android.os.RemoteException {
        return nativeGetQueueTitle(mNativeProxy);
    }

    @Override
    public android.os.Bundle getExtras() throws android.os.RemoteException {
        return nativeGetExtras(mNativeProxy);
    }

    @Override
    public int getRatingType() throws android.os.RemoteException {
        return nativeGetRatingType(mNativeProxy);
    }
}
