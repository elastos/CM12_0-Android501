
package android.media;

import android.os.RemoteException;

/**
 * {@hide}
 */
public class ElMediaRouterServiceProxy extends IMediaRouterService.Stub {
    private long mNativeProxy;

    private native void nativeRegisterClientAsUser(long nativeProxy, IMediaRouterClient client, String packageName, int userId);

    private native void nativeUnregisterClient(long nativeProxy, IMediaRouterClient client);

    private native MediaRouterClientState nativeGetState(long nativeProxy, IMediaRouterClient client);

    private native void nativeSetDiscoveryRequest(long nativeProxy, IMediaRouterClient client, int routeTypes, boolean activeScan);

    private native void nativeSetSelectedRoute(long nativeProxy, IMediaRouterClient client, String routeId, boolean explicit);

    private native void nativeRequestSetVolume(long nativeProxy, IMediaRouterClient client, String routeId, int volume);

    private native void nativeRequestUpdateVolume(long nativeProxy, IMediaRouterClient client, String routeId, int direction);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElMediaRouterServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void registerClientAsUser(IMediaRouterClient client, String packageName, int userId) throws RemoteException {
        nativeRegisterClientAsUser(mNativeProxy, client, packageName, userId);
    }

    @Override
    public void unregisterClient(IMediaRouterClient client) throws RemoteException {
        nativeUnregisterClient(mNativeProxy, client);
    }

    @Override
    public MediaRouterClientState getState(IMediaRouterClient client) throws RemoteException {
        return nativeGetState(mNativeProxy, client);
    }

    @Override
    public void setDiscoveryRequest(IMediaRouterClient client, int routeTypes, boolean activeScan) throws RemoteException {
        nativeSetDiscoveryRequest(mNativeProxy, client, routeTypes, activeScan);
    }

    @Override
    public void setSelectedRoute(IMediaRouterClient client, String routeId, boolean explicit) throws RemoteException {
        nativeSetSelectedRoute(mNativeProxy, client, routeId, explicit);
    }

    @Override
    public void requestSetVolume(IMediaRouterClient client, String routeId, int volume) throws RemoteException {
        nativeRequestSetVolume(mNativeProxy, client, routeId, volume);
    }

    @Override
    public void requestUpdateVolume(IMediaRouterClient client, String routeId, int direction) throws RemoteException {
        nativeRequestUpdateVolume(mNativeProxy, client, routeId, direction);
    }
}
