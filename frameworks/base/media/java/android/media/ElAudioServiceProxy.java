
package android.media;

import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.media.AudioRoutesInfo;
import android.media.IAudioFocusDispatcher;
import android.media.IAudioRoutesObserver;
import android.media.IRemoteControlClient;
import android.media.IRemoteControlDisplay;
import android.media.IRemoteVolumeObserver;
import android.media.IRingtonePlayer;
import android.media.IVolumeController;
import android.media.Rating;
import android.media.audiopolicy.AudioPolicyConfig;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.KeyEvent;

/**
 * {@hide}
 */
public class ElAudioServiceProxy extends IAudioService.Stub {
    private long mNativeProxy;

    private native void nativePlaySoundEffectVolume(long nativeProxy, int effectType, float volume);

    private native int nativeGetMasterVolume(long nativeProxy);

    private native int nativeGetMasterMaxVolume(long nativeProxy);

    private native IRingtonePlayer nativeGetRingtonePlayer(long nativeProxy);

    private native int nativeGetStreamVolume(long nativeProxy, int streamType);

    private native int nativeGetStreamMaxVolume(long nativeProxy, int streamType);

    private native int nativeGetMode(long nativeProxy);

    private native void nativeSetMode(long nativeProxy, int mode, IBinder cb);

    private native void nativeSetStreamVolume(long nativeProxy, int streamType, int index, int flags, String callingPackage);

    private native boolean nativeIsSpeakerphoneOn(long nativeProxy);

    private native int nativeGetRingerMode(long nativeProxy);

    private native boolean nativeLoadSoundEffects(long nativeProxy);

    private native void nativeAdjustSuggestedStreamVolume(long nativeProxy, int direction, int suggestedStreamType, int flags, String callingPackage);

    private native void nativeAdjustStreamVolume(long nativeProxy, int streamType, int direction, int flags, String callingPackage);

    private native void nativeAdjustMasterVolume(long nativeProxy, int direction, int flags, String callingPackage);

    private native int nativeRequestAudioFocus(long nativeProxy, int mainStreamType, int durationHint, IBinder cb, IAudioFocusDispatcher l, String clientId, String callingPackageName);

    private native int nativeAbandonAudioFocus(long nativeProxy, IAudioFocusDispatcher l, String clientId);

    private native void nativeSetRemoteStreamVolume(long nativeProxy, int index);

    private native void nativeSetMasterVolume(long nativeProxy, int index, int flags, String callingPackage);

    private native void nativeSetStreamSolo(long nativeProxy, int streamType, boolean state, IBinder cb);

    private native void nativeSetStreamMute(long nativeProxy, int streamType, boolean state, IBinder cb);

    private native boolean nativeIsStreamMute(long nativeProxy, int streamType);

    private native void nativeForceRemoteSubmixFullVolume(long nativeProxy, boolean startForcing, IBinder cb);

    private native void nativeSetMasterMute(long nativeProxy, boolean state, int flags, String callingPackage, IBinder cb);

    private native boolean nativeIsMasterMute(long nativeProxy);

    private native int nativeGetLastAudibleStreamVolume(long nativeProxy, int streamType);

    private native int nativeGetLastAudibleMasterVolume(long nativeProxy);

    private native void nativeSetMicrophoneMute(long nativeProxy, boolean on, String callingPackage);

    private native void nativeSetRingerMode(long nativeProxy, int ringerMode, boolean checkZen);

    private native void nativeSetVibrateSetting(long nativeProxy, int vibrateType, int vibrateSetting);

    private native int nativeGetVibrateSetting(long nativeProxy, int vibrateType);

    private native boolean nativeShouldVibrate(long nativeProxy, int vibrateType);

    private native void nativePlaySoundEffect(long nativeProxy, int effectType);

    private native void nativeUnloadSoundEffects(long nativeProxy);

    private native void nativeReloadAudioSettings(long nativeProxy);

    private native void nativeAvrcpSupportsAbsoluteVolume(long nativeProxy, String address, boolean support);

    private native void nativeSetSpeakerphoneOn(long nativeProxy, boolean on);

    private native void nativeSetBluetoothScoOn(long nativeProxy, boolean on);

    private native boolean nativeIsBluetoothScoOn(long nativeProxy);

    private native void nativeSetBluetoothA2dpOn(long nativeProxy, boolean on);

    private native boolean nativeIsBluetoothA2dpOn(long nativeProxy);

    private native void nativeUnregisterAudioFocusClient(long nativeProxy, String clientId);

    private native int nativeGetCurrentAudioFocus(long nativeProxy);

    private native boolean nativeRegisterRemoteControlDisplay(long nativeProxy, IRemoteControlDisplay rcd, int w, int h);

    private native boolean nativeRegisterRemoteController(long nativeProxy, IRemoteControlDisplay rcd, int w, int h, ComponentName listenerComp);

    private native void nativeUnregisterRemoteControlDisplay(long nativeProxy, IRemoteControlDisplay rcd);

    private native void nativeRemoteControlDisplayUsesBitmapSize(long nativeProxy, IRemoteControlDisplay rcd, int w, int h);

    private native void nativeRemoteControlDisplayWantsPlaybackPositionSync(long nativeProxy, IRemoteControlDisplay rcd, boolean wantsSync);

    private native void nativeStartBluetoothSco(long nativeProxy, IBinder cb, int targetSdkVersion);

    private native void nativeStartBluetoothScoVirtualCall(long nativeProxy, IBinder cb);

    private native void nativeStopBluetoothSco(long nativeProxy, IBinder cb);

    private native void nativeForceVolumeControlStream(long nativeProxy, int streamType, IBinder cb);

    private native void nativeSetRingtonePlayer(long nativeProxy, IRingtonePlayer player);

    private native int nativeGetMasterStreamType(long nativeProxy);

    private native void nativeSetWiredDeviceConnectionState(long nativeProxy, int device, int state, String name);

    private native int nativeSetBluetoothA2dpDeviceConnectionState(long nativeProxy, BluetoothDevice device, int state, int profile);

    private native AudioRoutesInfo nativeStartWatchingRoutes(long nativeProxy, IAudioRoutesObserver observer);

    private native boolean nativeIsCameraSoundForced(long nativeProxy);

    private native void nativeSetVolumeController(long nativeProxy, IVolumeController controller);

    private native void nativeNotifyVolumeControllerVisible(long nativeProxy, IVolumeController controller, boolean visible);

    private native boolean nativeIsStreamAffectedByRingerMode(long nativeProxy, int streamType);

    private native void nativeDisableSafeMediaVolume(long nativeProxy);

    private native int nativeSetHdmiSystemAudioSupported(long nativeProxy, boolean on);

    private native boolean nativeIsHdmiSystemAudioSupported(long nativeProxy);

    private native boolean nativeRegisterAudioPolicy(long nativeProxy, AudioPolicyConfig policyConfig, IBinder cb);

    private native void nativeUnregisterAudioPolicyAsync(long nativeProxy, IBinder cb);

    private native void nativeSetRemoteControlClientBrowsedPlayer(long nativeProxy);

    private native void nativeGetRemoteControlClientNowPlayingEntries(long nativeProxy);

    private native void nativeSetRemoteControlClientPlayItem(long nativeProxy, long uid, int scope);

    private native void nativeUpdateRemoteControllerOnExistingMediaPlayers(long nativeProxy);

    private native void nativeAddMediaPlayerAndUpdateRemoteController(long nativeProxy, String packageName);

    private native void nativeRemoveMediaPlayerAndUpdateRemoteController(long nativeProxy, String packageName);

    private static final native void nativeFinalize(long nativeProxy);

    protected void finalize() throws Throwable {
        if (mNativeProxy != 0) {
            nativeFinalize(mNativeProxy);
            mNativeProxy = 0;
        }
    }

    public ElAudioServiceProxy(long nativeProxy) {
        mNativeProxy = nativeProxy;
    }

    @Override
    public void playSoundEffectVolume(int effectType, float volume) throws RemoteException {
        nativePlaySoundEffectVolume(mNativeProxy, effectType, volume);
    }

    @Override
    public int getMasterVolume() throws RemoteException {
        return nativeGetMasterVolume(mNativeProxy);
    }

    @Override
    public int getMasterMaxVolume() throws RemoteException {
        return nativeGetMasterMaxVolume(mNativeProxy);
    }

    @Override
    public IRingtonePlayer getRingtonePlayer() throws RemoteException {
        return nativeGetRingtonePlayer(mNativeProxy);
    }

    @Override
    public int getStreamVolume(int streamType) throws RemoteException {
        return nativeGetStreamVolume(mNativeProxy, streamType);
    }

    @Override
    public int getStreamMaxVolume(int streamType) throws RemoteException {
        return nativeGetStreamMaxVolume(mNativeProxy, streamType);
    }

    @Override
    public int getMode() throws RemoteException {
        return nativeGetMode(mNativeProxy);
    }

    @Override
    public void setMode(int mode, IBinder cb) throws RemoteException {
        nativeSetMode(mNativeProxy, mode, cb);
    }

    @Override
    public void setStreamVolume(int streamType, int index, int flags, String callingPackage) throws RemoteException {
        nativeSetStreamVolume(mNativeProxy, streamType, index, flags, callingPackage);
    }

    @Override
    public boolean isSpeakerphoneOn() throws RemoteException {
        return nativeIsSpeakerphoneOn(mNativeProxy);
    }

    @Override
    public int getRingerMode() throws RemoteException {
        return nativeGetRingerMode(mNativeProxy);
    }

    @Override
    public boolean loadSoundEffects() throws RemoteException {
        return nativeLoadSoundEffects(mNativeProxy);
    }

    @Override
    public void adjustSuggestedStreamVolume(int direction, int suggestedStreamType, int flags, String callingPackage) throws RemoteException {
        nativeAdjustSuggestedStreamVolume(mNativeProxy, direction, suggestedStreamType, flags, callingPackage);
    }

    @Override
    public void adjustStreamVolume(int streamType, int direction, int flags, String callingPackage) throws RemoteException {
        nativeAdjustStreamVolume(mNativeProxy, streamType, direction, flags, callingPackage);
    }

    @Override
    public void adjustMasterVolume(int direction, int flags, String callingPackage) throws RemoteException {
        nativeAdjustMasterVolume(mNativeProxy, direction, flags, callingPackage);
    }

    @Override
    public void setRemoteStreamVolume(int index) throws RemoteException {
        nativeSetRemoteStreamVolume(mNativeProxy, index);
    }

    @Override
    public void setMasterVolume(int index, int flags, String callingPackage) throws RemoteException {
        nativeSetMasterVolume(mNativeProxy, index, flags, callingPackage);
    }

    @Override
    public void setStreamSolo(int streamType, boolean state, IBinder cb) throws RemoteException {
        nativeSetStreamSolo(mNativeProxy, streamType, state, cb);
    }

    @Override
    public void setStreamMute(int streamType, boolean state, IBinder cb) throws RemoteException {
        nativeSetStreamMute(mNativeProxy, streamType, state, cb);
    }

    @Override
    public boolean isStreamMute(int streamType) throws RemoteException {
        return nativeIsStreamMute(mNativeProxy, streamType);
    }

    @Override
    public void forceRemoteSubmixFullVolume(boolean startForcing, IBinder cb) throws RemoteException {
        nativeForceRemoteSubmixFullVolume(mNativeProxy, startForcing, cb);
    }

    @Override
    public void setMasterMute(boolean state, int flags, String callingPackage, IBinder cb) throws RemoteException {
        nativeSetMasterMute(mNativeProxy, state, flags, callingPackage, cb);
    }

    @Override
    public boolean isMasterMute() throws RemoteException {
        return nativeIsMasterMute(mNativeProxy);
    }

    @Override
    public int getLastAudibleStreamVolume(int streamType) throws RemoteException {
        return nativeGetLastAudibleStreamVolume(mNativeProxy, streamType);
    }

    @Override
    public int getLastAudibleMasterVolume() throws RemoteException {
        return nativeGetLastAudibleMasterVolume(mNativeProxy);
    }

    @Override
    public void setMicrophoneMute(boolean on, String callingPackage) throws RemoteException {
        nativeSetMicrophoneMute(mNativeProxy, on, callingPackage);
    }
    @Override
    public void setRingerMode(int ringerMode, boolean checkZen) throws RemoteException {
        nativeSetRingerMode(mNativeProxy, ringerMode, checkZen);
    }

    @Override
    public void setVibrateSetting(int vibrateType, int vibrateSetting) throws RemoteException {
        nativeSetVibrateSetting(mNativeProxy, vibrateType, vibrateSetting);
    }

    @Override
    public int getVibrateSetting(int vibrateType) throws RemoteException {
        return nativeGetVibrateSetting(mNativeProxy, vibrateType);
    }

    @Override
    public boolean shouldVibrate(int vibrateType) throws RemoteException {
        return nativeShouldVibrate(mNativeProxy, vibrateType);
    }

    @Override
    public void playSoundEffect(int effectType) throws RemoteException {
        nativePlaySoundEffect(mNativeProxy, effectType);
    }

    @Override
    public void unloadSoundEffects() throws RemoteException {
        nativeUnloadSoundEffects(mNativeProxy);
    }

    @Override
    public void reloadAudioSettings() throws RemoteException {
        nativeReloadAudioSettings(mNativeProxy);
    }

    @Override
    public void avrcpSupportsAbsoluteVolume(String address, boolean support) throws RemoteException {
        nativeAvrcpSupportsAbsoluteVolume(mNativeProxy, address, support);
    }
    @Override
    public void setSpeakerphoneOn(boolean on) throws RemoteException {
        nativeSetSpeakerphoneOn(mNativeProxy, on);
    }

    @Override
    public void setBluetoothScoOn(boolean on) throws RemoteException {
        nativeSetBluetoothScoOn(mNativeProxy, on);
    }

    @Override
    public boolean isBluetoothScoOn() throws RemoteException {
        return nativeIsBluetoothScoOn(mNativeProxy);
    }

    @Override
    public void setBluetoothA2dpOn(boolean on) throws RemoteException {
        nativeSetBluetoothA2dpOn(mNativeProxy, on);
    }

    @Override
    public boolean isBluetoothA2dpOn() throws RemoteException {
        return nativeIsBluetoothA2dpOn(mNativeProxy);
    }

    @Override
    public int requestAudioFocus(int mainStreamType, int durationHint, IBinder cb, IAudioFocusDispatcher l,
            String clientId, String callingPackageName) throws RemoteException {
        return nativeRequestAudioFocus(mNativeProxy, mainStreamType, durationHint, cb, l, clientId, callingPackageName);
    }

    @Override
    public int abandonAudioFocus(IAudioFocusDispatcher l, String clientId) throws RemoteException {
        return nativeAbandonAudioFocus(mNativeProxy, l, clientId);
    }

    @Override
    public void unregisterAudioFocusClient(String clientId) throws RemoteException {
        nativeUnregisterAudioFocusClient(mNativeProxy, clientId);
    }

    @Override
    public int getCurrentAudioFocus() throws RemoteException {
        return nativeGetCurrentAudioFocus(mNativeProxy);
    }

    @Override
    public boolean registerRemoteControlDisplay(IRemoteControlDisplay rcd, int w, int h) throws RemoteException {
        return nativeRegisterRemoteControlDisplay(mNativeProxy, rcd, w, h);
    }

    @Override
    public boolean registerRemoteController(IRemoteControlDisplay rcd, int w, int h, ComponentName listenerComp) throws RemoteException {
        return nativeRegisterRemoteController(mNativeProxy, rcd, w, h, listenerComp);
    }

    @Override
    public void unregisterRemoteControlDisplay(IRemoteControlDisplay rcd) throws RemoteException {
        nativeUnregisterRemoteControlDisplay(mNativeProxy, rcd);
    }

    @Override
    public void remoteControlDisplayUsesBitmapSize(IRemoteControlDisplay rcd, int w, int h) throws RemoteException {
        nativeRemoteControlDisplayUsesBitmapSize(mNativeProxy, rcd, w, h);
    }

    @Override
    public void remoteControlDisplayWantsPlaybackPositionSync(IRemoteControlDisplay rcd, boolean wantsSync) throws RemoteException {
        nativeRemoteControlDisplayWantsPlaybackPositionSync(mNativeProxy, rcd, wantsSync);
    }

    @Override
    public void startBluetoothSco(IBinder cb, int targetSdkVersion) throws RemoteException {
        nativeStartBluetoothSco(mNativeProxy, cb, targetSdkVersion);
    }

        @Override
    public void startBluetoothScoVirtualCall(IBinder cb) throws RemoteException {
        nativeStartBluetoothScoVirtualCall(mNativeProxy, cb);
    }

    @Override
    public void stopBluetoothSco(IBinder cb) throws RemoteException {
        nativeStopBluetoothSco(mNativeProxy, cb);
    }

    @Override
    public void forceVolumeControlStream(int streamType, IBinder cb) throws RemoteException {
        nativeForceVolumeControlStream(mNativeProxy, streamType, cb);
    }

    @Override
    public void setRingtonePlayer(IRingtonePlayer player) throws RemoteException {
        nativeSetRingtonePlayer(mNativeProxy, player);
    }

    @Override
    public int getMasterStreamType() throws RemoteException {
        return nativeGetMasterStreamType(mNativeProxy);
    }

    @Override
    public void setWiredDeviceConnectionState(int device, int state, String name) throws RemoteException {
        nativeSetWiredDeviceConnectionState(mNativeProxy, device, state, name);
    }

    @Override
    public int setBluetoothA2dpDeviceConnectionState(BluetoothDevice device, int state, int profile) throws RemoteException {
        return nativeSetBluetoothA2dpDeviceConnectionState(mNativeProxy, device, state, profile);
    }

    @Override
    public AudioRoutesInfo startWatchingRoutes(IAudioRoutesObserver observer) throws RemoteException {
        return nativeStartWatchingRoutes(mNativeProxy, observer);
    }

    @Override
    public boolean isCameraSoundForced() throws RemoteException {
        return nativeIsCameraSoundForced(mNativeProxy);
    }

    @Override
    public void setVolumeController(IVolumeController controller) throws RemoteException {
        nativeSetVolumeController(mNativeProxy, controller);
    }

    @Override
    public void notifyVolumeControllerVisible(IVolumeController controller, boolean visible) throws RemoteException {
        nativeNotifyVolumeControllerVisible(mNativeProxy, controller, visible);
    }

    @Override
    public boolean isStreamAffectedByRingerMode(int streamType) throws RemoteException {
        return nativeIsStreamAffectedByRingerMode(mNativeProxy, streamType);
    }

    @Override
    public void disableSafeMediaVolume() throws RemoteException {
        nativeDisableSafeMediaVolume(mNativeProxy);
    }

    @Override
    public int setHdmiSystemAudioSupported(boolean on) throws RemoteException {
        return nativeSetHdmiSystemAudioSupported(mNativeProxy, on);
    }

    @Override
    public boolean isHdmiSystemAudioSupported() throws RemoteException {
        return nativeIsHdmiSystemAudioSupported(mNativeProxy);
    }

    @Override
    public boolean registerAudioPolicy(AudioPolicyConfig policyConfig, IBinder cb) throws RemoteException {
        return nativeRegisterAudioPolicy(mNativeProxy, policyConfig, cb);
    }

    @Override
    public void unregisterAudioPolicyAsync(IBinder cb) throws RemoteException {
        nativeUnregisterAudioPolicyAsync(mNativeProxy, cb);
    }

    @Override
    public void setRemoteControlClientBrowsedPlayer() throws RemoteException {
        nativeSetRemoteControlClientBrowsedPlayer(mNativeProxy);
    }

    @Override
    public void getRemoteControlClientNowPlayingEntries() throws RemoteException {
        nativeGetRemoteControlClientNowPlayingEntries(mNativeProxy);
    }

    @Override
    public void setRemoteControlClientPlayItem(long uid, int scope) throws RemoteException {
        nativeSetRemoteControlClientPlayItem(mNativeProxy, uid, scope);
    }

    @Override
    public void updateRemoteControllerOnExistingMediaPlayers() throws RemoteException {
        nativeUpdateRemoteControllerOnExistingMediaPlayers(mNativeProxy);
    }

    @Override
    public void addMediaPlayerAndUpdateRemoteController(String packageName) throws RemoteException {
        nativeAddMediaPlayerAndUpdateRemoteController(mNativeProxy, packageName);
    }

    @Override
    public void removeMediaPlayerAndUpdateRemoteController(String packageName) throws RemoteException {
        nativeRemoveMediaPlayerAndUpdateRemoteController(mNativeProxy, packageName);
    }
}
