// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "media/midi/usb_midi_device_elastos.h"

//#include <jni.h>
#include <vector>

#include "base/elastos/jni_array.h"
#include "base/time/time.h"
#include "media/api/UsbMidiDeviceAndroid_api.h"

namespace media {

//UsbMidiDeviceAndroid::UsbMidiDeviceAndroid(ObjectRef raw_device, UsbMidiDeviceDelegate* delegate)
UsbMidiDeviceAndroid::UsbMidiDeviceAndroid(IInterface* raw_device, UsbMidiDeviceDelegate* delegate)
    : raw_device_(raw_device), delegate_(delegate) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaUsbMidiDeviceAndroidCallback);
  //Java_UsbMidiDeviceAndroid_registerSelf(env, raw_device_.obj(), reinterpret_cast<jlong>(this));
  sElaUsbMidiDeviceAndroidCallback->
      elastos_UsbMidiDeviceAndroid_registerSelf(raw_device_.Get(), reinterpret_cast<Elastos::Int64>(this));
}

UsbMidiDeviceAndroid::~UsbMidiDeviceAndroid() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaUsbMidiDeviceAndroidCallback);
  //Java_UsbMidiDeviceAndroid_close(env, raw_device_.obj());
  sElaUsbMidiDeviceAndroidCallback->elastos_UsbMidiDeviceAndroid_close(raw_device_.Get());
}

std::vector<uint8> UsbMidiDeviceAndroid::GetDescriptor() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaUsbMidiDeviceAndroidCallback);
  //base::android::ScopedJavaLocalRef<jbyteArray> descriptors =
  //    Java_UsbMidiDeviceAndroid_getDescriptors(env, raw_device_.obj());
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > descriptors =
      sElaUsbMidiDeviceAndroidCallback->elastos_UsbMidiDeviceAndroid_getDescriptors(raw_device_.Get());

  std::vector<uint8> ret;
  base::android::JavaByteArrayToByteVector(descriptors.Get(), &ret);
  return ret;
}

void UsbMidiDeviceAndroid::Send(int endpoint_number,
                                const std::vector<uint8>& data) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaUsbMidiDeviceAndroidCallback);
  const uint8* head = data.size() ? &data[0] : NULL;
  //ScopedJavaLocalRef<jbyteArray> data_to_pass = base::android::ToJavaByteArray(env, head, data.size());
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > data_to_pass = base::android::ToJavaByteArray(head, data.size());

  //Java_UsbMidiDeviceAndroid_send(env, raw_device_.obj(), endpoint_number, data_to_pass.obj());
  sElaUsbMidiDeviceAndroidCallback->elastos_UsbMidiDeviceAndroid_send(
          raw_device_.Get(), endpoint_number, data_to_pass.Get());
}

void UsbMidiDeviceAndroid::OnData(Elastos::Int32 endpoint_number,
                                  Elastos::ArrayOf<Elastos::Byte>* data) {
  std::vector<uint8> bytes;
  base::android::JavaByteArrayToByteVector(data, &bytes);

  const uint8* head = bytes.size() ? &bytes[0] : NULL;
  delegate_->ReceiveUsbMidiData(this, endpoint_number, head, bytes.size(),
                                base::TimeTicks::HighResNow());
}

bool UsbMidiDeviceAndroid::RegisterUsbMidiDevice() {
  return RegisterNativesImpl();
}

}  // namespace media
