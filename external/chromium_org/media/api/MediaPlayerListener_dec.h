//This file is autogenerated for
//    MediaPlayerListener.java
//put this file at the end of the include list
//so the type definition used in this file will be found
#ifndef ELASTOS_MEDIAPLAYERLISTENER_CALLBACK_DEC_HH
#define ELASTOS_MEDIAPLAYERLISTENER_CALLBACK_DEC_HH


#ifdef __cplusplus
extern "C"
{
#endif
    extern void Elastos_MediaPlayerListener_nativeOnMediaError(IInterface* caller,Handle32 nativeMediaPlayerListener,Int32 errorType);
    extern void Elastos_MediaPlayerListener_nativeOnVideoSizeChanged(IInterface* caller,Handle32 nativeMediaPlayerListener,Int32 width,Int32 height);
    extern void Elastos_MediaPlayerListener_nativeOnBufferingUpdate(IInterface* caller,Handle32 nativeMediaPlayerListener,Int32 percent);
    extern void Elastos_MediaPlayerListener_nativeOnMediaPrepared(IInterface* caller,Handle32 nativeMediaPlayerListener);
    extern void Elastos_MediaPlayerListener_nativeOnPlaybackComplete(IInterface* caller,Handle32 nativeMediaPlayerListener);
    extern void Elastos_MediaPlayerListener_nativeOnSeekComplete(IInterface* caller,Handle32 nativeMediaPlayerListener);
    extern void Elastos_MediaPlayerListener_nativeOnMediaInterrupted(IInterface* caller,Handle32 nativeMediaPlayerListener);
#ifdef __cplusplus
}
#endif


struct ElaMediaPlayerListenerCallback
{
    void (*elastos_MediaPlayerListener_releaseResources)(IInterface* obj);
    AutoPtr<IInterface> (*elastos_MediaPlayerListener_create)(Int64 nativeMediaPlayerListener, IInterface* context, IInterface* mediaPlayerBridge);
};


#endif //ELASTOS_MEDIAPLAYERLISTENER_CALLBACK_DEC_HH
