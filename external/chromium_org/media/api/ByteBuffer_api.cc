#include "ByteBuffer_api.h"

namespace media {

extern "C" {
struct ElaByteBufferCallback* sElaByteBufferCallback;
__attribute__((visibility("default")))
void Elastos_ByteBuffer_InitCallback(Elastos::Handle32 cb)
{
    sElaByteBufferCallback = (struct ElaByteBufferCallback*)cb;
    DLOG(INFO) << "init pointer for sElaByteBufferCallback is:" << sElaByteBufferCallback;
}
}; // extern "C"

}  // namespace media
