//This file is autogenerated for
//    MediaCodecBridge.java
//put this file at the end of the include list
//so the type definition used in this file will be found
#ifndef ELASTOS_MEDIACODECBRIDGE_CALLBACK_DEC_HH
#define ELASTOS_MEDIACODECBRIDGE_CALLBACK_DEC_HH


struct ElaMediaCodecBridgeCallback
{
    Int32 (*elastos_DequeueInputResult_status)(IInterface* obj);
    Int32 (*elastos_DequeueInputResult_index)(IInterface* obj);
    Elastos::String (*elastos_CodecInfo_codecType)(IInterface* obj);
    Elastos::String (*elastos_CodecInfo_codecName)(IInterface* obj);
    Int32 (*elastos_CodecInfo_direction)(IInterface* obj);
    Int32 (*elastos_DequeueOutputResult_status)(IInterface* obj);
    Int32 (*elastos_DequeueOutputResult_index)(IInterface* obj);
    Int32 (*elastos_DequeueOutputResult_flags)(IInterface* obj);
    Int32 (*elastos_DequeueOutputResult_offset)(IInterface* obj);
    Int64 (*elastos_DequeueOutputResult_presentationTimeMicroseconds)(IInterface* obj);
    Int32 (*elastos_DequeueOutputResult_numBytes)(IInterface* obj);
    AutoPtr<ArrayOf<IInterface*> > (*elastos_MediaCodecBridge_getCodecsInfo)();
    AutoPtr<IInterface> (*elastos_MediaCodecBridge_create)(const Elastos::String& mime, Boolean isSecure, Int32 direction);
    void (*elastos_MediaCodecBridge_release)(IInterface* obj);
    Boolean (*elastos_MediaCodecBridge_start)(IInterface* obj);
    AutoPtr<IInterface> (*elastos_MediaCodecBridge_dequeueInputBuffer)(IInterface* obj, Int64 timeoutUs);
    Int32 (*elastos_MediaCodecBridge_flush)(IInterface* obj);
    void (*elastos_MediaCodecBridge_stop)(IInterface* obj);
    Int32 (*elastos_MediaCodecBridge_getOutputHeight)(IInterface* obj);
    Int32 (*elastos_MediaCodecBridge_getOutputWidth)(IInterface* obj);
    AutoPtr<IInterface> (*elastos_MediaCodecBridge_getInputBuffer)(IInterface* obj, Int32 index);
    AutoPtr<IInterface> (*elastos_MediaCodecBridge_getOutputBuffer)(IInterface* obj, Int32 index);
    Int32 (*elastos_MediaCodecBridge_getInputBuffersCount)(IInterface* obj);
    Int32 (*elastos_MediaCodecBridge_getOutputBuffersCount)(IInterface* obj);
    Int32 (*elastos_MediaCodecBridge_getOutputBuffersCapacity)(IInterface* obj);
    Boolean (*elastos_MediaCodecBridge_getOutputBuffers)(IInterface* obj);
    Int32 (*elastos_MediaCodecBridge_queueInputBuffer)(IInterface* obj, Int32 index, Int32 offset, Int32 size, Int64 presentationTimeUs, Int32 flags);
    void (*elastos_MediaCodecBridge_setVideoBitrate)(IInterface* obj, Int32 bps);
    void (*elastos_MediaCodecBridge_requestKeyFrameSoon)(IInterface* obj);
    Int32 (*elastos_MediaCodecBridge_queueSecureInputBuffer)(IInterface* obj, Int32 index, Int32 offset, ArrayOf<Byte>* iv, ArrayOf<Byte>* keyId, ArrayOf<Int32>* numBytesOfClearData, ArrayOf<Int32>* numBytesOfEncryptedData, Int32 numSubSamples, Int64 presentationTimeUs);
    void (*elastos_MediaCodecBridge_releaseOutputBuffer)(IInterface* obj, Int32 index, Boolean render);
    AutoPtr<IInterface> (*elastos_MediaCodecBridge_dequeueOutputBuffer)(IInterface* obj, Int64 timeoutUs);
    Boolean (*elastos_MediaCodecBridge_configureVideo)(IInterface* obj, IInterface* format, IInterface* surface, IInterface* crypto, Int32 flags);
    AutoPtr<IInterface> (*elastos_MediaCodecBridge_createAudioFormat)(const Elastos::String& mime, Int32 sampleRate, Int32 channelCount);
    AutoPtr<IInterface> (*elastos_MediaCodecBridge_createVideoDecoderFormat)(const Elastos::String& mime, Int32 width, Int32 height);
    AutoPtr<IInterface> (*elastos_MediaCodecBridge_createVideoEncoderFormat)(const Elastos::String& mime, Int32 width, Int32 height, Int32 bitRate, Int32 frameRate, Int32 iFrameInterval, Int32 colorFormat);
    Boolean (*elastos_MediaCodecBridge_isAdaptivePlaybackSupported)(IInterface* obj, Int32 width, Int32 height);
    void (*elastos_MediaCodecBridge_setCodecSpecificData)(IInterface* format, Int32 index, ArrayOf<Byte>* bytes);
    void (*elastos_MediaCodecBridge_setFrameHasADTSHeader)(IInterface* format);
    Boolean (*elastos_MediaCodecBridge_configureAudio)(IInterface* obj, IInterface* format, IInterface* crypto, Int32 flags, Boolean playAudio);
    Int64 (*elastos_MediaCodecBridge_playOutputBuffer)(IInterface* obj, ArrayOf<Byte>* buf);
    void (*elastos_MediaCodecBridge_setVolume)(IInterface* obj, Double volume);
};


#endif //ELASTOS_MEDIACODECBRIDGE_CALLBACK_DEC_HH
