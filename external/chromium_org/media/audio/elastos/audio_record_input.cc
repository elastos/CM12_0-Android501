// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "media/audio/elastos/audio_record_input.h"

#include "base/logging.h"
#include "media/api/AudioRecordInput_api.h"
#include "media/api/ByteBuffer_api.h"
#include "media/audio/elastos/audio_manager_android.h"
#include "media/base/audio_bus.h"

namespace media {

AudioRecordInputStream::AudioRecordInputStream(
    AudioManagerAndroid* audio_manager,
    const AudioParameters& params)
    : audio_manager_(audio_manager),
      callback_(NULL),
      direct_buffer_address_(NULL),
      audio_bus_(media::AudioBus::Create(params)),
      bytes_per_sample_(params.bits_per_sample() / 8) {
  DVLOG(2) << __PRETTY_FUNCTION__;
  DCHECK(params.IsValid());
  DCHECK(sElaAudioRecordInputCallback);
  //j_audio_record_.Reset(
  //    Java_AudioRecordInput_createAudioRecordInput(
  //        base::android::AttachCurrentThread(),
  j_audio_record_ =
      sElaAudioRecordInputCallback->elastos_AudioRecordInput_createAudioRecordInput(
          reinterpret_cast<intptr_t>(this),
          params.sample_rate(),
          params.channels(),
          params.bits_per_sample(),
          params.GetBytesPerBuffer(),
          params.effects() & AudioParameters::ECHO_CANCELLER);
}

AudioRecordInputStream::~AudioRecordInputStream() {
  DVLOG(2) << __PRETTY_FUNCTION__;
  DCHECK(thread_checker_.CalledOnValidThread());
}

void AudioRecordInputStream::CacheDirectBufferAddress(IInterface* obj,
                                                      IInterface* byte_buffer) {
  DCHECK(thread_checker_.CalledOnValidThread());
  //(leliang) this function will be called in java AudioRecordInput.java
  //where the byte_fuffer will be a reference to ByteBuffer
  direct_buffer_address_ = static_cast<uint8*>(
    sElaByteBufferCallback->elastos_ByteBuffer_GetDirectBufferAddress(byte_buffer)
          );
  //    env->GetDirectBufferAddress(byte_buffer));
  LOG(INFO) << "audio_record_input.cc:CacheDirectBufferAddress, byte_buffer:" << byte_buffer
            << "; direct_buffer_address_:" << direct_buffer_address_;
}

// static
bool AudioRecordInputStream::RegisterAudioRecordInput() {
  return RegisterNativesImpl();
}

//void AudioRecordInputStream::OnData(JNIEnv* env, jobject obj, jint size, jint hardware_delay_bytes) {
void AudioRecordInputStream::OnData(IInterface* obj, Elastos::Int32 size, Elastos::Int32 hardware_delay_bytes) {
  DCHECK(direct_buffer_address_);
  DCHECK_EQ(size,
            audio_bus_->frames() * audio_bus_->channels() * bytes_per_sample_);
  // Passing zero as the volume parameter indicates there is no access to a
  // hardware volume slider.
  audio_bus_->FromInterleaved(
      direct_buffer_address_, audio_bus_->frames(), bytes_per_sample_);
  callback_->OnData(this, audio_bus_.get(), hardware_delay_bytes, 0.0);
}

bool AudioRecordInputStream::Open() {
  DVLOG(2) << __PRETTY_FUNCTION__;
  DCHECK(thread_checker_.CalledOnValidThread());
  DCHECK(sElaAudioRecordInputCallback);
  //return Java_AudioRecordInput_open(base::android::AttachCurrentThread(), j_audio_record_.obj());
  return sElaAudioRecordInputCallback->elastos_AudioRecordInput_open(j_audio_record_.Get());
}

void AudioRecordInputStream::Start(AudioInputCallback* callback) {
  DVLOG(2) << __PRETTY_FUNCTION__;
  DCHECK(thread_checker_.CalledOnValidThread());
  DCHECK(callback);

  if (callback_) {
    // Start() was already called.
    DCHECK_EQ(callback_, callback);
    return;
  }
  // The Java thread has not yet started, so we are free to set |callback_|.
  callback_ = callback;

  DCHECK(sElaAudioRecordInputCallback);
  //Java_AudioRecordInput_start(base::android::AttachCurrentThread(), j_audio_record_.obj());
  sElaAudioRecordInputCallback->elastos_AudioRecordInput_start(j_audio_record_.Get());
}

void AudioRecordInputStream::Stop() {
  DVLOG(2) << __PRETTY_FUNCTION__;
  DCHECK(thread_checker_.CalledOnValidThread());
  if (!callback_) {
    // Start() was never called, or Stop() was already called.
    return;
  }

  DCHECK(sElaAudioRecordInputCallback);
  //Java_AudioRecordInput_stop(base::android::AttachCurrentThread(), j_audio_record_.obj());
  sElaAudioRecordInputCallback->elastos_AudioRecordInput_stop(j_audio_record_.Get());

  // The Java thread must have been stopped at this point, so we are free to
  // clear |callback_|.
  callback_ = NULL;
}

void AudioRecordInputStream::Close() {
  DVLOG(2) << __PRETTY_FUNCTION__;
  DCHECK(thread_checker_.CalledOnValidThread());
  Stop();
  DCHECK(!callback_);
  DCHECK(sElaAudioRecordInputCallback);
  //Java_AudioRecordInput_close(base::android::AttachCurrentThread(), j_audio_record_.obj());
  sElaAudioRecordInputCallback->elastos_AudioRecordInput_close(j_audio_record_.Get());
  audio_manager_->ReleaseInputStream(this);
}

double AudioRecordInputStream::GetMaxVolume() {
  NOTIMPLEMENTED();
  return 0.0;
}

void AudioRecordInputStream::SetVolume(double volume) {
  NOTIMPLEMENTED();
}

double AudioRecordInputStream::GetVolume() {
  NOTIMPLEMENTED();
  return 0.0;
}

void AudioRecordInputStream::SetAutomaticGainControl(bool enabled) {
  NOTIMPLEMENTED();
}

bool AudioRecordInputStream::GetAutomaticGainControl() {
  NOTIMPLEMENTED();
  return false;
}

}  // namespace media
