// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "media/base/elastos/media_jni_registrar.h"

#include "base/basictypes.h"
#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"

#include "media/audio/elastos/audio_manager_android.h"
#include "media/audio/elastos/audio_record_input.h"
#include "media/base/elastos/media_codec_bridge.h"
#include "media/base/elastos/media_drm_bridge.h"
#include "media/base/elastos/media_player_bridge.h"
#include "media/base/elastos/media_player_listener.h"
#include "media/base/elastos/webaudio_media_codec_bridge.h"
#include "media/midi/usb_midi_device_elastos.h"
#include "media/midi/usb_midi_device_factory_elastos.h"
#include "media/video/capture/elastos/video_capture_device_android.h"
#include "media/video/capture/elastos/video_capture_device_factory_android.h"

namespace media {

static base::android::RegistrationMethod kMediaRegisteredMethods[] = {
  { "AudioManagerAndroid",
    AudioManagerAndroid::RegisterAudioManager },
  { "AudioRecordInput",
    AudioRecordInputStream::RegisterAudioRecordInput },
  { "MediaCodecBridge",
    MediaCodecBridge::RegisterMediaCodecBridge },
  { "MediaDrmBridge",
    MediaDrmBridge::RegisterMediaDrmBridge },
  { "MediaPlayerBridge",
    MediaPlayerBridge::RegisterMediaPlayerBridge },
  { "MediaPlayerListener",
    MediaPlayerListener::RegisterMediaPlayerListener },
  { "UsbMidiDevice",
    UsbMidiDeviceAndroid::RegisterUsbMidiDevice },
  { "UsbMidiDeviceFactory",
    UsbMidiDeviceFactoryAndroid::RegisterUsbMidiDeviceFactory },
  { "VideoCaptureDevice",
    VideoCaptureDeviceAndroid::RegisterVideoCaptureDevice },
  { "VideoCaptureDeviceFactory",
    VideoCaptureDeviceFactoryAndroid::RegisterVideoCaptureDeviceFactory },
  { "WebAudioMediaCodecBridge",
    WebAudioMediaCodecBridge::RegisterWebAudioMediaCodecBridge },
};

bool RegisterJni() {
  return base::android::RegisterNativeMethods(
      kMediaRegisteredMethods, arraysize(kMediaRegisteredMethods));
}

}  // namespace media
