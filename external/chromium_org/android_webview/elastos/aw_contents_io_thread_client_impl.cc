// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/aw_contents_io_thread_client_impl.h"

#include <map>
#include <utility>

#include "android_webview/common/devtools_instrumentation.h"
#include "android_webview/elastos/aw_web_resource_response_impl.h"
#include "base/elastos/jni_array.h"
#include "base/elastos/jni_string.h"
#include "base/elastos/api_weak_ref.h"
#include "base/lazy_instance.h"
#include "base/memory/linked_ptr.h"
#include "base/memory/scoped_ptr.h"
#include "base/synchronization/lock.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/render_frame_host.h"
#include "content/public/browser/render_process_host.h"
#include "content/public/browser/render_view_host.h"
#include "content/public/browser/resource_request_info.h"
#include "content/public/browser/web_contents.h"
#include "content/public/browser/web_contents_observer.h"
#include "android_webview/api/AwContentsIoThreadClient_api.h"
#include "net/http/http_request_headers.h"
#include "net/url_request/url_request.h"
#include "url/gurl.h"

//using base::android::AttachCurrentThread;
using base::android::ConvertUTF8ToJavaString;
//using base::android::JavaRef;
//using base::android::ScopedJavaLocalRef;
using base::android::ToJavaArrayOfStrings;
using base::LazyInstance;
using content::BrowserThread;
using content::RenderFrameHost;
using content::WebContents;
using std::map;
using std::pair;
using std::string;
using std::vector;

namespace android_webview {

namespace {

struct IoThreadClientData {
  bool pending_association;
  ObjectWeakGlobalRef io_thread_client;

  IoThreadClientData();
};

IoThreadClientData::IoThreadClientData() : pending_association(false) {}

typedef map<pair<int, int>, IoThreadClientData>
    RenderFrameHostToIoThreadClientType;

static pair<int, int> GetRenderFrameHostIdPair(RenderFrameHost* rfh) {
  return pair<int, int>(rfh->GetProcess()->GetID(), rfh->GetRoutingID());
}

// RfhToIoThreadClientMap -----------------------------------------------------
class RfhToIoThreadClientMap {
 public:
  static RfhToIoThreadClientMap* GetInstance();
  void Set(pair<int, int> rfh_id, const IoThreadClientData& client);
  bool Get(pair<int, int> rfh_id, IoThreadClientData* client);
  void Erase(pair<int, int> rfh_id);

 private:
  static LazyInstance<RfhToIoThreadClientMap> g_instance_;
  base::Lock map_lock_;
  RenderFrameHostToIoThreadClientType rfh_to_io_thread_client_;
};

// static
LazyInstance<RfhToIoThreadClientMap> RfhToIoThreadClientMap::g_instance_ =
    LAZY_INSTANCE_INITIALIZER;

// static
RfhToIoThreadClientMap* RfhToIoThreadClientMap::GetInstance() {
  return g_instance_.Pointer();
}

void RfhToIoThreadClientMap::Set(pair<int, int> rfh_id,
                                 const IoThreadClientData& client) {
  base::AutoLock lock(map_lock_);
  rfh_to_io_thread_client_[rfh_id] = client;
}

bool RfhToIoThreadClientMap::Get(
    pair<int, int> rfh_id, IoThreadClientData* client) {
  base::AutoLock lock(map_lock_);
  RenderFrameHostToIoThreadClientType::iterator iterator =
      rfh_to_io_thread_client_.find(rfh_id);
  if (iterator == rfh_to_io_thread_client_.end())
    return false;

  *client = iterator->second;
  return true;
}

void RfhToIoThreadClientMap::Erase(pair<int, int> rfh_id) {
  base::AutoLock lock(map_lock_);
  rfh_to_io_thread_client_.erase(rfh_id);
}

// ClientMapEntryUpdater ------------------------------------------------------

class ClientMapEntryUpdater : public content::WebContentsObserver {
 public:
  ClientMapEntryUpdater(WebContents* web_contents,
                        IInterface* jdelegate);

  virtual void RenderFrameCreated(RenderFrameHost* render_frame_host) OVERRIDE;
  virtual void RenderFrameDeleted(RenderFrameHost* render_frame_host) OVERRIDE;
  virtual void WebContentsDestroyed() OVERRIDE;

 private:
  ObjectWeakGlobalRef jdelegate_;
};

ClientMapEntryUpdater::ClientMapEntryUpdater(WebContents* web_contents,
                                             IInterface* jdelegate)
    : content::WebContentsObserver(web_contents),
      jdelegate_(jdelegate) {
  DCHECK(web_contents);
  DCHECK(jdelegate);

  if (web_contents->GetMainFrame())
    RenderFrameCreated(web_contents->GetMainFrame());
}

void ClientMapEntryUpdater::RenderFrameCreated(RenderFrameHost* rfh) {
  IoThreadClientData client_data;
  client_data.io_thread_client = jdelegate_;
  client_data.pending_association = false;
  RfhToIoThreadClientMap::GetInstance()->Set(
      GetRenderFrameHostIdPair(rfh), client_data);
}

void ClientMapEntryUpdater::RenderFrameDeleted(RenderFrameHost* rfh) {
  RfhToIoThreadClientMap::GetInstance()->Erase(GetRenderFrameHostIdPair(rfh));
}

void ClientMapEntryUpdater::WebContentsDestroyed() {
  delete this;
}

} // namespace

// AwContentsIoThreadClientImpl -----------------------------------------------

// static
scoped_ptr<AwContentsIoThreadClient>
AwContentsIoThreadClient::FromID(int render_process_id, int render_frame_id) {
  pair<int, int> rfh_id(render_process_id, render_frame_id);
  IoThreadClientData client_data;
  if (!RfhToIoThreadClientMap::GetInstance()->Get(rfh_id, &client_data))
    return scoped_ptr<AwContentsIoThreadClient>();

  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> java_delegate =
  Elastos::AutoPtr<IInterface> java_delegate =
      client_data.io_thread_client.get();
  DCHECK(!client_data.pending_association || !java_delegate.Get());
  return scoped_ptr<AwContentsIoThreadClient>(new AwContentsIoThreadClientImpl(
      client_data.pending_association, java_delegate));
}

// static
void AwContentsIoThreadClient::SubFrameCreated(int render_process_id,
                                               int parent_render_frame_id,
                                               int child_render_frame_id) {
  pair<int, int> parent_rfh_id(render_process_id, parent_render_frame_id);
  pair<int, int> child_rfh_id(render_process_id, child_render_frame_id);
  IoThreadClientData client_data;
  if (!RfhToIoThreadClientMap::GetInstance()->Get(parent_rfh_id,
                                                  &client_data)) {
    NOTREACHED();
    return;
  }

  RfhToIoThreadClientMap::GetInstance()->Set(child_rfh_id, client_data);
}

// static
void AwContentsIoThreadClientImpl::RegisterPendingContents(
    WebContents* web_contents) {
  IoThreadClientData client_data;
  client_data.pending_association = true;
  RfhToIoThreadClientMap::GetInstance()->Set(
      GetRenderFrameHostIdPair(web_contents->GetMainFrame()), client_data);
}

// static
void AwContentsIoThreadClientImpl::Associate(
    WebContents* web_contents,
    IInterface* jclient) {
  //JNIEnv* env = AttachCurrentThread();
  // The ClientMapEntryUpdater lifespan is tied to the WebContents.
  new ClientMapEntryUpdater(web_contents, jclient);
}

AwContentsIoThreadClientImpl::AwContentsIoThreadClientImpl(
    bool pending_association,
    IInterface* obj)
  : pending_association_(pending_association),
    java_object_(obj) {
}

AwContentsIoThreadClientImpl::~AwContentsIoThreadClientImpl() {
  // explict, out-of-line destructor.
}

bool AwContentsIoThreadClientImpl::PendingAssociation() const {
  return pending_association_;
}

AwContentsIoThreadClient::CacheMode
AwContentsIoThreadClientImpl::GetCacheMode() const {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));
  if (!java_object_.Get())
    return AwContentsIoThreadClient::LOAD_DEFAULT;

  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsIoThreadClientCallback);
  return static_cast<AwContentsIoThreadClient::CacheMode>(
      sElaAwContentsIoThreadClientCallback->elastos_AwContentsIoThreadClient_getCacheMode(
          java_object_.Get()));
}

scoped_ptr<AwWebResourceResponse>
AwContentsIoThreadClientImpl::ShouldInterceptRequest(
    const GURL& location,
    const net::URLRequest* request) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));
  if (!java_object_.Get())
    return scoped_ptr<AwWebResourceResponse>();
  const content::ResourceRequestInfo* info =
      content::ResourceRequestInfo::ForRequest(request);
  bool is_main_frame = info &&
      info->GetResourceType() == ResourceType::MAIN_FRAME;
  bool has_user_gesture = info && info->HasUserGesture();

  vector<string> headers_names;
  vector<string> headers_values;
  {
    net::HttpRequestHeaders headers;
    if (!request->GetFullRequestHeaders(&headers))
      headers = request->extra_request_headers();
    net::HttpRequestHeaders::Iterator headers_iterator(headers);
    while (headers_iterator.GetNext()) {
      headers_names.push_back(headers_iterator.name());
      headers_values.push_back(headers_iterator.value());
    }
  }

  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsIoThreadClientCallback);
  //ScopedJavaLocalRef<jstring> jstring_url =
  Elastos::String jstring_url =
      ConvertUTF8ToJavaString(location.spec());
  //ScopedJavaLocalRef<jstring> jstring_method =
  Elastos::String jstring_method =
      ConvertUTF8ToJavaString(request->method());
  //ScopedJavaLocalRef<jobjectArray> jstringArray_headers_names =
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::String> > jstringArray_headers_names =
      ToJavaArrayOfStrings(headers_names);
  //ScopedJavaLocalRef<jobjectArray> jstringArray_headers_values =
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::String> > jstringArray_headers_values =
      ToJavaArrayOfStrings(headers_values);
  devtools_instrumentation::ScopedEmbedderCallbackTask embedder_callback(
      "shouldInterceptRequest");
  //ScopedJavaLocalRef<jobject> ret =
  Elastos::AutoPtr<IInterface> ret =
      sElaAwContentsIoThreadClientCallback->elastos_AwContentsIoThreadClient_shouldInterceptRequest(
          java_object_.Get(),
          jstring_url,
          is_main_frame,
          has_user_gesture,
          jstring_method,
          jstringArray_headers_names.Get(),
          jstringArray_headers_values.Get());
  if (!ret.Get())
    return scoped_ptr<AwWebResourceResponse>();
  return scoped_ptr<AwWebResourceResponse>(
      new AwWebResourceResponseImpl(ret));
}

bool AwContentsIoThreadClientImpl::ShouldBlockContentUrls() const {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));
  if (!java_object_.Get())
    return false;

  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsIoThreadClientCallback);
  return sElaAwContentsIoThreadClientCallback->elastos_AwContentsIoThreadClient_shouldBlockContentUrls(
      java_object_.Get());
}

bool AwContentsIoThreadClientImpl::ShouldBlockFileUrls() const {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));
  if (!java_object_.Get())
    return false;

  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsIoThreadClientCallback);
  return sElaAwContentsIoThreadClientCallback->elastos_AwContentsIoThreadClient_shouldBlockFileUrls(
      java_object_.Get());
}

bool AwContentsIoThreadClientImpl::ShouldAcceptThirdPartyCookies() const {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));
  if (!java_object_.Get())
    return false;

  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsIoThreadClientCallback);
  return sElaAwContentsIoThreadClientCallback->elastos_AwContentsIoThreadClient_shouldAcceptThirdPartyCookies(
      java_object_.Get());
}

bool AwContentsIoThreadClientImpl::ShouldBlockNetworkLoads() const {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));
  if (!java_object_.Get())
    return false;

  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsIoThreadClientCallback);
  return sElaAwContentsIoThreadClientCallback->elastos_AwContentsIoThreadClient_shouldBlockNetworkLoads(
      java_object_.Get());
}

void AwContentsIoThreadClientImpl::NewDownload(
    const GURL& url,
    const string& user_agent,
    const string& content_disposition,
    const string& mime_type,
    int64 content_length) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));
  if (!java_object_.Get())
    return;

  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsIoThreadClientCallback);
  //ScopedJavaLocalRef<jstring> jstring_url =
  Elastos::String jstring_url =
      ConvertUTF8ToJavaString(url.spec());
  //ScopedJavaLocalRef<jstring> jstring_user_agent =
  Elastos::String jstring_user_agent =
      ConvertUTF8ToJavaString(user_agent);
  //ScopedJavaLocalRef<jstring> jstring_content_disposition =
  Elastos::String jstring_content_disposition =
      ConvertUTF8ToJavaString(content_disposition);
  //ScopedJavaLocalRef<jstring> jstring_mime_type =
  Elastos::String jstring_mime_type =
      ConvertUTF8ToJavaString(mime_type);

  sElaAwContentsIoThreadClientCallback->elastos_AwContentsIoThreadClient_onDownloadStart(
      java_object_.Get(),
      jstring_url,
      jstring_user_agent,
      jstring_content_disposition,
      jstring_mime_type,
      content_length);
}

void AwContentsIoThreadClientImpl::NewLoginRequest(const string& realm,
                                                   const string& account,
                                                   const string& args) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));
  if (!java_object_.Get())
    return;

  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsIoThreadClientCallback);
  //ScopedJavaLocalRef<jstring> jrealm = ConvertUTF8ToJavaString(env, realm);
  Elastos::String jrealm = ConvertUTF8ToJavaString(realm);
  //ScopedJavaLocalRef<jstring> jargs = ConvertUTF8ToJavaString(env, args);
  Elastos::String jargs = ConvertUTF8ToJavaString(args);

  //ScopedJavaLocalRef<jstring> jaccount;
  Elastos::String jaccount;
  if (!account.empty())
    jaccount = ConvertUTF8ToJavaString(account);

  sElaAwContentsIoThreadClientCallback->elastos_AwContentsIoThreadClient_newLoginRequest(
      java_object_.Get(), jrealm, jaccount, jargs);
}

bool RegisterAwContentsIoThreadClientImpl() {
  return RegisterNativesImpl();
}

} // namespace android_webview
