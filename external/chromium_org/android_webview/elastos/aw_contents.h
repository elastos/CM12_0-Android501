// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef ANDROID_WEBVIEW_NATIVE_AW_CONTENTS_H_
#define ANDROID_WEBVIEW_NATIVE_AW_CONTENTS_H_

//#include <jni.h>
#include <list>
#include <string>
#include <utility>

#include "android_webview/browser/aw_browser_permission_request_delegate.h"
#include "android_webview/browser/browser_view_renderer.h"
#include "android_webview/browser/browser_view_renderer_client.h"
#include "android_webview/browser/find_helper.h"
#include "android_webview/browser/gl_view_renderer_manager.h"
#include "android_webview/browser/icon_helper.h"
#include "android_webview/browser/renderer_host/aw_render_view_host_ext.h"
#include "android_webview/browser/shared_renderer_state.h"
#include "android_webview/elastos/permission/permission_request_handler_client.h"
#include "base/elastos/api_weak_ref.h"
//#include "base/android/scoped_java_ref.h"
#include "base/callback_forward.h"
#include "base/memory/scoped_ptr.h"

class SkBitmap;
class TabContents;
struct AwDrawGLInfo;

namespace content {
class WebContents;
}

namespace android_webview {

class AwContentsContainer;
class AwContentsClientBridge;
class AwPdfExporter;
class AwWebContentsDelegate;
class HardwareRenderer;
class PermissionRequestHandler;

// Native side of java-class of same name.
// Provides the ownership of and access to browser components required for
// WebView functionality; analogous to chrome's TabContents, but with a
// level of indirection provided by the AwContentsContainer abstraction.
//
// Object lifetime:
// For most purposes the java and native objects can be considered to have
// 1:1 lifetime and relationship. The exception is the java instance that
// hosts a popup will be rebound to a second native instance (carrying the
// popup content) and discard the 'default' native instance it made on
// construction. A native instance is only bound to at most one Java peer over
// its entire lifetime - see Init() and SetPendingWebContentsForPopup() for the
// construction points, and SetJavaPeers() where these paths join.
class AwContents : public FindHelper::Listener,
                   public IconHelper::Listener,
                   public AwRenderViewHostExtClient,
                   public BrowserViewRendererClient,
                   public PermissionRequestHandlerClient,
                   public AwBrowserPermissionRequestDelegate {
 public:
  // Returns the AwContents instance associated with |web_contents|, or NULL.
  static AwContents* FromWebContents(content::WebContents* web_contents);

  // Returns the AwContents instance associated with with the given
  // render_process_id and render_view_id, or NULL.
  static AwContents* FromID(int render_process_id, int render_view_id);

  AwContents(scoped_ptr<content::WebContents> web_contents);
  virtual ~AwContents();

  AwRenderViewHostExt* render_view_host_ext() {
    return render_view_host_ext_.get();
  }

  // |handler| is an instance of
  // org.chromium.android_webview.AwHttpAuthHandler.
  bool OnReceivedHttpAuthRequest(IInterface* handler,
                                 const std::string& host,
                                 const std::string& realm);

  // Methods called from Java.
  void SetJavaPeers(IInterface* obj,
                    IInterface* aw_contents,
                    IInterface* web_contents_delegate,
                    IInterface* contents_client_bridge,
                    IInterface* io_thread_client,
                    IInterface* intercept_navigation_delegate);
  Elastos::Handle64 GetWebContents(IInterface* obj);

  void Destroy();
  void DocumentHasImages(IInterface* obj, IInterface* message);
  void GenerateMHTML(IInterface* obj, const Elastos::String& jpath, IInterface* callback);
  void CreatePdfExporter(IInterface* obj, IInterface* pdfExporter);
  void AddVisitedLinks(IInterface* obj, Elastos::ArrayOf<Elastos::String>* jvisited_links);
  //base::android::ScopedJavaLocalRef<jbyteArray> GetCertificate(
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > GetCertificate(
      IInterface* obj);
  void RequestNewHitTestDataAt(IInterface* obj, Elastos::Int32 x, Elastos::Int32 y);
  void UpdateLastHitTestData(IInterface* obj);
  void OnSizeChanged(IInterface* obj, int w, int h, int ow, int oh);
  void SetViewVisibility(IInterface* obj, bool visible);
  void SetWindowVisibility(IInterface* obj, bool visible);
  void SetIsPaused(IInterface* obj, bool paused);
  void OnAttachedToWindow(IInterface* obj, int w, int h);
  void OnDetachedFromWindow();
  //base::android::ScopedJavaLocalRef<jbyteArray> GetOpaqueState(
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > GetOpaqueState(
      IInterface* obj);
  Elastos::Boolean RestoreFromOpaqueState(IInterface* obj, Elastos::ArrayOf<Elastos::Byte>* state);
  void FocusFirstNode(IInterface* obj);
  void SetBackgroundColor(IInterface* obj, Elastos::Int32 color);
  bool OnDraw(IInterface* obj,
              IInterface* canvas,
              Elastos::Boolean is_hardware_accelerated,
              Elastos::Int32 scroll_x,
              Elastos::Int32 scroll_y,
              Elastos::Int32 visible_left,
              Elastos::Int32 visible_top,
              Elastos::Int32 visible_right,
              Elastos::Int32 visible_bottom);
  Elastos::Handle64 GetAwDrawGLViewContext(IInterface* obj);
  Elastos::Handle64 CapturePicture(IInterface* obj, int width, int height);
  void EnableOnNewPicture(IInterface* obj, Elastos::Boolean enabled);
  void ClearView(IInterface* obj);
  void SetExtraHeadersForUrl(IInterface* obj,
                             const Elastos::String& url, const Elastos::String& extra_headers);

  void DrawGL(AwDrawGLInfo* draw_info);

  void InvokeGeolocationCallback(IInterface* obj,
                                 Elastos::Boolean value,
                                 const Elastos::String& origin);

  // PermissionRequestHandlerClient implementation.
  virtual void OnPermissionRequest(AwPermissionRequest* request) OVERRIDE;
  virtual void OnPermissionRequestCanceled(AwPermissionRequest* request) OVERRIDE;

  PermissionRequestHandler* GetPermissionRequestHandler() {
    return permission_request_handler_.get();
  }

  void PreauthorizePermission(IInterface* obj,
                              const Elastos::String& origin,
                              Elastos::Int64 resources);

  // AwBrowserPermissionRequestDelegate implementation.
  virtual void RequestProtectedMediaIdentifierPermission(
      const GURL& origin,
      const base::Callback<void(bool)>& callback) OVERRIDE;
  virtual void CancelProtectedMediaIdentifierPermissionRequests(
      const GURL& origin) OVERRIDE;
  virtual void RequestGeolocationPermission(
      const GURL& origin,
      const base::Callback<void(bool)>& callback) OVERRIDE;
  virtual void CancelGeolocationPermissionRequests(
      const GURL& origin) OVERRIDE;


  // Find-in-page API and related methods.
  void FindAllAsync(IInterface* obj, const Elastos::String& search_string);
  void FindNext(IInterface* obj, Elastos::Boolean forward);
  void ClearMatches(IInterface* obj);
  FindHelper* GetFindHelper();

  // Per WebView Cookie Policy
  bool AllowThirdPartyCookies();

  // FindHelper::Listener implementation.
  virtual void OnFindResultReceived(int active_ordinal,
                                    int match_count,
                                    bool finished) OVERRIDE;
  // IconHelper::Listener implementation.
  virtual bool ShouldDownloadFavicon(const GURL& icon_url) OVERRIDE;
  virtual void OnReceivedIcon(const GURL& icon_url,
                              const SkBitmap& bitmap) OVERRIDE;
  virtual void OnReceivedTouchIconUrl(const std::string& url,
                                      const bool precomposed) OVERRIDE;

  // AwRenderViewHostExtClient implementation.
  virtual void OnWebLayoutPageScaleFactorChanged(
      float page_scale_factor) OVERRIDE;
  virtual void OnWebLayoutContentsSizeChanged(
      const gfx::Size& contents_size) OVERRIDE;

  // BrowserViewRendererClient implementation.
  virtual bool RequestDrawGL(IInterface* canvas, bool wait_for_completion) OVERRIDE;
  virtual void PostInvalidate() OVERRIDE;
  virtual void UpdateParentDrawConstraints() OVERRIDE;
  virtual void OnNewPicture() OVERRIDE;
  virtual gfx::Point GetLocationOnScreen() OVERRIDE;
  virtual void ScrollContainerViewTo(gfx::Vector2d new_value) OVERRIDE;
  virtual bool IsFlingActive() const OVERRIDE;
  virtual void UpdateScrollState(gfx::Vector2d max_scroll_offset,
                                 gfx::SizeF contents_size_dip,
                                 float page_scale_factor,
                                 float min_page_scale_factor,
                                 float max_page_scale_factor) OVERRIDE;
  virtual void DidOverscroll(gfx::Vector2d overscroll_delta) OVERRIDE;

  const BrowserViewRenderer* GetBrowserViewRenderer() const;

  void ClearCache(IInterface* obj, Elastos::Boolean include_disk_files);
  void SetPendingWebContentsForPopup(scoped_ptr<content::WebContents> pending);
  Elastos::Handle64 ReleasePopupAwContents(IInterface* obj);

  void ScrollTo(IInterface* obj, Elastos::Int32 x, Elastos::Int32 y);
  void SetDipScale(IInterface* obj, Elastos::Float dip_scale);
  void SetSaveFormData(bool enabled);

  // Sets the java client
  void SetAwAutofillClient(IInterface* client);

  void SetJsOnlineProperty(IInterface* obj, Elastos::Boolean network_up);
  void TrimMemory(IInterface* obj, Elastos::Int32 level, Elastos::Boolean visible);

 private:
  void InitDataReductionProxyIfNecessary();
  void InitAutofillIfNecessary(bool enabled);

  void InitializeHardwareDrawIfNeeded();
  void ReleaseHardwareDrawIfNeeded();

  // Geolocation API support
  void ShowGeolocationPrompt(const GURL& origin, base::Callback<void(bool)>);
  void HideGeolocationPrompt(const GURL& origin);

  ObjectWeakGlobalRef java_ref_;
  scoped_ptr<content::WebContents> web_contents_;
  scoped_ptr<AwWebContentsDelegate> web_contents_delegate_;
  scoped_ptr<AwContentsClientBridge> contents_client_bridge_;
  scoped_ptr<AwRenderViewHostExt> render_view_host_ext_;
  scoped_ptr<FindHelper> find_helper_;
  scoped_ptr<IconHelper> icon_helper_;
  scoped_ptr<AwContents> pending_contents_;
  SharedRendererState shared_renderer_state_;
  BrowserViewRenderer browser_view_renderer_;
  scoped_ptr<HardwareRenderer> hardware_renderer_;
  scoped_ptr<AwPdfExporter> pdf_exporter_;
  scoped_ptr<PermissionRequestHandler> permission_request_handler_;

  // GURL is supplied by the content layer as requesting frame.
  // Callback is supplied by the content layer, and is invoked with the result
  // from the permission prompt.
  typedef std::pair<const GURL, base::Callback<void(bool)> > OriginCallback;
  // The first element in the list is always the currently pending request.
  std::list<OriginCallback> pending_geolocation_prompts_;

  base::Lock render_thread_lock_;
  GLViewRendererManager::Key renderer_manager_key_;

  DISALLOW_COPY_AND_ASSIGN(AwContents);
};

bool RegisterAwContents();

}  // namespace android_webview

#endif  // ANDROID_WEBVIEW_NATIVE_AW_CONTENTS_H_
