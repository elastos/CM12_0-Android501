// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/aw_picture.h"

#include "android_webview/elastos/java_browser_view_renderer_helper.h"
#include "base/bind.h"
#include "android_webview/api/AwPicture_api.h"
#include "third_party/skia/include/core/SkPicture.h"

namespace android_webview {

AwPicture::AwPicture(skia::RefPtr<SkPicture> picture)
    : picture_(picture) {
  DCHECK(picture_);
}

AwPicture::~AwPicture() {}

void AwPicture::Destroy() {
  delete this;
}

Elastos::Int32 AwPicture::GetWidth(IInterface* obj) {
  return picture_->width();
}

Elastos::Int32 AwPicture::GetHeight(IInterface* obj) {
  return picture_->height();
}

namespace {
bool RenderPictureToCanvas(SkPicture* picture, SkCanvas* canvas) {
  picture->draw(canvas);
  return true;
}
}

void AwPicture::Draw(IInterface* obj, IInterface* canvas) {
  bool ok = JavaBrowserViewRendererHelper::GetInstance()
                ->RenderViaAuxilaryBitmapIfNeeded(
                    canvas,
                    gfx::Vector2d(),
                    gfx::Size(picture_->width(), picture_->height()),
                    base::Bind(&RenderPictureToCanvas,
                               base::Unretained(picture_.get())));
  LOG_IF(ERROR, !ok) << "Couldn't draw picture";
}

bool RegisterAwPicture() {
  return RegisterNativesImpl();
}

}  // namespace android_webview
