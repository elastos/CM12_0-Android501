// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/permission/aw_permission_request.h"

#include "android_webview/elastos/permission/aw_permission_request_delegate.h"
#include "base/elastos/jni_string.h"
#include "android_webview/api/AwPermissionRequest_api.h"

//using base::android::AttachCurrentThread;
using base::android::ConvertUTF8ToJavaString;
//using base::android::ScopedJavaGlobalRef;
//using base::android::ScopedJavaLocalRef;

namespace android_webview {

AwPermissionRequest::AwPermissionRequest(
    scoped_ptr<AwPermissionRequestDelegate> delegate)
    : delegate_(delegate.Pass()),
      weak_factory_(this) {
  DCHECK(delegate_.get());
}

AwPermissionRequest::~AwPermissionRequest() {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwPermissionRequestCallback);
  //ScopedJavaLocalRef<jobject> j_object = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_object = java_ref_.get();
  if (!j_object.Get())
    return;
  //Java_AwPermissionRequest_detachNativeInstance(env, j_object.obj());
  sElaAwPermissionRequestCallback->elastos_AwPermissionRequest_detachNativeInstance(j_object.Get());
}

void AwPermissionRequest::OnAccept(IInterface* jcaller,
                                   Elastos::Boolean accept) {
  delegate_->NotifyRequestResult(accept);
  delete this;
}

Elastos::AutoPtr<IInterface> AwPermissionRequest::CreateJavaPeer() {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwPermissionRequestCallback);
  //ScopedJavaLocalRef<jobject> j_object = Java_AwPermissionRequest_create(
  Elastos::AutoPtr<IInterface> j_object = sElaAwPermissionRequestCallback->elastos_AwPermissionRequest_create(
      reinterpret_cast<Elastos::Int64>(this),
      ConvertUTF8ToJavaString(GetOrigin().spec()), GetResources());
  java_ref_ = ObjectWeakGlobalRef(j_object.Get());
  return j_object;
}

Elastos::AutoPtr<IInterface> AwPermissionRequest::GetJavaObject() {
  return java_ref_.get();
}

const GURL& AwPermissionRequest::GetOrigin() {
  return delegate_->GetOrigin();
}

int64 AwPermissionRequest::GetResources() {
  return delegate_->GetResources();
}

bool RegisterAwPermissionRequest() {
  return RegisterNativesImpl();
}

}  // namespace android_webivew
