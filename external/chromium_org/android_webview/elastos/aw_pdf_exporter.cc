// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/aw_pdf_exporter.h"

#include "android_webview/browser/renderer_host/print_manager.h"
#include "base/elastos/api_elastos.h"
#include "base/logging.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/web_contents.h"
#include "android_webview/api/AwPdfExporter_api.h"
#include "printing/print_settings.h"
#include "printing/units.h"

//using base::android::AttachCurrentThread;
//using base::android::ScopedJavaGlobalRef;
using content::BrowserThread;
using content::WebContents;
using printing::ConvertUnitDouble;
using printing::PageMargins;
using printing::PrintSettings;

namespace android_webview {

AwPdfExporter::AwPdfExporter(IInterface* obj,
                             WebContents* web_contents)
    : java_ref_(obj),
      web_contents_(web_contents) {
  DCHECK(obj);
  DCHECK(sElaAwPdfExporterCallback);
  sElaAwPdfExporterCallback->elastos_AwPdfExporter_setNativeAwPdfExporter(
      obj, reinterpret_cast<intptr_t>(this));
}

AwPdfExporter::~AwPdfExporter() {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwPdfExporterCallback);
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  // Clear the Java peer's weak pointer to |this| object.
  sElaAwPdfExporterCallback->elastos_AwPdfExporter_setNativeAwPdfExporter(obj.Get(), 0);
}

void AwPdfExporter::ExportToPdf(IInterface* obj,
                                int fd,
                                IInterface* cancel_signal) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  CreatePdfSettings(obj);
  print_manager_.reset(
      new PrintManager(web_contents_, print_settings_.get(), fd, this));
  if (!print_manager_->PrintNow())
    DidExportPdf(false);
}

namespace {
// Converts from 1/1000 of inches to device units using DPI.
int MilsToDots(int val, int dpi) {
  return static_cast<int>(ConvertUnitDouble(val, 1000.0, dpi));
}

}  // anonymous namespace

void AwPdfExporter::CreatePdfSettings(IInterface* obj) {
  print_settings_.reset(new PrintSettings);
  int dpi = sElaAwPdfExporterCallback->elastos_AwPdfExporter_getDpi(obj);
  int width = sElaAwPdfExporterCallback->elastos_AwPdfExporter_getPageWidth(obj);
  int height = sElaAwPdfExporterCallback->elastos_AwPdfExporter_getPageHeight(obj);
  gfx::Size physical_size_device_units;
  int width_in_dots = MilsToDots(width, dpi);
  int height_in_dots = MilsToDots(height, dpi);
  physical_size_device_units.SetSize(width_in_dots, height_in_dots);

  gfx::Rect printable_area_device_units;
  // Assume full page is printable for now.
  printable_area_device_units.SetRect(0, 0, width_in_dots, height_in_dots);

  print_settings_->set_dpi(dpi);
  // TODO(sgurun) verify that the value for newly added parameter for
  // (i.e. landscape_needs_flip) is correct.
  print_settings_->SetPrinterPrintableArea(physical_size_device_units,
                                           printable_area_device_units,
                                           true);

  PageMargins margins;
  margins.left =
      MilsToDots(sElaAwPdfExporterCallback->elastos_AwPdfExporter_getLeftMargin(obj), dpi);
  margins.right =
      MilsToDots(sElaAwPdfExporterCallback->elastos_AwPdfExporter_getRightMargin(obj), dpi);
  margins.top =
      MilsToDots(sElaAwPdfExporterCallback->elastos_AwPdfExporter_getTopMargin(obj), dpi);
  margins.bottom =
      MilsToDots(sElaAwPdfExporterCallback->elastos_AwPdfExporter_getBottomMargin(obj), dpi);
  print_settings_->SetCustomMargins(margins);
  print_settings_->set_should_print_backgrounds(true);
}

void AwPdfExporter::DidExportPdf(bool success) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwPdfExporterCallback);
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  sElaAwPdfExporterCallback->elastos_AwPdfExporter_didExportPdf(obj.Get(), success);
}

bool AwPdfExporter::IsCancelled() {
  // TODO(sgurun) implement. Needs connecting with the |cancel_signal| passed
  // in the constructor.
  return false;
}

bool RegisterAwPdfExporter() {
  return RegisterNativesImpl();
}

}  // namespace android_webview
