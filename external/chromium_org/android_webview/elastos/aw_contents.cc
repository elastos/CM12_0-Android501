// Copyright 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/aw_contents.h"

#include <limits>

#include "android_webview/browser/aw_browser_context.h"
#include "android_webview/browser/aw_browser_main_parts.h"
#include "android_webview/browser/aw_resource_context.h"
#include "android_webview/browser/browser_view_renderer.h"
#include "android_webview/browser/deferred_gpu_command_service.h"
#include "android_webview/browser/gpu_memory_buffer_factory_impl.h"
#include "android_webview/browser/hardware_renderer.h"
#include "android_webview/browser/net_disk_cache_remover.h"
#include "android_webview/browser/renderer_host/aw_resource_dispatcher_host_delegate.h"
#include "android_webview/browser/scoped_app_gl_state_restore.h"
#include "android_webview/common/aw_hit_test_data.h"
#include "android_webview/common/devtools_instrumentation.h"
#include "android_webview/elastos/aw_autofill_client.h"
#include "android_webview/elastos/aw_browser_dependency_factory.h"
#include "android_webview/elastos/aw_contents_client_bridge.h"
#include "android_webview/elastos/aw_contents_io_thread_client_impl.h"
#include "android_webview/elastos/aw_pdf_exporter.h"
#include "android_webview/elastos/aw_picture.h"
#include "android_webview/elastos/aw_web_contents_delegate.h"
#include "android_webview/elastos/java_browser_view_renderer_helper.h"
#include "android_webview/elastos/permission/aw_permission_request.h"
#include "android_webview/elastos/permission/permission_request_handler.h"
#include "android_webview/elastos/permission/simple_permission_request.h"
#include "android_webview/elastos/state_serializer.h"
#include "android_webview/public/browser/draw_gl.h"
#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_array.h"
#include "base/elastos/jni_string.h"
//#include "base/android/scoped_java_ref.h"
#include "base/atomicops.h"
#include "base/bind.h"
#include "base/callback.h"
#include "base/memory/memory_pressure_listener.h"
#include "base/message_loop/message_loop.h"
#include "base/pickle.h"
#include "base/strings/string16.h"
#include "base/supports_user_data.h"
#include "components/autofill/content/browser/content_autofill_driver.h"
#include "components/autofill/core/browser/autofill_manager.h"
#include "components/autofill/core/browser/webdata/autofill_webdata_service.h"
#include "components/data_reduction_proxy/browser/data_reduction_proxy_settings.h"
#include "components/navigation_interception_elastos/intercept_navigation_delegate.h"
#include "content/public/browser/elastos/content_view_core.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/cert_store.h"
#include "content/public/browser/favicon_status.h"
#include "content/public/browser/navigation_entry.h"
#include "content/public/browser/render_frame_host.h"
#include "content/public/browser/render_process_host.h"
#include "content/public/browser/render_view_host.h"
#include "content/public/browser/web_contents.h"
#include "content/public/common/renderer_preferences.h"
#include "content/public/common/ssl_status.h"
#include "android_webview/api/AwContents_api.h"
#include "net/base/auth.h"
#include "net/cert/x509_certificate.h"
#include "third_party/skia/include/core/SkPicture.h"
#include "ui/base/l10n/l10n_util_elastos.h"
#include "ui/gfx/elastos/java_bitmap.h"
#include "ui/gfx/image/image.h"
#include "ui/gfx/size.h"

struct AwDrawSWFunctionTable;
struct AwDrawGLFunctionTable;

using autofill::ContentAutofillDriver;
using autofill::AutofillManager;
//using base::android::AttachCurrentThread;
using base::android::ConvertJavaStringToUTF16;
using base::android::ConvertJavaStringToUTF8;
using base::android::ConvertUTF16ToJavaString;
using base::android::ConvertUTF8ToJavaString;
//using base::android::JavaRef;
//using base::android::ScopedJavaGlobalRef;
//using base::android::ScopedJavaLocalRef;
using data_reduction_proxy::DataReductionProxySettings;
using navigation_interception::InterceptNavigationDelegate;
using content::BrowserThread;
using content::ContentViewCore;
using content::WebContents;

extern "C" {
static AwDrawGLFunction DrawGLFunction;
static void DrawGLFunction(long view_context,
                           AwDrawGLInfo* draw_info,
                           void* spare) {
  // |view_context| is the value that was returned from the java
  // AwContents.onPrepareDrawGL; this cast must match the code there.
  reinterpret_cast<android_webview::AwContents*>(view_context)
      ->DrawGL(draw_info);
}
}

namespace android_webview {

namespace {

bool g_should_download_favicons = false;

const void* kAwContentsUserDataKey = &kAwContentsUserDataKey;

class AwContentsUserData : public base::SupportsUserData::Data {
 public:
  AwContentsUserData(AwContents* ptr) : contents_(ptr) {}

  static AwContents* GetContents(WebContents* web_contents) {
    if (!web_contents)
      return NULL;
    AwContentsUserData* data = reinterpret_cast<AwContentsUserData*>(
        web_contents->GetUserData(kAwContentsUserDataKey));
    return data ? data->contents_ : NULL;
  }

 private:
  AwContents* contents_;
};

base::subtle::Atomic32 g_instance_count = 0;

void OnIoThreadClientReady(content::RenderFrameHost* rfh) {
  int render_process_id = rfh->GetProcess()->GetID();
  int render_frame_id = rfh->GetRoutingID();
  AwResourceDispatcherHostDelegate::OnIoThreadClientReady(
      render_process_id, render_frame_id);
}

}  // namespace

// static
AwContents* AwContents::FromWebContents(WebContents* web_contents) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  return AwContentsUserData::GetContents(web_contents);
}

// static
AwContents* AwContents::FromID(int render_process_id, int render_view_id) {
  const content::RenderViewHost* rvh =
      content::RenderViewHost::FromID(render_process_id, render_view_id);
  if (!rvh) return NULL;
  content::WebContents* web_contents =
      content::WebContents::FromRenderViewHost(rvh);
  if (!web_contents) return NULL;
  return FromWebContents(web_contents);
}

// static
AwBrowserPermissionRequestDelegate* AwBrowserPermissionRequestDelegate::FromID(
    int render_process_id, int render_view_id) {
  AwContents* aw_contents = AwContents::FromID(render_process_id,
                                               render_view_id);
  return implicit_cast<AwBrowserPermissionRequestDelegate*>(aw_contents);
}

AwContents::AwContents(scoped_ptr<WebContents> web_contents)
    : web_contents_(web_contents.Pass()),
      shared_renderer_state_(
          BrowserThread::GetMessageLoopProxyForThread(BrowserThread::UI),
          this),
      browser_view_renderer_(
          this,
          &shared_renderer_state_,
          web_contents_.get(),
          BrowserThread::GetMessageLoopProxyForThread(BrowserThread::UI)),
      renderer_manager_key_(GLViewRendererManager::GetInstance()->NullKey()) {
  base::subtle::NoBarrier_AtomicIncrement(&g_instance_count, 1);
  icon_helper_.reset(new IconHelper(web_contents_.get()));
  icon_helper_->SetListener(this);
  web_contents_->SetUserData(kAwContentsUserDataKey,
                             new AwContentsUserData(this));
  render_view_host_ext_.reset(
      new AwRenderViewHostExt(this, web_contents_.get()));

  permission_request_handler_.reset(
      new PermissionRequestHandler(this, web_contents_.get()));

  AwAutofillClient* autofill_manager_delegate =
      AwAutofillClient::FromWebContents(web_contents_.get());
  InitDataReductionProxyIfNecessary();
  if (autofill_manager_delegate)
    InitAutofillIfNecessary(autofill_manager_delegate->GetSaveFormData());
}

void AwContents::SetJavaPeers(IInterface* obj,
                              IInterface* aw_contents,
                              IInterface* web_contents_delegate,
                              IInterface* contents_client_bridge,
                              IInterface* io_thread_client,
                              IInterface* intercept_navigation_delegate) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  // The |aw_content| param is technically spurious as it duplicates |obj| but
  // is passed over anyway to make the binding more explicit.
  java_ref_ = ObjectWeakGlobalRef(aw_contents);

  web_contents_delegate_.reset(
      new AwWebContentsDelegate(web_contents_delegate));
  web_contents_->SetDelegate(web_contents_delegate_.get());

  contents_client_bridge_.reset(
      new AwContentsClientBridge(contents_client_bridge));
  AwContentsClientBridgeBase::Associate(web_contents_.get(),
                                        contents_client_bridge_.get());

  AwContentsIoThreadClientImpl::Associate(
      web_contents_.get(), io_thread_client);

  InterceptNavigationDelegate::Associate(
      web_contents_.get(),
      make_scoped_ptr(new InterceptNavigationDelegate(
          (intercept_navigation_delegate))));

  // Finally, having setup the associations, release any deferred requests
  web_contents_->ForEachFrame(base::Bind(&OnIoThreadClientReady));
}

void AwContents::SetSaveFormData(bool enabled) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  InitAutofillIfNecessary(enabled);
  // We need to check for the existence, since autofill_manager_delegate
  // may not be created when the setting is false.
  if (ContentAutofillDriver::FromWebContents(web_contents_.get())) {
    AwAutofillClient::FromWebContents(web_contents_.get())->
        SetSaveFormData(enabled);
  }
}

void AwContents::InitDataReductionProxyIfNecessary() {
  AwBrowserContext* browser_context =
      AwBrowserContext::FromWebContents(web_contents_.get());
  browser_context->CreateUserPrefServiceIfNecessary();
}

void AwContents::InitAutofillIfNecessary(bool enabled) {
  // Do not initialize if the feature is not enabled.
  if (!enabled)
    return;
  // Check if the autofill driver already exists.
  content::WebContents* web_contents = web_contents_.get();
  if (ContentAutofillDriver::FromWebContents(web_contents))
    return;

  AwBrowserContext::FromWebContents(web_contents)->
      CreateUserPrefServiceIfNecessary();
  AwAutofillClient::CreateForWebContents(web_contents);
  ContentAutofillDriver::CreateForWebContentsAndDelegate(
      web_contents,
      AwAutofillClient::FromWebContents(web_contents),
      l10n_util::GetDefaultLocale(),
      AutofillManager::DISABLE_AUTOFILL_DOWNLOAD_MANAGER);
}

void AwContents::SetAwAutofillClient(IInterface* client) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  sElaAwContentsCallback->elastos_AwContents_setAwAutofillClient(obj.Get(), client);
}

AwContents::~AwContents() {
  DCHECK(AwContents::FromWebContents(web_contents_.get()) == this);
  DCHECK(!hardware_renderer_.get());
  web_contents_->RemoveUserData(kAwContentsUserDataKey);
  if (find_helper_.get())
    find_helper_->SetListener(NULL);
  if (icon_helper_.get())
    icon_helper_->SetListener(NULL);
  base::subtle::NoBarrier_AtomicIncrement(&g_instance_count, -1);
  // When the last WebView is destroyed free all discardable memory allocated by
  // Chromium, because the app process may continue to run for a long time
  // without ever using another WebView.
  if (base::subtle::NoBarrier_Load(&g_instance_count) == 0) {
    base::MemoryPressureListener::NotifyMemoryPressure(
        base::MemoryPressureListener::MEMORY_PRESSURE_CRITICAL);
  }
}

Elastos::Handle64 AwContents::GetWebContents(IInterface* obj) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  DCHECK(web_contents_);
  return reinterpret_cast<intptr_t>(web_contents_.get());
}

void AwContents::Destroy() {
  java_ref_.reset();

  // We clear the contents_client_bridge_ here so that we break the link with
  // the java peer. This is important for the popup window case, where we are
  // swapping AwContents out that share the same java AwContentsClientBridge.
  // See b/15074651.
  AwContentsClientBridgeBase::Disassociate(web_contents_.get());
  contents_client_bridge_.reset();

  // We do not delete AwContents immediately. Some applications try to delete
  // Webview in ShouldOverrideUrlLoading callback, which is a sync IPC from
  // Webkit.
  BrowserThread::DeleteSoon(BrowserThread::UI, FROM_HERE, this);
}

static Elastos::Handle64 Init(IInterface* browser_context) {
  // TODO(joth): Use |browser_context| to get the native BrowserContext, rather
  // than hard-code the default instance lookup here.
  scoped_ptr<WebContents> web_contents(content::WebContents::Create(
      content::WebContents::CreateParams(AwBrowserContext::GetDefault())));
  // Return an 'uninitialized' instance; most work is deferred until the
  // subsequent SetJavaPeers() call.
  return reinterpret_cast<intptr_t>(new AwContents(web_contents.Pass()));
}

static void SetAwDrawSWFunctionTable(Elastos::Handle64 function_table) {
  JavaBrowserViewRendererHelper::SetAwDrawSWFunctionTable(
      reinterpret_cast<AwDrawSWFunctionTable*>(function_table));
}

static void SetAwDrawGLFunctionTable(Elastos::Handle64 function_table) {
  GpuMemoryBufferFactoryImpl::SetAwDrawGLFunctionTable(
      reinterpret_cast<AwDrawGLFunctionTable*>(function_table));
}

static Elastos::Handle64 GetAwDrawGLFunction() {
  return reinterpret_cast<intptr_t>(&DrawGLFunction);
}

// static
Elastos::Int32 GetNativeInstanceCount() {
  return base::subtle::NoBarrier_Load(&g_instance_count);
}

Elastos::Handle64 AwContents::GetAwDrawGLViewContext(IInterface* obj) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  return reinterpret_cast<intptr_t>(this);
}

void AwContents::DrawGL(AwDrawGLInfo* draw_info) {
  if (draw_info->mode == AwDrawGLInfo::kModeSync) {
    if (hardware_renderer_)
      hardware_renderer_->CommitFrame();
    return;
  }

  {
    GLViewRendererManager* manager = GLViewRendererManager::GetInstance();
    base::AutoLock lock(render_thread_lock_);
    if (renderer_manager_key_ != manager->NullKey()) {
      manager->DidDrawGL(renderer_manager_key_);
    }
  }

  ScopedAppGLStateRestore state_restore(
      draw_info->mode == AwDrawGLInfo::kModeDraw
          ? ScopedAppGLStateRestore::MODE_DRAW
          : ScopedAppGLStateRestore::MODE_RESOURCE_MANAGEMENT);
  ScopedAllowGL allow_gl;

  if (draw_info->mode == AwDrawGLInfo::kModeProcessNoContext) {
    LOG(ERROR) << "Received unexpected kModeProcessNoContext";
  }

  // kModeProcessNoContext should never happen because we tear down hardware
  // in onTrimMemory. However that guarantee is maintained outside of chromium
  // code. Not notifying shared state in kModeProcessNoContext can lead to
  // immediate deadlock, which is slightly more catastrophic than leaks or
  // corruption.
  if (draw_info->mode == AwDrawGLInfo::kModeProcess ||
      draw_info->mode == AwDrawGLInfo::kModeProcessNoContext) {
    shared_renderer_state_.DidDrawGLProcess();
  }

  if (shared_renderer_state_.IsInsideHardwareRelease()) {
    hardware_renderer_.reset();
    // Flush the idle queue in tear down.
    DeferredGpuCommandService::GetInstance()->PerformAllIdleWork();
    return;
  }

  if (draw_info->mode != AwDrawGLInfo::kModeDraw) {
    if (draw_info->mode == AwDrawGLInfo::kModeProcess) {
      DeferredGpuCommandService::GetInstance()->PerformIdleWork(true);
    }
    return;
  }

  if (!hardware_renderer_) {
    hardware_renderer_.reset(new HardwareRenderer(&shared_renderer_state_));
    hardware_renderer_->CommitFrame();
  }

  hardware_renderer_->DrawGL(state_restore.stencil_enabled(),
                             state_restore.framebuffer_binding_ext(),
                             draw_info);
  DeferredGpuCommandService::GetInstance()->PerformIdleWork(false);
}

namespace {
void DocumentHasImagesCallback(IInterface* message,
                               bool has_images) {
  DCHECK(sElaAwContentsCallback);
  sElaAwContentsCallback->elastos_AwContents_onDocumentHasImagesResponse(has_images,
                                              message);
}
}  // namespace

void AwContents::DocumentHasImages(IInterface* obj, IInterface* message) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //ScopedJavaGlobalRef<jobject> j_message;
  Elastos::AutoPtr<IInterface> j_message;
  j_message = message;
  render_view_host_ext_->DocumentHasImages(
      base::Bind(&DocumentHasImagesCallback, j_message));
}

namespace {
void GenerateMHTMLCallback(IInterface* callback,
                           const base::FilePath& path, int64 size) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsCallback);
  // Android files are UTF8, so the path conversion below is safe.
  sElaAwContentsCallback->elastos_AwContents_generateMHTMLCallback(
      ConvertUTF8ToJavaString(path.AsUTF8Unsafe()),
      size, callback);
}
}  // namespace

void AwContents::GenerateMHTML(IInterface* obj,
                               const Elastos::String& jpath, IInterface* callback) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //ScopedJavaGlobalRef<jobject>* j_callback = new ScopedJavaGlobalRef<jobject>();
  //j_callback->Reset(env, callback);
  Elastos::AutoPtr<IInterface> j_callback = callback;
  base::FilePath target_path(ConvertJavaStringToUTF8(jpath));
  //TODO if the parameter j_callback ok here???
  web_contents_->GenerateMHTML(
      target_path,
      base::Bind(&GenerateMHTMLCallback, j_callback.Get(), target_path));
      //base::Bind(&GenerateMHTMLCallback, base::Owned(j_callback), target_path));
}

void AwContents::CreatePdfExporter(IInterface* obj,
                                   IInterface* pdfExporter) {
  pdf_exporter_.reset(
      new AwPdfExporter(pdfExporter,
                        web_contents_.get()));
}

bool AwContents::OnReceivedHttpAuthRequest(IInterface* handler,
                                           const std::string& host,
                                           const std::string& realm) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return false;

  //ScopedJavaLocalRef<jstring> jhost = ConvertUTF8ToJavaString(env, host);
  Elastos::String jhost = ConvertUTF8ToJavaString(host);
  //ScopedJavaLocalRef<jstring> jrealm = ConvertUTF8ToJavaString(env, realm);
  Elastos::String jrealm = ConvertUTF8ToJavaString(realm);
  devtools_instrumentation::ScopedEmbedderCallbackTask embedder_callback(
      "onReceivedHttpAuthRequest");
  sElaAwContentsCallback->elastos_AwContents_onReceivedHttpAuthRequest(obj.Get(), handler,
      jhost, jrealm);
  return true;
}

void AwContents::AddVisitedLinks(IInterface* obj,
                                   Elastos::ArrayOf<Elastos::String>* jvisited_links) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  std::vector<base::string16> visited_link_strings;
  base::android::AppendJavaStringArrayToStringVector(
      jvisited_links, &visited_link_strings);

  std::vector<GURL> visited_link_gurls;
  std::vector<base::string16>::const_iterator itr;
  for (itr = visited_link_strings.begin(); itr != visited_link_strings.end();
       ++itr) {
    visited_link_gurls.push_back(GURL(*itr));
  }

  AwBrowserContext::FromWebContents(web_contents_.get())
      ->AddVisitedURLs(visited_link_gurls);
}

bool RegisterAwContents() {
  return RegisterNativesImpl();
}

namespace {

void ShowGeolocationPromptHelperTask(IInterface* java_ref,
                                     const GURL& origin) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsCallback);
  //ScopedJavaLocalRef<jobject> j_ref = java_ref.get(env);
  Elastos::AutoPtr<IInterface> j_ref = java_ref;
  if (j_ref.Get()) {
    //ScopedJavaLocalRef<jstring> j_origin(
    Elastos::String j_origin(
        ConvertUTF8ToJavaString(origin.spec()));
    devtools_instrumentation::ScopedEmbedderCallbackTask embedder_callback(
        "onGeolocationPermissionsShowPrompt");
    sElaAwContentsCallback->elastos_AwContents_onGeolocationPermissionsShowPrompt(
                                                       j_ref.Get(), j_origin);
  }
}

void ShowGeolocationPromptHelper(IInterface* java_ref,
                                 const GURL& origin) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsCallback);
  //if (java_ref.get(env).obj())
  if (java_ref) {
    content::BrowserThread::PostTask(
        content::BrowserThread::UI,
        FROM_HERE,
        base::Bind(&ShowGeolocationPromptHelperTask,
                   java_ref,
                   origin));
  }
}

} // anonymous namespace

void AwContents::ShowGeolocationPrompt(const GURL& requesting_frame,
                                       base::Callback<void(bool)> callback) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));

  GURL origin = requesting_frame.GetOrigin();
  bool show_prompt = pending_geolocation_prompts_.empty();
  pending_geolocation_prompts_.push_back(OriginCallback(origin, callback));
  if (show_prompt) {
    ShowGeolocationPromptHelper(java_ref_.get(), origin);
  }
}

// Invoked from Java
void AwContents::InvokeGeolocationCallback(IInterface* obj,
                                           Elastos::Boolean value,
                                           const Elastos::String& origin) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));

  GURL callback_origin(base::android::ConvertJavaStringToUTF16(origin));
  if (callback_origin.GetOrigin() ==
      pending_geolocation_prompts_.front().first) {
    pending_geolocation_prompts_.front().second.Run(value);
    pending_geolocation_prompts_.pop_front();
    if (!pending_geolocation_prompts_.empty()) {
      ShowGeolocationPromptHelper(java_ref_.get(),
                                  pending_geolocation_prompts_.front().first);
    }
  }
}

void AwContents::HideGeolocationPrompt(const GURL& origin) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  bool removed_current_outstanding_callback = false;
  std::list<OriginCallback>::iterator it = pending_geolocation_prompts_.begin();
  while (it != pending_geolocation_prompts_.end()) {
    if ((*it).first == origin.GetOrigin()) {
      if (it == pending_geolocation_prompts_.begin()) {
        removed_current_outstanding_callback = true;
      }
      it = pending_geolocation_prompts_.erase(it);
    } else {
      ++it;
    }
  }

  if (removed_current_outstanding_callback) {
    //JNIEnv* env = AttachCurrentThread();
    DCHECK(sElaAwContentsCallback);
    //ScopedJavaLocalRef<jobject> j_ref = java_ref_.get(env);
    Elastos::AutoPtr<IInterface> j_ref = java_ref_.get();
    if (j_ref.Get()) {
      devtools_instrumentation::ScopedEmbedderCallbackTask embedder_callback(
          "onGeolocationPermissionsHidePrompt");
      sElaAwContentsCallback->elastos_AwContents_onGeolocationPermissionsHidePrompt(j_ref.Get());
    }
    if (!pending_geolocation_prompts_.empty()) {
      ShowGeolocationPromptHelper(java_ref_.get(),
                            pending_geolocation_prompts_.front().first);
    }
  }
}

void AwContents::OnPermissionRequest(AwPermissionRequest* request) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsCallback);
  //ScopedJavaLocalRef<jobject> j_request = request->CreateJavaPeer();
  Elastos::AutoPtr<IInterface> j_request = request->CreateJavaPeer();
  //ScopedJavaLocalRef<jobject> j_ref = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_ref = java_ref_.get();
  //if (j_request.is_null() || j_ref.is_null())
  if (!j_request.Get() || !j_ref.Get()) {
    permission_request_handler_->CancelRequest(
        request->GetOrigin(), request->GetResources());
    return;
  }

  sElaAwContentsCallback->elastos_AwContents_onPermissionRequest(j_ref.Get(), j_request.Get());
}

void AwContents::OnPermissionRequestCanceled(AwPermissionRequest* request) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsCallback);
  //ScopedJavaLocalRef<jobject> j_request = request->GetJavaObject();
  Elastos::AutoPtr<IInterface> j_request = request->GetJavaObject();
  //ScopedJavaLocalRef<jobject> j_ref = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_ref = java_ref_.get();
  //if (j_request.is_null() || j_ref.is_null())
  if (!j_request.Get() || !j_ref.Get())
    return;

  sElaAwContentsCallback->elastos_AwContents_onPermissionRequestCanceled(
      j_ref.Get(), j_request.Get());
}

void AwContents::PreauthorizePermission(
    IInterface* obj,
    const Elastos::String& origin,
    Elastos::Int64 resources) {
  permission_request_handler_->PreauthorizePermission(
      GURL(base::android::ConvertJavaStringToUTF8(origin)), resources);
}

void AwContents::RequestProtectedMediaIdentifierPermission(
    const GURL& origin,
    const base::Callback<void(bool)>& callback) {
  permission_request_handler_->SendRequest(
      scoped_ptr<AwPermissionRequestDelegate>(new SimplePermissionRequest(
          origin, AwPermissionRequest::ProtectedMediaId, callback)));
}

void AwContents::CancelProtectedMediaIdentifierPermissionRequests(
    const GURL& origin) {
  permission_request_handler_->CancelRequest(
      origin, AwPermissionRequest::ProtectedMediaId);
}

void AwContents::RequestGeolocationPermission(
    const GURL& origin,
    const base::Callback<void(bool)>& callback) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsCallback);
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;

  if (sElaAwContentsCallback->elastos_AwContents_useLegacyGeolocationPermissionAPI(obj.Get())) {
    ShowGeolocationPrompt(origin, callback);
    return;
  }
  permission_request_handler_->SendRequest(
      scoped_ptr<AwPermissionRequestDelegate>(new SimplePermissionRequest(
          origin, AwPermissionRequest::Geolocation, callback)));
}

void AwContents::CancelGeolocationPermissionRequests(const GURL& origin) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsCallback);
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;

  if (sElaAwContentsCallback->elastos_AwContents_useLegacyGeolocationPermissionAPI(obj.Get())) {
    HideGeolocationPrompt(origin);
    return;
  }
  permission_request_handler_->CancelRequest(
      origin, AwPermissionRequest::Geolocation);
}

void AwContents::FindAllAsync(IInterface* obj, const Elastos::String& search_string) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  GetFindHelper()->FindAllAsync(ConvertJavaStringToUTF16(search_string));
}

void AwContents::FindNext(IInterface* obj, Elastos::Boolean forward) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  GetFindHelper()->FindNext(forward);
}

void AwContents::ClearMatches(IInterface* obj) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  GetFindHelper()->ClearMatches();
}

void AwContents::ClearCache(
    IInterface* obj,
    Elastos::Boolean include_disk_files) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  render_view_host_ext_->ClearCache();

  if (include_disk_files) {
    RemoveHttpDiskCache(web_contents_->GetBrowserContext(),
                        web_contents_->GetRoutingID());
  }
}

FindHelper* AwContents::GetFindHelper() {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  if (!find_helper_.get()) {
    find_helper_.reset(new FindHelper(web_contents_.get()));
    find_helper_->SetListener(this);
  }
  return find_helper_.get();
}

void AwContents::OnFindResultReceived(int active_ordinal,
                                      int match_count,
                                      bool finished) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAwContentsCallback);
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;

  sElaAwContentsCallback->elastos_AwContents_onFindResultReceived(
      obj.Get(), active_ordinal, match_count, finished);
}

bool AwContents::ShouldDownloadFavicon(const GURL& icon_url) {
  return g_should_download_favicons;
}

void AwContents::OnReceivedIcon(const GURL& icon_url, const SkBitmap& bitmap) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;

  content::NavigationEntry* entry =
      web_contents_->GetController().GetActiveEntry();

  if (entry) {
    entry->GetFavicon().valid = true;
    entry->GetFavicon().url = icon_url;
    entry->GetFavicon().image = gfx::Image::CreateFrom1xBitmap(bitmap);
  }

  sElaAwContentsCallback->elastos_AwContents_onReceivedIcon(
      obj.Get(), gfx::ConvertToJavaBitmap(&bitmap).Get());
}

void AwContents::OnReceivedTouchIconUrl(const std::string& url,
                                        bool precomposed) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;

  sElaAwContentsCallback->elastos_AwContents_onReceivedTouchIconUrl(
      obj.Get(), ConvertUTF8ToJavaString(url), precomposed);
}

bool AwContents::RequestDrawGL(IInterface* canvas, bool wait_for_completion) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  DCHECK(!canvas || !wait_for_completion);
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return false;
  return sElaAwContentsCallback->elastos_AwContents_requestDrawGL(
      obj.Get(), canvas, wait_for_completion);
}

void AwContents::PostInvalidate() {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (!obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (obj.Get())
    sElaAwContentsCallback->elastos_AwContents_postInvalidateOnAnimation(obj.Get());
}

void AwContents::UpdateParentDrawConstraints() {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.UpdateParentDrawConstraints();
}

void AwContents::OnNewPicture() {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (!obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (obj.Get()) {
    devtools_instrumentation::ScopedEmbedderCallbackTask embedder_callback(
        "onNewPicture");
    sElaAwContentsCallback->elastos_AwContents_onNewPicture(obj.Get());
  }
}

//base::android::ScopedJavaLocalRef<jbyteArray>
Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> >
    AwContents::GetCertificate(IInterface* obj) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  content::NavigationEntry* entry =
      web_contents_->GetController().GetActiveEntry();
  if (!entry)
    return Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> >();
    //return ScopedJavaLocalRef<jbyteArray>();
  // Get the certificate
  int cert_id = entry->GetSSL().cert_id;
  scoped_refptr<net::X509Certificate> cert;
  bool ok = content::CertStore::GetInstance()->RetrieveCert(cert_id, &cert);
  if (!ok)
    return Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> >();
    //return ScopedJavaLocalRef<jbyteArray>();

  // Convert the certificate and return it
  std::string der_string;
  net::X509Certificate::GetDEREncoded(cert->os_cert_handle(), &der_string);
  return base::android::ToJavaByteArray(
      reinterpret_cast<const uint8*>(der_string.data()), der_string.length());
}

void AwContents::RequestNewHitTestDataAt(IInterface* obj,
                                         Elastos::Int32 x, Elastos::Int32 y) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  render_view_host_ext_->RequestNewHitTestDataAt(x, y);
}

void AwContents::UpdateLastHitTestData(IInterface* obj) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  if (!render_view_host_ext_->HasNewHitTestData()) return;

  const AwHitTestData& data = render_view_host_ext_->GetLastHitTestData();
  render_view_host_ext_->MarkHitTestDataRead();

  // Make sure to null the Java object if data is empty/invalid.
  //ScopedJavaLocalRef<jstring> extra_data_for_type;
  Elastos::String extra_data_for_type;
  if (data.extra_data_for_type.length())
    extra_data_for_type = ConvertUTF8ToJavaString(
        data.extra_data_for_type);

  //ScopedJavaLocalRef<jstring> href;
  Elastos::String  href;
  if (data.href.length())
    href = ConvertUTF16ToJavaString(data.href);

  //ScopedJavaLocalRef<jstring> anchor_text;
  Elastos::String anchor_text;
  if (data.anchor_text.length())
    anchor_text = ConvertUTF16ToJavaString(data.anchor_text);

  //ScopedJavaLocalRef<jstring> img_src;
  Elastos::String img_src;
  if (data.img_src.is_valid())
    img_src = ConvertUTF8ToJavaString(data.img_src.spec());

  sElaAwContentsCallback->elastos_AwContents_updateHitTestData(obj,
                                    data.type,
                                    extra_data_for_type,
                                    href,
                                    anchor_text,
                                    img_src);
}

void AwContents::OnSizeChanged(IInterface* obj,
                               int w, int h, int ow, int oh) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.OnSizeChanged(w, h);
}

void AwContents::SetViewVisibility(IInterface* obj, bool visible) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.SetViewVisibility(visible);
}

void AwContents::SetWindowVisibility(IInterface* obj, bool visible) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.SetWindowVisibility(visible);
}

void AwContents::SetIsPaused(IInterface* obj, bool paused) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.SetIsPaused(paused);
  ContentViewCore* cvc =
      ContentViewCore::FromWebContents(web_contents_.get());
  if (cvc) {
    cvc->PauseOrResumeGeolocation(paused);
    cvc->PauseOrResumeVideoCaptureStream(paused);
    //TODO leliang
	/*
    //ActionsCode(hexibin, bugfix BUG00262636)
    //if (paused) {
      cvc->SetIsPaused(paused);
    //}
	*/
	if (paused) {
      cvc->PauseVideo();
    }
  }
}

void AwContents::OnAttachedToWindow(IInterface* obj, int w, int h) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.OnAttachedToWindow(w, h);
}

void AwContents::InitializeHardwareDrawIfNeeded() {
  GLViewRendererManager* manager = GLViewRendererManager::GetInstance();

  base::AutoLock lock(render_thread_lock_);
  if (renderer_manager_key_ == manager->NullKey()) {
    renderer_manager_key_ = manager->PushBack(&shared_renderer_state_);
    DeferredGpuCommandService::SetInstance();
  }
}

void AwContents::OnDetachedFromWindow() {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  ReleaseHardwareDrawIfNeeded();
  browser_view_renderer_.OnDetachedFromWindow();
}

void AwContents::ReleaseHardwareDrawIfNeeded() {
  InsideHardwareReleaseReset inside_reset(&shared_renderer_state_);

  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (!obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (obj.Get())
    sElaAwContentsCallback->elastos_AwContents_invalidateOnFunctorDestroy(obj.Get());

  bool hardware_initialized = browser_view_renderer_.hardware_enabled();
  if (hardware_initialized) {
    bool draw_functor_succeeded = RequestDrawGL(NULL, true);
    if (!draw_functor_succeeded) {
      LOG(ERROR) << "Unable to free GL resources. Has the Window leaked?";
      // Calling release on wrong thread intentionally.
      AwDrawGLInfo info;
      info.mode = AwDrawGLInfo::kModeProcess;
      DrawGL(&info);
    }
    browser_view_renderer_.ReleaseHardware();
  }
  DCHECK(!hardware_renderer_);

  GLViewRendererManager* manager = GLViewRendererManager::GetInstance();

  {
    base::AutoLock lock(render_thread_lock_);
    if (renderer_manager_key_ != manager->NullKey()) {
      manager->Remove(renderer_manager_key_);
      renderer_manager_key_ = manager->NullKey();
    }
  }

  if (hardware_initialized) {
    // Flush any invoke functors that's caused by OnDetachedFromWindow.
    RequestDrawGL(NULL, true);
  }
}

//base::android::ScopedJavaLocalRef<jbyteArray>
Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> >
AwContents::GetOpaqueState(IInterface* obj) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  // Required optimization in WebViewClassic to not save any state if
  // there has been no navigations.
  if (!web_contents_->GetController().GetEntryCount())
    return Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> >();
    //return ScopedJavaLocalRef<jbyteArray>();

  Pickle pickle;
  if (!WriteToPickle(*web_contents_, &pickle)) {
    return Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> >();
    //return ScopedJavaLocalRef<jbyteArray>();
  } else {
    return base::android::ToJavaByteArray(
       reinterpret_cast<const uint8*>(pickle.data()), pickle.size());
  }
}

Elastos::Boolean AwContents::RestoreFromOpaqueState(
    IInterface* obj,
    Elastos::ArrayOf<Elastos::Byte>* state) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  // TODO(boliu): This copy can be optimized out if this is a performance
  // problem.
  std::vector<uint8> state_vector;
  base::android::JavaByteArrayToByteVector(state, &state_vector);

  Pickle pickle(reinterpret_cast<const char*>(state_vector.begin()),
                state_vector.size());
  PickleIterator iterator(pickle);

  return RestoreFromPickle(&iterator, web_contents_.get());
}

bool AwContents::OnDraw(IInterface* obj,
                        IInterface* canvas,
                        Elastos::Boolean is_hardware_accelerated,
                        Elastos::Int32 scroll_x,
                        Elastos::Int32 scroll_y,
                        Elastos::Int32 visible_left,
                        Elastos::Int32 visible_top,
                        Elastos::Int32 visible_right,
                        Elastos::Int32 visible_bottom) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  if (is_hardware_accelerated)
    InitializeHardwareDrawIfNeeded();
  return browser_view_renderer_.OnDraw(
      canvas,
      is_hardware_accelerated,
      gfx::Vector2d(scroll_x, scroll_y),
      gfx::Rect(visible_left,
                visible_top,
                visible_right - visible_left,
                visible_bottom - visible_top));
}

void AwContents::SetPendingWebContentsForPopup(
    scoped_ptr<content::WebContents> pending) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  if (pending_contents_.get()) {
    // TODO(benm): Support holding multiple pop up window requests.
    LOG(WARNING) << "Blocking popup window creation as an outstanding "
                 << "popup window is still pending.";
    base::MessageLoop::current()->DeleteSoon(FROM_HERE, pending.release());
    return;
  }
  pending_contents_.reset(new AwContents(pending.Pass()));
}

void AwContents::FocusFirstNode(IInterface* obj) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  web_contents_->FocusThroughTabTraversal(false);
}

void AwContents::SetBackgroundColor(IInterface* obj, Elastos::Int32 color) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  render_view_host_ext_->SetBackgroundColor(color);
}

Elastos::Handle64 AwContents::ReleasePopupAwContents(IInterface* obj) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  return reinterpret_cast<intptr_t>(pending_contents_.release());
}

gfx::Point AwContents::GetLocationOnScreen() {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return gfx::Point();
  std::vector<int> location;
  base::android::JavaIntArrayToIntVector(
      sElaAwContentsCallback->elastos_AwContents_getLocationOnScreen(obj.Get()).Get(),
      &location);
  return gfx::Point(location[0], location[1]);
}

void AwContents::ScrollContainerViewTo(gfx::Vector2d new_value) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  sElaAwContentsCallback->elastos_AwContents_scrollContainerViewTo(
      obj.Get(), new_value.x(), new_value.y());
}

bool AwContents::IsFlingActive() const {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return false;
  return sElaAwContentsCallback->elastos_AwContents_isFlingActive(obj.Get());
}

void AwContents::UpdateScrollState(gfx::Vector2d max_scroll_offset,
                                   gfx::SizeF contents_size_dip,
                                   float page_scale_factor,
                                   float min_page_scale_factor,
                                   float max_page_scale_factor) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  sElaAwContentsCallback->elastos_AwContents_updateScrollState(
                                    obj.Get(),
                                    max_scroll_offset.x(),
                                    max_scroll_offset.y(),
                                    contents_size_dip.width(),
                                    contents_size_dip.height(),
                                    page_scale_factor,
                                    min_page_scale_factor,
                                    max_page_scale_factor);
}

void AwContents::DidOverscroll(gfx::Vector2d overscroll_delta) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  sElaAwContentsCallback->elastos_AwContents_didOverscroll(
      obj.Get(), overscroll_delta.x(), overscroll_delta.y());
}

const BrowserViewRenderer* AwContents::GetBrowserViewRenderer() const {
  return &browser_view_renderer_;
}

void AwContents::SetDipScale(IInterface* obj, Elastos::Float dip_scale) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.SetDipScale(dip_scale);
}

void AwContents::ScrollTo(IInterface* obj, Elastos::Int32 x, Elastos::Int32 y) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.ScrollTo(gfx::Vector2d(x, y));
}

void AwContents::OnWebLayoutPageScaleFactorChanged(float page_scale_factor) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  sElaAwContentsCallback->elastos_AwContents_onWebLayoutPageScaleFactorChanged(obj.Get(),
                                                         page_scale_factor);
}

void AwContents::OnWebLayoutContentsSizeChanged(
    const gfx::Size& contents_size) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  //if (obj.is_null())
  DCHECK(sElaAwContentsCallback);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  sElaAwContentsCallback->elastos_AwContents_onWebLayoutContentsSizeChanged(
      obj.Get(), contents_size.width(), contents_size.height());
}

Elastos::Handle64 AwContents::CapturePicture(IInterface* obj,
                                 int width,
                                 int height) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  return reinterpret_cast<intptr_t>(
      new AwPicture(browser_view_renderer_.CapturePicture(width, height)));
}

void AwContents::EnableOnNewPicture(IInterface* obj,
                                    Elastos::Boolean enabled) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.EnableOnNewPicture(enabled);
}

void AwContents::ClearView(IInterface* obj) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  browser_view_renderer_.ClearView();
}

void AwContents::SetExtraHeadersForUrl(IInterface* obj,
                                       const Elastos::String& url, const Elastos::String& jextra_headers) {
  std::string extra_headers;
  if (jextra_headers)
    extra_headers = ConvertJavaStringToUTF8(jextra_headers);
  AwResourceContext* resource_context = static_cast<AwResourceContext*>(
      AwBrowserContext::FromWebContents(web_contents_.get())->
      GetResourceContext());
  resource_context->SetExtraHeaders(GURL(ConvertJavaStringToUTF8(url)),
                                    extra_headers);
}

void AwContents::SetJsOnlineProperty(IInterface* obj,
                                     Elastos::Boolean network_up) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  render_view_host_ext_->SetJsOnlineProperty(network_up);
}

void AwContents::TrimMemory(IInterface* obj,
                            Elastos::Int32 level,
                            Elastos::Boolean visible) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  enum {
    TRIM_MEMORY_MODERATE = 60,
  };
  if (level >= TRIM_MEMORY_MODERATE) {
    ReleaseHardwareDrawIfNeeded();
    return;
  }

  browser_view_renderer_.TrimMemory(level, visible);
}

void SetShouldDownloadFavicons() {
  g_should_download_favicons = true;
}

}  // namespace android_webview
