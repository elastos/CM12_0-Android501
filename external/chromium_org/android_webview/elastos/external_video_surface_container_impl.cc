// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/external_video_surface_container_impl.h"

#include "base/elastos/api_elastos.h"
#include "content/public/browser/elastos/content_view_core.h"
#include "android_webview/api/ExternalVideoSurfaceContainer_api.h"
#include "ui/gfx/rect_f.h"

//using base::android::AttachCurrentThread;
using content::ContentViewCore;

namespace android_webview {

ExternalVideoSurfaceContainerImpl::ExternalVideoSurfaceContainerImpl(
    content::WebContents* web_contents) {
  ContentViewCore* cvc = ContentViewCore::FromWebContents(web_contents);
  if (cvc) {
    //JNIEnv* env = AttachCurrentThread();
    DCHECK(sElaExternalVideoSurfaceContainerCallback);
    jobject_ =
        sElaExternalVideoSurfaceContainerCallback->elastos_ExternalVideoSurfaceContainer_create(
            reinterpret_cast<intptr_t>(this), cvc->GetJavaObject().Get());
  }
}

ExternalVideoSurfaceContainerImpl::~ExternalVideoSurfaceContainerImpl() {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaExternalVideoSurfaceContainerCallback);
  sElaExternalVideoSurfaceContainerCallback->elastos_ExternalVideoSurfaceContainer_destroy(jobject_.Get());
  jobject_ = NULL;
}

void ExternalVideoSurfaceContainerImpl::RequestExternalVideoSurface(
    int player_id,
    const SurfaceCreatedCB& surface_created_cb,
    const SurfaceDestroyedCB& surface_destroyed_cb) {
  surface_created_cb_ = surface_created_cb;
  surface_destroyed_cb_ = surface_destroyed_cb;

  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaExternalVideoSurfaceContainerCallback);
  sElaExternalVideoSurfaceContainerCallback->elastos_ExternalVideoSurfaceContainer_requestExternalVideoSurface(
      jobject_.Get(), static_cast<Elastos::Int32>(player_id));
}

void ExternalVideoSurfaceContainerImpl::ReleaseExternalVideoSurface(
    int player_id) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaExternalVideoSurfaceContainerCallback);
  sElaExternalVideoSurfaceContainerCallback->elastos_ExternalVideoSurfaceContainer_releaseExternalVideoSurface(
      jobject_.Get(), static_cast<Elastos::Int32>(player_id));

  surface_created_cb_.Reset();
  surface_destroyed_cb_.Reset();
}

void ExternalVideoSurfaceContainerImpl::OnFrameInfoUpdated() {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaExternalVideoSurfaceContainerCallback);
  sElaExternalVideoSurfaceContainerCallback->elastos_ExternalVideoSurfaceContainer_onFrameInfoUpdated(jobject_.Get());
}

void ExternalVideoSurfaceContainerImpl::OnExternalVideoSurfacePositionChanged(
    int player_id, const gfx::RectF& rect) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaExternalVideoSurfaceContainerCallback);
  sElaExternalVideoSurfaceContainerCallback->
      elastos_ExternalVideoSurfaceContainer_onExternalVideoSurfacePositionChanged(
      jobject_.Get(),
      static_cast<Elastos::Int32>(player_id),
      static_cast<Elastos::Float>(rect.x()),
      static_cast<Elastos::Float>(rect.y()),
      static_cast<Elastos::Float>(rect.x() + rect.width()),
      static_cast<Elastos::Float>(rect.y() + rect.height()));
}

// Methods called from Java.
void ExternalVideoSurfaceContainerImpl::SurfaceCreated(
    IInterface* obj, Elastos::Int32 player_id, IInterface* jsurface) {
  if (!surface_created_cb_.is_null())
    surface_created_cb_.Run(static_cast<int>(player_id), jsurface);
}

void ExternalVideoSurfaceContainerImpl::SurfaceDestroyed(
    IInterface* obj, Elastos::Int32 player_id) {
  if (!surface_destroyed_cb_.is_null())
    surface_destroyed_cb_.Run(static_cast<int>(player_id));
}

bool RegisterExternalVideoSurfaceContainer() {
  return RegisterNativesImpl();
}

}  // namespace android_webview
