// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/aw_form_database.h"

#include "android_webview/browser/aw_browser_context.h"
#include "android_webview/browser/aw_content_browser_client.h"
#include "android_webview/browser/aw_form_database_service.h"
#include "base/elastos/api_elastos.h"
#include "base/logging.h"
#include "base/time/time.h"
#include "android_webview/api/AwFormDatabase_api.h"

namespace android_webview {

namespace {

AwFormDatabaseService* GetFormDatabaseService() {

  AwBrowserContext* context = AwContentBrowserClient::GetAwBrowserContext();
  AwFormDatabaseService* service = context->GetFormDatabaseService();
  return service;
}

} // anonymous namespace

// static
Elastos::Boolean HasFormData() {
  return GetFormDatabaseService()->HasFormData();
}

// static
void ClearFormData() {
  GetFormDatabaseService()->ClearFormData();
}

bool RegisterAwFormDatabase() {
  return RegisterNativesImpl();
}

} // namespace android_webview
