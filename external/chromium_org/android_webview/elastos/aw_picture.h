// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef ANDROID_WEBVIEW_NATIVE_AW_PICTURE_H_
#define ANDROID_WEBVIEW_NATIVE_AW_PICTURE_H_

//#include <jni.h>

#include "base/elastos/api_weak_ref.h"
//#include "base/android/scoped_java_ref.h"
#include "base/memory/scoped_ptr.h"
#include "content/public/browser/web_contents_observer.h"
#include "skia/ext/refptr.h"

class SkPicture;

namespace android_webview {

class AwPicture {
 public:
  AwPicture(skia::RefPtr<SkPicture> picture);
  ~AwPicture();

  // Methods called from Java.
  void Destroy();
  Elastos::Int32 GetWidth(IInterface* obj);
  Elastos::Int32 GetHeight(IInterface* obj);
  void Draw(IInterface* obj, IInterface* canvas);

 private:
  skia::RefPtr<SkPicture> picture_;

  DISALLOW_IMPLICIT_CONSTRUCTORS(AwPicture);
};

bool RegisterAwPicture();

}  // android_webview

#endif  // ANDROID_WEBVIEW_NATIVE_AW_PICTURE_
