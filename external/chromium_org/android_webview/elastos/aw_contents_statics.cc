// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/aw_contents_statics.h"

#include "android_webview/browser/aw_browser_context.h"
#include "base/elastos/jni_string.h"
//#include "base/android/scoped_java_ref.h"
#include "ElAndroid.h"
#include "elastos.h"
#include "base/callback.h"
#include "components/data_reduction_proxy/browser/data_reduction_proxy_settings.h"
#include "content/public/browser/elastos/synchronous_compositor.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/common/url_constants.h"
#include "android_webview/api/AwContentsStatics_api.h"
#include "net/cert/cert_database.h"

//using base::android::AttachCurrentThread;
using base::android::ConvertJavaStringToUTF8;
//using base::android::ScopedJavaGlobalRef;
using content::BrowserThread;
using data_reduction_proxy::DataReductionProxySettings;

namespace android_webview {

namespace {

//void ClientCertificatesCleared(ScopedJavaGlobalRef<jobject>* callback) {
void ClientCertificatesCleared(IInterface* callback) {
  DCHECK_CURRENTLY_ON(BrowserThread::UI);
  //JNIEnv* env = AttachCurrentThread();
  //Java_AwContentsStatics_clientCertificatesCleared(env, callback->obj());
  DCHECK(sElaAwContentsStaticsCallback);
  //Java_AwContentsStatics_clientCertificatesCleared(env, callback->obj());
  sElaAwContentsStaticsCallback->elastos_AwContentsStatics_clientCertificatesCleared(callback);
}

void NotifyClientCertificatesChanged() {
  DCHECK_CURRENTLY_ON(BrowserThread::IO);
  net::CertDatabase::GetInstance()->OnAndroidKeyStoreChanged();
}

}  // namespace

// static
void ClearClientCertPreferences(IInterface* callback) {
  DCHECK_CURRENTLY_ON(content::BrowserThread::UI);
  //ScopedJavaGlobalRef<jobject>* j_callback = new ScopedJavaGlobalRef<jobject>();
  //j_callback->Reset(env, callback);
  Elastos::AutoPtr<IInterface> j_callback = callback;
  BrowserThread::PostTaskAndReply(
      BrowserThread::IO,
      FROM_HERE,
      base::Bind(&NotifyClientCertificatesChanged),
      base::Bind(&ClientCertificatesCleared, j_callback));
      //TODO if parameter j_callback is OK.
      //base::Bind(&ClientCertificatesCleared, base::Owned(j_callback)));
}

// static
void SetDataReductionProxyKey(const Elastos::String& key) {
  AwBrowserContext* browser_context = AwBrowserContext::GetDefault();
  DCHECK(browser_context);
  DataReductionProxySettings* drp_settings =
      browser_context->GetDataReductionProxySettings();
  if (drp_settings)
    drp_settings->params()->set_key(ConvertJavaStringToUTF8(key));
}

// static
void SetDataReductionProxyEnabled(Elastos::Boolean enabled) {
  AwBrowserContext::SetDataReductionProxyEnabled(enabled);
}

// static
Elastos::String GetUnreachableWebDataUrl() {
  return base::android::ConvertUTF8ToJavaString(
             content::kUnreachableWebDataURL);
}

// static
void SetRecordFullDocument(Elastos::Boolean record_full_document) {
  content::SynchronousCompositor::SetRecordFullDocument(record_full_document);
}

bool RegisterAwContentsStatics() {
  return RegisterNativesImpl();
}

}  // namespace android_webview
