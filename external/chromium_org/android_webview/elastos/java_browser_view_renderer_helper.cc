// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/java_browser_view_renderer_helper.h"

//#include <android/bitmap.h>
#include <elastos/bitmap.h>

#include "android_webview/public/browser/draw_sw.h"
#include "base/debug/trace_event.h"
#include "android_webview/api/JavaBrowserViewRendererHelper_api.h"
#include "third_party/skia/include/core/SkBitmap.h"
#include "third_party/skia/include/utils/SkCanvasStateUtils.h"

//using base::android::JavaRef;
//using base::android::ScopedJavaLocalRef;

namespace android_webview {

namespace {

// Provides software rendering functions from the Android glue layer.
// Allows preventing extra copies of data when rendering.
AwDrawSWFunctionTable* g_sw_draw_functions = NULL;

class ScopedPixelAccess {
 public:
  ScopedPixelAccess(IInterface* java_canvas) : pixels_(NULL) {
    if (g_sw_draw_functions)
      pixels_ = g_sw_draw_functions->access_pixels(java_canvas);
  }

  ~ScopedPixelAccess() {
    if (pixels_)
      g_sw_draw_functions->release_pixels(pixels_);
  }

  AwPixelInfo* pixels() { return pixels_; }

 private:
  AwPixelInfo* pixels_;

  DISALLOW_IMPLICIT_CONSTRUCTORS(ScopedPixelAccess);
};

}  // namespace

// static
void JavaBrowserViewRendererHelper::SetAwDrawSWFunctionTable(
    AwDrawSWFunctionTable* table) {
  g_sw_draw_functions = table;
}

// static
JavaBrowserViewRendererHelper* JavaBrowserViewRendererHelper::GetInstance() {
  static JavaBrowserViewRendererHelper* g_instance =
      new JavaBrowserViewRendererHelper;
  return g_instance;
}

// static
BrowserViewRendererJavaHelper* BrowserViewRendererJavaHelper::GetInstance() {
  return JavaBrowserViewRendererHelper::GetInstance();
}

JavaBrowserViewRendererHelper::JavaBrowserViewRendererHelper() {}

JavaBrowserViewRendererHelper::~JavaBrowserViewRendererHelper() {}

bool JavaBrowserViewRendererHelper::RenderViaAuxilaryBitmapIfNeeded(
    IInterface* java_canvas,
    const gfx::Vector2d& scroll_correction,
    const gfx::Size& auxiliary_bitmap_size,
    RenderMethod render_source) {
  TRACE_EVENT0("android_webview", "RenderViaAuxilaryBitmapIfNeeded");

  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaJavaBrowserViewRendererHelperCallback);
  ScopedPixelAccess auto_release_pixels(java_canvas);
  AwPixelInfo* pixels = auto_release_pixels.pixels();
  if (pixels && pixels->state) {
    skia::RefPtr<SkCanvas> canvas = skia::AdoptRef(
        SkCanvasStateUtils::CreateFromCanvasState(pixels->state));

    // Workarounds for http://crbug.com/271096: SW draw only supports
    // translate & scale transforms, and a simple rectangular clip.
    if (canvas && (!canvas->isClipRect() ||
                   (canvas->getTotalMatrix().getType() &
                    ~(SkMatrix::kTranslate_Mask | SkMatrix::kScale_Mask)))) {
      canvas.clear();
    }
    if (canvas) {
      canvas->translate(scroll_correction.x(), scroll_correction.y());
      return render_source.Run(canvas.get());
    }
  }
  return RenderViaAuxilaryBitmap(java_canvas,
                                 scroll_correction,
                                 auxiliary_bitmap_size,
                                 render_source);
}

bool JavaBrowserViewRendererHelper::RenderViaAuxilaryBitmap(
    IInterface* java_canvas,
    const gfx::Vector2d& scroll_correction,
    const gfx::Size& auxiliary_bitmap_size,
    const RenderMethod& render_source) {
  // Render into an auxiliary bitmap if pixel info is not available.
  //ScopedJavaLocalRef<jobject> jcanvas(env, java_canvas);
  Elastos::AutoPtr<IInterface> jcanvas(java_canvas);
  TRACE_EVENT0("android_webview", "RenderToAuxBitmap");

  if (auxiliary_bitmap_size.width() <= 0 || auxiliary_bitmap_size.height() <= 0)
    return false;

  DCHECK(sElaJavaBrowserViewRendererHelperCallback);
  //ScopedJavaLocalRef<jobject> jbitmap(
  Elastos::AutoPtr<IInterface> jbitmap =
      sElaJavaBrowserViewRendererHelperCallback->elastos_JavaBrowserViewRendererHelper_createBitmap(
          auxiliary_bitmap_size.width(),
          auxiliary_bitmap_size.height(),
          jcanvas.Get());
  if (!jbitmap.Get())
    return false;

  if (!RasterizeIntoBitmap(jbitmap,
                           render_source)) {
    return false;
  }

  sElaJavaBrowserViewRendererHelperCallback->elastos_JavaBrowserViewRendererHelper_drawBitmapIntoCanvas(
      jbitmap.Get(),
      jcanvas.Get(),
      scroll_correction.x(),
      scroll_correction.y());
  return true;
}

bool RegisterJavaBrowserViewRendererHelper() {
  return RegisterNativesImpl();
}

bool JavaBrowserViewRendererHelper::RasterizeIntoBitmap(
    IInterface* jbitmap,
    const JavaBrowserViewRendererHelper::RenderMethod& renderer) {
  DCHECK(jbitmap);

  AndroidBitmapInfo bitmap_info;
  if (AndroidBitmap_getInfo(jbitmap, &bitmap_info) < 0) {
    LOG(ERROR) << "Error getting java bitmap info.";
    return false;
  }

  void* pixels = NULL;
  if (AndroidBitmap_lockPixels(jbitmap, &pixels) < 0) {
    LOG(ERROR) << "Error locking java bitmap pixels.";
    return false;
  }

  bool succeeded;
  {
    SkImageInfo info =
        SkImageInfo::MakeN32Premul(bitmap_info.width, bitmap_info.height);
    SkBitmap bitmap;
    bitmap.installPixels(info, pixels, bitmap_info.stride);

    SkCanvas canvas(bitmap);
    succeeded = renderer.Run(&canvas);
  }

  if (AndroidBitmap_unlockPixels(jbitmap) < 0) {
    LOG(ERROR) << "Error unlocking java bitmap pixels.";
    return false;
  }

  return succeeded;
}

}  // namespace android_webview
