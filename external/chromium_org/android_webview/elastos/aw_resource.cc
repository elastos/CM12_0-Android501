// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/aw_resource.h"

#include "base/elastos/jni_string.h"
//#include "base/android/scoped_java_ref.h"
#include "android_webview/api/AwResource_api.h"

namespace android_webview {
namespace AwResource {

// These JNI functions are used by the Renderer but rely on Java data
// structures that exist in the Browser. By virtue of the WebView running
// in single process we can just reach over and use them. When WebView is
// multi-process capable, we'll need to rethink these. See crbug.com/156062.

std::string GetLoadErrorPageContent() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaAwResourceCallback);
  //ScopedJavaLocalRef<jstring> content =
  Elastos::String content =
      sElaAwResourceCallback->elastos_AwResource_getLoadErrorPageContent();
      //Java_AwResource_getLoadErrorPageContent(env);
  return base::android::ConvertJavaStringToUTF8(content);
}

std::string GetNoDomainPageContent() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaAwResourceCallback);
  //ScopedJavaLocalRef<jstring> content =
  Elastos::String content =
      sElaAwResourceCallback->elastos_AwResource_getNoDomainPageContent();
  return base::android::ConvertJavaStringToUTF8(content);
}

std::string GetDefaultTextEncoding() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaAwResourceCallback);
  //ScopedJavaLocalRef<jstring> encoding =
  Elastos::String encoding =
      sElaAwResourceCallback->elastos_AwResource_getDefaultTextEncoding();
  return base::android::ConvertJavaStringToUTF8(encoding);
}

bool RegisterAwResource() {
  return RegisterNativesImpl();
}

}  // namespace AwResource
}  // namespace android_webview
