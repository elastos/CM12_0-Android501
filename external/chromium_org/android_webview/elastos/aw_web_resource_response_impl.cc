// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/aw_web_resource_response_impl.h"

#include "android_webview/elastos/input_stream_impl.h"
#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_array.h"
#include "base/elastos/jni_string.h"
#include "android_webview/api/AwWebResourceResponse_api.h"
#include "net/http/http_response_headers.h"
#include "net/url_request/url_request.h"
#include "net/url_request/url_request_job.h"

//using base::android::ScopedJavaLocalRef;
using base::android::ConvertJavaStringToUTF8;
using base::android::AppendJavaStringArrayToStringVector;

namespace android_webview {

AwWebResourceResponseImpl::AwWebResourceResponseImpl(
    IInterface* obj)
  : java_object_(obj) {
}

AwWebResourceResponseImpl::~AwWebResourceResponseImpl() {
}

scoped_ptr<InputStream>
AwWebResourceResponseImpl::GetInputStream() const {
  DCHECK(sElaAwWebResourceResponseCallback);
  //ScopedJavaLocalRef<jobject> jstream =
  Elastos::AutoPtr<IInterface> jstream =
      sElaAwWebResourceResponseCallback->elastos_AwWebResourceResponse_getData(java_object_.Get());
      //Java_AwWebResourceResponse_getData(env, java_object_.obj());
  if (!jstream.Get())
    return scoped_ptr<InputStream>();
  return make_scoped_ptr<InputStream>(new InputStreamImpl(jstream));
}

bool AwWebResourceResponseImpl::GetMimeType(std::string* mime_type) const {
  DCHECK(sElaAwWebResourceResponseCallback);
  //ScopedJavaLocalRef<jstring> jstring_mime_type =
  Elastos::String jstring_mime_type =
      sElaAwWebResourceResponseCallback->elastos_AwWebResourceResponse_getMimeType(java_object_.Get());
  if (jstring_mime_type.IsNullOrEmpty())
    return false;
  *mime_type = ConvertJavaStringToUTF8(jstring_mime_type);
  return true;
}

bool AwWebResourceResponseImpl::GetCharset(
    std::string* charset) const {
  DCHECK(sElaAwWebResourceResponseCallback);
  //ScopedJavaLocalRef<jstring> jstring_charset =
  Elastos::String jstring_charset =
      sElaAwWebResourceResponseCallback->elastos_AwWebResourceResponse_getCharset(java_object_.Get());
      //Java_AwWebResourceResponse_getCharset(env, java_object_.obj());
  if (jstring_charset.IsNullOrEmpty())
    return false;
  *charset = ConvertJavaStringToUTF8(jstring_charset);
  return true;
}

bool AwWebResourceResponseImpl::GetStatusInfo(
    int* status_code,
    std::string* reason_phrase) const {
  DCHECK(sElaAwWebResourceResponseCallback);
  int status =
      sElaAwWebResourceResponseCallback->elastos_AwWebResourceResponse_getStatusCode(java_object_.Get());
      //Java_AwWebResourceResponse_getStatusCode(env, java_object_.obj());
  //ScopedJavaLocalRef<jstring> jstring_reason_phrase =
  Elastos::String jstring_reason_phrase =
      sElaAwWebResourceResponseCallback->elastos_AwWebResourceResponse_getReasonPhrase(java_object_.Get());
      //Java_AwWebResourceResponse_getReasonPhrase(env, java_object_.obj());
  if (status < 100 || status >= 600 || jstring_reason_phrase.IsNullOrEmpty())
    return false;
  *status_code = status;
  *reason_phrase = ConvertJavaStringToUTF8(jstring_reason_phrase);
  return true;
}

bool AwWebResourceResponseImpl::GetResponseHeaders(
    net::HttpResponseHeaders* headers) const {
  DCHECK(sElaAwWebResourceResponseCallback);
  //ScopedJavaLocalRef<jobjectArray> jstringArray_headerNames =
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::String> > jstringArray_headerNames =
      sElaAwWebResourceResponseCallback->elastos_AwWebResourceResponse_getResponseHeaderNames(
                                                         java_object_.Get());
  //ScopedJavaLocalRef<jobjectArray> jstringArray_headerValues =
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::String> > jstringArray_headerValues =
      sElaAwWebResourceResponseCallback->elastos_AwWebResourceResponse_getResponseHeaderValues(
                                                          java_object_.Get());
  if (!jstringArray_headerNames.Get() || !jstringArray_headerValues.Get())
    return false;
  std::vector<std::string> header_names;
  std::vector<std::string> header_values;
  AppendJavaStringArrayToStringVector(
      jstringArray_headerNames.Get(), &header_names);
  AppendJavaStringArrayToStringVector(
      jstringArray_headerValues.Get(), &header_values);
  DCHECK(header_names.size() == header_values.size());
  for(size_t i = 0; i < header_names.size(); ++i) {
    std::string header_line(header_names[i]);
    header_line.append(": ");
    header_line.append(header_values[i]);
    headers->AddHeader(header_line);
  }
  return true;
}

bool RegisterAwWebResourceResponse() {
  return RegisterNativesImpl();
}

} // namespace android_webview
