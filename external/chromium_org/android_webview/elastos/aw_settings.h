// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef ANDROID_WEBVIEW_NATIVE_AW_SETTINGS_H_
#define ANDROID_WEBVIEW_NATIVE_AW_SETTINGS_H_

//#include <jni.h>

#include "base/elastos/api_weak_ref.h"
//#include "base/android/scoped_java_ref.h"
#include "base/memory/scoped_ptr.h"
#include "content/public/browser/web_contents_observer.h"

struct WebPreferences;

namespace android_webview {

class AwRenderViewHostExt;

class AwSettings : public content::WebContentsObserver {
 public:
  static AwSettings* FromWebContents(content::WebContents* web_contents);

  AwSettings(IInterface* obj, Elastos::Handle64 web_contents);
  virtual ~AwSettings();

  // Called from Java. Methods with "Locked" suffix require that the settings
  // access lock is held during their execution.
  void Destroy(IInterface* obj);
  void PopulateWebPreferencesLocked(IInterface* obj, Elastos::Handle64 web_prefs);
  void ResetScrollAndScaleState(IInterface* obj);
  void UpdateEverythingLocked(IInterface* obj);
  void UpdateInitialPageScaleLocked(IInterface* obj);
  void UpdateUserAgentLocked(IInterface* obj);
  void UpdateWebkitPreferencesLocked(IInterface* obj);
  void UpdateFormDataPreferencesLocked(IInterface* obj);
  void UpdateRendererPreferencesLocked(IInterface* obj);

  void PopulateWebPreferences(WebPreferences* web_prefs);

 private:
  AwRenderViewHostExt* GetAwRenderViewHostExt();
  void UpdateEverything();

  // WebContentsObserver overrides:
  virtual void RenderViewCreated(
      content::RenderViewHost* render_view_host) OVERRIDE;
  virtual void WebContentsDestroyed() OVERRIDE;

  bool renderer_prefs_initialized_;

  ObjectWeakGlobalRef aw_settings_;
};

bool RegisterAwSettings();

}  // namespace android_webview

#endif  // ANDROID_WEBVIEW_NATIVE_AW_SETTINGS_H_
