// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

//#include <jni.h>

#include "android_webview/elastos/aw_assets.h"

#include "base/elastos/jni_array.h"
#include "base/elastos/jni_string.h"
//#include "base/android/scoped_java_ref.h"
#include "android_webview/api/AwAssets_api.h"

namespace android_webview {
namespace AwAssets {

bool OpenAsset(const std::string& filename,
               int* fd,
               int64* offset,
               int64* size) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //ScopedJavaLocalRef<jlongArray> jarr = Java_AwAssets_openAsset(
  DCHECK(sElaAwAssetsCallback);
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Int64> > jarr = sElaAwAssetsCallback->elastos_AwAssets_openAsset(
      base::android::GetAppContext(),
      base::android::ConvertUTF8ToJavaString(filename));
  std::vector<long> results;
  base::android::JavaLongArrayToLongVector(jarr.Get(), &results);
  DCHECK_EQ(3U, results.size());
  *fd = static_cast<int>(results[0]);
  *offset = results[1];
  *size = results[2];
  return *fd != -1;
}

}  // namespace AwAssets

bool RegisterAwAssets() {
  return RegisterNativesImpl();
}

}  // namespace android_webview
