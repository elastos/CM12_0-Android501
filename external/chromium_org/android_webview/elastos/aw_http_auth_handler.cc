// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/aw_http_auth_handler.h"

#include "android_webview/browser/aw_login_delegate.h"
#include "android_webview/elastos/aw_contents.h"
#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
#include "content/public/browser/browser_thread.h"
#include "android_webview/api/AwHttpAuthHandler_api.h"
#include "net/base/auth.h"
#include "content/public/browser/web_contents.h"

using base::android::ConvertJavaStringToUTF16;

namespace android_webview {

AwHttpAuthHandler::AwHttpAuthHandler(AwLoginDelegate* login_delegate,
                                     net::AuthChallengeInfo* auth_info,
                                     bool first_auth_attempt)
    : login_delegate_(login_delegate),
      host_(auth_info->challenger.host()),
      realm_(auth_info->realm) {
  DCHECK(content::BrowserThread::CurrentlyOn(content::BrowserThread::UI));
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaAwHttpAuthHandlerCallback);
  http_auth_handler_ =
      sElaAwHttpAuthHandlerCallback->elastos_AwHttpAuthHandler_create(
          reinterpret_cast<intptr_t>(this), first_auth_attempt);
}

AwHttpAuthHandler:: ~AwHttpAuthHandler() {
  DCHECK(content::BrowserThread::CurrentlyOn(content::BrowserThread::UI));
  DCHECK(sElaAwHttpAuthHandlerCallback);
  sElaAwHttpAuthHandlerCallback->elastos_AwHttpAuthHandler_handlerDestroyed(
                                          http_auth_handler_.Get());
}

void AwHttpAuthHandler::Proceed(IInterface* obj,
                                const Elastos::String& user,
                                const Elastos::String& password) {
  DCHECK(content::BrowserThread::CurrentlyOn(content::BrowserThread::UI));
  if (login_delegate_.get()) {
    login_delegate_->Proceed(ConvertJavaStringToUTF16( user),
                             ConvertJavaStringToUTF16(password));
    login_delegate_ = NULL;
  }
}

void AwHttpAuthHandler::Cancel(IInterface* obj) {
  DCHECK(content::BrowserThread::CurrentlyOn(content::BrowserThread::UI));
  if (login_delegate_.get()) {
    login_delegate_->Cancel();
    login_delegate_ = NULL;
  }
}

bool AwHttpAuthHandler::HandleOnUIThread(content::WebContents* web_contents) {
  DCHECK(web_contents);
  DCHECK(content::BrowserThread::CurrentlyOn(content::BrowserThread::UI));
  AwContents* aw_contents = AwContents::FromWebContents(web_contents);

  return aw_contents->OnReceivedHttpAuthRequest(http_auth_handler_, host_,
                                                realm_);
}

// static
AwHttpAuthHandlerBase* AwHttpAuthHandlerBase::Create(
    AwLoginDelegate* login_delegate,
    net::AuthChallengeInfo* auth_info,
    bool first_auth_attempt) {
  return new AwHttpAuthHandler(login_delegate, auth_info, first_auth_attempt);
}

bool RegisterAwHttpAuthHandler() {
  return RegisterNativesImpl();
}

}  // namespace android_webview
