// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "android_webview/elastos/android_webview_jni_registrar.h"

#include "android_webview/elastos/android_protocol_handler.h"
#include "android_webview/elastos/aw_assets.h"
#include "android_webview/elastos/aw_autofill_client.h"
#include "android_webview/elastos/aw_contents.h"
#include "android_webview/elastos/aw_contents_client_bridge.h"
#include "android_webview/elastos/aw_contents_io_thread_client_impl.h"
#include "android_webview/elastos/aw_contents_statics.h"
#include "android_webview/elastos/aw_dev_tools_server.h"
#include "android_webview/elastos/aw_form_database.h"
#include "android_webview/elastos/aw_http_auth_handler.h"
#include "android_webview/elastos/aw_pdf_exporter.h"
#include "android_webview/elastos/aw_picture.h"
#include "android_webview/elastos/aw_quota_manager_bridge_impl.h"
#include "android_webview/elastos/aw_resource.h"
#include "android_webview/elastos/aw_settings.h"
#include "android_webview/elastos/aw_web_contents_delegate.h"
#include "android_webview/elastos/aw_web_resource_response_impl.h"
#include "android_webview/elastos/cookie_manager.h"
#include "android_webview/elastos/external_video_surface_container_impl.h"
#include "android_webview/elastos/input_stream_impl.h"
#include "android_webview/elastos/java_browser_view_renderer_helper.h"
#include "android_webview/elastos/permission/aw_permission_request.h"
#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "base/debug/trace_event.h"

namespace android_webview {

static base::android::RegistrationMethod kWebViewRegisteredMethods[] = {
  // Register JNI for android_webview classes.
  { "AndroidProtocolHandler", RegisterAndroidProtocolHandler },
  { "AwAutofillClient", RegisterAwAutofillClient },
  { "AwAssets", RegisterAwAssets },
  { "AwContents", RegisterAwContents },
  { "AwContentsClientBridge", RegisterAwContentsClientBridge },
  { "AwContentsIoThreadClientImpl", RegisterAwContentsIoThreadClientImpl },
  { "AwContentsStatics", RegisterAwContentsStatics },
  { "AwDevToolsServer", RegisterAwDevToolsServer },
  { "AwFormDatabase", RegisterAwFormDatabase },
  { "AwPicture", RegisterAwPicture },
  { "AwSettings", RegisterAwSettings },
  { "AwHttpAuthHandler", RegisterAwHttpAuthHandler },
  { "AwPdfExporter", RegisterAwPdfExporter },
  { "AwPermissionRequest", RegisterAwPermissionRequest },
  { "AwQuotaManagerBridge", RegisterAwQuotaManagerBridge },
  { "AwResource", AwResource::RegisterAwResource },
  { "AwWebContentsDelegate", RegisterAwWebContentsDelegate },
  { "CookieManager", RegisterCookieManager },
#if defined(VIDEO_HOLE)
  { "ExternalVideoSurfaceContainer", RegisterExternalVideoSurfaceContainer },
#endif
  { "AwWebResourceResponseImpl", RegisterAwWebResourceResponse },
  { "InputStream", RegisterInputStream },
  { "JavaBrowserViewRendererHelper", RegisterJavaBrowserViewRendererHelper },
};

bool RegisterJni() {
  TRACE_EVENT0("startup", "android_webview::RegisterJni");
  return RegisterNativeMethods(
      kWebViewRegisteredMethods, arraysize(kWebViewRegisteredMethods));
}

} // namespace android_webview
