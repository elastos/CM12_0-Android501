// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "base/elastos/jni_utils.h"
#include "base/elastos/library_loader/library_loader_hooks.h"
#include "android_webview/lib/main/aw_main_delegate.h"
#include "android_webview/elastos/android_webview_jni_registrar.h"
#include "components/navigation_interception_elastos/component_jni_registrar.h"
#include "components/web_contents_delegate_elastos/component_jni_registrar.h"
#include "content/public/app/elastos_library_loader_hooks.h"
#include "content/public/app/content_main.h"
#include "url/url_util.h"
#include "printing/printing_api_registrar.h" //new Added to export the initcallback

static base::android::RegistrationMethod
    kWebViewDependencyRegisteredMethods[] = {
    { "NavigationInterception",
        navigation_interception::RegisterNavigationInterceptionJni },
    { "WebContentsDelegateAndroid",
        web_contents_delegate_android::RegisterWebContentsDelegateAndroidJni },
    { "PrintingContext", printing::RegisterPrintingContextJni},
};

// This is called by the VM when the shared library is first loaded.
// Most of the initialization is done in LibraryLoadedOnMainThread(), not here.
//JNI_EXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved) {
JNI_EXPORT int JNI_OnLoad(void* reserved) {

  LOG(INFO) << "webview_entry_point_elastos::JNI_OnLoad";
  base::android::SetLibraryLoadedHook(&content::LibraryLoaded);

  //LLbase::android::InitVM(vm);
  //JNIEnv* env = base::android::AttachCurrentThread();
  if (!base::android::RegisterLibraryLoaderEntryHook())
    return -1;

  // Register content JNI functions now, rather than waiting until
  // LibraryLoadedOnMainThread, so that we can call into native code early.
  if (!content::EnsureJniRegistered())
    return -1;

  // Register JNI for components we depend on.
  if (!RegisterNativeMethods(
      kWebViewDependencyRegisteredMethods,
      arraysize(kWebViewDependencyRegisteredMethods)))
    return -1;

  if (!android_webview::RegisterJni())
    return -1;

  //base::android::InitReplacementClassLoader(env,
  //                                          base::android::GetClassLoader(env));

  content::SetContentMainDelegate(new android_webview::AwMainDelegate());

  // Initialize url lib here while we are still single-threaded, in case we use
  // CookieManager before initializing Chromium (which would normally have done
  // this). It's safe to call this multiple times.
  url::Initialize();

  //return JNI_VERSION_1_4;
  return 0;
}
