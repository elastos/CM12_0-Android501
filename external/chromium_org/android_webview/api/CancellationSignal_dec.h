//This file is autogenerated for
//    CancellationSignal.class
//put this file at the end of the include list
//so the type definition used in this file will be found
#ifndef ELASTOS_CANCELLATIONSIGNAL_CALLBACK_DEC_HH
#define ELASTOS_CANCELLATIONSIGNAL_CALLBACK_DEC_HH


struct ElaCancellationSignalCallback
{
    Boolean (*elastos_CancellationSignal_isCanceled)(IInterface* obj);
    void (*elastos_CancellationSignal_throwIfCanceled)(IInterface* obj);
    void (*elastos_CancellationSignal_cancel)(IInterface* obj);
    void (*elastos_CancellationSignal_setOnCancelListener)(IInterface* obj, IInterface* p0);
    AutoPtr<IInterface> (*elastos_CancellationSignal_Constructor)();
};


#endif //ELASTOS_CANCELLATIONSIGNAL_CALLBACK_DEC_HH
