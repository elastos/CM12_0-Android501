// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file is autogenerated by
//     api_generator
// For
//     org/chromium/android_webview/AwContentsClientBridge

#ifndef ELASTOS_ORG_CHROMIUM_ANDROID_WEBVIEW_AWCONTENTSCLIENTBRIDGE_JNI
#define ELASTOS_ORG_CHROMIUM_ANDROID_WEBVIEW_AWCONTENTSCLIENTBRIDGE_JNI

//#include <jni.h>

#include "base/logging.h"
#include "ElAndroid.h"

//#include "base/android/jni_int_wrapper.h"

// Step 1: forward declarations.

namespace android_webview {

// Step 2: method stubs.

extern "C" {
__attribute__((visibility("default")))
void Elastos_AwContentsClientBridge_nativeProceedSslError(
    /* [in] */ IInterface* caller,
    /* [in] */ Elastos::Handle64 nativeAwContentsClientBridge,
    /* [in] */ Elastos::Boolean proceed,
    /* [in] */ Elastos::Int32 id) {
    AwContentsClientBridge* native = reinterpret_cast<AwContentsClientBridge*>(nativeAwContentsClientBridge);
    //CHECK_NATIVE_PTR(env, jcaller, native, "ProceedSslError");
    return native->ProceedSslError(caller, proceed, id);
}

__attribute__((visibility("default")))
void Elastos_AwContentsClientBridge_nativeProvideClientCertificateResponse(
    /* [in] */ IInterface* caller,
    /* [in] */ Elastos::Handle64 nativeAwContentsClientBridge,
    /* [in] */ Elastos::Int32 id,
    /* [in] */ Elastos::ArrayOf<Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > >* certChain,
    /* [in] */ IInterface* androidKey) {
    AwContentsClientBridge* native = reinterpret_cast<AwContentsClientBridge*>(nativeAwContentsClientBridge);
    //CHECK_NATIVE_PTR(env, jcaller, native, "ProvideClientCertificateResponse");
    return native->ProvideClientCertificateResponse(caller, id, certChain, androidKey);
}

__attribute__((visibility("default")))
void Elastos_AwContentsClientBridge_nativeConfirmJsResult(
    /* [in] */ IInterface* caller,
    /* [in] */ Elastos::Handle64 nativeAwContentsClientBridge,
    /* [in] */ Elastos::Int32 id,
    /* [in] */ const Elastos::String& prompt) {
    AwContentsClientBridge* native = reinterpret_cast<AwContentsClientBridge*>(nativeAwContentsClientBridge);
    //CHECK_NATIVE_PTR(env, jcaller, native, "ConfirmJsResult");
    return native->ConfirmJsResult(caller, id, prompt);
}

__attribute__((visibility("default")))
void Elastos_AwContentsClientBridge_nativeCancelJsResult(
    /* [in] */ IInterface* caller,
    /* [in] */ Elastos::Handle64 nativeAwContentsClientBridge,
    /* [in] */ Elastos::Int32 id) {
    AwContentsClientBridge* native = reinterpret_cast<AwContentsClientBridge*>(nativeAwContentsClientBridge);
    //CHECK_NATIVE_PTR(env, jcaller, native, "CancelJsResult");
    return native->CancelJsResult(caller, id);
}
};  // extern "C"

// Step 3: Callback init .
struct ElaAwContentsClientBridgeCallback
{
    void (*elastos_AwContentsClientBridge_setNativeContentsClientBridge)(IInterface* obj, Elastos::Handle64
        nativeContentsClientBridge);
    Elastos::Boolean (*elastos_AwContentsClientBridge_allowCertificateError)(IInterface* obj, Elastos::Int32 certError, Elastos::ArrayOf<Elastos::Byte>*
        derBytes, const Elastos::String& url, Elastos::Int32 id);
    void (*elastos_AwContentsClientBridge_selectClientCertificate)(IInterface* obj, Elastos::Int32 id, Elastos::ArrayOf<Elastos::String>*
        keyTypes, Elastos::ArrayOf<Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > >* encodedPrincipals, const Elastos::String& host, Elastos::Int32 port);
    void (*elastos_AwContentsClientBridge_handleJsAlert)(IInterface* obj, const Elastos::String& url, const
        Elastos::String& message, Elastos::Int32 id);
    void (*elastos_AwContentsClientBridge_handleJsConfirm)(IInterface* obj, const Elastos::String& url, const
        Elastos::String& message, Elastos::Int32 id);
    void (*elastos_AwContentsClientBridge_handleJsPrompt)(IInterface* obj, const Elastos::String& url, const
        Elastos::String& message, const Elastos::String& defaultValue, Elastos::Int32 id);
    void (*elastos_AwContentsClientBridge_handleJsBeforeUnload)(IInterface* obj, const Elastos::String& url, const
        Elastos::String& message, Elastos::Int32 id);
    Elastos::Boolean (*elastos_AwContentsClientBridge_shouldOverrideUrlLoading)(IInterface* obj, const Elastos::String& url);
};

extern "C" {
static struct ElaAwContentsClientBridgeCallback* sElaAwContentsClientBridgeCallback;
__attribute__((visibility("default")))
void Elastos_AwContentsClientBridge_InitCallback(Elastos::Handle64 cb)
{
    sElaAwContentsClientBridgeCallback = (struct ElaAwContentsClientBridgeCallback*)cb;
    DLOG(INFO) << "init pointer for sElaAwContentsClientBridgeCallback is:" << sElaAwContentsClientBridgeCallback;
}
}; // extern "C"

static bool RegisterNativesImpl() {

  return true;
}

}  // namespace android_webview

#endif  // ELASTOS_ORG_CHROMIUM_ANDROID_WEBVIEW_AWCONTENTSCLIENTBRIDGE_JNI
