// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file is autogenerated by
//     api_generator
// For
//     org/chromium/ui/gl/SurfaceTexturePlatformWrapper

#ifndef ELASTOS_ORG_CHROMIUM_UI_GL_SURFACETEXTUREPLATFORMWRAPPER_JNI
#define ELASTOS_ORG_CHROMIUM_UI_GL_SURFACETEXTUREPLATFORMWRAPPER_JNI

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/logging.h"

//#include "base/android/jni_int_wrapper.h"

// Step 1: forward declarations.

namespace gfx {

// Step 2: method stubs.

// Step 3: Callback init .
struct ElaSurfaceTexturePlatformWrapperCallback
{
    Elastos::AutoPtr<IInterface> (*elastos_SurfaceTexturePlatformWrapper_create)(Elastos::Int32 textureId);
    Elastos::AutoPtr<IInterface> (*elastos_SurfaceTexturePlatformWrapper_createSingleBuffered)(Elastos::Int32 textureId);
    void (*elastos_SurfaceTexturePlatformWrapper_destroy)(IInterface* surfaceTexture);
    void (*elastos_SurfaceTexturePlatformWrapper_setFrameAvailableCallback)(IInterface* surfaceTexture, Elastos::Int64
        nativeSurfaceTextureListener);
    void (*elastos_SurfaceTexturePlatformWrapper_updateTexImage)(IInterface* surfaceTexture);
    void (*elastos_SurfaceTexturePlatformWrapper_releaseTexImage)(IInterface* surfaceTexture);
    void (*elastos_SurfaceTexturePlatformWrapper_setDefaultBufferSize)(IInterface* surfaceTexture, Elastos::Int32 width, Elastos::Int32
        height);
    void (*elastos_SurfaceTexturePlatformWrapper_getTransformMatrix)(IInterface* surfaceTexture, Elastos::ArrayOf<Elastos::Float>*
        matrix);
    void (*elastos_SurfaceTexturePlatformWrapper_attachToGLContext)(IInterface* surfaceTexture, Elastos::Int32 texName);
    void (*elastos_SurfaceTexturePlatformWrapper_detachFromGLContext)(IInterface* surfaceTexture);
};

extern "C" {
static struct ElaSurfaceTexturePlatformWrapperCallback* sElaSurfaceTexturePlatformWrapperCallback;
__attribute__((visibility("default")))
void Elastos_SurfaceTexturePlatformWrapper_InitCallback(Elastos::Handle32 cb)
{
    sElaSurfaceTexturePlatformWrapperCallback = (struct ElaSurfaceTexturePlatformWrapperCallback*)cb;
    DLOG(INFO) << "init pointer for sElaSurfaceTexturePlatformWrapperCallback is:" <<
        sElaSurfaceTexturePlatformWrapperCallback;
}
}; // extern "C"

static bool RegisterNativesImpl() {

  return true;
}

}  // namespace gfx

#endif  // ELASTOS_ORG_CHROMIUM_UI_GL_SURFACETEXTUREPLATFORMWRAPPER_JNI
