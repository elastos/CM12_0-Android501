// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file is autogenerated by
//     api_generator
// For
//     org/chromium/ui/gfx/BitmapHelper

#ifndef ELASTOS_ORG_CHROMIUM_UI_GFX_BITMAPHELPER_JNI
#define ELASTOS_ORG_CHROMIUM_UI_GFX_BITMAPHELPER_JNI

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/logging.h"

//#include "base/android/jni_int_wrapper.h"

// Step 1: forward declarations.

namespace gfx {

// Step 2: method stubs.

// Step 3: Callback init .
struct ElaBitmapHelperCallback
{
    Elastos::AutoPtr<IInterface> (*elastos_BitmapHelper_createBitmap)(Elastos::Int32 width, Elastos::Int32 height, Elastos::Int32 bitmapFormatValue);
    Elastos::AutoPtr<IInterface> (*elastos_BitmapHelper_decodeDrawableResource)(const Elastos::String& name, Elastos::Int32 reqWidth,
        Elastos::Int32 reqHeight);
    Elastos::Int32 (*elastos_BitmapHelper_getBitmapFormatForConfig)(Elastos::Int32 bitmapConfig);
};

extern "C" {
static struct ElaBitmapHelperCallback* sElaBitmapHelperCallback;
__attribute__((visibility("default")))
void Elastos_BitmapHelper_InitCallback(Elastos::Handle32 cb)
{
    sElaBitmapHelperCallback = (struct ElaBitmapHelperCallback*)cb;
    DLOG(INFO) << "init pointer for sElaBitmapHelperCallback is:" << sElaBitmapHelperCallback;
}
}; // extern "C"

static bool RegisterNativesImpl() {

  return true;
}

}  // namespace gfx

#endif  // ELASTOS_ORG_CHROMIUM_UI_GFX_BITMAPHELPER_JNI
