// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/gfx/elastos/shared_device_display_info.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
#include "base/logging.h"
#include "ui/api/DeviceDisplayInfo_api.h"

namespace gfx {

// static JNI call
static void UpdateSharedDeviceDisplayInfo(IInterface* obj,
                                          Elastos::Int32 display_height,
                                          Elastos::Int32 display_width,
                                          Elastos::Int32 physical_display_height,
                                          Elastos::Int32 physical_display_width,
                                          Elastos::Int32 bits_per_pixel,
                                          Elastos::Int32 bits_per_component,
                                          Elastos::Double dip_scale,
                                          Elastos::Int32 smallest_dip_width,
                                          Elastos::Int32 rotation_degrees) {
  SharedDeviceDisplayInfo::GetInstance()->InvokeUpdate(obj,
      display_height, display_width,
      physical_display_height, physical_display_width,
      bits_per_pixel, bits_per_component,
      dip_scale, smallest_dip_width, rotation_degrees);
}

// static
SharedDeviceDisplayInfo* SharedDeviceDisplayInfo::GetInstance() {
  return Singleton<SharedDeviceDisplayInfo>::get();
}

int SharedDeviceDisplayInfo::GetDisplayHeight() {
  base::AutoLock autolock(lock_);
  DCHECK_NE(0, display_height_);
  return display_height_;
}

int SharedDeviceDisplayInfo::GetDisplayWidth() {
  base::AutoLock autolock(lock_);
  DCHECK_NE(0, display_width_);
  return display_width_;
}

int SharedDeviceDisplayInfo::GetPhysicalDisplayHeight() {
  base::AutoLock autolock(lock_);
  return physical_display_height_;
}

int SharedDeviceDisplayInfo::GetPhysicalDisplayWidth() {
  base::AutoLock autolock(lock_);
  return physical_display_width_;
}

int SharedDeviceDisplayInfo::GetBitsPerPixel() {
  base::AutoLock autolock(lock_);
  DCHECK_NE(0, bits_per_pixel_);
  return bits_per_pixel_;
}

int SharedDeviceDisplayInfo::GetBitsPerComponent() {
  base::AutoLock autolock(lock_);
  DCHECK_NE(0, bits_per_component_);
  return bits_per_component_;
}

double SharedDeviceDisplayInfo::GetDIPScale() {
  base::AutoLock autolock(lock_);
  DCHECK_NE(0, dip_scale_);
  return dip_scale_;
}

int SharedDeviceDisplayInfo::GetSmallestDIPWidth() {
  base::AutoLock autolock(lock_);
  DCHECK_NE(0, smallest_dip_width_);
  return smallest_dip_width_;
}

int SharedDeviceDisplayInfo::GetRotationDegrees() {
  base::AutoLock autolock(lock_);
  return rotation_degrees_;
}

// static
bool SharedDeviceDisplayInfo::RegisterSharedDeviceDisplayInfo() {
  return RegisterNativesImpl();
}

void SharedDeviceDisplayInfo::InvokeUpdate(IInterface* obj,
                                           Elastos::Int32 display_height,
                                           Elastos::Int32 display_width,
                                           Elastos::Int32 physical_display_height,
                                           Elastos::Int32 physical_display_width,
                                           Elastos::Int32 bits_per_pixel,
                                           Elastos::Int32 bits_per_component,
                                           Elastos::Double dip_scale,
                                           Elastos::Int32 smallest_dip_width,
                                           Elastos::Int32 rotation_degrees) {
  base::AutoLock autolock(lock_);

  UpdateDisplayInfo(obj,
      display_height, display_width,
      physical_display_height, physical_display_width,
      bits_per_pixel, bits_per_component, dip_scale,
      smallest_dip_width, rotation_degrees);
}

SharedDeviceDisplayInfo::SharedDeviceDisplayInfo()
    : display_height_(0),
      display_width_(0),
      bits_per_pixel_(0),
      bits_per_component_(0),
      dip_scale_(0),
      smallest_dip_width_(0) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaDeviceDisplayInfoCallback);
  //Java_DeviceDisplayInfo_create(
  j_device_info_ =
      sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_create(
          base::android::GetAppContext());
  UpdateDisplayInfo(j_device_info_.Get(),
          sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_getDisplayHeight(j_device_info_.Get()),
          sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_getDisplayWidth(j_device_info_.Get()),
          sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_getPhysicalDisplayHeight(j_device_info_.Get()),
          sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_getPhysicalDisplayWidth(j_device_info_.Get()),
          sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_getBitsPerPixel(j_device_info_.Get()),
          sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_getBitsPerComponent(j_device_info_.Get()),
          sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_getDIPScale(j_device_info_.Get()),
          sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_getSmallestDIPWidth(j_device_info_.Get()),
          sElaDeviceDisplayInfoCallback->elastos_DeviceDisplayInfo_getRotationDegrees(j_device_info_.Get())
      );
      //Java_DeviceDisplayInfo_getDisplayHeight(env, j_device_info_.obj()),
      //Java_DeviceDisplayInfo_getDisplayWidth(env, j_device_info_.obj()),
      //Java_DeviceDisplayInfo_getPhysicalDisplayHeight(env, j_device_info_.obj()),
      //Java_DeviceDisplayInfo_getPhysicalDisplayWidth(env, j_device_info_.obj()),
      //Java_DeviceDisplayInfo_getBitsPerPixel(env, j_device_info_.obj()),
      //Java_DeviceDisplayInfo_getBitsPerComponent(env, j_device_info_.obj()),
      //Java_DeviceDisplayInfo_getDIPScale(env, j_device_info_.obj()),
      //Java_DeviceDisplayInfo_getSmallestDIPWidth(env, j_device_info_.obj()),
      //Java_DeviceDisplayInfo_getRotationDegrees(env, j_device_info_.obj()));
}

SharedDeviceDisplayInfo::~SharedDeviceDisplayInfo() {
}

void SharedDeviceDisplayInfo::UpdateDisplayInfo(IInterface* jobj,
                                                Elastos::Int32 display_height,
                                                Elastos::Int32 display_width,
                                                Elastos::Int32 physical_display_height,
                                                Elastos::Int32 physical_display_width,
                                                Elastos::Int32 bits_per_pixel,
                                                Elastos::Int32 bits_per_component,
                                                Elastos::Double dip_scale,
                                                Elastos::Int32 smallest_dip_width,
                                                Elastos::Int32 rotation_degrees) {
  display_height_ = static_cast<int>(display_height);
  display_width_ = static_cast<int>(display_width);
  physical_display_height_ = static_cast<int>(physical_display_height);
  physical_display_width_ = static_cast<int>(physical_display_width);
  bits_per_pixel_ = static_cast<int>(bits_per_pixel);
  bits_per_component_ = static_cast<int>(bits_per_component);
  dip_scale_ = static_cast<double>(dip_scale);
  smallest_dip_width_ = static_cast<int>(smallest_dip_width);
  rotation_degrees_ = static_cast<int>(rotation_degrees);
}

}  // namespace gfx
