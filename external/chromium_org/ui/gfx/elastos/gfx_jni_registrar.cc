// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/gfx/elastos/gfx_jni_registrar.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "ui/gfx/elastos/java_bitmap.h"
#include "ui/gfx/elastos/shared_device_display_info.h"
#include "ui/gfx/elastos/view_configuration.h"

namespace gfx {
namespace android {

static base::android::RegistrationMethod kGfxRegisteredMethods[] = {
  { "SharedDeviceDisplayInfo",
      SharedDeviceDisplayInfo::RegisterSharedDeviceDisplayInfo },
  { "JavaBitmap", JavaBitmap::RegisterJavaBitmap },
  { "ViewConfiguration", ViewConfiguration::RegisterViewConfiguration }
};

bool RegisterJni() {
  return RegisterNativeMethods(kGfxRegisteredMethods,
                               arraysize(kGfxRegisteredMethods));
}

}  // namespace android
}  // namespace gfx
