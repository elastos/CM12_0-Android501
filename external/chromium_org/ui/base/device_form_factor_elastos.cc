// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/base/device_form_factor_elastos.h"

#include "base/elastos/api_elastos.h"
#include "ui/api/DeviceFormFactor_api.h"

namespace ui {

DeviceFormFactor GetDeviceFormFactor() {
  DCHECK(sElaDeviceFormFactorCallback);
  //bool is_tablet = Java_DeviceFormFactor_isTablet(
  //    base::android::AttachCurrentThread(),
  //    base::android::GetApplicationContext());
  bool is_tablet = sElaDeviceFormFactorCallback->elastos_DeviceFormFactor_isTablet(
          base::android::GetAppContext());
  return is_tablet ? DEVICE_FORM_FACTOR_TABLET : DEVICE_FORM_FACTOR_PHONE;
}

bool RegisterDeviceFormFactorAndroid() {
  return RegisterNativesImpl();
}

}  // namespace ui
