// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/base/elastos/ui_base_jni_registrar.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "ui/base/elastos/view_android.h"
#include "ui/base/elastos/window_android.h"
#include "ui/base/clipboard/clipboard_elastos_initialization.h"
#include "ui/base/device_form_factor_elastos.h"
#include "ui/base/l10n/l10n_util_elastos.h"
#include "ui/base/resource/resource_bundle_elastos.h"
#include "ui/base/touch/touch_device.h"

namespace ui {
namespace android {

static base::android::RegistrationMethod kUiRegisteredMethods[] = {
  { "Clipboard", RegisterClipboardAndroid },
  { "DeviceFormFactor", RegisterDeviceFormFactorAndroid },
  { "LocalizationUtils", l10n_util::RegisterLocalizationUtil },
  { "ResourceBundle", RegisterResourceBundleAndroid },
  { "TouchDevice", RegisterTouchDeviceAndroid },
  { "ViewAndroid", ViewAndroid::RegisterViewAndroid },
  { "WindowAndroid", WindowAndroid::RegisterWindowAndroid },
};

bool RegisterJni() {
   return RegisterNativeMethods(kUiRegisteredMethods,
                               arraysize(kUiRegisteredMethods));
}

}  // namespace android
}  // namespace ui
