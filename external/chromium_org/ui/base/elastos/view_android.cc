// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/base/elastos/view_android.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/api_weak_ref.h"
//#include "base/android/scoped_java_ref.h"
#include "ui/api/ViewAndroid_api.h"
#include "ui/base/elastos/window_android.h"

namespace ui {

//using base::android::AttachCurrentThread;
//using base::android::ScopedJavaLocalRef;

//ViewAndroid::ViewAndroid(JNIEnv* env, jobject obj, WindowAndroid* window)
ViewAndroid::ViewAndroid(IInterface* obj, WindowAndroid* window)
  : weak_java_view_(obj),
    window_android_(window) {}

void ViewAndroid::Destroy(IInterface* obj) {
  //DCHECK(obj && env->IsSameObject(weak_java_view_.get(env).obj(), obj));
  DCHECK(obj && obj == weak_java_view_.get().Get());
  delete this;
}

//ScopedJavaLocalRef<jobject> ViewAndroid::GetJavaObject() {
Elastos::AutoPtr<IInterface> ViewAndroid::GetJavaObject() {
  // It is mandatory to explicitly call destroy() before releasing the java
  // side object. This could be changed in future by adding CleanupReference
  // based destruct path;
  //return weak_java_view_.get(AttachCurrentThread());
  return weak_java_view_.get();
}

WindowAndroid* ViewAndroid::GetWindowAndroid() {
  return window_android_;
}

bool ViewAndroid::RegisterViewAndroid() {
  return RegisterNativesImpl();
}

ViewAndroid::~ViewAndroid() {
}

//jlong Init(JNIEnv* env, jobject obj, jlong window) {
Elastos::Handle64 Init(IInterface* obj, Elastos::Handle64 window) {
  ViewAndroid* view = new ViewAndroid(
      obj, reinterpret_cast<ui::WindowAndroid*>(window));
  return reinterpret_cast<intptr_t>(view);
}

}  // namespace ui
