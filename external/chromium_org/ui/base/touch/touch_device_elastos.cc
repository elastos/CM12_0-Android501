// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/base/touch/touch_device.h"
#include "base/elastos/api_elastos.h"
#include "ui/api/TouchDevice_api.h"

namespace ui {

bool IsTouchDevicePresent() {
  return true;
}

int MaxTouchPoints() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //jobject context = base::android::GetApplicationContext();
  Elastos::AutoPtr<IInterface> context = base::android::GetAppContext();
  //jint max_touch_points = Java_TouchDevice_maxTouchPoints(env, context);
  DCHECK(sElaTouchDeviceCallback);
  Elastos::Int32 max_touch_points = sElaTouchDeviceCallback->elastos_TouchDevice_maxTouchPoints(context);
  return static_cast<int>(max_touch_points);
}

bool RegisterTouchDeviceAndroid() {
  return RegisterNativesImpl();
}

}  // namespace ui
