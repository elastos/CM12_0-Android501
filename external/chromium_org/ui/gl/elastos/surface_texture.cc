// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/gl/elastos/surface_texture.h"

//#include <android/native_window_jni.h>
#include "elastos/native_window.h"

// TODO(boliu): Remove this include when we move off ICS.
#include "base/elastos/build_info.h"
#include "base/elastos/api_elastos.h"
#include "base/logging.h"
#include "ui/api/SurfaceTexturePlatformWrapper_api.h"
#include "base/elastos/api/NativeWindow_api.h"
#include "ui/gl/elastos/scoped_java_surface.h"
#include "ui/gl/elastos/surface_texture_listener.h"
#include "ui/gl/gl_bindings.h"

// TODO(boliu): Remove this method when when we move off ICS. See
// http://crbug.com/161864.
bool GlContextMethodsAvailable() {
  bool available = base::android::BuildInfo::GetInstance()->sdk_int() >= 16;
  if (!available)
    LOG(WARNING) << "Running on unsupported device: rendering may not work";
  return available;
}

namespace gfx {

scoped_refptr<SurfaceTexture> SurfaceTexture::Create(int texture_id) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaSurfaceTexturePlatformWrapperCallback);
  return new SurfaceTexture(
    sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_create(texture_id));
      //Java_SurfaceTexturePlatformWrapper_create(env, texture_id));
}

scoped_refptr<SurfaceTexture> SurfaceTexture::CreateSingleBuffered(
    int texture_id) {
  DCHECK(IsSingleBufferModeSupported());
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaSurfaceTexturePlatformWrapperCallback);
  return new SurfaceTexture(
    sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_createSingleBuffered(texture_id));
  //    Java_SurfaceTexturePlatformWrapper_createSingleBuffered(env, texture_id));
}

SurfaceTexture::SurfaceTexture(
    IInterface* j_surface_texture) {
    //const base::android::ScopedJavaLocalRef<jobject>& j_surface_texture) {
  //j_surface_texture_.Reset(j_surface_texture);
  j_surface_texture_ = j_surface_texture;
}

SurfaceTexture::~SurfaceTexture() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //Java_SurfaceTexturePlatformWrapper_destroy(env, j_surface_texture_.obj());
  DCHECK(sElaSurfaceTexturePlatformWrapperCallback);
  sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_destroy(j_surface_texture_.Get());
}

void SurfaceTexture::SetFrameAvailableCallback(
    const base::Closure& callback) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //Java_SurfaceTexturePlatformWrapper_setFrameAvailableCallback(env,
  DCHECK(sElaSurfaceTexturePlatformWrapperCallback);
    sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_setFrameAvailableCallback(
      j_surface_texture_.Get(),
      reinterpret_cast<intptr_t>(new SurfaceTextureListener(callback)));
}

void SurfaceTexture::UpdateTexImage() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //Java_SurfaceTexturePlatformWrapper_updateTexImage(env,
  DCHECK(sElaSurfaceTexturePlatformWrapperCallback);
  sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_updateTexImage(
                                                    j_surface_texture_.Get());
}

void SurfaceTexture::ReleaseTexImage() {
  DCHECK(IsSingleBufferModeSupported());
  //JNIEnv* env = base::android::AttachCurrentThread();
  //Java_SurfaceTexturePlatformWrapper_releaseTexImage(env,
  DCHECK(sElaSurfaceTexturePlatformWrapperCallback);
  sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_releaseTexImage(
                                                     j_surface_texture_.Get());
}

void SurfaceTexture::GetTransformMatrix(float mtx[16]) {
  //JNIEnv* env = base::android::AttachCurrentThread();

  DCHECK(sElaSurfaceTexturePlatformWrapperCallback);
  //base::android::ScopedJavaLocalRef<jfloatArray> jmatrix(
  //    env, env->NewFloatArray(16));
  Elastos::AutoPtr< Elastos::ArrayOf<Elastos::Float> > jmatrix = Elastos::ArrayOf<Elastos::Float>::Alloc(16);
  //Java_SurfaceTexturePlatformWrapper_getTransformMatrix(
  sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_getTransformMatrix(
      j_surface_texture_.Get(), jmatrix.Get());

  //jboolean is_copy;
  //jfloat* elements = env->GetFloatArrayElements(jmatrix.obj(), &is_copy);
  Elastos::Float* elements = jmatrix->GetPayload();
  for (int i = 0; i < 16; ++i) {
    mtx[i] = static_cast<float>(elements[i]);
  }
  //env->ReleaseFloatArrayElements(jmatrix.obj(), elements, JNI_ABORT);
}

void SurfaceTexture::SetDefaultBufferSize(int width, int height) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaSurfaceTexturePlatformWrapperCallback);

  if (width > 0 && height > 0) {
    //Java_SurfaceTexturePlatformWrapper_setDefaultBufferSize(env,
    sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_setDefaultBufferSize(
        j_surface_texture_.Get(), static_cast<Elastos::Int32>(width),
        static_cast<Elastos::Int32>(height));
  } else {
    LOG(WARNING) << "Not setting surface texture buffer size - "
                    "width or height is 0";
  }
}

void SurfaceTexture::AttachToGLContext() {
  if (GlContextMethodsAvailable()) {
    int texture_id;
    glGetIntegerv(GL_TEXTURE_BINDING_EXTERNAL_OES, &texture_id);
    DCHECK(texture_id);
    DCHECK(sElaSurfaceTexturePlatformWrapperCallback);
    //JNIEnv* env = base::android::AttachCurrentThread();
    //Java_SurfaceTexturePlatformWrapper_attachToGLContext(env,
    sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_attachToGLContext(
        j_surface_texture_.Get(), texture_id);
  }
}

void SurfaceTexture::DetachFromGLContext() {
  if (GlContextMethodsAvailable()) {
    //JNIEnv* env = base::android::AttachCurrentThread();
    //Java_SurfaceTexturePlatformWrapper_detachFromGLContext(env,
    DCHECK(sElaSurfaceTexturePlatformWrapperCallback);
    sElaSurfaceTexturePlatformWrapperCallback->elastos_SurfaceTexturePlatformWrapper_detachFromGLContext(
        j_surface_texture_.Get());
  }
}

ANativeWindow* SurfaceTexture::CreateSurface() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  ScopedJavaSurface surface(this);
  // Note: This ensures that any local references used by
  // ANativeWindow_fromSurface are released immediately. This is needed as a
  // workaround for https://code.google.com/p/android/issues/detail?id=68174
  //TODO see above comment,
  //base::android::ScopedJavaLocalFrame scoped_local_reference_frame(env);
  //ANativeWindow* native_window = ANativeWindow_fromSurface( surface.j_surface().Get());
  DCHECK(android::sElaNativeWindowCallback);
  ANativeWindow* native_window = android::sElaNativeWindowCallback->elastos_NativeWindow_GetFromSurface(surface.j_surface().Get());
  return native_window;
}

// static
bool SurfaceTexture::IsSingleBufferModeSupported() {
  return base::android::BuildInfo::GetInstance()->sdk_int() >= 19;
}

bool SurfaceTexture::RegisterSurfaceTexture() {
  return RegisterNativesImpl();
}

}  // namespace gfx
