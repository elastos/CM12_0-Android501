// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/gl/elastos/scoped_java_surface.h"

#include "base/logging.h"
#include "ui/api/Surface_api.h"
#include "ui/gl/elastos/surface_texture.h"

using namespace API_Surface;
namespace {

bool g_jni_initialized = false;

void RegisterNativesIfNeeded() {
  if (!g_jni_initialized) {
    API_Surface::RegisterNativesImpl();
    g_jni_initialized = true;
  }
}

}  // anonymous namespace

namespace gfx {

ScopedJavaSurface::ScopedJavaSurface() {
}

ScopedJavaSurface::ScopedJavaSurface(
   IInterface* surface)
    : auto_release_(true),
      is_protected_(false) {
          /*
  JNIEnv* env = base::android::AttachCurrentThread();
  RegisterNativesIfNeeded(env);
  DCHECK(env->IsInstanceOf(surface.obj(), Surface_clazz(env)));
  j_surface_.Reset(surface);
  */
  RegisterNativesIfNeeded();
  j_surface_ = surface;
}

ScopedJavaSurface::ScopedJavaSurface(
    const SurfaceTexture* surface_texture)
    : auto_release_(true),
      is_protected_(false) {
  DCHECK(sElaSurfaceCallback);
  //JNIEnv* env = base::android::AttachCurrentThread();
  RegisterNativesIfNeeded();
  //ScopedJavaLocalRef<jobject> tmp(JNI_Surface::Java_Surface_Constructor(
  //    env, surface_texture->j_surface_texture().obj()));
  //DCHECK(!tmp.is_null());
  //j_surface_.Reset(tmp);
  Elastos::AutoPtr<IInterface> tmp =
      sElaSurfaceCallback->elastos_Surface_Constructor(surface_texture->j_surface_texture().Get());
  DCHECK(tmp.Get());
  j_surface_ = tmp;
}

ScopedJavaSurface::ScopedJavaSurface(RValue rvalue) {
  MoveFrom(*rvalue.object);
}

ScopedJavaSurface& ScopedJavaSurface::operator=(RValue rhs) {
  MoveFrom(*rhs.object);
  return *this;
}

ScopedJavaSurface::~ScopedJavaSurface() {
  if (auto_release_ && j_surface_.Get()) {
    DCHECK(sElaSurfaceCallback);
    //JNIEnv* env = base::android::AttachCurrentThread();
    //JNI_Surface::Java_Surface_release(env, j_surface_.obj());
    sElaSurfaceCallback->elastos_Surface_release(j_surface_.Get());
  }
}

void ScopedJavaSurface::MoveFrom(ScopedJavaSurface& other) {
  DCHECK(sElaSurfaceCallback);
  //JNIEnv* env = base::android::AttachCurrentThread();
  //j_surface_.Reset(env, other.j_surface_.Release());
  j_surface_ = other.j_surface_;
  auto_release_ = other.auto_release_;
  is_protected_ = other.is_protected_;
}

bool ScopedJavaSurface::IsEmpty() const {
  //return j_surface_.is_null();
  return !j_surface_.Get();
}

// static
//ScopedJavaSurface ScopedJavaSurface::AcquireExternalSurface(jobject surface) {
ScopedJavaSurface ScopedJavaSurface::AcquireExternalSurface(IInterface* surface) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> surface_ref;
  //surface_ref.Reset(env, surface);
  Elastos::AutoPtr<IInterface> surface_ref(surface);
  gfx::ScopedJavaSurface scoped_surface(surface_ref);
  scoped_surface.auto_release_ = false;
  scoped_surface.is_protected_ = true;
  return scoped_surface;
}

}  // namespace gfx
