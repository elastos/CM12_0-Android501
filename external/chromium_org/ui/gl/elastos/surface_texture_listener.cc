// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/gl/elastos/surface_texture_listener.h"

#include "base/location.h"
#include "base/message_loop/message_loop_proxy.h"
#include "ui/api/SurfaceTextureListener_api.h"

namespace gfx {

SurfaceTextureListener::SurfaceTextureListener(const base::Closure& callback)
    : callback_(callback),
      browser_loop_(base::MessageLoopProxy::current()) {
}

SurfaceTextureListener::~SurfaceTextureListener() {
}

//void SurfaceTextureListener::Destroy(JNIEnv* env, jobject obj) {
void SurfaceTextureListener::Destroy(IInterface* obj) {
  delete this;
}

//void SurfaceTextureListener::FrameAvailable(JNIEnv* env, jobject obj) {
void SurfaceTextureListener::FrameAvailable(IInterface* obj) {
  if (!browser_loop_->BelongsToCurrentThread()) {
    browser_loop_->PostTask(FROM_HERE, callback_);
  } else {
    callback_.Run();
  }
}

// static
bool SurfaceTextureListener::RegisterSurfaceTextureListener() {
  return RegisterNativesImpl();
}

}  // namespace gfx
