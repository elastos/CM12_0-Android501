// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/gl/elastos/gl_jni_registrar.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "ui/gl/elastos/surface_texture.h"
#include "ui/gl/elastos/surface_texture_listener.h"

namespace ui {
namespace gl {
namespace android {

static base::android::RegistrationMethod kGLRegisteredMethods[] = {
  { "SurfaceTexture",
    gfx::SurfaceTexture::RegisterSurfaceTexture },
  { "SurfaceTextureListener",
    gfx::SurfaceTextureListener::RegisterSurfaceTextureListener },
};

bool RegisterJni() {
  return RegisterNativeMethods(kGLRegisteredMethods,
                               arraysize(kGLRegisteredMethods));
}

}  // namespace android
}  // namespace gl
}  // namespace ui
