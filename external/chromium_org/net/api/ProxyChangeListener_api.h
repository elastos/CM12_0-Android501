// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file is autogenerated by
//     api_generator
// For
//     org/chromium/net/ProxyChangeListener

#ifndef ELASTOS_ORG_CHROMIUM_NET_PROXYCHANGELISTENER_JNI
#define ELASTOS_ORG_CHROMIUM_NET_PROXYCHANGELISTENER_JNI

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/logging.h"

//#include "base/android/jni_int_wrapper.h"

// Step 1: forward declarations.

namespace net {

// Step 2: method stubs.

extern "C" {
__attribute__((visibility("default")))
void Elastos_ProxyChangeListener_nativeProxySettingsChangedTo(
    /* [in] */ IInterface* caller,
    /* [in] */ Elastos::Handle64 nativePtr,
    /* [in] */ const Elastos::String& host,
    /* [in] */ Elastos::Int32 port) {
    ProxyConfigServiceAndroid::JNIDelegate* native =
        reinterpret_cast<ProxyConfigServiceAndroid::JNIDelegate*>(nativePtr);
    //CHECK_NATIVE_PTR(env, jcaller, native, "ProxySettingsChangedTo");
    return native->ProxySettingsChangedTo(caller, host, port);
}

__attribute__((visibility("default")))
void Elastos_ProxyChangeListener_nativeProxySettingsChanged(
    /* [in] */ IInterface* caller,
    /* [in] */ Elastos::Handle64 nativePtr) {
    ProxyConfigServiceAndroid::JNIDelegate* native =
        reinterpret_cast<ProxyConfigServiceAndroid::JNIDelegate*>(nativePtr);
    //CHECK_NATIVE_PTR(env, jcaller, native, "ProxySettingsChanged");
    return native->ProxySettingsChanged(caller);
}
};  // extern "C"

// Step 3: Callback init .
struct ElaProxyChangeListenerCallback
{
    Elastos::AutoPtr<IInterface> (*elastos_ProxyChangeListener_create)(IInterface* context);
    Elastos::String (*elastos_ProxyChangeListener_getProperty)(const Elastos::String& property);
    void (*elastos_ProxyChangeListener_start)(IInterface* obj, Elastos::Handle64 nativePtr);
    void (*elastos_ProxyChangeListener_stop)(IInterface* obj);
};

extern "C" {
static struct ElaProxyChangeListenerCallback* sElaProxyChangeListenerCallback;
__attribute__((visibility("default")))
void Elastos_ProxyChangeListener_InitCallback(Elastos::Handle64 cb)
{
    sElaProxyChangeListenerCallback = (struct ElaProxyChangeListenerCallback*)cb;
    DLOG(INFO) << "init pointer for sElaProxyChangeListenerCallback is:" << sElaProxyChangeListenerCallback;
}
}; // extern "C"

static bool RegisterNativesImpl() {

  return true;
}

}  // namespace net

#endif  // ELASTOS_ORG_CHROMIUM_NET_PROXYCHANGELISTENER_JNI
