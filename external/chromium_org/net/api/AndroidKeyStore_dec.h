//This file is autogenerated for
//    AndroidKeyStore.java
//put this file at the end of the include list
//so the type definition used in this file will be found
#ifndef ELASTOS_ANDROIDKEYSTORE_CALLBACK_DEC_HH
#define ELASTOS_ANDROIDKEYSTORE_CALLBACK_DEC_HH


struct ElaAndroidKeyStoreCallback
{
    AutoPtr<ArrayOf<Byte> > (*elastos_AndroidKeyStore_getRSAKeyModulus)(IInterface* obj, IInterface* key);
    AutoPtr<ArrayOf<Byte> > (*elastos_AndroidKeyStore_getDSAKeyParamQ)(IInterface* obj, IInterface* key);
    AutoPtr<ArrayOf<Byte> > (*elastos_AndroidKeyStore_getECKeyOrder)(IInterface* obj, IInterface* key);
    AutoPtr<ArrayOf<Byte> > (*elastos_AndroidKeyStore_getPrivateKeyEncodedBytes)(IInterface* obj, IInterface* key);
    AutoPtr<ArrayOf<Byte> > (*elastos_AndroidKeyStore_rawSignDigestWithPrivateKey)(IInterface* obj, IInterface* key, ArrayOf<Byte>* message);
    Int32 (*elastos_AndroidKeyStore_getPrivateKeyType)(IInterface* obj, IInterface* key);
    Int64 (*elastos_AndroidKeyStore_getOpenSSLHandleForPrivateKey)(IInterface* obj, IInterface* key);
    void (*elastos_AndroidKeyStore_releaseKey)(IInterface* obj, IInterface* key);
};


#endif //ELASTOS_ANDROIDKEYSTORE_CALLBACK_DEC_HH
