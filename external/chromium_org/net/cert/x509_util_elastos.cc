// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/cert/x509_util_elastos.h"

#include "base/elastos/build_info.h"
#include "base/elastos/api_elastos.h"
#include "base/metrics/histogram.h"
#include "net/api/X509Util_api.h"
#include "net/cert/cert_database.h"

namespace net {

void NotifyKeyChainChanged() {
  CertDatabase::GetInstance()->OnAndroidKeyChainChanged();
}

void RecordCertVerifyCapabilitiesHistogram(Elastos::Boolean found_system_trust_roots) {
  // Only record the histogram for 4.2 and up. Before 4.2, the platform doesn't
  // return the certificate chain anyway.
  if (base::android::BuildInfo::GetInstance()->sdk_int() >= 17) {
    UMA_HISTOGRAM_BOOLEAN("Net.FoundSystemTrustRootsAndroid",
                          found_system_trust_roots);
  }
}

Elastos::AutoPtr<IInterface> GetApplicationContext() {
  return base::android::GetAppContext();
}

bool RegisterX509Util() {
  return RegisterNativesImpl();
}

}  // net namespace
