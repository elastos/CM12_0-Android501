// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/elastos/net_jni_registrar.h"

#include "base/basictypes.h"
#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "net/elastos/android_private_key.h"
#include "net/elastos/gurl_utils.h"
#include "net/elastos/keystore.h"
#include "net/elastos/network_change_notifier_android.h"
#include "net/elastos/network_library.h"
#include "net/cert/x509_util_elastos.h"
#include "net/proxy/proxy_config_service_elastos.h"

#if defined(USE_ICU_ALTERNATIVES_ON_ANDROID)
#include "net/base/net_string_util_icu_alternatives_elastos.h"
#endif

namespace net {
namespace android {

static base::android::RegistrationMethod kNetRegisteredMethods[] = {
  { "AndroidCertVerifyResult", net::android::RegisterCertVerifyResult },
  { "AndroidPrivateKey", net::android::RegisterAndroidPrivateKey},
  { "AndroidKeyStore", net::android::RegisterKeyStore },
  { "AndroidNetworkLibrary", net::android::RegisterNetworkLibrary },
  { "GURLUtils", net::RegisterGURLUtils },
  { "NetworkChangeNotifierAndroid",
    net::NetworkChangeNotifierAndroid::Register },
  { "ProxyConfigService", net::ProxyConfigServiceAndroid::Register },
  { "X509Util", net::RegisterX509Util },
#if defined(USE_ICU_ALTERNATIVES_ON_ANDROID)
  { "NetStringUtils", net::RegisterNetStringUtils }
#endif
};

bool RegisterJni() {
  return base::android::RegisterNativeMethods(
      kNetRegisteredMethods, arraysize(kNetRegisteredMethods));
}

}  // namespace android
}  // namespace net
