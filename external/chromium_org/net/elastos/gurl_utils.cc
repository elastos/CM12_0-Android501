// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/elastos/gurl_utils.h"

#include "base/elastos/jni_string.h"
#include "net/api/GURLUtils_api.h"
#include "url/gurl.h"

namespace net {

//jstring GetOrigin(JNIEnv* env, jclass clazz, jstring url) {
Elastos::String GetOrigin(const Elastos::String& url) {
  GURL host(base::android::ConvertJavaStringToUTF16(url));

  return base::android::ConvertUTF8ToJavaString(host.GetOrigin().spec());
}

//jstring GetScheme(JNIEnv* env, jclass clazz, jstring url) {
Elastos::String GetScheme(const Elastos::String& url) {
  GURL host(base::android::ConvertJavaStringToUTF16(url));

  return base::android::ConvertUTF8ToJavaString(host.scheme());
}

bool RegisterGURLUtils() {
  return RegisterNativesImpl();
}

}  // net namespace
