// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/elastos/cert_verify_result_android.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_array.h"
#include "net/api/AndroidCertVerifyResult_api.h"

//using base::android::AttachCurrentThread;
using base::android::JavaArrayOfByteArrayToStringVector;

namespace net {
namespace android {

void ExtractCertVerifyResult(IInterface* result,
                             CertVerifyStatusAndroid* status,
                             bool* is_issued_by_known_root,
                             std::vector<std::string>* verified_chain) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAndroidCertVerifyResultCallback);

  *status = static_cast<CertVerifyStatusAndroid>(
    sElaAndroidCertVerifyResultCallback->elastos_AndroidCertVerifyResult_getStatus(result));
      //Java_AndroidCertVerifyResult_getStatus(env, result));

  *is_issued_by_known_root =
      sElaAndroidCertVerifyResultCallback->elastos_AndroidCertVerifyResult_isIssuedByKnownRoot(result);
      //Java_AndroidCertVerifyResult_isIssuedByKnownRoot(env, result);

  //ScopedJavaLocalRef<jobjectArray> chain_byte_array =
  //    Java_AndroidCertVerifyResult_getCertificateChainEncoded(env, result);
   Elastos::AutoPtr<Elastos::ArrayOf<Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > > > chain_byte_array =
       sElaAndroidCertVerifyResultCallback->elastos_AndroidCertVerifyResult_getCertificateChainEncoded(result);
  JavaArrayOfByteArrayToStringVector(
      chain_byte_array.Get(), verified_chain);
}

bool RegisterCertVerifyResult() {
  return RegisterNativesImpl();
}

}  // namespace android
}  // namespace net
