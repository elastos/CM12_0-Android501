// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/elastos/keystore.h"

#include <vector>

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_array.h"
#include "base/logging.h"
#include "net/api/AndroidKeyStore_api.h"
#include "net/elastos/android_private_key.h"

//using base::android::AttachCurrentThread;
//using base::android::HasException;
using base::android::JavaByteArrayToByteVector;
//using base::android::ScopedJavaLocalRef;
using base::android::ToJavaByteArray;
using base::android::JavaArrayOfByteArrayToStringVector;

namespace net {
namespace android {

bool GetRSAKeyModulus(
    IInterface* private_key_ref,
    std::vector<uint8>* result) {
  //JNIEnv* env = AttachCurrentThread();

  //ScopedJavaLocalRef<jbyteArray> modulus_ref =
  //    Java_AndroidKeyStore_getRSAKeyModulus(env,
  DCHECK(sElaAndroidKeyStoreCallback);
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > modulus_ref =
  sElaAndroidKeyStoreCallback->elastos_AndroidKeyStore_getRSAKeyModulus(
                                            GetKeyStore(private_key_ref).Get(),
                                            private_key_ref);
  if (!modulus_ref.Get())
    return false;

  JavaByteArrayToByteVector(modulus_ref.Get(), result);
  return true;
}

bool GetDSAKeyParamQ(IInterface* private_key_ref,
                     std::vector<uint8>* result) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jbyteArray> q_ref =
  //    Java_AndroidKeyStore_getDSAKeyParamQ( env,
  DCHECK(sElaAndroidKeyStoreCallback);
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > q_ref =
  sElaAndroidKeyStoreCallback->elastos_AndroidKeyStore_getDSAKeyParamQ(
          GetKeyStore(private_key_ref).Get(),
          private_key_ref);
  if (!q_ref.Get())
    return false;

  JavaByteArrayToByteVector(q_ref.Get(), result);
  return true;
}

bool GetECKeyOrder(IInterface* private_key_ref,
                   std::vector<uint8>* result) {
  //JNIEnv* env = AttachCurrentThread();

  //ScopedJavaLocalRef<jbyteArray> order_ref =
  //    Java_AndroidKeyStore_getECKeyOrder( env,
  DCHECK(sElaAndroidKeyStoreCallback);
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > order_ref =
    sElaAndroidKeyStoreCallback->elastos_AndroidKeyStore_getECKeyOrder(
          GetKeyStore(private_key_ref).Get(),
          private_key_ref);

  if (!order_ref.Get())
    return false;

  JavaByteArrayToByteVector(order_ref.Get(), result);
  return true;
}

bool GetPrivateKeyEncodedBytes(IInterface* private_key_ref,
                               std::vector<uint8>* result) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jbyteArray> encoded_ref =
  //    Java_AndroidKeyStore_getPrivateKeyEncodedBytes( env,
  DCHECK(sElaAndroidKeyStoreCallback);
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > encoded_ref =
      sElaAndroidKeyStoreCallback->elastos_AndroidKeyStore_getPrivateKeyEncodedBytes(
          GetKeyStore(private_key_ref).Get(),
          private_key_ref);
  if (!encoded_ref.Get())
    return false;

  JavaByteArrayToByteVector(encoded_ref.Get(), result);
  return true;
}

bool RawSignDigestWithPrivateKey(
    IInterface* private_key_ref,
    const base::StringPiece& digest,
    std::vector<uint8>* signature) {
  //JNIEnv* env = AttachCurrentThread();

  // Convert message to byte[] array.
  //ScopedJavaLocalRef<jbyteArray> digest_ref =
  //    ToJavaByteArray(env,
  DCHECK(sElaAndroidKeyStoreCallback);
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > digest_ref =
      ToJavaByteArray(reinterpret_cast<const uint8*>(digest.data()),
                      digest.length());
  DCHECK(digest_ref.Get());

  // Invoke platform API
  //ScopedJavaLocalRef<jbyteArray> signature_ref =
  //    Java_AndroidKeyStore_rawSignDigestWithPrivateKey( env,
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > signature_ref =
      sElaAndroidKeyStoreCallback->elastos_AndroidKeyStore_rawSignDigestWithPrivateKey(
          GetKeyStore(private_key_ref).Get(),
          private_key_ref,
          digest_ref.Get());
  //if (HasException(env) || signature_ref.is_null())
  if (signature_ref.Get())
    return false;

  // Write signature to string.
  JavaByteArrayToByteVector(signature_ref.Get(), signature);
  return true;
}

PrivateKeyType GetPrivateKeyType(IInterface* private_key_ref) {
  //JNIEnv* env = AttachCurrentThread();
  //int type = Java_AndroidKeyStore_getPrivateKeyType( env,
  DCHECK(sElaAndroidKeyStoreCallback);
  int type = sElaAndroidKeyStoreCallback->elastos_AndroidKeyStore_getPrivateKeyType(
      GetKeyStore(private_key_ref).Get(),
      private_key_ref);
  return static_cast<PrivateKeyType>(type);
}

EVP_PKEY* GetOpenSSLSystemHandleForPrivateKey(IInterface* private_key_ref) {
  //JNIEnv* env = AttachCurrentThread();
  // Note: the pointer is passed as a jint here because that's how it
  // is stored in the Java object. Java doesn't have a primitive type
  // like intptr_t that matches the size of pointers on the host
  // machine, and Android only runs on 32-bit CPUs.
  //
  // Given that this routine shall only be called on Android < 4.2,
  // this won't be a problem in the far future (e.g. when Android gets
  // ported to 64-bit environments, if ever).
  //long pkey = Java_AndroidKeyStore_getOpenSSLHandleForPrivateKey( env,
  DCHECK(sElaAndroidKeyStoreCallback);
  long pkey = sElaAndroidKeyStoreCallback->elastos_AndroidKeyStore_getOpenSSLHandleForPrivateKey(
      GetKeyStore(private_key_ref).Get(),
      private_key_ref);
  return reinterpret_cast<EVP_PKEY*>(pkey);
}

void ReleaseKey(IInterface* private_key_ref) {
  //JNIEnv* env = AttachCurrentThread();
  //Java_AndroidKeyStore_releaseKey(env,
  DCHECK(sElaAndroidKeyStoreCallback);
  sElaAndroidKeyStoreCallback->elastos_AndroidKeyStore_releaseKey(
                                  GetKeyStore(private_key_ref).Get(),
                                  private_key_ref);
  //env->DeleteGlobalRef(private_key_ref);
  private_key_ref->Release();
}

bool RegisterKeyStore() {
  return RegisterNativesImpl();
}

}  // namespace android
}  // namespace net
