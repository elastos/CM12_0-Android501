// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/elastos/network_library.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_array.h"
#include "base/elastos/jni_string.h"
//#include "base/android/scoped_java_ref.h"
#include "base/logging.h"
#include "net/api/AndroidNetworkLibrary_api.h"

//using base::android::AttachCurrentThread;
using base::android::ConvertJavaStringToUTF8;
using base::android::ConvertUTF8ToJavaString;
using base::android::GetAppContext;
//using base::android::ScopedJavaLocalRef;
using base::android::ToJavaArrayOfByteArray;
using base::android::ToJavaByteArray;

namespace net {
namespace android {

void VerifyX509CertChain(const std::vector<std::string>& cert_chain,
                         const std::string& auth_type,
                         const std::string& host,
                         CertVerifyStatusAndroid* status,
                         bool* is_issued_by_known_root,
                         std::vector<std::string>* verified_chain) {
  //JNIEnv* env = AttachCurrentThread();

  //ScopedJavaLocalRef<jobjectArray> chain_byte_array =
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > > > chain_byte_array =
      ToJavaArrayOfByteArray(cert_chain);
  //DCHECK(!chain_byte_array.is_null());
  DCHECK(chain_byte_array.Get());

  //ScopedJavaLocalRef<jstring> auth_string =
  Elastos::String auth_string =
      ConvertUTF8ToJavaString(auth_type);
  DCHECK(!auth_string.IsNullOrEmpty());

  //ScopedJavaLocalRef<jstring> host_string =
  Elastos::String host_string =
      ConvertUTF8ToJavaString(host);
  DCHECK(!host_string.IsNullOrEmpty());

  //ScopedJavaLocalRef<jobject> result = Java_AndroidNetworkLibrary_verifyServerCertificates(
  Elastos::AutoPtr<IInterface> result =
      sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_verifyServerCertificates(
          chain_byte_array.Get(), auth_string, host_string);

  ExtractCertVerifyResult(result.Get(),
                          status, is_issued_by_known_root, verified_chain);
}

void AddTestRootCertificate(const uint8* cert, size_t len) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jbyteArray> cert_array = ToJavaByteArray(env, cert, len);
  //DCHECK(!cert_array.is_null());
  DCHECK(sElaAndroidNetworkLibraryCallback);
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > cert_array = ToJavaByteArray(cert, len);
  DCHECK(cert_array.Get());
  //Java_AndroidNetworkLibrary_addTestRootCertificate(env, cert_array.obj());
  sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_addTestRootCertificate(cert_array.Get());
}

void ClearTestRootCertificates() {
  //JNIEnv* env = AttachCurrentThread();
  //Java_AndroidNetworkLibrary_clearTestRootCertificates(env);
  DCHECK(sElaAndroidNetworkLibraryCallback);
  sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_clearTestRootCertificates();
}

bool StoreKeyPair(const uint8* public_key,
                  size_t public_len,
                  const uint8* private_key,
                  size_t private_len) {
  //JNIEnv* env = AttachCurrentThread();
  DCHECK(sElaAndroidNetworkLibraryCallback);
  //ScopedJavaLocalRef<jbyteArray> public_array =
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > public_array = ToJavaByteArray(public_key, public_len);
  //ScopedJavaLocalRef<jbyteArray> private_array =
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > private_array = ToJavaByteArray(private_key, private_len);
  //jboolean ret = Java_AndroidNetworkLibrary_storeKeyPair(env,
  Elastos::Boolean ret = sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_storeKeyPair(
      GetAppContext(), public_array.Get(), private_array.Get());
  LOG_IF(WARNING, !ret) <<
      "Call to Java_AndroidNetworkLibrary_storeKeyPair failed";
  return ret;
}

void StoreCertificate(net::CertificateMimeType cert_type,
                      const void* data,
                      size_t data_len) {
  DCHECK(sElaAndroidNetworkLibraryCallback);
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jbyteArray> data_array =
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::Byte> > data_array =
      ToJavaByteArray(reinterpret_cast<const uint8*>(data), data_len);
  //jboolean ret = Java_AndroidNetworkLibrary_storeCertificate(env,
  Elastos::Boolean ret = sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_storeCertificate(
      GetAppContext(), cert_type, data_array.Get());
  LOG_IF(WARNING, !ret) <<
      "Call to Java_AndroidNetworkLibrary_storeCertificate"
      " failed";
  // Intentionally do not return 'ret', there is little the caller can
  // do in case of failure (the CertInstaller itself will deal with
  // incorrect data and display the appropriate toast).
}

bool HaveOnlyLoopbackAddresses() {
  DCHECK(sElaAndroidNetworkLibraryCallback);
  //JNIEnv* env = AttachCurrentThread();
  //return Java_AndroidNetworkLibrary_haveOnlyLoopbackAddresses(env);
  return sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_haveOnlyLoopbackAddresses();
}

std::string GetNetworkList() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jstring> ret =
  //    Java_AndroidNetworkLibrary_getNetworkList(env);
  DCHECK(sElaAndroidNetworkLibraryCallback);
  Elastos::String ret =
        sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_getNetworkList();
  return ConvertJavaStringToUTF8(ret);
}

bool GetMimeTypeFromExtension(const std::string& extension,
                              std::string* result) {
  DCHECK(sElaAndroidNetworkLibraryCallback);
  //JNIEnv* env = AttachCurrentThread();

  //ScopedJavaLocalRef<jstring> extension_string =
  Elastos::String extension_string =
      ConvertUTF8ToJavaString(extension);
  //ScopedJavaLocalRef<jstring> ret =
  //    Java_AndroidNetworkLibrary_getMimeTypeFromExtension(
  Elastos::String ret =
      sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_getMimeTypeFromExtension(
          extension_string);

  if (ret.IsNullOrEmpty())
    return false;
  *result = ConvertJavaStringToUTF8(ret);
  return true;
}

std::string GetTelephonyNetworkCountryIso() {
  DCHECK(sElaAndroidNetworkLibraryCallback);
  return base::android::ConvertJavaStringToUTF8(
          sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_getNetworkCountryIso(
              base::android::GetAppContext())
          );
      //Java_AndroidNetworkLibrary_getNetworkCountryIso(
      //    base::android::AttachCurrentThread(),
      //    base::android::GetApplicationContext()));
}

std::string GetTelephonyNetworkOperator() {
  DCHECK(sElaAndroidNetworkLibraryCallback);
  return base::android::ConvertJavaStringToUTF8(
          sElaAndroidNetworkLibraryCallback->elastos_AndroidNetworkLibrary_getNetworkOperator(
              base::android::GetAppContext())
          );
      //Java_AndroidNetworkLibrary_getNetworkOperator(
      //    base::android::AttachCurrentThread(),
      //    base::android::GetApplicationContext()));
}

bool RegisterNetworkLibrary() {
  return RegisterNativesImpl();
}

}  // namespace android
}  // namespace net
