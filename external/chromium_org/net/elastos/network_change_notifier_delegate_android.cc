// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/elastos/network_change_notifier_delegate_android.h"

#include "base/logging.h"
#include "net/api/NetworkChangeNotifier_api.h"

namespace net {

namespace {

// Converts a Java side connection type (integer) to
// the native side NetworkChangeNotifier::ConnectionType.
NetworkChangeNotifier::ConnectionType ConvertConnectionType(
    Elastos::Int32 connection_type) {
  switch (connection_type) {
    case NetworkChangeNotifier::CONNECTION_UNKNOWN:
    case NetworkChangeNotifier::CONNECTION_ETHERNET:
    case NetworkChangeNotifier::CONNECTION_WIFI:
    case NetworkChangeNotifier::CONNECTION_2G:
    case NetworkChangeNotifier::CONNECTION_3G:
    case NetworkChangeNotifier::CONNECTION_4G:
    case NetworkChangeNotifier::CONNECTION_NONE:
    case NetworkChangeNotifier::CONNECTION_BLUETOOTH:
      break;
    default:
      NOTREACHED() << "Unknown connection type received: " << connection_type;
      return NetworkChangeNotifier::CONNECTION_UNKNOWN;
  }
  return static_cast<NetworkChangeNotifier::ConnectionType>(connection_type);
}

}  // namespace

NetworkChangeNotifierDelegateAndroid::NetworkChangeNotifierDelegateAndroid()
    : observers_(new ObserverListThreadSafe<Observer>()) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaNetworkChangeNotifierCallback);
  //java_network_change_notifier_.Reset(Java_NetworkChangeNotifier_init(
  java_network_change_notifier_ =
      sElaNetworkChangeNotifierCallback->elastos_NetworkChangeNotifier_init(
          base::android::GetAppContext());
  //Java_NetworkChangeNotifier_addNativeObserver(
  sElaNetworkChangeNotifierCallback->elastos_NetworkChangeNotifier_addNativeObserver(
      java_network_change_notifier_.Get(),
      reinterpret_cast<intptr_t>(this));
  SetCurrentConnectionType(
      ConvertConnectionType(
          sElaNetworkChangeNotifierCallback->elastos_NetworkChangeNotifier_getCurrentConnectionType(
              java_network_change_notifier_.Get())));
          //Java_NetworkChangeNotifier_getCurrentConnectionType(env, java_network_change_notifier_.obj())));
}

NetworkChangeNotifierDelegateAndroid::~NetworkChangeNotifierDelegateAndroid() {
  DCHECK(thread_checker_.CalledOnValidThread());
  observers_->AssertEmpty();
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaNetworkChangeNotifierCallback);
  //Java_NetworkChangeNotifier_removeNativeObserver(
  sElaNetworkChangeNotifierCallback->elastos_NetworkChangeNotifier_removeNativeObserver(
      java_network_change_notifier_.Get(),
      reinterpret_cast<intptr_t>(this));
}

NetworkChangeNotifier::ConnectionType
NetworkChangeNotifierDelegateAndroid::GetCurrentConnectionType() const {
  base::AutoLock auto_lock(connection_type_lock_);
  return connection_type_;
}

void NetworkChangeNotifierDelegateAndroid::NotifyConnectionTypeChanged(
    IInterface* obj,
    Elastos::Int32 new_connection_type) {
  DCHECK(thread_checker_.CalledOnValidThread());
  const ConnectionType actual_connection_type = ConvertConnectionType(
      new_connection_type);
  SetCurrentConnectionType(actual_connection_type);
  observers_->Notify(&Observer::OnConnectionTypeChanged);
}

Elastos::Int32 NetworkChangeNotifierDelegateAndroid::GetConnectionType(IInterface*) const {
  DCHECK(thread_checker_.CalledOnValidThread());
  return GetCurrentConnectionType();
}

void NetworkChangeNotifierDelegateAndroid::AddObserver(
    Observer* observer) {
  observers_->AddObserver(observer);
}

void NetworkChangeNotifierDelegateAndroid::RemoveObserver(
    Observer* observer) {
  observers_->RemoveObserver(observer);
}

// static
bool NetworkChangeNotifierDelegateAndroid::Register() {
  return RegisterNativesImpl();
}

void NetworkChangeNotifierDelegateAndroid::SetCurrentConnectionType(
    ConnectionType new_connection_type) {
  base::AutoLock auto_lock(connection_type_lock_);
  connection_type_ = new_connection_type;
}

void NetworkChangeNotifierDelegateAndroid::SetOnline() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  DCHECK(sElaNetworkChangeNotifierCallback);
  //Java_NetworkChangeNotifier_forceConnectivityState(env, true);
  sElaNetworkChangeNotifierCallback->elastos_NetworkChangeNotifier_forceConnectivityState(true);
}

void NetworkChangeNotifierDelegateAndroid::SetOffline() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //Java_NetworkChangeNotifier_forceConnectivityState(env, false);
  DCHECK(sElaNetworkChangeNotifierCallback);
  sElaNetworkChangeNotifierCallback->elastos_NetworkChangeNotifier_forceConnectivityState(false);
}

}  // namespace net
