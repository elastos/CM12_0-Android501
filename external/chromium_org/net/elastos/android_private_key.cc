// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/elastos/android_private_key.h"

#include <vector>
#include "net/api/AndroidPrivateKey_api.h"

namespace net {
namespace android {

//base::android::ScopedJavaLocalRef<jobject> GetKeyStore(
Elastos::AutoPtr<IInterface> GetKeyStore(
    IInterface* private_key_ref) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //return Java_AndroidPrivateKey_getKeyStore(env, private_key_ref);
  DCHECK(sElaAndroidPrivateKeyCallback);
  return sElaAndroidPrivateKeyCallback->elastos_AndroidPrivateKey_getKeyStore(private_key_ref);
}

bool RegisterAndroidPrivateKey() {
  return RegisterNativesImpl();
}

}  // namespace android
}  // namespace net
