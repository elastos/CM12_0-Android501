// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "components/autofill/core/browser/elastos/component_jni_registrar.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "components/autofill/core/browser/elastos/auxiliary_profile_loader_android.h"

namespace autofill {

static base::android::RegistrationMethod kComponentRegisteredMethods[] = {
  { "RegisterAuxiliaryProfileLoader",
      autofill::RegisterAuxiliaryProfileLoader },
};

bool RegisterAutofillAndroidJni() {
  return RegisterNativeMethods(
      kComponentRegisteredMethods, arraysize(kComponentRegisteredMethods));
}

}  // namespace autofill
