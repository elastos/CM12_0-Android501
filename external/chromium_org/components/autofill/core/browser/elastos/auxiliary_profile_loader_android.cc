// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "components/autofill/core/browser/elastos/auxiliary_profile_loader_android.h"

#include <vector>

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_array.h"
#include "base/elastos/jni_string.h"
#include "components/api/PersonalAutofillPopulator_api.h"

//#define JAVA_METHOD(__jmethod__) Java_PersonalAutofillPopulator_##__jmethod__( env_, populator_.obj())

#define JAVA_METHOD(__jmethod__) \
    sElaPersonalAutofillPopulatorCallback->elastos_PersonalAutofillPopulator_##__jmethod__(populator_.Get())

namespace {

//base::string16 SafeJavaStringToUTF16(const ScopedJavaLocalRef<jstring>& jstring) {
base::string16 SafeJavaStringToUTF16(const Elastos::String& jstring) {
  if (jstring.IsNullOrEmpty())
    return base::string16();

  return base::android::ConvertJavaStringToUTF16(jstring);
}

//void SafeJavaStringArrayToStringVector( const ScopedJavaLocalRef<jobjectArray>& jarray, JNIEnv* env,
void SafeJavaStringArrayToStringVector(Elastos::ArrayOf<Elastos::String>* jarray,
    std::vector<base::string16>* string_vector) {
  if (jarray) {
    base::android::AppendJavaStringArrayToStringVector(jarray,
                                                       string_vector);
  }
}

} // namespace

namespace autofill {

bool RegisterAuxiliaryProfileLoader() {
  return RegisterNativesImpl();
}

AuxiliaryProfileLoaderAndroid::AuxiliaryProfileLoaderAndroid() {}

AuxiliaryProfileLoaderAndroid::~AuxiliaryProfileLoaderAndroid() {}

void AuxiliaryProfileLoaderAndroid::Init(IInterface* context) {
  //env_ = env;
  //populator_ = Java_PersonalAutofillPopulator_create(env_, context);
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  populator_ = sElaPersonalAutofillPopulatorCallback->elastos_PersonalAutofillPopulator_create(context);
}

bool AuxiliaryProfileLoaderAndroid::GetHasPermissions() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return (bool)JAVA_METHOD(getHasPermissions);
}

// Address info
base::string16 AuxiliaryProfileLoaderAndroid::GetStreet() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getStreet));
}

base::string16 AuxiliaryProfileLoaderAndroid::GetPostOfficeBox() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getPobox));
}

base::string16 AuxiliaryProfileLoaderAndroid::GetNeighborhood() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getNeighborhood));
}

base::string16 AuxiliaryProfileLoaderAndroid::GetRegion() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getRegion));
}

base::string16 AuxiliaryProfileLoaderAndroid::GetCity() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getCity));
}

base::string16 AuxiliaryProfileLoaderAndroid::GetPostalCode() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getPostalCode));
}

base::string16 AuxiliaryProfileLoaderAndroid::GetCountry() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getCountry));
}

// Name info
base::string16 AuxiliaryProfileLoaderAndroid::GetFirstName() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getFirstName));
}

base::string16 AuxiliaryProfileLoaderAndroid::GetMiddleName() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getMiddleName));
}

base::string16 AuxiliaryProfileLoaderAndroid::GetLastName() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getLastName));
}

base::string16 AuxiliaryProfileLoaderAndroid::GetSuffix() const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  return SafeJavaStringToUTF16(JAVA_METHOD(getSuffix));
}

// Email info
void AuxiliaryProfileLoaderAndroid::GetEmailAddresses(
    std::vector<base::string16>* email_addresses) const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  SafeJavaStringArrayToStringVector(JAVA_METHOD(getEmailAddresses),
                                    email_addresses);
}

// Phone info
void AuxiliaryProfileLoaderAndroid::GetPhoneNumbers(
    std::vector<base::string16>* phone_numbers) const {
  DCHECK(sElaPersonalAutofillPopulatorCallback);
  SafeJavaStringArrayToStringVector(JAVA_METHOD(getPhoneNumbers),
                                    phone_numbers);
}

} // namespace
