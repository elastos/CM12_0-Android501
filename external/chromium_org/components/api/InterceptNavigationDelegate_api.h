// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file is autogenerated by
//     api_generator
// For
//     org/chromium/components/navigation_interception/InterceptNavigationDelegate

#ifndef ELASTOS_ORG_CHROMIUM_COMPONENTS_NAVIGATION_INTERCEPTION_INTERCEPTNAVIGATIONDELEGATE_JNI
#define ELASTOS_ORG_CHROMIUM_COMPONENTS_NAVIGATION_INTERCEPTION_INTERCEPTNAVIGATIONDELEGATE_JNI

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/logging.h"

//#include "base/android/jni_int_wrapper.h"

// Step 1: forward declarations.

// Step 2: method stubs.

// Step 3: Callback init .
struct ElaInterceptNavigationDelegateCallback
{
    Elastos::Boolean (*elastos_InterceptNavigationDelegate_shouldIgnoreNavigation)(IInterface* obj, IInterface*
        navigationParams);
};

extern "C" {
static struct ElaInterceptNavigationDelegateCallback* sElaInterceptNavigationDelegateCallback;
__attribute__((visibility("default")))
void Elastos_InterceptNavigationDelegate_InitCallback(Elastos::Handle32 cb)
{
    sElaInterceptNavigationDelegateCallback = (struct ElaInterceptNavigationDelegateCallback*)cb;
    DLOG(INFO) << "init pointer for sElaInterceptNavigationDelegateCallback is:" <<
        sElaInterceptNavigationDelegateCallback;
}
}; // extern "C"

static bool RegisterNativesImpl() {

  return true;
}

#endif  // ELASTOS_ORG_CHROMIUM_COMPONENTS_NAVIGATION_INTERCEPTION_INTERCEPTNAVIGATIONDELEGATE_JNI
