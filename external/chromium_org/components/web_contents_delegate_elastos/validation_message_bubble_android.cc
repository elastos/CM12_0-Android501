// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "components/web_contents_delegate_elastos/validation_message_bubble_android.h"

#include "base/elastos/jni_string.h"
#include "content/public/browser/elastos/content_view_core.h"
#include "content/public/browser/render_view_host.h"
#include "content/public/browser/web_contents.h"
#include "components/api/ValidationMessageBubble_api.h"
#include "ui/gfx/rect.h"

using base::android::ConvertUTF16ToJavaString;
using content::ContentViewCore;
using content::RenderWidgetHost;

namespace {

inline ContentViewCore* GetContentViewCoreFrom(RenderWidgetHost* widget_host) {
  return ContentViewCore::FromWebContents(
      content::WebContents::FromRenderViewHost(
          content::RenderViewHost::From(widget_host)));
}

}

namespace web_contents_delegate_android {

ValidationMessageBubbleAndroid::ValidationMessageBubbleAndroid(
    RenderWidgetHost* widget_host,
    const gfx::Rect& anchor_in_root_view,
    const base::string16& main_text,
    const base::string16& sub_text) {
  DCHECK(sElaValidationMessageBubbleCallback);
  //JNIEnv* env = base::android::AttachCurrentThread();
  //java_validation_message_bubble_.Reset(
  //    Java_ValidationMessageBubble_createAndShow(
  //        env,
  java_validation_message_bubble_ =
      sElaValidationMessageBubbleCallback->elastos_ValidationMessageBubble_createAndShow(
          GetContentViewCoreFrom(widget_host)->GetJavaObject().Get(),
          anchor_in_root_view.x(),
          anchor_in_root_view.y(),
          anchor_in_root_view.width(),
          anchor_in_root_view.height(),
          ConvertUTF16ToJavaString(main_text),
          ConvertUTF16ToJavaString(sub_text));
}

ValidationMessageBubbleAndroid::~ValidationMessageBubbleAndroid() {
  //Java_ValidationMessageBubble_close(base::android::AttachCurrentThread(), java_validation_message_bubble_.obj());
  DCHECK(sElaValidationMessageBubbleCallback);
  sElaValidationMessageBubbleCallback->elastos_ValidationMessageBubble_close(java_validation_message_bubble_.Get());
}

void ValidationMessageBubbleAndroid::SetPositionRelativeToAnchor(
    RenderWidgetHost* widget_host, const gfx::Rect& anchor_in_root_view) {
  DCHECK(sElaValidationMessageBubbleCallback);
  //Java_ValidationMessageBubble_setPositionRelativeToAnchor(
  //    base::android::AttachCurrentThread(),
  sElaValidationMessageBubbleCallback->elastos_ValidationMessageBubble_setPositionRelativeToAnchor(
      java_validation_message_bubble_.Get(),
      GetContentViewCoreFrom(widget_host)->GetJavaObject().Get(),
      anchor_in_root_view.x(),
      anchor_in_root_view.y(),
      anchor_in_root_view.width(),
      anchor_in_root_view.height());
}

// static
bool ValidationMessageBubbleAndroid::Register() {
  return RegisterNativesImpl();
}

}  // namespace web_contents_delegate_android
