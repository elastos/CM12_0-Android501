// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "components/web_contents_delegate_elastos/color_chooser_android.h"

#include "base/elastos/jni_array.h"
#include "base/elastos/jni_string.h"
#include "content/public/browser/elastos/content_view_core.h"
#include "content/public/browser/web_contents.h"
#include "content/public/common/color_suggestion.h"
#include "components/api/ColorChooserAndroid_api.h"

using base::android::ConvertUTF16ToJavaString;

namespace web_contents_delegate_android {

ColorChooserAndroid::ColorChooserAndroid(
    content::WebContents* web_contents,
    SkColor initial_color,
    const std::vector<content::ColorSuggestion>& suggestions)
    : web_contents_(web_contents) {
  //JNIEnv* env = AttachCurrentThread();
  content::ContentViewCore* content_view_core =
      content::ContentViewCore::FromWebContents(web_contents);
  DCHECK(content_view_core);

  //ScopedJavaLocalRef<jobjectArray> suggestions_array;
  Elastos::AutoPtr<Elastos::ArrayOf<IInterface*> > suggestions_array;

  DCHECK(sElaColorChooserAndroidCallback);
  if (suggestions.size() > 0) {
    //suggestions_array = Java_ColorChooserAndroid_createColorSuggestionArray(env, suggestions.size());
    suggestions_array = sElaColorChooserAndroidCallback->
        elastos_ColorChooserAndroid_createColorSuggestionArray(suggestions.size());

    for (size_t i = 0; i < suggestions.size(); ++i) {
      const content::ColorSuggestion& suggestion = suggestions[i];
      //ScopedJavaLocalRef<jstring> label = ConvertUTF16ToJavaString(env, suggestion.label);
      Elastos::String label = ConvertUTF16ToJavaString(suggestion.label);
      //Java_ColorChooserAndroid_addToColorSuggestionArray(
      sElaColorChooserAndroidCallback->elastos_ColorChooserAndroid_addToColorSuggestionArray(
          suggestions_array.Get(),
          i,
          suggestion.color,
          label);
    }
  }
  //j_color_chooser_.Reset(Java_ColorChooserAndroid_createColorChooserAndroid(
  j_color_chooser_ =
  sElaColorChooserAndroidCallback->elastos_ColorChooserAndroid_createColorChooserAndroid(
      reinterpret_cast<intptr_t>(this),
      content_view_core->GetJavaObject().Get(),
      initial_color,
      suggestions_array.Get());
}

ColorChooserAndroid::~ColorChooserAndroid() {
}

void ColorChooserAndroid::End() {
  if (j_color_chooser_.Get()) {
    DCHECK(sElaColorChooserAndroidCallback);
    //JNIEnv* env = AttachCurrentThread();
    //Java_ColorChooserAndroid_closeColorChooser(env, j_color_chooser_.obj());
    sElaColorChooserAndroidCallback->elastos_ColorChooserAndroid_closeColorChooser(j_color_chooser_.Get());
  }
}

void ColorChooserAndroid::SetSelectedColor(SkColor color) {
  // Not implemented since the color is set on the java side only, in theory
  // it can be set from JS which would override the user selection but
  // we don't support that for now.
}

void ColorChooserAndroid::OnColorChosen(IInterface* obj, Elastos::Int32 color) {
  web_contents_->DidChooseColorInColorChooser(color);
  web_contents_->DidEndColorChooser();
}

// ----------------------------------------------------------------------------
// Native JNI methods
// ----------------------------------------------------------------------------
bool RegisterColorChooserAndroid() {
  return RegisterNativesImpl();
}

}  // namespace web_contents_delegate_android
