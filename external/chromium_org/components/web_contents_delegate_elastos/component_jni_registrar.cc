// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "components/web_contents_delegate_elastos/component_jni_registrar.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "components/web_contents_delegate_elastos/color_chooser_android.h"
#include "components/web_contents_delegate_elastos/validation_message_bubble_android.h"
#include "components/web_contents_delegate_elastos/web_contents_delegate_android.h"

namespace web_contents_delegate_android {

static base::android::RegistrationMethod kComponentRegisteredMethods[] = {
  { "ColorChooserAndroid", RegisterColorChooserAndroid },
  { "ValidationMessageBubble", ValidationMessageBubbleAndroid::Register },
  { "WebContentsDelegateAndroid", RegisterWebContentsDelegateAndroid },
};

bool RegisterWebContentsDelegateAndroidJni() {
  return RegisterNativeMethods(
      kComponentRegisteredMethods, arraysize(kComponentRegisteredMethods));
}

} // namespace web_contents_delegate_android

