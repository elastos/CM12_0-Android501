// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "components/navigation_interception_elastos/navigation_params_android.h"

#include "base/elastos/jni_string.h"
#include "components/api/NavigationParams_api.h"

using base::android::ConvertUTF8ToJavaString;

namespace navigation_interception {

//base::android::ScopedJavaLocalRef<jobject> CreateJavaNavigationParams(
Elastos::AutoPtr<IInterface> CreateJavaNavigationParams(
    const NavigationParams& params) {
    Elastos::String jstring_url =
      ConvertUTF8ToJavaString(params.url().spec());

  DCHECK(sElaNavigationParamsCallback);
  //return Java_NavigationParams_create(env,
  return sElaNavigationParamsCallback->elastos_NavigationParams_create(
                                      jstring_url,
                                      params.is_post(),
                                      params.has_user_gesture(),
                                      params.transition_type(),
                                      params.is_redirect());
}

// Register native methods.

bool RegisterNavigationParams() {
  return RegisterNativesImpl();
}

}  // namespace navigation_interception
