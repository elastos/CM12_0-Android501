// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "components/navigation_interception_elastos/intercept_navigation_delegate.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
#include "base/callback.h"
#include "components/navigation_interception_elastos/intercept_navigation_resource_throttle.h"
#include "components/navigation_interception_elastos/navigation_params_android.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/render_view_host.h"
#include "content/public/browser/web_contents.h"
#include "components/api/InterceptNavigationDelegate_api.h"
#include "net/url_request/url_request.h"
#include "url/gurl.h"

using base::android::ConvertUTF8ToJavaString;
//using base::android::ScopedJavaLocalRef;
using content::BrowserThread;
using content::PageTransition;
using content::RenderViewHost;
using content::WebContents;

namespace navigation_interception {

namespace {

const void* kInterceptNavigationDelegateUserDataKey =
    &kInterceptNavigationDelegateUserDataKey;

bool CheckIfShouldIgnoreNavigationOnUIThread(WebContents* source,
                                             const NavigationParams& params) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  DCHECK(source);

  InterceptNavigationDelegate* intercept_navigation_delegate =
      InterceptNavigationDelegate::Get(source);
  if (!intercept_navigation_delegate)
    return false;

  return intercept_navigation_delegate->ShouldIgnoreNavigation(params);
}

}  // namespace

// static
void InterceptNavigationDelegate::Associate(
    WebContents* web_contents,
    scoped_ptr<InterceptNavigationDelegate> delegate) {
  web_contents->SetUserData(kInterceptNavigationDelegateUserDataKey,
                            delegate.release());
}

// static
InterceptNavigationDelegate* InterceptNavigationDelegate::Get(
    WebContents* web_contents) {
  return reinterpret_cast<InterceptNavigationDelegate*>(
      web_contents->GetUserData(kInterceptNavigationDelegateUserDataKey));
}

// static
content::ResourceThrottle* InterceptNavigationDelegate::CreateThrottleFor(
    net::URLRequest* request) {
  return new InterceptNavigationResourceThrottle(
      request, base::Bind(&CheckIfShouldIgnoreNavigationOnUIThread));
}

InterceptNavigationDelegate::InterceptNavigationDelegate(
    IInterface* jdelegate)
    : weak_jdelegate_(jdelegate) {
}

InterceptNavigationDelegate::~InterceptNavigationDelegate() {
}

bool InterceptNavigationDelegate::ShouldIgnoreNavigation(
    const NavigationParams& navigation_params) {
  if (!navigation_params.url().is_valid())
    return false;

  DCHECK(sElaInterceptNavigationDelegateCallback);
  //JNIEnv* env = base::android::AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> jdelegate = weak_jdelegate_.get(env);
  Elastos::AutoPtr<IInterface> jdelegate = weak_jdelegate_.get();

  if (!jdelegate.Get())
    return false;

  //ScopedJavaLocalRef<jobject> jobject_params =
  //    CreateJavaNavigationParams(env, navigation_params);
  Elastos::AutoPtr<IInterface> jobject_params =
      CreateJavaNavigationParams(navigation_params);

  //return Java_InterceptNavigationDelegate_shouldIgnoreNavigation(env,
  return sElaInterceptNavigationDelegateCallback->elastos_InterceptNavigationDelegate_shouldIgnoreNavigation(
      jdelegate.Get(),
      jobject_params.Get());
}

// Register native methods.

bool RegisterInterceptNavigationDelegate() {
  return RegisterNativesImpl();
}

}  // namespace navigation_interception
