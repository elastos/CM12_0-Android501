// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.


#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "printing/printing_context_elastos.h"

namespace printing {

static base::android::RegistrationMethod kComponentRegisteredMethods[] = {
  { "printcontext", PrintingContextAndroid::RegisterPrintingContext},
};

bool RegisterPrintingContextJni() {
  return RegisterNativeMethods(
      kComponentRegisteredMethods, arraysize(kComponentRegisteredMethods));
}

} // namespace web_contents_delegate_android

