// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COMPONENTS_PRINTING_CONTEXT_JNI_REGISTRAR_H_
#define COMPONENTS_PRINTING_CONTEXT_JNI_REGISTRAR_H_

//#include <jni.h>

namespace printing {

// Register all JNI bindings necessary for the printing context
// component.
bool RegisterPrintingContextJni();

}  // namespace web_contents_delegate_android

#endif  // COMPONENTS_WEB_CONTENTS_DELEGATE_ANDROID_COMPONENT_JNI_REGISTRAR_H_

