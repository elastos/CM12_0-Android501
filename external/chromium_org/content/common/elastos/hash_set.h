// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

//#include <jni.h>

//#include "base/android/scoped_java_ref.h"
#include "ElAndroid.h"
#include "elastos.h"

namespace content {

bool RegisterHashSet();

void JNI_Java_HashSet_add(IInterface* hash_set, IInterface* object);

void JNI_Java_HashSet_remove(IInterface* hash_set, IInterface* object);

void JNI_Java_HashSet_clear(IInterface* hash_set);

}  // namespace content
