// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/api/HashSet_api.h"

namespace content {

bool RegisterHashSet() {
  return API_HashSet::RegisterNativesImpl();
}

void JNI_Java_HashSet_add(IInterface* hash_set, IInterface* object) {
    DCHECK(API_HashSet::sElaHashSetCallback);
    API_HashSet::sElaHashSetCallback->elastos_HashSet_add(hash_set, object);
}

void JNI_Java_HashSet_remove(IInterface* hash_set, IInterface* object) {
    DCHECK(API_HashSet::sElaHashSetCallback);
    API_HashSet::sElaHashSetCallback->elastos_HashSet_remove(hash_set, object);
}

void JNI_Java_HashSet_clear(IInterface* hash_set) {
    DCHECK(API_HashSet::sElaHashSetCallback);
    API_HashSet::sElaHashSetCallback->elastos_HashSet_clear(hash_set);
}

}  // namespace content
