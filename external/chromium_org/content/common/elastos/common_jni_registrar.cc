// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/common/elastos/common_jni_registrar.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "content/common/elastos/hash_set.h"

namespace {
base::android::RegistrationMethod kContentRegisteredMethods[] = {
  { "HashSet", content::RegisterHashSet },
};

}  // namespace

namespace content {
namespace android {

bool RegisterCommonJni() {
  return RegisterNativeMethods(kContentRegisteredMethods,
                               arraysize(kContentRegisteredMethods));
}

}  // namespace android
}  // namespace content
