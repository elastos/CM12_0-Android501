// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/app/elastos/app_jni_registrar.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "content/app/elastos/child_process_service.h"
#include "content/app/elastos/content_main.h"

namespace {

base::android::RegistrationMethod kContentRegisteredMethods[] = {
  { "ContentMain", content::RegisterContentMain },
  { "ChildProcessService", content::RegisterChildProcessService },
};

}  // namespace

namespace content {
namespace android {

bool RegisterAppJni() {
  return RegisterNativeMethods(kContentRegisteredMethods,
                               arraysize(kContentRegisteredMethods));
    return false;
}

}  // namespace android
}  // namespace content
