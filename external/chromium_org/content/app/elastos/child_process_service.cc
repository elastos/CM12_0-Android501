// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/app/elastos/child_process_service.h"

//#include <android/native_window_jni.h>
#include "elastos/native_window.h"
#include <cpu-features.h>

#include "base/elastos/jni_array.h"
#include "base/elastos/library_loader/library_loader_hooks.h"
#include "base/elastos/memory_pressure_listener_android.h"
#include "base/logging.h"
#include "base/posix/global_descriptors.h"
#include "content/child/child_thread.h"
#include "content/common/elastos/surface_texture_lookup.h"
#include "content/common/elastos/surface_texture_peer.h"
#include "content/common/gpu/gpu_surface_lookup.h"
#include "content/public/app/elastos_library_loader_hooks.h"
#include "content/public/common/content_descriptors.h"
#include "ipc/ipc_descriptors.h"
#include "content/api/ChildProcessService_api.h"
#include "base/elastos/api/NativeWindow_api.h"
#include "ui/gl/elastos/scoped_java_surface.h"

//using base::android::AttachCurrentThread;
//using base::android::CheckException;
using base::android::JavaIntArrayToIntVector;

namespace content {

namespace {

class SurfaceTexturePeerChildImpl : public SurfaceTexturePeer,
                                    public GpuSurfaceLookup,
                                    public SurfaceTextureLookup {
 public:
  // |service| is the instance of
  // org.chromium.content.app.ChildProcessService.
  explicit SurfaceTexturePeerChildImpl(
      IInterface* service)
      : service_(service) {
    GpuSurfaceLookup::InitInstance(this);
    SurfaceTextureLookup::InitInstance(this);
  }

  virtual ~SurfaceTexturePeerChildImpl() {
    GpuSurfaceLookup::InitInstance(NULL);
    SurfaceTextureLookup::InitInstance(NULL);
  }

  // Overridden from SurfaceTexturePeer:
  virtual void EstablishSurfaceTexturePeer(
      base::ProcessHandle pid,
      scoped_refptr<gfx::SurfaceTexture> surface_texture,
      int primary_id,
      int secondary_id) OVERRIDE {
    //JNIEnv* env = base::android::AttachCurrentThread();
    //content::Java_ChildProcessService_establishSurfaceTexturePeer(
    DCHECK(sElaChildProcessServiceCallback);
    sElaChildProcessServiceCallback->elastos_ChildProcessService_establishSurfaceTexturePeer(
        service_.Get(), pid,
        surface_texture->j_surface_texture().Get(), primary_id,
        secondary_id);
    //CheckException(env);
  }

  // Overridden from GpuSurfaceLookup:
  virtual gfx::AcceleratedWidget AcquireNativeWidget(int surface_id) OVERRIDE {
    //JNIEnv* env = base::android::AttachCurrentThread();
    DCHECK(sElaChildProcessServiceCallback);
    gfx::ScopedJavaSurface surface(
    sElaChildProcessServiceCallback->elastos_ChildProcessService_getViewSurface(
        service_.Get(), surface_id
        ));
    //    content::Java_ChildProcessService_getViewSurface(
    //    env, service_.obj(), surface_id));

    if (!surface.j_surface().Get())
      return NULL;

    // Note: This ensures that any local references used by
    // ANativeWindow_fromSurface are released immediately. This is needed as a
    // workaround for https://code.google.com/p/android/issues/detail?id=68174
    //TODO see above comment base::android::ScopedJavaLocalFrame scoped_local_reference_frame(env);
    //ANativeWindow* native_window = ANativeWindow_fromSurface(surface.j_surface().Get());
    DCHECK(android::sElaNativeWindowCallback);
    ANativeWindow* native_window = android::sElaNativeWindowCallback->elastos_NativeWindow_GetFromSurface(surface.j_surface().Get());


    return native_window;
  }

  // Overridden from SurfaceTextureLookup:
  virtual gfx::AcceleratedWidget AcquireNativeWidget(int primary_id,
                                                     int secondary_id)
      OVERRIDE {
    //JNIEnv* env = base::android::AttachCurrentThread();
    DCHECK(sElaChildProcessServiceCallback);
    gfx::ScopedJavaSurface surface(
            sElaChildProcessServiceCallback->elastos_ChildProcessService_getSurfaceTextureSurface(
                service_.Get(), primary_id, secondary_id
                ));
    //    content::Java_ChildProcessService_getSurfaceTextureSurface(
    //        env, service_.obj(), primary_id, secondary_id));

    if (!surface.j_surface().Get())
      return NULL;

    // Note: This ensures that any local references used by
    // ANativeWindow_fromSurface are released immediately. This is needed as a
    // workaround for https://code.google.com/p/android/issues/detail?id=68174
    //TODO base::android::ScopedJavaLocalFrame scoped_local_reference_frame(env);
    //ANativeWindow* native_window = ANativeWindow_fromSurface(surface.j_surface().Get());
    DCHECK(android::sElaNativeWindowCallback);
    ANativeWindow* native_window = android::sElaNativeWindowCallback->elastos_NativeWindow_GetFromSurface(surface.j_surface().Get());

    return native_window;
  }

 private:
  // The instance of org.chromium.content.app.ChildProcessService.
  //base::android::ScopedJavaGlobalRef<jobject> service_;
  Elastos::AutoPtr<IInterface> service_;

  DISALLOW_COPY_AND_ASSIGN(SurfaceTexturePeerChildImpl);
};

// Chrome actually uses the renderer code path for all of its child
// processes such as renderers, plugins, etc.
void InternalInitChildProcess(const std::vector<int>& file_ids,
                              const std::vector<int>& file_fds,
                              IInterface* context,
                              IInterface* service_in,
                              Elastos::Int32 cpu_count,
                              Elastos::Int64 cpu_features) {
  //base::android::ScopedJavaLocalRef<jobject> service(env, service_in);
  Elastos::AutoPtr<IInterface> service(service_in);

  // Set the CPU properties.
  android_setCpu(cpu_count, cpu_features);
  // Register the file descriptors.
  // This includes the IPC channel, the crash dump signals and resource related
  // files.
  DCHECK(file_fds.size() == file_ids.size());
  for (size_t i = 0; i < file_ids.size(); ++i)
    base::GlobalDescriptors::GetInstance()->Set(file_ids[i], file_fds[i]);

  // SurfaceTexturePeerChildImpl implements the SurfaceTextureLookup interface,
  // which need to be set before we create a compositor thread that could be
  // using it to initialize resources.
  content::SurfaceTexturePeer::InitInstance(
      new SurfaceTexturePeerChildImpl(service));

  base::android::MemoryPressureListenerAndroid::RegisterSystemCallback();
}

}  // namespace <anonymous>

void InitChildProcess(IInterface* context,
                      IInterface* service,
                      Elastos::ArrayOf<Elastos::Int32>* j_file_ids,
                      Elastos::ArrayOf<Elastos::Int32>* j_file_fds,
                      Elastos::Int32 cpu_count,
                      Elastos::Int64 cpu_features) {
  std::vector<int> file_ids;
  std::vector<int> file_fds;
  JavaIntArrayToIntVector(j_file_ids, &file_ids);
  JavaIntArrayToIntVector(j_file_fds, &file_fds);

  InternalInitChildProcess(
      file_ids, file_fds, context, service,
      cpu_count, cpu_features);
}

void ExitChildProcess() {
  VLOG(0) << "ChildProcessService: Exiting child process.";
  base::android::LibraryLoaderExitHook();
  _exit(0);
}

bool RegisterChildProcessService() {
  return RegisterNativesImpl();
}

void ShutdownMainThread(IInterface* obj) {
  ChildThread::ShutdownThread();
}

}  // namespace content
