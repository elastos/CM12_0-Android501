// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/app/elastos/content_main.h"

#include "ElAndroid.h"
#include "elastos.h"
#include "base/elastos/api_elastos.h"
#include "base/at_exit.h"
#include "base/base_switches.h"
#include "base/command_line.h"
#include "base/debug/trace_event.h"
#include "base/lazy_instance.h"
#include "content/public/app/content_main.h"
#include "content/public/app/content_main_delegate.h"
#include "content/public/app/content_main_runner.h"
#include "content/public/common/content_switches.h"
#include "content/api/ContentMain_api.h"

using base::LazyInstance;

namespace content {

namespace {
LazyInstance<scoped_ptr<ContentMainRunner> > g_content_runner =
    LAZY_INSTANCE_INITIALIZER;

LazyInstance<scoped_ptr<ContentMainDelegate> > g_content_main_delegate =
    LAZY_INSTANCE_INITIALIZER;

}  // namespace

static void InitApplicationContext(IInterface* context) {
  //base::android::ScopedJavaLocalRef<jobject> scoped_context(env, context);
  //base::android::InitApplicationContext(env, scoped_context);
  Elastos::AutoPtr<IInterface> scoped_context(context);
  base::android::InitApplicationContext(scoped_context);
}

static Elastos::Int32 Start() {
  TRACE_EVENT0("startup", "content::Start");

  // On Android we can have multiple requests to start the browser in process
  // simultaneously. If we get an asynchonous request followed by a synchronous
  // request then we have to call this a second time to finish starting the
  // browser synchronously.
  if (!g_content_runner.Get().get()) {
    ContentMainParams params(g_content_main_delegate.Get().get());
    g_content_runner.Get().reset(ContentMainRunner::Create());
    g_content_runner.Get()->Initialize(params);
  }
  return g_content_runner.Get()->Run();
}

void SetContentMainDelegate(ContentMainDelegate* delegate) {
  DCHECK(!g_content_main_delegate.Get().get());
  g_content_main_delegate.Get().reset(delegate);
}

bool RegisterContentMain() {
  return RegisterNativesImpl();
}

}  // namespace content
