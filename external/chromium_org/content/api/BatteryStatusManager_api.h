// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file is autogenerated by
//     api_generator
// For
//     org/chromium/content/browser/BatteryStatusManager

#ifndef ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_BATTERYSTATUSMANAGER_JNI
#define ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_BATTERYSTATUSMANAGER_JNI

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/logging.h"

//#include "base/android/jni_int_wrapper.h"

// Step 1: forward declarations.

namespace content {

// Step 2: method stubs.

extern "C" {
__attribute__((visibility("default")))
void Elastos_BatteryStatusManager_nativeGotBatteryStatus(
    /* [in] */ IInterface* caller,
    /* [in] */ Elastos::Handle64 nativeBatteryStatusManager,
    /* [in] */ Elastos::Boolean charging,
    /* [in] */ Elastos::Double chargingTime,
    /* [in] */ Elastos::Double dischargingTime,
    /* [in] */ Elastos::Double level) {
    BatteryStatusManager* native = reinterpret_cast<BatteryStatusManager*>(nativeBatteryStatusManager);
    //CHECK_NATIVE_PTR(env, jcaller, native, "GotBatteryStatus");
    return native->GotBatteryStatus(caller, charging, chargingTime, dischargingTime, level);
}
};  // extern "C"

// Step 3: Callback init .
struct ElaBatteryStatusManagerCallback
{
    Elastos::AutoPtr<IInterface> (*elastos_BatteryStatusManager_getInstance)(IInterface* appContext);
    Elastos::Boolean (*elastos_BatteryStatusManager_start)(IInterface* obj, Elastos::Handle64 nativePtr);
    void (*elastos_BatteryStatusManager_stop)(IInterface* obj);
};

extern "C" {
static struct ElaBatteryStatusManagerCallback* sElaBatteryStatusManagerCallback;
__attribute__((visibility("default")))
void Elastos_BatteryStatusManager_InitCallback(Elastos::Handle64 cb)
{
    sElaBatteryStatusManagerCallback = (struct ElaBatteryStatusManagerCallback*)cb;
    DLOG(INFO) << "init pointer for sElaBatteryStatusManagerCallback is:" << sElaBatteryStatusManagerCallback;
}
}; // extern "C"

static bool RegisterNativesImpl() {

  return true;
}

}  // namespace content

#endif  // ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_BATTERYSTATUSMANAGER_JNI
