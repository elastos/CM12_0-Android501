//This file is autogenerated for
//    ContentReadbackHandler.java
//put this file at the end of the include list
//so the type definition used in this file will be found
#ifndef ELASTOS_CONTENTREADBACKHANDLER_CALLBACK_DEC_HH
#define ELASTOS_CONTENTREADBACKHANDLER_CALLBACK_DEC_HH


#ifdef __cplusplus
extern "C"
{
#endif
    extern Int64 Elastos_ContentReadbackHandler_nativeInit(IInterface* caller);
    extern void Elastos_ContentReadbackHandler_nativeDestroy(IInterface* caller,Handle32 nativeContentReadbackHandler);
    extern void Elastos_ContentReadbackHandler_nativeGetContentBitmap(IInterface* caller,Handle32 nativeContentReadbackHandler,Int32 readback_id,Float scale,Int32 config,Float x,Float y,Float width,Float height,IInterface* contentViewCore);
    extern void Elastos_ContentReadbackHandler_nativeGetCompositorBitmap(IInterface* caller,Handle32 nativeContentReadbackHandler,Int32 readback_id,Int64 nativeWindowAndroid);
#ifdef __cplusplus
}
#endif


struct ElaContentReadbackHandlerCallback
{
    void (*elastos_ContentReadbackHandler_notifyGetBitmapFinished)(IInterface* obj, Int32 readbackId, Boolean success, IInterface* bitmap);
};


#endif //ELASTOS_CONTENTREADBACKHANDLER_CALLBACK_DEC_HH
