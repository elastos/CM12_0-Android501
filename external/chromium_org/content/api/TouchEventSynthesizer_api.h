// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file is autogenerated by
//     api_generator
// For
//     org/chromium/content/browser/TouchEventSynthesizer

#ifndef ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_TOUCHEVENTSYNTHESIZER_JNI
#define ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_TOUCHEVENTSYNTHESIZER_JNI

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/logging.h"

//#include "base/android/jni_int_wrapper.h"

// Step 1: forward declarations.

namespace content {

// Step 2: method stubs.

// Step 3: Callback init .
struct ElaTouchEventSynthesizerCallback
{
    void (*elastos_TouchEventSynthesizer_setPointer)(IInterface* obj, Elastos::Int32 index, Elastos::Int32 x, Elastos::Int32 y, Elastos::Int32 id);
    void (*elastos_TouchEventSynthesizer_inject)(IInterface* obj, Elastos::Int32 action, Elastos::Int32 pointerCount, Elastos::Int64 timeInMs);
};

extern "C" {
static struct ElaTouchEventSynthesizerCallback* sElaTouchEventSynthesizerCallback;
__attribute__((visibility("default")))
void Elastos_TouchEventSynthesizer_InitCallback(Elastos::Handle32 cb)
{
    sElaTouchEventSynthesizerCallback = (struct ElaTouchEventSynthesizerCallback*)cb;
    DLOG(INFO) << "init pointer for sElaTouchEventSynthesizerCallback is:" << sElaTouchEventSynthesizerCallback;
}
}; // extern "C"

static bool RegisterNativesImpl() {

  return true;
}

}  // namespace content

#endif  // ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_TOUCHEVENTSYNTHESIZER_JNI
