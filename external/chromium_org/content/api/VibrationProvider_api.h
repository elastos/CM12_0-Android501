// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file is autogenerated by
//     api_generator
// For
//     org/chromium/content/browser/VibrationProvider

#ifndef ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_VIBRATIONPROVIDER_JNI
#define ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_VIBRATIONPROVIDER_JNI

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/logging.h"

//#include "base/android/jni_int_wrapper.h"

// Step 1: forward declarations.

namespace content {

// Step 2: method stubs.

// Step 3: Callback init .
struct ElaVibrationProviderCallback
{
    Elastos::AutoPtr<IInterface> (*elastos_VibrationProvider_create)(IInterface* context);
    void (*elastos_VibrationProvider_vibrate)(IInterface* obj, Elastos::Int64 milliseconds);
    void (*elastos_VibrationProvider_cancelVibration)(IInterface* obj);
};

extern "C" {
static struct ElaVibrationProviderCallback* sElaVibrationProviderCallback;
__attribute__((visibility("default")))
void Elastos_VibrationProvider_InitCallback(Elastos::Handle32 cb)
{
    sElaVibrationProviderCallback = (struct ElaVibrationProviderCallback*)cb;
    DLOG(INFO) << "init pointer for sElaVibrationProviderCallback is:" << sElaVibrationProviderCallback;
}
}; // extern "C"

static bool RegisterNativesImpl() {

  return true;
}

}  // namespace content

#endif  // ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_VIBRATIONPROVIDER_JNI
