// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file is autogenerated by
//     api_generator
// For
//     org/chromium/content/browser/DownloadController

#ifndef ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_DOWNLOADCONTROLLER_JNI
#define ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_DOWNLOADCONTROLLER_JNI

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/logging.h"

//#include "base/android/jni_int_wrapper.h"

// Step 1: forward declarations.

namespace content {

extern "C" {

static void Init(IInterface* caller);

__attribute__((visibility("default")))
void Elastos_DownloadController_nativeInit(
    /* [in] */ IInterface* caller) {
    return Init(caller);
}

};  // extern "C"

// Step 2: method stubs.

// Step 3: Callback init .
struct ElaDownloadControllerCallback
{
    Elastos::AutoPtr<IInterface> (*elastos_DownloadController_getInstance)();
    void (*elastos_DownloadController_newHttpGetDownload)(IInterface* obj, IInterface* view, const Elastos::String& url,
        const Elastos::String& userAgent, const Elastos::String& contentDisposition, const Elastos::String& mimeType,
        const Elastos::String& cookie, const Elastos::String& referer, const Elastos::String& filename, Elastos::Int64
        contentLength);
    void (*elastos_DownloadController_onDownloadStarted)(IInterface* obj, IInterface* view, const Elastos::String&
        filename, const Elastos::String& mimeType);
    void (*elastos_DownloadController_onDownloadCompleted)(IInterface* obj, IInterface* context, const Elastos::String&
        url, const Elastos::String& mimeType, const Elastos::String& filename, const Elastos::String& path, Elastos::Int64
        contentLength, Elastos::Boolean successful, Elastos::Int32 downloadId);
    void (*elastos_DownloadController_onDownloadUpdated)(IInterface* obj, IInterface* context, const Elastos::String&
        url, const Elastos::String& mimeType, const Elastos::String& filename, const Elastos::String& path, Elastos::Int64
        contentLength, Elastos::Boolean successful, Elastos::Int32 downloadId, Elastos::Int32 percentCompleted, Elastos::Int64 timeRemainingInMs);
    void (*elastos_DownloadController_onDangerousDownload)(IInterface* obj, IInterface* view, const Elastos::String&
        filename, Elastos::Int32 downloadId);
};

extern "C" {
static struct ElaDownloadControllerCallback* sElaDownloadControllerCallback;
__attribute__((visibility("default")))
void Elastos_DownloadController_InitCallback(Elastos::Handle32 cb)
{
    sElaDownloadControllerCallback = (struct ElaDownloadControllerCallback*)cb;
    DLOG(INFO) << "init pointer for sElaDownloadControllerCallback is:" << sElaDownloadControllerCallback;
}
}; // extern "C"

static bool RegisterNativesImpl() {

  return true;
}

}  // namespace content

#endif  // ELASTOS_ORG_CHROMIUM_CONTENT_BROWSER_DOWNLOADCONTROLLER_JNI
