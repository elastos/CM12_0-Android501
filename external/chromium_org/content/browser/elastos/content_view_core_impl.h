// Copyright 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CONTENT_BROWSER_ANDROID_CONTENT_VIEW_CORE_IMPL_H_
#define CONTENT_BROWSER_ANDROID_CONTENT_VIEW_CORE_IMPL_H_

#include <vector>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/elastos/api_elastos.h"
#include "base/elastos/api_weak_ref.h"
#include "base/compiler_specific.h"
#include "base/i18n/rtl.h"
#include "base/memory/scoped_ptr.h"
#include "base/process/process.h"
#include "content/browser/renderer_host/render_widget_host_view_elastos.h"
#include "content/browser/web_contents/web_contents_impl.h"
#include "content/public/browser/elastos/content_view_core.h"
#include "content/public/browser/web_contents_observer.h"
#include "third_party/WebKit/public/web/WebInputEvent.h"
#include "ui/gfx/rect.h"
#include "ui/gfx/rect_f.h"
#include "url/gurl.h"

namespace ui {
class ViewAndroid;
class WindowAndroid;
}

namespace content {
class GinJavaBridgeDispatcherHost;
class RenderWidgetHostViewAndroid;
struct MenuItem;

// TODO(jrg): this is a shell.  Upstream the rest.
class ContentViewCoreImpl : public ContentViewCore,
                            public WebContentsObserver {
 public:
  static ContentViewCoreImpl* FromWebContents(WebContents* web_contents);
  ContentViewCoreImpl(IInterface* obj,
                      WebContents* web_contents,
                      ui::ViewAndroid* view_android,
                      ui::WindowAndroid* window_android,
                      IInterface* java_bridge_retained_object_set);

  // ContentViewCore implementation.
  //virtual base::android::ScopedJavaLocalRef<jobject> GetJavaObject() OVERRIDE;
  virtual Elastos::AutoPtr<IInterface> GetJavaObject() OVERRIDE;
  virtual WebContents* GetWebContents() const OVERRIDE;
  virtual ui::ViewAndroid* GetViewAndroid() const OVERRIDE;
  virtual ui::WindowAndroid* GetWindowAndroid() const OVERRIDE;
  virtual scoped_refptr<cc::Layer> GetLayer() const OVERRIDE;
  virtual void LoadUrl(NavigationController::LoadURLParams& params) OVERRIDE;
  virtual void ShowPastePopup(int x, int y) OVERRIDE;
  virtual void GetScaledContentBitmap(
      float scale,
      Elastos::Int32 bitmap_config,
      gfx::Rect src_subrect,
      const base::Callback<void(bool, const SkBitmap&)>& result_callback)
      OVERRIDE;
  virtual float GetDpiScale() const OVERRIDE;

  //TODO leliang
  //ActionsCode(hexibin, bugfix BUG00262636)
  //virtual void SetIsPaused(bool paused) OVERRIDE;
  virtual void PauseVideo() OVERRIDE;
  virtual void PauseOrResumeGeolocation(bool should_pause) OVERRIDE;
  virtual void PauseOrResumeVideoCaptureStream(bool should_pause) OVERRIDE;
  virtual void RequestTextSurroundingSelection(
      int max_length,
      const base::Callback<void(const base::string16& content,
                                int start_offset,
                                int end_offset)>& callback) OVERRIDE;

  // --------------------------------------------------------------------------
  // Methods called from Java via JNI
  // --------------------------------------------------------------------------

  //LLbase::android::ScopedJavaLocalRef<jobject> GetWebContentsAndroid(JNIEnv* env, jobject obj);
  Elastos::AutoPtr<IInterface> GetWebContentsAndroid(IInterface* obj);

  void OnJavaContentViewCoreDestroyed(IInterface* obj);

  // Notifies the ContentViewCore that items were selected in the currently
  // showing select popup.
  void SelectPopupMenuItems(IInterface* obj, Elastos::ArrayOf<Elastos::Int32>* indices);

  void LoadUrl(
      IInterface* obj,
      const Elastos::String& url,
      Elastos::Int32 load_url_type,
      Elastos::Int32 transition_type,
      const Elastos::String& j_referrer_url,
      Elastos::Int32 referrer_policy,
      Elastos::Int32 ua_override_option,
      const Elastos::String& extra_headers,
      Elastos::ArrayOf<Elastos::Byte>* post_data,
      const Elastos::String& base_url_for_data_url,
      const Elastos::String& virtual_url_for_data_url,
      Elastos::Boolean can_load_local_resources,
      Elastos::Boolean is_renderer_initiated);
  Elastos::String  GetURL(IInterface*) const;
  Elastos::Boolean IsIncognito(IInterface* obj);
  void SendOrientationChangeEvent(IInterface* obj, Elastos::Int32 orientation);
  Elastos::Boolean OnTouchEvent( IInterface* obj,
                        IInterface* motion_event,
                        Elastos::Int64 time_ms,
                        Elastos::Int32 android_action,
                        Elastos::Int32 pointer_count,
                        Elastos::Int32 history_size,
                        Elastos::Int32 action_index,
                        Elastos::Float pos_x_0,
                        Elastos::Float pos_y_0,
                        Elastos::Float pos_x_1,
                        Elastos::Float pos_y_1,
                        Elastos::Int32 pointer_id_0,
                        Elastos::Int32 pointer_id_1,
                        Elastos::Float touch_major_0,
                        Elastos::Float touch_major_1,
                        Elastos::Float raw_pos_x,
                        Elastos::Float raw_pos_y,
                        Elastos::Int32 android_tool_type_0,
                        Elastos::Int32 android_tool_type_1,
                        Elastos::Int32 android_button_state);
  Elastos::Boolean SendMouseMoveEvent( IInterface* obj,
                              Elastos::Int64 time_ms,
                              Elastos::Float x,
                              Elastos::Float y);
  Elastos::Boolean SendMouseWheelEvent( IInterface* obj,
                               Elastos::Int64 time_ms,
                               Elastos::Float x,
                               Elastos::Float y,
                               Elastos::Float vertical_axis);
  void ScrollBegin(IInterface* obj, Elastos::Int64 time_ms,
                   Elastos::Float x, Elastos::Float y, Elastos::Float hintx, Elastos::Float hinty);
  void ScrollEnd(IInterface* obj, Elastos::Int64 time_ms);
  void ScrollBy( IInterface* obj, Elastos::Int64 time_ms,
                Elastos::Float x, Elastos::Float y, Elastos::Float dx, Elastos::Float dy);
  void FlingStart(IInterface* obj, Elastos::Int64 time_ms,
                  Elastos::Float x, Elastos::Float y, Elastos::Float vx, Elastos::Float vy);
  void FlingCancel(IInterface* obj, Elastos::Int64 time_ms);
  void SingleTap( IInterface* obj, Elastos::Int64 time_ms,
                 Elastos::Float x, Elastos::Float y);
  void DoubleTap(IInterface* obj, Elastos::Int64 time_ms,
                 Elastos::Float x, Elastos::Float y) ;
  void LongPress(IInterface* obj, Elastos::Int64 time_ms,
                 Elastos::Float x, Elastos::Float y);
  void PinchBegin(IInterface* obj, Elastos::Int64 time_ms, Elastos::Float x, Elastos::Float y);
  void PinchEnd(IInterface* obj, Elastos::Int64 time_ms);
  void PinchBy(IInterface* obj, Elastos::Int64 time_ms,
               Elastos::Float x, Elastos::Float y, Elastos::Float delta);
  void SelectBetweenCoordinates(IInterface* obj,
                                Elastos::Float x1, Elastos::Float y1,
                                Elastos::Float x2, Elastos::Float y2);
  void MoveCaret(IInterface* obj, Elastos::Float x, Elastos::Float y);

  void ResetGestureDetection(IInterface* obj);
  void SetDoubleTapSupportEnabled(IInterface* obj, Elastos::Boolean enabled);
  void SetMultiTouchZoomSupportEnabled(IInterface* obj,
                                       Elastos::Boolean enabled);

  void LoadIfNecessary(IInterface* obj);
  void RequestRestoreLoad(IInterface* obj);
  void Reload(IInterface* obj, Elastos::Boolean check_for_repost);
  void ReloadIgnoringCache(IInterface* obj, Elastos::Boolean check_for_repost);
  void CancelPendingReload(IInterface* obj);
  void ContinuePendingReload(IInterface* obj);
  void AddStyleSheetByURL(IInterface* obj, const Elastos::String& url);
  void ClearHistory(IInterface* obj);
  void EvaluateJavaScript(IInterface* obj,
                          const Elastos::String& script,
                          IInterface* callback,
                          Elastos::Boolean start_renderer);
  long GetNativeImeAdapter(IInterface* obj);
  void SetFocus(IInterface* obj, Elastos::Boolean focused);
  void ScrollFocusedEditableNodeIntoView(IInterface* obj);
  void SelectWordAroundCaret(IInterface* obj);

  Elastos::Int32 GetBackgroundColor(IInterface* obj);
  void SetBackgroundColor(IInterface* obj, Elastos::Int32 color);
  void OnShow(IInterface* obj);
  void OnHide(IInterface* obj);
  void ClearSslPreferences(IInterface* /* obj */);
  void SetUseDesktopUserAgent(IInterface* /* obj */,
                              Elastos::Boolean state,
                              Elastos::Boolean reload_on_state_change);
  bool GetUseDesktopUserAgent(IInterface* /* obj */);
  void Show();
  void Hide();
  void SetAllowJavascriptInterfacesInspection(IInterface* obj,
                                              Elastos::Boolean allow);
  void AddJavascriptInterface(IInterface* obj,
                              IInterface* object,
                              const Elastos::String& name,
                              IInterface* safe_annotation_clazz);
  void RemoveJavascriptInterface(IInterface* obj, const Elastos::String& name);
  int GetNavigationHistory(IInterface* obj, IInterface* history);
  void GetDirectedNavigationHistory(IInterface* obj,
                                    IInterface* history,
                                    Elastos::Boolean is_forward,
                                    Elastos::Int32 max_entries);
  Elastos::String GetOriginalUrlForActiveNavigationEntry(IInterface* obj);
  void WasResized(IInterface* obj);
  Elastos::Boolean IsRenderWidgetHostViewReady(IInterface* obj);
  void ExitFullscreen(IInterface* obj);
  void UpdateTopControlsState(IInterface* obj,
                              bool enable_hiding,
                              bool enable_showing,
                              bool animate);
  void ShowImeIfNeeded(IInterface* obj);

  void ShowInterstitialPage(IInterface* obj,
                            const Elastos::String& jurl,
                            Elastos::Handle64 delegate);
  Elastos::Boolean IsShowingInterstitialPage(IInterface* obj);

  void SetAccessibilityEnabled(IInterface* obj, bool enabled);

  void ExtractSmartClipData(IInterface* obj,
                            Elastos::Int32 x,
                            Elastos::Int32 y,
                            Elastos::Int32 width,
                            Elastos::Int32 height);

  void SetBackgroundOpaque(IInterface* jobj, Elastos::Boolean opaque);

  Elastos::Int32 GetCurrentRenderProcessId(IInterface* obj);

  // --------------------------------------------------------------------------
  // Public methods that call to Java via JNI
  // --------------------------------------------------------------------------

  void OnSmartClipDataExtracted(const base::string16& text,
                                const base::string16& html,
                                const gfx::Rect& clip_rect);

  // Creates a popup menu with |items|.
  // |multiple| defines if it should support multi-select.
  // If not |multiple|, |selected_item| sets the initially selected item.
  // Otherwise, item's "checked" flag selects it.
  void ShowSelectPopupMenu(const gfx::Rect& bounds,
                           const std::vector<MenuItem>& items,
                           int selected_item,
                           bool multiple);
  // Hides a visible popup menu.
  void HideSelectPopupMenu();

  // All sizes and offsets are in CSS pixels as cached by the renderer.
  void UpdateFrameInfo(const gfx::Vector2dF& scroll_offset,
                       float page_scale_factor,
                       const gfx::Vector2dF& page_scale_factor_limits,
                       const gfx::SizeF& content_size,
                       const gfx::SizeF& viewport_size,
                       const gfx::Vector2dF& controls_offset,
                       const gfx::Vector2dF& content_offset,
                       float overdraw_bottom_height);

  void UpdateImeAdapter(long native_ime_adapter, int text_input_type,
                        const std::string& text,
                        int selection_start, int selection_end,
                        int composition_start, int composition_end,
                        bool show_ime_if_needed, bool is_non_ime_change);
  void SetTitle(const base::string16& title);
  void OnBackgroundColorChanged(SkColor color);

  bool HasFocus();
  void OnGestureEventAck(const blink::WebGestureEvent& event,
                         InputEventAckState ack_result);
  bool FilterInputEvent(const blink::WebInputEvent& event);
  void OnSelectionChanged(const std::string& text);
  void OnSelectionBoundsChanged(
      const ViewHostMsg_SelectionBounds_Params& params);

  void StartContentIntent(const GURL& content_url);

  // Shows the disambiguation popup
  // |target_rect|   --> window coordinates which |zoomed_bitmap| represents
  // |zoomed_bitmap| --> magnified image of potential touch targets
  void ShowDisambiguationPopup(
      const gfx::Rect& target_rect, const SkBitmap& zoomed_bitmap);

  // Creates a java-side touch event, used for injecting touch event for
  // testing/benchmarking purposes
  //base::android::ScopedJavaLocalRef<jobject> CreateTouchEventSynthesizer();
  Elastos::AutoPtr<IInterface> CreateTouchEventSynthesizer();

  //base::android::ScopedJavaLocalRef<jobject> GetContentVideoViewClient();
  Elastos::AutoPtr<IInterface> GetContentVideoViewClient();

  // Returns the context that the ContentViewCore was created with, it would
  // typically be an Activity context for an on screen view.
  //base::android::ScopedJavaLocalRef<jobject> GetContext();
  Elastos::AutoPtr<IInterface> GetContext();

  // Returns True if the given media should be blocked to load.
  bool ShouldBlockMediaRequest(const GURL& url);

  void DidStopFlinging();

  // Returns the viewport size after accounting for the viewport offset.
  gfx::Size GetViewSize() const;

  void SetAccessibilityEnabledInternal(bool enabled);

  void ShowSelectionHandlesAutomatically() const;

  // --------------------------------------------------------------------------
  // Methods called from native code
  // --------------------------------------------------------------------------

  gfx::Size GetPhysicalBackingSize() const;
  gfx::Size GetViewportSizeDip() const;
  gfx::Size GetViewportSizeOffsetDip() const;
  float GetOverdrawBottomHeightDip() const;

  void AttachLayer(scoped_refptr<cc::Layer> layer);
  void RemoveLayer(scoped_refptr<cc::Layer> layer);

 private:
  class ContentViewUserData;

  friend class ContentViewUserData;
  virtual ~ContentViewCoreImpl();

  // WebContentsObserver implementation.
  virtual void RenderViewReady() OVERRIDE;
  virtual void RenderViewHostChanged(RenderViewHost* old_host,
                                     RenderViewHost* new_host) OVERRIDE;
  virtual void WebContentsDestroyed() OVERRIDE;

  // --------------------------------------------------------------------------
  // Other private methods and data
  // --------------------------------------------------------------------------

  void InitWebContents();

  RenderWidgetHostViewAndroid* GetRenderWidgetHostViewAndroid();

  blink::WebGestureEvent MakeGestureEvent(
      blink::WebInputEvent::Type type, int64 time_ms, float x, float y) const;

  gfx::Size GetViewportSizePix() const;
  gfx::Size GetViewportSizeOffsetPix() const;

  void DeleteScaledSnapshotTexture();

  bool OnMotionEvent(const ui::MotionEvent& event);
  void SendGestureEvent(const blink::WebGestureEvent& event);

  // Update focus state of the RenderWidgetHostView.
  void SetFocusInternal(bool focused);

  // Send device_orientation_ to renderer.
  void SendOrientationChangeEventInternal();

  float dpi_scale() const { return dpi_scale_; }

  // A weak reference to the Java ContentViewCore object.
  ObjectWeakGlobalRef java_ref_;

  // Reference to the current WebContents used to determine how and what to
  // display in the ContentViewCore.
  WebContentsImpl* web_contents_;

  // A compositor layer containing any layer that should be shown.
  scoped_refptr<cc::Layer> root_layer_;

  // Device scale factor.
  float dpi_scale_;

  // The Android view that can be used to add and remove decoration layers
  // like AutofillPopup.
  ui::ViewAndroid* view_android_;

  // The owning window that has a hold of main application activity.
  ui::WindowAndroid* window_android_;

  // The cache of device's current orientation set from Java side, this value
  // will be sent to Renderer once it is ready.
  int device_orientation_;

  bool accessibility_enabled_;

  // Manages injecting Java objects.
  //TODO js
  //scoped_ptr<GinJavaBridgeDispatcherHost>
  //    java_bridge_dispatcher_host_;

  DISALLOW_COPY_AND_ASSIGN(ContentViewCoreImpl);
};

bool RegisterContentViewCore();

}  // namespace content

#endif  // CONTENT_BROWSER_ANDROID_CONTENT_VIEW_CORE_IMPL_H_
