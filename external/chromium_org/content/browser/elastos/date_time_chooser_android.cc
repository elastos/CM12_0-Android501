// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/date_time_chooser_android.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
#include "base/i18n/char_iterator.h"
#include "content/common/date_time_suggestion.h"
#include "content/common/view_messages.h"
#include "content/public/browser/elastos/content_view_core.h"
#include "content/public/browser/render_view_host.h"
#include "content/api/DateTimeChooserAndroid_api.h"
#include "third_party/icu/source/common/unicode/uchar.h"
#include "third_party/icu/source/common/unicode/unistr.h"

//using base::android::AttachCurrentThread;
using base::android::ConvertJavaStringToUTF16;
using base::android::ConvertUTF8ToJavaString;
using base::android::ConvertUTF16ToJavaString;


namespace {

base::string16 SanitizeSuggestionString(const base::string16& string) {
  base::string16 trimmed = string.substr(0, 255);
  icu::UnicodeString sanitized;
  base::i18n::UTF16CharIterator sanitized_iterator(&trimmed);
  while (!sanitized_iterator.end()) {
    UChar c = sanitized_iterator.get();
    if (u_isprint(c))
      sanitized.append(c);
    sanitized_iterator.Advance();
  }
  return base::string16(sanitized.getBuffer(),
                        static_cast<size_t>(sanitized.length()));
}

}  // namespace

namespace content {

// DateTimeChooserAndroid implementation
DateTimeChooserAndroid::DateTimeChooserAndroid()
  : host_(NULL) {
}

DateTimeChooserAndroid::~DateTimeChooserAndroid() {
}

// static
void DateTimeChooserAndroid::InitializeDateInputTypes(
      int text_input_type_date, int text_input_type_date_time,
      int text_input_type_date_time_local, int text_input_type_month,
      int text_input_type_time, int text_input_type_week) {
  //JNIEnv* env = AttachCurrentThread();
  //Java_DateTimeChooserAndroid_initializeDateInputTypes(
  DCHECK(sElaDateTimeChooserAndroidCallback);
  sElaDateTimeChooserAndroidCallback->elastos_DateTimeChooserAndroid_initializeDateInputTypes(
         text_input_type_date, text_input_type_date_time,
         text_input_type_date_time_local, text_input_type_month,
         text_input_type_time, text_input_type_week);
}

void DateTimeChooserAndroid::ReplaceDateTime(IInterface*,
                                             Elastos::Boolean value) {
  host_->Send(new ViewMsg_ReplaceDateTime(host_->GetRoutingID(), value));
}

void DateTimeChooserAndroid::CancelDialog(IInterface*) {
  host_->Send(new ViewMsg_CancelDateTimeDialog(host_->GetRoutingID()));
}

void DateTimeChooserAndroid::ShowDialog(
    ContentViewCore* content,
    RenderViewHost* host,
    ui::TextInputType dialog_type,
    double dialog_value,
    double min,
    double max,
    double step,
    const std::vector<DateTimeSuggestion>& suggestions) {
  host_ = host;

  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobjectArray> suggestions_array;

  Elastos::AutoPtr<Elastos::ArrayOf<IInterface*> > suggestions_array;
  if (suggestions.size() > 0) {
    DCHECK(sElaDateTimeChooserAndroidCallback);
    suggestions_array =
        sElaDateTimeChooserAndroidCallback->elastos_DateTimeChooserAndroid_createSuggestionsArray(suggestions.size());
        //Java_DateTimeChooserAndroid_createSuggestionsArray(env, suggestions.size());
    for (size_t i = 0; i < suggestions.size(); ++i) {
      const content::DateTimeSuggestion& suggestion = suggestions[i];
      //ScopedJavaLocalRef<jstring> localized_value = ConvertUTF16ToJavaString(
      //    env, SanitizeSuggestionString(suggestion.localized_value));
      //ScopedJavaLocalRef<jstring> label = ConvertUTF16ToJavaString(
      //    env, SanitizeSuggestionString(suggestion.label));
      Elastos::String localized_value =
          ConvertUTF16ToJavaString(SanitizeSuggestionString(suggestion.localized_value));
      Elastos::String label =
          ConvertUTF16ToJavaString(SanitizeSuggestionString(suggestion.label));
      //Java_DateTimeChooserAndroid_setDateTimeSuggestionAt(env,
      sElaDateTimeChooserAndroidCallback->elastos_DateTimeChooserAndroid_setDateTimeSuggestionAt(
          suggestions_array.Get(), i, suggestion.value, localized_value, label);
    }
  }

  //j_date_time_chooser_.Reset(Java_DateTimeChooserAndroid_createDateTimeChooser(
  j_date_time_chooser_ = sElaDateTimeChooserAndroidCallback->elastos_DateTimeChooserAndroid_createDateTimeChooser(
      content->GetJavaObject().Get(),
      reinterpret_cast<intptr_t>(this),
      dialog_type,
      dialog_value,
      min,
      max,
      step,
      suggestions_array.Get());
}

// ----------------------------------------------------------------------------
// Native JNI methods
// ----------------------------------------------------------------------------
bool RegisterDateTimeChooserAndroid() {
  bool registered = RegisterNativesImpl();
  if (registered)
    DateTimeChooserAndroid::InitializeDateInputTypes(
        ui::TEXT_INPUT_TYPE_DATE,
        ui::TEXT_INPUT_TYPE_DATE_TIME,
        ui::TEXT_INPUT_TYPE_DATE_TIME_LOCAL,
        ui::TEXT_INPUT_TYPE_MONTH,
        ui::TEXT_INPUT_TYPE_TIME,
        ui::TEXT_INPUT_TYPE_WEEK);
  return registered;
}

}  // namespace content
