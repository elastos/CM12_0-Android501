// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/browser_startup_controller.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
#include "content/browser/android/content_startup_flags.h"

#include "content/api/BrowserStartupController_api.h"

namespace content {

bool BrowserMayStartAsynchronously() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //return Java_BrowserStartupController_browserMayStartAsynchonously(env);
  DCHECK(sElaBrowserStartupControllerCallback);
  return sElaBrowserStartupControllerCallback->elastos_BrowserStartupController_browserMayStartAsynchonously();
}

void BrowserStartupComplete(int result) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //Java_BrowserStartupController_browserStartupComplete(env, result);
  DCHECK(sElaBrowserStartupControllerCallback);
  sElaBrowserStartupControllerCallback->elastos_BrowserStartupController_browserStartupComplete(result);
}

bool RegisterBrowserStartupController() {
  return RegisterNativesImpl();
}

static void SetCommandLineFlags(Elastos::Int32 max_render_process_count,
                                const Elastos::String& plugin_descriptor) {
  std::string plugin_str =
      (plugin_descriptor == NULL
           ? std::string()
           : base::android::ConvertJavaStringToUTF8(plugin_descriptor));
  SetContentCommandLineFlags(max_render_process_count, plugin_str);
}

static Elastos::Boolean IsOfficialBuild() {
#if defined(OFFICIAL_BUILD)
  return true;
#else
  return false;
#endif
}

static Elastos::Boolean IsPluginEnabled() {
#if defined(ENABLE_PLUGINS)
  return true;
#else
  return false;
#endif
}

}  // namespace content
