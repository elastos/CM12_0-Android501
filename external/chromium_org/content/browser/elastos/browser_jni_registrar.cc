// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/browser_jni_registrar.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_registrar.h"
#include "content/common/elastos/hash_set.h"
#include "content/browser/renderer_host/ime_adapter_elastos.h"
#include "content/browser/renderer_host/input/motion_event_elastos.h"
#include "content/browser/accessibility/browser_accessibility_elastos.h"
#include "content/browser/accessibility/browser_accessibility_manager_elastos.h"
#include "content/browser/elastos/browser_startup_controller.h"
#include "content/browser/elastos/child_process_launcher_android.h"
#include "content/browser/elastos/content_readback_handler.h"
#include "content/browser/elastos/content_settings.h"
#include "content/browser/elastos/content_video_view.h"
#include "content/browser/elastos/content_view_core_impl.h"
#include "content/browser/elastos/content_view_render_view.h"
#include "content/browser/elastos/content_view_statics.h"
#include "content/browser/elastos/date_time_chooser_android.h"
#include "content/browser/elastos/download_controller_android_impl.h"
#include "content/browser/elastos/interstitial_page_delegate_android.h"
//#include "content/browser/elastos/java/java_bound_object.h"//js
#include "content/browser/elastos/load_url_params.h"
#include "content/browser/elastos/surface_texture_peer_browser_impl.h"
#include "content/browser/elastos/tracing_controller_android.h"
#include "content/browser/elastos/web_contents_observer_android.h"
#include "content/browser/battery_status/battery_status_manager.h"
#include "content/browser/device_sensors/sensor_manager_elastos.h"
#include "content/browser/frame_host/navigation_controller_elastos.h"
#include "content/browser/gamepad/gamepad_platform_data_fetcher_elastos.h"
#include "content/browser/geolocation/location_api_adapter_elastos.h"
#include "content/browser/media/elastos/media_drm_credential_manager.h"
#include "content/browser/media/elastos/media_resource_getter_impl.h"
#include "content/browser/power_save_blocker_elastos.h"
#include "content/browser/renderer_host/input/synthetic_gesture_target_elastos.h"
#include "content/browser/screen_orientation/screen_orientation_provider_elastos.h"
#include "content/browser/speech/speech_recognizer_impl_elastos.h"
#include "content/browser/time_zone_monitor_elastos.h"
#include "content/browser/vibration/vibration_provider_elastos.h"
#include "content/browser/web_contents/web_contents_elastos.h"

using content::SurfaceTexturePeerBrowserImpl;

namespace {
base::android::RegistrationMethod kContentRegisteredMethods[] = {
    {"AndroidLocationApiAdapter",
     content::AndroidLocationApiAdapter::RegisterGeolocationService},
    {"BatteryStatusManager",
     content::BatteryStatusManager::Register},
    {"BrowserAccessibilityManager",
     content::RegisterBrowserAccessibilityManager},
    {"BrowserStartupController", content::RegisterBrowserStartupController},
    {"ChildProcessLauncher", content::RegisterChildProcessLauncher},
    {"ContentReadbackHandler",
     content::ContentReadbackHandler::RegisterContentReadbackHandler},
    {"ContentSettings", content::ContentSettings::RegisterContentSettings},
    {"ContentVideoView", content::ContentVideoView::RegisterContentVideoView},
    {"ContentViewCore", content::RegisterContentViewCore},
    {"ContentViewRenderView",
     content::ContentViewRenderView::RegisterContentViewRenderView},
    {"DateTimePickerAndroid", content::RegisterDateTimeChooserAndroid},
    {"DownloadControllerAndroidImpl",
     content::DownloadControllerAndroidImpl::RegisterDownloadController},
    {"GamepadList", content::GamepadPlatformDataFetcherAndroid::
                        RegisterGamepadPlatformDataFetcherAndroid},
    {"InterstitialPageDelegateAndroid",
     content::InterstitialPageDelegateAndroid::
         RegisterInterstitialPageDelegateAndroid},
    {"LoadUrlParams", content::RegisterLoadUrlParams},
    {"MediaDrmCredentialManager",
     content::MediaDrmCredentialManager::RegisterMediaDrmCredentialManager},
    {"MediaResourceGetterImpl",
     content::MediaResourceGetterImpl::RegisterMediaResourceGetter},
    {"MotionEventAndroid",
     content::MotionEventAndroid::RegisterMotionEventAndroid},
    {"NavigationControllerAndroid",
     content::NavigationControllerAndroid::Register},
    {"PowerSaveBlock", content::RegisterPowerSaveBlocker},
    {"RegisterImeAdapter", content::RegisterImeAdapter},
    {"ScreenOrientationProvider",
     content::ScreenOrientationProviderAndroid::Register},
    {"SensorManagerAndroid", content::SensorManagerAndroid::Register},
    {"SpeechRecognizerImplAndroid",
     content::SpeechRecognizerImplAndroid::RegisterSpeechRecognizer},
    {"TimeZoneMonitorAndroid", content::TimeZoneMonitorAndroid::Register},
    {"TouchEventSynthesizer",
     content::SyntheticGestureTargetAndroid::RegisterTouchEventSynthesizer},
    {"TracingControllerAndroid", content::RegisterTracingControllerAndroid},
    {"VibrationProvider", content::VibrationProviderAndroid::Register},
    {"WebContentsAndroid", content::WebContentsAndroid::Register},
    {"WebContentsObserverAndroid", content::RegisterWebContentsObserverAndroid},
    {"WebViewStatics", content::RegisterWebViewStatics},
    {"HashSet", content::RegisterHashSet},
};

}  // namespace

namespace content {
namespace android {

bool RegisterBrowserJni() {
  return RegisterNativeMethods(kContentRegisteredMethods,
                               arraysize(kContentRegisteredMethods));
}

}  // namespace android
}  // namespace content
