// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/content_view_render_view.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
//#include "base/android/scoped_java_ref.h"
#include "base/bind.h"
#include "base/lazy_instance.h"
#include "base/memory/scoped_ptr.h"
#include "base/message_loop/message_loop.h"
#include "cc/layers/layer.h"
#include "content/browser/elastos/content_view_core_impl.h"
#include "content/public/browser/elastos/compositor.h"
#include "content/public/browser/elastos/content_view_layer_renderer.h"
#include "content/public/browser/elastos/layer_tree_build_helper.h"
#include "content/api/ContentViewRenderView_api.h"
#include "ui/gfx/elastos/java_bitmap.h"
#include "ui/gfx/size.h"

//#include <android/bitmap.h>
//#include <android/native_window_jni.h>
//#include "elastos/native_window.h"

//using base::android::ScopedJavaLocalRef;

namespace content {

namespace {

class LayerTreeBuildHelperImpl : public LayerTreeBuildHelper {
 public:
  LayerTreeBuildHelperImpl() {}
  virtual ~LayerTreeBuildHelperImpl() {}

  virtual scoped_refptr<cc::Layer> GetLayerTree(
      scoped_refptr<cc::Layer> content_root_layer) OVERRIDE {
    return content_root_layer;
  }

 private:
  DISALLOW_COPY_AND_ASSIGN(LayerTreeBuildHelperImpl);
};

}  // anonymous namespace

// static
bool ContentViewRenderView::RegisterContentViewRenderView() {
  return RegisterNativesImpl();
}

ContentViewRenderView::ContentViewRenderView(IInterface* obj,
                                             gfx::NativeWindow root_window)
    : layer_tree_build_helper_(new LayerTreeBuildHelperImpl()),
      root_window_(root_window),
      current_surface_format_(0) {
  //java_obj_.Reset(env, obj);
  java_obj_ =  obj;
}

ContentViewRenderView::~ContentViewRenderView() {
}

void ContentViewRenderView::SetLayerTreeBuildHelper(IInterface* obj,
                                                    Elastos::Handle64 native_build_helper) {
  CHECK(native_build_helper);

  LayerTreeBuildHelper* build_helper =
      reinterpret_cast<LayerTreeBuildHelper*>(native_build_helper);
  layer_tree_build_helper_.reset(build_helper);
}
// static
static Elastos::Handle64 Init(IInterface* obj,
                  Elastos::Handle64 native_root_window) {
  gfx::NativeWindow root_window =
      reinterpret_cast<gfx::NativeWindow>(native_root_window);
  ContentViewRenderView* content_view_render_view =
      new ContentViewRenderView(obj, root_window);
  return reinterpret_cast<intptr_t>(content_view_render_view);
}

void ContentViewRenderView::Destroy(IInterface* obj) {
  delete this;
}

void ContentViewRenderView::SetCurrentContentViewCore(
    IInterface* obj, Elastos::Handle64 native_content_view_core) {
  InitCompositor();
  ContentViewCoreImpl* content_view_core =
      reinterpret_cast<ContentViewCoreImpl*>(native_content_view_core);
  compositor_->SetRootLayer(content_view_core
                                ? layer_tree_build_helper_->GetLayerTree(
                                      content_view_core->GetLayer())
                                : scoped_refptr<cc::Layer>());
}

void ContentViewRenderView::SurfaceCreated(IInterface* obj) {
  current_surface_format_ = 0;
  InitCompositor();
}

void ContentViewRenderView::SurfaceDestroyed(IInterface* obj) {
  compositor_->SetSurface(NULL);
  current_surface_format_ = 0;
}

void ContentViewRenderView::SurfaceChanged(IInterface* obj,
    Elastos::Int32 format, Elastos::Int32 width, Elastos::Int32 height, IInterface* surface) {
  if (current_surface_format_ != format) {
    current_surface_format_ = format;
    compositor_->SetSurface(surface);
  }
  compositor_->SetWindowBounds(gfx::Size(width, height));
}

void ContentViewRenderView::SetOverlayVideoMode(
    IInterface* obj, bool enabled) {
  compositor_->SetHasTransparentBackground(enabled);
}

void ContentViewRenderView::Layout() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //Java_ContentViewRenderView_onCompositorLayout(env, java_obj_.obj());
  DCHECK(sElaContentViewRenderViewCallback);
  sElaContentViewRenderViewCallback->elastos_ContentViewRenderView_onCompositorLayout(java_obj_.Get());
}

void ContentViewRenderView::OnSwapBuffersCompleted(int pending_swap_buffers) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //Java_ContentViewRenderView_onSwapBuffersCompleted(env, java_obj_.obj());
  DCHECK(sElaContentViewRenderViewCallback);
  sElaContentViewRenderViewCallback->elastos_ContentViewRenderView_onSwapBuffersCompleted(java_obj_.Get());
}

void ContentViewRenderView::InitCompositor() {
  if (!compositor_)
    compositor_.reset(Compositor::Create(this, root_window_));
}
}  // namespace content
