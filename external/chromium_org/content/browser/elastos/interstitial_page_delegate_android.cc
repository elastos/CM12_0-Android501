// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/interstitial_page_delegate_android.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
//#include "base/android/scoped_java_ref.h"
#include "content/public/browser/interstitial_page.h"
#include "content/api/InterstitialPageDelegateAndroid_api.h"

//using base::android::AttachCurrentThread;
//using base::android::ScopedJavaLocalRef;

namespace content {

InterstitialPageDelegateAndroid::InterstitialPageDelegateAndroid(
    IInterface* obj,
    const std::string& html_content)
    : weak_java_obj_(obj),
      html_content_(html_content),
      page_(NULL) {
}

InterstitialPageDelegateAndroid::~InterstitialPageDelegateAndroid() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = weak_java_obj_.get(env);
  Elastos::AutoPtr<IInterface> obj = weak_java_obj_.get();
  DCHECK(sElaInterstitialPageDelegateAndroidCallback);
  if (obj.Get())
    sElaInterstitialPageDelegateAndroidCallback->elastos_InterstitialPageDelegateAndroid_onNativeDestroyed(obj.Get());
    //Java_InterstitialPageDelegateAndroid_onNativeDestroyed(env, obj.obj());
}

void InterstitialPageDelegateAndroid::Proceed(IInterface* obj) {
  if (page_)
    page_->Proceed();
}

void InterstitialPageDelegateAndroid::DontProceed(IInterface* obj) {
  if (page_)
    page_->DontProceed();
}

std::string InterstitialPageDelegateAndroid::GetHTMLContents() {
  return html_content_;
}

void InterstitialPageDelegateAndroid::OnProceed() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = weak_java_obj_.get(env);
  Elastos::AutoPtr<IInterface> obj = weak_java_obj_.get();
  DCHECK(sElaInterstitialPageDelegateAndroidCallback);
  if (obj.Get())
    sElaInterstitialPageDelegateAndroidCallback->elastos_InterstitialPageDelegateAndroid_onProceed(obj.Get());
    //Java_InterstitialPageDelegateAndroid_onProceed(env, obj.obj());
}

void InterstitialPageDelegateAndroid::OnDontProceed() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = weak_java_obj_.get(env);
  Elastos::AutoPtr<IInterface> obj = weak_java_obj_.get();
  DCHECK(sElaInterstitialPageDelegateAndroidCallback);
  if (obj.Get())
    sElaInterstitialPageDelegateAndroidCallback->elastos_InterstitialPageDelegateAndroid_onDontProceed(obj.Get());
    //Java_InterstitialPageDelegateAndroid_onDontProceed(env, obj.obj());
}

void InterstitialPageDelegateAndroid::CommandReceived(
    const std::string& command) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = weak_java_obj_.get(env);
  Elastos::AutoPtr<IInterface> obj = weak_java_obj_.get();
  DCHECK(sElaInterstitialPageDelegateAndroidCallback);
  if (obj.Get()) {
    std::string sanitized_command(command);
    // The JSONified response has quotes, remove them.
    if (sanitized_command.length() > 1 && sanitized_command[0] == '"') {
      sanitized_command = sanitized_command.substr(
          1, sanitized_command.length() - 2);
    }

    //Java_InterstitialPageDelegateAndroid_commandReceived(
    sElaInterstitialPageDelegateAndroidCallback->elastos_InterstitialPageDelegateAndroid_commandReceived(
        obj.Get(),
        base::android::ConvertUTF8ToJavaString(sanitized_command));
  }
}

// static
bool InterstitialPageDelegateAndroid
    ::RegisterInterstitialPageDelegateAndroid() {
  return RegisterNativesImpl();
}

static Elastos::Handle64 Init(IInterface* obj, const Elastos::String& html_content) {
  InterstitialPageDelegateAndroid* delegate =
      new InterstitialPageDelegateAndroid(
          obj, base::android::ConvertJavaStringToUTF8(html_content));
  return reinterpret_cast<intptr_t>(delegate);
}

}  // namespace content
