// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CONTENT_BROWSER_ANDROID_CONTENT_READBACK_HANDLER_H_
#define CONTENT_BROWSER_ANDROID_CONTENT_READBACK_HANDLER_H_

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/elastos/api_weak_ref.h"
#include "base/callback.h"
#include "base/memory/weak_ptr.h"

class SkBitmap;

namespace cc {
class CopyOutputResult;
}

namespace content {

// Native side of the ContentReadbackHandler.java, which issues content
// readbacks from the Java side.
class ContentReadbackHandler {
 public:
  // Registers the JNI methods for ContentViewRender.
  static bool RegisterContentReadbackHandler();

  // Methods called from Java via JNI -----------------------------------------
  ContentReadbackHandler(IInterface* obj);
  void Destroy(IInterface* obj);
  void GetContentBitmap(IInterface* obj,
                        Elastos::Int32 readback_id,
                        Elastos::Float scale,
                        Elastos::Int32 config,
                        Elastos::Float x,
                        Elastos::Float y,
                        Elastos::Float width,
                        Elastos::Float height,
                        IInterface* content_view_core);
  void GetCompositorBitmap(IInterface* obj,
                           Elastos::Int32 readback_id,
                           Elastos::Int64 native_window_android);

 private:
  virtual ~ContentReadbackHandler();

  void OnFinishReadback(int readback_id,
                        bool success,
                        const SkBitmap& bitmap);

  //base::android::ScopedJavaGlobalRef<jobject> java_obj_;
  Elastos::AutoPtr<IInterface> java_obj_;
  base::WeakPtrFactory<ContentReadbackHandler> weak_factory_;

  DISALLOW_COPY_AND_ASSIGN(ContentReadbackHandler);
};

}  // namespace content

#endif  // CONTENT_BROWSER_ANDROID_CONTENT_READBACK_HANDLER_H_
