// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/content_video_view.h"
#include "content/api/ContentVideoView_api.h"

#include "base/command_line.h"
#include "base/logging.h"
#include "base/message_loop/message_loop.h"
#include "content/browser/elastos/content_view_core_impl.h"
#include "content/browser/media/elastos/browser_media_player_manager.h"
#include "content/browser/power_save_blocker_impl.h"
#include "content/common/elastos/surface_texture_peer.h"
#include "content/public/common/content_switches.h"

//using base::android::AttachCurrentThread;
//using base::android::CheckException;
//using base::android::ScopedJavaGlobalRef;

namespace content {

namespace {
// There can only be one content video view at a time, this holds onto that
// singleton instance.
ContentVideoView* g_content_video_view = NULL;

}  // namespace

static Elastos::AutoPtr<IInterface> GetSingletonJavaContentVideoView() {
  if (g_content_video_view)
    return g_content_video_view->GetJavaObject();
  else
    return NULL;
}

bool ContentVideoView::RegisterContentVideoView() {
  return RegisterNativesImpl();
}

ContentVideoView* ContentVideoView::GetInstance() {
  return g_content_video_view;
}

ContentVideoView::ContentVideoView(
    BrowserMediaPlayerManager* manager)
    : manager_(manager),
      weak_factory_(this) {
  DCHECK(!g_content_video_view);
  j_content_video_view_ = CreateJavaObject();
  g_content_video_view = this;
  CreatePowerSaveBlocker();
}

ContentVideoView::~ContentVideoView() {
  DCHECK(g_content_video_view);
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> content_video_view = GetJavaObject(env);
  Elastos::AutoPtr<IInterface> content_video_view = GetJavaObject();
  if (content_video_view.Get()) {
    DCHECK(sElaContentVideoViewCallback);
    //Java_ContentVideoView_destroyContentVideoView(env,
    sElaContentVideoViewCallback->elastos_ContentVideoView_destroyContentVideoView(
        content_video_view.Get(), true);
    //j_content_video_view_.reset();
  }
  g_content_video_view = NULL;
}

void ContentVideoView::OpenVideo() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> content_video_view = GetJavaObject(env);
  Elastos::AutoPtr<IInterface> content_video_view = GetJavaObject();
  if (content_video_view.Get()) {
    CreatePowerSaveBlocker();
    DCHECK(sElaContentVideoViewCallback);
    //Java_ContentVideoView_openVideo(env, content_video_view.obj());
    sElaContentVideoViewCallback->elastos_ContentVideoView_openVideo(content_video_view.Get());
  }
}

void ContentVideoView::OnMediaPlayerError(int error_type) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> content_video_view = GetJavaObject(env);
  Elastos::AutoPtr<IInterface> content_video_view = GetJavaObject();
  if (content_video_view.Get()) {
    power_save_blocker_.reset();
    DCHECK(sElaContentVideoViewCallback);
    //Java_ContentVideoView_onMediaPlayerError(env, content_video_view.obj(), error_type);
    sElaContentVideoViewCallback->elastos_ContentVideoView_onMediaPlayerError(content_video_view.Get(), error_type);
  }
}

void ContentVideoView::OnVideoSizeChanged(int width, int height) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> content_video_view = GetJavaObject(env);
  Elastos::AutoPtr<IInterface> content_video_view = GetJavaObject();
  if (content_video_view.Get()) {
    //Java_ContentVideoView_onVideoSizeChanged(env, content_video_view.obj(),
    DCHECK(sElaContentVideoViewCallback);
    sElaContentVideoViewCallback->elastos_ContentVideoView_onVideoSizeChanged(content_video_view.Get(),
        width, height);
  }
}

void ContentVideoView::OnBufferingUpdate(int percent) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> content_video_view = GetJavaObject(env);
  Elastos::AutoPtr<IInterface> content_video_view = GetJavaObject();
  if (content_video_view.Get()) {
    DCHECK(sElaContentVideoViewCallback);
    //Java_ContentVideoView_onBufferingUpdate(env, content_video_view.obj(),
    sElaContentVideoViewCallback->elastos_ContentVideoView_onBufferingUpdate(content_video_view.Get(),
        percent);
  }
}

void ContentVideoView::OnPlaybackComplete() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> content_video_view = GetJavaObject(env);
  Elastos::AutoPtr<IInterface> content_video_view = GetJavaObject();
  if (content_video_view.Get()) {
    power_save_blocker_.reset();
    //Java_ContentVideoView_onPlaybackComplete(env, content_video_view.obj());
    DCHECK(sElaContentVideoViewCallback);
    sElaContentVideoViewCallback->elastos_ContentVideoView_onPlaybackComplete(content_video_view.Get());
  }
}

void ContentVideoView::OnExitFullscreen() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> content_video_view = GetJavaObject(env);
  Elastos::AutoPtr<IInterface> content_video_view = GetJavaObject();
  if (content_video_view.Get())
    DCHECK(sElaContentVideoViewCallback);
    //Java_ContentVideoView_onExitFullscreen(env, content_video_view.obj());
    sElaContentVideoViewCallback->elastos_ContentVideoView_onExitFullscreen(content_video_view.Get());
}

void ContentVideoView::UpdateMediaMetadata() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> content_video_view = GetJavaObject(env);
  Elastos::AutoPtr<IInterface> content_video_view = GetJavaObject();
  if (!content_video_view.Get())
    return;

  media::MediaPlayerAndroid* player = manager_->GetFullscreenPlayer();
  if (player && player->IsPlayerReady()) {
    DCHECK(sElaContentVideoViewCallback);
    //Java_ContentVideoView_onUpdateMediaMetadata(
    sElaContentVideoViewCallback->elastos_ContentVideoView_onUpdateMediaMetadata(
        content_video_view.Get(), player->GetVideoWidth(),
        player->GetVideoHeight(),
        static_cast<int>(player->GetDuration().InMilliseconds()),
        player->CanPause(),player->CanSeekForward(), player->CanSeekBackward());
  }
}

int ContentVideoView::GetVideoWidth(IInterface* obj) const {
  media::MediaPlayerAndroid* player = manager_->GetFullscreenPlayer();
  return player ? player->GetVideoWidth() : 0;
}

int ContentVideoView::GetVideoHeight(IInterface* obj) const {
  media::MediaPlayerAndroid* player = manager_->GetFullscreenPlayer();
  return player ? player->GetVideoHeight() : 0;
}

int ContentVideoView::GetDurationInMilliSeconds(IInterface* obj) const {
  media::MediaPlayerAndroid* player = manager_->GetFullscreenPlayer();
  return player ? player->GetDuration().InMilliseconds() : -1;
}

int ContentVideoView::GetCurrentPosition(IInterface* obj) const {
  media::MediaPlayerAndroid* player = manager_->GetFullscreenPlayer();
  return player ? player->GetCurrentTime().InMilliseconds() : 0;
}

bool ContentVideoView::IsPlaying(IInterface* obj) {
  media::MediaPlayerAndroid* player = manager_->GetFullscreenPlayer();
  return player ? player->IsPlaying() : false;
}

void ContentVideoView::SeekTo(IInterface* obj, Elastos::Int32 msec) {
  manager_->FullscreenPlayerSeek(msec);
}

void ContentVideoView::Play(IInterface* obj) {
  CreatePowerSaveBlocker();
  manager_->FullscreenPlayerPlay();
}

void ContentVideoView::Pause(IInterface* obj) {
  power_save_blocker_.reset();
  manager_->FullscreenPlayerPause();
}

void ContentVideoView::ExitFullscreen(
    IInterface*, Elastos::Boolean release_media_player) {
  power_save_blocker_.reset();
  j_content_video_view_.reset();
  manager_->ExitFullscreen(release_media_player);
}

void ContentVideoView::SetSurface(IInterface* obj, IInterface* surface) {
  manager_->SetVideoSurface(
      gfx::ScopedJavaSurface::AcquireExternalSurface(surface));
}

void ContentVideoView::RequestMediaMetadata(IInterface* obj) {
  base::MessageLoop::current()->PostTask(
      FROM_HERE,
      base::Bind(&ContentVideoView::UpdateMediaMetadata,
                 weak_factory_.GetWeakPtr()));
}

//ScopedJavaLocalRef<jobject> ContentVideoView::GetJavaObject(JNIEnv* env) {
Elastos::AutoPtr<IInterface> ContentVideoView::GetJavaObject() {
  return j_content_video_view_.get();
}

gfx::NativeView ContentVideoView::GetNativeView() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> content_video_view = GetJavaObject(env);
  Elastos::AutoPtr<IInterface> content_video_view = GetJavaObject();
  if (!content_video_view.Get())
    return NULL;

  DCHECK(sElaContentVideoViewCallback);
  //    Java_ContentVideoView_getNativeViewAndroid(env,
  return reinterpret_cast<gfx::NativeView>(
          sElaContentVideoViewCallback->elastos_ContentVideoView_getNativeViewAndroid(
                                                 content_video_view.Get()));

}

ObjectWeakGlobalRef ContentVideoView::CreateJavaObject() {
  ContentViewCoreImpl* content_view_core = manager_->GetContentViewCore();
  //JNIEnv* env = AttachCurrentThread();
  bool legacyMode = CommandLine::ForCurrentProcess()->HasSwitch(
      switches::kDisableOverlayFullscreenVideoSubtitle);
  DCHECK(sElaContentVideoViewCallback);
  //    Java_ContentVideoView_createContentVideoView(
  return ObjectWeakGlobalRef(
          sElaContentVideoViewCallback->elastos_ContentVideoView_createContentVideoView(
          content_view_core->GetContext().Get(),
          reinterpret_cast<intptr_t>(this),
          content_view_core->GetContentVideoViewClient().Get(),
          legacyMode).Get());
}

void ContentVideoView::CreatePowerSaveBlocker() {
  if (CommandLine::ForCurrentProcess()->HasSwitch(
      switches::kEnableContentVideoViewPowerSaveBlocker)) {
    // In fullscreen Clank reuses the power save blocker attached to the
    // container view that was created for embedded video. The WebView cannot
    // reuse that so we create a new blocker instead.
    if (power_save_blocker_) return;

    power_save_blocker_ = PowerSaveBlocker::Create(
        PowerSaveBlocker::kPowerSaveBlockPreventDisplaySleep,
        "Playing video").Pass();
    static_cast<PowerSaveBlockerImpl*>(power_save_blocker_.get())->
        InitDisplaySleepBlocker(GetNativeView());
  }
}
}  // namespace content
