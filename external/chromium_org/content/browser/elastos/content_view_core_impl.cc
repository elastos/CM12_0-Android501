// Copyright 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/content_view_core_impl.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_array.h"
#include "base/elastos/jni_string.h"
//#include "base/elastos/scoped_java_ref.h"
#include "base/command_line.h"
#include "base/json/json_writer.h"
#include "base/logging.h"
#include "base/metrics/histogram.h"
#include "base/strings/utf_string_conversions.h"
#include "base/values.h"
#include "cc/layers/layer.h"
#include "cc/layers/solid_color_layer.h"
#include "cc/output/begin_frame_args.h"
#include "content/browser/elastos/gesture_event_type.h"
#include "content/browser/elastos/interstitial_page_delegate_android.h"
//#include "content/browser/elastos/java/gin_java_bridge_dispatcher_host.h"//TODO js
#include "content/browser/elastos/load_url_params.h"
#include "content/browser/frame_host/interstitial_page_impl.h"
#include "content/browser/frame_host/navigation_controller_impl.h"
#include "content/browser/frame_host/navigation_entry_impl.h"
#include "content/browser/geolocation/geolocation_dispatcher_host.h"
#include "content/browser/media/media_web_contents_observer.h"
#include "content/browser/renderer_host/compositor_impl_elastos.h"
#include "content/browser/renderer_host/input/motion_event_elastos.h"
#include "content/browser/renderer_host/input/web_input_event_builders_elastos.h"
#include "content/browser/renderer_host/input/web_input_event_util.h"
#include "content/browser/renderer_host/render_view_host_impl.h"
#include "content/browser/renderer_host/render_widget_host_impl.h"
#include "content/browser/renderer_host/render_widget_host_view_elastos.h"
#include "content/browser/screen_orientation/screen_orientation_dispatcher_host.h"
#include "content/browser/ssl/ssl_host_state.h"
#include "content/browser/web_contents/web_contents_view_elastos.h"
#include "content/common/frame_messages.h"
#include "content/common/input/web_input_event_traits.h"
#include "content/common/input_messages.h"
#include "content/common/view_messages.h"
#include "content/public/browser/browser_accessibility_state.h"
#include "content/public/browser/browser_context.h"
#include "content/public/browser/favicon_status.h"
#include "content/public/browser/render_frame_host.h"
#include "content/public/browser/web_contents.h"
#include "content/public/common/content_client.h"
#include "content/public/common/content_switches.h"
#include "content/public/common/menu_item.h"
#include "content/public/common/page_transition_types.h"
#include "content/public/common/user_agent.h"
#include "content/api/ContentViewCore_api.h"
#include "third_party/WebKit/public/web/WebInputEvent.h"
#include "ui/base/elastos/view_android.h"
#include "ui/base/elastos/window_android.h"
#include "ui/gfx/elastos/java_bitmap.h"
#include "ui/gfx/screen.h"
#include "ui/gfx/size_conversions.h"
#include "ui/gfx/size_f.h"

//using base::android::AttachCurrentThread;
using base::android::ConvertJavaStringToUTF16;
using base::android::ConvertJavaStringToUTF8;
using base::android::ConvertUTF16ToJavaString;
using base::android::ConvertUTF8ToJavaString;
//using base::android::ScopedJavaGlobalRef;
//using base::android::ScopedJavaLocalRef;
using blink::WebGestureEvent;
using blink::WebInputEvent;

// Describes the type and enabled state of a select popup item.
namespace {

enum {
#define DEFINE_POPUP_ITEM_TYPE(name, value) POPUP_ITEM_TYPE_##name = value,
#include "content/browser/elastos/popup_item_type_list.h"
#undef DEFINE_POPUP_ITEM_TYPE
};

} //namespace

namespace content {

namespace {

const void* kContentViewUserDataKey = &kContentViewUserDataKey;

int GetRenderProcessIdFromRenderViewHost(RenderViewHost* host) {
  DCHECK(host);
  RenderProcessHost* render_process = host->GetProcess();
  DCHECK(render_process);
  if (render_process->HasConnection())
    return render_process->GetHandle();
  else
    return 0;
}

Elastos::AutoPtr<IInterface> CreateJavaRect(
    const gfx::Rect& rect) {
  DCHECK(sElaContentViewCoreCallback);
  //LLreturn ScopedJavaLocalRef<jobject>(
  //    Java_ContentViewCore_createRect(env,
  return sElaContentViewCoreCallback->elastos_ContentViewCore_createRect(
                                      static_cast<int>(rect.x()),
                                      static_cast<int>(rect.y()),
                                      static_cast<int>(rect.right()),
                                      static_cast<int>(rect.bottom()));
}

int ToGestureEventType(WebInputEvent::Type type) {
  switch (type) {
    case WebInputEvent::GestureScrollBegin:
      return SCROLL_START;
    case WebInputEvent::GestureScrollEnd:
      return SCROLL_END;
    case WebInputEvent::GestureScrollUpdate:
      return SCROLL_BY;
    case WebInputEvent::GestureFlingStart:
      return FLING_START;
    case WebInputEvent::GestureFlingCancel:
      return FLING_CANCEL;
    case WebInputEvent::GestureShowPress:
      return SHOW_PRESS;
    case WebInputEvent::GestureTap:
      return SINGLE_TAP_CONFIRMED;
    case WebInputEvent::GestureTapUnconfirmed:
      return SINGLE_TAP_UNCONFIRMED;
    case WebInputEvent::GestureTapDown:
      return TAP_DOWN;
    case WebInputEvent::GestureTapCancel:
      return TAP_CANCEL;
    case WebInputEvent::GestureDoubleTap:
      return DOUBLE_TAP;
    case WebInputEvent::GestureLongPress:
      return LONG_PRESS;
    case WebInputEvent::GestureLongTap:
      return LONG_TAP;
    case WebInputEvent::GesturePinchBegin:
      return PINCH_BEGIN;
    case WebInputEvent::GesturePinchEnd:
      return PINCH_END;
    case WebInputEvent::GesturePinchUpdate:
      return PINCH_BY;
    case WebInputEvent::GestureTwoFingerTap:
    case WebInputEvent::GestureScrollUpdateWithoutPropagation:
    default:
      NOTREACHED() << "Invalid source gesture type: "
                   << WebInputEventTraits::GetName(type);
      return -1;
  };
}

float GetPrimaryDisplayDeviceScaleFactor() {
  const gfx::Display& display =
      gfx::Screen::GetNativeScreen()->GetPrimaryDisplay();
  return display.device_scale_factor();
}

}  // namespace

// Enables a callback when the underlying WebContents is destroyed, to enable
// nulling the back-pointer.
class ContentViewCoreImpl::ContentViewUserData
    : public base::SupportsUserData::Data {
 public:
  explicit ContentViewUserData(ContentViewCoreImpl* content_view_core)
      : content_view_core_(content_view_core) {
  }

  virtual ~ContentViewUserData() {
    // TODO(joth): When chrome has finished removing the TabContents class (see
    // crbug.com/107201) consider inverting relationship, so ContentViewCore
    // would own WebContents. That effectively implies making the WebContents
    // destructor private on Android.
    delete content_view_core_;
  }

  ContentViewCoreImpl* get() const { return content_view_core_; }

 private:
  // Not using scoped_ptr as ContentViewCoreImpl destructor is private.
  ContentViewCoreImpl* content_view_core_;

  DISALLOW_IMPLICIT_CONSTRUCTORS(ContentViewUserData);
};

// static
ContentViewCoreImpl* ContentViewCoreImpl::FromWebContents(
    content::WebContents* web_contents) {
  ContentViewCoreImpl::ContentViewUserData* data =
      reinterpret_cast<ContentViewCoreImpl::ContentViewUserData*>(
          web_contents->GetUserData(kContentViewUserDataKey));
  return data ? data->get() : NULL;
}

// static
ContentViewCore* ContentViewCore::FromWebContents(
    content::WebContents* web_contents) {
  return ContentViewCoreImpl::FromWebContents(web_contents);
}

// static
ContentViewCore* ContentViewCore::GetNativeContentViewCore(IInterface* obj) {
  //return reinterpret_cast<ContentViewCore*>(
  //    Java_ContentViewCore_getNativeContentViewCore(env, obj));
  DCHECK(sElaContentViewCoreCallback);
  return reinterpret_cast<ContentViewCore*>(
          sElaContentViewCoreCallback->elastos_ContentViewCore_getNativeContentViewCore(obj));
}

ContentViewCoreImpl::ContentViewCoreImpl(
    IInterface* obj,
    WebContents* web_contents,
    ui::ViewAndroid* view_android,
    ui::WindowAndroid* window_android,
    IInterface* java_bridge_retained_object_set)
    : WebContentsObserver(web_contents),
      java_ref_(obj),
      web_contents_(static_cast<WebContentsImpl*>(web_contents)),
      root_layer_(cc::SolidColorLayer::Create()),
      dpi_scale_(GetPrimaryDisplayDeviceScaleFactor()),
      view_android_(view_android),
      window_android_(window_android),
      device_orientation_(0),
      accessibility_enabled_(false) {
  CHECK(web_contents) <<
      "A ContentViewCoreImpl should be created with a valid WebContents.";

  DCHECK(sElaContentViewCoreCallback);
  root_layer_->SetBackgroundColor(GetBackgroundColor(obj));
  //gfx::Size physical_size(
  //    Java_ContentViewCore_getPhysicalBackingWidthPix(env, obj),
  //    Java_ContentViewCore_getPhysicalBackingHeightPix(env, obj));
  gfx::Size physical_size(
          sElaContentViewCoreCallback->elastos_ContentViewCore_getPhysicalBackingWidthPix(obj),
          sElaContentViewCoreCallback->elastos_ContentViewCore_getPhysicalBackingHeightPix(obj)
          );
  root_layer_->SetBounds(physical_size);
  root_layer_->SetIsDrawable(true);

  // Currently, the only use case we have for overriding a user agent involves
  // spoofing a desktop Linux user agent for "Request desktop site".
  // Automatically set it for all WebContents so that it is available when a
  // NavigationEntry requires the user agent to be overridden.
  const char kLinuxInfoStr[] = "X11; Linux x86_64";
  std::string product = content::GetContentClient()->GetProduct();
  std::string spoofed_ua =
      BuildUserAgentFromOSAndProduct(kLinuxInfoStr, product);
  web_contents->SetUserAgentOverride(spoofed_ua);

  //js/car interoperation
  //java_bridge_dispatcher_host_.reset(
  //    new GinJavaBridgeDispatcherHost(web_contents,
  //                                    java_bridge_retained_object_set));
  //TODO js call java code
  LOG(INFO) << "leliang, content/browser/elastos/content_view_core_impl.cc, js call java";
  assert(false);
  InitWebContents();
}

ContentViewCoreImpl::~ContentViewCoreImpl() {
  //LLJNIEnv* env = base::android::AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  java_ref_.reset();
  if (j_obj.Get()) {
    DCHECK(sElaContentViewCoreCallback);
    //Java_ContentViewCore_onNativeContentViewCoreDestroyed(
    sElaContentViewCoreCallback->elastos_ContentViewCore_onNativeContentViewCoreDestroyed(
        j_obj.Get(), reinterpret_cast<intptr_t>(this));
  }
}

Elastos::AutoPtr<IInterface>
ContentViewCoreImpl::GetWebContentsAndroid(IInterface* obj) {
  return web_contents_->GetJavaWebContents();
}

void ContentViewCoreImpl::OnJavaContentViewCoreDestroyed(IInterface* obj) {
  //DCHECK(env->IsSameObject(java_ref_.get(env).obj(), obj));
  DCHECK(obj == java_ref_.get().Get());
  java_ref_.reset();
  //TODO leliang below added in Action
  /*
  //ActionsCode(hexibin, bugfix BUG00260970)
  // Java peer has gone, ContentViewCore is not functional and waits to
  // be destroyed with WebContents.
  // We need to reset WebContentsViewAndroid's reference, otherwise, there
  // could have call in when swapping the WebContents,
  // see http://crbug.com/383939 .
  DCHECK(web_contents_);
  static_cast<WebContentsViewAndroid*>(
      static_cast<WebContentsImpl*>(web_contents_)->GetView())->
          SetContentViewCore(NULL);
  */
}

void ContentViewCoreImpl::InitWebContents() {
  DCHECK(web_contents_);
  static_cast<WebContentsViewAndroid*>(
      static_cast<WebContentsImpl*>(web_contents_)->GetView())->
          SetContentViewCore(this);
  DCHECK(!web_contents_->GetUserData(kContentViewUserDataKey));
  web_contents_->SetUserData(kContentViewUserDataKey,
                             new ContentViewUserData(this));
}

void ContentViewCoreImpl::RenderViewReady() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (obj.Get())
  {
    DCHECK(sElaContentViewCoreCallback);
    //Java_ContentViewCore_onRenderProcessChange(env, obj.obj());
    sElaContentViewCoreCallback->elastos_ContentViewCore_onRenderProcessChange(obj.Get());
  }

  if (device_orientation_ != 0)
    SendOrientationChangeEventInternal();
}

void ContentViewCoreImpl::RenderViewHostChanged(RenderViewHost* old_host,
                                                RenderViewHost* new_host) {
  int old_pid = 0;
  if (old_host) {
    old_pid = GetRenderProcessIdFromRenderViewHost(old_host);

    RenderWidgetHostViewAndroid* view =
        static_cast<RenderWidgetHostViewAndroid*>(old_host->GetView());
    if (view)
      view->SetContentViewCore(NULL);

    view = static_cast<RenderWidgetHostViewAndroid*>(new_host->GetView());
    if (view)
      view->SetContentViewCore(this);
  }
  int new_pid = GetRenderProcessIdFromRenderViewHost(
      web_contents_->GetRenderViewHost());
  if (new_pid != old_pid) {
    // Notify the Java side that the renderer process changed.
    //JNIEnv* env = AttachCurrentThread();
    //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
    Elastos::AutoPtr<IInterface> obj = java_ref_.get();
    if (obj.Get()) {
      DCHECK(sElaContentViewCoreCallback);
      //Java_ContentViewCore_onRenderProcessChange(env, obj.obj());
      sElaContentViewCoreCallback->elastos_ContentViewCore_onRenderProcessChange(obj.Get());
    }
  }

  SetFocusInternal(HasFocus());
  SetAccessibilityEnabledInternal(accessibility_enabled_);
}

RenderWidgetHostViewAndroid*
    ContentViewCoreImpl::GetRenderWidgetHostViewAndroid() {
  RenderWidgetHostView* rwhv = NULL;
  if (web_contents_) {
    rwhv = web_contents_->GetRenderWidgetHostView();
    if (web_contents_->ShowingInterstitialPage()) {
      rwhv = static_cast<InterstitialPageImpl*>(
          web_contents_->GetInterstitialPage())->
              GetRenderViewHost()->GetView();
    }
  }
  return static_cast<RenderWidgetHostViewAndroid*>(rwhv);
}

Elastos::AutoPtr<IInterface> ContentViewCoreImpl::GetJavaObject() {
  //JNIEnv* env = AttachCurrentThread();
  //return java_ref_.get(env);
  return java_ref_.get();
}

Elastos::Int32 ContentViewCoreImpl::GetBackgroundColor(IInterface* obj) {
  RenderWidgetHostViewAndroid* rwhva = GetRenderWidgetHostViewAndroid();
  if (!rwhva)
    return SK_ColorWHITE;
  return rwhva->GetCachedBackgroundColor();
}

void ContentViewCoreImpl::OnHide(IInterface* obj) {
  Hide();
}

void ContentViewCoreImpl::OnShow(IInterface* obj) {
  Show();
}

void ContentViewCoreImpl::Show() {
  GetWebContents()->WasShown();
}

void ContentViewCoreImpl::Hide() {
  GetWebContents()->WasHidden();
  //TODO leliang update in Action
  //ActionsCode(hexibin, bugfix BUG00262636)
  //SetIsPaused(true);
  PauseVideo();
}
//TODO leliang update in Action
//ActionsCode(hexibin, bugfix BUG00262636)
//void ContentViewCoreImpl::SetIsPaused(bool paused) {
void ContentViewCoreImpl::PauseVideo() {
  RenderViewHostImpl* rvhi = static_cast<RenderViewHostImpl*>(
      web_contents_->GetRenderViewHost());
  if (rvhi)
    rvhi->media_web_contents_observer()->PauseVideo();
    //TODO leliang update in Actionrvhi->media_web_contents_observer()->SetIsPaused(paused);
}

void ContentViewCoreImpl::PauseOrResumeGeolocation(bool should_pause) {
  web_contents_->geolocation_dispatcher_host()->PauseOrResume(should_pause);
}

void ContentViewCoreImpl::PauseOrResumeVideoCaptureStream(bool should_pause) {
  RenderViewHostImpl* rvhi = static_cast<RenderViewHostImpl*>(
      web_contents_->GetRenderViewHost());
  if (!rvhi)
    return;
  if (should_pause)
    rvhi->Send(new ViewMsg_PauseVideoCaptureStream(rvhi->GetRoutingID()));
  else
    rvhi->Send(new ViewMsg_ResumeVideoCaptureStream(rvhi->GetRoutingID()));
}

// All positions and sizes are in CSS pixels.
// Note that viewport_width/height is a best effort based.
// ContentViewCore has the actual information about the physical viewport size.
void ContentViewCoreImpl::UpdateFrameInfo(
    const gfx::Vector2dF& scroll_offset,
    float page_scale_factor,
    const gfx::Vector2dF& page_scale_factor_limits,
    const gfx::SizeF& content_size,
    const gfx::SizeF& viewport_size,
    const gfx::Vector2dF& controls_offset,
    const gfx::Vector2dF& content_offset,
    float overdraw_bottom_height) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;

  if (window_android_) {
    window_android_->set_content_offset(
        gfx::ScaleVector2d(content_offset, dpi_scale_));
  }

  DCHECK(sElaContentViewCoreCallback);
  //Java_ContentViewCore_updateFrameInfo(
  sElaContentViewCoreCallback->elastos_ContentViewCore_updateFrameInfo(
      obj.Get(),
      scroll_offset.x(),
      scroll_offset.y(),
      page_scale_factor,
      page_scale_factor_limits.x(),
      page_scale_factor_limits.y(),
      content_size.width(),
      content_size.height(),
      viewport_size.width(),
      viewport_size.height(),
      controls_offset.y(),
      content_offset.y(),
      overdraw_bottom_height);
}

void ContentViewCoreImpl::SetTitle(const base::string16& title) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  //LLScopedJavaLocalRef<jstring> jtitle =
  Elastos::String jtitle =
      ConvertUTF8ToJavaString(base::UTF16ToUTF8(title));
  //Java_ContentViewCore_setTitle(env, obj.obj(), jtitle.obj());
  DCHECK(sElaContentViewCoreCallback);
  sElaContentViewCoreCallback->elastos_ContentViewCore_setTitle(obj.Get(), jtitle);
}

void ContentViewCoreImpl::OnBackgroundColorChanged(SkColor color) {
  root_layer_->SetBackgroundColor(color);

  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  //Java_ContentViewCore_onBackgroundColorChanged(env, obj.obj(), color);
  DCHECK(sElaContentViewCoreCallback);
  sElaContentViewCoreCallback->elastos_ContentViewCore_onBackgroundColorChanged(obj.Get(), color);
}

void ContentViewCoreImpl::ShowSelectPopupMenu(const gfx::Rect& bounds,
    const std::vector<MenuItem>& items, int selected_item, bool multiple) {
  //LLJNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  if (!j_obj.Get())
    return;

  //ScopedJavaLocalRef<jobject> bounds_rect(CreateJavaRect(env, bounds));
  Elastos::AutoPtr<IInterface> bounds_rect(CreateJavaRect(bounds));

  // For multi-select list popups we find the list of previous selections by
  // iterating through the items. But for single selection popups we take the
  // given |selected_item| as is.
  //ScopedJavaLocalRef<jintArray> selected_array;
  Elastos::AutoPtr< Elastos::ArrayOf<Elastos::Int32> > selected_array;
  if (multiple) {
    scoped_ptr<Elastos::Int32[]> native_selected_array(new Elastos::Int32[items.size()]);
    size_t selected_count = 0;
    for (size_t i = 0; i < items.size(); ++i) {
      if (items[i].checked)
        native_selected_array[selected_count++] = i;
    }
    selected_array = Elastos::ArrayOf<Elastos::Int32>::Alloc(selected_count);
    for (size_t i = 0; i < selected_count; ++i)
    {
        (*selected_array)[i] = native_selected_array[i];
    }

    //selected_array = ScopedJavaLocalRef<jintArray>(env, env->NewIntArray(selected_count));
    //env->SetIntArrayRegion(selected_array.obj(), 0, selected_count, native_selected_array.get());
  } else {
    //selected_array = ScopedJavaLocalRef<jintArray>(env, env->NewIntArray(1));
    selected_array = Elastos::ArrayOf<Elastos::Int32>::Alloc(1);
    (*selected_array)[0] = selected_item;
    //jint value = selected_item;
    //env->SetIntArrayRegion(selected_array.obj(), 0, 1, &value);
  }

  //ScopedJavaLocalRef<jintArray> enabled_array(env, env->NewIntArray(items.size()));
  Elastos::AutoPtr< Elastos::ArrayOf<Elastos::Int32> > enabled_array = Elastos::ArrayOf<Elastos::Int32>::Alloc(items.size());
  std::vector<base::string16> labels;
  labels.reserve(items.size());
  for (size_t i = 0; i < items.size(); ++i) {
    labels.push_back(items[i].label);
    Elastos::Int32 enabled =
        (items[i].type == MenuItem::GROUP ? POPUP_ITEM_TYPE_GROUP :
            (items[i].enabled ? POPUP_ITEM_TYPE_ENABLED :
                POPUP_ITEM_TYPE_DISABLED));
    //env->SetIntArrayRegion(enabled_array.obj(), i, 1, &enabled);
    (*enabled_array)[i] = enabled;
  }
  //ScopedJavaLocalRef<jobjectArray> items_array(base::android::ToJavaArrayOfStrings(env, labels));
  Elastos::AutoPtr<Elastos::ArrayOf<Elastos::String> > items_array = base::android::ToJavaArrayOfStrings(labels);
  DCHECK(sElaContentViewCoreCallback);
  //Java_ContentViewCore_showSelectPopup(env, j_obj.obj(),
  sElaContentViewCoreCallback->elastos_ContentViewCore_showSelectPopup(j_obj.Get(),
                                       bounds_rect.Get(),
                                       items_array.Get(),
                                       enabled_array.Get(),
                                       multiple,
                                       selected_array.Get());
}

void ContentViewCoreImpl::HideSelectPopupMenu() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  if (j_obj.Get())
  {
    //Java_ContentViewCore_hideSelectPopup(env, j_obj.obj());
    DCHECK(sElaContentViewCoreCallback);
    sElaContentViewCoreCallback->elastos_ContentViewCore_hideSelectPopup(j_obj.Get());
  }
}

void ContentViewCoreImpl::OnGestureEventAck(const blink::WebGestureEvent& event,
                                            InputEventAckState ack_result) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  if (!j_obj.Get())
    return;

  DCHECK(sElaContentViewCoreCallback);
  switch (event.type) {
    case WebInputEvent::GestureFlingStart:
      if (ack_result == INPUT_EVENT_ACK_STATE_CONSUMED) {
        // The view expects the fling velocity in pixels/s.
        //Java_ContentViewCore_onFlingStartEventConsumed(env, j_obj.obj(),
        sElaContentViewCoreCallback->elastos_ContentViewCore_onFlingStartEventConsumed(j_obj.Get(),
            event.data.flingStart.velocityX * dpi_scale(),
            event.data.flingStart.velocityY * dpi_scale());
      } else {
        // If a scroll ends with a fling, a SCROLL_END event is never sent.
        // However, if that fling went unconsumed, we still need to let the
        // listeners know that scrolling has ended.
        //Java_ContentViewCore_onScrollEndEventAck(env, j_obj.obj());
        sElaContentViewCoreCallback->elastos_ContentViewCore_onScrollEndEventAck(j_obj.Get());
      }

      if (ack_result == INPUT_EVENT_ACK_STATE_NO_CONSUMER_EXISTS) {
        // The view expects the fling velocity in pixels/s.
        //Java_ContentViewCore_onFlingStartEventHadNoConsumer(env, j_obj.obj(),
        sElaContentViewCoreCallback->elastos_ContentViewCore_onFlingStartEventHadNoConsumer(j_obj.Get(),
            event.data.flingStart.velocityX * dpi_scale(),
            event.data.flingStart.velocityY * dpi_scale());
      }
      break;
    case WebInputEvent::GestureFlingCancel:
      //Java_ContentViewCore_onFlingCancelEventAck(env, j_obj.obj());
      sElaContentViewCoreCallback->elastos_ContentViewCore_onFlingCancelEventAck(j_obj.Get());
      break;
    case WebInputEvent::GestureScrollBegin:
      //Java_ContentViewCore_onScrollBeginEventAck(env, j_obj.obj());
      sElaContentViewCoreCallback->elastos_ContentViewCore_onScrollBeginEventAck(j_obj.Get());
      break;
    case WebInputEvent::GestureScrollUpdate:
      if (ack_result == INPUT_EVENT_ACK_STATE_CONSUMED)
        sElaContentViewCoreCallback->elastos_ContentViewCore_onScrollUpdateGestureConsumed(j_obj.Get());
        //Java_ContentViewCore_onScrollUpdateGestureConsumed(env, j_obj.obj());
      break;
    case WebInputEvent::GestureScrollEnd:
      //Java_ContentViewCore_onScrollEndEventAck(env, j_obj.obj());
      sElaContentViewCoreCallback->elastos_ContentViewCore_onScrollEndEventAck(j_obj.Get());
      break;
    case WebInputEvent::GesturePinchBegin:
      //Java_ContentViewCore_onPinchBeginEventAck(env, j_obj.obj());
      sElaContentViewCoreCallback->elastos_ContentViewCore_onPinchBeginEventAck(j_obj.Get());
      break;
    case WebInputEvent::GesturePinchEnd:
      //Java_ContentViewCore_onPinchEndEventAck(env, j_obj.obj());
      sElaContentViewCoreCallback->elastos_ContentViewCore_onPinchEndEventAck(j_obj.Get());
      break;
    case WebInputEvent::GestureTap:
      //Java_ContentViewCore_onSingleTapEventAck( env,
      sElaContentViewCoreCallback->elastos_ContentViewCore_onSingleTapEventAck(
          j_obj.Get(),
          ack_result == INPUT_EVENT_ACK_STATE_CONSUMED,
          event.x * dpi_scale(),
          event.y * dpi_scale());
      break;
    case WebInputEvent::GestureDoubleTap:
      //Java_ContentViewCore_onDoubleTapEventAck(env, j_obj.obj());
      sElaContentViewCoreCallback->elastos_ContentViewCore_onDoubleTapEventAck(j_obj.Get());
      break;
    default:
      break;
  }
}

bool ContentViewCoreImpl::FilterInputEvent(const blink::WebInputEvent& event) {
  if (event.type != WebInputEvent::GestureTap &&
      event.type != WebInputEvent::GestureDoubleTap &&
      event.type != WebInputEvent::GestureLongTap &&
      event.type != WebInputEvent::GestureLongPress)
    return false;

  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  if (!j_obj.Get())
    return false;

  const blink::WebGestureEvent& gesture =
      static_cast<const blink::WebGestureEvent&>(event);
  int gesture_type = ToGestureEventType(event.type);
  //return Java_ContentViewCore_filterTapOrPressEvent(env,
  DCHECK(sElaContentViewCoreCallback);
  return sElaContentViewCoreCallback->elastos_ContentViewCore_filterTapOrPressEvent(
                                                    j_obj.Get(),
                                                    gesture_type,
                                                    gesture.x * dpi_scale(),
                                                    gesture.y * dpi_scale());

  // TODO(jdduke): Also report double-tap UMA, crbug/347568.
}

bool ContentViewCoreImpl::HasFocus() {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return false;
  //return Java_ContentViewCore_hasFocus(env, obj.obj());
  DCHECK(sElaContentViewCoreCallback);
  return sElaContentViewCoreCallback->elastos_ContentViewCore_hasFocus(obj.Get());
}

void ContentViewCoreImpl::OnSelectionChanged(const std::string& text) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  //ScopedJavaLocalRef<jstring> jtext = ConvertUTF8ToJavaString(env, text);
  Elastos::String jtext = ConvertUTF8ToJavaString(text);
  DCHECK(sElaContentViewCoreCallback);
  //Java_ContentViewCore_onSelectionChanged(env, obj.obj(), jtext.obj());
  sElaContentViewCoreCallback->elastos_ContentViewCore_onSelectionChanged(obj.Get(), jtext);
}

void ContentViewCoreImpl::OnSelectionBoundsChanged(
    const ViewHostMsg_SelectionBounds_Params& params) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  Elastos::AutoPtr<IInterface> anchor_rect_dip(
      CreateJavaRect(params.anchor_rect));
  Elastos::AutoPtr<IInterface> focus_rect_dip(
      CreateJavaRect(params.focus_rect));
  //Java_ContentViewCore_onSelectionBoundsChanged(env, obj.obj(),
  DCHECK(sElaContentViewCoreCallback);
  sElaContentViewCoreCallback->elastos_ContentViewCore_onSelectionBoundsChanged(obj.Get(),
                                                anchor_rect_dip.Get(),
                                                params.anchor_dir,
                                                focus_rect_dip.Get(),
                                                params.focus_dir,
                                                params.is_anchor_first);
}

void ContentViewCoreImpl::ShowPastePopup(int x_dip, int y_dip) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  DCHECK(sElaContentViewCoreCallback);
  //Java_ContentViewCore_showPastePopup(env, obj.obj(),
  sElaContentViewCoreCallback->elastos_ContentViewCore_showPastePopup(obj.Get(),
                                      static_cast<Elastos::Int32>(x_dip),
                                      static_cast<Elastos::Int32>(y_dip));
}

void ContentViewCoreImpl::GetScaledContentBitmap(
    float scale,
    Elastos::Int32 jbitmap_config,
    gfx::Rect src_subrect,
    const base::Callback<void(bool, const SkBitmap&)>& result_callback) {
  RenderWidgetHostViewAndroid* view = GetRenderWidgetHostViewAndroid();
  if (!view) {
    result_callback.Run(false, SkBitmap());
    return;
  }
  SkBitmap::Config skbitmap_format = gfx::ConvertToSkiaConfig(jbitmap_config);
  view->GetScaledContentBitmap(scale, skbitmap_format, src_subrect,
      result_callback);
}

void ContentViewCoreImpl::StartContentIntent(const GURL& content_url) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  if (!j_obj.Get())
    return;
  Elastos::String jcontent_url = ConvertUTF8ToJavaString(content_url.spec());
  //Java_ContentViewCore_startContentIntent(env,
  DCHECK(sElaContentViewCoreCallback);
  sElaContentViewCoreCallback->elastos_ContentViewCore_startContentIntent(
                                          j_obj.Get(),
                                          jcontent_url);
}

void ContentViewCoreImpl::ShowDisambiguationPopup(
    const gfx::Rect& target_rect,
    const SkBitmap& zoomed_bitmap) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;

  Elastos::AutoPtr<IInterface> rect_object(CreateJavaRect(target_rect));

  Elastos::AutoPtr<IInterface> java_bitmap = gfx::ConvertToJavaBitmap(&zoomed_bitmap);
  //DCHECK(!java_bitmap.is_null());
  DCHECK(java_bitmap.Get());

  DCHECK(sElaContentViewCoreCallback);
  //Java_ContentViewCore_showDisambiguationPopup(env,
  sElaContentViewCoreCallback->elastos_ContentViewCore_showDisambiguationPopup(
                                               obj.Get(),
                                               rect_object.Get(),
                                               java_bitmap.Get());
}

Elastos::AutoPtr<IInterface> ContentViewCoreImpl::CreateTouchEventSynthesizer() {
  //JNIEnv* env = AttachCurrentThread();

  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj)
    return NULL;//return ScopedJavaLocalRef<jobject>();
  //return Java_ContentViewCore_createTouchEventSynthesizer(env, obj.obj());
  DCHECK(sElaContentViewCoreCallback);
  return sElaContentViewCoreCallback->elastos_ContentViewCore_createTouchEventSynthesizer(obj.Get());
}

Elastos::AutoPtr<IInterface> ContentViewCoreImpl::GetContentVideoViewClient() {
  //JNIEnv* env = AttachCurrentThread();

  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return NULL;

  //return Java_ContentViewCore_getContentVideoViewClient(env, obj.obj());
  DCHECK(sElaContentViewCoreCallback);
  return sElaContentViewCoreCallback->elastos_ContentViewCore_getContentVideoViewClient(obj.Get());
}

Elastos::AutoPtr<IInterface> ContentViewCoreImpl::GetContext() {
  //JNIEnv* env = AttachCurrentThread();

  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return NULL;

  //return Java_ContentViewCore_getContext(env, obj.obj());
  DCHECK(sElaContentViewCoreCallback);
  return sElaContentViewCoreCallback->elastos_ContentViewCore_getContext(obj.Get());
}

bool ContentViewCoreImpl::ShouldBlockMediaRequest(const GURL& url) {
  //JNIEnv* env = AttachCurrentThread();

  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return true;
  Elastos::String j_url = ConvertUTF8ToJavaString(url.spec());
  //return Java_ContentViewCore_shouldBlockMediaRequest(env, obj.obj(), j_url.obj());
  DCHECK(sElaContentViewCoreCallback);
  return sElaContentViewCoreCallback->elastos_ContentViewCore_shouldBlockMediaRequest(obj.Get(), j_url);
}

void ContentViewCoreImpl::DidStopFlinging() {
  //JNIEnv* env = AttachCurrentThread();

  //ScopedJavaLocalRef<jobject> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (obj.Get())
  {
    //Java_ContentViewCore_onNativeFlingStopped(env, obj.obj());
    DCHECK(sElaContentViewCoreCallback);
    sElaContentViewCoreCallback->elastos_ContentViewCore_onNativeFlingStopped(obj.Get());
  }
}

gfx::Size ContentViewCoreImpl::GetViewSize() const {
  gfx::Size size = GetViewportSizeDip();
  gfx::Size offset = GetViewportSizeOffsetDip();
  size.Enlarge(-offset.width(), -offset.height());
  return size;
}

gfx::Size ContentViewCoreImpl::GetPhysicalBackingSize() const {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  if (!j_obj.Get())
    return gfx::Size();
  DCHECK(sElaContentViewCoreCallback);
  return gfx::Size(
          sElaContentViewCoreCallback->elastos_ContentViewCore_getPhysicalBackingWidthPix(j_obj.Get()),
          sElaContentViewCoreCallback->elastos_ContentViewCore_getPhysicalBackingHeightPix(j_obj.Get())
          );
      //Java_ContentViewCore_getPhysicalBackingWidthPix(env, j_obj.obj()),
      //Java_ContentViewCore_getPhysicalBackingHeightPix(env, j_obj.obj()));
}

gfx::Size ContentViewCoreImpl::GetViewportSizePix() const {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  if (!j_obj.Get())
    return gfx::Size();
  DCHECK(sElaContentViewCoreCallback);
  return gfx::Size(
          sElaContentViewCoreCallback->elastos_ContentViewCore_getViewportWidthPix(j_obj.Get()),
          sElaContentViewCoreCallback->elastos_ContentViewCore_getViewportHeightPix(j_obj.Get())
          );
      //Java_ContentViewCore_getViewportWidthPix(env, j_obj.obj()),
      //Java_ContentViewCore_getViewportHeightPix(env, j_obj.obj()));
}

gfx::Size ContentViewCoreImpl::GetViewportSizeOffsetPix() const {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  if (!j_obj.Get())
    return gfx::Size();
  DCHECK(sElaContentViewCoreCallback);
  return gfx::Size(
        sElaContentViewCoreCallback->elastos_ContentViewCore_getViewportSizeOffsetWidthPix(j_obj.Get()),
        sElaContentViewCoreCallback->elastos_ContentViewCore_getViewportSizeOffsetHeightPix(j_obj.Get())
          );
      //Java_ContentViewCore_getViewportSizeOffsetWidthPix(env, j_obj.obj()),
      //Java_ContentViewCore_getViewportSizeOffsetHeightPix(env, j_obj.obj()));
}

gfx::Size ContentViewCoreImpl::GetViewportSizeDip() const {
  return gfx::ToCeiledSize(
      gfx::ScaleSize(GetViewportSizePix(), 1.0f / dpi_scale()));
}

gfx::Size ContentViewCoreImpl::GetViewportSizeOffsetDip() const {
  return gfx::ToCeiledSize(
      gfx::ScaleSize(GetViewportSizeOffsetPix(), 1.0f / dpi_scale()));
}

float ContentViewCoreImpl::GetOverdrawBottomHeightDip() const {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> j_obj = java_ref_.get();
  if (!j_obj.Get())
    return 0.f;
  //return Java_ContentViewCore_getOverdrawBottomHeightPix(env, j_obj.obj()) / dpi_scale();
  DCHECK(sElaContentViewCoreCallback);
  return sElaContentViewCoreCallback->elastos_ContentViewCore_getOverdrawBottomHeightPix(
          j_obj.Get())/dpi_scale();
}

void ContentViewCoreImpl::AttachLayer(scoped_refptr<cc::Layer> layer) {
  root_layer_->AddChild(layer);
  root_layer_->SetIsDrawable(false);
}

void ContentViewCoreImpl::RemoveLayer(scoped_refptr<cc::Layer> layer) {
  layer->RemoveFromParent();

  if (!root_layer_->children().size())
    root_layer_->SetIsDrawable(true);
}

void ContentViewCoreImpl::LoadUrl(
    NavigationController::LoadURLParams& params) {
  GetWebContents()->GetController().LoadURLWithParams(params);
}

ui::ViewAndroid* ContentViewCoreImpl::GetViewAndroid() const {
  // view_android_ should never be null for Chrome.
  DCHECK(view_android_);
  return view_android_;
}

ui::WindowAndroid* ContentViewCoreImpl::GetWindowAndroid() const {
  // This should never be NULL for Chrome, but will be NULL for WebView.
  DCHECK(window_android_);
  return window_android_;
}

scoped_refptr<cc::Layer> ContentViewCoreImpl::GetLayer() const {
  return root_layer_.get();
}

// ----------------------------------------------------------------------------
// Methods called from Java via JNI
// ----------------------------------------------------------------------------

void ContentViewCoreImpl::SelectPopupMenuItems(IInterface* obj,
                                               Elastos::ArrayOf<Elastos::Int32>* indices) {
  RenderViewHostImpl* rvhi = static_cast<RenderViewHostImpl*>(
      web_contents_->GetRenderViewHost());
  DCHECK(rvhi);
  if (indices == NULL) {
    rvhi->DidCancelPopupMenu();
    return;
  }

  //int selected_count = env->GetArrayLength(indices);
  int selected_count = indices->GetLength();
  std::vector<int> selected_indices;
  //jint* indices_ptr = env->GetIntArrayElements(indices, NULL);
  Elastos::Int32* indices_ptr = indices->GetPayload();
  for (int i = 0; i < selected_count; ++i)
    selected_indices.push_back(indices_ptr[i]);
  //env->ReleaseIntArrayElements(indices, indices_ptr, JNI_ABORT);
  rvhi->DidSelectPopupMenuItems(selected_indices);
}

void ContentViewCoreImpl::LoadUrl(
    IInterface* obj,
    const Elastos::String& url,
    Elastos::Int32 load_url_type,
    Elastos::Int32 transition_type,
    const Elastos::String& j_referrer_url,
    Elastos::Int32 referrer_policy,
    Elastos::Int32 ua_override_option,
    const Elastos::String& extra_headers,
    Elastos::ArrayOf<Elastos::Byte>* post_data,
    const Elastos::String& base_url_for_data_url,
    const Elastos::String& virtual_url_for_data_url,
    Elastos::Boolean can_load_local_resources,
    Elastos::Boolean is_renderer_initiated) {
  DCHECK(url);
  NavigationController::LoadURLParams params(
      GURL(ConvertJavaStringToUTF8(url)));

  params.load_type = static_cast<NavigationController::LoadURLType>(
      load_url_type);
  params.transition_type = PageTransitionFromInt(transition_type);
  params.override_user_agent =
      static_cast<NavigationController::UserAgentOverrideOption>(
          ua_override_option);

  if (extra_headers)
    params.extra_headers = ConvertJavaStringToUTF8(extra_headers);

  if (post_data) {
    std::vector<uint8> http_body_vector;
    base::android::JavaByteArrayToByteVector(post_data, &http_body_vector);
    params.browser_initiated_post_data =
        base::RefCountedBytes::TakeVector(&http_body_vector);
  }

  if (base_url_for_data_url) {
    params.base_url_for_data_url =
        GURL(ConvertJavaStringToUTF8(base_url_for_data_url));
  }

  if (virtual_url_for_data_url) {
    params.virtual_url_for_data_url =
        GURL(ConvertJavaStringToUTF8(virtual_url_for_data_url));
  }

  params.can_load_local_resources = can_load_local_resources;
  if (j_referrer_url) {
    params.referrer = content::Referrer(
        GURL(ConvertJavaStringToUTF8(j_referrer_url)),
        static_cast<blink::WebReferrerPolicy>(referrer_policy));
  }

  params.is_renderer_initiated = is_renderer_initiated;

  LoadUrl(params);
}

Elastos::String ContentViewCoreImpl::GetURL(
    IInterface*) const {
  return ConvertUTF8ToJavaString(GetWebContents()->GetURL().spec());
}

Elastos::Boolean ContentViewCoreImpl::IsIncognito(IInterface* obj) {
  return GetWebContents()->GetBrowserContext()->IsOffTheRecord();
}

WebContents* ContentViewCoreImpl::GetWebContents() const {
  return web_contents_;
}

void ContentViewCoreImpl::SetFocus(IInterface* obj, Elastos::Boolean focused) {
  SetFocusInternal(focused);
}

void ContentViewCoreImpl::SetFocusInternal(bool focused) {
  if (!GetRenderWidgetHostViewAndroid())
    return;

  if (focused)
    GetRenderWidgetHostViewAndroid()->Focus();
  else
    GetRenderWidgetHostViewAndroid()->Blur();
}

void ContentViewCoreImpl::SendOrientationChangeEvent(IInterface* obj,
                                                     Elastos::Int32 orientation) {
  if (device_orientation_ != orientation) {
    device_orientation_ = orientation;
    SendOrientationChangeEventInternal();
  }
}

Elastos::Boolean ContentViewCoreImpl::OnTouchEvent( IInterface* obj,
                                           IInterface* motion_event,
                                           Elastos::Int64 time_ms,
                                           Elastos::Int32 android_action,
                                           Elastos::Int32 pointer_count,
                                           Elastos::Int32 history_size,
                                           Elastos::Int32 action_index,
                                           Elastos::Float pos_x_0,
                                           Elastos::Float pos_y_0,
                                           Elastos::Float pos_x_1,
                                           Elastos::Float pos_y_1,
                                           Elastos::Int32 pointer_id_0,
                                           Elastos::Int32 pointer_id_1,
                                           Elastos::Float touch_major_0,
                                           Elastos::Float touch_major_1,
                                           Elastos::Float raw_pos_x,
                                           Elastos::Float raw_pos_y,
                                           Elastos::Int32 android_tool_type_0,
                                           Elastos::Int32 android_tool_type_1,
                                           Elastos::Int32 android_button_state) {
  RenderWidgetHostViewAndroid* rwhv = GetRenderWidgetHostViewAndroid();
  // Avoid synthesizing a touch event if it cannot be forwarded.
  if (!rwhv)
    return false;

  MotionEventAndroid event(1.f / dpi_scale(),
                           motion_event,
                           time_ms,
                           android_action,
                           pointer_count,
                           history_size,
                           action_index,
                           pos_x_0,
                           pos_y_0,
                           pos_x_1,
                           pos_y_1,
                           pointer_id_0,
                           pointer_id_1,
                           touch_major_0,
                           touch_major_1,
                           raw_pos_x,
                           raw_pos_y,
                           android_tool_type_0,
                           android_tool_type_1,
                           android_button_state);

  return rwhv->OnTouchEvent(event);
}

float ContentViewCoreImpl::GetDpiScale() const {
  return dpi_scale_;
}

Elastos::Boolean ContentViewCoreImpl::SendMouseMoveEvent( IInterface* obj,
                                                 Elastos::Int64 time_ms,
                                                 Elastos::Float x,
                                                 Elastos::Float y) {
  RenderWidgetHostViewAndroid* rwhv = GetRenderWidgetHostViewAndroid();
  if (!rwhv)
    return false;

  blink::WebMouseEvent event = WebMouseEventBuilder::Build(
      WebInputEvent::MouseMove,
      blink::WebMouseEvent::ButtonNone,
      time_ms / 1000.0, x / dpi_scale(), y / dpi_scale(), 0, 1);

  rwhv->SendMouseEvent(event);
  return true;
}

Elastos::Boolean ContentViewCoreImpl::SendMouseWheelEvent( IInterface* obj,
                                                  Elastos::Int64 time_ms,
                                                  Elastos::Float x,
                                                  Elastos::Float y,
                                                  Elastos::Float vertical_axis) {
  RenderWidgetHostViewAndroid* rwhv = GetRenderWidgetHostViewAndroid();
  if (!rwhv)
    return false;

  WebMouseWheelEventBuilder::Direction direction;
  if (vertical_axis > 0) {
    direction = WebMouseWheelEventBuilder::DIRECTION_UP;
  } else if (vertical_axis < 0) {
    direction = WebMouseWheelEventBuilder::DIRECTION_DOWN;
  } else {
    return false;
  }
  blink::WebMouseWheelEvent event = WebMouseWheelEventBuilder::Build(
      direction, time_ms / 1000.0, x / dpi_scale(), y / dpi_scale());

  rwhv->SendMouseWheelEvent(event);
  return true;
}

WebGestureEvent ContentViewCoreImpl::MakeGestureEvent(
    WebInputEvent::Type type, int64 time_ms, float x, float y) const {
  return WebGestureEventBuilder::Build(
      type, time_ms / 1000.0, x / dpi_scale(), y / dpi_scale());
}

void ContentViewCoreImpl::SendGestureEvent(
    const blink::WebGestureEvent& event) {
  RenderWidgetHostViewAndroid* rwhv = GetRenderWidgetHostViewAndroid();
  if (rwhv)
    rwhv->SendGestureEvent(event);
}

void ContentViewCoreImpl::ScrollBegin(IInterface* obj,
                                      Elastos::Int64 time_ms,
                                      Elastos::Float x,
                                      Elastos::Float y,
                                      Elastos::Float hintx,
                                      Elastos::Float hinty) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GestureScrollBegin, time_ms, x, y);
  event.data.scrollBegin.deltaXHint = hintx / dpi_scale();
  event.data.scrollBegin.deltaYHint = hinty / dpi_scale();

  SendGestureEvent(event);
}

void ContentViewCoreImpl::ScrollEnd(IInterface* obj, Elastos::Int64 time_ms) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GestureScrollEnd, time_ms, 0, 0);
  SendGestureEvent(event);
}

void ContentViewCoreImpl::ScrollBy(IInterface* obj, Elastos::Int64 time_ms,
                                   Elastos::Float x, Elastos::Float y, Elastos::Float dx, Elastos::Float dy) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GestureScrollUpdate, time_ms, x, y);
  event.data.scrollUpdate.deltaX = -dx / dpi_scale();
  event.data.scrollUpdate.deltaY = -dy / dpi_scale();

  SendGestureEvent(event);
}

void ContentViewCoreImpl::FlingStart(IInterface* obj, Elastos::Int64 time_ms,
                                     Elastos::Float x, Elastos::Float y, Elastos::Float vx, Elastos::Float vy) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GestureFlingStart, time_ms, x, y);
  event.data.flingStart.velocityX = vx / dpi_scale();
  event.data.flingStart.velocityY = vy / dpi_scale();

  SendGestureEvent(event);
}

void ContentViewCoreImpl::FlingCancel(IInterface* obj, Elastos::Int64 time_ms) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GestureFlingCancel, time_ms, 0, 0);
  SendGestureEvent(event);
}

void ContentViewCoreImpl::SingleTap(IInterface* obj, Elastos::Int64 time_ms,
                                    Elastos::Float x, Elastos::Float y) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GestureTap, time_ms, x, y);
  event.data.tap.tapCount = 1;

  SendGestureEvent(event);
}

void ContentViewCoreImpl::DoubleTap(IInterface* obj, Elastos::Int64 time_ms,
                                    Elastos::Float x, Elastos::Float y) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GestureDoubleTap, time_ms, x, y);
  // Set the tap count to 1 even for DoubleTap, in order to be consistent with
  // double tap behavior on a mobile viewport. See crbug.com/234986 for context.
  event.data.tap.tapCount = 1;

  SendGestureEvent(event);
}

void ContentViewCoreImpl::LongPress(IInterface* obj, Elastos::Int64 time_ms,
                                    Elastos::Float x, Elastos::Float y) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GestureLongPress, time_ms, x, y);

  SendGestureEvent(event);
}

void ContentViewCoreImpl::PinchBegin(IInterface* obj, Elastos::Int64 time_ms,
                                     Elastos::Float x, Elastos::Float y) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GesturePinchBegin, time_ms, x, y);
  SendGestureEvent(event);
}

void ContentViewCoreImpl::PinchEnd(IInterface* obj, Elastos::Int64 time_ms) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GesturePinchEnd, time_ms, 0, 0);
  SendGestureEvent(event);
}

void ContentViewCoreImpl::PinchBy(IInterface* obj, Elastos::Int64 time_ms,
                                  Elastos::Float anchor_x, Elastos::Float anchor_y,
                                  Elastos::Float delta) {
  WebGestureEvent event = MakeGestureEvent(
      WebInputEvent::GesturePinchUpdate, time_ms, anchor_x, anchor_y);
  event.data.pinchUpdate.scale = delta;

  SendGestureEvent(event);
}

void ContentViewCoreImpl::SelectBetweenCoordinates(IInterface* obj,
                                                   Elastos::Float x1, Elastos::Float y1,
                                                   Elastos::Float x2, Elastos::Float y2) {
  if (!web_contents_)
    return;

  web_contents_->SelectRange(
      gfx::Point(x1 / dpi_scale(), y1 / dpi_scale()),
      gfx::Point(x2 / dpi_scale(), y2 / dpi_scale()));
}

void ContentViewCoreImpl::MoveCaret(IInterface* obj,
                                    Elastos::Float x, Elastos::Float y) {
  if (GetRenderWidgetHostViewAndroid()) {
    GetRenderWidgetHostViewAndroid()->MoveCaret(
        gfx::Point(x / dpi_scale(), y / dpi_scale()));
  }
}

void ContentViewCoreImpl::ResetGestureDetection(IInterface* obj) {
  RenderWidgetHostViewAndroid* rwhv = GetRenderWidgetHostViewAndroid();
  if (rwhv)
    rwhv->ResetGestureDetection();
}

void ContentViewCoreImpl::SetDoubleTapSupportEnabled(IInterface* obj,
                                                     Elastos::Boolean enabled) {
  RenderWidgetHostViewAndroid* rwhv = GetRenderWidgetHostViewAndroid();
  if (rwhv)
    rwhv->SetDoubleTapSupportEnabled(enabled);
}

void ContentViewCoreImpl::SetMultiTouchZoomSupportEnabled(IInterface* obj,
                                                          Elastos::Boolean enabled) {
  RenderWidgetHostViewAndroid* rwhv = GetRenderWidgetHostViewAndroid();
  if (rwhv)
    rwhv->SetMultiTouchZoomSupportEnabled(enabled);
}

void ContentViewCoreImpl::LoadIfNecessary(IInterface* obj) {
  web_contents_->GetController().LoadIfNecessary();
}

void ContentViewCoreImpl::RequestRestoreLoad(IInterface* obj) {
  web_contents_->GetController().SetNeedsReload();
}

void ContentViewCoreImpl::Reload(IInterface* obj,
                                 Elastos::Boolean check_for_repost) {
  if (web_contents_->GetController().NeedsReload())
    web_contents_->GetController().LoadIfNecessary();
  else
    web_contents_->GetController().Reload(check_for_repost);
}

void ContentViewCoreImpl::ReloadIgnoringCache(IInterface* obj,
                                              Elastos::Boolean check_for_repost) {
  web_contents_->GetController().ReloadIgnoringCache(check_for_repost);
}

void ContentViewCoreImpl::CancelPendingReload(IInterface* obj) {
  web_contents_->GetController().CancelPendingReload();
}

void ContentViewCoreImpl::ContinuePendingReload(IInterface* obj) {
  web_contents_->GetController().ContinuePendingReload();
}

void ContentViewCoreImpl::ClearHistory(IInterface* obj) {
  // TODO(creis): Do callers of this need to know if it fails?
  if (web_contents_->GetController().CanPruneAllButLastCommitted())
    web_contents_->GetController().PruneAllButLastCommitted();
}

void ContentViewCoreImpl::AddStyleSheetByURL(
    IInterface* obj, const Elastos::String& url) {
  if (!web_contents_)
    return;

  web_contents_->GetMainFrame()->Send(new FrameMsg_AddStyleSheetByURL(
      web_contents_->GetMainFrame()->GetRoutingID(),
      ConvertJavaStringToUTF8(url)));
}

void ContentViewCoreImpl::SetAllowJavascriptInterfacesInspection(
    IInterface* obj,
    Elastos::Boolean allow) {
  //js/car interoperation
  //TODO js call java code
  LOG(INFO) << "leliang, content/browser/elastos/content_view_core_impl.cc, js call java";
  assert(false);
  //java_bridge_dispatcher_host_->SetAllowObjectContentsInspection(allow);
}

void ContentViewCoreImpl::AddJavascriptInterface(
    IInterface* /* obj */,
    IInterface* object,
    const Elastos::String& name,
    IInterface* safe_annotation_clazz) {
  Elastos::AutoPtr<IInterface> scoped_object(object);
  Elastos::AutoPtr<IInterface> scoped_clazz(safe_annotation_clazz);
  //js/car interoperation
  //TODO js call java code
  LOG(INFO) << "leliang, content/browser/elastos/content_view_core_impl.cc, js call java";
  assert(false);
  //java_bridge_dispatcher_host_->AddNamedObject(
  //    ConvertJavaStringToUTF8(name), scoped_object.Get(), scoped_clazz);
}

void ContentViewCoreImpl::RemoveJavascriptInterface(IInterface* /* obj */,
                                                    const Elastos::String& name) {
  //just for compile, js/car interoperation
  //TODO js call java code
  LOG(INFO) << "leliang, content/browser/elastos/content_view_core_impl.cc, js call java";
  assert(false);
  //java_bridge_dispatcher_host_->RemoveNamedObject(ConvertJavaStringToUTF8(name));
}

void ContentViewCoreImpl::WasResized(IInterface* obj) {
  RenderWidgetHostViewAndroid* view = GetRenderWidgetHostViewAndroid();
  DCHECK(sElaContentViewCoreCallback);
  gfx::Size physical_size(
    sElaContentViewCoreCallback->elastos_ContentViewCore_getPhysicalBackingWidthPix(obj),
    sElaContentViewCoreCallback->elastos_ContentViewCore_getPhysicalBackingHeightPix(obj));
      //Java_ContentViewCore_getPhysicalBackingWidthPix(env, obj),
      //Java_ContentViewCore_getPhysicalBackingHeightPix(env, obj));
  root_layer_->SetBounds(physical_size);

  if (view) {
    RenderWidgetHostImpl* host = RenderWidgetHostImpl::From(
        view->GetRenderWidgetHost());
    host->SendScreenRects();
    view->WasResized();
  }
}

void ContentViewCoreImpl::ShowInterstitialPage(
    IInterface* obj, const Elastos::String& jurl, Elastos::Handle64 delegate_ptr) {
  GURL url(base::android::ConvertJavaStringToUTF8(jurl));
  InterstitialPageDelegateAndroid* delegate =
      reinterpret_cast<InterstitialPageDelegateAndroid*>(delegate_ptr);
  InterstitialPage* interstitial = InterstitialPage::Create(
      web_contents_, false, url, delegate);
  delegate->set_interstitial_page(interstitial);
  interstitial->Show();
}

Elastos::Boolean ContentViewCoreImpl::IsShowingInterstitialPage(IInterface* obj) {
  return web_contents_->ShowingInterstitialPage();
}

Elastos::Boolean ContentViewCoreImpl::IsRenderWidgetHostViewReady(IInterface* obj) {
  RenderWidgetHostViewAndroid* view = GetRenderWidgetHostViewAndroid();
  return view && view->HasValidFrame();
}

void ContentViewCoreImpl::ExitFullscreen(IInterface* obj) {
  RenderViewHost* host = web_contents_->GetRenderViewHost();
  if (!host)
    return;
  host->ExitFullscreen();
}

void ContentViewCoreImpl::UpdateTopControlsState(IInterface* obj,
                                                 bool enable_hiding,
                                                 bool enable_showing,
                                                 bool animate) {
  RenderViewHost* host = web_contents_->GetRenderViewHost();
  if (!host)
    return;
  host->Send(new ViewMsg_UpdateTopControlsState(host->GetRoutingID(),
                                                enable_hiding,
                                                enable_showing,
                                                animate));
}

void ContentViewCoreImpl::ShowImeIfNeeded(IInterface* obj) {
  RenderViewHost* host = web_contents_->GetRenderViewHost();
  host->Send(new ViewMsg_ShowImeIfNeeded(host->GetRoutingID()));
}

void ContentViewCoreImpl::ScrollFocusedEditableNodeIntoView(IInterface* obj) {
  RenderViewHost* host = web_contents_->GetRenderViewHost();
  host->Send(new InputMsg_ScrollFocusedEditableNodeIntoRect(
      host->GetRoutingID(), gfx::Rect()));
}

void ContentViewCoreImpl::SelectWordAroundCaret(IInterface* obj) {
  RenderViewHost* host = web_contents_->GetRenderViewHost();
  if (!host)
    return;
  host->SelectWordAroundCaret();
}

namespace {

static void AddNavigationEntryToHistory(IInterface* obj,
                                        IInterface* history,
                                        NavigationEntry* entry,
                                        int index) {
  // Get the details of the current entry
  Elastos::String j_url(ConvertUTF8ToJavaString(entry->GetURL().spec()));
  Elastos::String j_virtual_url(ConvertUTF8ToJavaString(entry->GetVirtualURL().spec()));
  Elastos::String j_original_url(ConvertUTF8ToJavaString(entry->GetOriginalRequestURL().spec()));
  Elastos::String j_title(ConvertUTF16ToJavaString(entry->GetTitle()));
  Elastos::AutoPtr<IInterface> j_bitmap;
  const FaviconStatus& status = entry->GetFavicon();
  if (status.valid && status.image.ToSkBitmap()->getSize() > 0)
    j_bitmap = gfx::ConvertToJavaBitmap(status.image.ToSkBitmap());

  // Add the item to the list
  DCHECK(sElaContentViewCoreCallback);
  //Java_ContentViewCore_addToNavigationHistory(
  sElaContentViewCoreCallback->elastos_ContentViewCore_addToNavigationHistory(
      obj, history, index, j_url, j_virtual_url,
      j_original_url, j_title, j_bitmap.Get());
}

}  // namespace

int ContentViewCoreImpl::GetNavigationHistory(IInterface* obj,
                                              IInterface* history) {
  // Iterate through navigation entries to populate the list
  const NavigationController& controller = web_contents_->GetController();
  int count = controller.GetEntryCount();
  for (int i = 0; i < count; ++i) {
    AddNavigationEntryToHistory(
        obj, history, controller.GetEntryAtIndex(i), i);
  }

  return controller.GetCurrentEntryIndex();
}

void ContentViewCoreImpl::GetDirectedNavigationHistory(IInterface* obj,
                                                       IInterface* history,
                                                       Elastos::Boolean is_forward,
                                                       Elastos::Int32 max_entries) {
  // Iterate through navigation entries to populate the list
  const NavigationController& controller = web_contents_->GetController();
  int count = controller.GetEntryCount();
  int num_added = 0;
  int increment_value = is_forward ? 1 : -1;
  for (int i = controller.GetCurrentEntryIndex() + increment_value;
       i >= 0 && i < count;
       i += increment_value) {
    if (num_added >= max_entries)
      break;

    AddNavigationEntryToHistory(obj, history, controller.GetEntryAtIndex(i), i);
    num_added++;
  }
}

Elastos::String
ContentViewCoreImpl::GetOriginalUrlForActiveNavigationEntry(IInterface* obj) {
  NavigationEntry* entry = web_contents_->GetController().GetVisibleEntry();
  if (entry == NULL)
    return Elastos::String();
  return ConvertUTF8ToJavaString(entry->GetOriginalRequestURL().spec());
}

long ContentViewCoreImpl::GetNativeImeAdapter(IInterface* obj) {
  RenderWidgetHostViewAndroid* rwhva = GetRenderWidgetHostViewAndroid();
  if (!rwhva)
    return 0;
  return rwhva->GetNativeImeAdapter();
}

namespace {
void JavaScriptResultCallback(IInterface* callback,
                              const base::Value* result) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  std::string json;
  base::JSONWriter::Write(result, &json);
  Elastos::String j_json = ConvertUTF8ToJavaString(json);
  //Java_ContentViewCore_onEvaluateJavaScriptResult(env,
  DCHECK(sElaContentViewCoreCallback);
  sElaContentViewCoreCallback->elastos_ContentViewCore_onEvaluateJavaScriptResult(
                                                  j_json,
                                                  callback);
}
}  // namespace

void ContentViewCoreImpl::EvaluateJavaScript(IInterface* obj,
                                             const Elastos::String& script,
                                             IInterface* callback,
                                             Elastos::Boolean start_renderer) {
  RenderViewHost* rvh = web_contents_->GetRenderViewHost();
  DCHECK(rvh);

  if (start_renderer && !rvh->IsRenderViewLive()) {
    if (!web_contents_->CreateRenderViewForInitialEmptyDocument()) {
      LOG(ERROR) << "Failed to create RenderView in EvaluateJavaScript";
      return;
    }
  }

  if (!callback) {
    // No callback requested.
    web_contents_->GetMainFrame()->ExecuteJavaScript(
        ConvertJavaStringToUTF16(script));
    return;
  }

  // Secure the Java callback in a scoped object and give ownership of it to the
  // base::Callback.
  Elastos::AutoPtr<IInterface> j_callback;
  j_callback = callback;
  content::RenderFrameHost::JavaScriptResultCallback c_callback =
      base::Bind(&JavaScriptResultCallback, j_callback);

  web_contents_->GetMainFrame()->ExecuteJavaScript(
      ConvertJavaStringToUTF16(script),
      c_callback);
}

bool ContentViewCoreImpl::GetUseDesktopUserAgent(
    IInterface* obj) {
  NavigationEntry* entry = web_contents_->GetController().GetVisibleEntry();
  return entry && entry->GetIsOverridingUserAgent();
}

void ContentViewCoreImpl::UpdateImeAdapter(long native_ime_adapter,
                                           int text_input_type,
                                           const std::string& text,
                                           int selection_start,
                                           int selection_end,
                                           int composition_start,
                                           int composition_end,
                                           bool show_ime_if_needed,
                                           bool is_non_ime_change) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<IInterface*> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;

  Elastos::String j_text = ConvertUTF8ToJavaString(text);
  //Java_ContentViewCore_updateImeAdapter(env, obj.obj(),
  DCHECK(sElaContentViewCoreCallback);
  sElaContentViewCoreCallback->elastos_ContentViewCore_updateImeAdapter(obj.Get(),
                                        native_ime_adapter, text_input_type,
                                        j_text,
                                        selection_start, selection_end,
                                        composition_start, composition_end,
                                        show_ime_if_needed, is_non_ime_change);
}

void ContentViewCoreImpl::ClearSslPreferences(IInterface* obj) {
  SSLHostState* state = SSLHostState::GetFor(
      web_contents_->GetController().GetBrowserContext());
  state->Clear();
}

void ContentViewCoreImpl::SetUseDesktopUserAgent(
    IInterface* obj,
    Elastos::Boolean enabled,
    Elastos::Boolean reload_on_state_change) {
  if (GetUseDesktopUserAgent(obj) == enabled)
    return;

  // Make sure the navigation entry actually exists.
  NavigationEntry* entry = web_contents_->GetController().GetVisibleEntry();
  if (!entry)
    return;

  // Set the flag in the NavigationEntry.
  entry->SetIsOverridingUserAgent(enabled);

  // Send the override to the renderer.
  if (reload_on_state_change) {
    // Reloading the page will send the override down as part of the
    // navigation IPC message.
    NavigationControllerImpl& controller =
        static_cast<NavigationControllerImpl&>(web_contents_->GetController());
    controller.ReloadOriginalRequestURL(false);
  }
}

void ContentViewCoreImpl::SetAccessibilityEnabled(IInterface* obj,
                                                  bool enabled) {
  SetAccessibilityEnabledInternal(enabled);
}

void ContentViewCoreImpl::ShowSelectionHandlesAutomatically() const {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj(java_ref_.get(env));
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  //Java_ContentViewCore_showSelectionHandlesAutomatically(env, obj.obj());
  DCHECK(sElaContentViewCoreCallback);
  sElaContentViewCoreCallback->elastos_ContentViewCore_showSelectionHandlesAutomatically(obj.Get());
}

void ContentViewCoreImpl::SetAccessibilityEnabledInternal(bool enabled) {
  accessibility_enabled_ = enabled;
  RenderWidgetHostViewAndroid* host_view = GetRenderWidgetHostViewAndroid();
  if (!host_view)
    return;
  RenderWidgetHostImpl* host_impl = RenderWidgetHostImpl::From(
      host_view->GetRenderWidgetHost());
  BrowserAccessibilityState* accessibility_state =
      BrowserAccessibilityState::GetInstance();
  if (enabled) {
    // This enables accessibility globally unless it was explicitly disallowed
    // by a command-line flag.
    accessibility_state->OnScreenReaderDetected();
    // If it was actually enabled globally, enable it for this RenderWidget now.
    if (accessibility_state->IsAccessibleBrowser() && host_impl)
      host_impl->AddAccessibilityMode(AccessibilityModeComplete);
  } else {
    accessibility_state->ResetAccessibilityMode();
    if (host_impl)
      host_impl->ResetAccessibilityMode();
  }
}

void ContentViewCoreImpl::SendOrientationChangeEventInternal() {
  RenderWidgetHostViewAndroid* rwhv = GetRenderWidgetHostViewAndroid();
  if (rwhv)
    rwhv->UpdateScreenInfo(GetViewAndroid());
}

void ContentViewCoreImpl::ExtractSmartClipData(IInterface* obj,
                                               Elastos::Int32 x,
                                               Elastos::Int32 y,
                                               Elastos::Int32 width,
                                               Elastos::Int32 height) {
  gfx::Rect rect(
      static_cast<int>(x / dpi_scale()),
      static_cast<int>(y / dpi_scale()),
      static_cast<int>((width > 0 && width < dpi_scale()) ?
          1 : (int)(width / dpi_scale())),
      static_cast<int>((height > 0 && height < dpi_scale()) ?
          1 : (int)(height / dpi_scale())));
  GetWebContents()->Send(new ViewMsg_ExtractSmartClipData(
      GetWebContents()->GetRoutingID(), rect));
}

Elastos::Int32 ContentViewCoreImpl::GetCurrentRenderProcessId(IInterface* obj) {
  return GetRenderProcessIdFromRenderViewHost(
      web_contents_->GetRenderViewHost());
}

void ContentViewCoreImpl::SetBackgroundOpaque(IInterface* jobj,
    Elastos::Boolean opaque) {
  if (GetRenderWidgetHostViewAndroid())
    GetRenderWidgetHostViewAndroid()->SetBackgroundOpaque(opaque);
}

void ContentViewCoreImpl::RequestTextSurroundingSelection(
    int max_length,
    const base::Callback<
        void(const base::string16& content, int start_offset, int end_offset)>&
        callback) {
  DCHECK(!callback.is_null());
  RenderFrameHost* focused_frame = web_contents_->GetFocusedFrame();
  if (!focused_frame)
    return;
  if (GetRenderWidgetHostViewAndroid()) {
    GetRenderWidgetHostViewAndroid()->SetTextSurroundingSelectionCallback(
        callback);
    focused_frame->Send(new FrameMsg_TextSurroundingSelectionRequest(
        focused_frame->GetRoutingID(), max_length));
  }
}

void ContentViewCoreImpl::OnSmartClipDataExtracted(
    const base::string16& text,
    const base::string16& html,
    const gfx::Rect& clip_rect) {
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<IInterface*> obj = java_ref_.get(env);
  Elastos::AutoPtr<IInterface> obj = java_ref_.get();
  if (!obj.Get())
    return;
  Elastos::String jtext = ConvertUTF16ToJavaString(text);
  Elastos::String jhtml = ConvertUTF16ToJavaString(html);
  Elastos::AutoPtr<IInterface> clip_rect_object(CreateJavaRect(clip_rect));
  //Java_ContentViewCore_onSmartClipDataExtracted(
  DCHECK(sElaContentViewCoreCallback);
  sElaContentViewCoreCallback->elastos_ContentViewCore_onSmartClipDataExtracted(
      obj.Get(), jtext, jhtml, clip_rect_object.Get());
}

void ContentViewCoreImpl::WebContentsDestroyed() {
  WebContentsViewAndroid* wcva = static_cast<WebContentsViewAndroid*>(
      static_cast<WebContentsImpl*>(web_contents())->GetView());
  DCHECK(wcva);
  wcva->SetContentViewCore(NULL);
}

// This is called for each ContentView.
Elastos::Handle64 Init(IInterface* obj,
           Elastos::Handle64 native_web_contents,
           Elastos::Handle64 view_android,
           Elastos::Handle64 window_android,
           IInterface* retained_objects_set) {
  ContentViewCoreImpl* view = new ContentViewCoreImpl(
      obj,
      reinterpret_cast<WebContents*>(native_web_contents),
      reinterpret_cast<ui::ViewAndroid*>(view_android),
      reinterpret_cast<ui::WindowAndroid*>(window_android),
      retained_objects_set);
  return reinterpret_cast<intptr_t>(view);
}

bool RegisterContentViewCore() {
  return RegisterNativesImpl();
}

}  // namespace content
