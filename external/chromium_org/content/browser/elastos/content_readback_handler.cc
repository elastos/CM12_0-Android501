// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/content_readback_handler.h"

#include "base/elastos/api_elastos.h"
#include "base/bind.h"
#include "cc/output/copy_output_request.h"
#include "cc/output/copy_output_result.h"
#include "content/browser/elastos/content_view_core_impl.h"
#include "content/api/ContentReadbackHandler_api.h"
#include "third_party/skia/include/core/SkBitmap.h"
#include "ui/base/elastos/window_android.h"
#include "ui/base/elastos/window_android_compositor.h"
#include "ui/gfx/elastos/java_bitmap.h"
#include "ui/gfx/rect.h"

namespace content {

namespace {

typedef base::Callback<void(bool, const SkBitmap&)> ResultCallback;

void OnFinishCopyOutputRequest(
    const ResultCallback& result_callback,
    scoped_ptr<cc::CopyOutputResult> copy_output_result) {
  if (!copy_output_result->HasBitmap()) {
    result_callback.Run(false, SkBitmap());
    return;
  }

  scoped_ptr<SkBitmap> bitmap = copy_output_result->TakeBitmap();
  result_callback.Run(true, *bitmap.Pass());
}

}  // anonymous namespace

// static
bool ContentReadbackHandler::RegisterContentReadbackHandler() {
  return RegisterNativesImpl();
}

ContentReadbackHandler::ContentReadbackHandler(IInterface* obj)
    : weak_factory_(this) {
  //java_obj_.Reset(env, obj);
  java_obj_ = obj;
}

void ContentReadbackHandler::Destroy(IInterface* obj) {
  delete this;
}

void ContentReadbackHandler::GetContentBitmap(IInterface* obj,
                                              Elastos::Int32 readback_id,
                                              Elastos::Float scale,
                                              Elastos::Int32 config,
                                              Elastos::Float x,
                                              Elastos::Float y,
                                              Elastos::Float width,
                                              Elastos::Float height,
                                              IInterface* content_view_core) {
  ContentViewCore* view =
      ContentViewCore::GetNativeContentViewCore(content_view_core);
  DCHECK(view);

  ResultCallback result_callback =
      base::Bind(&ContentReadbackHandler::OnFinishReadback,
                 weak_factory_.GetWeakPtr(),
                 readback_id);

  view->GetScaledContentBitmap(
      scale, config, gfx::Rect(x, y, width, height), result_callback);
}

void ContentReadbackHandler::GetCompositorBitmap(IInterface* obj,
                                                 Elastos::Int32 readback_id,
                                                 Elastos::Int64 native_window_android) {
  ui::WindowAndroid* window_android =
      reinterpret_cast<ui::WindowAndroid*>(native_window_android);
  DCHECK(window_android);

  ResultCallback result_callback =
      base::Bind(&ContentReadbackHandler::OnFinishReadback,
                 weak_factory_.GetWeakPtr(),
                 readback_id);

  base::Callback<void(scoped_ptr<cc::CopyOutputResult>)> copy_output_callback =
      base::Bind(&OnFinishCopyOutputRequest,
                 result_callback);

  ui::WindowAndroidCompositor* compositor = window_android->GetCompositor();

  if (!compositor) {
    copy_output_callback.Run(cc::CopyOutputResult::CreateEmptyResult());
    return;
  }

  compositor->RequestCopyOfOutputOnRootLayer(
      cc::CopyOutputRequest::CreateBitmapRequest(copy_output_callback));
}

ContentReadbackHandler::~ContentReadbackHandler() {}

void ContentReadbackHandler::OnFinishReadback(int readback_id,
                                              bool success,
                                              const SkBitmap& bitmap) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> java_bitmap;
  Elastos::AutoPtr<IInterface> java_bitmap;
  if (success)
    java_bitmap = gfx::ConvertToJavaBitmap(&bitmap);

  DCHECK(sElaContentReadbackHandlerCallback);
  //Java_ContentReadbackHandler_notifyGetBitmapFinished(
  sElaContentReadbackHandlerCallback->elastos_ContentReadbackHandler_notifyGetBitmapFinished(
      java_obj_.Get(), readback_id, success, java_bitmap.Get());
}

// static
static Elastos::Handle64 Init(IInterface* obj) {
  ContentReadbackHandler* content_readback_handler =
      new ContentReadbackHandler(obj);
  return reinterpret_cast<intptr_t>(content_readback_handler);
}

}  // namespace content
