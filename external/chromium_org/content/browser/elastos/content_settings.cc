// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/content_settings.h"

#include "base/elastos/api_elastos.h"
#include "content/browser/elastos/content_view_core_impl.h"
#include "content/browser/renderer_host/render_view_host_delegate.h"
#include "content/browser/renderer_host/render_view_host_impl.h"
#include "content/public/browser/web_contents.h"
#include "content/api/ContentSettings_api.h"
#include "webkit/common/webpreferences.h"

namespace content {

ContentSettings::ContentSettings(IInterface* obj,
                         WebContents* contents)
    : WebContentsObserver(contents),
      content_settings_(obj)
    {
}

ContentSettings::~ContentSettings() {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> obj = content_settings_.get(env);
  Elastos::AutoPtr<IInterface> obj = content_settings_.get();
  DCHECK(sElaContentSettingsCallback);
  if (obj.Get()) {
    //Java_ContentSettings_onNativeContentSettingsDestroyed(env, obj.obj(),
    sElaContentSettingsCallback->elastos_ContentSettings_onNativeContentSettingsDestroyed(obj.Get(),
        reinterpret_cast<intptr_t>(this));
  }
}

// static
bool ContentSettings::RegisterContentSettings() {
  return RegisterNativesImpl();
}

bool ContentSettings::GetJavaScriptEnabled(IInterface* obj) {
  RenderViewHost* render_view_host = web_contents()->GetRenderViewHost();
  if (!render_view_host)
    return false;
  return render_view_host->GetDelegate()->GetWebkitPrefs().javascript_enabled;
}

void ContentSettings::WebContentsDestroyed() {
  delete this;
}

static Elastos::Handle64 Init(IInterface* obj, Elastos::Handle64 nativeContentViewCore) {
  WebContents* web_contents =
      reinterpret_cast<ContentViewCoreImpl*>(nativeContentViewCore)
          ->GetWebContents();
  ContentSettings* content_settings =
      new ContentSettings(obj, web_contents);
  return reinterpret_cast<intptr_t>(content_settings);
}

}  // namespace content
