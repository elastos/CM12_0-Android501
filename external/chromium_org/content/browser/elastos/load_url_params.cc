// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/load_url_params.h"

//#include <jni.h>

#include "base/elastos/jni_string.h"
#include "content/public/browser/navigation_controller.h"
#include "content/public/common/url_constants.h"
#include "content/api/LoadUrlParams_api.h"
#include "url/gurl.h"

namespace {

using content::NavigationController;
using content::sElaLoadUrlParamsCallback;

}  // namespace

namespace content {

void RegisterConstants() {
  //Java_LoadUrlParams_initializeConstants(env,
  DCHECK(sElaLoadUrlParamsCallback);
  sElaLoadUrlParamsCallback->elastos_LoadUrlParams_initializeConstants(
      NavigationController::LOAD_TYPE_DEFAULT,
      NavigationController::LOAD_TYPE_BROWSER_INITIATED_HTTP_POST,
      NavigationController::LOAD_TYPE_DATA,
      NavigationController::UA_OVERRIDE_INHERIT,
      NavigationController::UA_OVERRIDE_FALSE,
      NavigationController::UA_OVERRIDE_TRUE);
}

bool RegisterLoadUrlParams() {
  if (!RegisterNativesImpl())
    return false;
  RegisterConstants();
  return true;
}

Elastos::Boolean IsDataScheme(const Elastos::String& jurl) {
  GURL url(base::android::ConvertJavaStringToUTF8(jurl));
  return url.SchemeIs(url::kDataScheme);
}

}  // namespace content
