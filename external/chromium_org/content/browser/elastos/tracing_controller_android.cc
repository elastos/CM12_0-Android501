// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/elastos/tracing_controller_android.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
#include "base/debug/trace_event.h"
#include "base/json/json_writer.h"
#include "base/logging.h"
#include "content/public/browser/tracing_controller.h"
#include "content/api/TracingControllerAndroid_api.h"

namespace content {

static Elastos::Handle64 Init(IInterface* obj) {
  TracingControllerAndroid* profiler = new TracingControllerAndroid(obj);
  return reinterpret_cast<intptr_t>(profiler);
}

TracingControllerAndroid::TracingControllerAndroid(IInterface* obj)
    : weak_java_object_(obj),
      weak_factory_(this) {}

TracingControllerAndroid::~TracingControllerAndroid() {}

void TracingControllerAndroid::Destroy(IInterface* obj) {
  delete this;
}

bool TracingControllerAndroid::StartTracing(IInterface* obj,
                                            const Elastos::String& jcategories,
                                            Elastos::Boolean record_continuously) {
  std::string categories =
      base::android::ConvertJavaStringToUTF8(jcategories);

  // This log is required by adb_profile_chrome.py.
  LOG(WARNING) << "Logging performance trace to file";

  return TracingController::GetInstance()->EnableRecording(
      categories,
      record_continuously ? TracingController::RECORD_CONTINUOUSLY
                          : TracingController::DEFAULT_OPTIONS,
      TracingController::EnableRecordingDoneCallback());
}

void TracingControllerAndroid::StopTracing(IInterface* obj,
                                           const Elastos::String& jfilepath) {
  base::FilePath file_path(
      base::android::ConvertJavaStringToUTF8(jfilepath));
  if (!TracingController::GetInstance()->DisableRecording(
      file_path,
      base::Bind(&TracingControllerAndroid::OnTracingStopped,
                 weak_factory_.GetWeakPtr()))) {
    LOG(ERROR) << "EndTracingAsync failed, forcing an immediate stop";
    OnTracingStopped(file_path);
  }
}

void TracingControllerAndroid::GenerateTracingFilePath(
    base::FilePath* file_path) {
  //JNIEnv* env = base::android::AttachCurrentThread();
  //LLScopedJavaLocalRef<jstring> jfilename =
  //LL    Java_TracingControllerAndroid_generateTracingFilePath(env);
  DCHECK(sElaTracingControllerAndroidCallback);
  Elastos::String jfilename =
  sElaTracingControllerAndroidCallback->elastos_TracingControllerAndroid_generateTracingFilePath();
  *file_path = base::FilePath(
      base::android::ConvertJavaStringToUTF8(jfilename));
}

void TracingControllerAndroid::OnTracingStopped(
    const base::FilePath& file_path) {
  //LLJNIEnv* env = base::android::AttachCurrentThread();
  //base::android::ScopedJavaLocalRef<jobject> obj = weak_java_object_.get(env);
  DCHECK(sElaTracingControllerAndroidCallback);
  Elastos::AutoPtr<IInterface> obj = weak_java_object_.get();
  if (obj.Get())
    sElaTracingControllerAndroidCallback->elastos_TracingControllerAndroid_onTracingStopped(obj);
    //Java_TracingControllerAndroid_onTracingStopped(env, obj.obj());
}

bool TracingControllerAndroid::GetKnownCategoryGroupsAsync(IInterface* obj) {
  if (!TracingController::GetInstance()->GetCategories(
          base::Bind(&TracingControllerAndroid::OnKnownCategoriesReceived,
                     weak_factory_.GetWeakPtr()))) {
    return false;
  }
  return true;
}

void TracingControllerAndroid::OnKnownCategoriesReceived(
    const std::set<std::string>& categories_received) {
  scoped_ptr<base::ListValue> category_list(new base::ListValue());
  for (std::set<std::string>::const_iterator it = categories_received.begin();
       it != categories_received.end();
       ++it) {
    category_list->AppendString(*it);
  }
  std::string received_category_list;
  base::JSONWriter::Write(category_list.get(), &received_category_list);

  // This log is required by adb_profile_chrome.py.
  LOG(WARNING) << "{\"traceCategoriesList\": " << received_category_list << "}";
}

static Elastos::String GetDefaultCategories(IInterface* obj) {
  return base::android::ConvertUTF8ToJavaString(
      base::debug::CategoryFilter::kDefaultCategoryFilterString);
}

bool RegisterTracingControllerAndroid() {
  return RegisterNativesImpl();
}

}  // namespace content
