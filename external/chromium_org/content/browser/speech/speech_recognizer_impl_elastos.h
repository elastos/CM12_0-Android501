// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CONTENT_BROWSER_SPEECH_SPEECH_RECOGNIZER_IMPL_ANDROID_H_
#define CONTENT_BROWSER_SPEECH_SPEECH_RECOGNIZER_IMPL_ANDROID_H_

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
//#include "base/android/scoped_java_ref.h"
#include "base/memory/ref_counted.h"
#include "base/memory/scoped_ptr.h"
#include "content/browser/speech/speech_recognizer.h"
#include "content/public/common/speech_recognition_error.h"
#include "content/public/common/speech_recognition_result.h"

namespace content {

class SpeechRecognitionEventListener;

class CONTENT_EXPORT SpeechRecognizerImplAndroid : public SpeechRecognizer {
 public:
  SpeechRecognizerImplAndroid(SpeechRecognitionEventListener* listener,
                              int session_id);

  // SpeechRecognizer methods.
  virtual void StartRecognition(const std::string& device_id) OVERRIDE;
  virtual void AbortRecognition() OVERRIDE;
  virtual void StopAudioCapture() OVERRIDE;
  virtual bool IsActive() const OVERRIDE;
  virtual bool IsCapturingAudio() const OVERRIDE;

  // Called from Java methods via JNI.
  void OnAudioStart(IInterface* obj);
  void OnSoundStart(IInterface* obj);
  void OnSoundEnd(IInterface* obj);
  void OnAudioEnd(IInterface* obj);
  void OnRecognitionResults(IInterface* obj, Elastos::ArrayOf<Elastos::String>* strings,
                            Elastos::ArrayOf<Elastos::Float>* floats, Elastos::Boolean interim);
  void OnRecognitionError(IInterface* obj, Elastos::Int32 error);
  void OnRecognitionEnd(IInterface* obj);

  static bool RegisterSpeechRecognizer();

 private:
  enum State {
    STATE_IDLE = 0,
    STATE_CAPTURING_AUDIO,
    STATE_AWAITING_FINAL_RESULT
  };

  void StartRecognitionOnUIThread(
      std::string language, bool continuous, bool interim_results);
  void OnRecognitionResultsOnIOThread(SpeechRecognitionResults const &results);

  virtual ~SpeechRecognizerImplAndroid();

  //base::android::ScopedJavaGlobalRef<jobject> j_recognition_;
  Elastos::AutoPtr<IInterface> j_recognition_;
  State state_;

  DISALLOW_COPY_AND_ASSIGN(SpeechRecognizerImplAndroid);
};

}  // namespace content

#endif  // CONTENT_BROWSER_SPEECH_SPEECH_RECOGNIZER_IMPL_ANDROID_H_
