// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/vibration/vibration_provider_elastos.h"

#include <algorithm>

#include "content/browser/vibration/vibration_message_filter.h"
#include "content/common/view_messages.h"
#include "content/api/VibrationProvider_api.h"
#include "third_party/WebKit/public/platform/WebVibration.h"

//using base::android::AttachCurrentThread;

namespace content {

VibrationProviderAndroid::VibrationProviderAndroid() {
}

VibrationProviderAndroid::~VibrationProviderAndroid() {
}

// static
bool VibrationProviderAndroid::Register() {
  return RegisterNativesImpl();
}

void VibrationProviderAndroid::Vibrate(int64 milliseconds) {
  DCHECK(sElaVibrationProviderCallback);
  if (j_vibration_provider_.Get()) {
    //j_vibration_provider_.Reset(
    //    Java_VibrationProvider_create(
    //        AttachCurrentThread(),
    //        base::android::GetApplicationContext()));
    j_vibration_provider_ =
        sElaVibrationProviderCallback->elastos_VibrationProvider_create(base::android::GetAppContext());
  }
  //Java_VibrationProvider_vibrate(AttachCurrentThread(),
  //                              j_vibration_provider_.obj(),
  sElaVibrationProviderCallback->elastos_VibrationProvider_vibrate(
                                j_vibration_provider_.Get(), milliseconds);
}

void VibrationProviderAndroid::CancelVibration() {
  // If somehow a cancel message is received before this object was
  // instantiated, it means there is no current vibration anyway. Just return.
  if (j_vibration_provider_.Get())
    return;

  DCHECK(sElaVibrationProviderCallback);
  //Java_VibrationProvider_cancelVibration(AttachCurrentThread(),
  sElaVibrationProviderCallback->elastos_VibrationProvider_cancelVibration(
                                        j_vibration_provider_.Get());
}

// static
VibrationProvider* VibrationMessageFilter::CreateProvider() {
  return new VibrationProviderAndroid();
}

}  // namespace content
