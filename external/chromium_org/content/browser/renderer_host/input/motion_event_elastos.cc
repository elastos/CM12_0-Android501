// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/api/MotionEvent_api.h"
#include "content/browser/renderer_host/input/motion_event_elastos.h"

#include "base/elastos/api_elastos.h"

//using base::android::AttachCurrentThread;
using namespace API_MotionEvent;

namespace content {
namespace {

int ToAndroidAction(MotionEventAndroid::Action action) {
  switch (action) {
    case MotionEventAndroid::ACTION_DOWN:
      return ACTION_DOWN;
    case MotionEventAndroid::ACTION_UP:
      return ACTION_UP;
    case MotionEventAndroid::ACTION_MOVE:
      return ACTION_MOVE;
    case MotionEventAndroid::ACTION_CANCEL:
      return ACTION_CANCEL;
    case MotionEventAndroid::ACTION_POINTER_DOWN:
      return ACTION_POINTER_DOWN;
    case MotionEventAndroid::ACTION_POINTER_UP:
      return ACTION_POINTER_UP;
  };
  NOTREACHED() << "Invalid Android MotionEvent type for gesture detection: "
               << action;
  return ACTION_CANCEL;
}

MotionEventAndroid::Action FromAndroidAction(int android_action) {
  switch (android_action) {
    case ACTION_DOWN:
      return MotionEventAndroid::ACTION_DOWN;
    case ACTION_UP:
      return MotionEventAndroid::ACTION_UP;
    case ACTION_MOVE:
      return MotionEventAndroid::ACTION_MOVE;
    case ACTION_CANCEL:
      return MotionEventAndroid::ACTION_CANCEL;
    case ACTION_POINTER_DOWN:
      return MotionEventAndroid::ACTION_POINTER_DOWN;
    case ACTION_POINTER_UP:
      return MotionEventAndroid::ACTION_POINTER_UP;
    default:
      NOTREACHED() << "Invalid Android MotionEvent type for gesture detection: "
                   << android_action;
  };
  return MotionEventAndroid::ACTION_CANCEL;
}

MotionEventAndroid::ToolType FromAndroidToolType(int android_tool_type) {
  switch (android_tool_type) {
    case TOOL_TYPE_UNKNOWN:
      return MotionEventAndroid::TOOL_TYPE_UNKNOWN;
    case TOOL_TYPE_FINGER:
      return MotionEventAndroid::TOOL_TYPE_FINGER;
    case TOOL_TYPE_STYLUS:
      return MotionEventAndroid::TOOL_TYPE_STYLUS;
    case TOOL_TYPE_MOUSE:
      return MotionEventAndroid::TOOL_TYPE_MOUSE;
    default:
      NOTREACHED() << "Invalid Android MotionEvent tool type: "
                   << android_tool_type;
  };
  return MotionEventAndroid::TOOL_TYPE_UNKNOWN;
}

int FromAndroidButtonState(int button_state) {
  int result = 0;
  if ((button_state & BUTTON_BACK) != 0)
    result |= MotionEventAndroid::BUTTON_BACK;
  if ((button_state & BUTTON_FORWARD) != 0)
    result |= MotionEventAndroid::BUTTON_FORWARD;
  if ((button_state & BUTTON_PRIMARY) != 0)
    result |= MotionEventAndroid::BUTTON_PRIMARY;
  if ((button_state & BUTTON_SECONDARY) != 0)
    result |= MotionEventAndroid::BUTTON_SECONDARY;
  if ((button_state & BUTTON_TERTIARY) != 0)
    result |= MotionEventAndroid::BUTTON_TERTIARY;
  return result;
}

int64 ToAndroidTime(base::TimeTicks time) {
  return (time - base::TimeTicks()).InMilliseconds();
}

base::TimeTicks FromAndroidTime(int64 time_ms) {
  return base::TimeTicks() + base::TimeDelta::FromMilliseconds(time_ms);
}

}  // namespace

MotionEventAndroid::MotionEventAndroid(float pix_to_dip,
                                       IInterface* event,
                                       Elastos::Int64 time_ms,
                                       Elastos::Int32 android_action,
                                       Elastos::Int32 pointer_count,
                                       Elastos::Int32 history_size,
                                       Elastos::Int32 action_index,
                                       Elastos::Float pos_x_0_pixels,
                                       Elastos::Float pos_y_0_pixels,
                                       Elastos::Float pos_x_1_pixels,
                                       Elastos::Float pos_y_1_pixels,
                                       Elastos::Int32 pointer_id_0,
                                       Elastos::Int32 pointer_id_1,
                                       Elastos::Float touch_major_0_pixels,
                                       Elastos::Float touch_major_1_pixels,
                                       Elastos::Float raw_pos_x_pixels,
                                       Elastos::Float raw_pos_y_pixels,
                                       Elastos::Int32 android_tool_type_0,
                                       Elastos::Int32 android_tool_type_1,
                                       Elastos::Int32 android_button_state)
    : cached_time_(FromAndroidTime(time_ms)),
      cached_action_(FromAndroidAction(android_action)),
      cached_pointer_count_(pointer_count),
      cached_history_size_(history_size),
      cached_action_index_(action_index),
      cached_button_state_(FromAndroidButtonState(android_button_state)),
      pix_to_dip_(pix_to_dip),
      should_recycle_(false) {
  DCHECK_GT(pointer_count, 0);
  DCHECK_GE(history_size, 0);

  event_ = event;
  DCHECK(event_.Get());

  cached_positions_[0] = ToDips(gfx::PointF(pos_x_0_pixels, pos_y_0_pixels));
  cached_positions_[1] = ToDips(gfx::PointF(pos_x_1_pixels, pos_y_1_pixels));
  cached_pointer_ids_[0] = pointer_id_0;
  cached_pointer_ids_[1] = pointer_id_1;
  cached_touch_majors_[0] = ToDips(touch_major_0_pixels);
  cached_touch_majors_[1] = ToDips(touch_major_1_pixels);
  cached_raw_position_offset_ =
      ToDips(gfx::PointF(raw_pos_x_pixels, raw_pos_y_pixels)) -
      cached_positions_[0];
  cached_tool_types_[0] = FromAndroidToolType(android_tool_type_0);
  cached_tool_types_[1] = FromAndroidToolType(android_tool_type_1);
}

MotionEventAndroid::MotionEventAndroid(float pix_to_dip,
                                       IInterface* event)
    : cached_time_(FromAndroidTime(sElaMotionEventCallback->elastos_MotionEvent_getEventTime(event))),
      cached_action_(
          FromAndroidAction(sElaMotionEventCallback->elastos_MotionEvent_getActionMasked(event))),
      cached_pointer_count_(sElaMotionEventCallback->elastos_MotionEvent_getPointerCount(event)),
      cached_history_size_(sElaMotionEventCallback->elastos_MotionEvent_getHistorySize(event)),
      cached_action_index_(sElaMotionEventCallback->elastos_MotionEvent_getActionIndex(event)),
      cached_button_state_(
          FromAndroidButtonState(sElaMotionEventCallback->elastos_MotionEvent_getButtonState(event))),
      pix_to_dip_(pix_to_dip),
      should_recycle_(true) {
  event_ = event;
  DCHECK(event_.Get());
  DCHECK(sElaMotionEventCallback);

  for (size_t i = 0; i < MAX_POINTERS_TO_CACHE; ++i) {
    if (i < cached_pointer_count_) {
      cached_positions_[i] =
          ToDips(gfx::PointF(sElaMotionEventCallback->elastos_MotionEvent_getXF_I(event, i),
                             sElaMotionEventCallback->elastos_MotionEvent_getYF_I(event, i)));
      cached_pointer_ids_[i] = sElaMotionEventCallback->elastos_MotionEvent_getPointerId(event, i);
      cached_touch_majors_[i] =
          ToDips(sElaMotionEventCallback->elastos_MotionEvent_getTouchMajorF_I(event, i));
      cached_tool_types_[i] =
          FromAndroidToolType(sElaMotionEventCallback->elastos_MotionEvent_getToolType(event, i));
    } else {
      cached_pointer_ids_[i] = 0;
      cached_touch_majors_[i] = 0.f;
      cached_tool_types_[i] = MotionEvent::TOOL_TYPE_UNKNOWN;
    }
  }

  cached_raw_position_offset_ =
      ToDips(gfx::PointF(sElaMotionEventCallback->elastos_MotionEvent_getRawX(event),
                         sElaMotionEventCallback->elastos_MotionEvent_getRawY(event))) -
      cached_positions_[0];
}

MotionEventAndroid::MotionEventAndroid(const MotionEventAndroid& other)
    : event_(Obtain(other)),
      cached_time_(other.cached_time_),
      cached_action_(other.cached_action_),
      cached_pointer_count_(other.cached_pointer_count_),
      cached_history_size_(other.cached_history_size_),
      cached_action_index_(other.cached_action_index_),
      cached_raw_position_offset_(other.cached_raw_position_offset_),
      cached_button_state_(other.cached_button_state_),
      pix_to_dip_(other.pix_to_dip_),
      should_recycle_(true) {
  DCHECK(event_.Get());
  for (size_t i = 0; i < MAX_POINTERS_TO_CACHE; ++i) {
    cached_positions_[i] = other.cached_positions_[i];
    cached_pointer_ids_[i] = other.cached_pointer_ids_[i];
    cached_touch_majors_[i] = other.cached_touch_majors_[i];
    cached_tool_types_[i] = other.cached_tool_types_[i];
  }
}

MotionEventAndroid::~MotionEventAndroid() {
  DCHECK(sElaMotionEventCallback);
  if (should_recycle_)
    sElaMotionEventCallback->elastos_MotionEvent_recycle(event_);
}

int MotionEventAndroid::GetId() const {
  return 0;
}

MotionEventAndroid::Action MotionEventAndroid::GetAction() const {
  return cached_action_;
}

int MotionEventAndroid::GetActionIndex() const {
  return cached_action_index_;
}

size_t MotionEventAndroid::GetPointerCount() const {
  return cached_pointer_count_;
}

int MotionEventAndroid::GetPointerId(size_t pointer_index) const {
  DCHECK_LT(pointer_index, cached_pointer_count_);
  if (pointer_index < MAX_POINTERS_TO_CACHE)
    return cached_pointer_ids_[pointer_index];
  //return Java_MotionEvent_getPointerId(
  //    AttachCurrentThread(), event_.obj(), pointer_index);
  DCHECK(sElaMotionEventCallback);
  return sElaMotionEventCallback->elastos_MotionEvent_getPointerId(event_, pointer_index);
}

float MotionEventAndroid::GetX(size_t pointer_index) const {
  DCHECK_LT(pointer_index, cached_pointer_count_);
  if (pointer_index < MAX_POINTERS_TO_CACHE)
    return cached_positions_[pointer_index].x();
  //return ToDips(Java_MotionEvent_getXF_I(
  //    AttachCurrentThread(), event_.obj(), pointer_index));
  DCHECK(sElaMotionEventCallback);
  return ToDips(sElaMotionEventCallback->elastos_MotionEvent_getXF_I(event_, pointer_index));
}

float MotionEventAndroid::GetY(size_t pointer_index) const {
  DCHECK_LT(pointer_index, cached_pointer_count_);
  if (pointer_index < MAX_POINTERS_TO_CACHE)
    return cached_positions_[pointer_index].y();
  //return ToDips(Java_MotionEvent_getYF_I(
  //    AttachCurrentThread(), event_.obj(), pointer_index));
  DCHECK(sElaMotionEventCallback);
  return ToDips(sElaMotionEventCallback->elastos_MotionEvent_getYF_I(event_, pointer_index));
}

float MotionEventAndroid::GetRawX(size_t pointer_index) const {
  return GetX(pointer_index) + cached_raw_position_offset_.x();
}

float MotionEventAndroid::GetRawY(size_t pointer_index) const {
  return GetY(pointer_index) + cached_raw_position_offset_.y();
}

float MotionEventAndroid::GetTouchMajor(size_t pointer_index) const {
  DCHECK_LT(pointer_index, cached_pointer_count_);
  if (pointer_index < MAX_POINTERS_TO_CACHE)
    return cached_touch_majors_[pointer_index];
  //return ToDips(Java_MotionEvent_getTouchMajorF_I(
  //    AttachCurrentThread(), event_.obj(), pointer_index));
  DCHECK(sElaMotionEventCallback);
  return ToDips(sElaMotionEventCallback->elastos_MotionEvent_getTouchMajorF_I(event_, pointer_index));
}

float MotionEventAndroid::GetPressure(size_t pointer_index) const {
  DCHECK_LT(pointer_index, cached_pointer_count_);
  //return Java_MotionEvent_getPressureF_I(
  //    AttachCurrentThread(), event_.obj(), pointer_index);
  DCHECK(sElaMotionEventCallback);
  return ToDips(sElaMotionEventCallback->elastos_MotionEvent_getPressureF_I(event_, pointer_index));
}

base::TimeTicks MotionEventAndroid::GetEventTime() const {
  return cached_time_;
}

size_t MotionEventAndroid::GetHistorySize() const {
  return cached_history_size_;
}

base::TimeTicks MotionEventAndroid::GetHistoricalEventTime(
    size_t historical_index) const {
  //return FromAndroidTime(Java_MotionEvent_getHistoricalEventTime(
  //    AttachCurrentThread(), event_.obj(), historical_index));
  DCHECK(sElaMotionEventCallback);
  return FromAndroidTime(sElaMotionEventCallback->elastos_MotionEvent_getHistoricalEventTime(event_, historical_index));
}

float MotionEventAndroid::GetHistoricalTouchMajor(
    size_t pointer_index,
    size_t historical_index) const {
  //return ToDips(Java_MotionEvent_getHistoricalTouchMajorF_I_I(
  //    AttachCurrentThread(), event_.obj(), pointer_index, historical_index));
  DCHECK(sElaMotionEventCallback);
  return ToDips(sElaMotionEventCallback->elastos_MotionEvent_getHistoricalTouchMajorF_I_I(event_, pointer_index, historical_index));
}

float MotionEventAndroid::GetHistoricalX(size_t pointer_index,
                                         size_t historical_index) const {
  //return ToDips(Java_MotionEvent_getHistoricalXF_I_I(
  //    AttachCurrentThread(), event_.obj(), pointer_index, historical_index));
  DCHECK(sElaMotionEventCallback);
  return ToDips(sElaMotionEventCallback->elastos_MotionEvent_getHistoricalXF_I_I(event_, pointer_index, historical_index));
}

float MotionEventAndroid::GetHistoricalY(size_t pointer_index,
                                         size_t historical_index) const {
  //return ToDips(Java_MotionEvent_getHistoricalYF_I_I(
  //    AttachCurrentThread(), event_.obj(), pointer_index, historical_index));
  DCHECK(sElaMotionEventCallback);
  return ToDips(sElaMotionEventCallback->elastos_MotionEvent_getHistoricalYF_I_I(event_, pointer_index, historical_index));
}

ui::MotionEvent::ToolType MotionEventAndroid::GetToolType(
    size_t pointer_index) const {
  DCHECK_LT(pointer_index, cached_pointer_count_);
  if (pointer_index < MAX_POINTERS_TO_CACHE)
    return cached_tool_types_[pointer_index];
  //return FromAndroidToolType(Java_MotionEvent_getToolType(
  //    AttachCurrentThread(), event_.obj(), pointer_index));
  DCHECK(sElaMotionEventCallback);
  return FromAndroidToolType(sElaMotionEventCallback->elastos_MotionEvent_getToolType(event_, pointer_index));
}

int MotionEventAndroid::GetButtonState() const {
  return cached_button_state_;
}

scoped_ptr<ui::MotionEvent> MotionEventAndroid::Clone() const {
  return scoped_ptr<MotionEvent>(new MotionEventAndroid(*this));
}

scoped_ptr<ui::MotionEvent> MotionEventAndroid::Cancel() const {
  // The input coordinates to |MotionEventAndroid| are always in device pixels,
  // but the cached coordinates are in DIPs.
  const gfx::PointF position_pixels =
      gfx::ScalePoint(cached_positions_[0], 1.f / pix_to_dip_);
  return scoped_ptr<MotionEvent>(
      new MotionEventAndroid(pix_to_dip_,
                             Obtain(GetDownTime(),
                                    GetEventTime(),
                                    MotionEventAndroid::ACTION_CANCEL,
                                    position_pixels.x(),
                                    position_pixels.y())));
}

float MotionEventAndroid::GetTouchMinor(size_t pointer_index) const {
  //return ToDips(Java_MotionEvent_getTouchMinorF_I(
  //    AttachCurrentThread(), event_.obj(), pointer_index));
  DCHECK(sElaMotionEventCallback);
  return sElaMotionEventCallback->elastos_MotionEvent_getTouchMinorF_I(event_, pointer_index);
}

float MotionEventAndroid::GetOrientation() const {
  //return Java_MotionEvent_getOrientationF(AttachCurrentThread(), event_.obj());
  DCHECK(sElaMotionEventCallback);
  return sElaMotionEventCallback->elastos_MotionEvent_getOrientationF(event_);
}

base::TimeTicks MotionEventAndroid::GetDownTime() const {
  //return FromAndroidTime(
  //    Java_MotionEvent_getDownTime(AttachCurrentThread(), event_.obj()));
  DCHECK(sElaMotionEventCallback);
  return FromAndroidTime(sElaMotionEventCallback->elastos_MotionEvent_getDownTime(event_));
}

float MotionEventAndroid::ToDips(float pixels) const {
  return pixels * pix_to_dip_;
}

gfx::PointF MotionEventAndroid::ToDips(const gfx::PointF& point_pixels) const {
  return gfx::ScalePoint(point_pixels, pix_to_dip_);
}

// static
bool MotionEventAndroid::RegisterMotionEventAndroid() {
  return API_MotionEvent::RegisterNativesImpl();
}

// static
Elastos::AutoPtr<IInterface> MotionEventAndroid::Obtain(
    const MotionEventAndroid& event) {
  //return Java_MotionEvent_obtainAVME_AVME(AttachCurrentThread(),
  //                                        event.event_.obj());
  return event.event_;
}

// static
Elastos::AutoPtr<IInterface> MotionEventAndroid::Obtain(
    base::TimeTicks down_time,
    base::TimeTicks event_time,
    Action action,
    float x_pixels,
    float y_pixels) {
  //return Java_MotionEvent_obtainAVME_J_J_I_F_F_I(AttachCurrentThread(),
  DCHECK(sElaMotionEventCallback);
  return sElaMotionEventCallback->elastos_MotionEvent_obtainAVME_J_J_I_F_F_I(
                                                 ToAndroidTime(down_time),
                                                 ToAndroidTime(event_time),
                                                 ToAndroidAction(action),
                                                 x_pixels,
                                                 y_pixels,
                                                 0);
}

}  // namespace content
