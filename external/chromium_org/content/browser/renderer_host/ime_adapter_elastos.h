// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CONTENT_BROWSER_RENDERER_HOST_IME_ADAPTER_ANDROID_H_
#define CONTENT_BROWSER_RENDERER_HOST_IME_ADAPTER_ANDROID_H_

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/elastos/api_weak_ref.h"

namespace content {

class RenderFrameHost;
class RenderWidgetHostImpl;
class RenderWidgetHostViewAndroid;
class WebContents;
struct NativeWebKeyboardEvent;

// This class is in charge of dispatching key events from the java side
// and forward to renderer along with input method results via
// corresponding host view.
// Ownership of these objects remains on the native side (see
// RenderWidgetHostViewAndroid).
class ImeAdapterAndroid {
 public:
  explicit ImeAdapterAndroid(RenderWidgetHostViewAndroid* rwhva);
  ~ImeAdapterAndroid();

  // Called from java -> native
  // The java side is responsible to translate android KeyEvent various enums
  // and values into the corresponding blink::WebInputEvent.
  bool SendKeyEvent(IInterface* caller, IInterface* original_key_event,
                    Elastos::Int32 action, Elastos::Int32 meta_state,
                    Elastos::Int64 event_time, Elastos::Int32 key_code,
                    Elastos::Boolean is_system_key, Elastos::Int32 unicode_text);
  // |event_type| is a value of WebInputEvent::Type.
  bool SendSyntheticKeyEvent(IInterface* caller, Elastos::Int32 event_type,
                             Elastos::Int64 timestamp_ms,
                             Elastos::Int32 native_key_code,
                             Elastos::Int32 unicode_char);
  void SetComposingText(IInterface* caller, IInterface* text,
                        const Elastos::String& text_str,
                        Elastos::Int32 new_cursor_pos);
  void CommitText(IInterface* caller, const Elastos::String& text_str);
  void FinishComposingText(IInterface*);
  void AttachImeAdapter(IInterface* caller);
  void SetEditableSelectionOffsets(IInterface* caller, Elastos::Int32 start, Elastos::Int32 end);
  void SetComposingRegion(IInterface* caller, Elastos::Int32 start, Elastos::Int32 end);
  void DeleteSurroundingText(IInterface* caller, Elastos::Int32 before, Elastos::Int32 after);
  void Unselect(IInterface*);
  void SelectAll(IInterface*);
  void Cut(IInterface*);
  void Copy(IInterface*);
  void Paste(IInterface*);
  void ResetImeAdapter(IInterface*);

  // Called from native -> java
  void CancelComposition();
  void FocusedNodeChanged(bool is_editable_node);

 private:
  RenderWidgetHostImpl* GetRenderWidgetHostImpl();
  RenderFrameHost* GetFocusedFrame();
  WebContents* GetWebContents();

  RenderWidgetHostViewAndroid* rwhva_;
  ObjectWeakGlobalRef java_ime_adapter_;
};

bool RegisterImeAdapter();

}  // namespace content

#endif  // CONTENT_BROWSER_RENDERER_HOST_IME_ADAPTER_ANDROID_H_
