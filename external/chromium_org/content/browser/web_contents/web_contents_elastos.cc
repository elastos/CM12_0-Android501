// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/web_contents/web_contents_elastos.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
#include "base/logging.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/web_contents.h"
#include "content/api/WebContentsImpl_api.h"

//using base::android::AttachCurrentThread;

namespace content {

// static
WebContents* WebContents::FromJavaWebContents(
    IInterface* jweb_contents_android) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  if (!jweb_contents_android)
    return NULL;

  DCHECK(sElaWebContentsImplCallback);
  WebContentsAndroid* web_contents_android
  = reinterpret_cast<WebContentsAndroid*>(
          sElaWebContentsImplCallback->elastos_WebContentsImpl_getNativePointer(jweb_contents_android));
  //        Java_WebContentsImpl_getNativePointer(AttachCurrentThread(),
  //                                              jweb_contents_android));
  if (!web_contents_android)
    return NULL;
  return web_contents_android->web_contents();
}

// static
bool WebContentsAndroid::Register() {
  return RegisterNativesImpl();
}

WebContentsAndroid::WebContentsAndroid(WebContents* web_contents)
    : web_contents_(web_contents),
      navigation_controller_(&(web_contents->GetController())) {
  //JNIEnv* env = AttachCurrentThread();
  //obj_.Reset(env, Java_WebContentsImpl_create(
  DCHECK(sElaWebContentsImplCallback);
  obj_ =
  sElaWebContentsImplCallback->elastos_WebContentsImpl_create(
                 reinterpret_cast<intptr_t>(this),
                 navigation_controller_.GetJavaObject().Get());
}

WebContentsAndroid::~WebContentsAndroid() {
  DCHECK(sElaWebContentsImplCallback);
  //Java_WebContentsImpl_destroy(AttachCurrentThread(), obj_.obj());
  sElaWebContentsImplCallback->elastos_WebContentsImpl_destroy(obj_.Get());
}

Elastos::AutoPtr<IInterface>
WebContentsAndroid::GetJavaObject() {
  return obj_;
}

Elastos::String WebContentsAndroid::GetTitle(
    IInterface* obj) const {
  return base::android::ConvertUTF16ToJavaString(web_contents_->GetTitle());
}

Elastos::String WebContentsAndroid::GetVisibleURL(
    IInterface* obj) const {
  return base::android::ConvertUTF8ToJavaString(web_contents_->GetVisibleURL().spec());
}

void WebContentsAndroid::Stop(IInterface* obj) {
  web_contents_->Stop();
}

}  // namespace content
