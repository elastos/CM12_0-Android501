// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/frame_host/navigation_controller_elastos.h"

#include "base/elastos/api_elastos.h"
#include "content/public/browser/navigation_controller.h"
#include "content/api/NavigationControllerImpl_api.h"

//using base::android::AttachCurrentThread;

namespace content {

// static
bool NavigationControllerAndroid::Register() {
  return RegisterNativesImpl();
}

NavigationControllerAndroid::NavigationControllerAndroid(
    NavigationController* navigation_controller)
    : navigation_controller_(navigation_controller) {
  //JNIEnv* env = AttachCurrentThread();
  //obj_.Reset(env,
  //           Java_NavigationControllerImpl_create(
  DCHECK(sElaNavigationControllerImplCallback);
  obj_ =
  sElaNavigationControllerImplCallback->elastos_NavigationControllerImpl_create(
                 reinterpret_cast<intptr_t>(this));
}

NavigationControllerAndroid::~NavigationControllerAndroid() {
  //Java_NavigationControllerImpl_destroy(AttachCurrentThread(), obj_.obj());
  DCHECK(sElaNavigationControllerImplCallback);
  sElaNavigationControllerImplCallback->elastos_NavigationControllerImpl_destroy(obj_.Get());
}

Elastos::AutoPtr<IInterface>
NavigationControllerAndroid::GetJavaObject() {
  //return base::android::ScopedJavaLocalRef<jobject>(obj_);
  return obj_;
}

Elastos::Boolean NavigationControllerAndroid::CanGoBack(IInterface* obj) {
  return navigation_controller_->CanGoBack();
}

Elastos::Boolean NavigationControllerAndroid::CanGoForward(IInterface* obj) {
  return navigation_controller_->CanGoForward();
}

Elastos::Boolean NavigationControllerAndroid::CanGoToOffset(IInterface* obj,
                                                    Elastos::Int32 offset) {
  return navigation_controller_->CanGoToOffset(offset);
}

void NavigationControllerAndroid::GoBack(IInterface* obj) {
  navigation_controller_->GoBack();
}

void NavigationControllerAndroid::GoForward(IInterface* obj) {
  navigation_controller_->GoForward();
}

void NavigationControllerAndroid::GoToOffset(IInterface* obj,
                                             Elastos::Int32 offset) {
  navigation_controller_->GoToOffset(offset);
}

void NavigationControllerAndroid::GoToNavigationIndex(IInterface* obj,
                                                      Elastos::Int32 index) {
  navigation_controller_->GoToIndex(index);
}

}  // namespace content
