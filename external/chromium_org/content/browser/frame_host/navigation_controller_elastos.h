// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CONTENT_BROWSER_FRAME_HOST_NAVIGATION_CONTROLLER_ANDROID_H_
#define CONTENT_BROWSER_FRAME_HOST_NAVIGATION_CONTROLLER_ANDROID_H_

//#include <jni.h>

#include "ElAndroid.h"
#include "elastos.h"
#include "base/macros.h"
//#include "base/android/scoped_java_ref.h"
#include "base/basictypes.h"
#include "base/compiler_specific.h"
#include "content/common/content_export.h"

namespace content {

class NavigationController;

// Android wrapper around NavigationController that provides safer passage
// from java and back to native and provides java with a means of communicating
// with its native counterpart.
class CONTENT_EXPORT NavigationControllerAndroid {
 public:
  static bool Register();

  explicit NavigationControllerAndroid(
      NavigationController* navigation_controller);
  ~NavigationControllerAndroid();

  NavigationController* navigation_controller() const {
    return navigation_controller_;
  }

  Elastos::AutoPtr<IInterface> GetJavaObject();

  Elastos::Boolean CanGoBack(IInterface* obj);
  Elastos::Boolean CanGoForward(IInterface* obj);
  Elastos::Boolean CanGoToOffset(IInterface* obj, Elastos::Int32 offset);
  void GoBack(IInterface*  obj);
  void GoForward(IInterface* obj);
  void GoToOffset(IInterface* obj, Elastos::Int32 offset);
  void GoToNavigationIndex(IInterface* obj, Elastos::Int32 index);

 private:
  NavigationController* navigation_controller_;
  Elastos::AutoPtr<IInterface> obj_;

  DISALLOW_COPY_AND_ASSIGN(NavigationControllerAndroid);
};

}  // namespace content

#endif  // CONTENT_BROWSER_FRAME_HOST_NAVIGATION_CONTROLLER_ANDROID_H_
