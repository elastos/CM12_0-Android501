// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/time_zone_monitor_elastos.h"

#include "base/elastos/api_elastos.h"
#include "content/api/TimeZoneMonitor_api.h"

namespace content {

TimeZoneMonitorAndroid::TimeZoneMonitorAndroid() : TimeZoneMonitor() {
  //impl_.Reset(Java_TimeZoneMonitor_getInstance(
  //    base::android::AttachCurrentThread(),
  //    base::android::GetApplicationContext(),
  DCHECK(sElaTimeZoneMonitorCallback);
  impl_ = sElaTimeZoneMonitorCallback->elastos_TimeZoneMonitor_getInstance(
      base::android::GetAppContext(),
      reinterpret_cast<intptr_t>(this));
}

TimeZoneMonitorAndroid::~TimeZoneMonitorAndroid() {
  //Java_TimeZoneMonitor_stop(base::android::AttachCurrentThread(), impl_.obj());
  DCHECK(sElaTimeZoneMonitorCallback);
  sElaTimeZoneMonitorCallback->elastos_TimeZoneMonitor_stop(impl_.Get());
}

// static
bool TimeZoneMonitorAndroid::Register() {
  return RegisterNativesImpl();
}

void TimeZoneMonitorAndroid::TimeZoneChangedFromJava(IInterface* caller) {
  NotifyRenderers();
}

// static
scoped_ptr<TimeZoneMonitor> TimeZoneMonitor::Create() {
  return scoped_ptr<TimeZoneMonitor>(new TimeZoneMonitorAndroid());
}

}  // namespace content
