// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CHROME_BROWSER_BATTERY_STATUS_BATTERY_STATUS_MANAGER_H_
#define CHROME_BROWSER_BATTERY_STATUS_BATTERY_STATUS_MANAGER_H_

#include "build/build_config.h"

#if defined(OS_ANDROID)
#if defined(OS_ELASTOS)
#include "ElAndroid.h"
#include "elastos.h"
#else
#include "base/android/scoped_java_ref.h"
#endif
#endif

#include "content/browser/battery_status/battery_status_service.h"

namespace content {

// Platform specific manager class for fetching battery status data.
class CONTENT_EXPORT BatteryStatusManager {
 public:
  explicit BatteryStatusManager(
      const BatteryStatusService::BatteryUpdateCallback& callback);
  virtual ~BatteryStatusManager();

  // Start listening for battery status changes. New updates are signalled
  // by invoking the callback provided at construction time.
  virtual bool StartListeningBatteryChange();

  // Stop listening for battery status changes.
  virtual void StopListeningBatteryChange();

#if defined(OS_ANDROID)
#if defined(OS_ELASTOS)
  // Must be called at startup.
  static bool Register();

  // Called from Java via JNI.
  void GotBatteryStatus(IInterface*, Elastos::Boolean charging,
                        Elastos::Double charging_time, Elastos::Double discharging_time,
                        Elastos::Double level);
#else
  // Must be called at startup.
  static bool Register(JNIEnv* env);

  // Called from Java via JNI.
  void GotBatteryStatus(JNIEnv*, jobject, jboolean charging,
                        jdouble charging_time, jdouble discharging_time,
                        jdouble level);
#endif
#endif

 protected:
  BatteryStatusManager();
  BatteryStatusService::BatteryUpdateCallback callback_;

 private:
#if defined(OS_ANDROID)
#if defined(OS_ELASTOS)
  // elastos provider of battery status info.
  Elastos::AutoPtr<IInterface> j_manager_;
#else
  // Java provider of battery status info.
  base::android::ScopedJavaGlobalRef<jobject> j_manager_;
#endif
#endif

  DISALLOW_COPY_AND_ASSIGN(BatteryStatusManager);
};

}  // namespace content

#endif  // CHROME_BROWSER_BATTERY_STATUS_BATTERY_STATUS_MANAGER_H_
