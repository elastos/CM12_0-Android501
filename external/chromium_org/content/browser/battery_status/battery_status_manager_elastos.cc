// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/battery_status/battery_status_manager.h"

#include "base/elastos/api_elastos.h"
#include "base/metrics/histogram.h"
#include "content/api/BatteryStatusManager_api.h"

//using base::android::AttachCurrentThread;

namespace content {

BatteryStatusManager::BatteryStatusManager(
    const BatteryStatusService::BatteryUpdateCallback& callback)
    : callback_(callback) {
  DCHECK(sElaBatteryStatusManagerCallback);
  j_manager_ =
      sElaBatteryStatusManagerCallback->elastos_BatteryStatusManager_getInstance(
              base::android::GetAppContext());
  //    Java_BatteryStatusManager_getInstance(
  //        AttachCurrentThread(), base::android::GetApplicationContext());
}

BatteryStatusManager::BatteryStatusManager() {
}

BatteryStatusManager::~BatteryStatusManager() {
}

bool BatteryStatusManager::Register() {
  return RegisterNativesImpl();
}

void BatteryStatusManager::GotBatteryStatus(IInterface*,
    Elastos::Boolean charging, Elastos::Double charging_time, Elastos::Double discharging_time,
    Elastos::Double level) {
  blink::WebBatteryStatus status;
  status.charging = charging;
  status.chargingTime = charging_time;
  status.dischargingTime = discharging_time;
  status.level = level;
  callback_.Run(status);
}

bool BatteryStatusManager::StartListeningBatteryChange() {
  DCHECK(sElaBatteryStatusManagerCallback);
  //bool result = Java_BatteryStatusManager_start(AttachCurrentThread(),
  //    j_manager_.obj(), reinterpret_cast<intptr_t>(this));
  bool result = sElaBatteryStatusManagerCallback->elastos_BatteryStatusManager_start(
          j_manager_.Get(), reinterpret_cast<intptr_t>(this));
  UMA_HISTOGRAM_BOOLEAN("BatteryStatus.StartAndroid", result);
  return result;
}

void BatteryStatusManager::StopListeningBatteryChange() {
  //Java_BatteryStatusManager_stop(AttachCurrentThread(), j_manager_.obj());
  DCHECK(sElaBatteryStatusManagerCallback);
  sElaBatteryStatusManagerCallback->elastos_BatteryStatusManager_stop(j_manager_.Get());
}

}  // namespace content
