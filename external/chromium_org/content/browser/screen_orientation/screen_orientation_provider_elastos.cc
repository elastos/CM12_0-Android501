// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/screen_orientation/screen_orientation_provider_elastos.h"

#include "content/browser/screen_orientation/screen_orientation_dispatcher_host.h"
#include "content/api/ScreenOrientationProvider_api.h"

namespace content {

ScreenOrientationProviderAndroid::ScreenOrientationProviderAndroid() {
}

ScreenOrientationProviderAndroid::~ScreenOrientationProviderAndroid() {
}

// static
bool ScreenOrientationProviderAndroid::Register() {
  return RegisterNativesImpl();
}

void ScreenOrientationProviderAndroid::LockOrientation(
    blink::WebScreenOrientationLockType orientation) {
  //LLif (j_screen_orientation_provider_.is_null()) {
  //  j_screen_orientation_provider_.Reset(Java_ScreenOrientationProvider_create(
  //      base::android::AttachCurrentThread()));
  //}
  DCHECK(sElaScreenOrientationProviderCallback);
  if (!j_screen_orientation_provider_.Get()) {
    j_screen_orientation_provider_ =
        sElaScreenOrientationProviderCallback->elastos_ScreenOrientationProvider_create();
  }

  //Java_ScreenOrientationProvider_lockOrientation(
  //    base::android::AttachCurrentThread(),
  //    j_screen_orientation_provider_.obj(), orientation);
  sElaScreenOrientationProviderCallback->elastos_ScreenOrientationProvider_lockOrientation(j_screen_orientation_provider_.Get(), orientation);
}

void ScreenOrientationProviderAndroid::UnlockOrientation() {
  // j_screen_orientation_provider_ is set when locking. If the screen
  // orientation was not locked, unlocking should be a no-op.
  //LLif (j_screen_orientation_provider_.is_null())
  if (!j_screen_orientation_provider_.Get())
    return;

  //LLJava_ScreenOrientationProvider_unlockOrientation(
  //    base::android::AttachCurrentThread(),
  //    j_screen_orientation_provider_.obj());
  DCHECK(sElaScreenOrientationProviderCallback);
  sElaScreenOrientationProviderCallback->elastos_ScreenOrientationProvider_unlockOrientation(j_screen_orientation_provider_.Get());
}

// static
ScreenOrientationProvider* ScreenOrientationDispatcherHost::CreateProvider() {
  return new ScreenOrientationProviderAndroid();
}

} // namespace content
