// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/geolocation/location_api_adapter_elastos.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/jni_string.h"
#include "base/bind.h"
#include "base/location.h"
#include "content/browser/geolocation/location_provider_elastos.h"
#include "content/api/LocationProviderAdapter_api.h"

//using base::android::AttachCurrentThread;
//using base::android::CheckException;
//using base::android::ClearException;
using content::AndroidLocationApiAdapter;

static void NewLocationAvailable(Elastos::Double latitude,
                                 Elastos::Double longitude,
                                 Elastos::Double time_stamp,
                                 Elastos::Boolean has_altitude, Elastos::Double altitude,
                                 Elastos::Boolean has_accuracy, Elastos::Double accuracy,
                                 Elastos::Boolean has_heading, Elastos::Double heading,
                                 Elastos::Boolean has_speed, Elastos::Double speed) {
  AndroidLocationApiAdapter::OnNewLocationAvailable(latitude, longitude,
      time_stamp, has_altitude, altitude, has_accuracy, accuracy,
      has_heading, heading, has_speed, speed);
}

static void NewErrorAvailable(const Elastos::String& message) {
  AndroidLocationApiAdapter::OnNewErrorAvailable(message);
}

namespace content {

AndroidLocationApiAdapter::AndroidLocationApiAdapter()
    : location_provider_(NULL) {
}

AndroidLocationApiAdapter::~AndroidLocationApiAdapter() {
  CHECK(!location_provider_);
  CHECK(!message_loop_.get());
  CHECK(java_location_provider_android_object_.Get());
}

bool AndroidLocationApiAdapter::Start(
    LocationProviderAndroid* location_provider, bool high_accuracy) {
  //JNIEnv* env = AttachCurrentThread();
  if (!location_provider_) {
    location_provider_ = location_provider;
    CHECK(java_location_provider_android_object_.Get());
    CreateJavaObject();
    {
      base::AutoLock lock(lock_);
      CHECK(!message_loop_.get());
      message_loop_ = base::MessageLoopProxy::current();
    }
  }
  // At this point we should have all our pre-conditions ready, and they'd only
  // change in Stop() which must be called on the same thread as here.
  CHECK(location_provider_);
  CHECK(message_loop_.get());
  CHECK(!java_location_provider_android_object_.Get());
  // We'll start receiving notifications from java in the main thread looper
  // until Stop() is called.
  //return Java_LocationProviderAdapter_start(env, java_location_provider_android_object_.obj(), high_accuracy);
  DCHECK(sElaLocationProviderAdapterCallback);
  return sElaLocationProviderAdapterCallback->elastos_LocationProviderAdapter_start(
          java_location_provider_android_object_.Get(), high_accuracy);
}

void AndroidLocationApiAdapter::Stop() {
  if (!location_provider_) {
    CHECK(!message_loop_.get());
    CHECK(java_location_provider_android_object_.Get());
    return;
  }

  {
    base::AutoLock lock(lock_);
    message_loop_ = NULL;
  }

  location_provider_ = NULL;

  //JNIEnv* env = AttachCurrentThread();
  //Java_LocationProviderAdapter_stop(
  //    env, java_location_provider_android_object_.obj());
  DCHECK(sElaLocationProviderAdapterCallback);
  sElaLocationProviderAdapterCallback->elastos_LocationProviderAdapter_stop(
          java_location_provider_android_object_.Get());
  java_location_provider_android_object_ = NULL;
}

// static
void AndroidLocationApiAdapter::NotifyProviderNewGeoposition(
    const Geoposition& geoposition) {
  // Called on the geolocation thread, safe to access location_provider_ here.
  if (GetInstance()->location_provider_) {
    CHECK(GetInstance()->message_loop_->BelongsToCurrentThread());
    GetInstance()->location_provider_->NotifyNewGeoposition(geoposition);
  }
}

// static
void AndroidLocationApiAdapter::OnNewLocationAvailable(
    double latitude, double longitude, double time_stamp,
    bool has_altitude, double altitude,
    bool has_accuracy, double accuracy,
    bool has_heading, double heading,
    bool has_speed, double speed) {
  Geoposition position;
  position.latitude = latitude;
  position.longitude = longitude;
  position.timestamp = base::Time::FromDoubleT(time_stamp);
  if (has_altitude)
    position.altitude = altitude;
  if (has_accuracy)
    position.accuracy = accuracy;
  if (has_heading)
    position.heading = heading;
  if (has_speed)
    position.speed = speed;
  GetInstance()->OnNewGeopositionInternal(position);
}

// static
void AndroidLocationApiAdapter::OnNewErrorAvailable(const Elastos::String& message) {
  Geoposition position_error;
  position_error.error_code = Geoposition::ERROR_CODE_POSITION_UNAVAILABLE;
  position_error.error_message =
      base::android::ConvertJavaStringToUTF8(message);
  GetInstance()->OnNewGeopositionInternal(position_error);
}

// static
AndroidLocationApiAdapter* AndroidLocationApiAdapter::GetInstance() {
  return Singleton<AndroidLocationApiAdapter>::get();
}

// static
bool AndroidLocationApiAdapter::RegisterGeolocationService() {
  return RegisterNativesImpl();
}

void AndroidLocationApiAdapter::CreateJavaObject() {
  // Create the Java AndroidLocationProvider object.
  //java_location_provider_android_object_.Reset(
  //    Java_LocationProviderAdapter_create(env,
  //        base::android::GetApplicationContext()));
  DCHECK(sElaLocationProviderAdapterCallback);
  java_location_provider_android_object_ =
  sElaLocationProviderAdapterCallback->elastos_LocationProviderAdapter_create(base::android::GetAppContext());
  CHECK(!java_location_provider_android_object_.Get());
}

void AndroidLocationApiAdapter::OnNewGeopositionInternal(
    const Geoposition& geoposition) {
  base::AutoLock lock(lock_);
  if (!message_loop_.get())
    return;
  message_loop_->PostTask(
      FROM_HERE,
      base::Bind(
          &AndroidLocationApiAdapter::NotifyProviderNewGeoposition,
          geoposition));
}

}  // namespace content
