// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/power_save_blocker_elastos.h"

#include "base/elastos/api_elastos.h"
#include "base/elastos/api_weak_ref.h"
#include "base/logging.h"
#include "content/browser/power_save_blocker_impl.h"
#include "content/public/browser/elastos/content_view_core.h"
#include "content/public/browser/browser_thread.h"
#include "content/api/PowerSaveBlocker_api.h"
#include "ui/base/elastos/view_android.h"

//using base::android::AttachCurrentThread;
//using base::android::ScopedJavaLocalRef;
using gfx::NativeView;

namespace content {

class PowerSaveBlockerImpl::Delegate
    : public base::RefCountedThreadSafe<PowerSaveBlockerImpl::Delegate> {
 public:
  explicit Delegate(NativeView view_android) {
    //j_view_android_ = ObjectWeakGlobalRef(
    //    AttachCurrentThread(), view_android->GetJavaObject().obj());
    j_view_android_ = ObjectWeakGlobalRef(view_android->GetJavaObject().Get());
  }

  // Does the actual work to apply or remove the desired power save block.
  void ApplyBlock();
  void RemoveBlock();

 private:
  friend class base::RefCountedThreadSafe<Delegate>;
  ~Delegate() {}

  ObjectWeakGlobalRef j_view_android_;

  DISALLOW_COPY_AND_ASSIGN(Delegate);
};

void PowerSaveBlockerImpl::Delegate::ApplyBlock() {
  //DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_object = j_view_android_.get(env);
  Elastos::AutoPtr<IInterface> j_object = j_view_android_.get();
  DCHECK(sElaPowerSaveBlockerCallback);
  if (j_object.Get())
    sElaPowerSaveBlockerCallback->elastos_PowerSaveBlocker_applyBlock(j_object.Get());
    //Java_PowerSaveBlocker_applyBlock(env, j_object.obj());
}

void PowerSaveBlockerImpl::Delegate::RemoveBlock() {
  //DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  //JNIEnv* env = AttachCurrentThread();
  //ScopedJavaLocalRef<jobject> j_object = j_view_android_.get(env);
  Elastos::AutoPtr<IInterface> j_object = j_view_android_.get();
  DCHECK(sElaPowerSaveBlockerCallback);
  if (j_object.Get())
    sElaPowerSaveBlockerCallback->elastos_PowerSaveBlocker_removeBlock(j_object.Get());
    //Java_PowerSaveBlocker_removeBlock(env, j_object.obj());
}

PowerSaveBlockerImpl::PowerSaveBlockerImpl(PowerSaveBlockerType type,
                                           const std::string& reason) {
  // Don't support kPowerSaveBlockPreventAppSuspension
}

PowerSaveBlockerImpl::~PowerSaveBlockerImpl() {
  if (delegate_) {
    BrowserThread::PostTask(
        BrowserThread::UI, FROM_HERE,
        base::Bind(&Delegate::RemoveBlock, delegate_));
  }
}

void PowerSaveBlockerImpl::InitDisplaySleepBlocker(NativeView view_android) {
  if (!view_android)
    return;

  delegate_ = new Delegate(view_android);
  // This may be called on any thread.
  BrowserThread::PostTask(
      BrowserThread::UI, FROM_HERE,
      base::Bind(&Delegate::ApplyBlock, delegate_));
}

bool RegisterPowerSaveBlocker() {
  return RegisterNativesImpl();
}

}  // namespace content
