typedef interface ICallbackRendezvous ICallbackRendezvous;
typedef interface ICallbackSink ICallbackSink;
typedef interface IRegime IRegime;
typedef interface IDataTypeInfo IDataTypeInfo;
typedef interface ILocalPtrInfo ILocalPtrInfo;
typedef interface IEnumInfo IEnumInfo;
typedef interface IEnumItemInfo IEnumItemInfo;
typedef interface ICarArrayInfo ICarArrayInfo;
typedef interface ICppVectorInfo ICppVectorInfo;
typedef interface IStructInfo IStructInfo;
typedef interface IFieldInfo IFieldInfo;
typedef interface IInterfaceInfo IInterfaceInfo;
typedef interface IFunctionInfo IFunctionInfo;
typedef interface IMethodInfo IMethodInfo;
typedef interface IParamInfo IParamInfo;
typedef interface ITypeAliasInfo ITypeAliasInfo;
typedef interface IArgumentList IArgumentList;
typedef interface IVariable IVariable;
typedef interface IVariableOfCarArray IVariableOfCarArray;
typedef interface IVariableOfStruct IVariableOfStruct;
typedef interface ICarArraySetter ICarArraySetter;
typedef interface ICarArrayGetter ICarArrayGetter;
typedef interface IStructSetter IStructSetter;
typedef interface IStructGetter IStructGetter;
typedef interface ICppVectorSetter ICppVectorSetter;
typedef interface ICppVectorGetter ICppVectorGetter;
typedef interface IModuleInfo IModuleInfo;
typedef interface IConstantInfo IConstantInfo;
typedef interface IClassInfo IClassInfo;
typedef interface IConstructorInfo IConstructorInfo;
typedef interface ICallbackMethodInfo ICallbackMethodInfo;
typedef interface IDelegateProxy IDelegateProxy;
typedef interface ICallbackInvocation ICallbackInvocation;
typedef interface ICallbackArgumentList ICallbackArgumentList;
typedef interface IParcel IParcel;
typedef interface IParcelable IParcelable;
typedef interface ICustomMarshal ICustomMarshal;

typedef struct ICallbackRendezvousVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICallbackRendezvous *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICallbackRendezvous *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICallbackRendezvous *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICallbackRendezvous *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *Wait)(
            ICallbackRendezvous *pThis,
            /* [in] */ Int32 timeout,
            /* [out] */ WaitResult * result);
}   ICallbackRendezvousVtbl;

interface ICallbackRendezvous {
    CONST_VTBL struct ICallbackRendezvousVtbl *v;
};

typedef struct ICallbackSinkVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICallbackSink *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICallbackSink *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICallbackSink *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICallbackSink *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *AddCallback)(
            ICallbackSink *pThis,
            /* [in] */ Int32 event,
            /* [in] */ EventHandler handler);

    ECode (CARAPICALLTYPE *RemoveCallback)(
            ICallbackSink *pThis,
            /* [in] */ Int32 event,
            /* [in] */ EventHandler handler);

    ECode (CARAPICALLTYPE *AcquireCallbackRendezvous)(
            ICallbackSink *pThis,
            /* [in] */ Int32 event,
            /* [out] */ ICallbackRendezvous ** rendezvous);

    ECode (CARAPICALLTYPE *RemoveAllCallbacks)(
            ICallbackSink *pThis);

    ECode (CARAPICALLTYPE *CancelPendingCallback)(
            ICallbackSink *pThis,
            /* [in] */ Int32 event);

    ECode (CARAPICALLTYPE *CancelAllPendingCallbacks)(
            ICallbackSink *pThis);
}   ICallbackSinkVtbl;

interface ICallbackSink {
    CONST_VTBL struct ICallbackSinkVtbl *v;
};

typedef struct IRegimeVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IRegime *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IRegime *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IRegime *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IRegime *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *ObjectEnter)(
            IRegime *pThis,
            /* [in] */ PInterface object);

    ECode (CARAPICALLTYPE *ObjectLeave)(
            IRegime *pThis,
            /* [in] */ PInterface object);

    ECode (CARAPICALLTYPE *CreateObject)(
            IRegime *pThis,
            /* [in] */ const ClassID * clsid,
            /* [in] */ const InterfaceID * iid,
            /* [out] */ IInterface ** object);
}   IRegimeVtbl;

interface IRegime {
    CONST_VTBL struct IRegimeVtbl *v;
};

typedef struct IDataTypeInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IDataTypeInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IDataTypeInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IDataTypeInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IDataTypeInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IDataTypeInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetSize)(
            IDataTypeInfo *pThis,
            /* [out] */ MemorySize * size);

    ECode (CARAPICALLTYPE *GetDataType)(
            IDataTypeInfo *pThis,
            /* [out] */ CarDataType * dataType);
}   IDataTypeInfoVtbl;

interface IDataTypeInfo {
    CONST_VTBL struct IDataTypeInfoVtbl *v;
};

typedef struct ILocalPtrInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ILocalPtrInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ILocalPtrInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ILocalPtrInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ILocalPtrInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IDataTypeInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetSize)(
            IDataTypeInfo *pThis,
            /* [out] */ MemorySize * size);

    ECode (CARAPICALLTYPE *GetDataType)(
            IDataTypeInfo *pThis,
            /* [out] */ CarDataType * dataType);

    ECode (CARAPICALLTYPE *GetTargetTypeInfo)(
            ILocalPtrInfo *pThis,
            /* [out] */ IDataTypeInfo ** dateTypeInfo);

    ECode (CARAPICALLTYPE *GetPtrLevel)(
            ILocalPtrInfo *pThis,
            /* [out] */ Int32 * level);
}   ILocalPtrInfoVtbl;

interface ILocalPtrInfo {
    CONST_VTBL struct ILocalPtrInfoVtbl *v;
};

typedef struct IEnumInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IEnumInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IEnumInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IEnumInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IEnumInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IDataTypeInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetSize)(
            IDataTypeInfo *pThis,
            /* [out] */ MemorySize * size);

    ECode (CARAPICALLTYPE *GetDataType)(
            IDataTypeInfo *pThis,
            /* [out] */ CarDataType * dataType);

    ECode (CARAPICALLTYPE *GetNamespace)(
            IEnumInfo *pThis,
            /* [out] */ String* ns);

    ECode (CARAPICALLTYPE *GetModuleInfo)(
            IEnumInfo *pThis,
            /* [out] */ IModuleInfo ** moduleInfo);

    ECode (CARAPICALLTYPE *GetItemCount)(
            IEnumInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllItemInfos)(
            IEnumInfo *pThis,
            /* [out] */ PCarQuintet itemInfos);

    ECode (CARAPICALLTYPE *GetItemInfo)(
            IEnumInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IEnumItemInfo ** enumItemInfo);
}   IEnumInfoVtbl;

interface IEnumInfo {
    CONST_VTBL struct IEnumInfoVtbl *v;
};

typedef struct IEnumItemInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IEnumItemInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IEnumItemInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IEnumItemInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IEnumItemInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IEnumItemInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetEnumInfo)(
            IEnumItemInfo *pThis,
            /* [out] */ IEnumInfo ** enumInfo);

    ECode (CARAPICALLTYPE *GetValue)(
            IEnumItemInfo *pThis,
            /* [out] */ Int32 * value);
}   IEnumItemInfoVtbl;

interface IEnumItemInfo {
    CONST_VTBL struct IEnumItemInfoVtbl *v;
};

typedef struct ICarArrayInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICarArrayInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICarArrayInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICarArrayInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICarArrayInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IDataTypeInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetSize)(
            IDataTypeInfo *pThis,
            /* [out] */ MemorySize * size);

    ECode (CARAPICALLTYPE *GetDataType)(
            IDataTypeInfo *pThis,
            /* [out] */ CarDataType * dataType);

    ECode (CARAPICALLTYPE *GetElementTypeInfo)(
            ICarArrayInfo *pThis,
            /* [out] */ IDataTypeInfo ** elementTypeInfo);

    ECode (CARAPICALLTYPE *CreateVariable)(
            ICarArrayInfo *pThis,
            /* [in] */ Int32 capacity,
            /* [out] */ IVariableOfCarArray ** variableBox);

    ECode (CARAPICALLTYPE *CreateVariableBox)(
            ICarArrayInfo *pThis,
            /* [in] */ PCarQuintet variableDescriptor,
            /* [out] */ IVariableOfCarArray ** variableBox);
}   ICarArrayInfoVtbl;

interface ICarArrayInfo {
    CONST_VTBL struct ICarArrayInfoVtbl *v;
};

typedef struct ICppVectorInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICppVectorInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICppVectorInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICppVectorInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICppVectorInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IDataTypeInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetSize)(
            IDataTypeInfo *pThis,
            /* [out] */ MemorySize * size);

    ECode (CARAPICALLTYPE *GetDataType)(
            IDataTypeInfo *pThis,
            /* [out] */ CarDataType * dataType);

    ECode (CARAPICALLTYPE *GetElementTypeInfo)(
            ICppVectorInfo *pThis,
            /* [out] */ IDataTypeInfo ** elementTypeInfo);

    ECode (CARAPICALLTYPE *GetLength)(
            ICppVectorInfo *pThis,
            /* [out] */ Int32 * length);
}   ICppVectorInfoVtbl;

interface ICppVectorInfo {
    CONST_VTBL struct ICppVectorInfoVtbl *v;
};

typedef struct IStructInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IStructInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IStructInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IStructInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IStructInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IDataTypeInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetSize)(
            IDataTypeInfo *pThis,
            /* [out] */ MemorySize * size);

    ECode (CARAPICALLTYPE *GetDataType)(
            IDataTypeInfo *pThis,
            /* [out] */ CarDataType * dataType);

    ECode (CARAPICALLTYPE *GetModuleInfo)(
            IStructInfo *pThis,
            /* [out] */ IModuleInfo ** moduleInfo);

    ECode (CARAPICALLTYPE *GetFieldCount)(
            IStructInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllFieldInfos)(
            IStructInfo *pThis,
            /* [out] */ PCarQuintet fieldInfos);

    ECode (CARAPICALLTYPE *GetFieldInfo)(
            IStructInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IFieldInfo ** fieldInfo);

    ECode (CARAPICALLTYPE *CreateVariable)(
            IStructInfo *pThis,
            /* [out] */ IVariableOfStruct ** variableBox);

    ECode (CARAPICALLTYPE *CreateVariableBox)(
            IStructInfo *pThis,
            /* [in] */ PVoid variableDescriptor,
            /* [out] */ IVariableOfStruct ** variableBox);
}   IStructInfoVtbl;

interface IStructInfo {
    CONST_VTBL struct IStructInfoVtbl *v;
};

typedef struct IFieldInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IFieldInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IFieldInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IFieldInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IFieldInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IFieldInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetTypeInfo)(
            IFieldInfo *pThis,
            /* [out] */ IDataTypeInfo ** typeInfo);
}   IFieldInfoVtbl;

interface IFieldInfo {
    CONST_VTBL struct IFieldInfoVtbl *v;
};

typedef struct IInterfaceInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IInterfaceInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IInterfaceInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IInterfaceInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IInterfaceInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IDataTypeInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetSize)(
            IDataTypeInfo *pThis,
            /* [out] */ MemorySize * size);

    ECode (CARAPICALLTYPE *GetDataType)(
            IDataTypeInfo *pThis,
            /* [out] */ CarDataType * dataType);

    ECode (CARAPICALLTYPE *GetNamespace)(
            IInterfaceInfo *pThis,
            /* [out] */ String* ns);

    ECode (CARAPICALLTYPE *GetId)(
            IInterfaceInfo *pThis,
            /* [out] */ InterfaceID * iid);

    ECode (CARAPICALLTYPE *GetModuleInfo)(
            IInterfaceInfo *pThis,
            /* [out] */ IModuleInfo ** moduleInfo);

    ECode (CARAPICALLTYPE *IsLocal)(
            IInterfaceInfo *pThis,
            /* [out] */ Boolean * isLocal);

    ECode (CARAPICALLTYPE *HasBase)(
            IInterfaceInfo *pThis,
            /* [out] */ Boolean * hasBase);

    ECode (CARAPICALLTYPE *GetBaseInfo)(
            IInterfaceInfo *pThis,
            /* [out] */ IInterfaceInfo ** baseInfo);

    ECode (CARAPICALLTYPE *GetMethodCount)(
            IInterfaceInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllMethodInfos)(
            IInterfaceInfo *pThis,
            /* [out] */ PCarQuintet methodInfos);

    ECode (CARAPICALLTYPE *GetMethodInfo)(
            IInterfaceInfo *pThis,
            /* [in] */ String name,
            /* [in] */ String signature,
            /* [out] */ IMethodInfo ** methodInfo);
}   IInterfaceInfoVtbl;

interface IInterfaceInfo {
    CONST_VTBL struct IInterfaceInfoVtbl *v;
};

typedef struct IFunctionInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IFunctionInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IFunctionInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IFunctionInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IFunctionInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IFunctionInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetParamCount)(
            IFunctionInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllParamInfos)(
            IFunctionInfo *pThis,
            /* [out] */ PCarQuintet paramInfos);

    ECode (CARAPICALLTYPE *GetParamInfoByIndex)(
            IFunctionInfo *pThis,
            /* [in] */ Int32 index,
            /* [out] */ IParamInfo ** paramInfo);

    ECode (CARAPICALLTYPE *GetParamInfoByName)(
            IFunctionInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IParamInfo ** paramInfo);
}   IFunctionInfoVtbl;

interface IFunctionInfo {
    CONST_VTBL struct IFunctionInfoVtbl *v;
};

typedef struct IMethodInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IMethodInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IMethodInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IMethodInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IMethodInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IFunctionInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetParamCount)(
            IFunctionInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllParamInfos)(
            IFunctionInfo *pThis,
            /* [out] */ PCarQuintet paramInfos);

    ECode (CARAPICALLTYPE *GetParamInfoByIndex)(
            IFunctionInfo *pThis,
            /* [in] */ Int32 index,
            /* [out] */ IParamInfo ** paramInfo);

    ECode (CARAPICALLTYPE *GetParamInfoByName)(
            IFunctionInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IParamInfo ** paramInfo);

    ECode (CARAPICALLTYPE *GetAnnotation)(
            IMethodInfo *pThis,
            /* [out] */ String* annotation);

    ECode (CARAPICALLTYPE *CreateArgumentList)(
            IMethodInfo *pThis,
            /* [out] */ IArgumentList ** argumentList);

    ECode (CARAPICALLTYPE *Invoke)(
            IMethodInfo *pThis,
            /* [in] */ PInterface target,
            /* [in] */ IArgumentList * argumentList);
}   IMethodInfoVtbl;

interface IMethodInfo {
    CONST_VTBL struct IMethodInfoVtbl *v;
};

typedef struct IParamInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IParamInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IParamInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IParamInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IParamInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetMethodInfo)(
            IParamInfo *pThis,
            /* [out] */ IMethodInfo ** methodInfo);

    ECode (CARAPICALLTYPE *GetName)(
            IParamInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetIndex)(
            IParamInfo *pThis,
            /* [out] */ Int32 * index);

    ECode (CARAPICALLTYPE *GetIOAttribute)(
            IParamInfo *pThis,
            /* [out] */ ParamIOAttribute * ioAttrib);

    ECode (CARAPICALLTYPE *IsReturnValue)(
            IParamInfo *pThis,
            /* [out] */ Boolean * returnValue);

    ECode (CARAPICALLTYPE *GetTypeInfo)(
            IParamInfo *pThis,
            /* [out] */ IDataTypeInfo ** typeInfo);

    ECode (CARAPICALLTYPE *GetAdvisedCapacity)(
            IParamInfo *pThis,
            /* [out] */ Int32 * advisedCapacity);

    ECode (CARAPICALLTYPE *IsUsingTypeAlias)(
            IParamInfo *pThis,
            /* [out] */ Boolean * usingTypeAlias);

    ECode (CARAPICALLTYPE *GetUsedTypeAliasInfo)(
            IParamInfo *pThis,
            /* [out] */ ITypeAliasInfo ** usedTypeAliasInfo);
}   IParamInfoVtbl;

interface IParamInfo {
    CONST_VTBL struct IParamInfoVtbl *v;
};

typedef struct ITypeAliasInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ITypeAliasInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ITypeAliasInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ITypeAliasInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ITypeAliasInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            ITypeAliasInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetTypeInfo)(
            ITypeAliasInfo *pThis,
            /* [out] */ IDataTypeInfo ** typeInfo);

    ECode (CARAPICALLTYPE *GetModuleInfo)(
            ITypeAliasInfo *pThis,
            /* [out] */ IModuleInfo ** moduleInfo);

    ECode (CARAPICALLTYPE *IsDummy)(
            ITypeAliasInfo *pThis,
            /* [out] */ Boolean * isDummy);

    ECode (CARAPICALLTYPE *GetPtrLevel)(
            ITypeAliasInfo *pThis,
            /* [out] */ Int32 * level);
}   ITypeAliasInfoVtbl;

interface ITypeAliasInfo {
    CONST_VTBL struct ITypeAliasInfoVtbl *v;
};

typedef struct IArgumentListVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IArgumentList *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IArgumentList *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IArgumentList *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IArgumentList *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetFunctionInfo)(
            IArgumentList *pThis,
            /* [out] */ IFunctionInfo ** functionInfo);

    ECode (CARAPICALLTYPE *SetInputArgumentOfInt16)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int16 value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfInt32)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int32 value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfInt64)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int64 value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfByte)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Byte value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfFloat)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Float value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfDouble)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Double value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfChar)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Char32 value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfString)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ String value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfBoolean)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Boolean value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfEMuid)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ EMuid * value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfEGuid)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ EGuid * value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfECode)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ ECode value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfLocalPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ LocalPtr value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfLocalType)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ PVoid value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfEnum)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int32 value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfCarArray)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ PCarQuintet value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfStructPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ PVoid value);

    ECode (CARAPICALLTYPE *SetInputArgumentOfObjectPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [in] */ PInterface value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfInt16Ptr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int16 * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfInt32Ptr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfInt64Ptr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int64 * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfBytePtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Byte * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfFloatPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Float * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfDoublePtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Double * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfCharPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Char32 * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfStringPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ String* value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfBooleanPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Boolean * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfEMuidPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ EMuid * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfEGuidPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ EGuid * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfECodePtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ ECode * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfLocalPtrPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ LocalPtr * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfLocalTypePtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PVoid value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfEnumPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfCarArrayPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PCarQuintet value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfCarArrayPtrPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PCarQuintet * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfStructPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PVoid value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfStructPtrPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PVoid * value);

    ECode (CARAPICALLTYPE *SetOutputArgumentOfObjectPtrPtr)(
            IArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PInterface * value);
}   IArgumentListVtbl;

interface IArgumentList {
    CONST_VTBL struct IArgumentListVtbl *v;
};

typedef struct IVariableVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IVariable *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IVariable *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IVariable *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IVariable *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetTypeInfo)(
            IVariable *pThis,
            /* [out] */ IDataTypeInfo ** typeInfo);

    ECode (CARAPICALLTYPE *GetPayload)(
            IVariable *pThis,
            /* [out] */ PVoid * payload);

    ECode (CARAPICALLTYPE *Rebox)(
            IVariable *pThis,
            /* [in] */ PVoid localVariablePtr);
}   IVariableVtbl;

interface IVariable {
    CONST_VTBL struct IVariableVtbl *v;
};

typedef struct IVariableOfCarArrayVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IVariableOfCarArray *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IVariableOfCarArray *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IVariableOfCarArray *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IVariableOfCarArray *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetTypeInfo)(
            IVariable *pThis,
            /* [out] */ IDataTypeInfo ** typeInfo);

    ECode (CARAPICALLTYPE *GetPayload)(
            IVariable *pThis,
            /* [out] */ PVoid * payload);

    ECode (CARAPICALLTYPE *Rebox)(
            IVariable *pThis,
            /* [in] */ PVoid localVariablePtr);

    ECode (CARAPICALLTYPE *GetSetter)(
            IVariableOfCarArray *pThis,
            /* [out] */ ICarArraySetter ** setter);

    ECode (CARAPICALLTYPE *GetGetter)(
            IVariableOfCarArray *pThis,
            /* [out] */ ICarArrayGetter ** getter);
}   IVariableOfCarArrayVtbl;

interface IVariableOfCarArray {
    CONST_VTBL struct IVariableOfCarArrayVtbl *v;
};

typedef struct IVariableOfStructVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IVariableOfStruct *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IVariableOfStruct *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IVariableOfStruct *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IVariableOfStruct *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetTypeInfo)(
            IVariable *pThis,
            /* [out] */ IDataTypeInfo ** typeInfo);

    ECode (CARAPICALLTYPE *GetPayload)(
            IVariable *pThis,
            /* [out] */ PVoid * payload);

    ECode (CARAPICALLTYPE *Rebox)(
            IVariable *pThis,
            /* [in] */ PVoid localVariablePtr);

    ECode (CARAPICALLTYPE *GetSetter)(
            IVariableOfStruct *pThis,
            /* [out] */ IStructSetter ** setter);

    ECode (CARAPICALLTYPE *GetGetter)(
            IVariableOfStruct *pThis,
            /* [out] */ IStructGetter ** getter);
}   IVariableOfStructVtbl;

interface IVariableOfStruct {
    CONST_VTBL struct IVariableOfStructVtbl *v;
};

typedef struct ICarArraySetterVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICarArraySetter *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICarArraySetter *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICarArraySetter *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICarArraySetter *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *SetUsed)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 used);

    ECode (CARAPICALLTYPE *SetInt16Element)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int16 value);

    ECode (CARAPICALLTYPE *SetInt32Element)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int32 value);

    ECode (CARAPICALLTYPE *SetInt64Element)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int64 value);

    ECode (CARAPICALLTYPE *SetByteElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Byte value);

    ECode (CARAPICALLTYPE *SetFloatElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Float value);

    ECode (CARAPICALLTYPE *SetDoubleElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Double value);

    ECode (CARAPICALLTYPE *SetEnumElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int32 value);

    ECode (CARAPICALLTYPE *SetCharElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Char32 value);

    ECode (CARAPICALLTYPE *SetStringElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ String value);

    ECode (CARAPICALLTYPE *SetBooleanElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Boolean value);

    ECode (CARAPICALLTYPE *SetEMuidElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ EMuid * value);

    ECode (CARAPICALLTYPE *SetEGuidElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ EGuid * value);

    ECode (CARAPICALLTYPE *SetECodeElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ ECode value);

    ECode (CARAPICALLTYPE *SetLocalTypeElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ PVoid value);

    ECode (CARAPICALLTYPE *SetObjectPtrElement)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ PInterface value);

    ECode (CARAPICALLTYPE *GetStructElementSetter)(
            ICarArraySetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ IStructSetter ** setter);
}   ICarArraySetterVtbl;

interface ICarArraySetter {
    CONST_VTBL struct ICarArraySetterVtbl *v;
};

typedef struct ICarArrayGetterVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICarArrayGetter *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICarArrayGetter *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICarArrayGetter *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICarArrayGetter *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetCapacity)(
            ICarArrayGetter *pThis,
            /* [out] */ Int32 * capacity);

    ECode (CARAPICALLTYPE *GetUsed)(
            ICarArrayGetter *pThis,
            /* [out] */ Int32 * used);

    ECode (CARAPICALLTYPE *GetInt16Element)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int16 * value);

    ECode (CARAPICALLTYPE *GetInt32Element)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *GetInt64Element)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int64 * value);

    ECode (CARAPICALLTYPE *GetByteElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Byte * value);

    ECode (CARAPICALLTYPE *GetFloatElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Float * value);

    ECode (CARAPICALLTYPE *GetDoubleElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Double * value);

    ECode (CARAPICALLTYPE *GetEnumElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *GetCharElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Char32 * value);

    ECode (CARAPICALLTYPE *GetStringElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ String* value);

    ECode (CARAPICALLTYPE *GetBooleanElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Boolean * value);

    ECode (CARAPICALLTYPE *GetEMuidElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ EMuid * value);

    ECode (CARAPICALLTYPE *GetEGuidElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ EGuid * value);

    ECode (CARAPICALLTYPE *GetECodeElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ ECode * value);

    ECode (CARAPICALLTYPE *GetLocalTypeElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PVoid value);

    ECode (CARAPICALLTYPE *GetObjectPtrElement)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PInterface * value);

    ECode (CARAPICALLTYPE *GetStructElementGetter)(
            ICarArrayGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ IStructGetter ** getter);
}   ICarArrayGetterVtbl;

interface ICarArrayGetter {
    CONST_VTBL struct ICarArrayGetterVtbl *v;
};

typedef struct IStructSetterVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IStructSetter *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IStructSetter *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IStructSetter *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IStructSetter *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *ZeroAllFields)(
            IStructSetter *pThis);

    ECode (CARAPICALLTYPE *SetInt16Field)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ Int16 value);

    ECode (CARAPICALLTYPE *SetInt32Field)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ Int32 value);

    ECode (CARAPICALLTYPE *SetInt64Field)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ Int64 value);

    ECode (CARAPICALLTYPE *SetByteField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ Byte value);

    ECode (CARAPICALLTYPE *SetFloatField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ Float value);

    ECode (CARAPICALLTYPE *SetDoubleField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ Double value);

    ECode (CARAPICALLTYPE *SetCharField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ Char32 value);

    ECode (CARAPICALLTYPE *SetBooleanField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ Boolean value);

    ECode (CARAPICALLTYPE *SetEMuidField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ EMuid * value);

    ECode (CARAPICALLTYPE *SetEGuidField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ EGuid * value);

    ECode (CARAPICALLTYPE *SetECodeField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ ECode value);

    ECode (CARAPICALLTYPE *SetLocalPtrField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ LocalPtr value);

    ECode (CARAPICALLTYPE *SetLocalTypeField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ PVoid value);

    ECode (CARAPICALLTYPE *SetEnumField)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [in] */ Int32 value);

    ECode (CARAPICALLTYPE *GetStructFieldSetter)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [out] */ IStructSetter ** setter);

    ECode (CARAPICALLTYPE *GetCppVectorFieldSetter)(
            IStructSetter *pThis,
            /* [in] */ String name,
            /* [out] */ ICppVectorSetter ** setter);
}   IStructSetterVtbl;

interface IStructSetter {
    CONST_VTBL struct IStructSetterVtbl *v;
};

typedef struct IStructGetterVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IStructGetter *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IStructGetter *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IStructGetter *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IStructGetter *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetInt16Field)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ Int16 * value);

    ECode (CARAPICALLTYPE *GetInt32Field)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *GetInt64Field)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ Int64 * value);

    ECode (CARAPICALLTYPE *GetByteField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ Byte * value);

    ECode (CARAPICALLTYPE *GetFloatField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ Float * value);

    ECode (CARAPICALLTYPE *GetDoubleField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ Double * value);

    ECode (CARAPICALLTYPE *GetCharField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ Char32 * value);

    ECode (CARAPICALLTYPE *GetBooleanField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ Boolean * value);

    ECode (CARAPICALLTYPE *GetEMuidField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ EMuid * value);

    ECode (CARAPICALLTYPE *GetEGuidField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ EGuid * value);

    ECode (CARAPICALLTYPE *GetECodeField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ ECode * value);

    ECode (CARAPICALLTYPE *GetLocalPtrField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ LocalPtr * value);

    ECode (CARAPICALLTYPE *GetLocalTypeField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ PVoid value);

    ECode (CARAPICALLTYPE *GetEnumField)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *GetStructFieldGetter)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ IStructGetter ** getter);

    ECode (CARAPICALLTYPE *GetCppVectorFieldGetter)(
            IStructGetter *pThis,
            /* [in] */ String name,
            /* [out] */ ICppVectorGetter ** getter);
}   IStructGetterVtbl;

interface IStructGetter {
    CONST_VTBL struct IStructGetterVtbl *v;
};

typedef struct ICppVectorSetterVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICppVectorSetter *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICppVectorSetter *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICppVectorSetter *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICppVectorSetter *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *ZeroAllElements)(
            ICppVectorSetter *pThis);

    ECode (CARAPICALLTYPE *SetAllElements)(
            ICppVectorSetter *pThis,
            /* [in] */ PVoid value,
            /* [in] */ MemorySize size);

    ECode (CARAPICALLTYPE *SetInt16Element)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int16 value);

    ECode (CARAPICALLTYPE *SetInt32Element)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int32 value);

    ECode (CARAPICALLTYPE *SetInt64Element)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int64 value);

    ECode (CARAPICALLTYPE *SetByteElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Byte value);

    ECode (CARAPICALLTYPE *SetFloatElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Float value);

    ECode (CARAPICALLTYPE *SetDoubleElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Double value);

    ECode (CARAPICALLTYPE *SetCharElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Char32 value);

    ECode (CARAPICALLTYPE *SetBooleanElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Boolean value);

    ECode (CARAPICALLTYPE *SetEMuidElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ EMuid * value);

    ECode (CARAPICALLTYPE *SetEGuidElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ EGuid * value);

    ECode (CARAPICALLTYPE *SetECodeElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ ECode value);

    ECode (CARAPICALLTYPE *SetLocalPtrElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ LocalPtr value);

    ECode (CARAPICALLTYPE *SetLocalTypeElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ PVoid value);

    ECode (CARAPICALLTYPE *SetEnumElement)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [in] */ Int32 value);

    ECode (CARAPICALLTYPE *GetStructElementSetter)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ IStructSetter ** setter);

    ECode (CARAPICALLTYPE *GetCppVectorElementSetter)(
            ICppVectorSetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ ICppVectorSetter ** setter);
}   ICppVectorSetterVtbl;

interface ICppVectorSetter {
    CONST_VTBL struct ICppVectorSetterVtbl *v;
};

typedef struct ICppVectorGetterVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICppVectorGetter *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICppVectorGetter *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICppVectorGetter *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICppVectorGetter *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetLength)(
            ICppVectorGetter *pThis,
            /* [out] */ Int32 * length);

    ECode (CARAPICALLTYPE *GetRank)(
            ICppVectorGetter *pThis,
            /* [out] */ Int32 * rank);

    ECode (CARAPICALLTYPE *GetInt16Element)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int16 * value);

    ECode (CARAPICALLTYPE *GetInt32Element)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *GetInt64Element)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int64 * value);

    ECode (CARAPICALLTYPE *GetByteElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Byte * value);

    ECode (CARAPICALLTYPE *GetFloatElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Float * value);

    ECode (CARAPICALLTYPE *GetDoubleElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Double * value);

    ECode (CARAPICALLTYPE *GetCharElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Char32 * value);

    ECode (CARAPICALLTYPE *GetBooleanElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Boolean * value);

    ECode (CARAPICALLTYPE *GetEMuidElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ EMuid * value);

    ECode (CARAPICALLTYPE *GetEGuidElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ EGuid * value);

    ECode (CARAPICALLTYPE *GetECodeElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ ECode * value);

    ECode (CARAPICALLTYPE *GetLocalPtrElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ LocalPtr * value);

    ECode (CARAPICALLTYPE *GetLocalTypeElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PVoid value);

    ECode (CARAPICALLTYPE *GetEnumElement)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *GetStructElementGetter)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ IStructGetter ** getter);

    ECode (CARAPICALLTYPE *GetCppVectorElementGetter)(
            ICppVectorGetter *pThis,
            /* [in] */ Int32 index,
            /* [out] */ ICppVectorGetter ** getter);
}   ICppVectorGetterVtbl;

interface ICppVectorGetter {
    CONST_VTBL struct ICppVectorGetterVtbl *v;
};

typedef struct IModuleInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IModuleInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IModuleInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IModuleInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IModuleInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetPath)(
            IModuleInfo *pThis,
            /* [out] */ String* path);

    ECode (CARAPICALLTYPE *GetVersion)(
            IModuleInfo *pThis,
            /* [out] */ Int32 * major,
            /* [out] */ Int32 * minor,
            /* [out] */ Int32 * build,
            /* [out] */ Int32 * revision);

    ECode (CARAPICALLTYPE *GetClassCount)(
            IModuleInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllClassInfos)(
            IModuleInfo *pThis,
            /* [out] */ PCarQuintet classInfos);

    ECode (CARAPICALLTYPE *GetClassInfo)(
            IModuleInfo *pThis,
            /* [in] */ String fullName,
            /* [out] */ IClassInfo ** classInfo);

    ECode (CARAPICALLTYPE *GetInterfaceCount)(
            IModuleInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllInterfaceInfos)(
            IModuleInfo *pThis,
            /* [out] */ PCarQuintet interfaceInfos);

    ECode (CARAPICALLTYPE *GetInterfaceInfo)(
            IModuleInfo *pThis,
            /* [in] */ String fullName,
            /* [out] */ IInterfaceInfo ** interfaceInfo);

    ECode (CARAPICALLTYPE *GetStructCount)(
            IModuleInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllStructInfos)(
            IModuleInfo *pThis,
            /* [out] */ PCarQuintet structInfos);

    ECode (CARAPICALLTYPE *GetStructInfo)(
            IModuleInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IStructInfo ** structInfo);

    ECode (CARAPICALLTYPE *GetEnumCount)(
            IModuleInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllEnumInfos)(
            IModuleInfo *pThis,
            /* [out] */ PCarQuintet enumInfos);

    ECode (CARAPICALLTYPE *GetEnumInfo)(
            IModuleInfo *pThis,
            /* [in] */ String fullName,
            /* [out] */ IEnumInfo ** enumInfo);

    ECode (CARAPICALLTYPE *GetTypeAliasCount)(
            IModuleInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllTypeAliasInfos)(
            IModuleInfo *pThis,
            /* [out] */ PCarQuintet typeAliasInfos);

    ECode (CARAPICALLTYPE *GetTypeAliasInfo)(
            IModuleInfo *pThis,
            /* [in] */ String name,
            /* [out] */ ITypeAliasInfo ** typeAliasInfo);

    ECode (CARAPICALLTYPE *GetConstantCount)(
            IModuleInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllConstantInfos)(
            IModuleInfo *pThis,
            /* [out] */ PCarQuintet constantInfos);

    ECode (CARAPICALLTYPE *GetConstantInfo)(
            IModuleInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IConstantInfo ** constantInfo);

    ECode (CARAPICALLTYPE *GetImportModuleInfoCount)(
            IModuleInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllImportModuleInfos)(
            IModuleInfo *pThis,
            /* [out] */ PCarQuintet moduleInfos);
}   IModuleInfoVtbl;

interface IModuleInfo {
    CONST_VTBL struct IModuleInfoVtbl *v;
};

typedef struct IConstantInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IConstantInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IConstantInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IConstantInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IConstantInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IConstantInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetValue)(
            IConstantInfo *pThis,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *GetModuleInfo)(
            IConstantInfo *pThis,
            /* [out] */ IModuleInfo ** moduleInfo);
}   IConstantInfoVtbl;

interface IConstantInfo {
    CONST_VTBL struct IConstantInfoVtbl *v;
};

typedef struct IClassInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IClassInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IClassInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IClassInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IClassInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IClassInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetNamespace)(
            IClassInfo *pThis,
            /* [out] */ String* ns);

    ECode (CARAPICALLTYPE *GetId)(
            IClassInfo *pThis,
            /* [out] */ ClassID * clsid);

    ECode (CARAPICALLTYPE *GetModuleInfo)(
            IClassInfo *pThis,
            /* [out] */ IModuleInfo ** moduleInfo);

    ECode (CARAPICALLTYPE *GetClassLoader)(
            IClassInfo *pThis,
            /* [out] */ IInterface ** loader);

    ECode (CARAPICALLTYPE *SetClassLoader)(
            IClassInfo *pThis,
            /* [in] */ IInterface * loader);

    ECode (CARAPICALLTYPE *IsSingleton)(
            IClassInfo *pThis,
            /* [out] */ Boolean * isSingleton);

    ECode (CARAPICALLTYPE *GetThreadingModel)(
            IClassInfo *pThis,
            /* [out] */ ThreadingModel * threadingModel);

    ECode (CARAPICALLTYPE *IsPrivate)(
            IClassInfo *pThis,
            /* [out] */ Boolean * isPrivate);

    ECode (CARAPICALLTYPE *IsReturnValue)(
            IClassInfo *pThis,
            /* [out] */ Boolean * returnValue);

    ECode (CARAPICALLTYPE *IsBaseClass)(
            IClassInfo *pThis,
            /* [out] */ Boolean * isBaseClass);

    ECode (CARAPICALLTYPE *HasBaseClass)(
            IClassInfo *pThis,
            /* [out] */ Boolean * hasBaseClass);

    ECode (CARAPICALLTYPE *GetBaseClassInfo)(
            IClassInfo *pThis,
            /* [out] */ IClassInfo ** baseClassInfo);

    ECode (CARAPICALLTYPE *IsGeneric)(
            IClassInfo *pThis,
            /* [out] */ Boolean * isGeneric);

    ECode (CARAPICALLTYPE *HasGeneric)(
            IClassInfo *pThis,
            /* [out] */ Boolean * hasGeneric);

    ECode (CARAPICALLTYPE *GetGenericInfo)(
            IClassInfo *pThis,
            /* [out] */ IClassInfo ** genericInfo);

    ECode (CARAPICALLTYPE *IsRegime)(
            IClassInfo *pThis,
            /* [out] */ Boolean * isRegime);

    ECode (CARAPICALLTYPE *GetAspectCount)(
            IClassInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllAspectInfos)(
            IClassInfo *pThis,
            /* [out] */ PCarQuintet aspectInfos);

    ECode (CARAPICALLTYPE *GetAspectInfo)(
            IClassInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IClassInfo ** aspectInfo);

    ECode (CARAPICALLTYPE *IsAspect)(
            IClassInfo *pThis,
            /* [out] */ Boolean * isAspect);

    ECode (CARAPICALLTYPE *GetAggregateeCount)(
            IClassInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllAggregateeInfos)(
            IClassInfo *pThis,
            /* [out] */ PCarQuintet aggregateeInfos);

    ECode (CARAPICALLTYPE *GetAggregateeInfo)(
            IClassInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IClassInfo ** aggregateeInfo);

    ECode (CARAPICALLTYPE *GetConstructorCount)(
            IClassInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllConstructorInfos)(
            IClassInfo *pThis,
            /* [out] */ PCarQuintet constructorInfos);

    ECode (CARAPICALLTYPE *GetConstructorInfoByParamNames)(
            IClassInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IConstructorInfo ** constructorInfo);

    ECode (CARAPICALLTYPE *GetConstructorInfoByParamCount)(
            IClassInfo *pThis,
            /* [in] */ Int32 count,
            /* [out] */ IConstructorInfo ** constructorInfo);

    ECode (CARAPICALLTYPE *GetInterfaceCount)(
            IClassInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllInterfaceInfos)(
            IClassInfo *pThis,
            /* [out] */ PCarQuintet interfaceInfos);

    ECode (CARAPICALLTYPE *GetInterfaceInfo)(
            IClassInfo *pThis,
            /* [in] */ String fullName,
            /* [out] */ IInterfaceInfo ** interfaceInfo);

    ECode (CARAPICALLTYPE *HasInterfaceInfo)(
            IClassInfo *pThis,
            /* [in] */ IInterfaceInfo * interfaceInfo,
            /* [out] */ Boolean * result);

    ECode (CARAPICALLTYPE *GetCallbackInterfaceCount)(
            IClassInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllCallbackInterfaceInfos)(
            IClassInfo *pThis,
            /* [out] */ PCarQuintet callbackInterfaceInfos);

    ECode (CARAPICALLTYPE *GetCallbackInterfaceInfo)(
            IClassInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IInterfaceInfo ** callbackInterfaceInfo);

    ECode (CARAPICALLTYPE *GetMethodCount)(
            IClassInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllMethodInfos)(
            IClassInfo *pThis,
            /* [out] */ PCarQuintet methodInfos);

    ECode (CARAPICALLTYPE *GetMethodInfo)(
            IClassInfo *pThis,
            /* [in] */ String name,
            /* [in] */ String signature,
            /* [out] */ IMethodInfo ** methodInfo);

    ECode (CARAPICALLTYPE *GetCallbackMethodCount)(
            IClassInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllCallbackMethodInfos)(
            IClassInfo *pThis,
            /* [out] */ PCarQuintet callbackMethodInfos);

    ECode (CARAPICALLTYPE *GetCallbackMethodInfo)(
            IClassInfo *pThis,
            /* [in] */ String name,
            /* [out] */ ICallbackMethodInfo ** callbackMethodInfo);

    ECode (CARAPICALLTYPE *RemoveAllCallbackHandlers)(
            IClassInfo *pThis,
            /* [in] */ PInterface server);

    ECode (CARAPICALLTYPE *CreateObject)(
            IClassInfo *pThis,
            /* [out] */ PInterface * object);

    ECode (CARAPICALLTYPE *CreateObjectInRegime)(
            IClassInfo *pThis,
            /* [in] */ PRegime rgm,
            /* [out] */ PInterface * object);
}   IClassInfoVtbl;

interface IClassInfo {
    CONST_VTBL struct IClassInfoVtbl *v;
};

typedef struct IConstructorInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IConstructorInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IConstructorInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IConstructorInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IConstructorInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IFunctionInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetParamCount)(
            IFunctionInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllParamInfos)(
            IFunctionInfo *pThis,
            /* [out] */ PCarQuintet paramInfos);

    ECode (CARAPICALLTYPE *GetParamInfoByIndex)(
            IFunctionInfo *pThis,
            /* [in] */ Int32 index,
            /* [out] */ IParamInfo ** paramInfo);

    ECode (CARAPICALLTYPE *GetParamInfoByName)(
            IFunctionInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IParamInfo ** paramInfo);

    ECode (CARAPICALLTYPE *GetAnnotation)(
            IConstructorInfo *pThis,
            /* [out] */ String* annotation);

    ECode (CARAPICALLTYPE *CreateArgumentList)(
            IConstructorInfo *pThis,
            /* [out] */ IArgumentList ** argumentList);

    ECode (CARAPICALLTYPE *CreateObject)(
            IConstructorInfo *pThis,
            /* [in] */ IArgumentList * argumentList,
            /* [out] */ PInterface * object);

    ECode (CARAPICALLTYPE *CreateObjectInRegime)(
            IConstructorInfo *pThis,
            /* [in] */ PRegime rgm,
            /* [in] */ IArgumentList * argumentList,
            /* [out] */ PInterface * object);
}   IConstructorInfoVtbl;

interface IConstructorInfo {
    CONST_VTBL struct IConstructorInfoVtbl *v;
};

typedef struct ICallbackMethodInfoVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICallbackMethodInfo *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICallbackMethodInfo *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICallbackMethodInfo *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICallbackMethodInfo *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetName)(
            IFunctionInfo *pThis,
            /* [out] */ String* name);

    ECode (CARAPICALLTYPE *GetParamCount)(
            IFunctionInfo *pThis,
            /* [out] */ Int32 * count);

    ECode (CARAPICALLTYPE *GetAllParamInfos)(
            IFunctionInfo *pThis,
            /* [out] */ PCarQuintet paramInfos);

    ECode (CARAPICALLTYPE *GetParamInfoByIndex)(
            IFunctionInfo *pThis,
            /* [in] */ Int32 index,
            /* [out] */ IParamInfo ** paramInfo);

    ECode (CARAPICALLTYPE *GetParamInfoByName)(
            IFunctionInfo *pThis,
            /* [in] */ String name,
            /* [out] */ IParamInfo ** paramInfo);

    ECode (CARAPICALLTYPE *AddCallback)(
            ICallbackMethodInfo *pThis,
            /* [in] */ PInterface server,
            /* [in] */ EventHandler handler);

    ECode (CARAPICALLTYPE *RemoveCallback)(
            ICallbackMethodInfo *pThis,
            /* [in] */ PInterface server,
            /* [in] */ EventHandler handler);

    ECode (CARAPICALLTYPE *CreateDelegateProxy)(
            ICallbackMethodInfo *pThis,
            /* [in] */ PVoid targetObject,
            /* [in] */ PVoid targetMethod,
            /* [in] */ ICallbackInvocation * callbackInvocation,
            /* [out] */ IDelegateProxy ** delegateProxy);
}   ICallbackMethodInfoVtbl;

interface ICallbackMethodInfo {
    CONST_VTBL struct ICallbackMethodInfoVtbl *v;
};

typedef struct IDelegateProxyVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IDelegateProxy *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IDelegateProxy *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IDelegateProxy *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IDelegateProxy *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetCallbackMethodInfo)(
            IDelegateProxy *pThis,
            /* [out] */ ICallbackMethodInfo ** callbackMethodInfo);

    ECode (CARAPICALLTYPE *GetTargetObject)(
            IDelegateProxy *pThis,
            /* [out] */ PVoid * targetObject);

    ECode (CARAPICALLTYPE *GetTargetMethod)(
            IDelegateProxy *pThis,
            /* [out] */ PVoid * targetMethod);

    ECode (CARAPICALLTYPE *GetCallbackInvocation)(
            IDelegateProxy *pThis,
            /* [out] */ ICallbackInvocation ** callbackInvocation);

    ECode (CARAPICALLTYPE *GetDelegate)(
            IDelegateProxy *pThis,
            /* [out] */ EventHandler * handler);
}   IDelegateProxyVtbl;

interface IDelegateProxy {
    CONST_VTBL struct IDelegateProxyVtbl *v;
};

typedef struct ICallbackInvocationVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICallbackInvocation *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICallbackInvocation *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICallbackInvocation *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICallbackInvocation *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *Invoke)(
            ICallbackInvocation *pThis,
            /* [in] */ PVoid targetObject,
            /* [in] */ PVoid targetMethod,
            /* [in] */ ICallbackArgumentList * callbackArgumentList);
}   ICallbackInvocationVtbl;

interface ICallbackInvocation {
    CONST_VTBL struct ICallbackInvocationVtbl *v;
};

typedef struct ICallbackArgumentListVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICallbackArgumentList *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICallbackArgumentList *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICallbackArgumentList *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICallbackArgumentList *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetCallbackMethodInfo)(
            ICallbackArgumentList *pThis,
            /* [out] */ ICallbackMethodInfo ** callbackMethodInfo);

    ECode (CARAPICALLTYPE *GetServerPtrArgument)(
            ICallbackArgumentList *pThis,
            /* [out] */ PInterface * server);

    ECode (CARAPICALLTYPE *GetInt16Argument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int16 * value);

    ECode (CARAPICALLTYPE *GetInt32Argument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *GetInt64Argument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int64 * value);

    ECode (CARAPICALLTYPE *GetByteArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Byte * value);

    ECode (CARAPICALLTYPE *GetFloatArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Float * value);

    ECode (CARAPICALLTYPE *GetDoubleArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Double * value);

    ECode (CARAPICALLTYPE *GetCharArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Char32 * value);

    ECode (CARAPICALLTYPE *GetStringArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ String* value);

    ECode (CARAPICALLTYPE *GetBooleanArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Boolean * value);

    ECode (CARAPICALLTYPE *GetEMuidArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ EMuid ** value);

    ECode (CARAPICALLTYPE *GetEGuidArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ EGuid ** value);

    ECode (CARAPICALLTYPE *GetECodeArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ ECode * value);

    ECode (CARAPICALLTYPE *GetLocalPtrArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ LocalPtr * value);

    ECode (CARAPICALLTYPE *GetEnumArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *GetCarArrayArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PCarQuintet * value);

    ECode (CARAPICALLTYPE *GetStructPtrArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PVoid * value);

    ECode (CARAPICALLTYPE *GetObjectPtrArgument)(
            ICallbackArgumentList *pThis,
            /* [in] */ Int32 index,
            /* [out] */ PInterface * value);
}   ICallbackArgumentListVtbl;

interface ICallbackArgumentList {
    CONST_VTBL struct ICallbackArgumentListVtbl *v;
};

typedef struct IParcelVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IParcel *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IParcel *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IParcel *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IParcel *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *Marshall)(
            IParcel *pThis,
            /* [out, callee] */ PCarQuintet * bytes);

    ECode (CARAPICALLTYPE *Unmarshall)(
            IParcel *pThis,
            /* [in] */ PCarQuintet data,
            /* [in] */ Int32 offest,
            /* [in] */ Int32 length);

    ECode (CARAPICALLTYPE *AppendFrom)(
            IParcel *pThis,
            /* [in] */ IParcel * parcel,
            /* [in] */ Int32 offset,
            /* [in] */ Int32 length);

    ECode (CARAPICALLTYPE *HasFileDescriptors)(
            IParcel *pThis,
            /* [out] */ Boolean * result);

    ECode (CARAPICALLTYPE *ReadByte)(
            IParcel *pThis,
            /* [out] */ Byte * value);

    ECode (CARAPICALLTYPE *WriteByte)(
            IParcel *pThis,
            /* [in] */ Byte value);

    ECode (CARAPICALLTYPE *ReadBoolean)(
            IParcel *pThis,
            /* [out] */ Boolean * value);

    ECode (CARAPICALLTYPE *WriteBoolean)(
            IParcel *pThis,
            /* [in] */ Boolean value);

    ECode (CARAPICALLTYPE *ReadChar)(
            IParcel *pThis,
            /* [out] */ Char32 * value);

    ECode (CARAPICALLTYPE *WriteChar)(
            IParcel *pThis,
            /* [in] */ Char32 value);

    ECode (CARAPICALLTYPE *ReadInt16)(
            IParcel *pThis,
            /* [out] */ Int16 * value);

    ECode (CARAPICALLTYPE *WriteInt16)(
            IParcel *pThis,
            /* [in] */ Int16 value);

    ECode (CARAPICALLTYPE *ReadInt32)(
            IParcel *pThis,
            /* [out] */ Int32 * value);

    ECode (CARAPICALLTYPE *WriteInt32)(
            IParcel *pThis,
            /* [in] */ Int32 value);

    ECode (CARAPICALLTYPE *ReadInt64)(
            IParcel *pThis,
            /* [out] */ Int64 * value);

    ECode (CARAPICALLTYPE *WriteInt64)(
            IParcel *pThis,
            /* [in] */ Int64 value);

    ECode (CARAPICALLTYPE *ReadFloat)(
            IParcel *pThis,
            /* [out] */ Float * value);

    ECode (CARAPICALLTYPE *WriteFloat)(
            IParcel *pThis,
            /* [in] */ Float value);

    ECode (CARAPICALLTYPE *ReadDouble)(
            IParcel *pThis,
            /* [out] */ Double * value);

    ECode (CARAPICALLTYPE *WriteDouble)(
            IParcel *pThis,
            /* [in] */ Double value);

    ECode (CARAPICALLTYPE *ReadString)(
            IParcel *pThis,
            /* [out] */ String* str);

    ECode (CARAPICALLTYPE *WriteString)(
            IParcel *pThis,
            /* [in] */ String str);

    ECode (CARAPICALLTYPE *ReadStruct)(
            IParcel *pThis,
            /* [out] */ Handle32 * address);

    ECode (CARAPICALLTYPE *WriteStruct)(
            IParcel *pThis,
            /* [in] */ Handle32 value,
            /* [in] */ Int32 size);

    ECode (CARAPICALLTYPE *ReadEMuid)(
            IParcel *pThis,
            /* [out] */ EMuid * id);

    ECode (CARAPICALLTYPE *WriteEMuid)(
            IParcel *pThis,
            /* [in] */ const EMuid * id);

    ECode (CARAPICALLTYPE *ReadEGuid)(
            IParcel *pThis,
            /* [out] */ EGuid * id);

    ECode (CARAPICALLTYPE *WriteEGuid)(
            IParcel *pThis,
            /* [in] */ const EGuid * id);

    ECode (CARAPICALLTYPE *ReadArrayOf)(
            IParcel *pThis,
            /* [out] */ Handle32 * array);

    ECode (CARAPICALLTYPE *WriteArrayOf)(
            IParcel *pThis,
            /* [in] */ Handle32 array);

    ECode (CARAPICALLTYPE *ReadArrayOfString)(
            IParcel *pThis,
            /* [out, callee] */ PCarQuintet * array);

    ECode (CARAPICALLTYPE *WriteArrayOfString)(
            IParcel *pThis,
            /* [in] */ PCarQuintet array);

    ECode (CARAPICALLTYPE *ReadInterfacePtr)(
            IParcel *pThis,
            /* [out] */ Handle32 * itfpp);

    ECode (CARAPICALLTYPE *WriteInterfacePtr)(
            IParcel *pThis,
            /* [in] */ IInterface * value);

    ECode (CARAPICALLTYPE *WriteFileDescriptor)(
            IParcel *pThis,
            /* [in] */ Int32 fd);

    ECode (CARAPICALLTYPE *WriteDupFileDescriptor)(
            IParcel *pThis,
            /* [in] */ Int32 fd);

    ECode (CARAPICALLTYPE *ReadFileDescriptor)(
            IParcel *pThis,
            /* [out] */ Int32 * fd);

    ECode (CARAPICALLTYPE *Clone)(
            IParcel *pThis,
            /* [in] */ IParcel * srcParcel);

    ECode (CARAPICALLTYPE *GetDataPosition)(
            IParcel *pThis,
            /* [out] */ Int32 * position);

    ECode (CARAPICALLTYPE *SetDataPosition)(
            IParcel *pThis,
            /* [in] */ Int32 position);

    ECode (CARAPICALLTYPE *GetElementPayload)(
            IParcel *pThis,
            /* [out] */ Handle32 * buffer);

    ECode (CARAPICALLTYPE *GetElementSize)(
            IParcel *pThis,
            /* [out] */ Int32 * size);
}   IParcelVtbl;

interface IParcel {
    CONST_VTBL struct IParcelVtbl *v;
};

typedef struct IParcelableVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            IParcelable *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            IParcelable *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            IParcelable *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            IParcelable *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *ReadFromParcel)(
            IParcelable *pThis,
            /* [in] */ IParcel * source);

    ECode (CARAPICALLTYPE *WriteToParcel)(
            IParcelable *pThis,
            /* [in] */ IParcel * dest);
}   IParcelableVtbl;

interface IParcelable {
    CONST_VTBL struct IParcelableVtbl *v;
};

typedef struct ICustomMarshalVtbl {
    PInterface (CARAPICALLTYPE *Probe)(
            ICustomMarshal *pThis,
            /* [in] */ REIID riid);

    UInt32 (CARAPICALLTYPE *AddRef)(
            ICustomMarshal *pThis);

    UInt32 (CARAPICALLTYPE *Release)(
            ICustomMarshal *pThis);

    ECode (CARAPICALLTYPE *GetInterfaceID)(
            ICustomMarshal *pThis,
            /* [in] */ IInterface *object,
            /* [in] */ InterfaceID *iid);

    ECode (CARAPICALLTYPE *GetClsid)(
            ICustomMarshal *pThis,
            /* [out] */ ClassID * clsid);

    ECode (CARAPICALLTYPE *CreateObject)(
            ICustomMarshal *pThis,
            /* [in] */ ICustomMarshal * originProxy,
            /* [out] */ IInterface ** newProxy);
}   ICustomMarshalVtbl;

interface ICustomMarshal {
    CONST_VTBL struct ICustomMarshalVtbl *v;
};
