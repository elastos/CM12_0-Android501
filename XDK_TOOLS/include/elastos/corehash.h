
#ifndef __ELASTOS_CORE_HASH_H__
#define __ELASTOS_CORE_HASH_H__

#include "elastos/core/Object.h"

//
// Elastos.Core
//
#ifdef HASH_FOR_CORE
#include <Elastos.CoreLibrary.Core.h>
DEFINE_OBJECT_HASH_FUNC_FOR(Elastos::Core::IInteger32)
#endif

#endif // __ELASTOS_CORE_HASH_H__
