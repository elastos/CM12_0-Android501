
#ifndef __DROIDFRAMEWORKEXT_H__
#define __DROIDFRAMEWORKEXT_H__

#ifndef __ELASTOS_H__
#include <elastos.h>
using namespace Elastos;
#endif

#include <Elastos.Droid.Core.h>

extern "C" const Elastos::ClassID ECLSID_CDummyObject;

#ifndef __ETL_HASH_FUN_H__
#include <elastos/utility/etl/etl_hash_fun.h>
#endif

#ifndef __ETL_FUNCTION_H__
#include <elastos/utility/etl/etl_function.h>
#endif

#ifndef __ELASTOS_DROID_FRAMEWORKDEF_H__
#include "elastos/droid/ext/frameworkdef.h"
#endif

#endif //__FRAMEWORKEXT_H__
