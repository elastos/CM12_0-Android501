/*
 * Copyright (C) 2012 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stddef.h>
#include <stdarg.h>
#include <fcntl.h>
#include <unwind.h>
#include <dlfcn.h>
#include <stdbool.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/system_properties.h>

#include "malloc_debug_disable.h"
#include "private/libc_logging.h"
#include "private/ScopedPthreadMutexLocker.h"

#include "malloc_debug_common.h"

#include <sys/mman.h>

extern const MallocDebug* g_malloc_dispatch;

void* malloc_mmap(size_t bytes) {
    void* mem = mmap(0, (bytes), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    return mem == (void*)-1 ? NULL : mem;
}

void free_munmap(void* mem, size_t bytes) {
    munmap(mem, bytes);
}

static void __attribute__((unused)) log_message(const char* format, ...) {
    extern const MallocDebug __libc_malloc_default_dispatch;
    extern const MallocDebug* __libc_malloc_dispatch;
    extern pthread_mutex_t gAllocationsMutex;

    va_list args;
    {
        ScopedPthreadMutexLocker locker(&gAllocationsMutex);
        const MallocDebug* current_dispatch = __libc_malloc_dispatch;
        __libc_malloc_dispatch = &__libc_malloc_default_dispatch;
        va_start(args, format);
        __libc_format_log(ANDROID_LOG_ERROR, "libc", format, args);
        va_end(args);
        __libc_malloc_dispatch = current_dispatch;
    }
}

struct head_t {
    size_t size;
    uint32_t pad;
};

static inline void* user(head_t* hdr) {
    return hdr + 1;
}

static inline head_t* meta(void* user) {
    return reinterpret_cast<head_t*>(user) - 1;
}

static inline const head_t* const_meta(const void* user) {
    return reinterpret_cast<const head_t*>(user) - 1;
}

extern "C" void* mmap_malloc(size_t size) {
    head_t* hdr = static_cast<head_t*>(malloc_mmap(sizeof(head_t) + size));
    if (hdr) {
        hdr->size = size;
        return user(hdr);
    }
    return NULL;
}

extern "C" void* mmap_memalign(size_t, size_t bytes) {
//    log_message("%s: %s\n", __FILE__, __FUNCTION__);
    // XXX: it's better to use malloc, than being wrong
    // return malloc_mmap(bytes);
    head_t* hdr = static_cast<head_t*>(malloc_mmap(sizeof(head_t) + bytes));
    if (hdr) {
        hdr->size = bytes;
        return user(hdr);
    }
    return NULL;
}

extern "C" void mmap_free(void *ptr) {
//  log_message("%s: %s\n", __FILE__, __FUNCTION__);

    if (!ptr) /* ignore free(NULL) */
        return;

    head_t* hdr = meta(ptr);
    free_munmap(hdr, sizeof(head_t) + hdr->size);
}

extern "C" void *mmap_realloc(void *ptr, size_t size) {
//  log_message("%s: %s\n", __FILE__, __FUNCTION__);

    if (!size) {
        mmap_free(ptr);
        return NULL;
    }

    if (!ptr) {
        return mmap_malloc(size);
    }

    head_t* hdr = meta(ptr);

    head_t* newhdr = static_cast<head_t*>(malloc_mmap(sizeof(head_t) + size));
    if (newhdr) {
        size_t cp_size = sizeof(head_t) + (size > hdr->size ? hdr->size : size);
        memcpy(newhdr, hdr, cp_size);
        mmap_free(ptr);
    }
    hdr = newhdr;
    if (hdr) {
        hdr->size = size;
        return user(hdr);
    }
    return NULL;
}

extern "C" void *mmap_calloc(int nmemb, size_t size) {
//  log_message("%s: %s\n", __FILE__, __FUNCTION__);
    size_t total_size = nmemb * size;
    // head_t* hdr = static_cast<head_t*>(dlcalloc(1, sizeof(head_t) + total_size + sizeof(ftr_t)));
    head_t* hdr = static_cast<head_t*>(malloc_mmap(sizeof(head_t) + total_size));
    if (hdr) {
        memset(hdr, 0, sizeof(head_t) + total_size);
        hdr->size = total_size;
        return user(hdr);
    }
    return NULL;
}

extern "C" struct mallinfo mmap_mallinfo() {
    return g_malloc_dispatch->mallinfo();
}

extern "C" size_t mmap_malloc_usable_size(const void* ptr) {
    if (DebugCallsDisabled()) {
        return g_malloc_dispatch->malloc_usable_size(ptr);
    }

    // malloc_usable_size returns 0 for NULL and unknown blocks.
    if (ptr == NULL)
        return 0;

    const head_t* hdr = const_meta(ptr);

    // The sentinel tail is written just after the request block bytes
    // so there is no extra room we can report here.
    return hdr->size;
}

extern "C" int mmap_posix_memalign(void** memptr, size_t alignment, size_t size) {
  if (DebugCallsDisabled()) {
    return g_malloc_dispatch->posix_memalign(memptr, alignment, size);
  }

  if (!powerof2(alignment)) {
    return EINVAL;
  }
  int saved_errno = errno;
  *memptr = mmap_memalign(alignment, size);
  errno = saved_errno;
  return (*memptr != NULL) ? 0 : ENOMEM;
}

#if defined(HAVE_DEPRECATED_MALLOC_FUNCS)
extern "C" void* mmap_pvalloc(size_t bytes) {
  if (DebugCallsDisabled()) {
    return g_malloc_dispatch->pvalloc(bytes);
  }

  size_t pagesize = getpagesize();
  size_t size = BIONIC_ALIGN(bytes, pagesize);
  if (size < bytes) { // Overflow
    return NULL;
  }
  return mmap_memalign(pagesize, size);
}

extern "C" void* mmap_valloc(size_t size) {
  if (DebugCallsDisabled()) {
    return g_malloc_dispatch->valloc(size);
  }
  return mmap_memalign(getpagesize(), size);
}
#endif
